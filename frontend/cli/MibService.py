import os
from pysnmp.smi import builder

class MibService:
	def reload(self):
		mib_dirs=["/usr/share/snmp/mibs","/usr/local/share/mibs/site","/usr/local/share/mibs/ietf","/usr/local/share/mibs/iana","/usr/local/share/mibs/irtf","/usr/local/share/mibs/tubs"]
		for dir in mib_dirs:
			os.system("find %s -type f -not -name SNMP* -not -name RFC1155* -not -name RFC1213* -not -name TRANSPORT-ADDRESS-MIB* -not -name PYSNMP* -printf '%%f\n' | xargs -L 1 -I '{}' build-pysnmp-mib -o /usr/local/pysnmp_mibs/'{}'.py %s/'{}'" % (dir, dir))
		self.mibbuilder = builder.MibBuilder()
		newpath = list(self.mibbuilder.getMibPath())
		newpath.append("/usr/local/pysnmp_mibs")
		self.mibbuilder.setMibPath(*newpath)
		files=[]
		for file in os.listdir("/usr/local/pysnmp_mibs"):
			if not os.path.isdir("/usr/local/pysnmp_mibs/"+file):
				files.append(file.replace(".py", ""))
		self.mibbuilder.loadModules('SNMPv2-MIB', 'SNMP-FRAMEWORK-MIB', 'SNMP-COMMUNITY-MIB')
		print "Loaded base MIBs"
		for file in files:
			print "Loading file: %s" % file
			self.mibbuilder.loadModules(file)
		print "done"
if __name__=="__main__":
	ms = MibService()
	ms.reload()
		
		
