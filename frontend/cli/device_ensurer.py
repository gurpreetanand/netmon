#!/usr/bin/env python

import os
import sys
import time
import signal
import string
import os.path

# Try to load the DB driver or abort
try:
  from madnet_db import *
except Exception, e:
  print e
  sys.exit(1)
  
# Connect to the database
try:
  db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
  db.set_autocommit(0)
except Exception, e:
  print "ERROR: %s" % e

class Device_Ensurer:
  ips = []

  def __init__(self):

    self.cacheIps()

    self.ensure("servers")
    self.ensure("smb_servers")
    self.ensure("df_servers")
    self.ensure("syslog_access")

  def cacheIps(self):
    self.ips = [x["ip_address"] for x in db.select("SELECT ip_address FROM devices")]

  def ensure(self, tableName):
    print "Ensuring on %s" % (tableName)

    query = "SELECT * FROM %s" % (tableName)

    try:
      res = db.select(query)
    except Exception, e:
      pass

    for row in res:
      if row['ip'] not in self.ips:
        query = """
          INSERT INTO devices (ip_address, label, profile) VALUES ('%s', '%s', '%s')
        """ % (row['ip'], row['ip'], "snmp")

        print "Adding device entry for IP: %s" % (row['ip'])
        self.ips.insert(0, row['ip'])
        db.insert(query)

Device_Ensurer()
