#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Author:   Xavier Spriet <xavier@netmon.ca>
# Purpose: Netmon Activation System 
# Created: 25/04/06

import sys
import unittest
import urllib
import os
import re


# CLI Arguments Parser
from optparse import OptionParser

# xml parser
from xml.dom import minidom

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)
	

# Constants
VERSION = "5.1"



# CLI prompt with support for default values and mandatory prompts.
def prompt(prompt, default = None, mandatory = False):
	oprompt = prompt
	odef    = default

	if (None != default):
		default = "[%s]" % default
		prompt = "%s %s" % (prompt, default)

	prompt = "\x1b[" + "33;01m" + "%s : " % prompt + "\x1b[" + "39;49;00m"
	res = raw_input(prompt)

	if (("" == res) and (None != default)):
		return odef
	elif (("" == res) and (None == default) and (True == mandatory)):
		return prompt(oprompt, odef)
	return res


########################################################################
class activator:
	"""
	Processes Netmon Activation Requests	
	"""
	# Preferred network interface (see get_iface())
	iface = ""
	
	# Registration dictionary extracted from the DB
	registration_data = {}
	
	# Activation data provided by the user
	activation_dict   = {}
	
	#----------------------------------------------------------------------
	def __init__(self, options, parser):
		"""Constructor"""
		self.options = options
		self.parser = parser

		# Connect to the DB
		try:
			self.db = madnet_db("dbname=netmon35 host=127.0.0.1")
		except Exception, e:
			print "Unable to connect to database: %s" % e
			pass
		
		self.retrieve_registration_info()
		self.get_registration_info()
		self.process_activation()
	
	def retrieve_registration_info(self):
		""" Extract the registration data from the DB """
		query = "SELECT * FROM netmon"
		
		try:
			res = self.db.getRow(query)
		except Exception, e:
			#print e
			return
		
		self.registration_data = res
		
	def get_reg_val(self, key_name):
		""" Queries the local registration dictionary and attempts to find a value while avoiding exceptions """
		try:
			return self.registration_data[key_name]
		except Exception, e:
			return ''
	
	def get_registration_info(self):
		self.pprint("Netmon %s registration..." % VERSION)
		self.activation_dict['registration_key']=self.options.registration_key or prompt("Please enter your registration key", self.get_reg_val('registration_key'))
		self.activation_dict['company_name']=self.options.company_name or prompt("Please enter your company name", self.get_reg_val('company_name'))
		self.activation_dict['company_address']=self.options.company_address or prompt("Please enter your company's address", self.get_reg_val('company_address'))
		self.activation_dict['company_city']=self.options.company_city or prompt("Please enter your company's city", self.get_reg_val('company_city'))
		self.activation_dict['company_state']=self.options.company_state or prompt("Please enter your company's state or province", self.get_reg_val('company_state'))
		self.activation_dict['company_country']=self.options.company_country or prompt("Please enter your company's country", self.get_reg_val('company_country'))
		self.activation_dict['contact_first_name']=self.options.contact_first_name or prompt("Please enter your company contact's first name", self.get_reg_val('contact_first_name'))
		self.activation_dict['contact_last_name']=self.options.contact_last_name or prompt("Please enter your company contact's last name", self.get_reg_val('contact_last_name'))
		self.activation_dict['contact_email']=self.options.contact_email or prompt("Please enter your company contact's e-mail address", self.get_reg_val('contact_email'))
		self.activation_dict['contact_phone']=self.options.contact_phone or prompt("Please enter your company contact's phone number", self.get_reg_val('contact_phone'))
		self.activation_dict['contact_phone_ext']=self.options.contact_phone_ext
			# or prompt("Please enter your company contact's phone extension", self.get_reg_val('contact_phone_ext'))
		
		# get first active ethernet interface
		self.get_iface()
		
		self.activation_dict['mac_address'] = os.popen("/sbin/ifconfig " + self.iface).readlines()[0].split()[4]
		
		self.pprint("Querying Netmon Remote Activation Service...")
		
		# Prepare query-string
		url = "http://dev.netmon.ca/cronos/blank.php?module=content&action=activate_license"
		
		try:
			content = "\n".join(urllib.urlopen(url,urllib.urlencode(self.activation_dict)).readlines())
		except Exception, e:
			print "Unable to contact activation server: %s" % e

		data = {}
		try:
			activation_doc = minidom.parseString(content)
			activation_xml = activation_doc.firstChild

			for node in activation_xml.getElementsByTagName('item'):
				data[node.getAttribute('name')] = node.getAttribute('value')
		except Exception,e:
			print "Unable to parse server response: %s" % e
			print content
			sys.exit(1)
			
		if ('OK' == data['status']):
			self.activation_dict['activation_key'] = data['activation_key']
			self.activation_dict['expires'] = data['expires']
			self.activation_dict['devices'] = data['devices']
			self.activation_dict['is_trial'] = data['is_trial']
			
		elif ('ERROR' == data['status']):
			print data['error']
			sys.exit(1)
			
		else:
			print 'Unexpected response from activation server'
			sys.exit(1)
			
	def process_activation(self):
		self.pprint("Processing device activation")
		
		query = "TRUNCATE TABLE netmon"
		try:
			res = self.db.delete(query)
		except Exception, e:
			pass
		del self.activation_dict['mac_address']
		
		query = "INSERT INTO netmon(%s) VALUES(%s)" % (",".join(self.activation_dict.keys()),
							 	",".join([("%(x)s".replace('x', "%s" % x)) for x in self.activation_dict.keys()]))

		try:
			res = self.db.insert(query, self.activation_dict)
			if self.options.quiet:
				print "{}"
			os.system("/etc/init.d/netmon restart 2>&1 > /dev/null &")
		except Exception, e:
			print "Unable to process activation. %s" % e
			pass
	
	def __del__(self):
		self.db.close()
		
	# Prints a message in red...
	def pprint(self, msg):
		if not self.options.quiet:
			print "\x1b[" + "31;01m" +  msg + "\x1b[" + "39;49;00m"	
	
	def get_iface(self):
		from operator import itemgetter
		
		# Add new patterns ordered by preference to this list.
		patterns = ['eth(\d)', 'wlan(\d)']
		patterns = filter(re.compile, patterns)
		
		# Final storage for our ordered interface list
		score = {}
		# Raw interface list
		iface = [x.strip() for x in (os.popen("ifconfig -s | cut -d ' ' -f 1 | grep -v 'Iface'"))]
		
		for i in iface:
		    for pattern in patterns:
		        re_match = re.match(pattern, i)
		        if None != re_match:
		            score[i] = (len(iface)*(len(patterns) - (patterns.index(pattern)))) - int(re_match.group(1)) 
		            
		items = score.items()
		items.sort(key=itemgetter(1), reverse=True)
		self.iface = items[0][0]

	

if __name__=='__main__':
	# Prepare the CLI options-parser
	usage       = "Usage: %prog [--add-table=table,<>] [--type=] [target args]"
	version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2006 Netmon Inc."
	description = "Netmon activation system."
	parser = OptionParser(usage=usage, version=version, description=description)
	
	
	# General Options
	
	
	parser.add_option("-q", action="store_true", dest="quiet", default=False, help="Enable extra output. Useful for debugging")
	parser.add_option("--company-name",action="store",dest="company_name",help="Company name")
	parser.add_option("--contact-first-name", action="store", dest="contact_first_name",help="Contact individual first name")
	parser.add_option("--contact-last-name", action="store", dest="contact_last_name",help="Contact individual last name")
	parser.add_option("--contact-email",action="store",dest="contact_email",help="Contact individual e-mail address")
	parser.add_option("--contact-phone",action="store",dest="contact_phone",help="Contact individual telephone number")
	parser.add_option("--contact-phone-ext",action="store",dest="contact_phone_ext",help="Contact individual telephone extension")
	parser.add_option("--company-address",action="store",dest="company_address",help="Company street address")
	parser.add_option("--company-city",action="store",dest="company_city",help="Company city")
	parser.add_option("--company-state",action="store",dest="company_state",help="State or Province of company")
	parser.add_option("--company-country",action="store",dest="company_country",help="Company Country")
	parser.add_option("-k", "--registration-key", action="store", dest="registration_key", help="Registration Key")
	
	(options, args) = parser.parse_args()
	act = activator(options, parser)
	
