#!/usr/bin/env python
# CLI Arguments Parser

from optparse import OptionParser

import debinterface
import json

class IfaceMgr:
    def __init__(self):
        self.adapters = debinterface.interfaces.interfaces()
        
    def update_interfaces(self, interfaces):
        js = json.loads(interfaces)
	for adapter in self.adapters.adapters:
		if adapter.ifAttributes['name'] == js['name']:
			adapter.setAddressSource(js['source'])
			if js['source'] == 'static':
				adapter.setAddress(js['address'])
				adapter.setNetmask(js['netmask'])
				adapter.setGateway(js['gateway'])
			adapter.setAuto(True)
		print adapter.export()
	self.adapters.writeInterfaces()
        
    def dump_interfaces(self):
	payload = []
	for adapter in self.adapters.adapters:
		payload.append(adapter.export())
	print(json.dumps(payload))
        
if '__main__' == __name__:
    mgr = IfaceMgr()
    usage       = "Usage: %prog [-v --verbose]"
    version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
    description = "Update Netmon Enterprise LCD Display "
    parser = OptionParser(usage=usage, version=version, description=description)
    parser.add_option("-s", "--set", action="store", dest="jsondata", help="Update interface from JSON")
    parser.add_option("-g", "--get-interfaces", action="store_true", dest="show", default=False, help="Dump interface options")
    (options, args) = parser.parse_args()

    if options.show:
        mgr.dump_interfaces()
    elif options.jsondata:
        mgr.update_interfaces(options.jsondata)
    else:
        parser.print_help()
