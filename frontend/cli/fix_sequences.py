#!/usr/bin/env python
# -*- coding: utf-8 -*-

# archive.py - Performs maintenance tasks against Netmon DB

import sys
import os
import time
import signal

############################################################################
### LOCKING MECHANISM
LOCKFILE = "/var/run/sequence.lock"
TIMEOUT  = 3600*48

mark = int(time.time())
if (os.path.isfile(LOCKFILE)):
	if ((mark - os.path.getmtime(LOCKFILE)) >= TIMEOUT):
		print "Clearing stale lockfile"
		os.unlink(LOCKFILE)
	else:
		print "Another instance of this script is already running. Aborting"
		sys.exit(0)

fp = open(LOCKFILE, "w")
fp.close()

def signal_handler(sig, frame):
	os.unlink(LOCKFILE)
	sys.exit(1)
	
signal.signal(signal.SIGINT, signal_handler)
############################################################################


# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	signal_handler(0,0)
	
# Connect to the database
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1")
	db.set_autocommit(0)
except Exception, e:
	print "ERROR: %s" % e
	signal_handler(0,0)
	

if __name__=='__main__':
	try:
		#get list of sequences
		seqs = db.select("select pc1.relname as seq, pc2.relname as table, c.attname as field from pg_depend, pg_class pc1, pg_class pc2, pg_attribute c where pc1.oid=pg_depend.objid and pc2.oid=pg_depend.refobjid and c.attnum=pg_depend.refobjsubid and c.attrelid=pc2.oid and pc1.relkind='S' and pc1.relname not like 'pg_toast%%'")
		#loop through them and fix the sequence
		for s in seqs:
			query="SELECT setval('%s', COALESCE((SELECT %s FROM %s ORDER BY %s DESC LIMIT 1), 0)+1)" % (s['seq'], s['field'], s['table'], s['field'])
			print(query)
			os.system("echo \"%s;\"|psql netmon35" % query)
	except Exception, e:
		print "ERROR: %s" % e
		
	signal_handler(0,0)
