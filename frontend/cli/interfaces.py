#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Interface discovery and description utility
# Created:   Jan 30, 08
# SVN:       $Id$


# Imports
import os
import sys
import socket
import fcntl
import struct
import signal


# Packet payload decoder
from impacket.ImpactDecoder import EthDecoder, TCPDecoder, LinuxSLLDecoder

# Try to load the python libpcap driver
try:
    import pcapy
except Exception, e:
    print e
    sys.exit(1)
    
# CLI Arguments Parser
from optparse import OptionParser

class IfaceAgent:
    
    ifaces      = []
    connections = []
    decoder     = None
    cap         = None
    
    def __init__(self):
        self.ifaces = [{'iface':x} for x in pcapy.findalldevs()]
        signal.signal(signal.SIGALRM, self.abort)
        
        
    def list_interfaces(self):
        print "\n".join([x['iface'] for x in self.ifaces])
        
    def get_iface_ip(self, iface):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', iface[:15]))[20:24])
    
    def decode_packet(self, hdr, data):
        pkt = self.decoder.decode(data)
        self.connections.append({'src': pkt.child().get_ip_src(), 'dst': pkt.child().get_ip_dst()})
        
    
    def get_port_mirroring(self):
        signal.alarm(50)
        
        self.cap = pcapy.open_live('any', 500, False, 100)
        
        # Instanciate the proper decoder based on the DDL protocol in use
        datalink = self.cap.datalink()
        if pcapy.DLT_EN10MB == datalink:
            self.decoder = EthDecoder()
        elif pcapy.DLT_LINUX_SLL == datalink:
            self.decoder = LinuxSLLDecoder()
        else:
            raise Exception("Datalink type not supported: " % datalink)
        
        # Capture a 100 packets sample in self.connections
        self.cap.setfilter('tcp')
        self.cap.loop(100, self.decode_packet)
        
        # Get a list of our ip addresses
        ips = []
        for iface in self.ifaces:
            if 'any' != iface['iface']:
                ip = self.get_iface_ip(iface['iface'])
                if ip not in ips:
                    ips.append(ip)
                
        
        # Look for a packet where the source and destination are not in our IP list
        for cnx in self.connections:
            if (cnx['src'] not in ips) and (cnx['dst'] not in ips):
                print "Port Mirroring  appears to be active"
                sys.exit(os.EX_OK)
        
        self.abort()
        
    def abort(self, sig = None, frame = None):
        print "Port Mirroring does not appear to be active!"
        try:
            del(self.cap)
        except Exception,e:
            pass
        sys.exit(os.EX_UNAVAILABLE)
        
        
        
        

        
if '__main__' == __name__:
    agent = IfaceAgent()
    agent.get_port_mirroring()
        
                