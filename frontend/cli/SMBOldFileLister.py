#!/usr/bin/env python
import os
import time
import signal
# CLI Arguments Parser
from optparse import OptionParser
signal.signal(signal.SIGCHLD, signal.SIG_DFL)
class SMBOldFileLister:

	def __init__(self, options):
		self.path=options.path
		self.age=options.age
		self.user=options.user
		self.passw=options.passw

        def run(self, cmd):
                p=os.popen(cmd)
                s=p.read()
		if s != "" and s!="\n":
			print(s)

	def getFiles(self):
		mountpoint="/mnt/smb_%d" % int(time.time())
		self.run("mkdir %s" % mountpoint)
		self.run("mount -t smbfs -o username=%s,password=%s,uid=root %s %s" % (self.user, self.passw, self.path, mountpoint))
		os.chdir(mountpoint)
		self.run("find . -mtime +%s -xtype f -print0|xargs -0 ls -d -1 -S " % (self.age))
		os.chdir("/root")
		time.sleep(5)
		self.run("umount %s" % mountpoint)
		self.run("rmdir %s" % mountpoint)

if __name__=='__main__':    
	# Prepare the CLI options-parser
	usage       = "Usage: %prog -p PATH -z SIZE -u USER -s PASSWORD"
	version     = "%prog (Netmon Inc.)"
	description = "Find a list of old files on a remote file system."
	parser = OptionParser(usage=usage, version=version, description=description)
	
	# General Options
	parser.add_option("-p", action="store", dest="path", default='', help="The path of the SMB shared folder, ie //10.10.1.23/share")
	parser.add_option("-z", action="store", dest="age", default=5, help="The minimum number of days since the last access of the files to list.")
	parser.add_option("-u", action="store", dest="user", default='', help="The username for accessing the share.")
	parser.add_option("-s", action="store", dest="passw", default='', help="The password for accessing the share.")


	(options, parser) = parser.parse_args()
	
	if options.path=='' or options.age =='' or options.user =='' or options.passw =='':
		parser.print_help()
		os._exit(os.EX_OK)
	
	s=SMBOldFileLister(options)
	s.getFiles()

