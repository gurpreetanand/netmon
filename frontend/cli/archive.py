#!/usr/bin/env python
# -*- coding: utf-8 -*-

# archive.py - Performs maintenance tasks against Netmon DB

import sys
import os
import time
import signal

############################################################################
### LOCKING MECHANISM
LOCKFILE = "/var/run/archive.lock"
TIMEOUT  = 3600*48

mark = int(time.time())
if (os.path.isfile(LOCKFILE)):
	if ((mark - os.path.getmtime(LOCKFILE)) >= TIMEOUT):
		print "Clearing stale lockfile"
		os.unlink(LOCKFILE)
	else:
		print "Another instance of this script is already running. Aborting"
		sys.exit(0)

fp = open(LOCKFILE, "w")
fp.close()

def signal_handler(sig, frame):
	os.unlink(LOCKFILE)
	sys.exit(1)
	
signal.signal(signal.SIGINT, signal_handler)
############################################################################


# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	signal_handler(0,0)
	
# Connect to the database
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1")
	db.set_autocommit(0)
except Exception, e:
	print "ERROR: %s" % e
	signal_handler(0,0)
	
# Variable declaration
time_week = 3600*24*7
daemons = {}

# Assign tables to daemons
daemons['mod_ip']      = {'tables': ['netflow', 'agg_netflow']   }
daemons['arpmond']     = {'tables': ['arptable']                 }
daemons['dfmond']      = {'tables': ['df_server_log']            }
daemons['smbmond']     = {'tables': ['smb_server_log']           }
daemons['resolvemond'] = {'tables': ['hosts']                    }
daemons['srvmond']     = {'tables': ['server_log']               }
daemons['snmpmond']    = {'tables': ['agg_snmp_log', 'snmp_log'] }
daemons['snmptrapd']   = {'tables': ['snmptrap_log']             }
daemons['syslogmond']  = {'tables': ['syslog']                   }
daemons['mod_http']    = {'tables': ['web_traffic']              }
daemons['oidmond']     = {'tables': ['oid_log']                  }
daemons['webmond']     = {'tables': ['url_log']                  }

# Itterate through the daemons list
for daemon in daemons.keys():
	print "Analyzing daemon %s" % daemon
	weeks_res = None
	
	# Find the data_archival value associated to this daemon
	weeks_query = """
	SELECT a.value FROM daemonsconfig a, daemons b
	WHERE a.daemon_id = b.id
	AND a.var = 'data_archival'
	AND b.name = '%s'
	LIMIT 1""" % daemon
	
	try:
		weeks_res = db.getRow(weeks_query)
	except Exception, e:
		print e
		signal_handler(0,0)
	
	# Skip if we find a -1 value for data_archival
	#print "%s => %s" % (daemon, weeks_res['value'])
	try:
		if (int(weeks_res['value']) == -1):
			print "Skipping daemon %s" % daemon
			continue
	except Exception, e:
		print e
		signal_handler(0,0)
	
	# For each of the tables associated with this daemon, clear old data
	for table in daemons[daemon]['tables']:
		
		# Special case to keep custom hostnames in the DB
		if (table == "hosts"):
			time_start = int(time.time() -  (time_week * int(weeks_res['value'])))
			query = "DELETE FROM %s WHERE timestamp < %s AND host_name_type <> 'CUSTOM'" % (table, time_start)
			print query
			print "Cleaning up table %s" % table
			commit = db.delete(query)
			continue
		
		# If we don't want any data, truncate is faster
		if (int(weeks_res['value']) == 0):
			query = "TRUNCATE TABLE %s" % table
		else:
			time_start = int(time.time() -  (time_week * int(weeks_res['value'])))
			query = "DELETE FROM %s WHERE timestamp < %s" % (table, time_start)
			print query
		
		# Display and execute the query
		#print query
		print "Cleaning up table %s" % table
		commit = db.delete(query)
	
# Once all old data has been cleared, run a vacuum.
# print "Vacuuming Database"
# os.system("vacuumdb -f -z netmon35")

signal_handler(0,0)
