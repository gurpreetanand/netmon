#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
import os.path
import os
import signal

############################################################################
### LOCKING MECHANISM
LOCKFILE = "/var/run/aggregator.lock"
TIMEOUT  = 3600*18

mark = int(time.time())
if (os.path.isfile(LOCKFILE)):
	if ((mark - os.path.getmtime(LOCKFILE)) >= TIMEOUT):
		print "Clearing stale lockfile"
		os.unlink(LOCKFILE)
	else:
		print "Another instance of this script is already running. Aborting"
		sys.exit(0)

fp = open(LOCKFILE, "w")
fp.close()

def signal_handler(sig, frame):
	os.unlink(LOCKFILE)
	sys.exit(1)
	
signal.signal(signal.SIGINT, signal_handler)
############################################################################

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print "Unable to establish connection to the database.\n%s" % e
	signal_handler(0,0)
	sys.exit(1)


# Connect to the database
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1")
	db.set_autocommit(0)
except Exception, e:
	print "ERROR: %s" % e
	signal_handler(0,0)
	sys.exit(1)



mark = time.time()	
	
# Aggregation query
query = """
INSERT INTO agg_netflow(
    dst_ip, src_ip, octets, lowest_port, 
    timestamp, in_iface, out_iface, flow_src)
  
  SELECT 
    dst_ip, src_ip, SUM(octets)::int8,
    (CASE WHEN dst_port < src_port THEN dst_port ELSE src_port END) AS "port",
    MAX(timestamp), in_iface, out_iface, flow_src
  
  FROM netflow
    
  GROUP BY src_ip, dst_ip, port, in_iface, out_iface, flow_src
  """

# Execute query
try:
	res = db.insert(query)
except Exception, e:
	print e
	signal_handler(0,0)
	sys.exit(-1)




print "Aggregate operation successful (%0.2fs)" % (time.time() - mark)

mark = time.time()

query = "TRUNCATE TABLE netflow"

try:
	res = db.delete(query)
except Exception, e:
	print e
	signal_handler(0,0)
	sys.exit(-1)
	
print "TRUNCATE operation successful (%0.2fs)" % (time.time() - mark)

signal_handler(0,0)