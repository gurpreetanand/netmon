#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Author:   Xavier Spriet <xavier@netmon.ca>
# Purpose: Netmon Topology Mapping System
# Created: 26/02/08


import datetime
import commands
import socket
import sys
import os

# CLI Arguments Parser
from optparse import OptionParser

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)
	
# Attempt to import the JSON writer module
from json import JsonWriter
	
# Connect to the database
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
	db.set_autocommit(0)
except Exception, e:
	print "ERROR: %s" % e
	sys.exit(1)
	
	
class Mapper:
	
	snmp_devices = {}
	nodes		= {}
	routers	  = []
	
	def __init__(self, options, parser):
		
		# Increase the recursion depth limit
		sys.setrecursionlimit(5000)
		
		# Store CLI options
		self.options = options

		# Retrieve our global nodes pool
		self.initialize_global_pool()
		
		# Move switches and nodes to their appropriate router
		self.link_nodes()
		
		#print " ========= NODES ========= "
		self.dump_nodes(self.nodes)


	def dump_nodes(self, root, offset = 0):
		
		
		"""
		for node in root.values():
			print "%s%s -> %s (%s trackers - %s open ports)" % ("\t"*offset, node['ip'], node['type'], len(node['trackers']), len(node['ports']))
			if node.has_key('nodes'):
				self.dump_nodes(node['nodes'], offset+1)
				
		
		"""
		writer = JsonWriter()
		print writer.write(self.nodes)
		
		

	def initialize_global_pool(self):
		self.load_saved_settings()
		self.locate_routers()
		self.extract_snmp_data()
		self.extract_portscan_data()
		self.extract_tracker_data()
		self.extract_disk_tracker_data()
		
	def extract_portscan_data(self):		
		# Extract device list from portscan report
		query = "SELECT srv_ip, timestamp, ports FROM scan_log ORDER BY srv_ip ASC"
		try:
			data = db.select(query)
		except Exception, e:
			print "Database Error: %s" % e
			sys.exit(1)
		
		for node in data:
			self.register_ports(node['srv_ip'], node['ports'])
			"""
			self.nodes[node['srv_ip']] = {'ip': node['srv_ip'], 'timestamp': node['timestamp'], 'ports': [], 'type': 'node'}
			if node['ports'] != '{}':
				self.nodes[node['srv_ip']]['ports'] = node['ports']
			"""
		
	def extract_snmp_data(self):
		# Get all SNMP devices and profiles
		query = "SELECT a.id, a.ip_address, a.label, a.status, a.profile, COUNT(b.id) AS ifaces FROM devices a LEFT OUTER JOIN interfaces b ON (b.device_id = a.id) GROUP BY a.id, a.ip_address, a.label, a.status, a.profile ORDER BY ip_address ASC"
		try:
			data = db.select(query)
		except Exception, e:
			print "Database Error: %s" % e
			sys.exit(1)
			
		# Find out the device type for each device based on the profile
		for node in data:
			if node['profile'] in ['dash_akcpsp2', 'dash_enviro', 'dash_netbotz', 'dash_sensorhawk']:
				type = 'enviro'
			elif node['profile'] in ['dash_barracuda_spam']:
				type = 'spam'
			elif node['profile'] in ['dash_cisco_asa', 'dash_checkpoint_firewall', 'dash_cisco_firewall']:
				type = 'firewall'
			elif node['profile'] in ['dash_cisco_router']:
				type = 'router'
			elif node['profile'] in ['dash_cisco_vpn']:
				type = 'vpn'
			elif node['profile'] in ['dash_linux_generic', 'dash_linux_adv']:
				type = 'linux'
			elif node['profile'] in ['dash_lotus_domino_7', 'dash_lotus_domino_7_informant']:
				type = 'domino'
			elif node['profile'] in ['dash_printer_generic', 'dash_printer_hp']:
				type = 'printer'
			elif node['profile'] in ['dash_ups_apc']:
				type = 'ups'
			elif node['profile'] in ['dash_windows_generic', 'dash_windows_iis', 'dash_windows_snmp_inf_adv', 'dash_windows_snmp_inf_exch', 'dash_snmp_inf_iis', 'dash_windows_snmp_inf_sql', 'dash_windows_snmp_inf_std']:
				type = 'windows'
			elif node['ifaces'] > 3:
				type = 'switch'
			else:
				type = 'server'

			self._register_node(node['ip_address'])
			self.nodes[node['ip_address']]['type'] = type
			
		
	def extract_tracker_data(self):
		query = "SELECT srv_id AS id, ip, status, latency, protocol, port FROM servers ORDER BY ip DESC"
		try:
			data = db.select(query)
		except Exception, e:
			print "Database Error: %s" % e
			sys.exit(1)
			
		for node in data:
			self.register_tracker(node['ip'], node)
			
	def _convert_datetime(self, tstamp):
		return tstamp.strftime('%m/%d/%Y %H:%M:%S')
	
	
	def extract_disk_tracker_data(self):
		query = """select 'SMB' AS type, 'smb_'||srv_id AS id, share AS name, srv_id, ip, servername, status,
		threshold, ((a.blocksize * a.total::int8) - (a.blocksize * a.available::int8))::int8 AS used,
		(a.blocksize * a.total::int8)::int8 AS total, timestamp, message
		FROM smb_servers a
		UNION
		select 'DF' AS type, 'df_'||srv_id AS id, partition AS name, srv_id, ip, servername, status,
		threshold, ((1024 * total::int8) - (1024 * available::int8))::int8 AS used, (1024 * total::int8)::int8 AS total,
		timestamp, message
		FROM df_servers b
		ORDER BY status DESC, srv_id ASC"""
		
		try:
			data = db.select(query)
		except Exception, e:
			print "Database Error: %s" % e
			sys.exit(1)
			
		for node in data:
			#print "registering disk tracker against %s" % node['ip']
			self.register_tracker(node['ip'], node)
	
	
	def register_tracker(self, ip, tracker):
		try:
			node = self.nodes[ip]
		except Exception, e:
			self._register_node(ip)
			self.register_tracker(ip, tracker)
	
		if tracker['id'] not in self.nodes[ip]['trackers'].keys():
			#print "Adding tracker %s to node %s" % (tracker['id'], ip)
			self.nodes[ip]['trackers'][tracker['id']] = tracker
	
	
	def register_ports(self, ip, ports):
		try:
			node = self.nodes[ip]
		except Exception, e:
			self._register_node(ip)
			self.register_ports(ip, ports)
		
		
		for port in ports:
			if port not in self.nodes[ip]['ports']:
				self.nodes[ip]['ports'].append(port)
		return True
			
	
	
	def _register_node(self, ip):
		if self.nodes.has_key(ip):
			return
		
		coords = [x for x in self.coords if x['node_ip'] == ip]
		
		self.nodes[ip] = {'trackers': {}, 
						'ports': [], 
						'ip': ip, 
						'type': 'node', 
						'timestamp': self._convert_datetime(datetime.datetime.now()),
						}
		
		if coords != []:
			self.nodes[ip]['coords'] = coords
				
		 
	def locate_routers(self):
	   # Extract from 'route' (netstats) the local gateway(s)
	   # run traceroutes
	   gateways = commands.getoutput("route | awk '/default/' | awk '{print $2}'").split("\n")
	   
	   for gw in gateways:
	   	if gw not in self.nodes.keys():
	   			self._register_node(gw)
	   	self.nodes[gw]['type'] = 'router'
	   	
	def load_saved_settings(self):
		# First, we'll load node coordinates
		 query = "SELECT * FROM topology_nodes ORDER BY node_ip ASC"
		 
		 try:
		 	nodes = db.select(query)
		 except Exception, e:
		 	nodes = []
		 	
		 # Now we'll load node to node connections
		 query = "SELECT parent_ip, child_ip FROM topology_connections ORDER BY parent_ip ASC"
		
		 try:
		 	connections = db.select(query)
		 except Exception, e:
		 	connections = []
		 	
		 self.coords = nodes
		 self.connections = connections
		
		
	
	def link_nodes(self):
		# Get the list of interfaces for SNMP devices, and dump them in the appropriate device
		#
		query = """
		SELECT d.ip_address AS device_ip, a.ip AS ip, i.interface AS interface
		FROM devices d
			 INNER JOIN interfaces i ON (i.device_id = d.id)
				 INNER JOIN arptable a on a.mac=i.mac and a.ip != d.ip_address and a.ip IN (SELECT ip FROM arptable WHERE mac = i.mac AND ip != d.ip_address ORDER BY timestamp DESC LIMIT 1)
		WHERE a.ip != d.ip_address
		ORDER BY d.ip_address ASC, a.ip ASC
		"""
		
		try:
			data = db.select(query)
		except Exception, e:
			print "Database Error: %s" % e
			sys.exit(1)
			
		
		# For each node retrieved from the SNMP set, perform recursive linking against the nodes tree.
		ungroup_queue = []
		group_queue = []
		
		# This dictionary will contain parent entries
		parents = {}
		
		for node in data:
			#if node['ip'] not in self.nodes:
			#	self._register_node(node['ip'])
			#if node['device_ip'] not in self.nodes:
			#	self._register_node(node['device_ip'])
			if not parents.has_key(node['device_ip']):
				parents[node['device_ip']] = []
				
			if node['ip'] in self.nodes:
				parents[node['device_ip']].append(self.nodes[node['ip']])
				self.nodes.pop(node['ip'])
			else:
				parents[node['device_ip']].append({'trackers': {}, 
							'ports': [], 
							'ip': node['ip'], 
							'type': 'node', 
							'timestamp': self._convert_datetime(datetime.datetime.now()),
							})
			
				
		for parent in parents:
			for tree in self.nodes:
				#print "Searching for %s in %s" % (parent, tree)
				if not self._link_node(self.nodes[tree], parent, parents[parent]):
					for node in parents[parent]:
						group_queue.append(node)
				else:
					  for node in parents[parent]:
					  	ungroup_queue.append(node['ip'])
			
		#self.dump_nodes(self.nodes)
		# Now that SNMP nodes are linked, let's create the saved connections
		for conx in self.connections:
			if self.nodes.has_key(conx['child_ip']):
				#node = self.nodes.pop(conx['child_ip'])
				node = self.nodes[conx['child_ip']]
				for node_ip in self.nodes:
					if self._connect(self.nodes[node_ip], conx, node):
						if conx['child_ip'] not in ungroup_queue:
							ungroup_queue.append(conx['child_ip'])
		
		
		for node in group_queue:
			self.nodes[node['ip']] = node
		
		for node in ungroup_queue:
			self._unlink_node(self.nodes, node)
	

	
	def _link_node(self, root, parent, payload):
		#print "Looking for %s in %s" % (parent, root['ip'])

		
		if root['ip'] == parent:
			if not root.has_key('nodes'):
				root['nodes'] = {}
				for child in payload:
					root['nodes'][child['ip']] = child
			#print "Found %s" % parent
			return True
		else:
			if root.has_key('nodes'):
				for subtree in root['nodes']:
					#print "Examining subtree %s" % subtree
					if True == self._link_node(root['nodes'][subtree], parent, payload):
						return True
		return False
		
	# Recursive function that links nodes together into an existing tree
	def _connect(self, root, conx, node):
		#try:
		#print "Looking for %s on behalf of %s" % (conx['parent_ip'], conx['child_ip'])
		if root.has_key('nodes'):
			if not root['nodes'].has_key(conx['parent_ip']):
				for subtree in root['nodes']:
					#print "\tAnalysing subtree %s" % subtree
					if self._connect(root['nodes'][subtree], conx, node):
						return True
				return False
			if not root['nodes'][conx['parent_ip']].has_key('nodes'):
				root['nodes'][conx['parent_ip']]['nodes'] = {}
			root['nodes'][conx['parent_ip']]['nodes'][conx['child_ip']] = node
			return True
		else:
			pass
		#except Exception, e:
		#	print "ERROR: %s" % e
		#	self.dump_nodes(self.nodes, 0)
		#	sys.exit(1)
	
	def _unlink_node(self, root, node):
		if root.has_key(node):
			return root.pop(node)
		""" - Actually i don't think recursion is required here
		if root.has_key('nodes'):
			for subtree in root['nodes']:
				self._unlink(root['nodes'][subtree], node)
		"""
	
if __name__ == '__main__':
	# Jump to the script's root
	# 20181217 - Fixed Error of No file or directory '' - Gurpreet Anand
  	os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

	usage	   = "Usage: %prog [-v --verbose]"
	version	 = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2008 Netmon Inc."
	description = "Network Topology Mapping Tool"
	parser = OptionParser(usage=usage, version=version, description=description)
	
	
	(options, parser) = parser.parse_args()
	obj = Mapper(options, parser)