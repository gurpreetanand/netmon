<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @package    MADNET
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

ob_implicit_flush(true);
ini_set('track_errors', true);
ini_set('html_errors', false);
ini_set('magic_quotes_runtime', false);


/**
  * Accepts command line input
  *
  * @return string
  * @param int $length
  */
function read ($length='255')
{

   $line = fgets (STDIN ,$length);
   return trim ($line);
}

/**
  * Prompts for a value with a message and return user response
  *
  * This function displays a message, prompts for a value
  * Checks if the value is optional or not (indicates to the
  * user if it is), check if there is a default value and return
  * the result or prompt again if necessary
  *
  * @return string
  * @param string $message
  * @param mixed/string $default
  * @param bool $optional
  */
function prompt($message, $default = NULL, $optional = 0) {
	$val = "";
	if ($default) { $message .= " [ $default ]: ";}
	if ($optional) { echo "[optional] $message"; $val = read(); }
	else {
		while (TRUE) {
			echo $message;
			$val = read();
			if (($val == "") && ($default) && (!empty($default))) {
				return $default;
			} elseif (!empty($val)) {
				return $val;
			}
		}
	}
}


/**
  * @author Xavier Spriet
  * Loads core configuration file
  */
require_once("../api/config.inc.php");

/**
  * @author Xavier Spriet
  * Load the Controler Facade
  */
require_once(ROOT . "/modules/core/classes/controler_facade.class.php");

$controler = new Controler_Facade;

$registry = Registry::get_registry();




?>