#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author:   Xavier Spriet <xavier@netmon.ca>
# Purpose: Netmon Database Backup Management system 
# Created: 18/10/06

import os
import re
import sys
import pwd
import time
import string
import signal
import os.path
import smtplib
import dbtoxml
import commands

# String escaping
from re import escape

# XML DOM Parser
from xml.dom import minidom

# CLI Arguments Parser
from optparse import OptionParser

# Rely on the snapshot class to retrieve SMTP settings
from snapshot import Log_Snapshot

# Try to load the DB driver or abort
try:
  from madnet_db import *
except Exception, e:
  print e
  sys.exit(1)

# Connect to the database
try:
  db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
  db.set_autocommit(0)
except Exception, e:
  print "ERROR: %s" % e
  #signal_handler(0,0)
  sys.exit(1)

""" This is the core backup management module for Netmon NMS.
This module must be provided with a valid list of options provided
directly by the optparse module (see bellow) and can perform postgreSQL
backups on a number of different targets."""
class Backup_Manager:
  
  # The ID of the backup operation in the database.
  id = 0
  
  # The filename for the actual final backup
  dest_filename = ""
  
  # Mountpoint for NFS, FTP, Rsync and SMB backups
  mountpoint = ""
  
  # Default destination for local backups.
  local_target = "/var/backups"
  
  
  """ Retrieve CLI arguments and the structure of our postgres DB """
  def __init__(self, options, parser):
    print "Backup Manager component initiated"
    self.options = options
    self.parser = parser
    
    self._get_db_structure()
  
  
  """
  Validates the CLI input, and triggers the appropriate callback 
  to launch the backup
  """
  def init_backup(self):
    # Determine which args are mandatory depending on the target type.
    if (self.options.type == "smb"):
      mandatory = ['share', 'ip_address', 'from', 'to']
    elif (self.options.type == "local"):
      mandatory = ['label', 'from', 'to']
    else:
      print "Only SMB and Local backups are supported at the moment."
      mandatory = ['type']
    
    # Exit if mandatory arguments are missing in action.
    for opt in mandatory:
      if (None == getattr(self.options, opt)):
        print "Error: The option '--%s' is mandatory for %s backups" % (opt, self.options.type or "all")
        sys.exit(os.EX_USAGE)
    
    # Create the backup entity in the DB (for tracking purposes)
    query = """
    INSERT INTO backups 
    (tables, owner, status, notify, init_timestamp, description) 
    VALUES('%s', '%s', '%s', '%s', %s, '%s')
    """ % ("full", 
           escape(self.options.owner), 'pending', 
           escape(self.options.notify), 
           int(time.time()), 
           escape(self._describe_backup()))
    
    query = query.replace("  ", "")
    try:
      db.insert(query)
    except Exception, e:
      print "Unable to register the backup operation. Continuing anyway: %s" % e
      pass
    
    # Retrieve the ID of our new backup entity
    
    query = "SELECT id FROM backups ORDER BY id DESC LIMIT 1"
    try:
      row = db.getRow(query)
      self.id = row['id']
      #print "backupID=%d" % self.id
    except Exception, e:
      pass    
    
    # Fork into the background
    self.detach()
    self.add_event("Initializing backup")
    
    # Trigger the appropriate backup callback.
    if (self.options.type == "smb"):
      self.backup_smb()
    elif (self.options.type == "local"):
      self.backup_local()

  """ Returns a human-readable representation of the backup operation """
  def _describe_backup(self):
    if (self.options.type == "smb"):
      dest = "SMB share %s on %s" % (self.options.share, self.options.ip_address)
    elif (self.options.type == "local"):
      dest = "Local file with label %s" % (self.options.label)
    else:
      dest = "Unknown"
    
    
    desc = "Destination: %s\nOwner: %s\nNotify: %s" \
         % (dest, self.options.owner, 
      self.options.notify)
    
    self._print_verbose(desc)
    return desc
  
  """ Adds a tracking event in the DB for the current backup entity """
  def add_event(self, str):
    self._print_verbose(str)
    if (self.id > 0):
      query = """
      INSERT INTO backup_events(backup_id, timestamp, event)
      VALUES(%s, '%s', '%s')""" % (self.id, int(time.time()), re.escape(str))
      
      try:
        res = db.insert(query)
      except Exception, e:
        print "A database error has occured while registering the event '%s'.\n%s" % (str, e)
        pass
  
  """ Updates the 'status' field of the backup entity (for tracking purposes) """
  def set_status(self, status):
    self._print_verbose("Setting backup status to %s" % status)
    if (self.id > 0):
      query = "UPDATE backups SET status = '%s' WHERE id = %d" % (status, self.id)
      
      try:
        res = db.update(query)
      except Exception, e:
        print "Unable to update backup status!!"
        pass
  
  """
  This method uses our dbtoxml module to retrieve an XML representation
  of our database schema and then parses the resulting XML document using
  a DOM parser to create a native data-structure representing the DB.
  """
  def _get_db_structure(self):
    try:
      db = dbtoxml.XMLDatabase()      
      xml = db.getXML(dbtoxml.DataModel("netmon35", 0))
    except Exception, e:
      print "Error: Unable to analyse database structure: %s" % e
      sys.exit(os.EX_DATAERR)
    
    data = {}
    root = minidom.parseString(xml)
    db_dom = root.firstChild
    
    components = db_dom.getElementsByTagName("component")
    for comp in components:
      tables = {}
      for table in comp.getElementsByTagName("table"):
        # name, type, label, index
        tables[table.getAttribute("name")] = \
              { \
                "type": table.getAttribute("type"),
                "label": table.getAttribute("label"),
                "index": table.getAttribute("index")
          }
      data[comp.getAttribute("name")] = tables
    #self._print_verbose(data)
    self.db_structure = data
  
  """ Triggers an SMB backup operation """
  def backup_smb(self):
    self._smb_mount()
    self._perform_backup()
    self._smb_umount()
    self.notify("success")
  
  """ Triggers a local FS backup operation """
  def backup_local(self):
    self._infer_dest_filename()
    self._set_file_label()
    self._perform_backup()
    self._set_label_not_busy()
    self.notify("success")
    
  """ Creates a label for the local file in the DB for the VFS modules """
  def _set_file_label(self):
    if (None != self.options.label):
      query = """
      INSERT INTO fs_files(directory_id, label, description, filename, busy)
      VALUES(
          (SELECT id FROM fs_directories WHERE label ILIKE 'backups' LIMIT 1),
          '%s', 'Netmon Backup', '%s', true)
      """ % (re.escape(self.options.label), os.path.basename(self.dest_filename))
      try:
        res = db.insert(query)
      except Exception, e:
        print "Unable to assign a label to the local backup file: %s" % e
        pass
      
  """ Removes 'busy' bit from the file-label """
  def _set_label_not_busy(self):
    if (None != self.options.label):
      query = "UPDATE fs_files SET busy = false WHERE filename = '%s'" \
            % (os.path.basename(self.dest_filename))
      
      try:
        res = db.update(query)
      except Exception, e:
        print "Unable to unlock the file label. %s" % e
        pass
  
  """ Sends a notification email with description and status of the backup operation """
  def notify(self, status):
    self.add_event("Backup operation completed. Sending notification email.")
    self.set_status(status)
    
    
    if (None != self.options.notify):
      to_addr = self.options.notify
      try:
        snap = Log_Snapshot(self.options, self.parser)
        from_addr = snap.smtp['smtpaccount'] or "backups@netmon.ca"
        smtp_server = snap.smtp['smtpserver'] or "localhost"
      except Exception, e:
        print "Error retrieving SMTP settings: %s" % e
        from_addr = "backups@netmon.ca"
        smtp_server = "localhost"
        pass
      
      msg = """
Here are the results of your Netmon appliance backup:

Backup operation status: %s

Backup Description: %s""" % (status, self._describe_backup())  
      
      
      payload = ("From: %s\r\nTo: %s\r\nSubject: Netmon Backup Operation Completed\r\n\r\n%s"
           % (from_addr, to_addr, msg))
      # Dispatch the notification email.
      server = smtplib.SMTP(smtp_server)
      print payload
      server.sendmail(from_addr, to_addr, payload)
      server.quit()
  
  """ Triggers the pg_dump | bzip2 | tar operation """
  def _perform_backup(self):
    tmp_file = "/var/backups/netmon35.db"
    cmd = "sudo pg_dump -U postgres -Fc netmon35 > %s" % (self.dest_filename)
    
    (status, out) = commands.getstatusoutput(cmd)
    self._print_verbose("Command %s returns %s with output: %s" % (cmd, status, out))

    # Temporarily disable truncation
    #self._truncate_data()
      
  """ Mounts a remote SMB share locally """
  def _smb_mount(self):
    self.add_event("Mounting remote SMB share...")
    
    # Create a pseudo-unique mountpoint
    self.mountpoint = "/mnt/smb_%d" % int(time.time())
    
    try:
      os.mkdir(self.mountpoint)
    except Exception, e:
      err = "Error: Unable to create the SMB Mountpoint %s\naborting" % self.mountpoint
      print err
      self.add_event(err)
      sys.exit(os.EX_CANTCREAT)
    
    # Build the sys command line to execute to perform the mount
    options = []
    if (None != self.options.username):
      options.append("username=%s" % re.escape(self.options.username))
    if (None != self.options.password):
      options.append("password=%s" % re.escape(self.options.password))
    options.append("uid=www-data")
    options.append("gid=www-data")
    
    cmd = "smbmount //%s/%s %s -o %s" % (self.options.ip_address, self.options.share,\
              self.mountpoint, ",".join(options))
    self._print_verbose(cmd)
    # We should perform error control at this point once the return values
    # are all spec'd-out
    (status, out) = commands.getstatusoutput(cmd)
    
    self._print_verbose("Command %s returns %s with output: %s" % (cmd, status, out))
    
    # Infer the filename of the destination target.
    self._infer_dest_filename()
  
  
  """ Unmounts a remote SMB share """
  def _smb_umount(self):
    self.add_event("Unmounting remote SMB share")
    cmd = "sudo umount -f %s" % self.mountpoint
    self._print_verbose(cmd)
    (status, out) = commands.getstatusoutput(cmd)
    self._print_verbose("Command %s returns %s with output: %s" % (cmd, status, out))
    os.rmdir(self.mountpoint)
  
  def _truncate_data(self):
    if (True == self.options.archive):
      struct = self.db_structure
      self._print_verbose("Truncating historical tables")
      
      for component in struct.keys():
        comp = struct[component]
        for table in comp.keys():
          if table in self.options.tables:
            if "data" == comp[table]['type']:
              print "Deleting data from %s" % table
              query = "TRUNCATE TABLE %s" % table
              
              try:
                res = db.delete(str(query))
              except Exception, e:
                print "Unable to clear table %s: %s" % (table, e)
        
  
  """ Make up a pseudo-unique filename for the archive based on the target FS """
  def _infer_dest_filename(self):
    filename = "netmon35_%d.db" % int(time.time())
    if (self.options.type == "smb"):
      self.dest_filename = "%s" % (os.path.join(self.mountpoint, filename))
    else:
      self.dest_filename = os.path.join(self.local_target, filename)
  
  """ Simply prints a string if the verbose CLI arg was specified """
  def _print_verbose(self, str):
    if (self.options.verbose):
      print str
  
  """ Fork the backup utility into the background """
  def detach(self):
    self._print_verbose("Daemonizing...")
    try:
      pid = os.fork()
    except OSError, e:
      return ((e.errno, e.strerror))
  
    if (pid == 0):
      os.setsid()
      # Ignore HUP signal.
      signal.signal(signal.SIGHUP, signal.SIG_IGN)
  
      try:
        # Fork into a new process
        pid = os.fork()
      except OSError, e:
        return ((e.errno, e.strerror))
  
      if (pid == 0):
        os.umask(0)
      else:
        # Destroy the old process
        os._exit(os.EX_OK)
    else:
      os._exit(os.EX_OK)
  
  

if __name__=='__main__':    
  # Prepare the CLI options-parser
  usage       = "Usage: %prog [--add-table=table,<>] [--type=] [target args]"
  version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2006 Netmon Inc."
  description = "Netmon backup management system."
  parser = OptionParser(usage=usage, version=version, description=description)
  
  # General Options
  parser.add_option("-v", action="store_true", dest="verbose", default=False, help="Enable extra output. Useful for debugging")
  parser.add_option("-r", "--archive", action="store_true", dest="archive", default=False, help="Archive mode (delete data after it is backed up")
  
  # Backup selection options
  parser.add_option("-a", "--add-table", action="append", type="string", dest="tables", help="Adds a table to the backup list")
  parser.add_option("-t", "--type", action="store", type="string", dest="type", help="Target type")
  parser.add_option("--from", action="store", type="int", dest="from", default="-1", help="Base timestamp for the data to back up")
  parser.add_option("--to", action="store", type="int", dest="to", default="-1", help="End timestamp for the data to back up")
  parser.add_option("--owner", action="store", type="string", dest="owner", help="Name of the person who initiated this backup", default=pwd.getpwuid(os.getuid())[0])
  parser.add_option("--notify", action="store", type="string", dest="notify", help="Email address to notify upon completion.", default="")
  
  # Options for SMB targets
  parser.add_option("--ip_address", action="store", type="string", dest="ip_address", help="IP Address of the remote target")
  parser.add_option("--share", action="store", type="string", dest="share", help="Name of the remote partition")
  parser.add_option("--username", action="store", type="string", dest="username", help="Username for authentication to the remote target")
  parser.add_option("--password", action="store", type="string", dest="password", help="Password for authentication to the remote target")
  
  
  # Options for LOCAL target
  parser.add_option("--label", action="store", type="string", dest="label", help="Label for the local backup file.")
  
  
  # Options for NFS targets
  
  # Options for FTP targets
  
  (options, args) = parser.parse_args()
  
  BackupManager = Backup_Manager(options, parser)
  #(BackupManager.options, args) = parser.parse_args()
  if (len(args) > 0):
    print "The following arguments have been discarded:\n%s" % ", ".join(args)
  
  BackupManager.init_backup()