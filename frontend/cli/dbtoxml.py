#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)
	
	
class Sequence:
	def __init__(self, name, field):
		self.name=name
		self.field=field

class Column:
	def __init__(self, name, ctype, comment, desc):
		self.name=name
		self.ctype=ctype
		self.comment=comment
		if desc==None:
			self.desc=''
		else:
			self.desc=desc

class Table:
	def __init__(self, name, ttype, label, index, desc):
		self.name=name
		self.ttype=ttype
		self.label=label
		self.index=index
		self.pkey=''
		self.size=-1
		self.desc=desc
		
	def setPrimaryKey(self, pkey):
		#print pkey+"\n"
		self.pkey=pkey
	
	def hasPrimaryKey(self):
		if self.pkey=='':
			return 0
		else:
			return 1
		
	def setSequences(self, s):
		self.s=s
	
	def getSequences(self):
		return self.s

	def setColumns(self, c):
		self.cols=c

	def getColumns(self):
		return self.cols

	def compare(t1, t2):
		return cmp(t1.name, t2.name)

class DataModel:
	def __init__(self, database, sizes, columns=0):
		self.sizes=sizes
		self.columns=columns
		# Connect to the DB
		try:
			self.db = madnet_db("user=root dbname=%s host=127.0.0.1" % database)
		except Exception, e:
			print "Unable to connect to database %s: %s" % (database, e)
			sys.exit(1)
			
	def getTables(self, component):
		rs = self.db.select ("SELECT relname, description FROM pg_class, pg_description d where d.objoid=pg_class.oid and relkind='r' and objsubid=0 and description like '%%component="+component+"%%'");
		t = [];
		for r in rs:
			#extract the vars from the comment string
			ttype=""
			label=""
			index=""
			varpairs=r['description'].split("&")
			for pair in varpairs:
				if pair.split("=")[0]=="type":
					ttype=pair.split("=")[1]
				elif pair.split("=")[0]=="label":
					label=pair.split("=")[1]
				elif pair.split("=")[0]=="index":
					index=pair.split("=")[1]
				elif pair.split("=")[0]=="desc":
					comment=pair.split("=")[1]
			#create table object
			tab = Table(r['relname'], ttype, label, index, comment)
			
			#get primary key
			p = self.db.select("select conname from pg_constraint, pg_class where pg_class.relname='"+tab.name+"' and pg_class.oid=pg_constraint.conrelid and contype='p'")
			for key in p:
				tab.setPrimaryKey(key['conname'])
			
			#get sequences
			s = self.db.select("select pc1.relname as seq, pc2.relname as table, c.attname as field from pg_depend, pg_class pc1, pg_class pc2, pg_attribute c where pc1.oid=pg_depend.objid and pc2.oid=pg_depend.refobjid and c.attnum=pg_depend.refobjsubid and c.attrelid=pc2.oid and pc2.relname='"+tab.name+"' and pc1.relkind='S' and pc1.relname not like 'pg_toast%%'")
			seqs=[]
			for seq in s:
				seqs.append(Sequence(seq['seq'], seq['field']));
			tab.setSequences(seqs);
			
			
			#get table size
			if self.sizes==1:
				s=self.db.select("SELECT tuple_len FROM pgstattuple('"+tab.name+"')")
				for siz in s:
					tab.size=long(siz['tuple_len'])
			
			#get columns
			if self.columns==1:
				s=self.db.select("select attname, typname, attnotnull, description from pg_attribute join pg_class on attrelid=pg_class.oid join pg_type on pg_type.oid=atttypid left outer join pg_description on (pg_description.objoid=pg_class.oid and pg_description.objsubid=pg_attribute.attnum) where relname='%s'" % (tab.name))
				cols=[]
				for c in s:
					if c['attname'] not in ['oid', 'ctid', 'xmin', 'xmax','cmin', 'cmax', 'tableoid']:
						cols.append(Column(c['attname'], c['typname'], c['attnotnull'], c['description']))
				tab.setColumns(cols)
			
			#add the table
			t.append(tab)
		t.sort(Table.compare)
		return t
		
	def getDatabaseName(self):
		return "netmon35"
	
	def getComponents(self):
		c=[]
		rs = self.db.select("SELECT description FROM pg_description, pg_class WHERE objoid=pg_class.oid and relkind='r' and objsubid=0");
		for r in rs:
			pairs=r["description"].split("&")
			for p in pairs:
				if p.split("=")[0]=="component":
					if len(p.split("="))>1 and p.split("=")[1] not in c:
						c.append(p.split("=")[1]);
		c.sort()
		return c
	
		

class XMLDatabase:
	def getXML(self, model):
		out='<?xml version="1.0" encoding="UTF-8" ?>\n'
		databasename=model.getDatabaseName()
		out+='<database name="' + databasename + '">\n'
		components=model.getComponents()
		for c in components:
			out+='\t<component name="'+c+'">\n'
			self.tables=model.getTables(c)
			for t in self.tables:
				if model.sizes==1 :
					out+='\t\t<table name="'+t.name+'" size="%i" type="' % t.size +t.ttype+'" label="'+t.label+'" index="'+t.index+'">\n'
				else :
					out+='\t\t<table name="'+t.name+'" type="' +t.ttype+'" label="'+t.label+'" index="'+t.index+'">\n'
				out+='\t\t\t<desc>\n'
				out+='<![CDATA['+t.desc+']]>'
				out+='\t\t\t</desc>\n'
				if t.hasPrimaryKey()==1:
					out+='\t\t\t<pkey field="'+t.pkey+'" />\n'
				for s in t.getSequences():
					out+='\t\t\t<sequence name="'+s.name+'" field="'+s.field+'" />\n'
				
				if model.columns==1:
					for c in t.getColumns():
						out+='\t\t\t<column name="'+c.name+'" type="'+c.ctype+'">\n'
                                		out+='\t\t\t\t<desc>\n'
                                		out+='\t\t\t\t<![CDATA['+c.desc+']]>\n'
                                		out+='\t\t\t\t</desc>\n'
						out+='\t\t\t</column>\n'
				out+='\t\t</table>\n'
			out+='\t</component>\n'
		out+="</database>\n"
		return out
		

if __name__ == '__main__':
	e =XMLDatabase()
	sizes=0
	columns=0
	if len(sys.argv)==2 and sys.argv[1]=='-s':
		sizes =1
	elif len(sys.argv)==2 and sys.argv[1]=='-c':
		columns=1
	elif len(sys.argv)==3 and (sys.argv[1]=='-c' or sys.argv[1]=='-s') and (sys.argv[2]=='-c' or sys.argv[2]=='-s'):
		sizes=1
		columns=1
	
	print e.getXML(DataModel("netmon35", sizes, columns))
