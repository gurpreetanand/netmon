#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
import os
import signal

############################################################################
### LOCKING MECHANISM
LOCKFILE = "/var/run/hourly_cleanup.lock"
TIMEOUT  = 3600*8

mark = int(time.time())
if (os.path.isfile(LOCKFILE)):
	if ((mark - os.path.getmtime(LOCKFILE)) >= TIMEOUT):
		print "Clearing stale lockfile"
		os.unlink(LOCKFILE)
	else:
		print "Another instance of this script is already running. Aborting"
		sys.exit(0)

fp = open(LOCKFILE, "w")
fp.close()

def signal_handler(sig, frame):
	os.unlink(LOCKFILE)
	sys.exit(1)
	
signal.signal(signal.SIGINT, signal_handler)
############################################################################




# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	signal_handler(0,0)
	
# Connect to the database
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1")
	db.set_autocommit(0)
except Exception, e:
	print "ERROR: %s" % e
	signal_handler(0,0)
	
query = """
DELETE FROM protocol_breakdown 
WHERE start_time < ((date_part('epoch'::text, now()))::integer - (3600 * 24 * 31 * 2))
"""

try:
	db.delete(query)
except Exception, e:
	pass

#query = "VACUUM FULL ANALYZE protocol_breakdown"

try:
	db.insert(query)
except Exception, e:
	pass

db.close()

print "Hourly cleanup completed"

signal_handler(0,0)
