#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Netmon Event Scheduling and displatching system 
# Created:   18/04/07 02:40:10 PM
# SVN:       $Id$

# Imports
import os
import sys
import time
import signal
import smtplib
import os.path
import plugins
import madnet_db
# Implement threading support
import threading

# Mime email handling stuff.
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders

# CLI Arguments Parser
from optparse import OptionParser

# Allow the import of helper classes and madnet DB access layer
sys.path.append("/apache/cli")

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)
	
# Connect to the database
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
	db.set_autocommit(0)
except Exception, e:
	print "ERROR: %s" % e
	sys.exit(1)
	
"""
# Execution Exception
class ThreadingError(Exception):
	def __init__(self, value, thread_id):
		self.value = value
	
	def __str__(self):
		return repr(self.value)
"""


class EventScheduler:
	"""Netmon Event Scheduling and displatching system """
	
	# Events scheduled for this run
	_events_stack  = {}
	# Active, working threads
	_threads_stack = {}
	# Current PID
	_pid = os.getpid()
	# Stack of plugins
	_plugins = {}
	# SMTP Parameters
	_smtp = {}

	def __init__(self, options, parser):
		""" Constructor """
		
		try:
			# Initialize parser variables
			self.options = options
			self.parser  = parser
			
			# Retrieve SMTP server settings
			self.get_smtp_params()
			
			# Initialize the signal handler
			self.print_verbose("Installing signal handler")
			signal.signal(signal.SIGINT, self.sigint_handler)
			
			# Check for other running schedulers
			self.resolve_pid_conflicts()
			
			# Fill up the plugins stack
			self.print_verbose("Registering plugins")
			self._plugins = plugins.getPlugins()
			
			# Start scheduling events
			self.print_verbose("Initializing polling mechanism")
			self.poll()
			
		# Barbaric error-handling
		except Exception, e:
			self.print_verbose("Handling exception: %s" % e)
			self.cleanup()
			sys.exit(os.EX_SOFTWARE)
			
		# Clean up and exit
		self.cleanup()
		sys.exit(os.EX_OK)
		
	def get_smtp_params(self):
		# Retrieve the SMTP server and source address
		# from the Netmon settings database.
		#print "Retrieving SMTP settings"
		query = "SELECT var, value FROM daemonsconfig WHERE daemon_id = (SELECT id FROM daemons WHERE name = 'emailmond')"
		
		try:
			res = db.select(query)
		except Exception, e:
			# In case of error, we will just use the default values.
			pass
		
		for row in res:
			self._smtp[row['var']] = row['value']
		
	def sigint_handler(self, sig, frame):
		self.poll()

	def print_verbose(self, str, source = "Parent Thread"):
		if (self.options.verbose):
			print "[VERBOSE] - [%s/%s] - %s" % (source, threading.currentThread().getName(), str)

	def resolve_pid_conflicts(self):
		""" This method prevents the scheduler from running twice in different processes.
		It first checks the PID file to see if it already exists. If it does, it extracts
		the PID from that file and sends a SIG_INT to that PID and then exits.
		If it does not exist, it creates it and enters the current PID in the file."""
		if os.path.isfile(self.options.pid_file):
			self.print_verbose("PID file already in use.")
			# Extract the pid and send it a SIG_INT
			pid = open(self.options.pid_file, 'r').readline().strip()
			self.print_verbose("Sending SIG_INT to process %s" % pid)
			try:
				os.kill(int(pid), 2)
			except Exception, e:
				self.print_verbose("Exception - %s" % e)
				self.cleanup()
				self.print_verbose("Attempting to recover")
				self.resolve_pid_conflicts()
			else:# Then exit
				self.print_verbose("Exiting")
				sys.exit(os.EX_OK)
		else:
			self.print_verbose("Locking PID file")
			# Lock the PID file for future instances
			open(self.options.pid_file, 'w').write("%s" % self._pid)
		
	def poll(self):
		""" This method polls the database for new events that need that are scheduled
		to be triggered at the current time. It obtains that list through the database,
		and then adds each event to the events stack if not already present in it. """
		
		query = """
		SELECT schedule_start_date, schedule_stop_date, id, notify_users, event_type
		FROM events
		WHERE (
		  (recurrence_unit = 'day')
		  OR ((recurrence_unit = 'week') AND (schedule_week = extract('dow' FROM now())))
		  OR ((recurrence_unit = 'month') AND (schedule_day = extract('day' FROM now())))
		  OR ((recurrence_unit = 'year') AND (schedule_month = extract('month' FROM now())) AND (schedule_day = extract('day' FROM now())))
		)
		AND schedule_hour = extract('hour' FROM now())
		ORDER BY id ASC
		"""
		
		try:
			res = db.select(query)
		except Exception, e:
			print "An error has occured while retrieving a list of scheduled events:\n%s" % e
			self.cleanup()
			sys.exit(os.EX_DATAERR)
			
			
		if len(res) == 0:
			self.print_verbose("No events scheduled for this itteration")
			
			
		for row in res:
			if row['id'] not in self._events_stack.keys():
				if self.dateRangeIsCurrent(row['schedule_start_date'], row['schedule_stop_date']):
					self._events_stack[row['id']] = row
					self.trigger_event(row['id'])
	
	def dateRangeIsCurrent(self, start, stop):
		validRange = True
		now = int(time.time())
		
		if (start == None) or (start == ''):
			start = -1
			
		if (stop == None) or (stop == ''):
			stop = now + 1
			
		if (now <= stop) and (now >= start):
			return True
		else:
			return False
			
	
	def trigger_event(self, event_id):
		""" This method determines the appropriate plugin for an event and creates a new
		thread for this plugin to handle the event."""
		
		event = self._events_stack[event_id]
		
		self.print_verbose("Locating and instanciating appropriate plugin")
		try:
			plugin = self._plugins[event['event_type']]
			exec "objInstance = plugins.%s.Plugin()\n" % plugin
			objInstance.print_verbose = self.print_verbose
		except Exception, e:
			print "Unable to instanciate the required plugin %s. Aborting" % plugin
			self.cleanup()
			sys.exit(os.EX_SOFTWARE)
			
		self.print_verbose("Dispatching event through plugin")
		objInstance.setEvent(event_id)
		self._threads_stack[event_id] = objInstance
		objInstance.start()
		self.print_verbose("Event has been sent to the plugin")
		
	
	def cleanup(self):
		# Wait for all threads to finish their job, and join() them
		for key, val in self._threads_stack.items():
			if val.isAlive() != True:
				self.dispatchNotification(val.getEvent(), val.getNotificationMessage())
				val.join()
				del self._threads_stack[key]
				
		if len(self._threads_stack) == 0:
			# Clean up the PID file before exiting.
			self.print_verbose("All threads have terminated. Cleaning up PID file")
			if os.path.isfile(self.options.pid_file):
				os.unlink(self.options.pid_file)
		else:
			self.print_verbose("Some threads are still active. Checking again in 5sec")
			t = threading.Timer(5.0, self.cleanup)
			t.start()
			
	def dispatchNotification(self, event_id, message):
		user_ids = self._events_stack[event_id]['notify_users']
		self.print_verbose("Dispatching notification for event #%s" % event_id)
		
		if '' == user_ids[0]:
			return
		
		try:
			query = "SELECT email FROM users WHERE id IN (%s) GROUP BY id, email ORDER BY email ASC" % ",".join(["%s" % x for x in user_ids])
			res = db.select(query)
		except Exception, e:
			print "Unable to retrieve notification email addresses. - %s" % e
			return
		
		for row in res:
			try:
				self.notify_user(row['email'], message)
			except Exception, e:
				print "Unable to send e-mail"
			
	def notify_user(self, email, message):
		# Create the multipart container and initialize it
		msg = MIMEMultipart()
		msg['Subject'] = "[REPORT] Netmon Report Available"
		msg['From'] = self._smtp['smtpaccount']
		msg['To'] = email
		msg.preamble = "A new Netmon report has completed."
		msg.epilogue = ""
		
		# Add the notification message
		body = MIMEText(message)
		body.add_header('Content-Disposition', 'inline')
		msg.attach(body)
		
		# Send the email using the local SMTP server
		self.print_verbose("Sending email to %s" % email)
		sc = smtplib.SMTP(self._smtp['smtpserver'])
		sc.sendmail(self._smtp['smtpaccount'], email, msg.as_string())
		sc.quit()


if __name__=='__main__':
	# Jump to the script's root
	# 20181217 - Fixed Error of No file or directory '' - Gurpreet Anand
  	os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

	usage       = "Usage: %prog [-v --verbose]"
	version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
	description = "Netmon Event Scheduling and displatching system "
	parser = OptionParser(usage=usage, version=version, description=description)
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Enable extra output")
	parser.add_option("-p", "--pid", action="store", dest="pid_file", default="/var/run/netmon_scheduler.pid", help="PID File")

	(options, args) = parser.parse_args()
	obj = EventScheduler(options, parser)
