#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Alert Maintenance plugin for the Netmon Event Scheduler
# Created:   09/11/2009
# SVN:       $Id$

# Initial imports
import os
import sys
import time
import math
import threading


# Allow the import of helper classes and madnet DB access layer
sys.path.append("/apache/cli")
sys.path.append("/apache/cli/reporting")

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)
	


class Plugin(threading.Thread):
	""" Netmon scheduling system plugin """
	# Event-type entry in the DB table
	event_type = 'maintenance'
	# Event ID
	event_id = None
	# Alert maintenance data
	_maintenance = None
	# Report generation time
	_time = time.time()
	
	def __init__(self):
		threading.Thread.__init__(self)
	
	def setEvent(self, event_id):
		''' Dispatch the specified event '''
		self.event_id = event_id
		self.print_verbose("Thread %s is configured" % event_id, event_id)
	
	def getEvent(self):
		''' Returns the current event ID '''
		return self.event_id
	
	def run(self):
		
		# Connect to the database
		self.print_verbose("Initializing DB connection.", self.event_id)
		try:
			self.db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
			self.db.set_autocommit(0)
		except Exception, e:
			print "ERROR: %s" % e
			sys.exit(os.EX_SOFTWARE)
		
		''' Main method called on thread start() '''
		self.print_verbose("Retrieving alert maintenance data associated with event_id %d" % int(self.event_id), self.event_id)
		query = self.db.pack("""
		SELECT a.*, b.condition 
		FROM alert_maintenance a, 
		extract_alert((select trigger_id from alert_maintenance where event_id = %s)) b
		WHERE a.event_id = %s
		""", [int(self.event_id), int(self.event_id)])
		
		try:
			res = self.db.getRow(query)
			self.print_verbose(res, self.event_id)
		except Exception, e:
			print "[ERROR] - [%s/%s] - Unable to retrieve specified event from the database: %s" % (self.event_id, threading.currentThread(), e)
			sys.exit(os.EX_SOFTWARE)
		self._maintenance = res
		self.print_verbose("Alert Description: %s" % self._maintenance['condition'])
		#self._maintenance['pretty_type'] = " ".join([x.capitalize() for x in self._report['report_type'].split("_")])
		self.launch_maintenance()
		
	def launch_maintenance(self):
		query = "UPDATE alert_triggers SET active = 'f' WHERE trigger_id = %d" % (self._maintenance['trigger_id'])
		
		try:
			res = self.db.update(query)
		except Exception, e:
			print "[ERROR] - [%s/%s] - Unable to pause specified alert: %s]" % (self.event_id, threading.currentThread(), e)
			sys.exit(os.EX_SOFTWARE)
			
		""" 
		Wait until it's time to resume the alert. 
		Note that if the system is rebooted now, we have an alert that will be paused at boot-time.
		In order to prevent this issue from taking place, we must resume all alerts at boot-time.
		"""
		self.print_verbose("Sleeping for %d hours" % (self._maintenance['maintenance_duration']))
		time.sleep(3600*int(self._maintenance['maintenance_duration']))
		
		query = "UPDATE alert_triggers SET active = 't' WHERE trigger_id = %d" % (self._maintenance['trigger_id'])
		try:
			res = self.db.update(query)
		except Exception, e:
			print "[ERROR] - [%s/%s] - Unable to resume specified alert: %s]" % (self.event_id, threading.currentThread(), e)
			sys.exit(os.EX_SOFTWARE)
			
		self._time = time.time() - self._time
		


	
	def getNotificationMessage(self):
		''' Returns the message to dispatch for user notification '''
		self.print_verbose("Alert Resumed. Data structure looks like this: %s" % dir(self._maintenance), self.event_id)
		try:
			return """
			The following alert has been resumed, after being paused for %s:
			Alert Description: %s
			""" % (self._maintenance['condition'], reportHelper.format_time(math.ceil(self._time)))
		except Exception, e:
			print "[ERROR]: [%s/%s] - %s" % (self.event_id, threading.currentThread(), e)
			print self._maintenance
			sys.exit(os.EX_SOFTWARE)
