#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Netmon Event Scheduling Plugins structure constructor
# Created:   20/04/07
# SVN:       $Id$

import os
from threading import Thread


def getPlugins():
	""" Returns a dictionary of instances to plugin objects.
	The dict keys are the 'event_type' attribute of each instance """
	
	# Get a list of plugins in the current path
	path = (os.path.dirname(os.path.realpath(__file__)))
	_plugins = [x for x in os.listdir(path) if x[-3:] == '.py']
	
	
	stack = {}
	for plugin in _plugins:
		# Skip the module constructor
		if '__init__.py' != plugin:
			# Remove '.py' extension
			plugin = plugin[:-3]
			try:
				exec "import %s" % plugin
				exec "pluginInstance = %s.Plugin()" % plugin
				exec "stack['%s'] = '%s'\n" % (pluginInstance.event_type, plugin)
				del(pluginInstance)
			except Exception, e:
				print "Error loading plugin %s: %s" % (plugin, e)
				pass
		
	return stack