#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Report Manager plugin for the Netmon Event Scheduler
# Created:   20/04/2007
# SVN:       $Id$

REPORTS_PATH='/var/reports/'


import re
import os
import sys
import time
import math
import threading
import commands

# We use minidom to parse CLI options for the report
from xml.dom import minidom


# Allow the import of helper classes and madnet DB access layer
sys.path.append("/apache/cli")
sys.path.append("/apache/cli/reporting")
import reportHelper

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)
	


class Plugin(threading.Thread):
	""" Netmon scheduling system plugin """
	# Event-type entry in the DB table
	event_type = 'report'
	# Event ID
	event_id = None
	# Report data
	_report = None
	# Report generation time
	_time = time.time()
	
	def __init__(self):
		threading.Thread.__init__(self)
	
	def setEvent(self, event_id):
		''' Dispatch the specified event '''
		self.event_id = event_id
		self.print_verbose("Thread %s is configured" % event_id, event_id)
	
	def getEvent(self):
		''' Returns the current event ID '''
		return self.event_id
	
	def run(self):
		# Connect to the database
		self.print_verbose("Initializing DB connection.", self.event_id)
		try:
			self.db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
			self.db.set_autocommit(0)
		except Exception, e:
			print "ERROR: %s" % e
			sys.exit(os.EX_SOFTWARE)
		
		''' Main method called on thread start() '''
		self.print_verbose("Retrieving report associated with event_id %d" % int(self.event_id), self.event_id)
		query = "SELECT * FROM reports WHERE event_id = %s" % int(self.event_id)
		try:
			res = self.db.getRow(query)
			self.print_verbose(res, self.event_id)
		except Exception, e:
			print "[ERROR] - [%s/%s] - Unable to retrieve specified event from the database: %s" % (self.event_id, threading.currentThread(), e)
			sys.exit(os.EX_SOFTWARE)
		self._report = res
		self._report['pretty_type'] = " ".join([x.capitalize() for x in self._report['report_type'].split("_")])
		self.launch_report()
		
	def launch_report(self):
		# Build the list of CLI options
		options = {}
		wanted = ['report_period', 'start_date', 'end_date', 'start_time', 'end_time', 'format']
		for field in wanted:
			if None != self._report[field]:
				options["--%s" % field] = self._report[field]
			
		#row_options = self._report['options']
		try:
			options_dom = minidom.parseString(self._report['options']).firstChild.getElementsByTagName('option')
			for itm in options_dom:
				options[itm.getAttribute('key')] = itm.getAttribute('val')
		except Exception, e:
			self.print_verbose("Unable to parse options XML list. Aborting options parsing for this event\n%s" % e, self.event_id)
			pass
		
		# Create a unique filename
		self._report['filename'] = options['--save-as'] = "%s%s_%s%s" % (REPORTS_PATH, self._report['report_type'], self.event_id, self.label_counter())		
		# Build and run the command
		cmd = "python /apache/cli/reporting/%s/report.py %s" % (self._report['report_type'], 
																" ".join(["%s \"%s\"" % (str(key).strip(), str(val).strip()) for key,val in options.items()]))

		# Create the VFS entry with the appropriate label
		query = self.db.pack("""INSERT INTO fs_files(directory_id, label, description, filename, busy)
		VALUES((SELECT id FROM fs_directories WHERE real_path = '/var/reports/'), %s, 
		%s, %s, 't')""", [self._report['label'], "%s Report [Scheduled]" % self._report['pretty_type'],
						 options['--save-as'].replace(REPORTS_PATH, '')])

		try:
			self.db.insert(query)
		except Exception, e:
			print "Unable to create VFS entry: %s" % e
			pass
		
		self.print_verbose("Executing command: %s" % cmd, self.event_id)
		out = commands.getoutput(cmd)
		
		self._time = time.time() - self._time
		
		query = self.db.pack("UPDATE fs_files SET busy = 'f' WHERE filename = %s", [options['--save-as'].replace(REPORTS_PATH, '')])
		try:
			self.db.update(query)
		except Exception, e:
			print "Error: Unable to update VFS entry: %s" % e
			pass
		
	def label_counter(self):
		""" Returns a suffix for the report filename to use. The suffix is in the form
		of .X.html where X is a number above the last X found while browsing the target
		directory. Basically, you will end up with report.1.html, report.2.html, etc... """
		files = os.listdir(REPORTS_PATH)
		name = "%s_%s" % (self._report['report_type'], self.event_id)
		
		if "%s.1.html" % name not in files:
			return '.1.html'
		
		regex = re.compile("%s\.([0-9]+)\.html" % name)
		maxVal = 1
		
		for file in files:
			match = regex.match(file)
			if None != match:
				idx = int(match.group(1))
				maxVal = max(maxVal, idx)
		maxVal += 1
		return ".%d.html" % maxVal
	
	def getNotificationMessage(self):
		''' Returns the message to dispatch for user notification '''
		self.print_verbose("Report completed. Data structure looks like this: %s" % dir(self._report), self.event_id)
		try:
			return """%s report has completed successfully.
	It should now be available under your Netmon system's 'Files' section.
	
	Report Details:
	===============
	Label: %s
	Filename: %s		
	Generated in %s
			""" % (self._report['pretty_type'], self._report['label'], self._report['filename'], reportHelper.format_time(math.ceil(self._time)))
		except Exception, e:
			self.print_verbose("[ERROR]: [%s/%s] - %s" % (self.event_id, threading.currentThread(), e))
