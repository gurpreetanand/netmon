#!/usr/bin/env python
# -*- coding: utf-8 -*-

import smtplib
import os
import re
import sys
import tarfile
import os.path
import mimetypes
import commands
import string

# CLI Arguments Parser
from optparse import OptionParser




# Mime email handling stuff.
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders




# CLI prompt with support for default values and mandatory prompts.
def prompt(prompt, default = None, mandatory = False):
	oprompt = prompt
	odef    = default

	if (None != default):
		default = "[%s]" % default
		prompt = "%s %s" % (prompt, default)

	prompt = "\x1b[" + "33;01m" + "%s : " % prompt + "\x1b[" + "39;49;00m"
	res = raw_input(prompt)

	if (("" == res) and (None != default)):
		return odef
	elif (("" == res) and (None == default) and (True == mandatory)):
		return prompt(oprompt, odef)
	return res


class Log_Snapshot:
	
	# Properties initialization
	snapshot    = "/var/netmon_snapshot.tgz"
	logdir      = "/var/log/"
	to_addr     = ["doug@netmon.ca"]
	smtp        = {"smtpaccount": "no-reply@netmon.ca", "smtpserver": "127.0.0.1"}
	
	def __init__(self, options, parser):
		self.options = options
		self.parser  = parser
		
		# Remove previous snapshot container.
		if os.path.exists(self.snapshot):
			os.unlink(self.snapshot)

	
		
	"""
	There doesn't appear to be a high-level mime email
	handling module in the core python distribution so
	we need to work with the components of the 'email'
	module.
	
	This method creates a multipart mime email container
	and attaches the tarball created in the build_snapshot
	method. It then calls the build_system_summary
	method and captures the output to attach it as a text
	payload, which email clients will use as actual
	content of the email.
	
	"""
	def send_snapshot(self):

		
		# Create the multipart container and initialize it
		msg = MIMEMultipart()
		msg['Subject'] = "[SNAP] Netmon SE Test Snapshot"
		msg['From'] = self.smtp['smtpaccount']
		msg['To'] = ", ".join(self.to_addr)
		msg.epilogue = ""
		
		
		# Add the summary
		summary = MIMEText(self.build_system_summary())
		summary.add_header('Content-Disposition', 'inline')
		msg.attach(summary)
		
		# Send the email using the local SMTP server
		print "Sending the snapshot"
		sc = smtplib.SMTP(self.smtp['smtpserver'])
		sc.sendmail(self.smtp['smtpaccount'], self.to_addr, msg.as_string())
		sc.quit()
		
	# Creates a summary of the system status
	def build_system_summary(self):
		summary = ""
		
		
		# get info interactively
		summary = summary + prompt("Please type your company name then press ENTER")
		summary = summary + "\n" + prompt("Please briefly describe your hardware (at minimum manufacturer, and model #) then press ENTER") + "\n"
		
		print "\x1b[" + "33;01m" + "Please enter any feedback you have on the installation process.  Press ENTER on a blank line to continue"  + "\x1b[" + "39;49;00m"
		while(1):
			line = prompt("")
			if ("" == line):
				break
			summary = summary + line
		
		# Add the output of all the commands in the list to the summary
		my_commands = ['ifconfig -v', 'route -vneF', 'w', 'uname -a', \
					   'df -hTP', 'ip neigh show', 'ipcs -u', 'ps auxwwwf', \
					   'free -kt', 'mount', 'lspci -n', 'last -adx', 'lastb -adx',\
					   'netstat -tulnp', 'crontab -l', \
					   'tail -n80 /var/log/syslog', 'dmesg | tail -n10']
		
		for command in my_commands:
			summary = summary + "\n\n%s" % self.get_command(command)
			summary = summary + "\n\n" + ("*" * 45) + "\n"
		
		my_files = ['/etc/network/interfaces',\
			    '/etc/cron.d/postgresql', '/root/.bash_history']
		
		for file in my_files:
			summary = summary + "\n\n%s" % self.get_file(file)
			summary = summary + "\n\n[" + ("=" * 45) + "]\n"
			    
		return summary
	
	# Simple pretty wrapper for commands.getoutput()
	def get_command(self, command):
		print "Running %s" % command
		return "Output of command \"%s\":\n%s" % (command, commands.getoutput(command))
		
	def get_file(self, file):
		print "Examining file %s" % file
		try:
			return "Contents of file %s:\n\n%s" % (file, "".join(open(file).readlines()))
		except Exception, e:
			print "Warning: Unable to retrieve file %s: %s" % (file, e)
			pass

if __name__=='__main__':
	# Jump to the script's root
	# 20181217 - Fixed Error of No file or directory '' - Gurpreet Anand
  	os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

	usage       = "Usage: %prog [-v --verbose]"
	version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
	description = "Netmon Automated Snapshot System"
	parser = OptionParser(usage=usage, version=version, description=description)
	
	
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Enable extra output")
	parser.add_option("-c", "--crash", action="store_true", dest="crash", default=False, help="Attach daemons core dumps (system crash)")


	(options, args) = parser.parse_args()
	obj = Log_Snapshot(options, parser)
	obj.send_snapshot()
