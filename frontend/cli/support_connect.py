#!/usr/bin/env python

import os
import socket
import sys
import pexpect
import time
import urllib
import daemon
import lockfile
import pwd
import syslog #removeme
from optparse import OptionParser

#Connects to Netmon's ssh tunnel server


usage       = "Usage: %prog [-p pathname][-v]"
version     = "%prog (Netmon Inc.) v2.0\nCopyright (c) 2009 Netmon Inc."
description = "Netmon SSH tunnel script."
parser = OptionParser(usage=usage, version=version, description=description)
  
parser.add_option("-p", "--password", action="store", default="", type="string", dest="pwd", help="SSH password for tunnel connections.")
parser.add_option("-n", action="store", dest="tunnel", default=0, help="Tunnel number.")
parser.add_option("-j", action="store_true", dest="json", default=False, help="JSON Output")
parser.add_option("-d", action="store_true", dest="debug", default=False, help="Debug mode")

connections = {}
  
options, args = parser.parse_args()
try:
  username = pwd.getpwuid(os.getuid())[0]
  homedir = os.path.expanduser("~"+username+"/")
  pidfile = "/tmp/support_connect.py.pid"
  if (options.debug == True):
    print "Home directory: %s" % (homedir)
    print "PID file: %s" % (pidfile)
  os.system("rm " + homedir + "/.ssh/known_hosts")
except OSError, e:
  if (e.errno == 2):
    pass # ignore file-not-found

# if no tunnel specified, get a list of available tunnels from the ssh server
if (options.tunnel) > 0 :
  tunnel = int(options.tunnel)
else:

  f = urllib.urlopen("http://demo.netmon.ca/tunnels.php")

  s = f.read()
  f.close()
  tunnel = int(s.split(',')[0])

if (tunnel < 1):
  if (options.json == True):
    print '{"error": "No tunnels available"}'
  else:
    print "No tunnels available, please contact support."

  sys.exit(0)
else:
  if (options.json == False or options.debug == True):
    print "Tunnel #%d is available." %(tunnel)


pwd = options.pwd

if len(pwd) < 1:
  pwd = raw_input("please type the password:")

if (options.debug == True):
  print "Using password: %s" % (pwd)

ports = [(6670+tunnel),(6610+tunnel)]
local_ports = [80,22]

if (options.json == False or options.debug):
  print "Connecting to Netmon Support Server.."

for index in range (0,len(ports)):
  cmd = "ssh  -nNT -f -R %d:127.0.0.1:%d support@demo.netmon.ca" %(ports[index],local_ports[index])
  if (options.debug == True):
    print("Running command: %s" % (cmd))
    print "Index: %s" % (index)

  connections[local_ports[index]] = pexpect.spawn(cmd)
  if index < 1:
    connections[local_ports[index]].expect(".*(yes/no).*")
    time.sleep(0.1)
    connections[local_ports[index]].sendline("yes")
    if (options.debug == True):
      print "Sent line 'yes'"
  connections[local_ports[index]].expect(".*ssword:")
  connections[local_ports[index]].sendline(pwd)
  if (options.debug == True):
    print "Sent password: %s" % (pwd)

if (options.json == True):
  json = "{\"http\": %s, \"ssh\": %s}" % (6670+tunnel, 6610+tunnel)
  print json
  # Flush so the daemon context doesn't break everything.
  sys.stdout.flush()
else:
  print "Connected on tunnel # %d" % (tunnel)
  print "The tunnels are now connected and Netmon support staff will"
  print "have access to your Netmon device.  Leave this window "
  print "open until Netmon Support instructs you to close it."
  print ""
  print "To close the tunnels now, type 'exit'."

context = daemon.DaemonContext(
  pidfile = lockfile.FileLock(pidfile)
)

with context:
  while True:
    time.sleep(5)