#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Author:   Xavier Spriet <xavier@netmon.ca>
# Purpose: Netmon CD Update system
# Created: 08/05/06


import os
import sys

print "Mounting CD-Rom"
os.system("mkdir /mnt/cdrom")
retval = os.WEXITSTATUS(os.system("mount -t iso9660 -o ro /dev/cdrom /mnt/cdrom"))

if (retval > 0):
	print "Unable to mount CD-Rom"
	os.system("eject /dev/cdrom")
	sys.exit(-1)

# Change the active directory
try:
	os.chdir("/mnt/cdrom")
except Exception, e:
	print e
	os.system("eject /dev/cdrom")
	sys.exit(1)

# Execute the upgrade script
if (os.access("/mnt/cdrom/upgrade.py", os.R_OK)):
	print "Running Upgrade Script from CD-Rom"
	os.system("python /mnt/cdrom/upgrade.py")
else:
	print "Unable to update from the CD media."
	os.system("eject /dev/cdrom")
	sys.exit(1)

# Change the active directory
try:
	os.chdir("/")
except Exception, e:
	print e
	os.system("eject /dev/cdrom")
	sys.exit(1)

os.system("eject /dev/cdrom")