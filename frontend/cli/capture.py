#!/usr/bin/env python
import time
import sys
import os
import time
import string
import os.path
import signal

# CLI Arguments Parser
from optparse import OptionParser


# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)


"""
TODO: Make capture generator create the fs_files entry if it doesn't already exist.
"""

# Gather the data we need from CL args
#packets, interface, out = sys.argv[1:4]


class Traffic_Capture:
	
	# TCPdump filter expression
	_filter = []
	
	
	def __init__(self, options, parser):
		self.options = options
		self.parser = parser
		
		self.check_options()
		self.marshall_filter()
		self.perform_capture()
		
	def check_options(self):
		mandatory = ['target', 'packets', 'interface']
		missing_options = False
		
		for arg in mandatory:
			if (None == getattr(self.options, arg)):
				print "Mandatory argument %s is missing" % arg
				missing_options = True
				
		if (True == missing_options):
			self.parser.print_help()
			sys.exit(1)
			
	def marshall_filter(self):
		if self.options.ip:
			self._filter.append("host %s" % self.options.ip)
		if self.options.port:
			self._filter.append("port %s" % self.options.port)				
		
	""" Fork the backup utility into the background """
	def detach(self):
		try:
			pid = os.fork()
		except OSError, e:
			return ((e.errno, e.strerror))
		
		if (pid == 0):
			os.setsid()
			# Ignore HUP signal.
			signal.signal(signal.SIGHUP, signal.SIG_IGN)
			
			try:
				# Fork into a new process
				pid = os.fork()
			except OSError, e:
				return ((e.errno, e.strerror))
			
			if (pid == 0):
				os.umask(0)
			else:
				# Destroy the old process
				os._exit(os.EX_OK)
		else:
			os._exit(os.EX_OK)
			
	def perform_capture(self):
		fp = open("/var/captures/%s" % self.options.target, "w")
		fp.close()
		print "Initializing new traffic capture at %s" % self.options.target
		self.detach()
		
		os.system("sudo /usr/sbin/tcpdump -XX -s 0 -i %s -c %s -w /var/captures/%s %s" % (self.options.interface,
																				   self.options.packets,
																				   self.options.target,
																				   " and ".join(self._filter)))
		
		# Connection object for NEW DB
		try:
			db = madnet_db("dbname=netmon35 host=127.0.0.1")
		except Exception, e:
			print "ERROR: %s" % e
			sys.exit(1)	
			
		time.sleep(2)
		
		# Make the file available in VFS
		query = "UPDATE fs_files SET busy = 'f' WHERE filename = '%s' AND busy = 't'" % self.options.target
		
		try:
			db.update(query)
		except Exception, e:
			print e
			os._exit(os.EX_DATAERR)
			
			
if (__name__ == "__main__"):
	# Prepare the CLI options-parser
	usage       = "Usage: %prog [--interface=eth<x>] [--packets=] [--target=]"
	version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2006 Netmon Inc."
	description = "Netmon traffic capture wrapper."
	parser = OptionParser(usage=usage, version=version, description=description)
	
	parser.add_option("-v", action="store_true", dest="verbose", default=False, help="Enable extra output. Useful for debugging")
	parser.add_option("-i", "--interface", action="store", type="string", dest="interface", help="Network Interface")
	parser.add_option("-t", "--target", action="store", type="string", dest="target", help="Where to store the capture")
	parser.add_option("-p", "--packets", action="store", type="string", dest="packets", help="Number of packets to capture")
	parser.add_option("--ip", action="store", type="string", dest="ip", help="IP Address to look for")
	parser.add_option("--port", action="store", type="string", dest="port", help="Port number to look for")
	
	(options, args) = parser.parse_args()
	
	capture = Traffic_Capture(options, parser)