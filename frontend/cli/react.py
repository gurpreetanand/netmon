#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Netmon VM Re-activation system 
# Created:   23/07/07 01:04:13 PM
# SVN:       $Id$

# Imports
import os
import sys
import os.path
import urllib
import re

# xml parser
from xml.dom import minidom

# CLI Arguments Parser
from optparse import OptionParser

# Netmon release DOM extractor
from update import Netmon_Updater

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)
	
class ReactivationError(Exception):
	"""Exception raised when reactivation process fails """
	def __init__(self, value):
		self.value = value
	
	def __str__(self):
		return repr(self.value)

class Reactor:
	"""Netmon VM Re-activation system """
	
	# This will contain the registration info extracted from the DB
	reg_dict = {}
	
	iface=''
	
	# We need the updater component to extract the version number
	updater = Netmon_Updater()
	version = updater.version

	def __init__(self, options, parser):
		""" Class Constructor """
		self.options = options
		self.parser  = parser
		
		# Connect to the DB
		try:
			self.db = madnet_db("dbname=netmon35 host=127.0.0.1")
		except Exception, e:
			raise ReactivationError("Unable to connect to database. %s " % e)
			
		self.get_current_reg()
		self.reg_dict['version'] = self.version
		
		# Initiate a new transaction
		self.db.set_autocommit(2)
		self.erase_current_reg()
		self.process_reactivation()
		# Commit the transaction, and revert the transaction state
		self.db.commit()
		self.db.set_autocommit(0)
		# Send reload signal to procmond
		self.restart_services()
		

	def print_verbose(self, str):
		""" Prints a string is 'verbose' mode is turned on """
		if (self.options.verbose):
			print "[DEBUG]: %s" % str

	def get_current_reg(self):
		""" Fetch the activation tokens from the DB """
		self.print_verbose("Retrieving Activation Tokens")
		query = "SELECT * FROM netmon"
		
		try:
			self.reg_dict = self.db.getRow(query)
		except Exception, e:
			raise ReactivationError("Database Error.%s " % e)
		
		
	
	def erase_current_reg(self):
		""" Deletes data from the 'netmon' table that contains the activation tokens """
		self.print_verbose("Deleting Activation Tokens")
		query = "delete from netmon"
		
		try:
			self.db.delete(query)
		except Exception, e:
			if ('1' == self.reg_dict['is_trial']):
				self.db.commit()
			else:
				self.db.rollback()
			raise ReactivationError("Database Error.%s " % e)

	
	def process_reactivation(self):
		""" Queries the cronos re-activation service and ensures the activation tokens are still active """
		self.print_verbose("Querying Netmon Activation Service")
		self.get_iface()
		self.reg_dict['mac_address'] = os.popen("/sbin/ifconfig " + self.iface).readlines()[0].split()[4]
		# Prepare query-string
		url = "http://dev.netmon.ca/cronos/blank.php?module=content&action=trial_react"

		# If there is an error, only one line is returned (the error message)
		# If the activation was successful, the first returned line will be the
		# registration key, and the second line will contain the client name.

		try:
			content = "\n".join(urllib.urlopen(url,urllib.urlencode(self.reg_dict)).readlines())
		except Exception,e:
			print "Unable to connect to activation server: %s" % e
			if ('1' == self.reg_dict['is_trial']):
				self.db.commit()
			else:
				print "Rolling back transaction"
				self.db.rollback()
			raise ReactivationError("Unable to register activation tokens.%s " % e)


		data = {}
		try:
			activation_doc = minidom.parseString(content)
			activation_xml = activation_doc.firstChild

			for node in activation_xml.getElementsByTagName('item'):
				data[node.getAttribute('name')] = node.getAttribute('value')
		except Exception,e:
			print "Unable to parse server response: %s" % e
			print content
			if ('1' == self.reg_dict['is_trial']):
				self.db.commit()
			else:
				self.db.rollback()
			raise ReactivationError("Unable to register activation tokens.%s " % e)
			
		
		try:
			status = data['status']
		except Exception, e:
			self.print_verbose("Activation server payload is invalid")
			self.db.commit()
			sys.exit(1)
		
		if (("OK" != data['status']) and ('1' == self.reg_dict['is_trial'])):
			self.print_verbose("Reactivation attempt failed.")
			self.print_verbose("Message from server was: %s" % status)
			self.db.commit()
			raise ReactivationError("Unable to register activation tokens.")
		elif ('OK' == data['status']):
			self.reg_dict['activation_key'] = data['activation_key']
			self.reg_dict['expires'] = data['expires']
			self.reg_dict['devices'] = data['devices']
			self.reg_dict['is_trial'] = data['is_trial']
			del(self.reg_dict['version'])
			del(self.reg_dict['mac_address'])
			
			query = "INSERT INTO netmon(%s) VALUES(%s)" % (",".join([x for x in self.reg_dict.keys()]),
													    ",".join([("%(x)s".replace('x', "%s" % x)) for x in self.reg_dict.keys()]))

			try:
				self.db.insert(query, self.reg_dict)
			except Exception, e:
				if ('1' == self.reg_dict['is_trial']):
					self.db.commit()
				else:
					self.db.rollback()
				raise ReactivationError("Unable to register activation tokens.%s " % e)
			
		elif ('ERROR' == data['status']):
			if ('1' == self.reg_dict['is_trial']):
				print "The activation server has returned the following error message:\n %s \n %s" % (data['error'], data['debug'])
				self.db.commit()
				raise ReactivationError("Unable to register activation tokens.")
			else:
				self.db.rollback()
			
		else:
			print 'Unexpected response from activation server'
			if ('1' == self.reg_dict['is_trial']):
				self.db.commit()
			else:
				self.db.rollback()
			raise ReactivationError("Unable to register activation tokens.")
		
			
	
	def restart_services(self):
		""" This should send a SIGINT to procmond so it can do its part """
		self.print_verbose("Reloading Netmon Services")
		os.system("/etc/init.d/netmon reload")

	def get_iface(self):
		# Add new patterns ordered by preference to this list.
		patterns = ['eth(\d)', 'wlan(\d)']
		patterns = filter(re.compile, patterns)
		
		# Final storage for our ordered interface list
		score = {}
		# Raw interface list
		iface = [x.strip() for x in (os.popen("cat /proc/net/dev | cut -f 1 -d ':' | grep -v '|'"))]
		
		for i in iface:
		    for pattern in patterns:
		        re_match = re.match(pattern, i)
		        if None != re_match:
		            score[i] = (len(iface)*(len(patterns) - (patterns.index(pattern)))) - int(re_match.group(1)) 
		            
		items = score.items()
		items.sort(key=lambda x: x[1], reverse=True)
		self.iface = items[0][0]

if __name__=='__main__':
	# Jump to the script's root
	os.chdir(os.path.dirname(sys.argv[0]))

	usage       = "Usage: %prog [-v --verbose]"
	version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
	description = "Netmon Re-activation system "
	parser = OptionParser(usage=usage, version=version, description=description)
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Enable extra output")

	(options, args) = parser.parse_args()
	try:
		obj = Reactor(options, parser)
	except Exception, e:
		print "An error occured: %s" % e
		sys.exit(1)