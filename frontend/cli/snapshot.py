#!/usr/bin/env python
# -*- coding: utf-8 -*-

import smtplib
import os
import re
import sys
import tarfile
import os.path
import mimetypes
import commands
import string

# CLI Arguments Parser
from optparse import OptionParser

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)

# Mime email handling stuff.
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders


# Connect to the DB
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
except Exception, e:
	print "Unable to connect to database: %s" % e
	pass

class Log_Snapshot:
	
	# Properties initialization
	snapshot    = "/var/log/netmon_snapshot.tgz"
	logdir      = "/var/log/netmon"
	to_addr     = ["snapshots@netmon.ca"]
	smtp        = {"smtpaccount": "no-reply@netmon.ca", "smtpserver": "127.0.0.1"}
	
	def __init__(self, options, parser):
		self.options = options
		self.parser  = parser
		
		# Remove previous snapshot container.
		if os.path.exists(self.snapshot):
			os.unlink(self.snapshot)
		self.get_smtp_params()
	
	def get_smtp_params(self):
		# Retrieve the SMTP server and source address
		# from the Netmon settings database.
		#print "Retrieving SMTP settings"
		query = "SELECT var, value FROM daemonsconfig WHERE daemon_id = (SELECT id FROM daemons WHERE name = 'emailmond')"
		
		try:
			res = db.select(query)
		except Exception, e:
			# In case of error, we will just use the default values.
			pass
		
		for row in res:
			self.smtp[row['var']] = row['value']
	
	# This method creates the actual tarball
	def build_snapshot(self):
		try:
			tf = tarfile.open(self.snapshot, "w:gz")
		except Exception, e:
			print "Error: %s" % e
			sys.exit(-1)
		
		os.chdir(self.logdir)		
		
		for logfile in os.listdir(self.logdir):
			print "Trimming log file %s" % (logfile)
			# tail -n200 logfile > logfile would not work, so we create a tmp file.
			os.system("tail -n200 %s > %s2" % (logfile, logfile))
			os.system("mv %s2 %s" % (logfile, logfile))
			print "Adding %s to the snapshot payload" % (logfile)
			tf.add(logfile)
		
		os.chdir("/")	
		if (True == self.options.crash):
			regex = re.compile("^core.*")
			for rootfile in os.listdir("/"):
				if regex.match(rootfile) != None:
					print "Adding %s core dump to snapshot" % rootfile
					tf.add(rootfile)
					os.unlink(rootfile)
			
		tf.close()
		del(tf)
		
	"""
	There doesn't appear to be a high-level mime email
	handling module in the core python distribution so
	we need to work with the components of the 'email'
	module.
	
	This method creates a multipart mime email container
	and attaches the tarball created in the build_snapshot
	method. It then calls the build_system_summary
	method and captures the output to attach it as a text
	payload, which email clients will use as actual
	content of the email.
	
	"""
	def send_snapshot(self):
		# Create the tarball
		self.build_snapshot()
		
		# Create the multipart container and initialize it
		msg = MIMEMultipart()
		msg['Subject'] = "[SNAP] Netmon Log Snapshot"
		msg['From'] = self.smtp['smtpaccount']
		msg['To'] = ", ".join(self.to_addr)
		msg.preamble = "Logfile snapshot attached"
		msg.epilogue = ""
		
		# Get the mime properties of our tarball
		ctype, encoding = mimetypes.guess_type(self.snapshot)
		
		if ctype is None or encoding is None:
			ctype = "application/octet-stream"
		maintype, subtype = ctype.split('/', 1)
		
		# Attach the snapshot as mime payload
		fp = open(self.snapshot, "rb")
		payload = MIMEBase(maintype, subtype)
		payload.set_payload(fp.read())
		fp.close()
		Encoders.encode_base64(payload)
		payload.add_header('Content-Disposition', 'attachment', filename=os.path.basename(self.snapshot))
		msg.attach(payload)
		
		# Add the summary
		summary = MIMEText(self.build_system_summary())
		summary.add_header('Content-Disposition', 'inline')
		msg.attach(summary)
		
		# Send the email using the local SMTP server
		print "Sending the snapshot"
		sc = smtplib.SMTP(self.smtp['smtpserver'])
		sc.sendmail(self.smtp['smtpaccount'], self.to_addr, msg.as_string())
		sc.quit()
		
	# Creates a summary of the system status
	def build_system_summary(self):
		summary = ""
		
		# Attempt to retrieve registration information from the DB
		try:
			print "Retrieving registration information"
			row = db.getRow("SELECT * FROM netmon")
		except:
			pass
		
		
		max_keys = max(map(len, row.keys()))+3
		max_vals = max(map(len, map(str, row.values())))+3
		
		# Format registration info.
		summary = summary + "Key".center(max_keys) + "Value".ljust(max_vals) + "\n"
		summary = summary + ("=" * (max_keys + max_vals)) + "\n"
		for field in row.keys():
			summary = summary + str(field).ljust(max_keys) + str(row[field]).ljust(max_vals) + "\n"
		
		try:
			print "Determining network size"
			row = db.getRow("SELECT COUNT(DISTINCT mac) AS nodes FROM arptable")
		except Exception, e:
			print e
			pass
		
		summary = summary + "\nNetwork size: Approximately %s nodes\n" % row['nodes']
			
		print "Retrieving Data-archival settings"
		summary = summary + "\nData-Archival Settings:\n"
		try:
			rows = db.select("select a.name AS \"Daemon\", b.value AS \"Duration (weeks)\" FROM daemons a, daemonsconfig b WHERE a.id = b.daemon_id AND b.var = 'data_archival'")
		except Exception, e:
			print e
			pass
		
		summary = summary + rows[0].keys()[0].rjust(20)   + "      " + rows[0].keys()[1].ljust(10) + "\n"
		for row in rows:
			summary = summary + row.values()[0].rjust(20) + "      " + row.values()[1].ljust(10) + "\n"
		summary = summary + "\n"
		
		
		# Add the output of all the commands in the list to the summary
		my_commands = ['ifconfig -v', 'route -vneF', 'w', 'uname -a', \
					   'python /apache/cli/update.pyc -s', \
					   'df -hTP', 'ip neigh show', 'ipcs -u', 'ps auxwwwf', \
					   'free -kt', 'mount', 'lspci', 'last -adx', 'lastb -adx',
					   'netstat -tulnp', 'crontab -l', 
					   'tail -n80 /var/log/syslog', 'dmesg | tail -n10']
		
		if (True == self.options.daemons):
			my_commands.append('python /apache/cli/daemon_check.py')
		
		for command in my_commands:
			summary = summary + "\n\n%s" % self.get_command(command)
			summary = summary + "\n\n" + ("*" * 45) + "\n"
		
		my_files = ['/etc/init.d/netmon', '/etc/network/interfaces',
			    '/etc/cron.d/netmon', '/etc/cron.d/netmon_updates',
			    '/etc/cron.d/postgresql', '/root/.bash_history']
		
		for file in my_files:
			summary = summary + "\n\n%s" % self.get_file(file)
			summary = summary + "\n\n[" + ("=" * 45) + "]\n"
			    
		return summary
	
	# Simple pretty wrapper for commands.getoutput()
	def get_command(self, command):
		print "Running %s" % command
		return "Output of command \"%s\":\n%s" % (command, commands.getoutput(command))
		
	def get_file(self, file):
		print "Examining file %s" % file
		try:
			return "Contents of file %s:\n\n%s" % (file, "".join(open(file).readlines()))
		except Exception, e:
			print "Warning: Unable to retrieve file %s: %s" % (file, e)
			pass

if __name__=='__main__':
	# Jump to the script's root
	# 20181217 - Fixed Error of No file or directory '' - Gurpreet Anand
  	os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

	usage       = "Usage: %prog [-v --verbose]"
	version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
	description = "Netmon Automated Snapshot System"
	parser = OptionParser(usage=usage, version=version, description=description)
	
	
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Enable extra output")
	parser.add_option("-c", "--crash", action="store_true", dest="crash", default=False, help="Attach daemons core dumps (system crash)")
	parser.add_option("-d", "--daemoncheck", action="store_true", dest="daemons", default=False, help="Check daemon data to verify they are running (very expensive)")


	(options, args) = parser.parse_args()
	obj = Log_Snapshot(options, parser)
	obj.send_snapshot()
