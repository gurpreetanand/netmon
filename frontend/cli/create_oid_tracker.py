#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
import os


# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	
# Connect to the database
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1")
	db.set_autocommit(0)
except Exception, e:
	print "ERROR: %s" % e
	

if __name__=='__main__':
	try:


		dev_id = sys.argv[1]
		oid = sys.argv[2]
		label = sys.argv[3]
		datatype="INTEGER"
		if (sys.argv[4]):
			datatype = sys.argv[4]  

		query="INSERT INTO OIDS (device_id,oid,interval,label,datatype) VALUES (%s,'%s','3600','%s','%s')" % (dev_id,oid,label,datatype)
		print(query)
		os.system("echo \"%s;\"|psql netmon35" % query)
	except Exception, e:
		print "ERROR: %s" % e

