#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Sends SHUN/NO SHUN commands to a Cisco PIX firewall 
# Created:   14/03/07 02:47:21 PM

# Imports
import os
import sys
import os.path
import cisco

# CLI Arguments Parser
from optparse import OptionParser

# Exceptions
class UsageError(Exception):
	""" Simple handler for CLI usage errors """
	def __init__(self, msg):
		self.msg = msg
		
class RuntimeError(Exception):
	""" Simple handler for run-time errors """
	def __init__(self, msg):
		self.msg = "!!! ERROR: %s" % msg


# Main class
class PIX_SHUN_Manager:
	"""Sends SHUN/NO SHUN commands to a Cisco PIX firewall """
	errors = ""
	ip     = ""

	def __init__(self, options, parser, ip):
		""" Constructor """
		self.options = options
		self.parser = parser
		self.ip = ip
		
		self.check_params()
		self.manage_shun()

	def check_params(self):
		""" This method ensures all the mandatory CLI arguments were provided.
		It does not attempt to validate any of the arguments. at this point """

		if not self.options.host:
			self._add_error("Missing option --cisco-pix (or -c)")
		if not self.options.password:
			self._add_error("Missing option --password (or -p)")
		if not self.options.enable:
			self._add_error("Missing option --enable (or -e)")
		if not self.options.action:
			self._add_error("Missing --block or --unblock argument")
		if len(self.ip) == 0:
			self._add_error("You did not speficy a list of IPs to shun/no shun")
			
		# Raise UsageError if any errors were encountered.
		if len(self.errors) > 0:
			raise UsageError, self.errors
	
	def _add_error(self, error):
		""" Simply registers an error in our UsageError buffer """
		self.errors += "!!! ERROR: %s\n" % error
		
	def manage_shun(self):
		""" This function uses our cisco.PIX object to communicate with the PIX
		firewall and send it the appropriate SHUN or NO SHUN command.
		Once the command has been executed, it displays a summary of registered
		SHUNs in the device as well as SHUN statistics. """
		pix = cisco.PIX(self.options.host, self.options.password, self.options.enable)
		if ("block" == self.options.action):
			print "SHUNing %s" % self.ip
			print pix.getoutput("shun %s" % self.ip)
		else:
			print "Removing SHUN on %s" % self.ip
			print pix.getoutput("no shun %s" % self.ip)
			
		print "Current SHUNs:"
		print pix.getoutput("show shun")
		print "\nSHUN Settings:"
		print pix.getoutput("show shun statistics")
			
	
	def _print_verbose(self, msg):
		""" Prints the message along with a "--VERBOSE--" flag if verbose mode is active """
		if (True == self.options.verbose):
			print "--VERBOSE-- %s" % msg




if __name__=='__main__':
	# Jump to the script's root
	# 20181217 - Fixed Error of No file or directory '' - Gurpreet Anand
  	os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

	usage       = "Usage: %prog [-c PIX_IP] [-u USERNAME] [-e ENABLE_PASS] [-p PASS] [--block | --unblock] <-v> IP_ADDRESS"
	version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
	description = "Sends SHUN/NO SHUN commands to a Cisco PIX firewall through Telnet"
	parser = OptionParser(usage=usage, version=version, description=description)
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Enable extra output")
	parser.add_option("-c", "--cisco-pix", action="store", dest="host", help="Cisco PIX host address")
	parser.add_option("-p", "--password", action="store", dest="password", help="Authentication Password")
	parser.add_option("-e", "--enable", action="store", dest="enable", help="Enable password")
	parser.add_option("--block", action="store_const", dest="action", const="block", help="SHUN the IP address")
	parser.add_option("--unblock", action="store_const", dest="action", const="unblock", help="NO SHUN the IP address")

	(options, parser) = parser.parse_args()
	if (len(args) > 1):
		print "*** WARNING: The following parameters were discarded: %s" % ",".join(["'%s'" %  x for x in args[1:]])
	
	try:
		obj = PIX_SHUN_Manager(options, parser, args[0])
	except UsageError, e:
		parser.print_help()
		print "\n\n%s" % e.msg
		sys.exit(1)
	except RuntimeError, e:
		print "\n%s" % e.msg
		sys.exit(1)
	#except Exception, e:
	#    print "\nUnhandled exception: %s" % e
	#    sys.exit(1)