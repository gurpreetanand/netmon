#!/usr/bin/env python


# Generic DB Error Exception
class DB_Error(Exception):
	def __init__(self, value):
		self.value = value
	
	def __str__(self):
		return repr(self.value)


# Connection Error
class DB_Connect_Error(DB_Error):
	pass


# Execution Exception
class DB_Query_Error(DB_Error):
	def __init__(self, query, error):
		self.query = query
		self.error = error
	
	def __str__(self):
		return str(self.error)
		

# psycoPg missing
class DB_Import_Error(DB_Error):
	pass

# Imports
try:
	import psycopg2
except:
	raise DB_Import_Error("You must install the python module named \"psycopg2\" in order to use this module.")

# Core class
class madnet_db:
	"""
	__init__(dsn)
	
	dsn should be a string in the format "a=b c=d e=f"
	The following arguments are supported:
	
	dbname, user, password, port, host
	
	If you intend to connect to a local database, do not specify port or host
	and the module will automatically use local socket connection.
	"""
	def __init__(self, dsn=""):
		try:
			self.conn = psycopg2.connect(dsn)
		except Exception, e:
			raise DB_Connect_Error("%s" % e)
		
		self.autocommit = 1
		self.curs = self.conn.cursor()
		
	def close(self):
		self.commit()
		self.conn.close()
	
	def commit(self):
		self.conn.commit()
			
	def rollback(self):
		self.conn.rollback()
	
	def set_autocommit(self, val = 2):
		self.autocommit = val
		self.conn.set_isolation_level(val)
		
	def insert(self, query, vars=None):
		query,vars = self.unpack(query, vars)
		self.curs.execute(query, vars)
		if ((0 == self.autocommit) or (1 == self.autocommit)):
			self.commit()
		
	def insert_many(self, query, vars):
		query,vars = self.unpack(query, vars)
		try:
			self.curs.executemany(query, vars)
			self.commit()
		except Exception, e:
			self.commit()
			print "Failed query!", self.curs.query, e
		
	def pack(self, query, vars=None):
		return {'query': query, 'vars': vars}
	
	def unpack(self, query, vars=None):
		if isinstance(query, dict):
			return (query['query'], query['vars'])
		else:
			return (query, vars)
		
	def update(self, query, vars=None):
		self.insert(query, vars)
		
	def delete(self, query, vars=None):
		self.insert(query, vars)
		
	def select(self, query, vars=None, iter=False):
		query,vars = self.unpack(query, vars)
		try:
			self.curs.execute(str(query), vars)
		except Exception, e:
			raise DB_Query_Error(query, "Unable to execute query: %s\n%s" % (str(self.curs.query), str(e)))
		
		try:
			if True == iter:
				resultset = self._to_iter(self.curs)
			else:
				resultset = self._iter_to_dicts(self._to_iter(self.curs))
		except Exception, e:
			raise DB_Query_Error(query, "Unable to fetch resultset - %s\n%s" % (e, self.curs.query))
		
		return resultset
	

	def getRow(self, query, vars=None):
		query,vars = self.unpack(query, vars)
		try:
			self.curs.execute(str(query), vars)
		except Exception, e:
			raise DB_Query_Error(query, "Unable to execute query - %s\n%s" % (e, self.curs.query))
		
		try:
			row = self.curs.fetchone()
		except Exception, e:
			raise DB_Query_Error(self.curs.query, "Unable to fetch resultset: %s\n%s" % (e, self.curs.query))
		
		try:
			resultset = self._to_dict(self.curs.description, row)
		except Exception, e:
			raise DB_Query_Error(self.curs.query, "Unable to analyze resultset: %s\n%s\n%s" % (e, self.curs.query, row))
		
		return resultset
	
	""" These helpers are based on Django's backend util source """
	
	# Since psycopg2 dropped dictfetchone, this creates a dict based on a resultset
	def _to_dict(self, desc, res):
		return dict(zip([x[0] for x in desc], res))

	# And since it dropped dictfetchall, this creates an itterator based on the previous method
	def _to_iter(self, cursor):
		desc = cursor.description
		for row in cursor.fetchall():
			yield self._to_dict(desc, row)

	# Retrieves the contents of an iterator into a dict
	def _iter_to_dicts(self, iter):
		dict = []
		for row in iter:
			dict.append(row)
		return dict

