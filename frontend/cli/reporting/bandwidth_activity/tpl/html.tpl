#import time
#import urllib
#from reportHelper import *

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

#def getXML
#set $count=1
<graph lineThickness='2' showAnchors='#if (len($data.time) > 30) then "0" else "1"#' caption='$interface.device_name ($interface.ip)' subcaption='Interface $interface.interface : $interface.description.replace('\'', "`")' numberSuffix='bps' decimalPrecision='1' numdivlines='6' showAreaBorder='1' areaBorderColor='444444' numberPrefix='' canvasBgColor='' canvasbordercolor='888888' canvasBorderThickness='2' showNames='1' rotateNames='1' numVDivLines='25' vDivLineAlpha='30' showAlternateHGridColor='1' alternateHGridColor='C9C9C9' formatNumberScale='1' bgcolor='F1F1F1' animation='0' anchorRadius='5' showShadow='1' lineThickness='3'>#slurp
<categories>#slurp
#for $row in $data.time
<category name='$time.strftime('%Y-%m-%d %H:%M', $time.localtime($row))' #slurp
#if ($divmod($count,3)[1] == 0) then "showName='1'" else "showName='0'" # /> #slurp
#set $count += 1
#end for
</categories>#slurp
<dataset seriesname='Inbound Traffic' color='0066CC' showValues='0' showAreaBorder='1' areaBorderColor='0066CC' anchorsides='5' anchorBorderThickness='1' anchorBgColor='D4E5F6' >#slurp
#for $row in $data.in
<set value='$row' />#slurp
#end for
</dataset>#slurp
<dataset seriesname='Outbound Traffic' color='339999' showValues='0' showAreaBorder='1' areaBorderColor='339999' anchorsides='5' anchorBorderThickness='1' anchorBgColor='B2D1F0'>#slurp
#for $row in $data.out
<set value='$row' />#slurp
#end for
</dataset>#slurp
</graph>#slurp
#end def

<!-- (\$Id$) -->
#if ($varExists('data') and len($data) > 0)
<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Bandwidth Activity Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	  </tr>

	</table>
	<br /><br />
</div>

<div class="noPrint">
	<div class="titlebar">
		Bandwidth Activity Report
	</div>

	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page">
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_bandwidth_activity');">
	</div>
</div>

<div class="panel" align="center">
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="500" height="400">
				<param name="movie" value="/assets/charts/FC_2_3_MSLine_2.swf">
				<param name="quality" value="">
				<param name="bgcolor" value="#F1F1F1">
				<param name="FlashVars" value="&chartWidth=500&chartHeight=400&dataXML=$getXML()">
				<embed src="/assets/charts/FC_2_3_MSLine_2.swf" bgcolor="#F1F1F1" FlashVars="&chartWidth=500&chartHeight=400&dataXML=$getXML()" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="500" height="400"></embed>
        </object>
</div>

<div class="datagrid center" id="TotalGrid">
	<table width="100%" cellpadding="3" cellspacing="0" border="0">
		<tr>
			<th>Inbound: </th>
			<th>Outbound: </th>
			<th>Total: </th>
		</tr>
		<tr>
			<td>$tpl_capacity($totals.in)</td>
			<td>$tpl_capacity($totals.out)</td>
			<td>$tpl_capacity($totals.in+$totals.out)</td>
		</tr>
	</table>
</div>

#end if
