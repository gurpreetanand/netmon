#import time
#import urllib
#from reportHelper import *

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

#def getPieXML
<graph caption='$urllib.quote_plus($data.meta[0].label)' bgcolor='F1F1F1' formatNumberScale='0' pieSliceDepth='75' decimalPrecision='0' showPercentageValues='1' showNames='0' numberPrefix='' showValues='0' numberSuffix='' showPercentageInLabel='1' pieYScale='35' pieBorderAlpha='100' pieBorderColor='F1F1F1' pieFillAlpha='90' pieBorderThickness='2' showHoverCap='1' animation='1' showShadow='1' shadowColor='666666' shadowXShift='4' shadowYShift='4' shadowAlpha='60' pieRadius='200'>#slurp
#for $row in $data.datapoints
<set name='$urllib.quote_plus($row.name)' value='$row.frequency' color='$row.colour' />#slurp
#end for
</graph>#slurp
#end def

#def getLineXML
<graph yAxisMinValue='$data.min' yAxisMaxValue='$data.max' lineThickness='#if (len($data.datapoints) < 150) then "2" else "1"#' showAnchors='#if (len($data.datapoints) > 30) then "0" else "1"#' caption='$urllib.quote_plus($data.meta[0].label)' subcaption='on $data.meta[0].device_label' divlinecolor='C5C5C5' decimalPrecision='1' numdivlines='6' showAreaBorder='1' areaBorderColor='444444' numberPrefix='' canvasBgColor='E1E1E1' canvasbordercolor='888888' canvasBorderThickness='2' showNames='1' rotateNames='1' numVDivLines='25' vDivLineAlpha='30' showAlternateHGridColor='1' alternateHGridColor='C9C9C9' formatNumberScale='0' bgcolor='F1F1F1' animation='0' anchorRadius='3' showShadow='1'>#slurp
<categories>#slurp
#set $count=-1
#for $row in $data.datapoints
#set $count += 1
<category name='$time.strftime("%b %d, %y %H:%M:%S", $time.localtime($row.timestamp))' showName='#if ($divmod($count, $data.step)[1] == 0) then "1" else "0"#' />#slurp
#end for
</categories>#slurp
<dataset seriesname='$urllib.quote_plus($data.meta[0].label)' color='FF6600' showValues='0' showAreaBorder='1' areaBorderColor='FFD9BF' anchorsides='5' anchorBorderThickness='1' anchorBgColor='D4E5F6' >#slurp
#for $row in $data.datapoints
<set value='#if (True == $options.delta) then $row.delta else $row.message#' />#slurp
#end for
</dataset>#slurp
</graph>#slurp
#end def


#if ($varExists('data') and (len($data) > 0))
<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>OID Tracker Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	  </tr>

	</table>
	<br /><br />
</div>

<div class="noPrint">
	<div class="titlebar">
		OID Tracker Report
	</div>

	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page">
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_oid_tracker');">
	</div>
</div>

#if ($varExists('data.meta') and len($data.meta) > 0)
#if ('numeric' == $data.meta[0].datatype)
<div class="panel" align="center">
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="500" height="400">
				<param name="movie" value="/assets/charts/FC_2_3_MSLine_2.swf">
				<param name="quality" value="">
				<param name="bgcolor" value="#F1F1F1">
				<param name="FlashVars" value="&chartWidth=500&chartHeight=400&dataXML=$getLineXML()">
				<embed src="/assets/charts/FC_2_3_MSLine_2.swf" bgcolor="#F1F1F1" FlashVars="&chartWidth=500&chartHeight=400&dataXML=$getLineXML()" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="500" height="400"></embed>
        </object>
</div>
#else
<div class="panel">
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="500" height="400">
				<param name="movie" value="/assets/charts/FC_2_3_MSLine_2.swf">
				<param name="quality" value="">
				<param name="bgcolor" value="#F1F1F1">
				<param name="FlashVars" value="&chartWidth=500&chartHeight=400&dataXML=$getPieXML()">
				<embed src="/assets/charts/FC_2_3_Pie3D.swf" bgcolor="#F1F1F1" FlashVars="&chartWidth=500&chartHeight=400&dataXML=$getPieXML()" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="500" height="400"></embed>
        </object>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="500" height="400">
				<param name="movie" value="/assets/charts/FC_2_3_MSLine_2.swf">
				<param name="quality" value="">
				<param name="bgcolor" value="#F1F1F1">
				<param name="FlashVars" value="&chartWidth=500&chartHeight=400&dataXML=$getPieXML()">
				<embed src="/assets/charts/FC_2_3_SSGrid.swf" bgcolor="#F1F1F1" FlashVars="&chartWidth=500&chartHeight=400&dataXML=$getPieXML()" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="500" height="400"></embed>
        </object>
</div>
#end if

#end if

#end if
