#import cgi
#import time
#import urllib
#from reportHelper import *

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>
#set $div_id = 0


<!-- (\$Id$) -->

<script language="Javascript">

	toggleTR = function(id) {
		tr_id = "tr_"+id
		td_id = "td_"+id
		
		style = document.getElementById(tr_id).style.display;
		
		if (style == 'none') {
			document.getElementById(tr_id).style.display = '';
			document.getElementById(td_id).childNodes[0].innerHTML = 'Hide';
		} else {
			document.getElementById(tr_id).style.display = 'none';
			document.getElementById(td_id).childNodes[0].innerHTML = 'Show';
		}
	}

</script>

<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Email Traffic Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	  </tr>

	</table>
	<br /><br />
</div>
<div class="noPrint">
	<div class="titlebar">
		Email Traffic Report
	</div>
	
	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page"> 
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_web_traffic');">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	</div>
</div>


#if ($varExists('data') and len($data) > 0)
<div class="datagrid">

	<table width="100%" class="sort-table" id="report_table" border="0" cellpadding="1" cellspacing="0">
		<thead>
			<tr>
				<td width="50px">Details</td>
				<td width="200px">Sender </td>
				<td width="200px">Recipient</td>
				<td>Subject</td>
			</tr>
		</thead>
		<tbody>

		#for $row in $data
		#set $div_id += 1
		#try
		#set address = $cgi.escape($row.account+ "@" + $row.domain)
		#except
		#set address = "N/A"
		#end try
		
		#try
		#set rec_address = $cgi.escape($row.recipients[0].account + "@" + $row.recipients[0].domain)
		#except
		#set rec_address = "N/A"
		#end try
		<tr>
			<td  width="60px" id="td_$div_id"><a href="javascript:toggleTR($div_id)">Show</a></td>
			<td width="150px"><a title="$address" href="mailto:$address">#if '' != $row.label then $row.label else $address#</a></td>
			#if len($row.recipients) > 0
			<td width="150px">$row.recipients[0].email_field.capitalize(): <a title="$rec_address" href="mailto:$rec_address">#if '' != $row.recipients[0].label then $row.recipients[0].label else $rec_address#</a>#if len($row.recipients) > 1 then "..." else ""#</td>
			#else
			<td></td>
			#end if
			<td>$row.subject</td>
		</tr>
		<tr id="tr_$div_id" style="display: none">
			<td colspan="4">
				<div class="datagrid">
				<table width="100%" align="center" border="0">
					<tbody>
						<tr>
							<td class="headerstyle" style="background-color: #F1F1F1" width="150px">All Recipients</td>
							<td colspan="3">
							#for rec in $row.recipients
							#set rec_address = $rec.account + "@" + $rec.domain
							$rec.email_field.capitalize():&nbsp;<a title="$rec_address.replace('"', '')" href="mailto:$rec_address">#if '' != $rec.label then $cgi.escape($rec.label) else $rec_address#</a><br />
							#end for
							</td>
						</tr>
						<tr>
							<td class="headerstyle" style="background-color: #F1F1F1" width="150px">Client IP</td>
							<td colspan="3">$row.client_ip</td>
						</tr>
						<tr>
							<td class="headerstyle" style="background-color: #F1F1F1" width="150px">Server IP</td>
							<td colspan="3">$row.server_ip</td>
						</tr>
						<tr>
							<td class="headerstyle" style="background-color: #F1F1F1" width="150px">Attachments</td>
							<td colspan="3">
							#if len($row.attachments) > 0
							#for at in $row.attachments
							<strong>$cgi.escape($at.filename)</strong><embed> ($at.content_type) - $tpl_capacity($at.size)</embed><br />
							#end for
							#else
							<embed>N/A</embed>
							#end if
							</td>
						</tr>
						<tr>
							<td class="headerstyle" style="background-color: #F1F1F1" width="150px">Message Size</td>
							<td colspan="3">$tpl_capacity($row.size)</td>
						</tr>
						<tr>
							<td class="headerstyle" style="background-color: #F1F1F1" width="150px">Headers</td>
							<td colspan="3"><pre>$cgi.escape($row.headers)</pre></td>
						</tr>
						
					</tbody>
				</table>
				</div>
			</td>
		</tr>
	#end for
	</table>
#end if