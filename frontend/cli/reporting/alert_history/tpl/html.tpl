#import time
#from reportHelper import *

<!-- (\$Id$) -->

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>
	
<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Alert History Report</h1></div></td>
	  </tr>
	  <tr>
	   <td><div align="right">$time.strftime("%y-%m-%d %H:%M:%S")</div></td>
	  </tr>
	  
	</table>
	<br /><br />
</div>

<div class="noPrint">
	<div class="titlebar">
		Alert History Report
	</div>
	
	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page"> 
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_alert_history');">
	</div>
</div>

#if ($varExists('data') and len($data) > 0)

<div class="datagrid">
<table width="100%" cellpadding="0" cellspacing="0" id="report_table">
	<thead>
	<tr>
		<td>Triggered</td>
		<td>Dispatched</td>
		<td>Recipient</td>
		<td>Message</td>
		<td>Notified</td>
		<td>Retries</td>
	</tr>
	</thead>
	<tbody>
	#for $row in $data
	<tr>
		#set $name = $str($row.first_name).capitalize() + ' ' + $str($row.last_name).capitalize()
		<td>$time.strftime("%b %d, %y %H:%M:%S", $time.localtime($row.trigger_timestamp))</td>
		<td>$time.strftime("%b %d, %y %H:%M:%S", $time.localtime($row.dispatch_timestamp))</td>
		<td><div title="$row.email">$name ($row.media.capitalize())</div></td>
		<td><pre>$row.parsed_alert_message</pre></td>
		<td>#if ($row.sent == '1') then 'Y' else 'N'#</td>
		<td>$row.retries_processed/$row.required_retries</td>
	</tr>
	#end for
		</tbody>
</table>
</div>
<script language="Javascript">
document.getElementById("report_table").className = 'sort-table';
var types = ['netmon_date', 'netmon_date', 'CaseInsensitiveString', 'CaseInsensitiveString', 'String', 'retries'];
var report = new SortableTable(document.getElementById("report_table"), types);
report.sort(0, true);	
</script>

#else
$message_bar("No alert has been triggered during the specified time-range")
#end if

<!-- (\$Id$) -->