#import time
#import urllib
#from reportHelper import *

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

#def getXML
<graph showAnchors='#if (len($data.datapoints) > 30) then "0" else "1"#' lineThickness='#if (len($data.datapoints) < 150) then "2" else "1"#' caption='Pattern match for $urllib.quote_plus($data.meta[0].pattern)' subcaption='On $urllib.quote_plus($data.meta[0].url)' divlinecolor='C5C5C5' numdivlines='6' showAreaBorder='1' divLineDecimalPrecision='1' areaBorderColor='444444' numberPrefix='' canvasBgColor='E1E1E1' canvasbordercolor='888888' canvasBorderThickness='2' showNames='1' rotateNames='1' numVDivLines='25' vDivLineAlpha='30' showAlternateHGridColor='1' alternateHGridColor='C9C9C9' bgcolor='F1F1F1' animation='0' numberSuffix=' ms' decimalPrecision='0' formatNumber='1' formatNumberScale='0' anchorRadius='4' showShadow='1' lineThickness='3'  yAxisMinValue='$data.meta[0].min' yAxisMaxValue='$data.meta[0].max'>#slurp
<categories>#slurp
#set $count=-1
#for $row in $data.datapoints
#set $count += 1
<category name='$time.strftime("%b %d, %y %H:%M:%S", $time.localtime($row.timestamp))' showName='#if ($divmod($count, $data.step)[1] == 0) then "1" else "0"#' />#slurp
#end for
</categories>#slurp
<dataset seriesname='Pattern Matched' color='009900' showValues='0' showAreaBorder='1' areaBorderColor='009900' anchorsides='5' anchorBorderThickness='1' anchorBgColor='EEEEEE' >#slurp
#for $row in $data.datapoints
#if 'MATCH' == $row.status
<set value='$row.latency' />#slurp
#else
<set />#slurp
#end if
#end for
</dataset>#slurp
<dataset showAnchors='#if ($data.meta[0].no_match > 30) then "0" else "1"#' seriesname='Pattern Did not Match' color='FF0000' showValues='0' showAreaBorder='1' areaBorderColor='FF0000' anchorsides='3' anchorBorderThickness='1' anchorBgColor='EEEEEE' >#slurp
#for $row in $data.datapoints
#if 'MATCH' != $row.status
<set value='$row.latency' />#slurp
#else
<set />#slurp
#end if
#end for
</dataset>#slurp
</graph>#slurp
#end def

#if ($varExists('data') and (len($data) > 0))
<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>URL Tracker Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	  </tr>

	</table>
	<br /><br />
</div>

<div class="noPrint">
	<div class="titlebar">
		URL Tracker Report
	</div>

	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page">
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_url_tracker');">
	</div>
</div>

#if ($varExists('data.meta') and len($data.meta) > 0)

<div class="panel" align="center">
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="500" height="400">
				<param name="movie" value="/assets/charts/FC_2_3_MSLine_2.swf">
				<param name="quality" value="">
				<param name="bgcolor" value="#F1F1F1">
				<param name="FlashVars" value="&chartWidth=500&chartHeight=400&dataXML=$getXML()">
				<embed src="/assets/charts/FC_2_3_MSLine_2.swf" bgcolor="#F1F1F1" FlashVars="&chartWidth=500&chartHeight=400&dataXML=$getXML()" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="500" height="400"></embed>
        </object>
</div>

#end if



#end if
