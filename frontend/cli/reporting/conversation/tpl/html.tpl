#import time
#from reportHelper import *

<!-- (\$Id$) -->

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>


<style>

span.resolved {
	display: list-item;
	list-style-type: none;
	list-style-image: none;
	text-decoration: underline;
}

span.unresolved {
	display: list-item;
	list-style: square inside url('assets/icons/unresolved.gif');
}

DIV.red {
	font-size: 11px;
	padding: 2px;
	background-color: #FF0000;
	color: White;
	vertical-align: absmiddle;
	margin-right: 2px;
	padding-left: 4px;
	display: inline;
}

DIV.green {
	font-size: 11px;
	padding: 2px;
	background-color: #00CC00;
	color: White;
	vertical-align: absmiddle;
	margin-right: 2px;
	padding-left: 4px;
	display: inline;
}

</style>

<script>


init_sort = function() {
	document.getElementById("report_table").className = 'sort-table';
	var types = [#if ("timestamp" == $options.orderby) then "'netmon_date', " else ""#'CaseInsensitiveString', 'CaseInsensitiveString', 'netmon_capacity', 'CaseInsensitiveString'];
	var report = new SortableTable(document.getElementById("report_table"), types);
	report.sort(#if ("timestamp" == $options.orderby) then 0 else 2#, true);
}

function toggleDiv(div_id, title_div) {
	div = document.getElementById(div_id);
	if (div.style.display == "none") {
	div.style.display = "block";
	title_div.className = "titlebar_expanded";
	} else {
	div.style.display = "none";
	title_div.className = "titlebar_collapsed";
	}
}

</script>

<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	<tr>
	<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
	<td><div align="right"><h1>Conversation Report</h1></div></td>
	</tr>
	<tr>
	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	</tr>

	</table>
	<br /><br />
</div>
<div class="noPrint">
	<div class="titlebar">
	Conversation Report
	</div>
	<!--
	<div class="titlebar_collapsed" onclick="toggleDiv('builderDiv', this);">
	Report Parameters
	</div>
	<div id="builderDiv" class="panel" style="display: none">
	{madnet_action module="mod_reports" action="form" type=network_activity}
	</div>
	-->
	<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page"> 
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_conversation');">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">

	</div>
</div>
#if ($varExists('data') and len($data) > 0)
<div class="datagrid center">

	<table width="100%" border="0" cellpadding="3" cellspacing="0" id="report_table">
		<thead>
			<tr>
				#if ("timestamp" == $options.orderby) then "<td>When</td>" else "" #
				<td>Destination </td>
				<td>Source </td>
				<td>Bytes </td>
				<td>Port </td>
			</tr>
		</thead>
		<tbody>
		#for $row in $data
			<tr>
				#if ("timestamp" == $options.orderby) then "<td>"+$time.strftime("%b %d, %y %H:%M:%S", $time.localtime($row.latest))+"</td>" else "" #
				<td><a title="$row.Client" href="javascript: parent.location='/?module=mod_layout&action=render_section&layout=network&ip=$row.Client&store_request=2';"><span time="$row.latest" class="resolved">$row.client_name</span></a></td>
				<td><a title="$row.Server" href="javascript: parent.location='/?module=mod_layout&action=render_section&layout=network&ip=$row.Server&store_request=2';"><span time="$row.latest" class="resolved">$row.server_name</span></a></td>
				<td>$tpl_capacity($row.agg_bytes)</td>
				<td><span class="port">$row.Port</span></td>
			</tr>
		#end for
		</tbody>
	</table>
</div>

<script>
	init_sort();
</script>

#else:
$message_bar("Could not find any data matching your search criteria.")
#end if
<!-- (\$Id$) -->