#import time
#from reportHelper import *

<!-- (\$Id$) -->

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

<style>

span.resolved {
	display: list-item;
	list-style-type: none;
	list-style-image: none;
	text-decoration: none;
}

span.unresolved {
	display: list-item;
	list-style: square inside url('assets/icons/unresolved.gif');
	text-decoration: none;
}

DIV.red {
	font-size: 11px;
	padding: 2px;
	background-color: #FF0000;
	color: White;
	vertical-align: absmiddle;
	margin-right: 2px;
	padding-left: 4px;
	display: inline;
}

DIV.green {
	font-size: 11px;
	padding: 2px;
	background-color: #00CC00;
	color: White;
	vertical-align: absmiddle;
	margin-right: 2px;
	padding-left: 4px;
	display: inline;
}

</style>

<script>



init_sort = function() {
		document.getElementById("report_table").className = 'sort-table';
		#if ('bytes' == $options.unit)
				#set $unit = "Number"
		#else
				#set $unit = "netmon_capacity"
		#end if
		var types = [#if ('timestamp' == $options.orderby) then "'netmon_date', " else ''#'ipv4_address', 'CaseInsensitiveString', '$unit', '$unit', '$unit'];
		var report = new SortableTable(document.getElementById("report_table"), types);
		report.sort(0#if ('timestamp' == $options.orderby) then ", true" else ""#);
}

</script>



<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Bandwidth Consumption Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	  </tr>

	</table>
	<br /><br />
</div>
<div class="noPrint">
	<div class="titlebar">
		Bandwidth Consumption Report
	</div>
	
	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page"> 
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_bandwidth_consumption');">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">

	</div>
</div>
#if ($varExists('data') and len($data) > 0)
	<div class="datagrid center">

	<table width="100%" border="0" cellpadding="3" cellspacing="0" id="report_table">
	<thead>
		<tr>
			#if ("timestamp" == $options.orderby) then "<td>When</td>" else "" #
			<td>IP Address</td>
			<td>Resolved Name</td>
			<td>In </td>
			<td>Out </td>
			<td>Total </td>
		</tr>
		</thead>
		<tbody>
	#for $row in $data
		<tr>
			<td>#if ('ip' == $options.group) then $row.host else $row.network + " to " + $row.broadcast #</td>
			<td>#if ('ip' == $options.group) then '<span time="'+$end+'" class="resolved">' + $row.hostname + '</span>' else $row.label#</td>
			<td>#if ('bytes' == $options.unit) then $row.in else $tpl_capacity($row.in)#</td>
			<td>#if ('bytes' == $options.unit) then $row.out else $tpl_capacity($row.out)#</td>
			<td>#if ('bytes' == $options.unit) then ($row.in+$row.out) else $tpl_capacity($row.in+$row.out)#</td>
		</tr>
	#end for
	</tbody>
	</table>

	</div>

<script>
init_sort();
</script>

#else

$message_bar("Could not find any data matching your search criteria")

#end if
<!-- Load Deltas: 1min->$timer.load_delta[0] - 5min->$timer.load_delta[1] - 15min->$timer.load_delta[2] -->
<!-- Actual Time: $timer.user sec -->