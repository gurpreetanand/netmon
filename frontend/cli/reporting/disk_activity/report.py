#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Disk Activity Report
# Created:   11/04/07 10:06:36 AM

# Imports
import os
import re
import sys
import math
import string
import os.path

# CLI Arguments Parser
from optparse import OptionParser

# Allow the import of helper classes and madnet DB access layer
sys.path.append("/apache/cli")
sys.path.append("/apache/cli/reporting")
from reportHelper import *

# Cheetah templating framework
from Cheetah.Template import Template

# Try to load the DB driver or abort
try:
  from madnet_db import *
except Exception, e:
  print e
  sys.exit(1)
  
# Connect to the database
try:
  db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
  db.set_autocommit(0)
except Exception, e:
  print "ERROR: %s" % e
  sys.exit(1)
  

class DiskActivityReport:
  """ Disk Activity Report"""
  # SQL query generated by the report manager
  _sql = ""
  # Internal data-structure containing report data
  _data = []
  # Time-range information for the current report
  _timestamps = []
  # Dictionary of filter SQL clauses
  _filters = {'limit': '', 'time': ''}

  def __init__(self, options, parser):
    """ Constructor """
    self.options = options
    self.parser = parser
    
    if "" == self.options.report_period:
      print "You must specify a reporting period."
      print parser.print_help()
      sys.exit(1)

    if None == self.options.id:
      print "You must specify a device identifier"
      print parser.print_help()
      sys.exit(1)
      
    if None == self.options.type:
      print "You must specify a partition type"
      print parser.print_help()
      sys.exit(1)
    else:
      self.options.type = self.options.type.lower()
    
    if True == self.options.background:
      self.print_verbose("Daemonizing...")
      detach()
    
    self._timestamps = generate_timestamps(self.options.report_period, 
                        self.options.start_date, 
                        self.options.end_date, 
                        self.options.start_time, 
                        self.options.end_time)
    self.generate_sql()
    self.process_sql()
    self.generate_report()
        
  def marshall_limit(self):
    if (self.options.limit):
      if ("all" != self.options.limit):
        self._filters['limit'] = "LIMIT %s" % self.options.limit
    else:
      self._filters['limit'] = "LIMIT 2000"
      
  def marshall_timestamps(self):
    time_list = []
    if (len(self._timestamps) > 0):
      for pair in self._timestamps:
        time_list.append("(b.timestamp BETWEEN %s::int AND %s::int)" % (pair['start'], pair['end']))
        self._filters['time'] = "AND (%s)" % "\nOR ".join(time_list)
        
        
  def generate_sql(self):
    self.marshall_limit()
    self.marshall_timestamps()
    
    # Those parameters depend on the partition type:
    log_table = "%s_server_log" % self.options.type
    
    
    unit = ("smb" == self.options.type) and "b.blocksize" or "1000"
    status_calc = "(COALESCE(b.total::float, 0)-COALESCE(b.available::float, 0))*%s" % unit
    total_calc = "(COALESCE(b.total::float,0)*%s)" % unit
    
    self._sql = """SELECT %s AS status, b.timestamp, %s AS total
    FROM %s b
    WHERE b.srv_id = %s
    %s
    AND %s > 0
    ORDER BY timestamp ASC        
    """ % (status_calc, total_calc, log_table, self.options.id, self._filters['time'],
        status_calc)
    
    self.print_verbose("Query: %s" % self._sql)
    
  def process_sql(self):
    
    # First, we're going to get the partition info...
    disk_table = "%s_servers" % self.options.type
    partition_field = ("smb" == self.options.type) and "a.share" or "partition"
    
    query = """SELECT a.ip, %s AS partition
    FROM %s a
    WHERE a.srv_id = %s
    LIMIT 1
    """ % (partition_field, disk_table, self.options.id)
    self.print_verbose("Device retrieval query:\n%s" % query)
    
    # Retrieve partition info
    try:
      part_res = db.getRow(query)
    except Exception, e:
      print "Unable to retrieve the specified device\n%s" % e
      sys.exit(1)
    
    # Run the main query
    try:
      data = db.select(self._sql)
    except Exception, e:
      print "Database Error: %s" % e
      sys.exit(1)
      
    
    ####################
    ## Scaling Logic: ##
    ####################
    
    # Scaling divider
    step = 1

    # Build the structured payload for the graph and calculate max/min/avg values
    payload = {'times': [], 'vals': []}
    maxval = avgval = 0.0
    
    if data:
      minval = data[step]['status']
        
      for i in range(step, len(data), step):
        row = data[i]
        payload['times'].append(row['timestamp'])
        payload['vals'].append(row['status'])
        maxval = max(maxval, math.ceil(row['total']))
        minval = min(minval, math.floor(row['status']))
        avgval += float(row['status'])
      avgval = avgval/len(payload['times'])

    
    
      # Create the main payload bundle
      report = {"type": "disk_activity", "ip": part_res['ip'], 
            "partition": part_res['partition'],
            "max": maxval, "min": minval, 
            "avg": avgval,"payload": payload,
            "timestamps": self._timestamps[0]}
      #print report
      self._data = report
    
  def generate_report(self):
    payload = json.dumps(self._data, cls=NetmonEncoder)
    if None != self.options.save:
      self.print_verbose("Writing to  %s" % self.options.save)
      f = open(self.options.save, "w")
      f.write(payload)
      f.close()
    else:
      print payload

  def print_verbose(self, str):
    if (self.options.verbose):
      print str



if __name__=='__main__':
  # Jump to the script's root
  # 20181217 - Fixed Error of No file or directory '' - Gurpreet Anand
  os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

  usage       = "Usage: %prog [-v --verbose]"
  version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
  description = "Disk Activity Report"
  parser = OptionParser(usage=usage, version=version, description=description)
  
  # Marshall time-range info
  register_timestamp_arguments(parser)
  
  parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Enable extra output")
  parser.add_option("-b", "--background", action="store_true", dest="background", default=False, help="Run in the background")
  parser.add_option("-f", "--format", action="store", dest="format", default="json", help="Output Format (HTML/XML/CSV)")
  parser.add_option("-s", "--save-as", action="store", dest="save", default=None, help="Save report output in the specified file")
  parser.add_option("-i", "--id", action="store", dest="id", default=None, help="Disk Identifier")
  parser.add_option("-t", "--type", action="store", dest="type", default=None, help="Partition Type (SMB/DF)")
  parser.add_option("--limit", action="store", dest="limit", help="Limit the number of returned rows")
  


  (options, args) = parser.parse_args()
  obj = DiskActivityReport(options, parser)
