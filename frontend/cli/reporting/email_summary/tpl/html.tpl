#import time
#from reportHelper import *

<!-- (\$Id$) -->

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	<tr>
	<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
	<td><div align="right"><h1>E-mail Summary Report</h1></div></td>
	</tr>
	<tr>
	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	</tr>

	</table>
	<br /><br />
</div>

<div class="noPrint">
	<div class="titlebar">
	Email Summary Report
	</div>

	<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page">
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_netmon_login');">
	</div>
</div>

#if ($varExists('data') and len($data) > 0)
<div class="datagrid center">

	<table class="sort-table" id="report_table" width="100%" border="0" cellpadding="3" cellspacing="0">
		<thead>
			<tr>
				<td>$options.reporttype.capitalize()</td>
				<td>Messages Received</td>
				<td>Messages Sent</td>
				<td>Average Size (Received)</td>
				<td>Average Size (Sent)</td>
				<td>Total Size (Received)</td>
				<td>Total Size (Sent)</td>
			</tr>
		</thead>
		<tbody>
		#for $row in $data
			<tr>
				<td>$row.group_identifier&nbsp;</td>
				<td>$row.recvmessages</td>
				<td>$row.sentmessages</td>
				<td>$tpl_capacity($row.avgrecvsize)</td>
				<td>$tpl_capacity($row.avgsentsize)</td>
				<td>$tpl_capacity($row.trecvsize)</td>
				<td>$tpl_capacity($row.tsentsize)</td>
			</tr>
		#end for
		</tbody>
	</table>
	
	</div>
	
	<script language="Javascript">
	var types = ['CaseInsensitiveString', 'Number', 'Number', 'Number', 'Number', 'Number', 'Number'];
	var report = new SortableTable(document.getElementById("report_table"), types);
	report.sort(1, true);
	</script>
	
#else
$message_bar("Could not find any data matching your search criteria")
#end if
