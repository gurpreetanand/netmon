#import time
#from reportHelper import *

<!-- (\$Id$) -->

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	<tr>
	<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
	<td><div align="right"><h1>Login Report</h1></div></td>
	</tr>
	<tr>
	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	</tr>

	</table>
	<br /><br />
</div>

<div class="noPrint">
	<div class="titlebar">
	Login Report
	</div>

	<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page">
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_netmon_login');">
	</div>
</div>

#if ($varExists('data') and len($data) > 0)
<div class="datagrid center">

	<table class="sort-table" id="report_table" width="100%" border="0" cellpadding="3" cellspacing="0">
		<thead>
			<tr>
				<td>User</td>
				<td>Date/Time</td>
				<td>IP Address</td>
				<td>Status</td>
			</tr>
		</thead>
		<tbody>
		#for $row in $data
			<tr>
				<td>$row.username&nbsp;</td>
				<td>$time.strftime("%b %d, %Y %H:%M:%S", $time.localtime($row.timestamp))</td>
				<td><a title="$row.ip" href="#" onClick="parent.location = '?module=mod_layout&action=render_section&layout=network&ip=$row.ip&store_request=1'">$truncate_reverse(20, $resolve_ip($row.ip))</a></td>
				<td><div class="#if ($row.status == 'Failed') then "cell_red" else "cell_green"#"><strong>$row.status</strong></div></td>
			</tr>
		#end for
		</tbody>
	</table>
	
	</div>
	
	<script language="Javascript">
	var types = ['CaseInsensitiveString', 'netmon_date', 'CaseInsensitiveString', 'CaseInsensitiveString'];
	var report = new SortableTable(document.getElementById("report_table"), types);
	report.sort(1, true);
	</script>
	
#else
$message_bar("Could not find any data matching your search criteria")
#end if