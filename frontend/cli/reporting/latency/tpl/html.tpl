#import time
#import urllib
#from reportHelper import *

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

#def getXML
#set $count=1
<graph yAxisMinValue='$data.min' yAxisMaxValue='$data.max' lineThickness='1' showAnchors='0' caption='Tracker Latency' subcaption='On $urllib.quote_plus($data.stats[0].name) ($data.stats[0].ip)' numberSuffix='ms' decimalPrecision='2' numdivlines='6' showAreaBorder='1' areaBorderColor='444444' numberPrefix='' canvasBgColor='E1E1E1' canvasbordercolor='888888' canvasBorderThickness='2' showNames='1' rotateNames='1' numVDivLines='25' vDivLineAlpha='30' showAlternateHGridColor='1' alternateHGridColor='C9C9C9' formatNumberScale='0' bgcolor='F1F1F1' animation='0' showShadow='0' lineThickness='3'>#slurp
<categories>#slurp
#for $row in $data.points
<category name='$time.strftime('%Y-%m-%d %H:%M', $time.localtime($row.timestamp))' #slurp
#if ($divmod($count,$data.step)[1] == 0) then "showName='1'" else "showName='0'" # />#slurp
#set $count += 1
#end for
</categories>#slurp
<dataset seriesname='Latency' color='0066CC' showValues='0' showAreaBorder='1' areaBorderColor='0066CC' >#slurp
#for $row in $data.points
<set value='$row.val' />#slurp
#end for
</dataset>#slurp
</graph>#slurp
#end def

#if ($varExists('data') and len($data) > 0)
<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Latency Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	  </tr>

	</table>
	<br /><br />
</div>

<div class="noPrint">
	<div class="titlebar">
		Latency Report
	</div>

	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page">
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_latency');">
	</div>
</div>

#if ($varExists('data.stats') and len($data.stats) > 0)
<div class="datagrid center">

	<table width="100%" border="0" cellpadding="3" cellspacing="0" id="report_table">
	<thead>
		<tr>
			<td>Service </th>
			<td>Max. Latency </td>
			<td>Min. Latency </td>
			<td>Avg. Latency </td>
		</tr>
		</thead>
		<tbody>
		#for $row in $data.stats
		<tr>
			<td><div title="$row.ip">$row.name</div></td>
			#if (0 == $row.max) then "<td style='background-color: #CC0000; color: #FFFFFF; font-weight: bold;'>N/A</td>" else "<td>"+$str($row.max)+" ms</td>"#
			#if (0 == $row.min) then "<td style='background-color: #CC0000; color: #FFFFFF; font-weight: bold;'>N/A</td>" else "<td>"+$str($row.min)+" ms</td>"#
			#if (0 == $row.average) then "<td style='background-color: #CC0000; color: #FFFFFF; font-weight: bold;'>N/A</td>" else "<td>"+$str($row.average)+" ms</td>"#
		</tr>
		#end for
		</tbody>
	</thead>
	</table>
	
<script language="Javascript">
document.getElementById("report_table").className = 'sort-table';
var types = ['CaseInsensitiveString', 'latency', 'latency', 'latency'];
var report = new SortableTable(document.getElementById("report_table"), types);
report.sort(0, true);
</script>
#end if

#if (('all' != $options.id) and $varExists('data.points') and len($data.points) > 0)
<div class="panel" align="center">
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="500" height="400">
				<param name="movie" value="/assets/charts/FC_2_3_MSLine_2.swf">
				<param name="quality" value="">
				<param name="bgcolor" value="#F1F1F1">
				<param name="FlashVars" value="&chartWidth=500&chartHeight=400&dataXML=$getXML()">
				<embed src="/assets/charts/FC_2_3_MSLine_2.swf" bgcolor="#F1F1F1" FlashVars="&chartWidth=500&chartHeight=400&dataXML=$getXML()" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="500" height="400"></embed>
        </object>
</div>
#end if

#end if
#if (not $varExists('data.stats') or (0 == len($data.stats)))
$message_bar("There have been no recorded latency above the tracker logging threshold for the specified time-range.")
#end if
