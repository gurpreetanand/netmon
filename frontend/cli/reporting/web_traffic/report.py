#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Web Traffic Report 
# Created:   13/02/07 10:27:24 AM

# Imports
import os
import re
import sys
import string
import os.path
import json

# CLI Arguments Parser
from optparse import OptionParser

# Allow the import of helper classes and madnet DB access layer
sys.path.append("/apache/cli")
sys.path.append("/apache/cli/reporting")
from reportHelper import *

# Cheetah templating framework
from Cheetah.Template import Template


# Try to load the DB driver or abort
try:
  from madnet_db import *
except Exception, e:
  print e
  sys.exit(1)
  
# Connect to the database
try:
  db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
  db.set_autocommit(0)
except Exception, e:
  print "ERROR: %s" % e
  sys.exit(1)

class Web_Traffic_Report:
  """Web Traffic Report """
  # SQL query generated by the report manager
  _sql = ""
  # Internal data-structure containing report data
  _data = []
  # Time-range information for the current report
  _timestamps = []
  # Dictionary of filter SQL clauses
  _filters = {'host': '', 'limit': '', 'time': '', 'keyword': ''}

  def __init__(self, options, parser):
    """ Constructor """
    self.options = options
    self.parser = parser
    
    if "" == self.options.report_period:
      print "You must specify a reporting period."
      print parser.print_help()
      sys.exit(1)
    
    if True == self.options.background:
      self.print_verbose("Daemonizing...")
      detach()
    
    self._timestamps = generate_timestamps(self.options.report_period, 
                        self.options.start_date, 
                        self.options.end_date, 
                        self.options.start_time, 
                        self.options.end_time)
    self.generate_sql()
    self.process_sql()
    self.generate_report()
    
  def marshall_host_filter(self):
    if (self.options.hostfilter):
      hf = self.options.hostfilter
      # Look for a user-specified IP address
      if ("select" == hf):
        if (self.options.host):
          self._filters['host'] = "AND (a.src_ip = '%s' OR a.dst_ip = '%s')" % (self.options.host, self.options.host)
        else:
          print "Error: Missing argument: --host"
          print self.parser.print_help()
          sys.exit(1)
      elif (hf == "all"):
        pass
      # Look for a keyword in the hostname
      elif (hf == "keyword"):
        if (self.options.keyword):
          self._filters['host'] = " AND (SELECT hostname FROM hosts WHERE (ip = a.src_ip OR ip = a.dst_ip) LIMIT 1) ILIKE '%%%s%%'" % (re.escape(self.options.keyword))
        else:
          print "Error: Missing argument: --keyword"
          sys.exit(1)
      # Use a host filter
      else:
        if (self.options.hostfilter):
          try:
            host_filter = netmon_filter("host")
          except Exception, e:
            print "Unable to extract host filter registry. %s" % e
            sys.exit(1)
            
          if hf in host_filter.keys():
            params = ["'%s'" % x for x in host_filter[hf]['params']['values']]
            params = ",".join(params)
            self._filters['host'] = "AND (a.src_ip IN (%s) OR a.dst_ip IN (%s))" % (params, params)
        else:
          print "Error: Missing argument: --host-filter"
          print self.parser.print_help()
          sys.exit(1)
          
  def marshall_limit(self):
    if (self.options.limit):
      if ("all" != self.options.limit):
        self._filters['limit'] = "LIMIT %s" % self.options.limit
    else:
      self._filters['limit'] = "LIMIT 500000"
      
  def marshall_timestamps(self):
    time_list = []
    if (len(self._timestamps) > 0):
      for pair in self._timestamps:
        time_list.append("(a.timestamp BETWEEN %s::int AND %s::int)" % (pair['start'], pair['end']))
        self._filters['time'] = "WHERE (%s)" % "\nOR ".join(time_list)
        
  def marshall_keyword(self):
    if (self.options.url_keyword):
      keys = ["'%%%%%s%%%%'" % re.escape(x.strip()) for x in self.options.url_keyword.split(",")]
      self._filters['keyword'] = " a.host_name ILIKE " + " OR a.host_name ILIKE ".join(keys)
      self._filters['keyword'] = "%s OR a.url ILIKE %s" % (self._filters['keyword'], " OR a.url ILIKE ".join(keys))
      self._filters['keyword'] = "AND (%s)" % self._filters['keyword']
      
        
  def generate_sql(self):
    self.marshall_host_filter()
    self.marshall_limit()
    self.marshall_timestamps()
    self.marshall_keyword()
    
    self._sql = """SELECT a.timestamp, a.dst_ip AS "server",
    a.src_ip AS "client", (SELECT hostname FROM resolve(a.src_ip, a.timestamp)) AS "client_host", a.host_name, a.url
    FROM web_traffic a
    %s
    %s
    %s
    ORDER BY a.timestamp ASC
    %s
    """ % (self._filters['time'], self._filters['host'], self._filters['keyword'], self._filters['limit'])
    
    self.print_verbose("Query: %s" % self._sql)
    
  def process_sql(self):
    try:
      data = db.select(self._sql)
    except Exception, e:
      print "Database Error: %s" % e
      sys.exit(1)
    
    rss_regex = [re.compile('.*atom.*', re.I), re.compile('.*xml.*', re.I), \
           re.compile('.*rss.*', re.I), re.compile('.*feed.*', re.I), \
           re.compile('.*rdf.*', re.I)]
    
    
    payload = {}
    for row in data:
      key = "%s_%s" % (row['client'], row['host_name'])
      if not payload.has_key(key):
        payload[key] = {'hits': [], 'rss': 0}
        fields = ['client', 'server', 'host_name', 'client_host']
        for field in fields:
          payload[key][field] = row[field]
      payload[key]['hits'].append({'url': row['url'], 'time': row['timestamp']})
      for regex in rss_regex:
        if regex.match("%s%s" % (row['host_name'], row['url'])):
          payload[key]['rss']+=1
    
    # Next, mark the records that have a fair ratio of RSS content
    payload = payload.values()
    for row in payload:
      ratio = float(0)
      if int(row['rss']) > 0:
        ratio = float(row['rss'])/float(len(row['hits']))
        if (ratio > float(0.5)):
          # Likely RSS
          row['rss'] = True
        else:
          # Likely not RSS
          row['rss'] = False
      else:
        # Definitely not RSS
        row['rss'] = False
        
    self._data = payload
    
  def generate_report(self):
    payload = json.dumps({'type': 'web_traffic', 'rows': self._data, 'timestamps': self._timestamps[0]})
    if None != self.options.save:
      self.print_verbose("Writing to  %s" % self.options.save)
      f = open(self.options.save, "w")
      f.write(payload)
      f.close()
    else:
      print payload
  
  def print_verbose(self, str):
    if (self.options.verbose):
      print str



if __name__=='__main__':
  # Jump to the script's root
  # 20181217 - Fixed Error of No file or directory '' - Gurpreet Anand
  os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

  usage       = "Usage: %prog [-v --verbose]"
  version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
  description = "Web Traffic Report "
  parser = OptionParser(usage=usage, version=version, description=description)
  
  # Marshall time-range info
  register_timestamp_arguments(parser)
  
  parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Enable extra output")
  parser.add_option("-b", "--background", action="store_true", dest="background", default=False, help="Run in the background")
  parser.add_option("-f", "--format", action="store", dest="format", default="json", help="Output Format (HTML/XML/CSV)")
  parser.add_option("-s", "--save-as", action="store", dest="save", default=None, help="Save report output in the specified file")
  parser.add_option("--host-filter", action="store", dest="hostfilter", default=None, help="Host Filter (select/all/keyword/any host filter")
  parser.add_option("--host", action="store", dest="host", help="Selected host IP (host-filter must be set to 'select'")
  parser.add_option("--keyword", action="store", dest="keyword", help="Hostname keyword (if host-filter is 'keyword')")
  parser.add_option("--url-keyword", action="store", dest="url_keyword", help="URL keyword")
  parser.add_option("--limit", action="store", dest="limit", help="Limit the number of returned rows")
  


  (options, parser) = parser.parse_args()
  obj = Web_Traffic_Report(options, parser)
    
