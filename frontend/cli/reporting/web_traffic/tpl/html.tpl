#import cgi
#import time
#import urllib
#from reportHelper import *

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

#set $div_id = 0
<!-- (\$Id$) -->


<script language="Javascript">

	toggleDiv = function(div_id) {
		style = document.getElementById(div_id).style.display;
		document.getElementById(div_id).style.display = (style == 'none') ? 'block' : 'none';
	}

</script>


<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Web Traffic Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	  </tr>

	</table>
	<br /><br />
</div>
<div class="noPrint">
	<div class="titlebar">
		Web Traffic Report
	</div>
	
	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page"> 
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_web_traffic');">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	</div>
</div>
#if ($varExists('data') and len($data) > 0)
<div class="datagrid">

	<table width="100%" class="sort-table" id="report_table" border="0" cellpadding="1" cellspacing="0">
		<thead>
			<tr>
				<td>Client </td>
				<td>Website</td>
			</tr>
		</thead>
		<tbody>
		#for $row in $data
		#set $div_id += 1
		<tr>
			<td valign="top"><div align="center"><a href="#" onClick="parent.location = '?module=mod_layout&action=render_section&layout=network&ip=$urllib.quote_plus($row.client)&store_request=2'" title="$row.client#if $row.client_host != $row.client then "->"+$cgi.escape($row.client_host) else $row.client# ">
				$cgi.escape($truncate_reverse(35, $row.client_host))</a></div></td>
			<td valign="top">
			
			<div class="expandingGroup" align="left"><nobr><img onClick="toggleSiblingDIV(this)" src="assets/icons/plus.gif" class="icon">#if ($row.rss == True) then "<img src='/assets/icons/rss.gif' class='icon' title='Possible RSS traffic' alt='Possible RSS traffic' />&nbsp;" else ""#<a href="#" onClick="javascript:toggleSiblingDIV(this)">http://$row.host_name</a></nobr></div><div class="closed">
				#for hit in $row.hits
				<img src="/assets/icons/page_white_link.png" class="icon"> <a href="http://$cgi.escape($row.host_name)$cgi.escape($hit.url)" target="_blank">$cgi.escape($row.host_name)$cgi.escape($truncate_middle(23, 15, $hit.url))</a>
				&nbsp;&nbsp;&nbsp;<span style="color: #999999">[$time.strftime("%b %d, %y %H:%M:%S", $time.localtime($hit.time))]</span>
				<br />
				#end for
			</div>
			</td>
			
		</tr>
		#end for
		</table>
</div>

<script language="Javascript">
init_sort = function() {
		document.getElementById("report_table").className = 'sort-table';

		var types = ['CaseInsensitiveString', 'CaseInsensitiveString'];
		var report = new SortableTable(document.getElementById("report_table"), types);
		report.sort(0);
}
init_sort();
</script>

#end if