#import cgi
#import time
#from reportHelper import *

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

<!-- (\$Id$) -->
	<script src="/assets/jscript/sortabletable.js"></script>
	<script src="/assets/jscript/sort_lib.js"></script>
<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Up/Downtime Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
	  </tr>

	</table>
	<br /><br />
</div>
<div class="noPrint">
	<div class="titlebar">
		Up/Downtime Report
	</div>
	
	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page"> 
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_up_downtime');">
	</div>
</div>
#if ($varExists('data') and len($data) > 0)
<div class="datagrid center">

	<table class="sort-table" id="report_table" width="100%" border="0" cellpadding="3" cellspacing="0">
		<thead>
		<tr>
			<td>Service Name </td>
			<td>Up Percentage </td>
			<td>Uptime </td>
			<td>Downtime </td>
		</tr>
		</thead>
		<tbody>
		#for $row in $data
		<tr>
				<td><div title="$cgi.escape($row.name) ($cgi.escape($row.ip))">$cgi.escape($row.name)</div></td>
				<td>$row.up_percent</td>
				<td>$format_time($row.uptime)</td>
				<td>$format_time($row.down)</td>
		</tr>
		#end for
		</tbody>
	</table>
	
	</div>
	
	<script language="Javascript">
	var types = ['CaseInsensitiveString', 'percentage', 'netmon_time', 'netmon_time'];
	var report = new SortableTable(document.getElementById("report_table"), types);
	report.sort(1, false);
	</script>
#end if
<!-- (\$Id$) -->
