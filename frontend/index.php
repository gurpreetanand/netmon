<?php

  require_once("api/config.inc.php");
  require_once("api/modules/core/classes/controler_facade.class.php");
  require_once("api/modules/core/classes/system_manager.class.php");

  $controler = new Controler_Facade;
  $system = new System_Manager;

  $registry = Registry::get_registry();

  $build = simplexml_load_file("/etc/netmon-build.xml");

  $registration = $controler->db->select('select * from netmon');
  $numDevices = $controler->db->get_row('select count_unique_ips()');
  $version = $build["version"];

  $buildString = $build["version"];

  foreach ($build->component as $component) {
    $buildString .= "-" . $component["version"];
  }

  $info = array(
    "registration" => $registration[0],
    "numDevices" => $numDevices['count_unique_ips'],
    "version" => "$version",
    "areDaemonsUp" => $system->areDaemonsUp(),
    "build" => $buildString,
    "patchLevel" => "" . $build->component[0]["version"][0],
    "sessionId" => $_SESSION['id'],
    "sysinfo" => array(
      "varDisk" => array(
        "free" => disk_free_space("/var"),
        "total" => disk_total_space("/var")
      ),
      "mainDisk" => array(
        "free" => disk_free_space("/"),
        "total" => disk_total_space("/")
      ),
      "ram" => $system->getMeminfo()
    )
  );

?>

<DOCTYPE html>
    <html lang="en">
    <head>
        <link rel="SHORTCUT ICON" href="assets/icons/fav.ico">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
        <link href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cosmo/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/jquery-tagit.css" rel="stylesheet">
        <link rel="stylesheet" href="/css/jstree.css" rel="stylesheet">
        <link href="/css/main.css?<?php print $buildString; ?>" rel="stylesheet">
        <title>Netmon <?php print $version; ?></title>
    </head>

    <body class="application">
        <div id="main-navbar" class="navbar navbar-inverse navbar-fixed-top netmon-nav" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"> Netmon <span class="label label-success"><?php print $version; ?></span></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav">
                  <li class="nav-home active"><a href="/"><i class="icon-home"></i> Home</a></li>
                  <li class="nav-networks"><a href="/networks"><i class="icon-sitemap"></i> Network Traffic</a></li>
                  <li class="nav-devices dropdown">
                    <a class="dropdown-toggle" href="/default_devices">Devices <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="/tree"><i class="icon-desktop"></i> Tree View</a></li>
                      <li><a href="/devices"><i class="icon-desktop"></i> Widget View</a></li>
                    </ul>
                  </li>

                  <li class="nav-webtrackers"><a href="/webtrackers"><i class="icon-link"></i> Web Trackers</a></li>
                  <li class="nav-reports"><a href="/reports"><i class="icon-bar-chart"></i> Reports</a></li>
              </ul>
              <ul class="nav navbar-nav pull-right hidden-xs">
                  <li class="nav-settings"><a href="/settings"><i class="icon-gear"></i> Settings</a></li>
                  <li><a href="/logout"><i class="icon-signout"></i> Logout</a></li>
              </ul>
            </div>
        </div>

        <div id="message-bar"></div>


        <div class="content">
          <div id="initial-spinner">
            <img src="/assets/spinner.gif">
          </div>
        </div>

        <div class="navbar navbar-inverse navbar-static-bottom netmon-nav visible-xs">
            <ul class="nav navbar-nav pull-right">
                <li><a href="/settings.html"><i class="icon-gear"></i> Settings</a></li>
                <li><a href="#"><i class="icon-signout"></i> Logout</a></li>
            </ul>
        </div>

        <script data-main="/main.js" src="/js/libs/require.min.js"></script>

        <script>
          window.Info = {};
          <?php
            print "window.Info = " . json_encode($info) . ";";
          ?>

          require.config({waitSeconds: 120, urlArgs: "<?php print $buildString; ?>"});

        </script>

    </body>
</html>
