<?php


/**
  * @author Xavier Spriet <xavier@netmon.ca>
  * @version  1.00
  * @access   public
  * @package  MADNET
  */


/* Runtime constants (Do not change these)            */

/**
  * This is the directory containing config.inc.php
  * @name ROOT
  */
define("ROOT",                               dirname(__FILE__));
/**
  * Turns debugging on or off
  * @name __DEBUG__
  */
define("__DEBUG_DEFAULT__",                  FALSE);
/**
  * If this is turned on, XDebug will generate a stack trace file in append mode
  * @name __STACK_TRACE__
  */
define("__STACK_TRACE__",                    FALSE);
/**
  * Turns Logging on or off
  * @name __LOGGING__
  */
define("__LOGGING__",                        FALSE);
/**
  * After what severity should we start logging
  * @name __LOGGING_THRESHOLD__
  */
define("__LOGGING_THRESHOLD__",              3);
/**
  * Select a back-end for logging events (SQL or TXT)
  * @name __LOGGING_BACKEND__
  */
define("__LOGGING_BACKEND__",                "SQL");
/**
  * Destination of event logs (SQL tablename or TXT filename)
  * @name __LOGGING_DEST__
  */
define("__LOGGING_DEST__",                   "log");


/* Paths information (Do NOT change these EVER)       */

/**
  * Root directory where new modules are created
  * @name MODS_PATH
  */
define("MODS_PATH",                          ROOT . "/modules/");



/* You might have to change this on your system */
/**
  * Path to the statically built PEAR repository and SMARTY include path.
  * @name INCLUDE_PATH
  */
define("INCLUDE_PATH",                          ROOT . "/include/");
ini_set("include_path",                         ".:" . INCLUDE_PATH .':'. ini_get('include_path'));

/* You might have to change this on your system */
/**
  * Path to the Smarty templating engine (smarty.class.php)
  * @name PEAR_ROOT
  */
#define("SMARTY",                             PEAR_ROOT . "smarty/libs/Smarty.class.php");
define("SMARTY",                              INCLUDE_PATH . "smarty/Smarty.class.php");

/* Misc Options                                       */

/**
  * Site configuration
  */
include_once(MODS_PATH . "core/conf/conf_site.inc.php");
/**
  * Database access driver settings
  */
include_once(MODS_PATH . "core/conf/conf_db.inc.php");
/**
  * Time constants
  */
include_once(MODS_PATH . "core/conf/conf_time.inc.php");
/**
  * Session configuration
  */
include_once(MODS_PATH . "core/conf/conf_session.inc.php");
/**
  * Core default values
  */
include_once(MODS_PATH . "core/conf/conf_defaults.inc.php");
/**
  * Encryption configuration
  */
include_once(MODS_PATH . "core/conf/conf_encryption.inc.php");
/**
  * Shared Memory configuration
  */
include_once(MODS_PATH . "core/conf/conf_shm.inc.php");
/**
  * SNMP Module configuration
  */
include_once(MODS_PATH . "core/conf/conf_snmp.inc.php");

# Dynamically include the configuration files of every installed modules.
# Please note that the modules must be named mod_* and the config files
# Must be placed in the /conf/ file in that module's root, and each config
# file must have the .conf extension

?>
