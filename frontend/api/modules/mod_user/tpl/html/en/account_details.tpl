<!-- {$smarty.template} ($Id$) -->

<div class="datagrid">

<table width="100%" border="0" cellspacing="0" cellpadding="3">
	{if $acm->get("first_name")}
	<tr>
		<td width="95"><div align="right"><strong>First Name:</strong></div></td>
		<td>{$acm->get("first_name")}</td>
	</tr>
	{/if}
	{if $acm->get("last_name")}
	<tr>
		<td><div align="right"><strong>Last Name:</strong></div></td>
		<td>{$acm->get("last_name")}</td>
	</tr>
	{/if}
	{if $acm->get("username")}
	<tr>
		<td><div align="right"><strong>Username:</strong></div></td>
		<td>{$acm->get("username")}</td>
	</tr>
	{/if}
	{if $acm->get("email")}
	<tr>
		<td><div align="right"><strong>Email Address:</strong></div></td>
		<td>{mailto address=$acm->get("email") encode="javascript"}</td>
	</tr>
	{/if}
	{if sizeof($acm->get("sub_account_group")) > 0}
	<tr>
		<td valign="top"><div align="right"><strong>Account Groups:</strong></div></td>
		<td>
		
			{foreach from=$subs.sub_account_group->get_groups() item="group" key="id"}
				{if (in_array($id, $acm->get("sub_account_group")))}
					{$group}<br />
				{/if}
			{/foreach}
		
		</td>
	</tr>
	{/if}
	{if $acm->get("pager_terminal")}
	<tr>
		<td><div align="right"><strong>Pager Terminal #:</strong></div></td>
		<td>
			{$acm->get("pager_terminal")}
		</td>
	</tr>
	{/if}
	{if $acm->get("pager_number")}
	<tr>
		<td><div align="right"><strong>Pager #:</strong></div></td>
		<td>
			{$acm->get("pager_number")}
		</td>
	</tr>
	{/if}
</table>

</div>

<!-- end of {$smarty.template} -->
