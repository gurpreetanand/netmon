<!-- {$smarty.template} ($Id$) -->
<div class="panel">
	<img src="assets/separator_double.gif" width="10" height="21" class="icon"> 
	<strong>Group Name:</strong>&nbsp; {$gm->get("group_name")}
</div>
<div class="datagrid">
<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td valign="top" width="30%"><div align="center"><strong>Permissions:</strong></div></td>
		<td>
		
		{assign var="all_perms" value=$subs.sub_group_permissions->get_perms_by_gid($gm->get("id"))}
		
		{if $all_perms}
		{foreach from=$all_perms item="category"}
			{if $category.perms}
				<strong>{$category.name}</strong><br />
				{foreach from=$category.perms item="perm"}
				- {$perm.name}<br />
				{/foreach}
			{/if}
		{/foreach}
		{/if}
		</td>
	</tr>
</table>

</div>

<!-- end of {$smarty.template} -->
