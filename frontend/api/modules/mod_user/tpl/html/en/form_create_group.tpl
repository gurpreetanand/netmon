<!-- {$smarty.template} ($Id$) -->

<form action="?module=mod_user&action=process_create_group&root_tpl=blank_panel" method="POST">
<div class="panel">
	<img src="assets/separator_double.gif" width="10" height="21" class="icon"> 
	Group name: <input type="text" {param name="group_name"} value="{$smarty.post.group_name}">
</div>
<div class="panel">
<p align="center"><strong>Available Permissions</strong></p>

	{assign var="all_perms" value=$subs.sub_group_permissions->get_all_perms()}
		
		{if $all_perms}
		{capture assign="size"}{array_size array=$all_perms}{/capture}
		<table width="100%" align="center" cellspacing="0" cellpadding="5">
			{section loop=$size/2 name="row"}
			<tr valign="top">
				{section loop=2 name="col"}
				{counter assign="idx"}{capture assign="offset"}{$idx-1}{/capture}
					<td valign="top"> 
					{if $size >= $offset}
						<strong>{$all_perms[$offset].name}</strong><br />
						{foreach from=$all_perms[$offset].perms item="perm"}
						<input type="checkbox" name="sub_group_permissions[]" value="{$perm.id}"> {$perm.name}<br />
						{/foreach}
					{else}
					&nbsp;
					{/if}
					</td>
				{/section} 
			</tr>
			{/section}
			
		</table>
		{/if}


		<p align="center">
			<input type="submit" value="Create Group" {param name="submit" type="button"}>
			<input type="reset" value="Reset Form" {param name="reset" type="button" doc="Reset this form back to default values"}>
		</p>

</div>
</form>

<!-- end of {$smarty.template} -->