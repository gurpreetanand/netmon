<!-- {$smarty.template} ($Id$) -->
<div class="panel">
	<img src="assets/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="assets/buttons/button_help.gif" class="icon" onClick="parent.showHelp('user_edit');" title="Help">
</div>
<div class="panel">
<form action="?module=mod_user&action=process_create_user" method="POST" name="form_create_user">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
	<tr>
		<td><div align="center"><b>First Name:</b><br />
		<input type="text" {param name="first_name"} value="{$smarty.post.first_name}"></div></td>
		
		<td><div align="center"><b>Last Name:</b><br />
		<input type="text" {param name="last_name"} value="{$smarty.post.last_name}"></div></td>
	</tr>

	<tr>
		<td><div align="center"><b>Username:</b><br />
		<input type="text" {param name="username"} value="{$smarty.post.username}"></div></td>
		
		<td><div align="center"><b>Password:</b><br />
		<input type="password" {param name="passwd"} value="{$smarty.post.passwd}"></div></td>
	</tr>

	<tr>
		<td><div align="center"><b>Pager Terminal #:</b><br />
			<input type="text" {param name="pager_terminal"} value="{$smarty.post.pager_terminal}"></div>
		</td>
	
		<td><div align="center"><b>Pager Number:</b><br />
		<input type="text" {param name="pager_number"} value="{$smarty.post.pager_number}"></div></td>
	</tr>
	
	<tr>
		<td><div align="center"><b>Email Address:</b><br />
		<input type="text" {param name="email"} value="{$smarty.post.email}"></div></td>
	
		<td><div align="center"><b>Account Group:</b><br />
			<select size="5" multiple="true" {param name="sub_account_group[]"}>
				{html_options options=$subs.sub_account_group->get_groups() selected=$smarty.post.sub_account_group}
			</select></div>
		</td>
	</tr>
</table>
	<p align="center">
		<input type="submit" value="Create Account" {param name="submit" type="button" doc="Create user account"}>
		<input type="reset" value="Reset Form" {param name="reset" type="button" doc="Reset this form back to default values"}>
	</p>
</form>
</div>
<!-- end of {$smarty.template} -->
