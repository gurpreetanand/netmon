<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("groups").className = 'sort-table';
	var types = ['CaseInsensitiveString', null];
	var report = new SortableTable(document.getElementById("groups"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="titlebar">
	Manage Account Groups
</div>
<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="assets/buttons/button_refresh.gif" width="24" height="24" class="icon" onClick="document.location.reload();" title="Refresh">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	{input type="button" value="Add New Group" onClick="parent.pnl_right.showEditor('?module=mod_user&action=form_create_group&root_tpl=blank_panel');" class="button"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="assets/buttons/button_help.gif" class="icon" onClick="parent.pnl_right.showHelp('manage_groups');" title="Help"> 
</div>


{if $ids}
<div class="datagrid center">
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" id="groups">
	<thead>
	<tr>
		<td>Group Name</td>
		<td>Actions</td>
	</tr>
	</thead>
{foreach from=$ids item="id"}

	{if $gm->pop($id.id)}
	{assign var="gm_id" value=$gm->get("id")}
	<tr>
		<td>{$gm->get("group_name")|capitalize|escape:"html"}</td>
		<td>

		<a href="#" onclick="parent.pnl_right.showEditor('?module=mod_user&action=group_details&id={$gm_id}&root_tpl=blank_panel');">Details</a> |
		<a href="#" onClick="parent.pnl_right.showEditor('?module=mod_user&action=edit_group&id={$gm_id}&root_tpl=blank_panel');">Edit</a> |
		<a href="#" onclick="if (confirm('Are you sure you wish to delete the group {$gm->get("group_name")|capitalize|escape:"javascript"|escape:"html"} ?')) {ldelim} parent.pnl_right.showEditor('?module=mod_user&action=delete_group&id={$gm_id}&root_tpl=blank_panel'); {rdelim}">Delete</a></td>
	</tr>
	{/if}
{/foreach}
</table>
</div>
{else}
{"There are currently no groups in the database."|message_bar}
{/if}

<!-- end of {$smarty.template} -->

