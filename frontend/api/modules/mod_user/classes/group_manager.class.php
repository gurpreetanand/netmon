<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * Group Managament class
  *
  * This class is a subclass of MadnetElement and is therefore a top-level
  * data element. It is used to manage user groups, and their association
  * with permissions.
  *
  * The system is pretty simple. First, you create permissions (through a front-end
  * if you have one, directly in the DB otherwise) and when you create a new group,
  * you can associate permissions to this group. Then when you want to create a user
  * account, simply select the group(s) that user should belong to, and the permissions
  * will be assigned and maintained automatically.
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class Group_Manager extends MadnetElement {

	
	/* Other attributes */
	
	/**
	  * Database table
	  *
	  * @var $table
	  * @access private
	  */
	var $table = "groups";
	/**
	  * Primary Key
	  *
	  * @var $pkey
	  * @access private
	  */
	var $pkey = "id";
	/**
	  * Module this element belongs to
	  *
	  * @var $module
	  * @access private
	  */
	var $module = "mod_user";
	/**
	  * Name of the element
	  *
	  * @var $element
	  * @access private
	  */
	var $element = __CLASS__;
	/**
	  * Meta structure
	  *
	  * @var $meta
	  * @access private
	  */
	var $meta;
	

	/**
	  * Initializes all primitive stack entries (and sub_elements) and all aggregated objects.
	  *
	  * @return Account_Manager
	  * @access public
	  */
	function init() {
		$this->params->add_primitive("group_name",     "string",           TRUE ,   "Group name",      "Name of the group"   );
		
		$this->params->add_sub("mod_user", "group_permissions", TRUE, "post");
		
		$this->debugger->add_hit("Instanciating " . __CLASS__);
	}
	
	/**
	  * Returns an array containing the user ID of every user account in the DB
	  *
	  * @return mixed
	  */
	function get_all_ids() {
		$query = "SELECT {$this->pkey} AS \"id\", group_name FROM {$this->table} ORDER BY group_name ASC";

		$result = $this->db->select($query);
		
		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			$this->err->err_from_string("Unable to retrieve a list of group ids");
			return FALSE;
		}
		return $result;
	}
	
	/**
	  * Fetches a user account from the DB
	  *
	  * This will eventually be replated by MadnetElement->fetch()
	  * once the API cools down. It is too unstable at the moment.
	  * This might see the light of day before Madnet 4.0
	  *
	  * @param integer id
	  * @return boolean
	  */
	function pop($id) {
		$query = "SELECT * FROM {$this->table} WHERE {$this->pkey} = " . $this->db->escape($id);
		
		$result = $this->db->get_row($query);
		
		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			$this->err->err_from_string("Unable to retrieve group information for group #{$id}");
			return FALSE;
		}
		
		foreach($result as $key => $value) {
			$this->params->setval($key, $value);
		}
		
		return TRUE;
	}
	
	/**
	  * Deletes all the user to group relationships associated with the group we are deleting
	  *
	  * This is the pre_delete hook in the MadnetElement superclass
	  * We are using it to clean up the user to groups relationship table.
	  *
	  * @param integer $id
	  * @return boolean
	  */
	   
	function pre_delete($id) {
		$query = "DELETE FROM user2groups WHERE group_id = " . $this->db->escape($id);
		
		$result = $this->db->delete($query);
		
		if (DB_QUERY_ERROR == $result) {
			return FALSE;
		} else {
			return TRUE;
		}
		
	}
	
}

?>