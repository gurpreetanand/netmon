<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  * Group to permission relationship manager
  *
  * This class handles the process of managing the relationship
  * between user groups and permission bits.
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class sub_group_permissions extends MadnetSubElement {

  /**
    * META structure of the MadnetElement object we are bound to
    *
    * @var $element
    * @access private
    */
  var $element;

  /**
    * Class constructor method. Instanciates all class attributes.
    *
    * @return sub_account_group
    * @access public
    */
  function sub_group_permissions() {
    parent::MadnetSubElement();

    $this->table = "permission2groups";
    $this->fkey = "group_id";
    $this->pkey = "permission_id";
    $this->mandatory = TRUE;
    $this->doc = "Permissions to assign to this entire user group";
    $this->friendly_name = "Permissions to group association";

    $this->debugger->add_hit(__CLASS__ . " instanciated");
  }

  /**
    * Validates the data found in the POST stack
    *
    * Creates a structure based on the POST stack filled with all group IDs.
    * It then ensures that none of the values in the structure are null or
    * non-integer, and return TRUE if everything is valid, FALSE otherwise.
    *
    * This provides a fine-grained control over SQL injection
    *
    * @return boolean
    */
  function validate() {
    $gids = $GLOBALS['json_post'][__CLASS__];

    if ((!is_array($gids)) && ($this->mandatory == TRUE)) {
      $this->err->err_from_string("The permission relationship is a mandatory field");
      return FALSE;
    }

    #$this->debugger->add_hit("GIDs (before): ", NULL, NULL, vdump($gids));
    for($i = 0; $i < sizeof($gids); $i++) {
      $gids[$i] = (intval($gids[$i]) > 0);
    }


    $this->debugger->add_hit("GIDs (after): ", NULL, NULL, vdump($gids));

    # No permissions is legal.
    if (!is_array($gids)) { return TRUE; }

    $success = ((intval($this->element['pkey_value']) > 0) && (!in_array(FALSE, $gids)));
    $this->debugger->add_hit("Validate returns " . ($success ? "true" : "false"));
    return $success;
  }

  /**
    * Returns a structure of all permissions.
    *
    * If a GID is passed, the "checked" field of each permission
    * structure will determine whether or not the specified GID
    * has that specific permission bit turned on.
    *
    * @param integer $gid
    * @return mixed
    */
  function get_all_perms($gid = null) {
    $query = "
    SELECT a.id, a.name, b.name AS \"category\"
    FROM permissions a, permission_categories b
    WHERE b.id = a.category_id
    ORDER BY b.id ASC";
    $result = $this->db->select($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      $this->err->err_from_string("Unable to fetch a list of available permissions");
      return FALSE;
    }

    $temp = array();
    $categories = array();

    foreach($result as $current) {
      if (!in_array($current['category'], array_keys($categories))) {
        $categories[$current['category']] = sizeof($temp);
        array_push($temp, array("name" => $current['category'], "perms" => array()));
      }
      array_push($temp[$categories[$current['category']]]['perms'], array("id" => $current['id'], "name" => $current['name']));
    }

    if ($gid) {
      $gid_perm_ids = $this->get_perm_ids_by_gid($gid);

      # OUCH!!!
      foreach($temp as $key => $val) {
        foreach($val['perms'] as $perm_key => $perm_val) {
          $temp[$key]['perms'][$perm_key]['checked'] = FALSE;
          if (is_array($gid_perm_ids)) {
            if (in_array($perm_val['id'], $gid_perm_ids)) {
              $temp[$key]['perms'][$perm_key]['checked'] = TRUE;
            }
          }
        }
      }

    }

    return $temp;

  }

  /**
    * Returns complete description of all perms associated to a specific group
    *
    * @param integer $gid
    * @return mixed
    */
  function get_perms_by_gid($gid) {
    $gid = $this->db->escape($gid);

    $query = <<<EOM
    SELECT a.id, a.name, b.name AS "category"
    FROM permissions a, permission_categories b
    WHERE b.id = a.category_id
    AND a.id IN (SELECT permission_id FROM permission2groups WHERE group_id = $gid)
    ORDER BY b.id ASC;
EOM;

    $result = $this->db->select($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      $this->err->err_from_string("Unable to retrieve a list permissions associated with group $gid");
      return FALSE;
    }

    $temp = array();
    $categories = array();

    foreach($result as $current) {
      if (!in_array($current['category'], array_keys($categories))) {
        $categories[$current['category']] = sizeof($temp);
        array_push($temp, array("name" => $current['category'], "perms" => array()));
      }
      array_push($temp[$categories[$current['category']]]['perms'], array("id" => $current['id'], "name" => $current['name']));
    }


    return $temp;

  }

  /**
    * Returns the ID of all permissions associated with a group
    *
    * @param integer $gid
    * @return mixed
    */
  function get_perm_ids_by_gid($gid) {
    $gid = $this->db->escape($gid);

    $query = <<<EOM
    SELECT a.id FROM permissions a
    WHERE a.id IN (SELECT permission_id FROM permission2groups WHERE group_id = $gid)
    ORDER BY a.id ASC;
EOM;

    $result = $this->db->select($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      $this->err->err_from_string("Unable to retrieve a list permissions associated with group $gid");
      return FALSE;
    }

    $tmp = array();
    foreach($result as $current) {
      array_push($tmp, $current['id']);
    }
    unset($result);

    return $tmp;
  }

  /**
    * Inserts the relationship in the database
    *
    * Builds a hash-table based on the group ID that was just created
    * (since the MadnetElement will update $this->element['pkey_value'] with that data)
    * and autoinserts it in the DB.
    *
    * @return boolean
    */
  function insert() {

    $success = TRUE;

    $id = intval($this->element['pkey_value']);
    $data = $GLOBALS['json_post'][__CLASS__];


    if ($id == 0) {
      $this->err->err_from_string("ID has not been passed by reference in " . __CLASS__, NULL, NULL, vdump($this->element));
      return FALSE;
    }

    foreach($data as $rel_id) {
      $auto = array($this->fkey => $id,
                $this->pkey => $rel_id);
      if (DB_QUERY_ERROR == $this->db->auto_insert($this->table, $auto)) {
        $success = FALSE;
      }
    }

    if (!$success) {
      # Undo
      $this->db->delete("DELETE FROM " . $this->table . " WHERE {$this->fkey} = " . $this->db->escape($id));
      return FALSE;
    }

    $this->debugger->add_hit(__FUNCTION__ . " returns " . vdump($success));
    return $success;


  }


  /**
    * Deletes all permission relationships with the specified group and re-creates them
    *
    * It is MUCH simpler to start from scratch and call $this->insert than to do a diff of
    * a resultset with POST data.
    *
    * @return boolean
    */
  function update() {
    $query = "DELETE FROM {$this->table} WHERE {$this->fkey} = {$this->element['pkey_value']}";

    $result = $this->db->delete($query);
    $this->debugger->add_hit("Result of $query", NULL, NULL, vdump($result));

    if (DB_QUERY_ERROR == $result) {
      $this->err->err_from_code(DB_QUERY_ERROR, "Unable to delete old records", $result->getMessage());
      return FALSE;
    }

    return $this->insert();

  }

  /**
    * Deletes all relationships pertaining to a user account
    *
    * @return boolean
    */
  function delete() {
    $query = "DELETE FROM {$this->table} WHERE {$this->fkey} = {$this->element['pkey_value']}";

    $res = $this->db->delete($query);


    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_string("Unable to sever relationship");
      return FALSE;
    }

    return TRUE;
  }
}

?>