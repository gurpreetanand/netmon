<report>
{foreach from=$data key="key" item="val"}
{if $key == 'event'}
	<schedule>
{foreach from=$val key="event_key" item="event_val"}
		<{$event_key}>{$event_val}</{$event_key}>
{/foreach}
	</schedule>
{elseif $key neq 'options'}
	<{$key}>{$val}</{$key}>
{else}
	{$val|replace:"--":""}
{/if}
{/foreach}
</report>

