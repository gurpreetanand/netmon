<?php


class Custom_Report_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "reports";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "report_id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_reports";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		$this->params->add_primitive("config_string", "string",          FALSE,   "Configuration");
		$this->params->add_primitive("report_type",   "string",           TRUE,   "Report Type");
		$this->params->add_primitive("report_name",   "string",           TRUE,   "Report Type");
	}



	/**
	  * Returns an array containing the user ID of every user account in the DB
	  *
	  * @return mixed
	  */
	function get_all_ids() {
		$query = "SELECT {$this->pkey}, report_name FROM {$this->table} ORDER BY report_name ASC";
		$result = $this->db->select($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			return $result;
		}
	}


	function pre_insert() {
		$name = $this->db->escape($this->params->getVal("report_name"));
		$query = "DELETE FROM reports WHERE report_name = $name";
		return $this->db->delete($query);
	}

	function pre_update($id) {
		return $this->pre_insert($id);
	}

	function build_config_string() {
		$junk = array("module", "action", "type", "report_name", "class");

		$string = "";

		foreach($_GET as $key => $val) {

			if (!in_array($key, $junk)) {

				if (strlen($val) >= 1) {
					$string .= "&$key=$val";
				}
			}
		}

		if (strlen($string) >= 1) {
			$string = substr($string, 1, strlen($string)-1);
		}


		$_POST['config_string'] = $string;
	}

	function pop($id) {
		$query = "SELECT report_type, report_name, config_string FROM reports WHERE {$this->pkey} = " . $this->db->escape($id);

		$res =  $this->db->get_row($query);

		if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
			return;
		}

		return $res;
	}

}


?>