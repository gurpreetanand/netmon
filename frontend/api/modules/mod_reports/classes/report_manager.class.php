<?php

class Report_Manager extends MadnetElement
{
  /**
    * Database table associated with this subclass
    *
    * @var $table
    * @access protected
    */
  var $table = "reports";
  /**
    * Name of the primary key in the table
    *
    * @var string $pkey
    * @access protected
    */
  var $pkey = "id";
  /**
    * Name of the module this MadnetElement subclass belongs to
    *
    * @var string $module
    * @access protected
    */
  var $module = "mod_reports";
  /**
    * Name of the class containing the domain logic for this Element
    *
    * @var string $element
    * @access protected
    */
  var $element = __CLASS__;

  function init() {
    $this->params->add_primitive("report_period", "string",  TRUE,  "Report Period");
    $this->params->add_primitive("report_type",   "string",  TRUE,  "Report Type");
    $this->params->add_primitive("start_date",    "string",  FALSE, "Start Date");
    $this->params->add_primitive("start_time",    "string",  FALSE, "Start Time");
    $this->params->add_primitive("end_date",      "string",  FALSE, "End Date");
    $this->params->add_primitive("end_time",      "string",  FALSE, "End Time");
    $this->params->add_primitive("format",        "string",  FALSE, "Format");
    $this->params->add_primitive("label",         "string",  TRUE,  "Label");
    $this->params->add_primitive("options",       "string",  FALSE, "Options");
    $this->params->add_primitive("event_id",      "integer", FALSE, "Event ID");
  }
  
  function get_custom_report_ids() {
    $query = "SELECT id, label, report_type FROM reports ORDER BY id ASC";
    $res = $this->db->select($query);
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    
    return $res;
  }
  
  function post_fetch($id) {
                /*
    $opts = array();
    
    foreach($this->get("options") as $key => $va) {
      $opts[str_replace("--", "", $key)] = $val;
    }
    
    $this->params->setval("options", $opts);
                */

    
    if ($this->get("event_id") != NULL) {
      require_class("core", "Event_Manager");
      $em = new Event_Manager();
      if ($em->fetch($this->get("event_id"))) {
        $this->params->setval("event", $em->params->build_autoinsert_array());
      }
    }
    
    return TRUE;
  }
  
  function pre_delete($id) {
    $query = "DELETE FROM events WHERE id = (SELECT event_id FROM reports WHERE id = " . $this->db->escape($id) . ")";
    return $this->db->delete($query);
  }
  
  
  function getAll() {
    $query = "SELECT r.event_id, r.report_period, r.report_type, r.label, r.id, e.schedule_hour FROM reports r LEFT JOIN events e ON e.id = r.event_id ORDER BY id ASC";
    $res = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    
    return $res;
  }

}

?>
