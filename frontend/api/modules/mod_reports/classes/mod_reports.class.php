<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    4.6
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


// mod_reports.class.php

/**
  * Reporting MCO
  *
  * This module manages custom reports and interfaces with the report manager model(s?)
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class mod_reports extends MadnetModule {

  /**
    * Initializes all module variables
    *
    * @return void
    */
  function init() {
    require_class($this->module, "report_manager");
  }

  function get_reports_tree(&$index_content) {
    $crm = new Report_Manager();
    $ids = $crm->get_custom_report_ids();

    $parser = new Parser($this->tpl_dir);
    $parser->assign("custom_reports", $ids);
    $parser->assign("dir_id", $this->_get_saved_reports_dir_id());
    $index_content .= $parser->fetch("reports_explorer.tpl");
    return "Reports Explorer";
  }
  
  function _get_saved_reports_dir_id() {
    $query = "SELECT id FROM fs_directories WHERE label ILIKE 'Saved Reports'";
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    return $res['id'];
  }
  
  function get_reports() {
    require_class("mod_vfs", "File_Manager");
    $fm = new File_Manager();

    $directorySaved = 5;
    
    $fm->sync_directory($directorySaved);
    
    $saved = $this->saved_reports();
    
    foreach ($saved as &$report) {
      $report['params'] = $this->get_report_parameters($report['id']);
    }

    $payload = array(
      "completed" => $fm->get_dir_files($directorySaved),
      "saved" => $saved
    );
    print json_encode($payload);
  }
  
  function get_builder_payload() {
    $payload = array();
    
    $query = "SELECT * FROM interfaces WHERE enable_logging='t'";
    $rows = $this->db->select($query);
    
    if (!is_array($rows)) {
      $rows = array();
    }
    
    $devices = array();
    
    foreach ($rows as &$row) {
      if (!$devices[$row['device_id']]) {
        $query = 'SELECT * FROM devices WHERE id=' . $row['device_id'];
        $device = $this->db->select($query);
        if ($device[0]) {
          $device = $device[0];
          $devices[$row['device_id']] = $device;
          $devices[$row['device_id']]['interfaces'] = array();
        }
      }
      array_push($devices[$row['device_id']]['interfaces'], $row);
    }
    
    $netflow = array();
    foreach ($devices as $device) {
      array_push($netflow, $device);
    }
    
    $payload['netflow'] = $netflow;

    $payload['hostFilters'] = $this->read_filter('host');
    $payload['trafficFilters'] = $this->read_filter('traffic');
    
    $query = "SELECT i.*, d.label AS device_label FROM interfaces i, devices d WHERE i.enable_shm='t' and i.device_id = d.id";
    $flow_interfaces = $this->db->select($query);
    if (!is_array($flow_interfaces)) {
      $flow_interfaces = array();
    }          
    $payload['netflowInterfaces'] = $flow_interfaces;
    
    $query = "SELECT * FROM localnets";
    $localnets = $this->db->select($query);
    if (!is_array($localnets)) {
      $localnets = array();
    }          
    $payload['localnets'] = $localnets;
    
    $query = "SELECT *, 'df' as type FROM df_servers";
    $res = $this->db->select($query);
    if (!is_array($res))
      $res = array();
    $df = $res;
    
    $query = "SELECT *, 'smb' as type FROM smb_servers";
    $res = $this->db->select($query);
    if (!is_array($res))
      $res = array();
    $smb = $res;

    $query = "SELECT * FROM urls";
    $res = $this->db->select($query);
    if (!is_array($res))
      $res = array();
    $payload['urls'] = $res;

    $query = "SELECT * FROM events";
    $res = $this->db->select($query);
    if (!is_array($res))
      $res = array();
    $payload['events'] = $res;

    $query = "SELECT * FROM servers WHERE protocol='ICMP'";
    $res = $this->db->select($query);
    if (!is_array($res))
      $res = array();
    $payload['latency'] = $res;

    $query = "SELECT a.*, b.ip_address, b.label AS device_label
      FROM oids a, devices b
      WHERE a.device_id = b.id
      ORDER BY a.label ASC, a.oid ASC";
    $res = $this->db->select($query);
    if (!is_array($res))
      $res = array();

    $payload['oids'] = $res;
    $payload['disks'] = array_merge($smb, $df);

    $payload['emails'] = $this->db->select("SELECT first_name, last_name, email, id FROM users");

    print json_encode($payload);
    return;
  }
  
  function read_filter($filter = null) {
    if ($filter == null) {
      $filter = 'host';
    }

    $file = "/var/www/registry/filters/" . $filter . ".xml";

    $dom = new DOMDocument();
    $dom->load($file);
    $xpath = new DOMXpath($dom);
    $filters = $xpath->query("//filter");

    $payload = array();

    foreach ($filters as $element) {
      array_push($payload, $element->getAttribute('name'));
    }

    return $payload;
  }

  function form(&$index_content) {
    
    $parser = new Parser($this->tpl_dir);
    
    
    if (!empty($_GET['id'])) {
      $rm = new Report_Manager();
      $rm->fetch($_GET['id']);
      $report = $rm->params->build_autoinsert_array();
    } else {
      $report = array();
    }
    
    $parser->assign("report", $report);
    
    $parser->assign("type", stripslashes(trim($_GET['type'])));
    $index_content .= $parser->fetch("report_form.tpl");
    return "Report Input Form";

  }

  function process() {
    $GLOBALS['json_post'] = $input = json_decode(file_get_contents("php://input"), true);

    $denied = array("portscan", "web_traffic", "netmon_login", "email_traffic");

    if ((has_perm("Guest/Demo Account")) && (in_array($_GET['type'], $denied))) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
      return FALSE;
    }

    $cmd = "python /apache/cli/reporting/" . $_GET['type'] . "/report.py";

    $options = $input['options'];

    foreach($options as $key => $val) {
      if (strcmp('b', $key) == 0) {
        $background = TRUE;
      }
      // escapeshellarg and escapeshellcmd force escape "|", which we need for email traffic report.
      $cmd .= " --". $key . " " . str_replace('\|', '|', escapeshellarg($val));
    }

    $this->debugger->add_hit("Report Command: ", NULL, NULL, $cmd);

    if (TRUE == $background) {
      print '';
      $status = proc_close(proc_open($cmd, array(), $foo));
    } else {
      print `$cmd`;
    }
  }


  function save() {
    if (!$GLOBALS['json_post']) {
      $GLOBALS['json_post'] = json_decode(file_get_contents("php://input"), true);
    }

    $rm = new Report_Manager();
    
    $opt_xml = "";    
    
    foreach($GLOBALS['json_post']['options'] as $key => $val) {
      $opt_xml .= "<option key='--" . $key . "' val='" . $val . "' />";
    }

    $GLOBALS['json_post']['options'] = "<options>".$opt_xml."</options>";

    if ($rm->insert()) {
      return true;
    } else { 
      return false;
    }
  }
  
  function schedule() {
    require_class("core", "Event_Manager");
    $em = new Event_Manager();

    if (!$GLOBALS['json_post']) {
      $GLOBALS['json_post'] = json_decode(file_get_contents("php://input"), true);
    }

    if ($em->insert()) {
      $GLOBALS['json_post']['event_id'] = $em->last_insert_id();
      if ($this->save()) {
        print '{}';
      } else { 
        $em->delete($GLOBALS['json_post']['event_id']);
        $this->err->err_from_code(400, "Unable to save/scheduled this report. Please contact the Netmon support helpdesk for assistance.");
      }
    }
  }
  
  function delete(&$index_content) {
    require_class($this->module, "Report_Manager");
    $rm = new Report_Manager();

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("Invalid Report Identifier");
      return;
    }
    
    // Delete event
    if ($rm->fetch($id)) {
      $event_id = $rm->get('event_id');
      if ($event_id) {
        require_class("core", "Event_Manager");
        $em = new Event_Manager();
        $em->delete($event_id);
      }
    }

    if ($rm->delete($id)) {
      print '{}';
    } else { 
      $this->err->err_from_code(400, "Unable to delete report");
    }
  }
  
  function get_report_parameters($id) {
    if ($id <= 0) {
      return array();
    }
    
    $rm = new Report_Manager();
    if ($rm->fetch($id)) {
      return $rm->params->build_autoinsert_array();
    } else {
      return array();
    }
  }
  
  function update_report() {
    $GLOBALS['json_post'] = json_decode(file_get_contents("php://input"), true);

    $id = intval($_GET['id']);
    if ($id <= 0) {
      $this->err->err_from_code(400, "Invalid report identifier. Aborting");
    }
    
    $opt_xml = "";    
    $options = explode(",", $GLOBALS['json_post']['options']);    
    
    foreach($options as $option) {
      list($key, $val) = explode('=', $option);    
      if (strcmp("", $key) != 0) {
        $opt_xml .= "<option key='--" . $key . "' val='" . $val . "' />";
      }
    }    
    
    $GLOBALS['json_post']['options'] = "<options>$opt_xml</options>";
    
    $rm = new Report_Manager();
    
    if (!$rm->fetch($id)) {
      $this->err->err_from_code(400, "Unable to retrieve the specified report. Aborting.");
      return;
    }

    if ($rm->get('event_id') != NULL) {
      require_class("core", "event_manager");
      $em = new Event_Manager();

      if (!$GLOBALS['json_post']['event_type'] || strcmp($GLOBALS['json_post']['event_type'], '') == 0) {
        if ($em->delete($rm->get('event_id'))) {
          //$index_content .= message_bar("Scheduled Event Deleted Successfully");
          $rm->setBit($id,'event_id','NULL');
        } else {
          if ($em->update($rm->get('event_id'))) {
            //$index_content .= message_bar("Scheduled Event Updated Successfully");
          }
        }


      }
    } elseif ($GLOBALS['json_post']['event_type'] && strcmp($GLOBALS['json_post']['event_type'], '') != 0) {
      require_class("core", "event_manager");
      $em = new Event_Manager();
      if ($em->insert()) {
        $GLOBALS['json_post']['event_id'] = $em->last_insert_id();
      }
    }
    
    // Remove the values that don't belong in this object for updating
    unset($rm->params->primitives['id']);
    unset($rm->params->primitives['event']);
    $this->debugger->add_hit("Report Manager:", NULL,NULL, vdump($rm->params));

    if ($rm->update($id)) {
      $em = new Event_Manager();

      if ($em->update($rm->get('event_id'))) {
        print '{}';
      } else {
        $this->err->err_from_code(400, "Unable to update report schedule");
      }

    } else {
      $this->err->err_from_code(400, "Unable to update report");
    }
  }
  
  /**
   * Report management action
   *
   */
  function saved_reports() {
    $rm = new Report_Manager();
    $reports = $rm->getAll();
    
    if (FALSE == $reports) {
      $reports = array();
    }
    
    return $reports;
  }
  
    # Private Smarty modifier
    function _resolve_colorblock($port_num)
    {
        if($port_num <= 25)    return 1;
        if($port_num <= 50)    return 2;
        if($port_num <= 75)    return 3;
        if($port_num <= 100)   return 4;
        if($port_num <= 150)   return 5;
        if($port_num <= 250)   return 6;
        if($port_num <= 500)   return 7;
        if($port_num <= 1000)  return 8;
        if($port_num <= 5000)  return 9;
        #if($port_num <= 65536) return 10;
        return 10;
    }
  
  
  
}


?>
