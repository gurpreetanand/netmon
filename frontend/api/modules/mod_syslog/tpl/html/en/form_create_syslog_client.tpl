<!-- {$smarty.template} ($Id$) -->
<script>
parent.document.getElementById("Netmon_Syslog_Manager").innerHTML = "Add New SYSLOG Client";
</script>
<div class="panel">
<form action="?module=mod_syslog&action=process_create_syslog_client" method="POST">
<table width="100%" cellpadding="4" cellspacing="0" align="center">
	<tr>
		<td>Client IP Address:</td>
		<td><input type="text" value="{$smarty.post.ip}" {param name="ip"}></td>
	</tr>
	<tr>
		<td>Facility:</td>
		<td>
		<select name="facility">
			<option value="-1">ALL</option>
			{html_options options=$facilities selected=$smarty.post.facility}
		</select>
		</td>
	</tr>
	<tr>
		<td>Severity:</td>
		<td>{html_options options=$severities name="severity" selected=$smarty.post.severity}</td>
	</tr>
	<tr>
		<td align="center" colspan="2">{input type="submit" class="button" value="Add Client"}</td>
	</tr>
</table>
</form>
</div>

<!-- end of {$smarty.template} -->
