<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.treeview"}
{literal}
<script>
var tree;
function treeInit() {
	tree = new YAHOO.widget.TreeView("treeDiv1");
	var root = tree.getRoot();

   var myobj = { label: "<img src=\"assets/icons/device.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Manage Syslog Clients", id:"clients", href:"?module=mod_syslog&action=manage_clients", target:"pnl_middle" } ;
   var clients = new YAHOO.widget.TextNode(myobj, root, false);
{/literal}

{if $items}
	{foreach from=$items item="entry"}
		{counter assign="entry_id"}
		
		   var myobj = {ldelim} label: "<img src=\"assets/icons/{if (($smarty.get.explore_by|lower == "severity") or ($smarty.get.explore_by == ""))}syslog_severity_{$entry.id}.gif{else}{$smarty.get.explore_by|lower}.gif{/if}\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> {$entry.val}", id:"{$entry.val}", href:"?module=mod_syslog&action=view_syslog_data&filter={$smarty.get.explore_by|lower|default:severity}&id={$entry.id}", target:"pnl_middle" {rdelim} ;
		   var tree_{$entry_id} = new YAHOO.widget.TextNode(myobj, root, false);
	{/foreach}
{/if}

{literal}

   tree.onExpand = function(node) {
      //alert(node.data.id + " was expanded");
   }

   tree.onCollapse = function(node) {
      //alert(node.data.id + " was collapsed");
   }

   tree.draw();
   
}
</script>
{/literal}




<div id="treeDiv1" class="tree" style="margin-left: 9px;"></div>
<script>treeInit();</script>




<!-- end of {$smarty.template} -->
