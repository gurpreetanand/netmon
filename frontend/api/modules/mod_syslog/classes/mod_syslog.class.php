<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


// mod_syslog.class.php

/**
  * Syslog MCO
  *
  * This module is used to perform any application-level change and management
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class Mod_Syslog extends MadnetModule {


  /**
    * Data structure used for caching of NS lookups
    */
  var $cache = array();

  /**
    * Initializes all module variables
    *
    * @return void
    */
  function init() {
    $this->facilities = array("KERN", "USER", "MAIL", "DAEMON", "AUTH",
                  "SYSLOG", "LPR", "NEWS", "UUCP", "CRON",
                  "AUTHPRIV", "FTP", "NTP", "LOGAUDIT",
                  "LOGALERT", "CRON", "LOCAL0", "LOCAL1",
                  "LOCAL2", "LOCAL3", "LOCAL5", "LOCAL6",
                  "LOCAL7");

    $this->severities = array("EMERGENCY", "ALERT", "CRITICAL", "ERROR",
                  "WARNING", "NOTICE", "INFO", "DEBUG");
  }


  function __get_facility_by_id($id) {
    if ($id == -1) { return "ALL"; }
    return $this->facilities[$id] ? $this->facilities[$id] : "Unknown";
  }


  function __get_severity_by_id($id) {
    if ($id == -1) { return "ALL"; }
    return $this->severities[$id] ? $this->severities[$id] : "Unknown";
  }

  function _get_syslog_clients() {
    $query = "SELECT syslog_id, ip, facility, severity FROM syslog_access ORDER BY ip ASC";
    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }

    return $res;
  }

  # WARNING: Smarty format only
  function __get_syslog_clients_smarty() {
    $res = $this->_get_syslog_clients();

    $return = array();

    if ((FALSE != $res) && (count($res) >= 1) && (is_array($res))) {
      foreach($res as $current) {
        $return[$current['syslog_id']] = $current['ip'];
      }
    }

    $this->debugger->add_hit("Syslog Clients", NULL, NULL, vdump($res));
    
    return $return;
  }


  function __generate_tpl_data_structure($array) {
    $res = array();
    foreach(array_keys($array) as $current) {
      array_push($res, array('id' => $current, 'val' => $array[$current]));
    }
    return $res;
  }

  function explore(&$index_content) {
    $parser = new Parser($this->tpl_dir);

    switch($_GET['explore_by']) {
      case "facility": $parser->assign_by_ref("items", $this->__generate_tpl_data_structure($this->facilities)); break;
      case "ip"      : $parser->assign_by_ref("items", $this->__generate_tpl_data_structure($this->__get_syslog_clients_smarty())); break;
      default        : $parser->assign_by_ref("items", $this->__generate_tpl_data_structure($this->severities)); break;
    }

    $index_content .= $parser->fetch("syslog_explorer.tpl");
  }

  function view_syslog_data(&$index_content) {

    $query = "SELECT * FROM syslog WHERE true ";


    # ASSUMPTION: There cannot be a start date without an end date specified

    if ($_POST['start_date']) {
      $_GET['start_date'] = $_POST['start_date'];
      $_GET['end_date']   = $_POST['end_date'];
    }

    if ($_GET['start_date']) {
      $date = $_GET['start_date'];
      $time = $_POST['start_time'];
      $start = $this->db->escape(mktime($time['Time_Hour'], $time['Time_Minute'], 0, $date['Date_Month'], $date['Date_Day'], $date['Date_Year']));
      
      
      $date = $_GET['end_date'];
      $time = $_POST['end_time'];
      $end = $this->db->escape(mktime($time['Time_Hour'], $time['Time_Minute'], 59, $date['Date_Month'], $date['Date_Day'], $date['Date_Year']));

      # Add a query clause
      $query .= "AND timestamp BETWEEN $start AND $end ";
    }

    # This is for the syslog explorer
    if ($_GET['filter']) {

      if (strtolower($_GET['filter']) == "ip") {
        $clients = $this->__get_syslog_clients_smarty();
        $_GET['id'] = $clients[$_GET['id']] ? $clients[$_GET['id']] : 0;
      }

      $query .= "AND " . $this->db->quote_key($_GET['filter']) . " = " . $this->db->escape($_GET['id']);
    }
    
    if ($_GET['ip']) {
      $query .= "AND ip = " . $this->db->escape($_GET['ip']) . "\n";
    }


    /***************************************************************************
      * This section is used for the search form
      *
      */

    if (is_array($_POST['facility'])) {
      $seq = "";

      foreach($_POST['facility'] as $current) {
        $seq .= $this->db->escape($current) . ", ";
      }

      $seq = substr($seq, 0, strlen($seq)-2);
      $query .= " AND facility IN ($seq) ";
    }

    if (is_array($_POST['severity'])) {
      $seq = "";

      foreach($_POST['severity'] as $current) {
        $seq .= $this->db->escape($current) . ", ";
      }

      $seq = substr($seq, 0, strlen($seq)-2);
      $query .= " AND severity IN ($seq) ";
    }

    if (is_array($_POST['clients'])) {
      $clients = $this->__get_syslog_clients_smarty();
      $seq = "";

      foreach($_POST['clients'] as $current) {
        $seq .= $this->db->escape($clients[$current]) . ", ";
      }

      $seq = substr($seq, 0, strlen($seq)-2);
      $query .= " AND ip IN ($seq) ";
    }

    if ($_POST['key']) {
      $key = $this->db->escape($_POST['key']);

      $query .= " AND (check_regex($key, message) = 1) ";
    }
    
    $limit = $_POST['limit'] ? intval($_POST['limit']) : 500;
    $query .= " ORDER BY timestamp DESC LIMIT $limit";

    /**
      * End of search form handling code
      **************************************************************************
      */


    

    $res = $this->db->select($query);

    if ((DB_NO_RESULT == $res)) {
      $res = null;
    }

    $parser = new Parser($this->tpl_dir);
    $parser->register_modifier("get_facility_by_id", array(&$this, "__get_facility_by_id"));
    $parser->register_modifier("get_severity_by_id", array(&$this, "__get_severity_by_id"));
    $parser->assign_by_ref("entries", $res);

    # The Dashboard module has an IP resolver for the top activity panels so we will let smarty use it.
    require_module("mod_dashboard");
    $dashboard = new mod_dashboard();

    $parser->register_modifier("resolve_ip", array(&$dashboard, "resolve_ip"));

    $index_content .= $parser->fetch("view_syslog_data.tpl");
  }

  function form_search(&$index_content) {
    $parser = new Parser($this->tpl_dir);

    $parser->assign_by_ref("facilities", &$this->facilities);
    $parser->assign_by_ref("severities", &$this->severities);
    $parser->assign_by_ref("clients", $this->__get_syslog_clients_smarty());
    $limits = array();
    
    $limits[100] = 100;
    for($i = 0; $i <= 10000; $i += 500) {
      $limits[$i] = $i;
    }
    
    unset($limits[0]);
    $parser->assign_by_ref("limits", $limits);

    $index_content .= $parser->fetch("form_search.tpl");
  }


  function delete_syslog_entries() {
    if (is_array($_POST['msg_id'])) {
      $seq = '';
      foreach($_POST['msg_id'] as $current) {
        $seq .= $this->db->escape($current) . ", ";
      }
      $query = "DELETE FROM syslog WHERE msg_id IN (" . substr($seq, 0, strlen($seq)-2) . ")";

      $res = $this->db->delete($query);
      if ((DB_QUERY_ERROR == $res) || (DB_MISC_ERROR == $res)) {
        return FALSE;
      }
    }
    print "{}";
  }


  function manage_clients(&$index_content) {
    $clients = $this->_get_syslog_clients();
    $parser = new Parser($this->tpl_dir);

    # The Dashboard module has an IP resolver for the top activity panels so we will let smarty use it.
    $dashboard = require_module("mod_dashboard");
    $parser->register_modifier("resolve_ip", array(&$dashboard, "resolve_ip"));
    $parser->register_modifier("resolve_severity", array(&$this, "__get_severity_by_id"));
    $parser->register_modifier("resolve_facility", array(&$this, "__get_facility_by_id"));

    $parser->assign_by_ref("clients", &$clients);
    $index_content .= $parser->fetch("manage_clients.tpl");
  }

  function form_create_syslog_client(&$index_content) {
    $parser = new Parser($this->tpl_dir);

    require_class($this->module, "syslog_manager");
    $sm = new Syslog_Manager();

    $parser->set_params($sm->params);
    $parser->assign_by_ref("sm", &$sm);
    $parser->assign_by_ref("facilities", &$this->facilities);
    $parser->assign_by_ref("severities", &$this->severities);


    $index_content .= $parser->fetch("form_create_syslog_client.tpl");
  }

  function form_edit_syslog_client(&$index_content) {
    $parser = new Parser($this->tpl_dir);

    require_class($this->module, "syslog_manager");
    $sm = new Syslog_Manager();

    $query = "SELECT * FROM syslog_access WHERE syslog_id = ". $this->db->escape($_GET['syslog_id']);

                $res = $this->db->select($query);

                if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
                        return FALSE;
                }

    $parser->set_params($sm->params);
    $parser->assign_by_ref("sm", &$sm);
    $parser->assign_by_ref("facilities", &$this->facilities);
    $parser->assign_by_ref("severities", &$this->severities);

    $parser->assign('clientid', intval($_GET['syslog_id']));
    $parser->assign('clientip', $res[0]['ip']);
    $parser->assign('clientfacility', $res[0]['facility']);
    $parser->assign('clientseverity', $res[0]['severity']);

    $index_content .= $parser->fetch("form_edit_syslog_client.tpl");
  }

  function process_create_syslog_client(&$index_content) {
    require_class($this->module, "syslog_manager");
    $sm = new Syslog_Manager();

    if ($sm->insert()) {
      $parser = new Parser($this->tpl_dir);
      $parser->assign("item", "Syslog Client");
      $parser->assign("action_taken", "created");
      $index_content .= $parser->fetch("item_modified.tpl");
    }
  }

  function process_edit_syslog_client(&$index_content) {
    require_class($this->module, "syslog_manager");
    $sm = new Syslog_Manager();
    $id = intval($_GET['syslog_id']);

    if ($sm->update($id)) {
      $parser = new Parser($this->tpl_dir);
      $parser->assign("item", "Syslog Client");
      $parser->assign("action_taken", "updated");
      $index_content .= $parser->fetch("item_modified.tpl");
    }
  }


  function delete_syslog_client(&$index_content) {
    require_class($this->module, "syslog_manager");
    $sm = new Syslog_Manager();
    $id = intval($_GET['syslog_id']);

    if ($id <= 0) {
      $this->err->err_from_string("A client ID was not passed along with this request.");
      return "Error";
    }

    if ($sm->delete($id)) {
      $parser = new Parser($this->tpl_dir);
      $parser->assign("item", "Syslog Client");
      $parser->assign("action_taken", "deleted");
      $index_content .= $parser->fetch("item_modified.tpl");
    }
  }



}

?>
