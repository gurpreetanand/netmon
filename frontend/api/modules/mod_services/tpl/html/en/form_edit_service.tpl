<!-- {$smarty.template} ($Id$) -->
{literal}
<script language="Javascript">
	function set_port_value() {
		if (document.forms['edit_service'].elements['protocol'].value == 'ICMP') {
			document.forms['edit_service'].elements['port'].value = '-1';
		}
	}
	
	function set_down_threshold() {
		dropdown = document.forms['edit_service'].elements['log_timeout'];
		timeout  = document.forms['edit_service'].elements['timeout'];
		
		dropdown.options[dropdown.length-1].value = timeout.value * 60000;
	}
	
	function toggle_port_status() {
		type_field = document.forms['edit_service'].elements['protocol'];
		type_val = type_field.options[type_field.selectedIndex].value;
		if (type_val == "ICMP") {
			document.forms['edit_service'].elements['port'].disabled = true;
			document.forms['edit_service'].elements['port'].className = "disabled";
		} else {
			document.forms['edit_service'].elements['port'].disabled = false;
			document.forms['edit_service'].elements['port'].className = "enabled";
			if (document.forms['edit_service'].elements['port'].value == '-1') {
				document.forms['edit_service'].elements['port'].value = "";
			}
		}
		
	}
</script>
{/literal}
<div class="panel">
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="parent.showHelp('services_add');">
</div>
<div class="panel">
<form method="POST" name="edit_service"  action="?module=mod_services&action=process_update_service&srv_id={$service->get('srv_id')}" onsubmit="set_port_value();">

	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<th colspan="2" align="center">Edit Tracker </th>
		</tr>
		<tr>
			<td>Transport Protocol</td>
			<td>
				{html_options name="protocol" options=$protocols selected=$smarty.post.protocol|default:$service->get('protocol')}
			</td>
		</tr>
		<tr>
			<td>IP Address</td>
			<td><input type="text" {param name="ip" class="input"} value="{$smarty.post.ip|default:$service->get('ip')}"> </td>
		</tr>
		<tr>
			<td>Friendly Name</td>
			<td><input type="text" {param name="name" class="input"} value="{$smarty.post.name|default:$service->get('name')}"></td>
		</tr>
		<tr>
			<td>Port</td>
			<td><input size="4" type="text" {param name="port" class="input"} value="{$smarty.post.port|default:$service->get('port')}"></td>
		</tr>
		<tr>
			<td>Interval</td>
			<td><input size="4" type="text" {param name="interval" class="input"} value="{$smarty.post.interval|default:$service->get('interval')|default:'60'}"> second(s)</td>
		</tr>
		<tr>
			<td>Timeout</td>
			<td><input size="4" type="text" {param name="timeout" class="input"} value="{$smarty.post.timeout|default:$service->get('timeout')|default:'1'}" onChange="set_down_threshold();"> minute(s)</td>
		</tr>
		<tr>
			<td>Logging Threshold</td>
			<td>
			{html_options name="log_timeout" selected=$smarty.post.log_timeout|default:$service->get('log_timeout') options=$timeouts}
			{*
			<select name="log_timeout">
				{foreach from=$timeouts item="timeout" key="key"}
				<option value="{$key}" {if $smarty.post.log_timeout == $key or $service->get('log_timeout') == $key}selected{/if}> {$timeout}</option>
				{/foreach}
			</select>
			*}
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">{input class="button" type="submit" value="Update Tracker"}</td>
		</tr>
	</table>

</form>
</div>

<script language="Javascript">
//set_down_threshold();
toggle_port_status();
</script>

<!-- end of {$smarty.template} -->
