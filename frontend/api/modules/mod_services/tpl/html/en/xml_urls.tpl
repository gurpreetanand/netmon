<urls>
{if $urls}
{foreach from=$urls item="url"}
	<url id="{$url.id}" uri="{$url.url|escape:"htmlall"}" pattern="{$url.pattern|escape:"htmlall"}" />
{/foreach}
{/if}
</urls>

