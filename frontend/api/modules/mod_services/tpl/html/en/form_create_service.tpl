<!-- {$smarty.template} ($Id$) -->
{literal}
<script language="Javascript">
	function set_port_value() {
		if (document.forms['create_service'].elements['protocol'].value == 'ICMP') {
			document.forms['create_service'].elements['port'].value = '-1';
		}
	}

	function set_down_threshold() {
		dropdown = document.forms['create_service'].elements['log_timeout'];
		timeout  = document.forms['create_service'].elements['timeout'];

		dropdown.options[dropdown.length-1].value = timeout.value * 60000;
	}

	function toggle_port_status() {
		type_field = document.forms['create_service'].elements['protocol'];
		type_val = type_field.options[type_field.selectedIndex].value;
		if (type_val == "ICMP") {
			document.forms['create_service'].elements['port'].disabled = true;
			document.forms['create_service'].elements['port'].className = "disabled";
		} else {
			document.forms['create_service'].elements['port'].disabled = false;
			document.forms['create_service'].elements['port'].className = "enabled";
			if (document.forms['create_service'].elements['port'].value == '-1') {
				document.forms['create_service'].elements['port'].value = "";
			}
		}

	}
</script>
{/literal}
<div class="panel">
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="parent.showHelp('services_add');">
</div>

<div class="panel">
<form method="POST" name="create_service" action="?module=mod_services&action=process_create_service" onsubmit="set_port_value();">
{if $smarty.get.norefresh}
	<input type="hidden" name="norefresh" value="{$smarty.get.norefresh}" />
{/if}
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<th colspan="2" align="center">New Tracker</th>
		</tr>
		<tr>
			<td>Transport Protocol</td>
			<td>
				{capture assign="selected"}{$smarty.post.protocol|default:$smarty.get.type|upper}{/capture}
				{html_options name="protocol" options=$protocols selected=$selected onChange="toggle_port_status();"}
			</td>
		</tr>
		<tr>
			<td>IP Address</td>
			<td><input type="text" {param name="ip" class="input"} value="{$smarty.post.ip|default:$smarty.get.ip}"> </td>
		</tr>
		<tr>
			<td>Friendly Name</td>
			<td><input type="text" {param name="name" class="input"} value="{$smarty.post.name|default:$smarty.get.name}"></td>
		</tr>
		<tr>
			<td>Port</td>
			<td><input size="4" type="text" {param name="port" class="input"} value="{$smarty.post.port|default:$smarty.get.port}"></td>
		</tr>
		<tr>
			<td>Interval</td>
			<td><input size="4" type="text" {param name="interval" class="input"} value="{$smarty.post.interval|default:'60'}"> second(s)</td>
		</tr>
		<tr>
			<td>Timeout</td>
			<td><input size="4" type="text" {param name="timeout" class="input"} value="{$smarty.post.timeout|default:'1'}" onChange="set_down_threshold();"> minute(s)</td>
		</tr>
		<tr>
			<td>Logging Threshold</td>
			<td>
			<select name="log_timeout">
				<option value="-1" {if $smarty.post.log_timeout == -1}selected{/if}>Disable Logging</option>
				<option value="0" {if $smarty.post.log_timeout == 0}selected{/if}>Log Everything</option>
				{foreach from=$timeouts item="timeout" key="key"}
				<option value="{$key}" {if $smarty.post.log_timeout == $key}selected{/if}>Latency &gt;= {$timeout}</option>
				{/foreach}
				<option value="60000" {if !$smarty.post.log_timeout or $smarty.post.log_timeout > 1500}selected{/if}>Service DOWN (Recommended)</option>
			</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">{input class="button" type="submit" value="Add Tracker "}</td>
		</tr>
	</table>

</form>
</div>

<script language="Javascript">
set_down_threshold();
toggle_port_status();
</script>

<!-- end of {$smarty.template} -->
