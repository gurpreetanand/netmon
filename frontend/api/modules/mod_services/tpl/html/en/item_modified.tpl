<!-- {$smarty.template} ($Id$) -->

{"$item entry has been $action_taken successfully."|message_bar}

{if (!$smarty.post.norefresh) || ($smarty.post.norefresh <> 1)}
<script language="Javascript">
	// Refresh the parent
	parent.parent.pnl_left.iframe_main.location.reload();
</script>
{/if}

<!-- end of {$smarty.template} -->
