
<!-- {$smarty.template} ($Id$) -->
{literal}
<script language="Javascript">
	function checkbox_update(checkbox, fieldname) {
		document.getElementById(fieldname).value = (checkbox.checked == true) ? "true" : "false";
	}
</script>
{/literal}



<div class="panel">
<form method="POST" name="update_url" action="?module=mod_services&action=process_update_url&id={$um->get('id')}">
<input type="hidden" name="enable_logging" id="enable_logging" value="{if ($smarty.post.enable_logging == "true") or ($um->get('enable_logging') == 't')}true{else}false{/if}" />
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<th colspan="2" align="center">Edit URL Tracker</th>
		</tr>
		<tr>
			<td>URL</td>
			<td>
				<input type="text" {param name="url" class="input"} value="{$smarty.post.url|default:$um->get('url')}" />
			</td>
		</tr>
		<tr>
			<td>Pattern</td>
			<td><input type="text" {param name="pattern" class="input"} value="{$smarty.post.pattern|default:$um->get('pattern')|escape:html}"></td>
		</tr>
		<tr>
			<td>Enable Logging</td>
			<td><input type="checkbox" onchange="checkbox_update(this, 'enable_logging');" name="chk_logging" value="true" {if ($smarty.post.enable_logging == "true") or ($um->get('enable_logging') == 't')}checked{/if} /></td>
		</tr>
		<tr>
			<td>Interval</td>
			<td><input size="4" type="text" {param name="interval" class="input"} value="{$smarty.post.interval|default:$um->get('interval')|default:'300'}"> second(s)</td>
		</tr>
		<tr>
			<td colspan="2" align="center">{input class="button" type="submit" value="Update Tracker"}</td>
		</tr>
	</table>

</form>
</div>

<!-- end of {$smarty.template} -->
