
<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">
	setTimeout("window.location.reload()", 15000);

	function delete_url(id, url) {
		if (confirm("Are you sure you wish to delete the URL " + url + "?")) {
			parent.parent.pnl_right.showEditor('?module=mod_services&action=process_delete_url&id=' + id);
		}
	}
init_sort = function() {
	document.getElementById("services").className = 'sort-table';
	var types = ['CaseInsensitiveString', 'CaseInsensitiveString', 'netmon_date', 'CaseInsensitiveString', 'latency', 'CaseInsensitiveString', 'CaseInsensitiveString'];
	var report = new SortableTable(document.getElementById("services"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);

function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}
</script>
{/literal}

<div class="panel">
<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon">

	{input name="new_url" type="button" class="button" value="Add New URL Tracker" onClick="parent.parent.pnl_right.showEditor('?module=mod_services&action=form_create_url&root_tpl=blank_panel');"}
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="parent.parent.pnl_right.showHelp('url_service');">
</div>

{if $urls}
<div class="datagrid center">
<table cellspacing="0" id="services" cellpadding="0" align="center" class="sortable">
	<thead>
	<tr>
		<td>URL</td>
		<td>Pattern</td>
		<td>Last Checked</td>
		<td>Status</td>
		<td>Latency</td>
		<td>Status Msg</td>
		<td>Actions</td>
	</tr>
	</thead>
	{foreach from=$urls item="url"}
	<tr>
		<td><a title="{$url.url}" target="_blank" href="{$url.url}">{$url.url|truncate:30}</a></td>
		<td><div title="{$url.pattern|escape:html}">{$url.pattern|escape:html|truncate:30}</div></td>
		<td>{$url.timestamp|date_format:"%b %e, %Y %H:%M:%S"|default:"New Service"}</td>
		{if $url.status == "MATCH"}
		<td style="background-color: #009900; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_green.gif'); background-position: top left; background-repeat: repeat-x;">
		{elseif $url.status == "NO MATCH"}
		<td style="background-color: #CC0000; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_red.gif'); background-position: top left; background-repeat: repeat-x;">
		{else}
		<td style="background-color: #444444; color: #FFFFFF; font-weight: bold;">
		{/if}
		{$url.status|default:"<strong>Unknown</strong>"}
		</td>
		<td>{$url.latency/1000|default:"?"} ms</td>
		<td><div title="{$server.message}">{$url.message|truncate:30}</div></td>
		<td>
			<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_services&action=form_edit_url&id={$url.id}');">Edit</a> |
			<a href="javascript:delete_url('{$url.id}', '{$url.url|escape:javascript}');">Del</a> |
			<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=web_alert_down&url_id={$url.id}&rel=urls')">Alerts</a>

		</td>
	</tr>
	{/foreach}
</table>
</div>
{else}
{"There are no URLs in the monitoring database at the moment"|message_bar}
{/if}

<!-- end of {$smarty.template} -->
