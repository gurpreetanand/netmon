<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>
<script language="Javascript">

	setTimeout("window.location.reload()", 15000);

	function delete_service(srv_id, name, ip) {
		if (confirm("Are you sure you wish to delete the service " + name + " at " + ip + "?")) {
			parent.parent.pnl_right.showEditor('?module=mod_services&action=delete_service&srv_id=' + srv_id);
		}
	}
init_sort = function() {
	document.getElementById("services").className = 'sort-table';
	var types = ['CaseInsensitiveString', 'netmon_ipv4', 'CaseInsensitiveString', 'netmon_date', 'CaseInsensitiveString', 'latency', 'CaseInsensitiveString', 'CaseInsensitiveString'];
	var report = new SortableTable(document.getElementById("services"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);

function ts_getInnerText(el) {
	if (typeof el == "string") return el;
	if (typeof el == "undefined") { return el };
	if (el.innerText) return el.innerText;	//Not needed but it is faster
	var str = "";

	var cs = el.childNodes;
	var l = cs.length;
	for (var i = 0; i < l; i++) {
		switch (cs[i].nodeType) {
			case 1: //ELEMENT_NODE
				str += ts_getInnerText(cs[i]);
				break;
			case 3:	//TEXT_NODE
				str += cs[i].nodeValue;
				break;
		}
	}
	return str;
}



function getParent(el, pTagName) {
	if (el == null) return null;
	else if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())	// Gecko bug, supposed to be uppercase
		return el;
	else
		return getParent(el.parentNode, pTagName);
}


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}

<div class="panel">

<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon">

	{input name="new_service" type="button" class="button" value="Add New Tracker" onClick="parent.parent.pnl_right.showEditor('?module=mod_services&action=form_create_service&root_tpl=blank_panel&type=`$smarty.get.type`');"}
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="parent.parent.pnl_right.showHelp('services_explorer');">
</div>


{if $servers}
<div class="datagrid center">
<table cellspacing="0" id="services" cellpadding="0" align="center">
	<thead>
	<tr>
		<td>Name</td>
		<td>IP Address</td>
		<td>Protocol</td>
		<td>Last Checked</td>
		<td>Status</td>
		<td>Latency</td>
		<td>Status Msg</td>
		<td>Actions</td>
	</tr>
	</thead>
	{foreach from=$servers item="server"}
	<tr>
		<td>{$server.name}</td>
		<td><a title="{$server.ip}" href="#" onClick="javascript:parent.parent.location='?module=mod_layout&action=render_section&layout=network&ip={$server.ip}&store_request=2'">{$server.ip}</a></td>
		<td>{$server.protocol}{if $server.port > 1}-{$server.port}{/if}</td>
		<td>{$server.timestamp|date_format:"%b %e, %Y %H:%M:%S"|default:"New Service"}</td>
		{if $server.status == "UP"}
		<td style="background-color: #009900; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_green.gif'); background-position: top left; background-repeat: repeat-x;">
		{elseif $server.status == "DOWN"}
		<td style="background-color: #CC0000; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_red.gif'); background-position: top left; background-repeat: repeat-x;">
		{else}
		<td style="background-color: #444444; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_gray2.gif'); background-position: top left; background-repeat: repeat-x;">
		{/if}
		{$server.status|default:"<strong>Unknown</strong>"}
		</td>
		<td>{$server.latency/1000|default:"?"} ms</td>
		<td><div title="{$server.message}">{$server.message|truncate:30}</div></td>
		<td>
			<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_services&action=form_edit_service&srv_id={$server.srv_id}');">Edit</a> |
			<a href="javascript:delete_service('{$server.srv_id}', '{$server.name|escape:javascript}', '{$server.ip}');">Del</a> |
			<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=srv_service_down&reference_pkey_val={$server.srv_id}&rel=servers')">Alerts</a>

		</td>
	</tr>
	{/foreach}
</table>
</div>
{else}
{"There are no servers in the monitoring database at the moment"|message_bar}
{/if}


<!-- end of {$smarty.template} -->
