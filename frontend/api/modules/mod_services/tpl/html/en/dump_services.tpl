<!-- {$smarty.template} ($Id$) -->


{if $servers}
<div class="datagrid center">
<table cellspacing="0" cellpadding="3" align="center">
	<tr>
		<th>Host</th>
		<th>Protocol</th>
		<th>Last Checked</th>
		<th>Status</th>
		<th>Status Message</th>
	</tr>
	{foreach from=$servers item="server"}
	<tr>
		
		<td>{$server.name} (<acronym title="{$server.ip|resolve_ip}">{$server.ip|resolve_ip}</acronym>)</td> 
		<td>{$server.protocol}{if $server.port > 0}:{$server.port|resolve_port} ({$server.port}){/if}</td>
		<td>{$server.timestamp|date_format:"%b %e, %Y %H:%M:%S"|default:"New Service"}</td>
		{if $server.status == "UP"}
		<td style="background-color: #009900; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_green.gif'); background-position: top left; background-repeat: repeat-x;">
		{elseif $server.status == "DOWN"}
		<td style="background-color: #CC0000; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_red.gif'); background-position: top left; background-repeat: repeat-x;">
		 {/if}
		 {$server.status|default:"<strong>Unknown</strong>"}
		 </td>
		<td><div title="{$server.message}">{$server.message}</div></td>
	</tr>
	{/foreach}
</table>
</div>
{/if}

{if not $servers and not $urls and not $disks}
<div class="datagrid center">
<table cellspacing="0" cellpadding="3" align="center">
	<tr><td>
		<div style="margin-top: 10px; margin-bottom: 10px;background-color: White;" align="left">
			<img src="assets/icons/up_large.gif" class="icon">
			&nbsp;&nbsp;<strong>All services are up and running.<strong>
		</div>
	</td></tr>
</table>
</div>
{/if}



{if $disks}

<div class="secondarybar">
	Disk Alerts
</div>

<div class="datagrid center">
<table cellspacing="0" cellpadding="3" align="center">
	<tr>
		<th>&nbsp;</th>
		<th>Host</th>
		<th>Protocol</th>
		<th>Last Checked</th>
		<th>Status Message</th>
	</tr>
	{foreach from=$disks item="disk"}
	<tr>
		<td>
		<div align="center">
			<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" WIDTH="52" HEIGHT="47">
				<PARAM NAME=movie VALUE="assets/diskview.swf">
				<PARAM NAME="wmode" VALUE="transparent">
				<PARAM NAME="FlashVars" VALUE="currentStatus={$disk.percent|default:""}&threshold={$disk.threshold|default:"0"}&animation=1">
				<PARAM NAME="quality" VALUE="high">
				<PARAM NAME="bgcolor" VALUE="#D4D0C8">
				<EMBED src="assets/diskview.swf?currentStatus={$disk.percent|default:""}&threshold={$disk.threshold|default:"0"}&animation=1" quality="high" bgcolor="#D4D0C8"  WIDTH="52" WMODE="transparent" HEIGHT="47" NAME="mod_Disks_FlashSymbol" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED>
			</OBJECT>
			{* {$disk.percent}% Full *}</td>

		<td>{$disk.share|truncate:15:"..."} on <a href="#" onClick="parent.location='?module=mod_layout&action=render_section&layout=network&ip={$disk.ip}&store_request=2';">{$disk.ip|resolve_ip}</a></td>
		<td>{$disk.type}</td>
		<td>{$disk.timestamp|date_format:"%b %e, %Y %H:%M:%S"|default:"New Service"}</td>

		<td><div title="{$server.message}">{$disk.message|truncate:30:"..."}</div></td>
	</tr>
	{/foreach}


</table>
</div>
{/if}

{if $urls}

<div class="secondarybar">
	URL Alerts
</div>

<div class="datagrid center">
<table cellspacing="0" cellpadding="3" align="center">
	<tr>
		<th>URL</th>
		<th>Pattern</th>
		<th>Last Checked</th>
		<th>Status</th>
		<th>Status Msg</th>
	</tr>
	{foreach from=$urls item="url"}
	<tr>
		<td><a title="{$url.url}" target="_blank" href="{$url.url}">{$url.url|truncate:30}</a></td>
		<td><div title="{$url.pattern|escape:html}">{$url.pattern|escape:html|truncate:30}</div></td>
		<td>{$url.timestamp|date_format:"%b %e, %Y %H:%M:%S"|default:"New Service"}</td>
		{if $url.status == "MATCH"}
		<td style="background-color: #009900; color: #FFFFFF; font-weight: bold;">
		{elseif $url.status == "NO MATCH"}
		<td style="background-color: #CC0000; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_red.gif'); background-position: top left; background-repeat: repeat-x;">
		{else}
		<td style="background-color: #444444; color: #FFFFFF; font-weight: bold;">
		{/if}
		{$url.status|default:"<strong>Unknown</strong>"}
		</td>
		<td><div title="{$server.message}">{$url.message|truncate:30}</div></td>
	</tr>
	{/foreach}
</table>
</div>
{/if}




<!-- end of {$smarty.template} -->
