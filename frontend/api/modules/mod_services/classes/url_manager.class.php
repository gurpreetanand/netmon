<?php


class URL_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "urls";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "id";
	/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_services";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		$this->params->add_primitive("url",      "url",     TRUE, "URL to monitor",   "URL");
		$this->params->add_primitive("pattern",  "string",  TRUE, "Search Pattern",   "Pattern");
		$this->params->add_primitive("interval", "integer", TRUE, "Polling Interval", "Polling Interval");
		$this->params->add_primitive("enable_logging", "pg_bool",           TRUE,   "Enable historical logging", "Select this option if you intend to run historical reports");
	}

	function get_all_urls() {
		$query = "SELECT * FROM {$this->table} ORDER BY status DESC, id ASC";
		$res = $this->db->select($query);

		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			return;
		}

		return $res;

	}

	function get_down_urls() {
		$query = "SELECT * FROM {$this->table} WHERE status ILIKE 'NO MATCH' OR status ILIKE 'DOWN' ORDER BY id ASC";
		$res = $this->db->select($query);

		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			return;
		}

		return $res;
	}

	function get_by_ip($ip) {
                $ip = addslashes($ip);

                $query = "SELECT * FROM {$this->table} WHERE url ILIKE '%" . $ip . "%' ORDER BY id ASC";
                $res = $this->db->select($query);

                if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
                        return;
                }

                return $res;
	}


	function pop($id) {
		$id = $this->db->escape($id);

		$query = "SELECT * FROM {$this->table} WHERE {$this->pkey} = $id";

		$result = $this->db->get_row($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			foreach($result as $key => $value) {
				$this->params->setval($key, $value);
			}
			return TRUE;
		}
	}
	
	function pre_delete($id) {
		$query = "DELETE FROM alert_handlers WHERE trigger_id IN (select trigger_id from alert_triggers WHERE reference_table_name = 'urls' AND reference_pkey_val = $id)";
		$this->db->delete($query);
		$query = "DELETE FROM alert_triggers WHERE reference_table_name = 'urls' AND reference_pkey_val = $id";
		$this->db->delete($query);
		$this->db->delete("DELETE FROM alert_pending WHERE sent = '0' AND handler_id NOT IN (SELECT id FROM alert_handlers)");
		return TRUE;
	}




}
?>
