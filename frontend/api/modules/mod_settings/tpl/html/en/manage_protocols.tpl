<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("protocols").className = 'sort-table';
	var types = ['CaseInsensitiveString', 'Number', 'CaseInsensitiveString', null];
	var report = new SortableTable(document.getElementById("protocols"), types);
	report.sort(1, false);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="titlebar">
	Manage Port Label Database
</div>

<div class="panel">
	{input name="Button" type="button" class="button" value="Add New Port Label" onClick="parent.pnl_right.showEditor('?module=mod_settings&action=form_create_protocol&root_tpl=blank_panel');"}
	<img src="assets/buttons/button_help.gif" alt="Help" title="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=protocol_mgmt&root_tpl=blank');" />
</div>

{if $protocols}
<div class="datagrid center">
<table width="100%" border="0" cellpadding="3" cellspacing="0" id="protocols">
	<thead>
	<tr>
		<td>Transport Layer</td>
		<td>Port</td>
		<td>Label</td>
		<td>Actions</td>
	</tr>
	</thead>
	{foreach from=$protocols item="protocol"}
	<tr>
		<td>{$protocol.protocol}</td>
		<td>{$protocol.port}</td>
		<td>{$protocol.name}</td>
		<td>
			<a href="#" onClick="parent.pnl_right.showEditor('?module=mod_settings&action=form_edit_protocol&id={$protocol.id}');">Edit</a> |
			<a href="#" onClick="if (confirm('Are you sure you want to delete the port label for the protocol {$protocol.name|escape:javascript}?')) {ldelim} parent.pnl_right.showEditor('?module=mod_settings&action=process_delete_protocol&id={$protocol.id}'); {rdelim}">Del</a> |
			<a href="#" onClick="parent.pnl_right.showHelp('?module=mod_help&action=get_protocol_doc&transport_layer={$protocol.protocol}&port_number={$protocol.port}');">Doc</a> |
			<a href="#" onClick="parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=port_protocol_traffic&protocol_id={$protocol.id}&port={$protocol.port}&transport={$protocol.protocol}');">Alerts</a>
		</td>
	</tr>
	{/foreach}

</table>
</div>
{/if}

<!-- end of {$smarty.template} -->
