
<!-- {$smarty.template} ($Id$) -->

	<div class="titlebar">Netmon Update Service</div>

	<div class="panel">
	
	{input type="button" class="button" value="Check for New Updates Now" onClick="showProgressIndicator('progress'); document.location = '?module=mod_settings&action=fetch_updates&root_tpl=blank_panel';"}
	
	<img src="/assets/buttons/button_help.gif" alt="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('netmon_updates');" title="Show Help" />
	<img style="align: right; display: inline;" src="/assets/progress_big.gif" id="progress" class="progress_big" />
	
	</div>
	
	{'Warning: Your firewall must allow your Netmon device to establish connections on TCP Port 873 in order for updates to be retrieved.'|message_bar}
	
<div class="panel white">
	<h1>Update Summary</h1>
	<pre>{$summary}</pre>
</div>
	
<!-- end of {$smarty.template} -->
