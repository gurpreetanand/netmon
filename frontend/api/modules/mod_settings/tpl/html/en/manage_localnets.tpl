<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("nets").className = 'sort-table';
	var types = ['CaseInsensitiveString', 'ipv4_address', 'CaseInsensitiveString', null];
	var report = new SortableTable(document.getElementById("nets"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="titlebar">
	Manage Network Range(s)
</div>
<div class="panel">
  {input name="Button" type="button" class="button" value="Add New Network Range" onClick="parent.pnl_right.showEditor('?module=mod_settings&action=form_create_localnet&root_tpl=blank_panel');"}
	<img src="/assets/buttons/button_help.gif" alt="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('localnet_setup');" title="Show Help" />
</div>
<div class="datagrid center">

<table width="100%" border="0" cellpadding="3" cellspacing="0" id="nets">
	<thead>
	<tr>
		<td>Label</td>
		<td>Address Range</td>
		<td>Flags</td>
		<td>Action</td>
	</tr>
	</thead>
    {if $ids}
    	{foreach from=$ids item="id"}
    	<tr>
    		<td>{$id.label}</td>
	    	<td>[{$id.network}] to [{$id.broadcast}]</td>
	    	<td>
		    	<div align="left">
				<p>SNMP AutoDiscovery: {if $id.enable_snmp_discovery == 't'}<span class="green">Enabled</span>{else}<span class="gray2">Disabled</span>{/if}</p>	
		    	<p>Background Port Scans: {if $id.enable_portscan == 't'}<span class="green">Enabled</span>{else}<span class="gray2">Disabled</span>{/if}</p>
				</div>
	    	</td>
	    	<td>
	    		<a href="javascript: parent.pnl_right.showEditor('?module=mod_settings&action=form_edit_localnet&id={$id.id}');">Edit</a> |
	    		<a href="#" onClick="if (confirm('Are you sure you wish to delete the local network starting with IP address {$id.network} and ending with IP address {$id.broadcast}?')) {ldelim} parent.pnl_right.showEditor('?module=mod_settings&action=delete_localnet&id={$id.id}'); {rdelim}">Del</a>
	    		
	    	</td>
	    </tr>
    	{/foreach}
</table>
    {else}
    	</table>
    	{"There are currently no local networks registered in the database"|message_bar}
    {/if}
    
    
<!-- end of {$smarty.template} -->