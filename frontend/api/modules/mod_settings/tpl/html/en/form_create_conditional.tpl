<!-- {$smarty.template} ($Id$) -->

<div class="panel">
<form action="?module=mod_settings&action=process_create_conditional&root_tpl=blank_panel" method="POST">

	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<th colspan="2" align="center">New Alert Conditional</th>
		</tr>
		<tr>
			<td>IP Address</td>
			<td><input type="text" {param name="ip" class="input"} value="{$smarty.post.ip}"> </td>
		</tr>
		<tr>
			<td>Conditional Name</td>
			<td><input type="text" {param name="name" class="input"} value="{$smarty.post.name}"></td>
		</tr>
		<tr>
			<td colspan="2" align="center">{input class="button" type="submit" value="Add Conditional"}</td>
		</tr>
	</table>

</form>
</div>
<!-- end of {$smarty.template} -->
