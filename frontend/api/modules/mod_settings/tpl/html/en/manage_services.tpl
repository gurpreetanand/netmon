<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,yui.connection"}
{literal}
<script language="Javascript">

	success_handler = function(o) {
		hideProgressIndicator(o.argument);
	}
	
	failure_handler = function(o) {
		alert("An error has occured while changing the startup setting for this service: " + o.responseText);
	}

	change_startup = function(process, val) {
		uri = "?module=mod_settings&action=ajax_change_startup&process="+process+"&val="+val+"&root_tpl=blank";
		indicator = process+"_progress";
		showProgressIndicator(indicator);
		YAHOO.util.Connect.asyncRequest('GET', uri, {success:success_handler, failure:failure_handler, argument:indicator}, null);
	}
	
	change_plugin_startup = function(plugin, iface, val) {
		uri = "?module=mod_settings&action=ajax_change_plugin_startup&plugin="+plugin+"&iface="+iface+"&val="+val+"&root_tpl=blank";
		indicator = plugin+"_"+iface+"_progress";
		showProgressIndicator(indicator);
		YAHOO.util.Connect.asyncRequest('GET', uri, {success:success_handler, failure:failure_handler, argument:indicator}, null);
	}
</script>
{/literal}

<div class="titlebar">
	{$smarty.const.SITE_NAME} Services Manager
</div>

<div class="panel">
  <img src="/assets/buttons/button_help.gif" alt="Help" title="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=managing_daemons&root_tpl=blank');" />
</div>

{if $services}
<div class="datagrid center">

<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<th><div align="left">Service Name </div></th>
		<th>Status</th>
		<th>Start/Stop</th>
		<th>Configure</th>
	</tr>
	<!-- Netmon Daemons -->
	{foreach from=$services item="service"}
	<tr>
		<td>
		<div align="left"><img src="/assets/progress_transparent.gif" border="0" id="{$service.name}_progress" class="progress" style="display: inline" />
		<a href="javascript: parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=daemon_{$service.name}&root_tpl=blank')"><img src="/assets/icons/services_grayscale.gif" class="icon" width="16" height="16"></a> {$service.description} &nbsp;<a href="#" onClick="parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=daemon_{$service.name}&root_tpl=blank')">Info</a></div></td>
		<td><img src="assets/icons/{if $service.status == "up"}up{else}off{/if}.gif"></td>
		{if $service.status == "up"}
			{assign var="action"  value="stop_service"}
		{else}
			{assign var="action"  value="start_service"}
		{/if}
		{assign var="caption" value=$action|mktitle}
		{assign var="process" value=$service.name|escape:"url"}
		<td>
		{input type="button" value="$caption" onClick="document.location = '?module=mod_settings&action=$action&service=$process&root_tpl=blank_panel';"}
		<select name="startup" onChange="change_startup('{$process}', this.value);">
			<option {if $service.start_auto == 'yes'}selected{/if} value="auto">Automatic</option>
			<option {if $service.start_auto == 'no'}selected{/if} value="manual">Manual</option>
		</select>
		
		</td>
		<td>
		{if (has_perm("low_level_settings"))}
		<a href="?module=mod_settings&action=form_configure_daemon&daemon={$process}" target="_self">Configure</a>
		{else}&nbsp;{/if}
		</td>
	</tr>
	<!-- End of Netmon Daemons -->
	{if is_array($service.plugins)}
	<!-- Plugins -->
	{foreach from=$service.plugins.plugin item="plugin"}
	<tr>
		<td><div align="left"><img src="/assets/core/spacer.gif" width="30px" height="1px" border="0" /><a href="javascript: parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=daemon_{$plugin.name}&root_tpl=blank')"><img src="/assets/icons/services_grayscale.gif" class="icon" width="16" height="16"></a> {$plugin.description} &nbsp;<a href="#" onClick="parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=daemon_{$plugin.name}&root_tpl=blank')">Info</a></div></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>
		{if (has_perm("low_level_settings"))}
		<a href="?module=mod_settings&action=form_configure_daemon&daemon={$plugin.name}" target="_self">Configure</a>
		{else}&nbsp;{/if}
		</td>
	</tr>
	<!-- Plugin Interfaces -->
	{if is_array($plugin.interfaces.interface[0])}
		{assign var="interfaces" value=$plugin.interfaces.interface}
	{else}
		{assign var="interfaces" value=$plugin.interfaces}
	{/if}
	{foreach from=$interfaces item="interface"}
	<tr>
		<td><div align="left"><img src="/assets/progress_transparent.gif" border="0" id="{$plugin.name}_{$interface.iface}_progress" class="progress" style="display: inline" /><img src="/assets/core/spacer.gif" width="60px" height="1px" border="0" />{$interface.iface}</div></td>
		<td><img src="assets/icons/{if $interface.status == 'up'}up{else}off{/if}.gif" /></td>
		<td>{if $interface.status == 'up'}{assign var="action" value="stop_plugin"}
		{else}{assign var="action" value="start_plugin"}{/if}
		{assign var="caption" value=$action|mktitle}
		{assign var="plugin_name" value=$plugin.name}
		{assign var="iface" value=$interface.iface}
		{input type="button" value="$caption" onClick="document.location = '?module=mod_settings&action=$action&plugin=$plugin_name&iface=$iface&root_tpl=blank_panel';"}
		<select name="startup" onChange="change_plugin_startup('{$plugin_name}', '{$interface.iface}' ,this.value);">
			<option {if $interface.start_auto == 'yes'}selected{/if} value="auto">Automatic</option>
			<option {if $interface.start_auto == 'no'}selected{/if} value="manual">Manual</option>
		</select>
		</td>
		<td>&nbsp;</td>
	</tr>
	{/foreach}
	<!-- End of Plugin Interfaces -->
	{/foreach}
	<!-- End of Plugins -->
	{/if}
	{/foreach}
</table>
{else}
{"Services status currently unavailable. Please try again later or contact the Netmon Helpdesk for assistance"|message_bar}
{/if}

<!-- end of {$smarty.template} -->
