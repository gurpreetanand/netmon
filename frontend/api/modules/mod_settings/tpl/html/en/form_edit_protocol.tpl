<!-- {$smarty.template} ($Id$) -->


<div class="empty">
<form action="?module=mod_settings&action=update_protocol&id={$pm->get("id")}" method="POST">
	<br />
	<div align="center"><strong>Edit Port Label</strong></div>
	<br />
	
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<td><div align="right">Transport Layer:</div></td>
			<td>
			{capture assign="selected"}{$smarty.post.protocol|default:$pm->get("protocol")|default:""}{/capture}
			<select name="protocol">
				<option value="TCP" {if $selected == "TCP"}selected{/if}>TCP</option>
				<option value="UDP" {if $selected == "UDP"}selected{/if}>UDP</option>
			</select>			
			</td>
		</tr>
		<tr>
			<td><div align="right">Port Number:</div></td>
			<td><input type="text" {param name="port" class="input"} value="{$smarty.post.port|default:$pm->get("port")}" style="width: 35px;"></td>
		</tr>
		<tr>
			<td><div align="right">Protocol Name:</div></td>
			<td><input type="text" {param name="name" class="input"} value="{$smarty.post.name|default:$pm->get("name")}"></td>
		</tr>
	</table>

	<p align="center">{input class="button" type="submit" value="Update Port Label"}</p>

</form>
</div>


<!-- end of {$smarty.template} -->
