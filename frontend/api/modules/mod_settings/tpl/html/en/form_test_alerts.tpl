
<!-- {$smarty.template} ($Id$) -->

<div class="titlebar">Alert Testing Utility</div>


<div class="panel">
<form method="POST" action="?module=mod_settings&action=alert_test&root_tpl=blank_panel" onSubmit="showProgressIndicator('progress');">
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center"><strong>Recipient</strong></td>
	</tr>
	<tr>
		<td align="center">{html_options name="user_id" options=$users selected=$smarty.session.id}</td>
	</tr>
	<tr>
		<td align="center"><strong>Subject</strong></td>
	</tr>
	<tr>
		<td align="center">{input type="textbox" name="subject" value="Netmon Alert Test"}</td>
	</tr>
	<tr>
		<td align="center"><strong>Body</strong></td>
	</tr>
	<tr>
		<td align="center">
			<textarea name="body" cols="35" rows="4">Testing the Netmon alert notification system. This is just a drill</textarea>
		</td>
	</tr>
	<tr>
		<td align="center">
		<img style="float: right; display: inline" src="/assets/progress_big.gif" id="progress" class="progress_big" />
		{input type="submit" class="button" value="Send"}
		{input type="reset" class="button" value="Reset Form"}
		</td>
	</tr>
</table>
</form>
</div>

<!-- end of {$smarty.template} -->
