<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,yui.event,yui.dom,yui.animation,yui.container,yui.dragdrop,yui.connection"}
{import_css files="yui_sam.container,yui_sam.button"}

<div class="titlebar">Netmon Registration Information</div>

<div class="panel white">You may use this form to use a new license key, or to update your contact information</div>

<form id="{$smarty.const.SITE_NAME}_activation" name="{$smarty.const.SITE_NAME}_activation" onSubmit="return performActivation();">
<fieldset>
<legend>System Registration</legend>
<table border="0" cellpadding="10" cellspacing="0" width="100%">
  <tr> 
    <td width="35%"><div align="right"><strong>Registration Key:</strong></div></td>
    <td width="65%"><div align="left">{input type="text" name="key" size="16" value=$smarty.post.key|default:$activation.registration_key doc="Registration Key"}
  </tr>
</table>
</fieldset>

<fieldset>
<legend>Corporate Information</legend>
<table border="0" cellpadding="10" cellspacing="0" width="100%">
 <tr>
  	<td width="35%"><div align="right">Company Name</div></td>
  	<td width="65%"><div align="left">{input type="text" name="company_name" size="30" value=$smarty.post.company_name|default:$activation.company_name doc="Company Name"}</div></td>
  </tr>
  <tr>
  	<td width="35%"><div align="right">Address</div></td>
  	<td width="65%"><div align="left"><textarea name="company_address" cols="30" rows="4" title="Company Address">{$smarty.post.company_address|default:$activation.company_address}</textarea></div></td>
  </tr>
  <tr>
  	<td width="35%"><div align="right">City</div></td>
  	<td width="65%"><div align="left">{input type="text" name="company_city" size="30" value=$smarty.post.company_city|default:$activation.company_city doc="Company City"}</div></td>
  </tr>
  <tr>
  	<td width="35%"><div align="right">Province/State</div></td>
  	<td width="65%"><div align="left">{input type="text" name="company_state" size="30" value=$smarty.post.company_state|default:$activation.company_state doc="Company State/Province"}</div></td>
  </tr>
  
  <tr>
  	<td width="35%"><div align="right">Country</div></td>
  	<td width="65%"><div align="left">{input type="text" name="company_country" size="30" value=$smarty.post.company_country|default:$activation.company_country doc="Company Country"}</div></td>
  </tr>
</table>
</fieldset>

<fieldset>
<legend>Contact Information</legend>
<table border="0" cellpadding="10" cellspacing="0" width="100%">
  <tr>
  	<td width="35%"><div align="right">First Name</div></td>
  	<td width="65%"><div align="left">{input type="text" name="contact_first_name" size="15" value=$smarty.post.contact_first_name|default:$activation.contact_first_name doc="Contact First Name"}</div></td>
  </tr>
  <tr>
    <td width="35%"><div align="right">Last Name</div></td>
  	<td width="65%"><div align="left">{input type="text" name="contact_last_name" size="15" value=$smarty.post.contact_last_name|default:$activation.contact_last_name}</div></td>
  </tr>
  <tr>
    <td width="35%"><div align="right">Email Address</div></td>
  	<td width="65%"><div align="left">{input type="text" name="contact_email" size="15" value=$smarty.post.contact_email|default:$activation.contact_email}</div></td>
  </tr>
  <tr>
    <td width="35%"><div align="right">Phone</div></td>
  	<td width="65%"><div align="left">{input type="text" name="contact_phone" size="12" value=$smarty.post.contact_phone|default:$activation.contact_phone} (Ext: {input size="4" type="text" name="contact_phone_ext" doc="Phone Extension" value=$smarty.post.contact_phone_ext|default:$activation.contact_phone_ext})</div></td>
  </tr>
  
</table>
</fieldset>

<div align="center" id="actStatus"></div>
<div align="center"><img style="display: inline" align="center" src="/assets/progressbar.gif" id="progress" class="progressbar"/></div>
<table border="0" cellpadding="10" cellspacing="0"  width="100%">
	<tr> 
		<td colspan="2" align="center"> 
			
			{input name="Submit" type="submit" class="button" value="Activate"}
		</td>
	</tr>
</table>
 
<script language="Javascript">
document.body.className = 'yui-skin-sam';
	
	// Sets the focus to the reg key input field
	initFocus = function() {ldelim}
		document.{$smarty.const.SITE_NAME}_activation.key.focus();
	{rdelim}

	{literal}
	
	// Submits form payload through an AJAX POST request
	performActivation = function() {
		showProgressIndicator('progress');
		console.log("Activating...");
		showProgressIndicator('progress');
		formObj = document.getElementById('{/literal}{$smarty.const.SITE_NAME}{literal}_activation');
		YAHOO.util.Connect.setForm(formObj);
		callback = {success: ajax_success, failure: ajax_failure};
		conn = YAHOO.util.Connect.asyncRequest('POST', '?module=core&action=activate&root_tpl=blank', callback);
		return false;
	}
	
	// Success handler
	ajax_success = function(o) {
		hideProgressIndicator('progress');
		resp = o.responseText;
		
		divObj = document.getElementById('actStatus');
		divEl = new YAHOO.util.Element('actStatus'); 
		//divObj.style.opacity = 0;
		//divObj.style.filter = "alpha(opacity=0)";
		divEl.setStyle('opacity', '0.1');
		
		var transition_attributes = {
			opacity: {to: 1},
			duration: 1
		}
		
		var transition = new YAHOO.util.Anim(divObj, transition_attributes);
		
		if ("OK" == resp) {
			divObj.innerHTML = '<strong><font color="#00AC27">ACTIVATION SUCCESSFUL</font></strong>';
			//transition.onComplete.subscribe(toLoginDialog);
		} else {
			divObj.innerHTML = '<fieldset style="text-align: left"><legend><strong><font color="#ff0000">Error Report:</font></strong></legend><strong>'+resp+'</font></strong></fieldset>';
			transition.onComplete.subscribe(initFocus);			
		}
		
		
		
		transition.animate();
	}
	
	toLoginDialog = function() {
		document.location = '?module=core&action=login_dialog';
	}
	
	// Failure handler
	ajax_failure = function(o) {
		hideProgressIndicator('progress');
		divObj = document.getElementById('actStatus');
		resp = o.statusText;
		
		divObj = document.getElementById('actStatus');
		divObj.style.opacity = 0;
		divObj.style.filter = "alpha(opacity=0)";
		
		divObj.innerHTML = '<strong><font color="#ff0000">ERROR: '+resp+'</font></strong>';
		
		var transition_attributes = {
			opacity: {to: 1},
			duration: 1
		}
		
		var transition = new YAHOO.util.Anim(divObj, transition_attributes);
		transition.onComplete.subscribe(initFocus);
		transition.animate();
		
	}
	
	// Fade in the login dialog and calls initFocus() once the fading has completed
	showDialog = function(e) {
		var transition_attributes = {
			opacity: {to: 1},
			duration: 1
		}
		
		var transition = new YAHOO.util.Anim(document.getElementById('actDialog'), transition_attributes);
		transition.onComplete.subscribe(initFocus);
		transition.animate();
	}
	
	// Call showDialog() once the div has finished rendering w/ its opacity set to 0
	YAHOO.util.Event.onAvailable("actDialog", showDialog);

	
	
	
	function handleOK() {
		YAHOO.netmon.panels.wait.hide();
	}
	

	{/literal}
	
	
	
</script>