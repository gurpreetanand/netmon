<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,yui.connection"}

{literal}
<style>
.highlighted { 	border: 2px solid #FFFF00; }
.cleared { 	border: 0px; }
</style>



<script language="Javascript">
function addText(input, insText) {
	input.focus();
	if( input.createTextRange ) {
   document.selection.createRange().text += insText;
 } else if( input.setSelectionRange ) {
   var len = input.selectionEnd;
   input.value = input.value.substr( 0, len )
     + insText + input.value.substr( len );
   input.setSelectionRange(len+insText.length,len+insText.length);
 } else { input.value += insText; }
}
{/literal}

DefaultText    = "{$tpl.original_template|regex_replace:"/(\r\n|\r|\n)/":"\\n"}";
DefaultSubject = "{$tpl.description}";
alertTypeID = "{$smarty.get.id}";

{literal}
CurrentObjectFocus = "default_template";

function ChangeFocus(elementId){
	CurrentObjectFocus = elementId;
	ClearFormatting();
	document.getElementById(elementId).className = 'highlighted';
}

function ClearFormatting(){
	document.getElementById('default_subject').className = '';
	document.getElementById('default_template').className = '';
	{/literal}
	{if $commands_ok == TRUE}
	document.getElementById('command').className = '';
	document.getElementById('edit_command_command').className = '';
	{/if}
	{literal}
	
}

toggle_panel = function(panel) {
	if (getBrowser() == "ie") {
		sib = panel.nextSibling;
	} else {
		sib = panel.nextSibling.nextSibling;
	}
	if (sib.className == "region, closed") {
		sib.className = "region, open";
		panel.className = "titlebar_expanded";
	} else {
		sib.className = "region, closed"
		panel.className = "titlebar_collapsed";
	}
}

show_edit_container = function() {
	container = document.getElementById('editCommandContainer');
	container.style.visibility = 'visible';
	container.style.display = 'block';
	hide_create_form();
	show_edit_form();
}

hide_create_form = function() {
	panel = document.getElementById('new_command');
	if (getBrowser() == "ie") {
		sib = panel.nextSibling;
	} else {
		sib = panel.nextSibling.nextSibling;
	}
	
	sib.className = "region, closed"
	panel.className = "titlebar_collapsed";
}

show_edit_form = function() {
	panel = document.getElementById('edit_command');
	if (getBrowser() == "ie") {
		sib = panel.nextSibling;
	} else {
		sib = panel.nextSibling.nextSibling;
	}
	
	sib.className = "region, open";
	panel.className = "titlebar_expanded";
}

fill_edit_container = function(command_id, label, command, timeout, async, cond_failure, cond_recovery) {
	update_field('edit_command_label', label);
	update_field('command_id', command_id);
	//update_field('edit_command_timeout', timeout);
	update_field('edit_command_command', command);
	if (async == 'f') {
		document.getElementById('edit_async_false').checked = true;
		document.getElementById('edit_async_true').checked = false;
	} else {
		document.getElementById('edit_async_false').checked = false;
		document.getElementById('edit_async_true').checked = true;
	}
	
	if (cond_failure == 't') {
		document.getElementById('edit_perform_on_failure').checked = true;
	} else {
		document.getElementById('edit_perform_on_failure').checked = false;
	}
	
	if (cond_recovery == 't') {
		document.getElementById('edit_perform_on_recovery').checked = true;
	} else {
		document.getElementById('edit_perform_on_recovery').checked = false;
	}
}

update_field = function(field, val) {
	document.getElementById(field).value = val;
}

refresh_commands_table = function(o) {
	res = o.responseText;
	hideProgressIndicator(o.argument);
	document.getElementById('commandsList').innerHTML = res;
}

ajax_update_command = function(command_id) {
	formObj = document.getElementById('editForm');
	YAHOO.util.Connect.setForm(formObj);
	showProgressIndicator("update_progress");
	YAHOO.util.Connect.asyncRequest('POST', '/?module=mod_settings&action=ajax_update_command&root_tpl=blank_panel&id=' + command_id, {success:refresh_commands_table, failure:refresh_commands_table, argument:"update_progress"});
}

ajax_create_command = function() {
	formObj = document.getElementById('createForm');
	YAHOO.util.Connect.setForm(formObj);
	showProgressIndicator("create_progress");
	YAHOO.util.Connect.asyncRequest('POST', '/?module=mod_settings&action=ajax_create_command&root_tpl=blank', {success:refresh_commands_table, failure:refresh_commands_table,argument:"create_progress"});
}

ajax_delete_command = function(command_id) {
	progressIndicator="delete_"+command_id+"_progress";
	showProgressIndicator(progressIndicator);
	query = "/?module=mod_settings&action=ajax_delete_command&id=" + command_id + "&alert_type_id=" + alertTypeID + "&root_tpl=blank";
	YAHOO.util.Connect.asyncRequest('GET', query, {success: refresh_commands_table, failure: refresh_commands_table,argument:progressIndicator});
}

</script>
{/literal}

<div class="titlebar">
	Alert Template - {$tpl.description|capitalize}
</div>
<form method="POST" action="?module=mod_settings&action=update_alert_template&id={$tpl.id}">
<div class="panel">
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon">
	{input type="button" name="restore_default" value="Restore Default Template" class="button" onClick="document.getElementById('default_template').value=DefaultText; document.getElementById('default_subject').value=DefaultSubject"}
	{input type="submit" name="submit" value="Save Template" class="button"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=alert_templates&root_tpl=blank');" alt="Help" title="Help">
</div>
<div class="panel">
	{if $vars.alert_vars}
	<fieldset class="highlighted">
		<legend>Insert an Alert Variable</legend>
			{foreach from=$vars.alert_vars item="var"}
            <input type="button" name="" class="button" value="{$var.label}" style="margin-top: 1px; margin-bottom: 1px; font-size: 9px; padding: 2px;" onClick="addText(document.getElementById(CurrentObjectFocus), '{ldelim}${$var.var_name}{rdelim}');" />
			{/foreach}
	</fieldset>
	{/if}
	{if $vars.object_vars}
	<fieldset class="highlighted">
		<legend>Insert an Object Variable</legend>
			{foreach from=$vars.object_vars item="var"}
			<input type="button" name="" class="button" value="{$var.field|mktitle}" title="{$var.description|escape:html}" style="margin-top: 1px; margin-bottom: 1px; font-size: 9px; padding: 2px;" onClick="addText(document.getElementById(CurrentObjectFocus), '{ldelim}$obj.{$var.field}{rdelim}');" />
			{/foreach}
	</fieldset>
	{/if}
</div>
<div class="panel">
<fieldset>
	<legend>&nbsp;<img src="/assets/icons/alert.png" width="16" height="16">&nbsp; <strong>Alert Message Template</strong>&nbsp;</legend>

	<table cellpadding="8" cellspacing="0" border="0">
		<tr>
		  <td valign="top">Email Subject:
          <input name="default_subject" type="text" id="default_subject" value="{$tpl.default_subject}" size="60" maxlength="64" onClick="ChangeFocus(this.id)" />		  </td>
      </tr>
		<tr>
			<td valign="top">
			
<textarea name="default_template" id="default_template" cols="70" rows="6" onClick="ChangeFocus(this.id)">{$tpl.default_template}</textarea>
			</td>
	    </tr>
	</table>

</fieldset>

</form>


{if $commands_ok == TRUE}
<fieldset>
	<legend>&nbsp;<img src="/assets/icons/alert_command.png" width="16" height="16">&nbsp; <strong>Alert Response Commands</strong>&nbsp;</legend>
	
	<table  border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td>&nbsp;&nbsp; Commands can optionally be associated with alert conditions.</td>
      </tr>
    </table>
    
    
    
    
    <div id="new_command" class="titlebar_expanded" onClick="toggle_panel(this)">
    	Create New Command
    </div>
    <div class="region, open">
	<form id="createForm" action="?module=mod_settings&action=process_create_alert_command&root_tpl=blank_panel" method="POST">
	<input type="hidden" name="alert_type_id" value="{$smarty.get.id}" />
	<table  border="0" cellspacing="0" cellpadding="4">
      <tr>
        <td><div align="right">Label: </div></td>
        <td><tt>
          <input value="{$smarty.post.label}" name="label" type="text" id="command_label" size="15">
        </tt></td>
        <td><img src="/assets/icons/thread_async.png" width="16" height="16" class="icon" alt="Process Command Asynchronously" title="Process Command Asynchronously" style="margin-right: -5px;">
        <input name="async" type="radio" value="true" {if $smarty.post.async == 'true'}checked{/if}>
        Process Asynchronously <a href="javascript:parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=alert_commands&root_tpl=blank');">[?]</a></td>
      </tr>
      <tr>
        <td><div align="right">&nbsp;</div></td>
        <td><tt>
          &nbsp;
        </tt></td>
        <td><img src="/assets/icons/thread_sync.png" width="16" height="16" class="icon" alt="Add Output to Alert" title="Add Output to Alert" style="margin-right: -5px;">
        <input name="async" type="radio" value="false" {if $smarty.post.async != 'true'}checked{/if}>
        Add Output to Alert <a href="javascript:parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=alert_commands&root_tpl=blank');">[?]</a></td>
      </tr>
      <tr>
	      <td><div align="right">Conditions:</div></td>
	      <td><img src="/assets/icons/icon_error.gif" width="16" height="16" class="icon" title="Perform on Failure" alt="Perform on Failure" /><input type="checkbox" name="perform_on_failure"  id="perform_on_failure"  checked value="true" /> Process on Failure</td>
	      <td>  		<img src="/assets/icons/up.gif" width="16" height="16" class="icon" title="Perform on Recovery" alt="Perform on Recovery" /><input type="checkbox" name="perform_on_recovery" id="perform_on_recovery" value="true" />Process on Recovery</td>
      </tr>
      <tr>
      	<td colspan="3" align="center">
      	<fieldset>
      		<legend>Command</legend>
      		<textarea rows="4" cols="70" name="command" id="command" onClick="ChangeFocus(this.id)">{$smarty.post.command}</textarea>
      	</fieldset>
      	</td>
      </tr>
      <!--
        <td><div align="right">Timeout:</div></td>
        <td>
          <input value="{$smarty.post.timeout|default:60}" name="timeout" type="text" id="command_timeout" size="5">
&nbsp;seconds</td>
	-->
	<tr>
        <td colspan="3" align="center"><input name="Save" type="button" id="Save" value="Create Command" onClick="ajax_create_command();">
        <img id="create_progress" src="/assets/progress.gif" class="progress" />
        </td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
    </table>
	</form>
	</div>
	
	
	
	
	<div style="visibility:invisible; display:none" id="editCommandContainer">
	<div id="edit_command" class="titlebar_expanded" onClick="toggle_panel(this)">
    	Edit Command
    </div>
    <div class="region, open">
	<form id="editForm">
	<input type="hidden" name="alert_type_id" value="{$smarty.get.id}" />
	<input id="command_id" type="hidden" name="id" value="" />
	<table  border="0" cellspacing="0" cellpadding="4">
      <tr>
        <td><div align="right">Label: </div></td>
        <td><tt>
          <input value="{$smarty.post.label}" name="label" type="text" id="edit_command_label" size="15">
        </tt></td>
        <td><img src="/assets/icons/thread_async.png" width="16" height="16" class="icon" alt="Process Command Asynchronously" title="Process Command Asynchronously" style="margin-right: -5px;">
        <input id="edit_async_true" name="async" type="radio" value="true" {if $smarty.post.async == 'true'}checked{/if}>
        Process Asynchronously <a href="#">[?]</a></td>
      </tr>
      <tr>
        <td><div align="right">&nbsp;</div></td>
        <td><tt>
          &nbsp;
        </tt></td>
        <td><img src="/assets/icons/thread_sync.png" width="16" height="16" class="icon" alt="Add Output to Alert" title="Add Output to Alert" style="margin-right: -5px;">
        <input id="edit_async_false" name="async" type="radio" value="false" {if $smarty.post.async != 'true'}checked{/if}>
        Add Output to Alert <a href="#">[?]</a></td>
      </tr>
      <tr>
      <td><div align="right">Conditions:</div></td>
      <td><img src="/assets/icons/icon_error.gif" width="16" height="16" class="icon" title="Perform on Failure" alt="Perform on Failure" /><input type="checkbox" name="perform_on_failure"  id="edit_perform_on_failure"  checked value="true" /> Process on Failure</td>
      <td>  		<img src="/assets/icons/up.gif" width="16" height="16" class="icon" title="Perform on Recovery" alt="Perform on Recovery" /><input type="checkbox" name="perform_on_recovery" id="edit_perform_on_recovery" value="true" />Process on Recovery</td>
      </tr>
      
      <tr>
      	<td colspan="3" align="center">
      	<fieldset>
      		<legend>Command</legend>
      		<textarea rows="4" cols="70" name="command" id="edit_command_command" onClick="ChangeFocus(this.id)">{$smarty.post.command}</textarea>
      	</fieldset>
      	</td>
      </tr>
      
      <tr>
      <!--
        <td><div align="right">Timeout:</div></td>
        <td>
          <input value="{$smarty.post.timeout|default:60}" name="timeout" type="text" id="edit_command_timeout" size="5">
&nbsp;seconds</td>
	-->
        <td colspan="3" align="center"><input name="Save" type="button" onClick="ajax_update_command(document.getElementById('command_id').value);" id="Save" value="Update Command">
        <img id="update_progress" src="/assets/progress.gif" class="progress" />
        </td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
    </table>
	</form>
	</div>
	</div>
	
	
	
	<br />
	
	<div id="commandsList" class="datagrid">
	<p><span class="blue2">Retrieving commands, please wait...</span></p><img id="ajax_commands_progress" src="/assets/progress.gif" class="progress" style="display: inline; visibility: visible" />
	</div>
</fieldset>

</div>

<script language="Javascript">
{literal}
query = "/?module=mod_settings&action=ajax_get_alert_commands&root_tpl=blank&alert_type_id=" + alertTypeID;
YAHOO.util.Connect.asyncRequest('GET', query, {success: refresh_commands_table, failure: refresh_commands_table, argument: "ajax_commands_progress"});
{/literal}
</script>

{/if}
<!-- end of {$smarty.template} -->
