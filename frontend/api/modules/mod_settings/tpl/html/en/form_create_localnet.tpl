<!-- {$smarty.template} ($Id$) -->
{literal}
<script language="Javascript">
	function checkbox_update(checkbox, fieldname) {
		document.getElementById(fieldname).value = (checkbox.checked == true) ? "true" : "false";
	}
</script>
{/literal}
<div class="panel">
	<img src="assets/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="/assets/buttons/button_help.gif" alt="Help" width="24" height="24" class="icon" onClick="parent.showHelp('localnet_setup');" title="Show Help" />
</div>
<div class="panel">
<form action="?module=mod_settings&action=process_create_localnet&root_tpl=blank_panel" method="POST">
<input type="hidden" name="enable_snmp_discovery" id="enable_snmp_discovery" value="{if $smarty.post.enable_snmp_discovery == 'false'}false{else}true{/if}" />
<input type="hidden" name="enable_portscan"       id="enable_portscan"       value="{if $smarty.post.enable_portscan == 'false'}false{else}true{/if}" />
	<br />
	<div align="center"><strong>New Network Range </strong></div>
	<br />
	
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
	<tr>
			<td><div align="right">Label (e.g. VLAN1):</div></td>
			<td><input type="text" {param name="label" class="input"} value="{$smarty.post.label}"></td>
	  </tr>
		<tr>
			<td><div align="right">Starting IP Address:</div></td>
			<td><input type="text" {param name="network" class="input"} value="{$smarty.post.network}"> </td>
		</tr>
		<tr>
			<td><div align="right">Ending IP Address:</div></td>
			<td><input type="text" {param name="broadcast" class="input"} value="{$smarty.post.broadcast}"></td>
		</tr>
		<tr>
			<td><div align="right">Enable SNMP AutoDiscovery:</div></td>
			<td><input type="checkbox" name="enable_snmp_discovery_chk" onchange="checkbox_update(this, 'enable_snmp_discovery');" checked /></td>
		</tr>
		<tr>
			<td><div align="right">Enable Background Port Scans:</div></td>
			<td><input type="checkbox" name="enable_portscan_chk" onchange="checkbox_update(this, 'enable_portscan');" checked /></td>
		</tr>

	</table>

	<p align="center">{input class="button" type="submit" value="Add Network Range "}</p>

</form>
</div>
<!-- end of {$smarty.template} -->
