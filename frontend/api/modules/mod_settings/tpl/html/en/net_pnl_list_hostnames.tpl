<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">
init_sort = function() {
	document.getElementById("results").className = 'sort-table';
	var types = [null, 'CaseInsensitiveString', 'CaseInsensitiveString', null];
	var report = new SortableTable(document.getElementById("results"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}
</script>
{/literal}
{if $smarty.get.search}
	{capture assign="host"}{$smarty.post.search_key|default:"Any Host"}{/capture}
	{capture assign="type"}{$smarty.post.filter|translate_type} Sources{/capture}

	{if $hosts}
		<div class="datagrid center">
			<table id="results" width="100%" border="0" cellpadding="3" cellspacing="0">
				<thead>
				<tr>
					<td width="16">&nbsp;</td>
					<td>Name</td>
					<td>Source</td>
					<td>Action</td>
				</tr>
				</thead>
				{foreach from=$hosts item="host"}
				<tr>
					<td width="16"><a href="?module=mod_settings&action=form_edit_host&id={$host.id}"><img border="0" title="{$host.node_type|capitalize}" alt="Host Type" src="assets/icons/vne_{$host.node_type|default:"default"}.gif"></a></td>
					<td><div title="{$host.hostname}"><a href="?module=mod_settings&action=form_edit_host&id={$host.id}">{$host.hostname|truncate:32}</a></div></td>
					<td>{$host.host_name_type|translate_type}</td>
					<td><a href="?module=mod_settings&action=delete_host&id={$host.id}&context=network_panel">Del</a></td>
				</tr>
				{/foreach}
			</table>
		</div>
	{else}
		{"Could not find hosts matching your search criteria."|message_bar}
		{madnet_action module="mod_settings" action="form_create_hostname"}
	{/if}
{/if}


<!-- end of {$smarty.template} -->
