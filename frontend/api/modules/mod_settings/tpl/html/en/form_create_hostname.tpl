<!-- {$smarty.template} ($Id$) -->

{literal}
<script language="Javascript">

function update_preview() {
	document.getElementById('img_preview').src = "assets/icons/vne_" + document.host_form.node_type.value + ".gif";
}

</script>
{/literal}


<div class="panel">
<form action="?module=mod_settings&action=process_create_hostname" method="POST" name="host_form">
	<br />
	<div align="center"><strong>New Custom Hostname</strong></div>
	<br />
	
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<td><div align="right">Hostname:</div></td>
			<td><input type="text" {param name="hostname" class="input"} value="{$smarty.post.hostname|default:$hostname|default:""}"> </td>
		</tr>
		<tr>
			<td><div align="right">IP Address:</div></td>
			<td><input type="text" {param name="ip" class="input"} value="{$smarty.post.ip|default:$ip_address|default:""}"></td>
		</tr>
		<tr>
			<td><div align="right">Node Type:</div></td>
			<td>
				<select name="node_type" onchange="update_preview();">
					{html_options options=$node_types selected=$smarty.post.node_type}
				</select>
			</td>
		</tr>
		<tr>
			<td><div align="right">Preview</div></td>
			<td><img id="img_preview" src="assets/icons/icon_default.gif"></td>
		</tr>
	</table>

	<p align="center">{input class="button" type="submit" value="Add Hostname"}</p>

</form>
</div>

<script language="Javascript">
update_preview();
</script>

<!-- end of {$smarty.template} -->
