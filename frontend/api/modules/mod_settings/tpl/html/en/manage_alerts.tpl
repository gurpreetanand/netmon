{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,ext/adapter/ext/ext-base,ext/ext-all-debug"}

<div class="titlebar">Alert Management</div>

<div class="panel">
{if $alerts}
<div id="alerts" class="datagrid">
<form id="bam_form" method="POST" action="?module=mod_alerts&action=bulk_manage&root_tpl=blank_panel">
<input type="checkbox" name="select_all" id="select_all" onclick="toggle_all_alerts();" />
<label for="select_all">Toggle all checkboxes</label>
<div style="float: right" display="inline">
<input type="submit" id="pause_selected_btn" name="submit" value="Pause Selected" onClick="document.getElementById('action_fld').value = 'pause'; return true;" />
<input type="submit" id="resume_selected_btn" name="submit" value="Resume Selected" onClick="document.getElementById('action_fld').value = 'resume'; return true;" />
<input type="submit" id="delete_selected_btn" name="submit" value="Delete Selected" onClick="document.getElementById('action_fld').value = 'delete'; return confirm('Are you sure you wish to delete the selected alerts?');" />
<input id="action_fld" type="hidden" name="action" value="" />
</div>
<table id="alerts" width="100%" align="center" border="0" cellspacing="3" cellpadding="3">
{foreach from=$alerts item="alert"}
<tr id="alert_{$alert.trigger_id}">
	<td valign="top">
	<p><span style="color: #0066CC; font-weight: bold; font-family: Tahoma, sans-serif; font-size: 11px;"><input id="trigger_{$alert.trigger_id}" type="checkbox" name="trigger_ids[]" value="{$alert.trigger_id}" /><label for="trigger_{$alert.trigger_id}">{$types[$alert.type]|capitalize} Alert{if $alert.label}: <span class="inlineHeading">{$alert.label}</span>{/if}</label></span></p>
	
	<div style="float: right" display="inline">
		<a href="javascript:parent.pnl_right.showEditor('?module=mod_alerts&action=form_edit_alert&trigger_id={$alert.trigger_id}');">Edit</a> |
		<a href="javascript:delete_alert({$alert.trigger_id});">Delete</a> |
		<a id="action_{$alert.trigger_id}" href="javascript:toggle({$alert.trigger_id});">{if $alert.active == 't'}Pause{else}Resume{/if}</a>
		<br />
		<a href="javascript:parent.pnl_right.showEditor('?module=mod_alerts&action=form_schedule_maintenance&trigger_id={$alert.trigger_id}')">Schedule Maintenance</a>
	</div>
	
	<p><span class="inlineHeading">Trigger Condition:</span>{$alert.condition}</p>
	<p><span class="inlineHeading">Alert Recipient:</span>{$alert.recipient}  ({$alert.media})</p>
	<p><span class="inlineHeading">Maintenance Schedule:</span>
	<div>
	{if $alert.maintenance}
	{foreach from=$alert.maintenance item="maintenance"}
	{$maintenance.condition} [<a href="javascript:parent.pnl_right.showEditor('?module=mod_alerts&action=delete_alert_maintenance&id={$maintenance.id}')">delete</a>]<br />
	{/foreach}
	{else}
	No maintenance scheduled
	{/if}
	</div>
	</p>
	</td>
	</tr>
{/foreach}
</table>

</form>
</div>
{else}
{"There are no alerts in the system at the moment."|message_bar}
{/if}
</div>
{literal}
<script language="Javascript">
var toggle = function(trigger_id) {
	YAHOO.util.Connect.asyncRequest('GET', 
		'?module=mod_alerts&action=toggle_alert&trigger_id='+trigger_id+'root_tpl=blank', 
		{'success': function(o) {
			if ('OK' == o.responseText) {
				var action_link = document.getElementById('action_'+o.argument['trigger_id']);
				action_link.innerHTML = (('Pause' == action_link.innerHTML) && 'Resume' || 'Pause') ;
			} else {
			 	alert("Unable to toggle the alert. Server response: " + o.responseText);
			}
		},
		 'failure': function(o) {alert('Unable to toggle the status of this alert');},
		 argument: {'trigger_id': trigger_id}
		 }
	);
}


var delete_alert = function(trigger_id) {
	if (confirm('Are you sure you wish to delete this alert?')) {
		YAHOO.util.Connect.asyncRequest('GET', 
			'?module=mod_alerts&action=ajax_delete_alert&trigger_id='+trigger_id+'&root_tpl=blank', 
			{'success': function(o) {
				if ('OK' == o.responseText) {
					row = document.getElementById('alert_'+o.argument.trigger_id);
					row.parentNode.deleteRow(row.rowIndex);
				} else {
				 	alert("Unable to toggle the alert. Server response: " + o.responseText);
				}
			},
			 'failure': function(o) {alert('Unable to toggle the status of this alert');},
			 argument: {'trigger_id': trigger_id}
			 }
		);
	}
}

var toggle_all_alerts = function() {
	var checked = Ext.get('select_all').dom.checked;
	
	Ext.select('#alerts input[type=checkbox]').each(function(el, comp, idx) {
		el.dom.checked = checked;
	} );
	//console.log(boxes);
	//boxes.checked = true;
}

var pause_selected = function() {
	alert('Coming soon...');
}

var delete_selected = function() {
	alert('Coming soon...');
}




</script>
{/literal}
