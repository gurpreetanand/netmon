<div class="titlebar">Device Slot Usage Report</div>
{capture assign="size"}{array_size array=$devices}{/capture}
{"You are using $size device slots"|message_bar}
<div class="white">
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<td><strong>Device IP</strong></td>
			<td><strong>SNMP Agents</strong></td>
			<td><strong>UNIX Partitions</strong></td>
			<td><strong>Windows Partitions</strong></td>
			<td><strong>TCP/ICMP Trackers</strong></td>
			<td><strong>Syslog Servers</strong></td>
		</tr>
	</thead>
	<tbody>
	{foreach from=$devices item="device"}
		<tr bgcolor="{cycle values='#eeeeee,#ffffff'}">
			<td>{$device.ip}</td>
			<td align="center"><img src="/assets/images/check/check{if $device.devices == 1}2{else}0{/if}.gif" /></td>
			<td align="center"><img src="/assets/images/check/check{if $device.df_servers == 1}2{else}0{/if}.gif" /></td>
			<td align="center"><img src="/assets/images/check/check{if $device.smb_servers == 1}2{else}0{/if}.gif" /></td>
			<td align="center"><img src="/assets/images/check/check{if $device.servers == 1}2{else}0{/if}.gif" /></td>
			<td align="center"><img src="/assets/images/check/check{if $device.syslog_access == 1}2{else}0{/if}.gif" /></td>
		</tr>
	{/foreach}

	</tbody>
</table>
</div>