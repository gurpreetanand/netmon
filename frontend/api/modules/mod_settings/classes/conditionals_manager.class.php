<?php


class Conditionals_Manager extends MadnetElement {
	
	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "conditionals";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "cond_id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_settings";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;
	
	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;
	
	function init() {
		$this->params->add_primitive("ip",     "ip_address",           TRUE,   "IP Address",      "IP Address");
		$this->params->add_primitive("name",   "string",               TRUE,   "Name",            "Name");
	}
	
	
	
	/**
	  * Returns an array containing the user ID of every user account in the DB
	  *
	  * @return mixed
	  */
	function get_all_ids() {
		$query = "SELECT {$this->pkey} AS \"id\", ip, name FROM {$this->table} ORDER BY name, ip ASC";
		$result = $this->db->select($query);
		
		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			return $result;
		}
	}
	
	function pre_insert($id = null) {
		
		$query = "SELECT {$this->pkey} FROM {$this->table} WHERE ip = " . 
				 $this->db->escape($this->params->getVal("ip")) .
		         " AND name = " . $this->db->escape($this->params->getVal("name"));
		         
		if ($id) { $query .= "AND {$this->pkey} != " . $this->db->escape($id); }
		
		
		$result = $this->db->select($query);
		
		
		
		if ((!is_array($result)) || ((is_array($result)) && (sizeof($result) == 0))) {
			return TRUE;
		} else {
			$this->err->err_from_string("A conditional with the same parameters already exists");
			return FALSE;
		}
	}
	
	
	function pre_update($id) {
		return $this->pre_insert($id);
	}
	
	
}


?>