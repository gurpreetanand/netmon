<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * $Id$
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


// mod_settings.class.php

/**
  * Settings MCO
  *
  * This module is used to perform any application-level change and management
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class mod_settings extends MadnetModule {

  /**
    * Initializes all module variables
    *
    * @return void
    */
  function init() {
    $this->_set_module(__CLASS__);
    require_class($this->module, "localnets_manager");
    require_class($this->module, "conditionals_manager");
    require_class($this->module, "backup_manager");

    # XML Unserializer options
    $this->options = array(
      'complexType'       => 'array',
      'parseAttributes'   => TRUE,
      'attributesArray'   => FALSE,
      'contentName'       => 'panels'
    );
  }


  /** Presentation Layer **/
  function get_settings_tree(&$index_content) {

    /*************  ALERT TEMPLATES  *************/

    $query = "SELECT id, description FROM alert_types ORDER BY description ASC";
    $res = $this->db->select($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      $this->err->handle_fatal("Unable to fetch a list of alert template types");
    }

    /**********  END OF ALERT TEMPLATES  **********/

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("alert_types", $res);
    $index_content .= $parser->fetch("settings_tree.tpl");
    return '';
  }

  function settings_editor(&$index_content) {
    $parser = new Parser($this->tpl_dir);

    $index_content .= $parser->fetch("settings_editor.tpl");
    return "Settings Editor";
  }

  /** Local Networks Settings Management **/
  function get_localnets() {
    $lm = new LocalNets_Manager;
    $ids = $lm->get_all_ids();
    
    return $ids;
  }
  
  function get_settings() {
    $query = "select d.name, d.start_auto, d.component_type, c.* from
              daemons d left join daemonsconfig c on c.daemon_id=d.id";
    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $this->err->err_from_string("Unable to retrive list of startup interfaces for that service. Aborting");
      return '[]';
    }

    $payload = array();

    foreach ($res as $row) {
      if (!$payload[$row['name']]) {
        $payload[$row['name']] = array();
      }
      $payload[$row['name']][$row['var']] = $row;
    }



    print json_encode($payload);
    return;
  }
  
  function get_users_object() {
    $res = $this->db->select("SELECT id, first_name, last_name, username, email FROM users");
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    }
    return $res;
  }
  
  function get_network_info() {
    $interfaces = `python /apache/cli/intmgr.py -g`;
    $payload = array(
      'interfaces' => json_decode($interfaces, true),
      'localnets' => $this->get_localnets(),
      'users' => $this->get_users_object(),
      'tunnel' => $this->get_open_tunnel()
    );
    print json_encode($payload);
  }

  function process_create_localnet() {

    $lm = new LocalNets_Manager();

    if ($lm->insert()) {
      print '{}';
    }
    print '{}';
  }

  function delete_localnet() {
    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_code(400, "A local network ID was not passed along with this request.");
      return "Error";
    }


    $lm = new LocalNets_Manager();

    if (!$lm->delete($id)) {
      $this->err->err_from_code(400, "Unable to delete local network #$id");
      return "Error";
    }

    print '{}';
  }

  function form_create_localnet(&$index_content) {

    $lm = new LocalNets_Manager();
    $parser = new Parser($this->tpl_dir);
    $parser->set_params($lm->params);
    $index_content .= $parser->fetch("form_create_localnet.tpl");
    return;
  }
  
  /**
   * Edits a local network entry
   *
   * @param pointer $index_content
   */
  function form_edit_localnet(&$index_content) {
    $id = intval($_GET['id']);
    
    if ($id <= 0) {
      $this->err->err_from_string("Invalid network identifier specified. Aborting.");
      return;
    }
    
    
    $lm = new LocalNets_Manager();
    
    if (!$lm->fetch($id)) {
      $this->err->err_from_string("Unable to retrieve the specified network entry. Aborting.");
      return;
    }
    
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("lm", $lm);
    $parser->set_params($lm->params);
    $index_content .= $parser->fetch("form_edit_localnet.tpl");
    
  }
  
  /**
   * Updates a local network entry
   *
   * @param pointer $index_content
   */
  function process_update_localnet(&$index_content) {
    $id = intval($_GET['id']);
    
    if ($id <= 0) {
      $this->err->err_from_code(400, "Invalid network identifier specified. Aborting.");
      return;
    }
    
    
    $lm = new LocalNets_Manager();
    
    if ($lm->update($id)) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to update localnet");
    }
  }


  /** Conditionals Management **/
  function get_conditionals(&$index_content) {

    $cm = new Conditionals_Manager();
    $ids = $cm->get_all_ids();
    print json_encode($ids);
    return;
  }


  function process_create_conditional() {

    $cm = new Conditionals_Manager();

    if ($cm->insert()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to create conditional");
    }
  }

  function delete_conditional(&$index_content) {
    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("A Conditional ID was not passed along with this request.");
      return "Error";
    }


    $cm = new Conditionals_Manager();

    if (!$cm->delete($id)) {
      $this->err->err_from_code(400, "Unable to delete conditional #$id");
      return "Error";
    }

    print '{}';
  }

  function form_create_conditional(&$index_content) {

    $cm = new Conditionals_Manager();
    $parser = new Parser($this->tpl_dir);
    $parser->set_params($cm->params);
    $index_content .= $parser->fetch("form_create_conditional.tpl");
    return;
  }

  /** Backups Management **/
  
  
  /**
   * Outputs an XML document describing SMB shares associated to an IP address
   *
   * @param pointer $index_content
   */
  function ajax_get_smb_shares_xml(&$index_content) {
    $username   = $_GET['username'] ? $_GET['username'] : "guest";
    $password   = $_GET['password'] ? $_GET['password'] : "guest";
    $domain     = $_GET['domain'];
    $ip_address = $_GET['ip_address'];
    
    $validator = $this->registry->get_singleton("core", "validator");
    
    if (!Validator::validate_ip_address($_GET['ip_address'])) {
      return;
    }
    
    $bm = new Backup_Manager();
    $shares = $bm->find_smb_shares($ip_address, $username, $password, $domain);
    
    if (sizeof($shares) == 0) {
      return;
    }
    
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("shares", $shares);
    $index_content .= $parser->fetch("ajax_get_smb_shares_xml.tpl");
    header("Content-type: application/xml");
    return;
    
  }
  
  /**
   * Outputs an XML document representing all DB tables
   *
   * @param pointer $index_content
   */
  function ajax_get_db_tables(&$index_content) {
    $bm = new Backup_Manager();
    $index_content = $bm->retrieve_xml_table_structure();
    header("Content-type: application/xml");
  }
  
  /**
   * Returns an XML document representing all events pertaining
   * to the specified backup that have been recorded since a specified
   * timestamp.
   *
   * @param pointer $index_content
   */
  function ajax_get_backup_events(&$index_content) {
    $bm = new Backup_Manager();
    $backup = $bm->get_backup_status(intval($_GET['id']), intval($_GET['last_timestamp']));
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("backup", $backup);
    $index_content .= $parser->fetch("xml_backup_events.tpl");
    
    header("Content-type: application/xml");
    return;
  }
  
  /**
   * Displays the size of a DB table
   *
   * @param pointer $index_content
   */
  function ajax_get_table_size(&$index_content) {
    
    if (has_perm("Guest/Demo Account")) {
      #$index_content .= '0';
      return;
    }

    
    $bm = new Backup_Manager();
    $parser = new Parser($this->tpl_dir);
    
    $size = $bm->_get_table_size($_GET['tablename']);
    
    if (is_numeric($size)) {
      $size = $parser->translate_capacity($size);
    }

    $index_content .= $size;
    return;
  }
  
  /**
   * Returns a list of all users
   *
   * @return array
   */
  function _get_backup_users() {
    $query = "SELECT email, (COALESCE(last_name, '') || ', '::varchar || COALESCE(first_name, '')) AS \"name\" FROM users ORDER BY name ASC";
    $res = $this->db->select_smarty($query, "email", "name");
    return $res ? $res : array();
  }
  
  /**
   * Displays the backup set-up form
   *
   * @param pointer $index_content
   */
  function form_create_backup(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $parser->assign("users", $this->_get_backup_users());
    $index_content .= $parser->fetch("form_create_backup.tpl");
  }
  
  /**
   * Triggers the actual backup process
   *
   * @param pointer $index_content
   */
  function launch_backup_process() {
  
    $input = json_decode(file_get_contents("php://input"), true);
    
    if (has_perm("Guest/Demo Account")) {
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
    }
    
    
    $bm = new Backup_Manager();
    
    # Gather (of infer) start/stop timestamps.
    $from_parts = (strcmp($input['all_time'], 'true') == 0) ? -1 : explode("/", $input['date_from']);
    $to_parts = (strcmp($input['all_time'], 'true') == 0) ? -1 : explode("/", $input['date_to']);
    
    if (strcmp($input['all_time'], 'true') == 0) {
      $from = -1;
      $to = mktime();
    } else {
      $from_parts = explode("/", $input['date_from']);
      $to_parts = explode("/", $input['date_to']);
      
      $from = mktime(0,0,0, $from_parts[1], $from_parts[0], $from_parts[2]);
      $to = mktime(0,0,0, $to_parts[1], $to_parts[0], $to_parts[2]);
    }
    
    # Register options
    $archive = (strcmp($input['archive_mode'], "true") == 0) ? TRUE : FALSE;
    $options = array(
      "from" => $from, 
      "to" => $to, 
      "archive" => $archive, 
      "notify" => $input['notify'],
      "owner" => $_SESSION['email']
    );
    
    # Register the backup destination
    switch($input['destination']) {
      case 'dest_div_local':
        $target = $bm->get_local($tables, $input['label']);
        break;
      case 'dest_div_smb':
        $target = $bm->get_smb_share($input['smb_ip_address'],
                       $input['smb_share'],
                       $input['smb_username'],
                       $input['smb_domain'],
                       $input['smb_password'],
                       $tables);
        break;
      default:
        $this->err->err_from_code(400, "Invalid destination.");
        return;
    }
    
    # Launch the actual backup process.
    $id = $bm->init_backup($target, $tables, $options);
    
    
    if ($id == FALSE) {
      $this->err->err_from_code(400, "The backup process did not return a tracking ID");
      return;
    }

    print '{}';
  }
  
  /**
   * Displays the ajax-based backup monitor dialog
   *
   * @param pointer $index_content
   */
  function monitor_backup_process(&$index_content, $id = NULL) {
    $id = (intval($id) > 0) ? $id : $_GET['id'];
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign("id", $id);
    $index_content .= $parser->fetch("monitor_backup_process.tpl");
    return;
  }
  
  /**
   * Displays a list of available backup processes.
   *
   * @param pointer $index_content
   */
  function get_backups(&$index_content) {
    $bm = new Backup_Manager();
    $backups = $bm->get_backup_entities();

    if (FALSE == $backups) {
      $this->err->err_from_string("There are no traces of previous backups on this system");
      return '{}';
    }

    require_class("mod_vfs", "File_Manager");
    $fm = new File_Manager();

    $fm->sync_directory(2);

    $query = "SELECT id, email, (COALESCE(last_name, '') || ', '::varchar || COALESCE(first_name, '')) AS \"name\" FROM users WHERE email is not null ORDER BY name ASC";
    $res = $this->db->select($query);

    $payload = array(
      "files" => $fm->get_dir_files(2),
      "backups" => $backups,
      "users" => $res
    );
    print json_encode($payload);
  }
  
  /**
   * Removes a backup process entry from the DB
   *
   * @param pointer $index_content
   */
  function clear_backup_process() {
    
    if (has_perm("Guest/Demo Account")) {
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
      print '{}';
      return;
    }
    
    
    $id = intval($_GET['id']);
    
    if ($id <= 0) {
      $this->err->err_from_code(400, "Invalid backup identifier specified. Aborting");
      print '{}';
      return;
    }
    
    $bm = new Backup_Manager();
    $bm->clear($id);

    print '{}';
    return;
  }
  
  function get_traffic_filters(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $index_content .= $parser->fetch("traffic_filters.tpl");
    return "Traffic Filters";
  }

  function get_filter_xml() {
    $filter = $_GET['filter'];
    if (!$filter) {
      $filter = 'host';
    }
    $file = "/apache/registry/filters/" . $filter . ".xml";

    # This will be loaded inside a flash widget, not a template so no error control.
    if (!file_exists($file)) {
      die("Unable to locate filter");
    }

    print file_get_contents($file);
    return;
  }

  function update_filter_xml() {
    $filter = $_GET['filter'];
    if (!$filter) {
      $filter = 'host';
    }    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
      return FALSE;
    }
    
    $file = "/apache/registry/filters/" . $filter . ".xml";

    if (!is_writable($file)) {
      $this->err->err_from_code(400, "Filter is not writable");
    }

    $data = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $_POST['xml_stream']);
    
    # This will be loaded inside a flash widget, not a template so no error control.
    if (!$fp = fopen($file, "w")) {
      $this->err->err_from_code(400, "Unable to open filter file");
    }

    $res = fwrite($fp, $data);

    fclose($fp);

    print '{}';
  }

  function get_host_filters(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $index_content .= $parser->fetch("hosts_filters.tpl");
    return "Host Filters";
  }



  /** Hostname Database Management **/

  function manage_hostnames() {
    if ($_GET['search']) {
      require_class($this->module, "Hostnames_Manager");
      $hm = new Hostnames_Manager();

      $hosts = $hm->host_search($_REQUEST['filter'], $_REQUEST['search_key']);
      print json_encode($hosts);
    } else {
      print '{}';
    }
  }

  function delete_host(&$index_content) {

    require_class($this->module, "Hostnames_Manager");
    $hm = new Hostnames_Manager();

    if ($hm->delete(intval($_GET['id']))) {
      print '{}';
    }

  }

  function delete_hosts() {
    require_class($this->module, "Hostnames_Manager");
    $hm = new Hostnames_Manager();

    foreach (explode(",", $_GET['ids']) as $id) {
      $hm->delete(intval($id));
    }
    print '{}';
  }

  function form_create_hostname(&$index_content) {
    $parser = new Parser($this->tpl_dir);

    require_class($this->module, "Hostnames_Manager");
    $hm = new Hostnames_Manager();
    $parser->set_params($hm->params);

    $parser->assign_by_ref("node_types", $this->_get_node_types_hash());

    $validator = &$this->registry->get_singleton("core", "validator");

    if ($validator->validate("ip_address", $_GET['name'])) {
      $parser->assign("ip_address", $_GET['name']);
    } else {
      $parser->assign("hostname", $_GET['name']);
    }

    $index_content .= $parser->fetch("form_create_hostname.tpl");
  }

  function process_create_hostname(&$index_content) {
    require_class($this->module, "Hostnames_Manager");
    $hm = new Hostnames_Manager();
    if ($hm->insert()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to insert hostname");
    }
  }


  function get_protocols() {
    require_class($this->module, "Protocols_Manager");
    $pm = new Protocols_Manager();
    print json_encode($pm->get_all_ids(), true);
  }

  function form_create_protocol(&$index_content) {
    require_class("mod_settings", "protocols_manager");
    $pm = new Protocols_Manager();

    $parser = new Parser($this->tpl_dir);
    $parser->set_params($pm->params);

    $index_content .= $parser->fetch("form_create_protocol.tpl");
    return "Create New Protocol Entry";
  }

  function process_create_protocol() {
    require_class($this->module, "protocols_manager");

    $pm = new Protocols_Manager();

    if ($pm->insert()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to create protocol");
    }
  }
  
  function process_update_interfaces() {
    $input = file_get_contents("php://input");
    $json = json_decode($input, true);

    $cmd = "sudo python /apache/cli/intmgr.py -s " . escapeshellarg($input);
    $status = `$cmd`;
    $cmd = "sudo ifdown " . escapeshellarg($json['name']);
    $status = `$cmd`;
    $cmd = "sudo ifup " . escapeshellarg($json['name']);
    $status = `$cmd`;

    print '{}';
  }

  function form_edit_host(&$index_content) {
    require_class("mod_settings", "hostnames_manager");
    $parser = new Parser($this->tpl_dir);
    $hm = new Hostnames_Manager();

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("A hostname identifier has been lost between HTTP requests");
      return;
    }

    if (!$hm->pop($id)) {
      $this->err->err_from_string("Unable to fetch hostname entry");
      return;
    }

    $parser->assign_by_ref("node_types", $this->_get_node_types_hash());
    $parser->set_params($hm->params);
    $parser->assign_by_ref("host", &$hm);

    $index_content .= $parser->fetch("form_edit_hostname.tpl");
    return;

  }

  function form_edit_protocol(&$index_content) {
    require_class("mod_settings", "protocols_manager");

    $parser = new Parser($this->tpl_dir);

    $pm = new Protocols_Manager();


    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("A protocol ID was not passed along with this request.");
      return "Error";
    }

    if (!$pm->pop($id)) {
      $this->err->err_from_string("Unable to fetch protocol information");
      return "Error";
    }

    $parser->set_params($pm->params);
    $parser->assign_by_ref("pm", &$pm);

    $index_content .= $parser->fetch("form_edit_protocol.tpl");
    return "Edit Protocol Entry";
  }


  function process_update_hostname() {
    require_class("mod_settings", "hostnames_manager");


    $hm = new Hostnames_Manager();


    if ($hm->update()) {
      print '{}';
    } else {
      print '{"error": "Could not update"}';
    }
  }

  function update_protocol(&$index_content) {
    require_class("mod_settings", "protocols_manager");

    $pm = new Protocols_Manager();

    if ($pm->update()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to update port");
    }
  }

  function process_delete_protocol() {
    require_class("mod_settings", "protocols_manager");
    $pm = new Protocols_Manager();


    $id = intval($_GET['id']);

    if ($id == 0) {
      $this->err->err_from_code(400, "An identifier has been lost between HTTP requests. This must be an application error. Please report a bug.");
    }

    if ($pm->delete($id)) {
      print '{}';
    } else { 
      $this->err->err_from_code(400, "Unable to delete protocol");
    }
  }


  /** Services Management **/

  function manage_netmon_services(&$index_content) {

    $data = $this->_get_services();
    $parser = new Parser($this->tpl_dir);
    
    $payload = array(
      "services" => $data['services']['service'],
      "interfaces" => $data['running_interfaces']['interface']
    );

    print json_encode($payload, true);
  }
  
  function _get_services() {
    $this->debugger->add_hit("Sending signal 'status' to procmond");
    $xml = $this->_sig_procmond("status");
    
    # Load the XML Unserializer in memory
    require_once "XML/Unserializer.php";

    # Initialize the XML Unserializer object
    $unserializer = new XML_Unserializer($this->options);


    # Unserialize the XML structure
    $op = $unserializer->unserialize($xml, FALSE);

    # Error control
    if (PEAR::isError($op)) {
      $this->err->err_from_string("Unable to unserialize XML data: " . $op->getUserInfo());
      return FALSE;
    }

    $data = $unserializer->getUnserializedData();
    $this->debugger->add_hit("Services Data:", NULL, NULL, vdump($data));

    return $data;
  }

  function start_service() {
    $process = $_GET['service'];
    $result = $this->_sig_procmond("start $process");

    if (FALSE === $result) {
      $this->err->err_from_code(400, "Unable to start the specified service.");
    } else {
      $query = "UPDATE daemons SET start_auto = 't' WHERE name = " . $this->db->escape($_GET['service']);
      $res = $this->db->update($query);
      print '{}';
    }
  }
  
  /**
   * Starts a plugin against a specific interface
   *
   * @param pointer $index_content
   */
  function start_plugin(&$index_content) {
    
    /**
     * Retrieve a list of interfaces currently running for that plugin
     */
    $services = $this->_get_services();
    $running_ifaces = array();
    
    foreach($services['services']['service'] as $service) {
      if (is_array($service['plugins'])) {
        foreach($service['plugins']['plugin'] as $plugin) {
          if (strcmp($plugin['name'], $_GET['plugin']) == 0) {
            if (is_array($plugin['interfaces']['interface'])) {
              $ifaces = $plugin['interfaces']['interface'];
            } else {
              $ifaces = $plugin['interfaces'];
            }
            foreach($ifaces as $interface) {
              if (strcmp($interface['status'], 'up') == 0) {
                $this->debugger->add_hit("Interface " . $interface['iface'] . " is up");
                array_push($running_ifaces, $interface['iface']);
              }
            }
          }
        }
      }
    }
  
    /**
     * Find out if the specified interface is already running
     */
    
    if (in_array($_GET['iface'], $running_ifaces)) {
      $this->err->err_from_code(400, "The plugin " . htmlentities($_GET['plugin']) . " is already running on the interface " . htmlentities($_GET['iface']));
    }
    
    /**
     * Add the selected interface to the list
     */
    
    array_push($running_ifaces, $_GET['iface']);
    
    /**
     * Retrieve the list of interfaces actually set up for boot-time loading
     * (this could very well be different from what is currently running)
     */
    $query = "SELECT start_ifaces FROM plugins WHERE name = " . $this->db->escape($_GET['plugin']);
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $this->err->err_from_code(400, "Unable to retrive list of startup interfaces for that service. Aborting");
      return;
    }
    
    $startup_ifaces = $res['start_ifaces'];
    
    /**
     * Replace start_interfaces with the new list
     */
    $new_startup_ifaces = join(",", $running_ifaces);
    $query = "UPDATE plugins SET start_ifaces = " . $this->db->escape($new_startup_ifaces) . " WHERE name = " . $this->db->escape($_GET['plugin']);
    
    $res = $this->db->update($query);
    
    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_code(400, "Unable to update plugin state. Aborting");
      return;
    }
    
    /**
     * Retart Netmond
     */
    $this->_sig_procmond("restart netmond");

    /**
     * Return the services management dialog
     */
    print '{}';
  }
  
  
  /**
   * Stops a plugin for a specific interface
   *
   * @param pointer $index_content
   */
  function stop_plugin(&$index_content) {
    
    //plugin - iface
    
    /**
     * Retrieve a list of interfaces currently running for that plugin
     */
    $services = $this->_get_services();
    $running_ifaces = array();
    
    foreach($services['services']['service'] as $service) {
      if (is_array($service['plugins'])) {
        foreach($service['plugins']['plugin'] as $plugin) {
          if (strcmp($plugin['name'], $_GET['plugin']) == 0) {
            if (is_array($plugin['interfaces']['interface'][0])) {
              $ifaces = $plugin['interfaces']['interface'];
              $this->debugger->add_hit("Multiple interfaces detected");
            } else {
              $ifaces = $plugin['interfaces'];
              $this->debugger->add_hit("Single interface detected");
            }
            foreach($ifaces as $interface) {
              if (strcmp($interface['status'], 'up') == 0) {
                $this->debugger->add_hit("Interface " . $interface['iface'] . " is up");
                array_push($running_ifaces, $interface['iface']);
              } else {
                $this->debugger->add_hit("Interface " . $interface['iface'] . " is down");
              }
            }
          }
        }
      }
    }
    
    
    /**
     * Find out if the interface is actually loaded. If it isn't, no need to stop it.
     */
    $this->debugger->add_hit("Running interfaces", NULL, NULL, vdump($running_ifaces));
    if (!in_array($_GET['iface'], $running_ifaces)) {
      $this->err->err_from_code(400, "The plugin " . htmlentities($_GET['plugin']) . " is not running on the interface " . htmlentities($_GET['iface']));
      return;
    }
    
    /**
     * Remove the interface from the list
     */
    for ($i = 0; $i < sizeof($running_ifaces); $i++) {
      if (strcmp(($running_ifaces[$i]), $_GET['iface']) == 0) {
        unset($running_ifaces[$i]);
      }
    }
    
    
    /**
     * Get the list of interfaces to load on start-up and save them in a buffer
     */
    $query = "SELECT start_ifaces FROM plugins WHERE name = " . $this->db->escape($_GET['plugin']);
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $this->err->err_from_code(400, "Unable to retrive list of startup interfaces for that service. Aborting");
      return;
    }
    
    $startup_ifaces = $res['start_ifaces'];
    
    /**
     * Replace start_interfaces with the new list
     */
    $new_startup_ifaces = join(",", $running_ifaces);
    $query = "UPDATE plugins SET start_ifaces = " . $this->db->escape($new_startup_ifaces) . " WHERE name = " . $this->db->escape($_GET['plugin']);
    
    $res = $this->db->update($query);
    
    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_code(400, "Unable to update plugin state. Aborting");
      return;
    }
    
    /**
     * Restart netmond
     */
    $this->_sig_procmond("restart netmond");

    /**
     * Return the services management dialog
     */
    print '{}';
  }

  function stop_service(&$index_content) {
    $process = $_GET['service'];
    $result = $this->_sig_procmond("stop $process");

    if (FALSE === $result) {
      $this->err->err_from_code(400, "Unable to stop the specified service.");
    } else {
      $query = "UPDATE daemons SET start_auto = 'f' WHERE name = " . $this->db->escape($_GET['service']);
      $res = $this->db->update($query);
      print '{}';
    }
  }

  /** Private methods **/
  function _get_create_time($file) {
    $f_struct = file($file);
    return $f_struct[0];
  }

  function _convert_timestamp_to_ctime($timestamp) {
    return date("D M j H:i:s Y", $timestamp);
  }


  function _host_name_type_to_plain_english($type = NULL) {
    switch ($type) {
      case "NetBIOS": return "NetBIOS";
      case "CUSTOM":  return "User-defined";
      case "DNS":     return "DNS Query";
      case "HTTP":    return "HTTP Query";
      default:        return "All";
    }
  }

  /**
   * Changes the startup setting for a Netmon service
   *
   * @param pointer $index_content
   */
  function ajax_change_startup(&$index_content) {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }
    
    
    $auto = (strcmp($_GET['val'], "auto") == 0) ? 'true' : 'false';
    
    $query = "UPDATE daemons SET start_auto = $auto WHERE name = " . $this->db->escape($_GET['process']);
    $this->db->update($query);
    $this->_sig_procmond("stop " . $_GET['process']);
    $this->_sig_procmond("start " . $_GET['process']);
    sleep(2);
  }
  
  /**
   * Changes the startup setting of a Netmon pluggin
   *
   * @param pointer $index_content
   */
  function ajax_change_plugin_startup(&$index_content) {
    // plugin, iface, val
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }

    $query = "SELECT start_ifaces FROM plugins WHERE name = " . $this->db->escape($_GET['plugin']);
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $this->err->err_from_string("Unable to retrive list of startup interfaces for that service. Aborting");
      return;
    }
    
    $startup_ifaces = explode(',', str_replace(" ", "", $res['start_ifaces']));
    
    # Since nothing else appears to help, brute force it.
    
    for($i = 0; $i < sizeof($startup_ifaces); $i++) {
      if (strcmp($startup_ifaces[$i], '') == 0) {
        $this->debugger->add_hit("Removing entry $i");
        unset($startup_ifaces[$i]);
      }
    }
    
    
    
    $this->debugger->add_hit("Start Interfaces:", NULL, NULL, vdump($startup_ifaces));
    
    if (strcmp('auto', $_GET['val']) == 0) { 
      if (in_array($_GET['iface'], $startup_ifaces)) {
        // Nothing to be done
        return;
      } else {
        array_push($startup_ifaces, $_GET['iface']);
      }
    }
    
    if (strcmp('manual', $_GET['val']) == 0) {
      if (!in_array($_GET['iface'], $startup_ifaces)) {
        // Nothing to be done
        return;
      } else {
        for($i = 0; $i < sizeof($startup_ifaces); $i++) {
          if (strcmp($_GET['iface'], $startup_ifaces[$i]) == 0) {
            unset($startup_ifaces[$i]);
          }
        }
      }
    }
    $this->debugger->add_hit("Interfaces", NULL, NULL, vdump($startup_ifaces));
    $query = "UPDATE plugins SET start_ifaces = " . 
          $this->db->escape(join(",", $startup_ifaces)) . 
          " WHERE name = " . $this->db->escape($_GET['plugin']);
          
    $this->db->update($query);
    
    $this->_sig_procmond("stop netmond");
    $this->_sig_procmond("start netmond");
    sleep(2);
  }

  function _get_node_types_hash() {
    return array("default"     => "Default",
           "workstation" => "Workstation",
           "linux_generic" => "Linux Workstation",
           "windows_generic" => "Windows Workstation",
           "snmp" => "Generic SNMP Device",
           "barracuda_spam" => "Barracuda Appliance",
           "enviro" => "Enviro MINI Device",
           "printer_generic" => "Printer (Generic)",
           "printer_hp" => "Printer (HP)",
           "printer"     => "Printer",
           "ups_apc" => "UPS - APC",
           "windows_iis" => "IIS Server",
           "switch"      => "Network Switch",
           "firewall"    => "Firewall",
           "router"      => "Router");
  }


   #Sends a signal to a specific netmon daemon through procmond
  function _sig_procmond($signal) {
    
    if (($signal != 'status') && (has_perm("Guest/Demo Account"))) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }
    
    $errno  = 0;
    $errstr = "";
    # Establish a local socket connection to procmond
    if (phpversion() >= 5) {
      $sock_url = "unix:///var/run/.netmon.s";
    } else {
      $sock_url = "/var/run/.netmon.s";
    }
    
    $sock = fsockopen($sock_url, -1, $errno, $errstr, 10);

    if (FALSE === $sock) {
      $this->err->err_from_code(400, "Unable to establish a connection to the core process manager. Error: <strong>$errstr</strong><br />Using socket: <strong>$sock_url</strong><br /> Please contact the Netmon Helpdesk for assistance.");
      return;
    }

    # Marshalls the message to send to procmond
    $signal .= "\n";
    $this->debugger->add_hit("Signal: " . $signal);

    # Send the signal
    if (FALSE === fwrite($sock, $signal)) {
      $this->err->err_from_code(400, "Unable to send the $signal command to the core process manager.");
      @fclose($sock);
      return FALSE;
    }

    # The first 3 characters of the response should be 'OK ', followed by the response
    $response = '';

    $response .= fread($sock, 8192);

    $this->debugger->add_hit("Response for $signal", NULL, NULL, htmlentities($response));
    if (strcmp("OK", (trim(substr($response, 0, 3)))) == 0) {
      $response = trim(substr($response, 3));
    } else {
      $this->err->err_from_string("Error: " . substr($response, 3));
      $response = FALSE;
    }

    @fclose($sock);
    $this->debugger->add_hit("Socket connection has been terminated");
    

    # Return our new data structure
    return $response;
  }


  #Temporary placeholder function for alert template functions. Right now, it simply loads a mock template.
  function setup_alert_tpl(&$index_content) {    
    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("No alert type identifier has been passed to this request.");
      return;
    }

    $id = $this->db->escape($id);

    $query = "SELECT id, name, original_template, default_template, default_subject, description FROM alert_types WHERE id = $id";
    $tpl = $this->db->get_row($query);

    if ((DB_NO_RESULT == $tpl) || (DB_QUERY_ERROR == $tpl)) {
      $this->err->err_from_string("Unable to fetch a template for this alert");
    }
    
    $vars = $this->_get_alert_vars($id, $tpl['name']);
    $am = require_module("mod_alerts");

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("vars", $vars);
    $parser->assign_by_ref("tpl", $tpl);
    $parser->assign("commands_ok", in_array(strtolower($tpl['name']), $am->alert_types));
    $index_content .= $parser->fetch("edit_alert_template.tpl");
    return "Alert Template Editor";
  }
  
  function _get_alert_vars($id, $name) {
    $return_vars = array();
    
    /**
     * 1- Retrieve alert_vars entries from the DB
     */
    $query = "SELECT label, var_name FROM alert_vars WHERE alert_type_id = $id";
    $vars = $this->db->select($query);

    if ((DB_NO_RESULT == $vars) || (DB_QUERY_ERROR == $vars)) {
      $this->err->err_from_string("Unable to fetch a list of variables assigned to this alert.");
    } else {
      $return_vars['alert_vars'] = $vars;
    }
     
     /**
      * 2- Retrieve reference_table for this alert_type and add the fields.
      */
     $am = require_module("mod_alerts");
     $typedef = $am->_get_alert_params(strtolower($name));
     $table = $typedef['reference_table_name'];
     $query = <<<EOM
SELECT a.attname AS "field", c.description
FROM pg_attribute a JOIN pg_class b ON (a.attrelid = b.oid)
LEFT JOIN pg_description c ON (c.objoid = b.oid AND c.objsubid = a.attnum)
WHERE b.relname = '$table'
AND a.attnum > 0
ORDER BY a.attname ASC
EOM;
     
     $res = $this->db->select($query);
     
     if ((DB_NO_RESULT != $res) && (DB_QUERY_ERROR != $res)) {
       $return_vars['object_vars'] = $res;
     }
     
     
     return $return_vars;
  }

  function update_alert_template(&$index_content) {
    require_class($this->module, "Alert_Template_Manager");
    $tm = new Alert_Template_Manager();

    if (!$tm->update()) {
      return;
    }

    $index_content .= message_bar("Template edited successfully");
    return $this->setup_alert_tpl($index_content);
  }
  
  function grouped_by_daemon() {
    $query = "SELECT id, name FROM daemons";
    $daemons = $this->db->select($query);

    if ((DB_NO_RESULT == $daemons) || (DB_QUERY_ERROR == $daemons)) {
      $daemons = array();
    }
    
    $payload = array();
    
    foreach ($daemons as $daemon) {
      $query = "SELECT * FROM daemonsconfig WHERE daemon_id=" . $daemon['id'];
      $res = $this->db->select($query);
      if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
        $res = array();
      }
      $payload[$daemon['name']] = $this->set_meta($res);
    }
    
    print json_encode($payload);
  }
  
  
  function get_daemon_settings() {
    $daemon = $this->db->escape($_GET['daemon']);
    $query = "SELECT b.var, b.value, b.id, b.docstring_xml
      FROM daemons a, daemonsconfig b
      WHERE a.id = b.daemon_id
      AND a.name ILIKE $daemon 
      ORDER BY b.var ASC
    ";
    
    $res = $this->db->select($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      print '{}';
      return;
    }

    print json_encode($this->set_meta($res));
  }
  
  function set_meta($res) {
    # Load the XML Unserializer in memory
    require_once "XML/Unserializer.php";

    # Initialize the XML Unserializer object
    $this->xml = new XML_Unserializer($this->options);
    
    for ($i = 0; $i < sizeof($res); $i++) {
      # Unserialize the XML structure
      $xml_res = $this->xml->unserialize($res[$i]['docstring_xml'], FALSE);

      unset($res[$i]['docstring_xml']);

      # Error control
      if (PEAR::isError($xml_res)) {
      print "error";
        $this->err->err_from_string("Unable to parse service variables registry.");
      }

      # Assign unserialized data to object
      $res[$i]['meta'] = $this->xml->getUnserializedData();
    }
    
    return $res;
  }


  /**
   * Daemon configuration form
   *
   * This function retrieves all variables associated with a specific daemon
   * from the DB. It then proceeds to create a meta-data structure based on
   * on the results of an unserialization process against the XML docstring
   * stored in the DB along with other fields for each variable.
   *
   * It then passes all those data structures to a special template that is
   * in charge of rendering a form for each of these fields that will be
   * different depending on the type of the field and its parameters.
   *
   * @param pointer $index_content
   */
  function form_configure_daemon(&$index_content) {

    $service = $this->db->escape($_GET['daemon']);

    $query = "SELECT a.name, a.description, b.var, b.value, b.id, b.docstring_xml
    FROM daemons a, daemonsconfig b
    WHERE a.id = b.daemon_id
    AND a.name ILIKE $service
    ORDER BY b.var ASC
    ";

    $res = $this->db->select($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      $this->err->err_from_string("There are no configuration options for this service.");
      return;
    }

    # Load the XML Unserializer in memory
    require_once "XML/Unserializer.php";

    # Initialize the XML Unserializer object
    $this->xml = new XML_Unserializer($this->options);

    for ($i = 0; $i < sizeof($res); $i++) {

      # Unserialize the XML structure
      $xml_res = $this->xml->unserialize($res[$i]['docstring_xml'], FALSE);

      # Error control
      if (PEAR::isError($xml_res)) {
        $this->err->err_from_string("Unable to parse service variables registry.");
      }

      # Assign unserialized data to object
      $res[$i]['meta'] = $this->xml->getUnserializedData();

      # If min == 0, intval() and strval() and $min will return 0 (== FALSE) so the
      # condition will not execute. strcmp(strval($min, "")) is the only working way
      # of doing this right now AFAIK.
      if ((strcmp(strval($res[$i]['meta']['min']), "") <> 0)
        && (strcmp(strval($res[$i]['meta']['max']), "") <> 0)) {

          # HAH! that would have been too easy...
          # Since the indexes don't match the values, smarty screws up if your
          # min > 0 and sets the value to 0 for the first index, etc...
          #$res[$i]['meta']['options'] = range($res[$i]['meta']['min'], $res[$i]['meta']['max']);
          $res[$i]['meta']['options'] = array();
          for ($x = $res[$i]['meta']['min']; $x <= $res[$i]['meta']['max']; $x++) {
            $res[$i]['meta']['options'][$x] = $x;
          }
      }

      # We will filter hidden fields out at the presentation layer.
      /*
      if ($res[$i]['meta']['type'] == "hidden") {
        unset($res[$i]);
      }
      */
    }

    $parser = new Parser($this->tpl_dir);

    $parser->assign_by_ref("fields", &$res);

    $index_content .= $parser->fetch("form_configure_daemon.tpl");
  }

  /**
   * Updates daemon-level configuration setting
   *
   * @param pointer $index_content
   */
  function process_update_daemon_var() {
    $input = json_decode(file_get_contents("php://input"), true);

    $query = "SELECT a.docstring_xml, b.component_type
    FROM daemonsconfig a, daemons b
    WHERE a.daemon_id = b.id
    AND b.name = " . $this->db->escape($_GET['daemon']) . "
    AND var = " . $this->db->escape($_GET['var']);
    $res = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $this->err->err_from_code(400, "Unable to fetch variable");
      return;
    }

    # Load the XML Unserializer in memory
    require_once "XML/Unserializer.php";

    # Initialize the XML Unserializer object
    $this->xml = new XML_Unserializer($this->options);

    # Unserialize the XML structure
    $xml_res = $this->xml->unserialize($res['docstring_xml'], FALSE);
    $type = $res['component_type'];

    # Error control
    if (PEAR::isError($xml_res)) {
      $this->err->err_from_string("Unable to parse service variables registry.");
    }

    # Assign unserialized data to object
    $meta = $this->xml->getUnserializedData();

    # Initialize the validator object.
    $validator = $this->registry->get_singleton("core", "validator");


    # Update the value in the DB
    if ($validator->validate($meta['type'], $input[$_GET['var']])) {
      $query = "UPDATE daemonsconfig SET value = " . $this->db->escape($input[$_GET['var']]) . "
      WHERE daemon_id =
        (SELECT id FROM daemons WHERE name = " . $this->db->escape($_GET['daemon']) . ")
      AND var = " . $this->db->escape($_GET['var']);

      $res = $this->db->update($query);

      /*
       * TODO: This is stupid.
       *
      # And restart the service if the update was successful.
      if (DB_QUERY_ERROR != $res){
        if (strcmp($type, "plugin") == 0) {
          $this->_sig_procmond("stop netmond");
          $this->_sig_procmond("start netmond");
        } else {
          $this->_sig_procmond("stop ".$_GET['daemon']);
          $this->_sig_procmond("start ".$_GET['daemon']);
        }
      }
      */
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to update setting, validation failed");
    }
  }

  /**
   * Displays the update management dialog
   *
   * @param pointer $index_content
   */
  function manage_updates(&$index_content) {
    $cmd = "sudo nohup python /apache/cli/update.pyc -s";
    $summary = `$cmd`;

    $this->debugger->add_hit("Summary", NULL, NULL, $summary);
    $this->debugger->add_hit("Command", NULL, NULL, $cmd);

    $parser = new Parser($this->tpl_dir);
    $parser->assign("summary", $summary);
    $index_content .= $parser->fetch("manage_updates.tpl");
  }

  /**
   * Runs the upgrade script
   *
   */
  function perform_upgrade() {
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
      return FALSE;
    }

    $build = simplexml_load_file("/etc/netmon-build.xml");
    $version = $build["version"];
    $path = "/usr/local/netmon_updates/" . $version . "/upgrade";
    $fName = $path . "/upgrade.json";
    $updateAvailable = file_exists($fName);

    if (!$updateAvailable) {
      $this->err->err_from_code(400, "Unable to run upgrade, please contact support. No upgrade currently available.");
      return;
    }

    $info = json_decode(file_get_contents($fName), true);

    if (!$info["version"] || !$info["command"]) {
      $this->err->err_from_code(400, "Unable to run upgrade, please contact support. Upgrade command or version not provided.");
      return;
    }

    $version = $info["version"];
    $cmd = $info["command"];

    print "Updating to Netmon v" . $version . "\r\n";
    $this->stream_command_output($cmd);
  }

  function get_build_string() {
    $build = simplexml_load_file("/etc/netmon-build.xml");

    $buildString = $build["version"];

    foreach ($build->component as $component) {
      $buildString .= "-" . $component["version"];
    }

    print $buildString;
  }

  /**
   * Runs the update script
   *
   * @param pointer $index_content
   */
  function fetch_updates(&$index_content) {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
      return FALSE;
    }
    
    $this->_get_rsync_port_status();
    $cmd = "sudo nohup python /apache/cli/update.pyc";

    $this->debugger->add_hit("Command", NULL, NULL, $cmd);

    $this->stream_command_output($cmd);
  }

  function stream_command_output($cmd) {
    ob_implicit_flush(true);
    ob_end_flush();
    $descriptorspec = array(
       0 => array("pipe", "r"),   // stdin is a pipe that the child will read from
       1 => array("pipe", "w"),   // stdout is a pipe that the child will write to
       2 => array("pipe", "w")    // stderr is a pipe that the child will write to
    );
    flush();

    $process = proc_open($cmd, $descriptorspec, $pipes, realpath('./'), array());
    if (is_resource($process)) {
        while ($s = fgets($pipes[1])) {
          print $s;
          flush();
        }
    }
  }

  function _get_rsync_port_status() {
    $fp = fsockopen("tcp://dev.netmon.ca", 873);
    if (!$fp) {
      $this->err->err_from_string("Netmon was unable to establish a connection on port 873/tcp.");
      return FALSE;
    } else {
      fclose($fp);
      return TRUE;
    }
  }
  
  /**
   * Returns a list of local networks in XML format
   *
   * @param pointer $index_content
   */
  function hook_get_localnets(&$index_content) {
    $query = "SELECT id, network, broadcast, COALESCE(label, 'Local ('||network||'-'||broadcast||')') AS label FROM localnets ORDER BY id ASC";
    $res = $this->db->select($query);
    
    if (DB_QUERY_ERROR == $res) {
      return;
    } elseif (DB_NO_RESULT == $res) {
      $res = NULL;
    }
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign("networks", $res);
    $index_content .= $parser->fetch("xml_localnets.tpl");
    header("Content-type: application/xml");
  }
  
  
  /**
   * Displays the alert testing tool's entry form
   *
   * @param pointer $index_content
   */
  function form_test_alerts(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    
    $query = "SELECT id, (COALESCE(last_name, '') || ', '::varchar || COALESCE(first_name, '')) AS \"name\" FROM users WHERE email is not null ORDER BY name ASC";
    $res = $this->db->select_smarty($query, "id", "name");
    $parser->assign("users", $res);
    
    $index_content .= $parser->fetch("form_test_alerts.tpl");
  }
  
  /**
   * Runs the Netmon Alert Testing Tool
   *
   * @param pointer $index_content
   */
  function alert_test(&$index_content) {
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
      print '{}';
    }
    
    $input = json_decode(file_get_contents("php://input"), true);

    $subject = escapeshellarg($input['subject']);
    $body = escapeshellarg($input['body']);
    $user = escapeshellarg($input['user_id']);
    
    $cmd = "sudo /usr/local/sbin/alerttest -u $user -s $subject -m $body";
    $this->debugger->add_hit("Alert Command", NULL, NULL, $cmd);
    
    $output = `$cmd`;
    print '{}';
  }
  
  /**
   * Creates an alert-bound command
   * 
   * @param pointer $index_content
   * @author xavier@netmon.ca
   */
  function process_create_alert_command(&$index_content) {
    
    
    require_class("mod_alerts", "Alert_Command_Manager");
    $_GET['id'] = $_POST['alert_type_id'];
    $acm = new Alert_Command_Manager();
    if ($acm->insert()) {
      $parser = new Parser($this->tpl_dir);
      $parser->assign("action_taken", "created");
      $parser->assign("setting", "alert command");

      $index_content .= $parser->fetch("setting_modified.tpl");
    }
    return $this->setup_alert_tpl($index_content);
  }
  
  /**
   * Returns an HTML table containing all known commands for an alert type
   * 
   * @param pointer $index_content
   * @author xavier@netmon.ca
   */
  function ajax_get_alert_commands(&$index_content) {
    $id = $_GET['alert_type_id'] ? $_GET['alert_type_id'] : $_POST['alert_type_id'];
    require_class("mod_alerts", "Alert_Command_Manager");
    $acm = new Alert_Command_Manager();
    $commands = $acm->get_commands_by_alert_type($id);
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign("commands", $commands);
    $index_content .= $parser->fetch("ajax_alert_commands.tpl");
    return;
  }
  
  /**
   * Updates an alert command
   * 
   * @param pointer $index_content
   * @author xavier@netmon.ca
   */
  function ajax_update_command(&$index_content) {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }
    
    
    require_class("mod_alerts", "Alert_Command_Manager");
    $acm = new Alert_Command_Manager();
    $acm->update();
    $this->ajax_get_alert_commands($index_content);
  }
  
  /**
   * Creates a new alert command
   * 
   * @param pointer $index_content
   * @author xavier@netmon.ca
   */
  function ajax_create_command(&$index_content) {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }
    
    
    require_class("mod_alerts", "Alert_Command_Manager");
    $acm = new Alert_Command_Manager();
    $acm->insert();
    return $this->ajax_get_alert_commands($index_content);
  }
  
  /**
   * Deletes an existing alert command
   * 
   * @param pointer $index_content
   * @author xavier@netmon.ca
   */
  function ajax_delete_command(&$index_content) {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }
    
    
    require_class("mod_alerts", "Alert_Command_Manager");
    $acm = new Alert_Command_Manager();
    $acm->delete($_GET['id']);
    return $this->ajax_get_alert_commands($index_content);
  }
  
  /**
   * Displays the Netmon Registration form
   *
   * @param pointer $index_content
   */
  function form_edit_license(&$index_content) {
    require_class("core", "activation_manager");
    
    $query = "SELECT n.*, EXTRACT('epoch' FROM expires) AS expire_epoch FROM netmon n";
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $res = array();
    }
    
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign("activation", $res);
    $index_content .= $parser->fetch("form_edit_license.tpl");
    return "Product Activation";
  }
  
  function get_license_info() {
    $query = "SELECT n.*, EXTRACT('epoch' FROM expires) AS expire_epoch FROM netmon n";
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $res = array();
    }
    print json_encode($res);
  }
  
  /**
   * Netmon Report for device slot usage
   *
   * @param pointer $index_content
   */
  function device_report(&$index_content) {
    $query = "select d.id,l.* from devices d, list_unique_ips() l WHERE d.ip_address=l.ip ORDER by l.ip DESC";
    
    $res = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      print '[]';
    }
    print json_encode($res);
    return;
  }

  /**
   *
   **/
    function get_alerts_object($ip_address = NULL) {
      $alerts = array();

      $query = "SELECT trigger_id FROM alert_triggers ORDER BY trigger_id ASC";

      $res = $this->db->select($query);
      if (DB_QUERY_ERROR == $res) {
        $this->err->err_from_string("Unable to retrieve a list of alerts. You may not have any alerts configured yet.") ;
        return;
      }

      foreach($res as $row) {
        # Retrieve meta-data and human-readable description about the alert
        $query = "SELECT e.*, e.threshold\"trigger_threshold\", e.comparison\"comp_exp\", h.user_id, h.media_id FROM extract_alert(".$this->db->escape($row['trigger_id']).") e, alert_handlers h WHERE e.handler_id = h.id";
        $alert = $this->db->get_row($query);
        if (DB_QUERY_ERROR == $res) {
          $this->err->err_from_string("Unable to retrieve alert with trigger " . $row['trigger_id']);
        }
        if ($ip_address == NULL || $alert['ip'] == $ip_address) {
          $device = $this->db->get_row("SELECT * FROM devices WHERE ip_address='" . $alert['ip'] . "'");
          if (DB_QUERY_ERROR != $device) {
            # Retrieve any alert maintenance window associated with this alert
            $query = "SELECT * FROM alert_maintenance WHERE trigger_id = " . $this->db->escape($row['trigger_id']);
            $schedule = $this->db->select($query);
            if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
              $alert['maintenance'] = array();
            } else {
              $alert['maintenance'] = $this->_get_maintenance_schedule_description($schedule);
              $this->debugger->add_hit('Alert:', null, null, vdump($alert));
            }

            $alert['trigger_id'] = $row['trigger_id'];

            if (!is_array($alerts[$alert['ip']])) {
              $alerts[$alert['ip']] = array();
              $alerts[$alert['ip']]['alerts'] = array();
            }

            if (sizeof($device['label']) > 0) {
              $alerts[$alert['ip']]['label'] = $device['label'];
            } else {
              $alerts[$alert['ip']]['label'] = $alert['ip'];
            }
            array_push($alerts[$alert['ip']]['alerts'], $alert);
          }
        }
      }

    $types = array(
      'oids' => 'OID Tracker',
      'servers' => 'ICMP/TCP Tracker',
      'urls' => 'URL Tracker',
      'df_servers' => 'UNIX Disk Tracker',
      'smb_servers' => 'Windows Share Tracker',
      'scan_log' => 'Open-Ports Tracker',
      'syslog_access' => 'Event Log Tracker',
      'interfaces' => 'SNMP Bandwidth Tracker',
      'arptable' => 'Host Detection Tracker',
      'protocols' => 'Traffic Analysis Tracker',
      'snmpoids' => 'SNMP Trap Tracker'
    );

    return array(
                  "alerts" => $alerts,
                  "types" => $types
               );

    }


  /**
   * Displays the alert management dialog
   *
   * @param pointer $index_content
   */
  function get_alerts() {
    print json_encode($this->get_alerts_object());
  }

  function get_alerts_by_ip() {
    print json_encode($this->get_alerts_object($_GET['ip']));
  }

  function _get_maintenance_schedule_description($schedule) {
    $windows = array();
    if (is_array($schedule)) {
      foreach($schedule as $s) {
        $condition = '';
        if ('week' == $s['recurrence_unit']) {
          $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
          $condition = 'Every ' . $days[intval($s['schedule_week'])];
        } elseif ('month' == $s['recurrence_unit']) {
          $condition = 'Monthly, on the ' . $this->_ordinal($s['schedule_day']);
        } elseif ('year' == $s['recurrence_unit']) {
          $months = array('Jannuary', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
          $condition = "Annualy on " . $months[intval($s['schedule_month'])-1] . ' ' . $this->_ordinal(intval($s['schedule_day']));
        } else {
          $condition = 'Daily';
        }
        $s['schedule_hour'] = (intval($s['schedule_hour']) <= 12) ? (intval($s['schedule_hour']).'AM') : (intval($s['schedule_hour'])-12 . 'PM');
        $s['condition'] = $condition . ' at ' . $s['schedule_hour'] . ' for ' . $s['maintenance_duration'] . ' hour' . (intval($s['maintenance_duration']) > 1 ? 's' : '');
        $windows[] = $s;
      }
      return $windows;
    } else {
      return NULL;
    }
  }
  
  function _ordinal($n) {
    return $n . date('S', mktime(0,0,0,1,$n));
  }

  function check_update_available() {
    $payload = array();

    $build = simplexml_load_file("/etc/netmon-build.xml");
    $version = $build["version"];
    $payload['fName'] = "/usr/local/netmon_updates/" . $version . "/upgrade/upgrade.json";
    $payload['updateAvailable'] = file_exists($payload['fName']);
    if ($payload['updateAvailable']) {
      $payload['info'] = json_decode(file_get_contents($payload['fName']), true);
    }

    print json_encode($payload);
  }

  function get_open_tunnel() {
    $print = $_GET['print'];
    $sshPort = $httpPort = -1;

    $tunnels = `ps xa | grep [s]upport@demo | awk '{print $9}'`;
    foreach (explode("\n", $tunnels) as $tunnel) {
      $tunnelInfo = explode(":", $tunnel);
      if (sizeof($tunnelInfo) == 3) {
        if ($tunnelInfo[2] == "22") {
          $sshPort = $tunnelInfo[0];
        } else if ($tunnelInfo[2] == "80") {
          $httpPort = $tunnelInfo[0];
        }
      }
    }

    $payload = array();
    $payload['ssh'] = $sshPort;
    $payload['http'] = $httpPort;

    if ($print) {
      print json_encode($payload);
    }

    return $payload;
  }

  function open_support_tunnel() {
    $pwd = $_GET['pwd'];
    print `sudo python /apache/cli/support_connect.py -j -p$pwd`;
  }

  function kill_support_tunnels() {
    $womp = `sudo pkill -2 -x ssh`;
    $processes = `ps xa | grep [s]upport_connect.py | awk '{print $1}'`;
    if (sizeof($processes) > 2) {
      foreach (explode("\n", $processes) as $process) {
        $kill = `sudo kill -9 $process`;
      }
    }
    if ($_GET['print']) {
      print '{}';
    }
  }

}
?>
