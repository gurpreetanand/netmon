<?php


class Protocols_Manager extends MadnetElement {
	
	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "protocols";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_settings";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;
	
	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;
	
	function init() {
		$this->params->add_primitive("port",            "integer",    TRUE,    "Port Number",          "Port Number");
		$this->params->add_primitive("name",            "string",     TRUE,    "Protocol Name",            "Protocol Name");
		$this->params->add_primitive("protocol",        "string",    TRUE,    "Transport Layer",           "Transport Layer");
	}
	
	
	
	/**
	  * Retrieves all protocol IDs from the DB
	  *
	  * @return mixed
	  */
	function get_all_ids() {
		$query = "SELECT {$this->pkey} AS \"id\", port, protocol, name FROM {$this->table} ORDER BY port, protocol ASC";
		$result = $this->db->select($query);
		
		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			return $result;
		}
	}
	
	function protocol_search($type, $key = NULL) {
		
		$query = "SELECT {$this->pkey} AS \"id\", name, protocol FROM {$this->table} ";
		
		if ($key) {
			$key = $this->db->escape('%' . $key . '%');
			$query .= "AND name LIKE $key ";
		}
		
	
		$query .= "ORDER BY port, protocol ASC LIMIT 2000";
		
		$result = $this->db->select($query);
		
		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			return $result;
		}
	}
	
	function pre_insert($id = null) {
		$protocol = $this->db->escape($this->params->primitives['protocol']['value']);
		$port = $this->db->escape($this->params->primitives['port']['value']);
		$name = $this->db->escape($this->params->primitives['name']['value']);
		$query = "SELECT protocol FROM {$this->table} WHERE protocol = $protocol AND port = $port AND name = $name";
			
		if ($id) {
			$query .= " AND id <> $id";
		}
		
		
		$result = $this->db->get_row($query);
		
		if (DB_NO_RESULT != $result) {
			$this->err->err_from_string("There is already a port label in the database with the same description.");
			return FALSE;
		}
		return TRUE;
		
		
	}
	
	
	function pre_update($id) {
		return $this->pre_insert($id);
	}
	
	function pop($id) {
		$id = $this->db->escape($id);
		
		$query = "SELECT * FROM {$this->table} WHERE {$this->pkey} = $id";
		
		$result = $this->db->get_row($query);
		
		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			foreach($result as $key => $value) {
				$this->params->setval($key, $value);
			}
			return TRUE;
		}
	}
}


?>