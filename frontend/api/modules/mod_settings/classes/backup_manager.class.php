<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    4.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  * $Id$
  * $URL: svn+ssh://dev/usr/local/svn/middleware/trunk/modules/mod_settings/classes/backup_manager.class.php $
  */


// backup_manager.class.php

/**
  *
  * This module handle the management operations related to backup management.
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class Backup_Manager extends MadnetModule {

  
  var $components;
  
  /**
    * Initializes all module variables
    *
    * @return void
    */
  function init() {
    $this->_set_module("mod_settings");
    
    $this->options = array(
      'complexType'       => 'array',
      'parseAttributes'   => TRUE,
      'attributesArray'   => FALSE,
      'contentName'       => 'panels'
    );
  }
  
  /**
   * Finds all SMB shares available for writing on a host
   *
   * @param string $ip_address
   * @param string $username
   * @param string $password
   * @access public
   */
  function find_smb_shares($ip_address, $username, $password, $domain = "") {
    $username   = $username ? "-u " . $username : "";
    $password   = $password ? "-p " . $password : "";
    $domain     = $domain   ? "-d " . $domain : "";

    
    $cmd = escapeshellcmd("/usr/local/sbin/smbscan -f $username $password $domain $ip_address");
    $res = "";
    exec($cmd, $res);
    
    $this->debugger->add_hit("SMB Autodetect command: $cmd", NULL, NULL, vdump($res));
    
    $out = array();
    $parts = array();
    
    $regex = "(.*) ([0-9]+) bytes free";
    foreach($res as $share) {
      if (eregi($regex, $share, $parts)) {
        array_push($out, array("share" => $parts[1],"space" => $parts[2]));
      }
    }
    
    return $out;
  }
  
  /**
   * Returns a multi-dimensional hash-table of the form:
   * array {
   *   "syslog" => array("From" => 123, "To" => 124, "size" => 109247),
   *   ...
   * }
   * That describes the tables to include in the backup operation.
   * This data is extracted from POST data.
   *
   */
  function get_backup_tables($tables, $from, $to) {
    # Note: This can be changed later if it ever becomes useful to have To: From: fields
    # that have different values for each table, but for now, we will just use one value
    # for all of them.
    $out = array();
    $to = $to ? $to : mktime();
    foreach($tables as $table) {


    /*
      # Most of the code in this block is used to determine the size of
      # the selected segment within the table (in bytes).
      $query = "SELECT tuple_len/CASE WHEN tuple_count = 0 THEN 1 ELSE tuple_count END AS row_size,
              table_len-tuple_len AS overhead 
              FROM pgstattuple(" . $this->db->escape($table) . ")";

      $stats_res = $this->db->get_row($query);

      if ((DB_QUERY_ERROR == $stats_res) || (DB_NO_RESULT == $stats_res)) {
        $this->err->err_from_string("Unable to determine size statistics for the table $table.");
        continue;
      }

      $query = "SELECT COUNT(*) AS rows FROM " . $this->db->quote_key($table) . "
              WHERE timestamp 
                BETWEEN " . $this->db->escape($from) . " 
                AND " . $this->db->escape($to);
      $rows_res = $this->db->get_row($query);


      # If the operation failed, it is likely that the time has no 'timestamp' field
      # so we will end up backing up the entire table anyway since rows are not time-based.
      if ((DB_QUERY_ERROR == $rows_res) || (DB_NO_RESULT == $rows_res)) {
        # Remove the error triggered by the failed query.
        array_pop($this->err->errors_collection);
        $query = "SELECT COUNT(*) AS rows FROM " . $this->db->quote_key($table);
        $rows_res = $this->db->get_row($query);
      }

      $rows = $rows_res['rows'];
      $size = $stats_res['row_size'] * $rows;
    */
      $size = 0;

      $out[$table] = array("from" => $from, "to" => $to, "size" => $size);
    }
    return $out;
  }
  
  /**
   * Checks that the SMB share meets minimum space requirements and makes sure it is
   * writeable.
   * Returns a data-structure describing the SMB share if successful, FALSE
   * otherwise.
   *
   * @param string $ip_address
   * @param string $share
   * @param string $username
   * @param string $password
   * @param array $tables
   */
  function get_smb_share($ip_address, $smb_share, $username, $domain, $password, $tables) {
    
    # Retrieve the amount of space required for the backup
    $required = $this->_get_space_requirement($tables);
    
    # Find the available space for the specified share using 
    # smbscan and looping through the results.
    $shares = $this->find_smb_shares($ip_address, $username, $password);
    foreach($shares as $share) {
      if (strcmp($smb_share, $share[0]) == 0) {
        if ($required >= $share[1]) {
          $this->err->err_from_string("Insufficient space available on the share $smb_share on $ip_address");
          return FALSE;
        }
        break;
      }
    }
    
    # Create a new temporary mountpoint in /mnt
    $mountpoint = "/mnt/netmon_backup_" . mktime();
    `sudo mkdir $mountpoint`;
    `sudo chown -R www-data:www-data $mountpoint`;
    
    /*
    if (!mkdir($mountpoint)) {
      $this->err->err_from_string("Unable to create the directory $mountpoint");
      return FALSE;
    }
    */
    
    # Attempt to mount the share to the mountpoint.
    $return = 0;
    $out = "";
    $cmd = escapeshellcmd("sudo smbmount //$ip_address/$smb_share $mountpoint -o username=$username,password=$password,workgroup=$domain,uid=www-data,gid=www-data,rw");
    $this->debugger->add_hit("SMB Mount Command", NULL, NULL, $cmd);
    exec($cmd, $out, $return);
    
    # Typically, if there is an error, smbmount will return 0 but display an error msg.
    if (sizeof($out) > 0) {
      $this->err->err_from_string("'$out' - '$return' " . vdump($out));
      `sudo umount -f $mountpoint`;
      `sudo rm -rf $mountpoint`;
      return FALSE;
    }
    
    
    # I've never seen smbmount return an error code but this behaviour is uncodumented so
    # we will assume it is able to return an error code.
    if ($return <> 0) {
      $this->err->err_from_string("An unexpected error has occured while connecting to the SMB share.");
      `sudo rm -rf $mountpoint`;
      return FALSE;
    }
    
    # Create a temporary file in the mount point to determine if new files can be created.
    $touch_file = "netmon_test_" . mktime();
    
    # If we can create the file AND write to it, the share is legit and we return
    # a data-structure describing it.
    $this->debugger->add_hit("Creating $mountpoint/$touch_file");
    
    if ($fp = fopen("$mountpoint/$touch_file", "w")) {
      fclose($fp);
      unlink($mountpoint . "/" . $touch_file);
      exec("sudo umount -f $mountpoint");
      `sudo rm -rf $mountpoint`;
      return array("type" => "smb",
            "ip_address" => $ip_address, 
            "share" => $smb_share, 
            "username" => $username,
            "password" => $password);
    } else {
      $this->err->err_from_string("Unable to create a new file on the SMB share.");
      exec("sudo umount -f $mountpoint");
      @fclose($fp);
      unlink($mountpoint . "/" . $touch_file);
      `sudo rm -rf $mountpoint`;
      return FALSE;
    }
  }
  
  /**
   * Checks that the FTP dir meets minimum space requirements and makes sure it is
   * writeable.
   * Returns a data-structure describing the FTP dir if successful, FALSE otherwise.
   *
   * @param string $ip_address
   * @param string $dir
   * @param string $username
   * @param string $password
   * @param array $tables
   * 
   * @TODO Implement FTP support for backup system. Bug #362
   */
  function get_ftp_dir($ip_address, $dir, $username, $password, $tables) {
    # Retrieve the amount of space required for the backup
    #$required = $this->_get_space_requirement($tables);
    $this->err->err_from_string("FTP Backups are not supported at the moment.");
    return FALSE;
    
  }
  
  function get_local($tables, $label) {
    
    $req = $this->_get_space_requirement($tables);
    
    $cmd = "df -lP --block-size=1 /var";
    $out = `$cmd`;
    $out = explode("\n", $out);
    $out = $out[1];
    
    $parts = array();
    if (eregi("^.* + [0-9]+ [0-9]+ +([0-9]+) +[0-9]+% +/.*", $out, $parts)) {
      $size = $parts[1];
    } else {
      $this->err->err_from_string("Unable to determine the amount of free space on the /var partition.");
      return FALSE;
    }
    
    if ($req >= $size) {
      $this->err->err_from_string("Insufficient space available on the local disk.");
      return FALSE;
    }
    
    return array("type" => "local",
           "label" => escapeshellarg($label));
  }
  
  /**
   * Triggers the backup operation.
   *
   * @param array $target
   * @param array $tables
   * 
   * @TODO Implement FTP support for backup system. Bug #362
   */
  function init_backup($target, $tables, $options) {
    ignore_user_abort(TRUE);
    $str_target  = "";
    $str_options = "";
    
    # Constructs an arguments list based on the data-structure describing the target.
    foreach($target as $key => $val) {
      $str_target .= " --" . $key . "=" . escapeshellarg($val);
    }
    
    foreach($options as $key => $val) {
      if (strcmp(trim($val), "") <> 0) {
        $str_options .= "--" . escapeshellarg($key) . "=" . escapeshellarg($val) . " ";
      }
    }
    
    # For now, we just want to see what the command looks like.
    $cmd = "sudo /apache/cli/backup_manager.py $str_target $str_options";

    $this->debugger->add_hit("Backup command:", NULL, NULL, $cmd);
    
    # Experimental...
    $descriptors = array(1=> array("pipe", "w"));
    $pipes = array();
    $proc = proc_open($cmd, $descriptors, $pipes);
    sleep(1);
    proc_close($proc);
    
    $query = "SELECT id FROM backups ORDER BY id DESC LIMIT 1";
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    
    return $res['id'];
  }
  
  /**
   * Calls the python db2xml component and break-down the resulting XML document into
   * a PHP data-structure.
   *
   */
  function retrieve_tables_structure() {
    # Load the XML Unserializer in memory
    require_once "XML/Unserializer.php";

    # Initialize the XML Unserializer object
    $this->xml = new XML_Unserializer($this->options);
    
    $cmd = ROOT . "/cli/db2xml.py";
    $xml = `$cmd`;
    
    # Unserialize the XML document
    $xml_res = $this->xml->unserialize($xml, FALSE);
    
    # Perform error control
    if (PEAR::isError($xml_res)) {
      $this->err->err_from_string("Unable to parse service variables registry.");
    }
    
    $tables = $this->xml->getUnserializedData();
    
    return $tables;
  }
  
  /**
   * Returns the XML document as returned by the db2xml python component.
   *
   */
  function retrieve_xml_table_structure() {
    $cmd = ROOT . "/cli/dbtoxml.py";
    return `$cmd`;
  }
  
  /**
   * Attempts to determine the minimum amount of space required for
   * the backup of the tables described in the $table data-structure.
   * The resulting size is expressed in bytes.
   *
   * @param array $tables
   */
  function _get_space_requirement($tables) {
    # We can obtain free-space information about a table through:
    # SELECT * from pgstattuple('table_name');
    $required = 0;
    foreach($tables as $table) {
      $required += $table['size'];
    }
    
    # According to my benchmarks, postgres tuple data in plain-text format
    # offers a 1:11 compression ratio through bzip2 --fast.
    return $required / 11;
  }
  
  function _get_table_size($table) {
    $query = "SELECT pg_relation_size(" . $this->db->escape($table) . ") AS table_len";
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return 0;
    }
    
    return $res['table_len'];
  }
  
  function get_backup_status($backup_id, $last_timestamp) {
    $query = "SELECT id, timestamp, event FROM backup_events
          WHERE backup_id = " . $this->db->escape($backup_id) . "
          AND timestamp > " . $this->db->escape($last_timestamp) . "
          ORDER BY timestamp ASC, id ASC";
    
    $events = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $events) || (DB_NO_RESULT == $events)) {
      $events = array();
    }
    
    $query = "SELECT status FROM backups WHERE id = " . $this->db->escape($backup_id);
    
    $backup = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $backup) || (DB_NO_RESULT == $backup)) {
      return FALSE;
    }
    
    $backup['events'] = $events;
    
    return $backup;
  }
  
  function get_backup_entities() {
    $query = "SELECT id, init_timestamp, description, status, owner FROM backups ORDER BY id DESC";
    
    $res = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    
    return $res;
  }
  
  function clear($id) {
    $query = "DELETE FROM backups WHERE id = " . $this->db->escape($id);
    $res = $this->db->delete($query);
    return ($res != DB_QUERY_ERROR);
  }
}

?>