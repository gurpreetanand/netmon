<?php


class Hostnames_Manager extends MadnetElement {
  
  /**
    * Database table associated with this subclass
    *
    * @var $table
    * @access protected
    */
  var $table = "hosts";
  /**
    * Name of the primary key in the table
    *
    * @var string $pkey
    * @access protected
    */
  var $pkey = "id";/**
    * Name of the module this MadnetElement subclass belongs to
    *
    * @var string $module
    * @access protected
    */
  var $module = "mod_settings";
  /**
    * Name of the class containing the business logic for this Element
    *
    * @var string $element
    * @access protected
    */
  var $element = __CLASS__;
  
  /**
    * Meta-structure (see MadnetElement for more info)
    *
    * @var hashtable $meta
    * @access private
    */
  var $meta;
  
  function init() {
    $this->params->add_primitive("ip",              "ip_address", TRUE,   "IP Address",          "IP Address");
    $this->params->add_primitive("hostname",        "string",     TRUE,   "Hostname",            "Hostname");
    $this->params->add_primitive("timestamp",       "integer",    TRUE,   "Timestamp",           "Timestamp");
    $this->params->add_primitive("host_name_type",  "string",     TRUE,    "Hostname Type",      "Hostname Type");
    $this->params->add_primitive("node_type",       "string",     TRUE,    "Node Type",          "Type of network device");
  }
  
  
  
  /**
    * Returns an array containing the user ID of every user account in the DB
    *
    * @return mixed
    */
  function get_all_ids() {
    $query = "SELECT {$this->pkey} AS \"id\", ip, hostname, node_type FROM {$this->table} ORDER BY hostname, ip ASC";
    $result = $this->db->select($query);
    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      return $result;
    }
  }
  
  function pop($id) {
    $id = $this->db->escape($id);
    
    $query = "SELECT * FROM {$this->table} WHERE {$this->pkey} = $id";
    
    $result = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      foreach($result as $key => $value) {
        $this->params->setval($key, $value);
      }
      return TRUE;
    }
  }
  
  function host_search($type, $key = NULL) {
    
    $query = "SELECT {$this->pkey}, ip, host_name_type, hostname, node_type, timestamp FROM {$this->table} ";
    
    if ($key) {
      $ip = $this->db->escape($key);
      $key = $this->db->escape('%' . $key . '%');
      
      $validator = &$this->registry->get_singleton("core", "validator");
    
      if ($validator->validate("ip_address", $ip)) {
        $query .= "WHERE ip = $ip ";
      } else {
        $query .= "WHERE hostname ILIKE $key ";
      }

    }
    
    if ($type) {
      $type = $this->db->escape('%' . $type . '%');
      $query .= ($key ? "AND" : "WHERE") . " host_name_type ILIKE $type ";
    }
    
    $query .= "ORDER BY timestamp DESC LIMIT 10";
    
    $this->debugger->add_hit("Host Search Query", NULL, NULL, $query);
    $result = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      return $result;
    }
  }
  
  function pre_insert($id = null) {
    return TRUE;
  }
  
  
  function pre_update($id) {
    return $this->pre_insert($id);
  }
  
  function _delete($params) {
    $query = "DELETE FROM {$this->table} WHERE (TRUE) ";
    foreach($params as $key => $val) {
      $query .= "AND (" . $this->db->quote_key($key) . " = " . $this->db->escape($val) . ") ";
    }
    $result = $this->db->delete($query);
    
    if (DB_QUERY_ERROR == $result) {
      return FALSE;
    } else {
      return $result;
    }
  }
  
  
}


?>