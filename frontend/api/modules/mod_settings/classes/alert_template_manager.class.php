<?php


class Alert_Template_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "alert_types";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_settings";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		$this->params->add_primitive("default_template",     "string",           TRUE,   "Template",      "Template");
		$this->params->add_primitive("default_subject",      "string",           TRUE,   "Subject",        "Subject");
	}



	/**
	  * Returns an array containing the user ID of every user account in the DB
	  *
	  * @return mixed
	  */
	/*
	function get_all_ids() {
		$query = "SELECT {$this->pkey}, network, broadcast FROM {$this->table} ORDER BY network, broadcast ASC";
		$result = $this->db->select($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			return $result;
		}
	}
	*/


	function pre_insert($id = null) {
		return TRUE;
	}


	function pre_update($id) {
		return $this->pre_insert($id);
	}


}


?>