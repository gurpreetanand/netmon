<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

// mod_layout.class.php

/**
  * Layout MCO
  *
  * This module is in charge of working out the details of frameset
  * handling and general layout issues for the framework.
  *
  * It will use a registry of which "area"
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class mod_layout extends MadnetModule {

	/**
	  * Initializes all module variables
	  *
	  * @return void
	  */
	function init() {

		$this->options = array(
			'complexType'       => 'array',
			'parseAttributes'   => TRUE,
			'attributesArray'   => FALSE,
			'contentName'       => 'panels'
		);
	}
	/**
	  * Interacts with the Parser object to create frame-based layout from XML specs
	  *
	  * This method is the key of the layout rendering mechanism.
	  * The file registry/components.xml is used by the toolbar flash widget to
	  * display the navigation entries by area.
	  *
	  * Since each of these areas use different layouts, we are using the same file
	  * to specify layout attributes.
	  *
	  * The XML spec includes a set of "panel" elements. Each "panel" (a frame in the
	  * frameset) has the following attributes:
	  *
	  * - width
	  * - scrolling
	  * - name
	  * - module
	  * - action
	  *
	  * The mod_layout constructor extracts the XML data from components.xml
	  * while this method isolates the components related to the area the user
	  * is accessing in the application. Once the data has been isolated, this
	  * method sends a reference to our data-structure to the templating engine.
	  *
	  * The layout.tpl template itterates through the "panel" objects in the
	  * data structure, and creates an HTML frame for each of the panels.
	  *
	  * It will also use information about the structure itself to build the
	  * initial frameset.
	  *
	  * @param pointer $index_content
	  * @return string
	  *
	  */
	function render_section(&$index_content) {

		$perm = FALSE;

		if (($_GET['layout'] == "settings") && (has_perm("change_settings"))) {
			$perm = TRUE;
		} elseif ($_GET['layout'] == "home") {
			$perm = TRUE;
		} elseif (!in_array($_GET['layout'], array("settings", "home"))) {
			if (has_perm("access_" . $_GET['layout'] . "_console")) {
				$perm = TRUE;
			}
		}

		if ($perm == FALSE) {
			$this->err->err_from_code(403, "You do not have access to this area");
			return "Permission Error";
		}

		# Load the XML Unserializer in memory
		require_once "XML/Unserializer.php";

		# Instanciate the XML Unserializer object
		$this->xml = new XML_Unserializer($this->options);

		# Unserialize our component registry
		$query = array("module" => "core", "action" => "get_components", "root_tpl" => "blank");
		$result = $this->xml->unserialize(Parser::madnet_action_handler($query), FALSE);

		#$result = $this->xml->unserialize(ROOT . '/registry/components.xml', true);

		# Error control
		if (PEAR::isError($result)) {
			$this->err->err_from_string("Unable to parse component registry. Please ensure it is valid XML data and that I have permission to access it");
		}

		# Assign unserialized data to object
		$this->data = $this->xml->getUnserializedData();

		# The default root template is not appropriate for framesets - override it.
		$_GET['root_tpl'] = "blank";

		if (!$_GET['layout']) { $_GET['layout'] = "home"; }

		# Itterate through the list of "button" elements (indexed array)
		foreach($this->data['button'] as $layout) {
			# Check to see if this element matches the GET request.
			if ($layout['id'] == $_GET['layout']) {
				$parser = new Parser($this->tpl_dir);
				# Pass a reference of the array to Parser and let it deal with it.
				$parser->assign_by_ref("panels", $layout['panel']);
				$index_content .= $parser->fetch("layout.tpl");
				# There is no placeholder for section titles in blank templates.
				return;

			}
		}

		# If we haven't been able to locate the element, set the root tpl back to index
		$_GET['root_tpl'] = "index";
		# And display an error.
		$index_content .= "The system was not able to find layout information for the section titled " . mktitle($_GET['layout']);
		return "Error";
	}

	function render_panel(&$index_content) {
		#$_GET['root_tpl'] = "blank";
		$parser = new Parser($this->tpl_dir);

		$tpl = file_exists($this->tpl_dir . $_GET['id'] . ".tpl") ? $_GET['id'] . ".tpl" : $this->tpl_dir . "default.tpl";
		$index_content .= $parser->fetch($tpl);
		return $tpl;
	}



}


?>