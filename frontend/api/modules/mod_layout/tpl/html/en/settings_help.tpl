<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
{literal}
<script language="Javascript">
	function showEditor(dest){
		document.getElementById("iframe_settings_editor").src = dest;
		activatePanel(document.getElementById("Netmon_Settings_Editor"));
	}
	
	function showHelp(subject){
		document.getElementById("iframe_help").src = "?module=mod_help&action=get_help&section=" + subject;
		activatePanel(document.getElementById("Netmon_Help"));
	}

function AdjustIframes(){	
	document.getElementById("iframe_help").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("Netmon_Settings_Editor").offsetHeight - document.getElementById("Netmon_Help").offsetHeight - document.getElementById("toolbar0").offsetHeight - 4;
}
window.onresize = AdjustIframes;

</script>
{/literal}


	<!-- Settings Editor -->
		<div id="Netmon_Settings_Editor" class="titlebar_collapsed" onclick="activatePanel(this)">
		 	Settings Editor
		</div>
		<div class="panel, closed">
			<!-- Sub-Window (Inline Frame) -->
			<iframe src="?module=mod_settings&action=settings_editor&root_tpl=blank_panel" id="iframe_settings_editor" name="iframe_settings_editor" width="100%" height="340px" frameborder="0"></iframe>
		</div>

{include file="help_panel.tpl"}

<script language="Javascript">
//setInitPanel();
activePanel = document.getElementById("Netmon_Help");
openPanel(activePanel);
AdjustIframes();

</script>

<!-- end of {$smarty.template} -->
