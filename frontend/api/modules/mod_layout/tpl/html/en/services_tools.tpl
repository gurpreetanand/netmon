<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
{literal}
<script language="Javascript">
function showHelp(subject){
	document.getElementById("iframe_help").src = "?module=mod_help&action=get_help&section=" + subject;
	activatePanel(document.getElementById("Netmon_Help"));
}

function showEditor(dest){
	document.getElementById("iframe_services_editor").src = dest;
	activatePanel(document.getElementById("Netmon_Services_Editor"));
}

function AdjustIframes(){
	document.getElementById("iframe_help").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("Netmon_Services_Editor").offsetHeight - document.getElementById("Netmon_Help").offsetHeight - document.getElementById("toolbar0").offsetHeight - 4;
}
window.onresize = AdjustIframes;
</script>
{/literal}

<!-- Help Panel -->
<div>

	<!-- Service Editor Box -->
	<div id="Netmon_Services_Editor" class="titlebar_collapsed" onclick="activatePanel(this)">
		Tracker Manager
	</div>
	<div class="panel, closed">

	<!-- Sub-Window (Inline Frame) -->
	<iframe src="?module=mod_services&action=form_create_service&root_tpl=blank_panel" id="iframe_services_editor" name="iframe_services_editor" width="100%" height="320" frameborder="0"></iframe>

	</div>
</div>


{include file="help_panel.tpl"}

<script language="Javascript">
//setInitPanel();
activePanel = document.getElementById("Netmon_Help");
openPanel(activePanel);
AdjustIframes();
</script>

<!-- end of {$smarty.template} -->
