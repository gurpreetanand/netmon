<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
{literal}
<script language="Javascript">
function showHelp(subject){
	document.getElementById("iframe_help").src = "?module=mod_help&action=get_help&section=" + subject;
	activatePanel(document.getElementById("Netmon_Help"));
}

function showEditor(dest) {
	document.getElementById("iframe_syslog_editor").src = dest
	activatePanel(document.getElementById("Netmon_Syslog_Manager"));
}

function AdjustIframes(){	
	document.getElementById("iframe_help").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("Netmon_Syslog_Manager").offsetHeight - document.getElementById("Netmon_Help").offsetHeight - document.getElementById("toolbar0").offsetHeight - 4;
}
window.onresize = AdjustIframes;
</script>
{/literal}

		<!-- Settings Editor -->
		<div id="Netmon_Syslog_Manager" class="titlebar_collapsed" onclick="activatePanel(this)">
		 	Event Log Search</div>
		<div class="panel, closed">
			<!-- Sub-Window (Inline Frame) -->
			<iframe src="?module=mod_syslog&action=form_search&root_tpl=blank_panel" id="iframe_syslog_editor" name="iframe_syslog_editor" width="100%" height="400" frameborder="0"></iframe>
		</div>

{include file="help_panel.tpl"}

<script language="Javascript">
activePanel = document.getElementById("Netmon_Syslog_Manager");
openPanel(activePanel);
AdjustIframes();
</script>

<!-- end of {$smarty.template} -->
