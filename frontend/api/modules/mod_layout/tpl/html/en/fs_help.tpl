
<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
{literal}
<script language="Javascript">
	function showHelp(subject){
		document.getElementById("iframe_help").src = "?module=mod_help&action=get_help&section=" + subject;
		activatePanel(document.getElementById("Netmon_Help"));
	}

	function showEditor(dest){
		document.getElementById("iframe_files_manager").src = dest;
		activatePanel(document.getElementById("Netmon_File_Editor"));
	}

function AdjustIframes(){	
	document.getElementById("iframe_help").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("Netmon_Help").offsetHeight - document.getElementById("toolbar0").offsetHeight - 4;
}
window.onresize = AdjustIframes;
</script>
{/literal}
	
<div id="Netmon_File_Editor" class="titlebar_collapsed" onclick="activatePanel(this)">
 	Files Manager
</div>
<div class="panel, closed">
	<!-- Sub-Window (Inline Frame) -->
	<iframe src="?module=core&action=blank&root_tpl=blank_panel" id="iframe_files_manager" name="iframe_files_manager" width="100%" height="370px" frameborder="0"></iframe>
</div>
		
			
{include file="help_panel.tpl"}
	
<script language="Javascript">
//setInitPanel();
activePanel = document.getElementById("Netmon_Help");
openPanel(activePanel);
AdjustIframes();
</script>

<!-- end of {$smarty.template} -->
