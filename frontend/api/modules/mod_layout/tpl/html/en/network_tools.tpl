<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
{capture assign="ip"}{$smarty.get.ip|default:$smarty.session.request.ip}{/capture}

{literal}
<script>
function showHelp(subject){
	document.getElementById("iframe_help").src = "?module=mod_help&action=get_help&section=" + subject;
	activatePanel(document.getElementById("Netmon_Help"));
}

function showConnections(ip,extraInfo){
	ActiveConnectionsURL = "?module=mod_network&action=get_active_connections&root_tpl=blank_panel&ip=" + ip + extraInfo;
	document.getElementById("iframe_active_connections").src = ActiveConnectionsURL;
	document.getElementById("ip").value = ip;
	activatePanel(document.getElementById("Remote_Connections"));
	document.getElementById("portscan_ip").value = ip;
	document.getElementById("iframe_port_scan").src="?module=core&action=blank&root_tpl=blank_panel";
	document.getElementById("hostname_ip").value = ip;
	showlastRefreshed("Connections");
	// populate the other panels with data
	document.getElementById("iframe_hostname").src = "?module=mod_settings&action=manage_hostnames&search=1&tpl=net_pnl_list_hostnames&search_key=" + ip;
}

function showProtocolDetail(protocol){
	document.getElementById("iframe_help").src = "?module=mod_help&action=get_proto&proto=" + protocol;
	activatePanel(document.getElementById("Netmon_Help"));
}

function showlastRefreshed(){
	myDate = new Date();
	document.getElementById("lastRefreshedConn").innerHTML = myDate.toString();
}

function AdjustIframes(){
	document.getElementById("iframe_help").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("Remote_Connections").offsetHeight - document.getElementById("Port_Scan").offsetHeight - document.getElementById("Host_Names").offsetHeight - document.getElementById("Tools").offsetHeight - document.getElementById("Netmon_Help").offsetHeight - document.getElementById("toolbar0").offsetHeight - 16;
}


</script>
{/literal}


<!-- Active Connections Panel -->
<div>
	<!-- Menu header box -->
	<div id="Remote_Connections" class="titlebar_collapsed" onclick="activatePanel(this)">
		Active Connections
	</div>
	<div class="panel, closed">

		<!-- Button Row -->
	<div class="panel">
		<form id="" name="" action="index.php" target="iframe_active_connections" method="get" onSubmit="showlastRefreshed()">
		<input type="hidden" name="module" value="mod_network">
		<input type="hidden" name="action" value="get_active_connections">
		<input type="hidden" name="root_tpl" value="blank_panel">
		Host: <input type="text" name="ip" id="ip" style="width: 100px;" value="{$smarty.get.ip|default:""}" />
		<img src="assets/buttons/button_print.gif" class="icon" onClick="iframe_active_connections.focus();iframe_active_connections.print();" title="Print this page">
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="showHelp('panel_active_connections');" title="Show Help">
		</form>
	</div>

	<div>
		<!-- Sub-Window (Inline Frame) -->
		<iframe src="?module=mod_network&action=get_active_connections&root_tpl=blank_panel&ip={$ip}" id="iframe_active_connections" name="iframe_active_connections" width="100%" height="300" frameborder="0"></iframe>

		<!-- Button Row -->
		<div class="panel">
			Last Refreshed: <span id="lastRefreshedConn"></span>
		</div>
	</div>
</div>






<!-- Port Scanner Box -->
<div id="Port_Scan" class="titlebar_collapsed" onclick="activatePanel(this)">
	Port Scan
</div>
<div class="panel, closed">

	<!-- Button Row -->
	<form id="form_port_scan" name="form_port_scan" action="index.php" target="iframe_port_scan" method="get">
	<input type="hidden" name="module" value="mod_network">
	<input type="hidden" name="action" value="perform_portscan">
	<input type="hidden" name="root_tpl" value="blank_panel">
	<div class="panel">
		Host: <input type="text" name="portscan_ip" id="portscan_ip" style="width: 100px;" value="{$smarty.get.portscan_ip|default:""}" />
		&nbsp;
		<input type="submit" id="goscan" value="Scan Now" />
		<img src="assets/buttons/button_print.gif" class="icon" onClick="iframe_port_scan.focus();iframe_port_scan.print();">
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="showHelp('port_scanner');">

	</div>

	<div class="panel">
		<input type="radio" name="scan_type" id="scan_type_std" value="std" checked="true" onClick="document.getElementById('port_list').disabled=true; document.getElementById('advisory').style.display='none';" /> Standard
		&nbsp;
		<input type="radio" name="scan_type" id="scan_type_com" value="com" onClick="document.getElementById('port_list').disabled=true; document.getElementById('advisory').style.display='block';" /> Complete
		&nbsp;
		<input type="radio" name="scan_type" id="scan_type_cus" value="cus" onClick="document.getElementById('port_list').disabled=false; document.getElementById('advisory').style.display='none';" /> Range:
		&nbsp;
		<input type="text" name="port_list" id="port_list" value="" style="width: 60px;" disabled="true" />
	</div>
	<div class="panel" style="background-color: #FFFF80; display: none" id="advisory"><div><img src="assets/icons/icon_caution.gif" width="16" height="16" border="0" align="absmiddle" /> &nbsp;<strong>Advisory</strong>:&nbsp;Running a complete scan can take several minutes or more.</div></div>
	</form>

	<!-- Sub-Window (Inline Frame) -->
	<iframe src="?module=core&action=blank&root_tpl=blank_panel" id="iframe_port_scan" name="iframe_port_scan" width="100%" height="200" frameborder="0"></iframe>

	<!-- Button Row -->
	<div class="panel">
		&nbsp;
	</div>
</div>



<!-- Host Name(s) Panel -->
<div id="Host_Names" class="titlebar_collapsed" onclick="activatePanel(this)">
	Host Name(s)
</div>

<div class="panel, closed">

	<!-- Button Row -->
	<div class="panel">
		Host: <input type="text" name="host" id="hostname_ip" value="{$smarty.get.ip|default:""}" style="width: 100px;"  />
		<input type="button" value="Search" onClick="iframe_hostname.location='?module=mod_settings&action=manage_hostnames&tpl=net_pnl_list_hostnames&search=1&search_key=' + document.getElementById('hostname_ip').value;">
		<img src="assets/buttons/button_print.gif" class="icon" onClick="iframe_hostname.focus();iframe_hostname.print();">
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="showHelp('host_mgmt');">
	</div>

	<!-- Sub-Window (Inline Frame) -->
	<iframe src="?module=mod_settings&action=form_create_hostname" id="iframe_hostname" name="iframe_hostname" width="100%" height="240" frameborder="0"></iframe>

	<!-- Button Row -->
	<div class="panel">
		&nbsp;
	</div>
</div>


<!-- Tools Panel -->
<div id="Tools" class="titlebar_collapsed" onclick="activatePanel(this)">
	Tools
</div>

<div class="panel, closed">

	<!-- Button Row -->
	<div class="panel">
		<img src="assets/buttons/button_print.gif" class="icon" onClick="iframe_tools.focus();iframe_tools.print();">
		{input class="button" type="button" name="ns_lookup" value="DNS Lookup" onClick="iframe_tools.location='?module=mod_network&action=form_ns_lookup';"}
		{input class="button" type="button" name="tcpdump" value="Traffic Capture" onClick="iframe_tools.location='?module=mod_network&action=form_tcpdump';"}
		{input class="button" type="button" name="traceroute" value="Traceroute" onClick="iframe_tools.location='?module=mod_network&action=form_traceroute';"}
	</div>

	<!-- Sub-Window (Inline Frame) -->
	<!-- Open up the NS Lookup tool by default -->
	<iframe src="?module=mod_network&action=form_ns_lookup" id="iframe_tools" name="iframe_tools" width="100%" height="240" frameborder="0"></iframe>

	<!-- Button Row -->
	<div class="panel">
		&nbsp;
	</div>
</div>



{include file="help_panel.tpl"}

<script language="Javascript">
showHelp('visual_net');
window.onresize = AdjustIframes;
AdjustIframes();
</script>

<!-- end of {$smarty.template} -->
