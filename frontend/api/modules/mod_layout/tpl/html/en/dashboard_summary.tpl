<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation"}
<!-- Active Content Script -->

{literal}
<script>

alert_success_handler = function(o) {
	divElem = document.getElementById('alert_content')
	divParent = document.getElementById('alert_area')
	divParent.style.opacity = 0.01;

	if (divElem.innerHTML != o.responseText) {
		divElem.innerHTML = o.responseText;
		console.log("Updating alert content");
	} else {
		console.log("Alert content has not changed. Skipping update");
	}
	
	var transition_attributes = {
		opacity: {to: 1},
		duration: 1
	}
	
	var transition = new YAHOO.util.Anim(divParent, transition_attributes);
	transition.animate();	
	window.setTimeout(refresh_alerts, 25000);
}

alert_failure_handler = function() {
	document.getElementById('alert_area').innerHTML = "Unable to fetch current alerts. Will try again shortly";
	window.setTimeout(refresh_alerts, 25000);
}

refresh_alerts = function() {
	url = "?module=mod_services&action=get_system_status&root_tpl=blank";
	YAHOO.util.Connect.asyncRequest('GET', url, {success:alert_success_handler, failure:alert_failure_handler}, null);
}

var gm = new GraphManager();


</script>
{/literal}


<div class="titlebar">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td>Current System Status</td>
		<td align="right"><img id="restore" src="assets/core/restore.gif" class="button" width="13" height="15" onClick="parent.document.body.cols = '60%,40%'; document.getElementById('maximize').style.display='block'; this.style.display='none';" title="Restore" style="display: none;"> <img id="maximize" class="button" src="assets/core/maximize.gif" width="13" height="15" onClick="parent.document.body.cols = '*,0'; document.getElementById('restore').style.display='block'; this.style.display='none';" title="Maximize"></td>
	</tr>
</table>
</div>

<div id="alert_area"><div id="alert_content"></div></div>
{*madnet_action module="mod_services" action="get_services_stats"*}
{madnet_action module="mod_dashboard" action="get_protocol_graph"}
{madnet_action module="mod_dashboard" action="get_bandwidth_monitors"}
{madnet_action module="mod_dashboard" action="get_oid_monitors"}

<script>
//parent.parent.toolbar.MM_nbGroup('down','group1','dashboard','assets/toolbar/home_over.gif',1);
gm.init();
refresh_alerts();
</script>

<!-- end of {$smarty.template} -->
