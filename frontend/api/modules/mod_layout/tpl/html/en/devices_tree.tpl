<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
<!-- Menu header box -->
{literal}
<script>
function AdjustIframes(){
	document.getElementById("iframe_snmp_explorer").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("titlebar0").offsetHeight - 4;
}
window.onresize = AdjustIframes;
</script>
{/literal}

<div class="titlebar" id="titlebar0">
	Device Explorer
</div>

<!-- Button Row -->
<!-- 
<div class="panel" id="toolbar0">
	{input name="Submit" type="submit" class="button" value="Add New Device" onClick="parent.pnl_right.showEditor('?module=mod_devices&action=form_create_snmp_device&root_tpl=blank_panel');"}
	{input type="button" class="button" onClick="parent.pnl_middle.location.href='?module=mod_devices&action=manage_mibs'" value="Manage MIBs"}
	<img src="assets/buttons/button_help.gif" alt="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('snmp_device_explorer');" title="Help" alt="Help" />
</div>
 -->

<!-- Sub-Window (Inline Frame) -->
<iframe src="?module=mod_devices&action=get_snmp_tree&root_tpl=blank_panel&class=white" id="iframe_snmp_explorer" width="100%" height="100%" name="iframe_snmp_explorer" frameborder="0"></iframe>


<script>
AdjustIframes();
parent.parent.toolbar.MM_nbGroup('down','group1','devices','assets/toolbar/devices_over.gif',1);
</script>
<!-- end of {$smarty.template} ($Id$) -->
