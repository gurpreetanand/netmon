<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
{literal}
<script>
function AdjustIframes(){	
	document.getElementById("tree_syslog").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("titlebar0").offsetHeight - document.getElementById("toolbar0").offsetHeight - 4;
}
window.onresize = AdjustIframes;
</script>
{/literal}

<!-- Menu header box -->
<div class="titlebar" id="titlebar0">
	Event Log Explorer
</div>

<!-- Button Row -->
<div class="panel" id="toolbar0">
	View By: 
	  <select name="explore_by" onChange="document.getElementById('tree_syslog').src='?module=mod_syslog&action=explore&class=white&root_tpl=blank_panel&explore_by=' + this.value;">
	    <option value="ip">Client</option>
	    <option value="facility">Facility</option>
	    <option value="severity" selected="selected">Severity</option>
      </select>
	   
	   <img src="assets/buttons/button_help.gif" alt="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('syslog_explorer');" title="Show Help" />
	   
</div>

<!-- Sub-Window (Inline Frame) -->
<iframe src="?module=mod_syslog&action=explore&root_tpl=blank_panel&class=white" id="tree_syslog" width="100%" height="420px" name="iframe_syslog_explorer" frameborder="0"></iframe>

<script>
AdjustIframes();
parent.parent.toolbar.MM_nbGroup('down','group1','syslog','assets/toolbar/syslog_over.gif',1);
</script>


<!-- end of {$smarty.template} -->
