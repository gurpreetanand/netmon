<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Network Monitoring and SNMP</title>
<link href="/assets/lib_css.css" rel="stylesheet" type="text/css">
<link href="/assets/lib_css_printable.css" rel="stylesheet" type="text/css" media="print">
</head>
<body class="iframe">
<div class="panel white">
<h1>Network Monitoring and SNMP</h1>
<p>Effective network monitoring encompasses a broad range of responsibilities. You need to understand your network traffic from several vantage points, but it also becomes important to monitor the health, availability and load of many different kinds of mission-critical devices.</p>
<h2>Introducing SNMP</h2>
<p>The solution is the Simple Network Management Protocol (SNMP): a widely supported monitoring and management protocol for network-aware devices. Managed devices, as SNMP-capable devices are otherwise known, can include things like switches, routers, multi-function printers, fax stations, firewalls, thin clients, wireless transmitters, and much more. Thousands of different devices support the SNMP protocol.</p>
<p>SNMP provides the ability to query and update a managed device remotely. Using this protocol, you can retrieve a potentially rich set of information about a particular device: data such as inbound and outbound traffic levels, current connections, CPU load, memory status, usage history, error messages, device status, and countless other details. This is really nice stuff to know. Furthermore, SNMP 'write' operations can even allow devices to be configured and managed remotely.</p>
<p>Devices can also be configured to automatically 'push' SNMP data to a remote monitoring or management system. For example, you might configure a laser printer to send information about current toner level. These UDP datagrams are known as SNMP traps, and they're generally sent to a remote monitoring system where they're collected and handled appropriately. (Netmon 3.5 will feature an SNMP trap handling engine.) </p>
<h2>The SNMP Protocol </h2>
<p>The SNMP protocol itself is a relatively simple request-response protocol. It works at the application layer, and typically utilizes UDP ports 161 and 162.</p>
<p>The choice of UDP may seem a bit unusual for a request-response protocol, but SNMP was designed from the outset to move across the network as 'non-critical' traffic. In high load situations, UDP packets that are dropped from the network are not resent by the originating host. This  reduces network congestion in critical load situations. To ensure that SNMP traffic doesn't unnecessarily burden a network, its designers skipped the higher overhead of a full-blown TCP connection in favour of a more graceful failure scenario.</p>
<p>Every managed device keeps a hierarchial database of values, known as a Management Information Base (MIB). These MIBs are sent as numerical indexes (known as object identifiers, or OIDs) in the SNMP packet payload, and each one represents some type of configuration detail. Each MIB has an associated meaning, such as the following:</p>
<p> <span class="inlineHeading">MIB: </span>Cisco Router <br>
  <span class="inlineHeading">OID:</span>  1.3.6.1.4.1.9.1.1 </p>
<h2>The Good, the Bad and the Ugly</h2>
<p>White it is certainly true that SNMP can provide you with a rich source of information for every managed device on your network, it also comes with a few drawbacks.</p>
<p>First off, while SNMP is indeed a &lsquo;simple&rsquo; protocol, its real world implementation is not very simple at all. SNMP data is built around the idea that <em>any kind of information</em> can be stored and communicated by a managed device. Of course, different devices will want to communicate different kinds of data. Switches will tell you how much traffic is going in and out of each port, and so will firewalls, but printers might tell you how many pages have been printed today, or how much ink is left in each of the cartridges.</p>
<p>The result is that every device implements SNMP data structures in their own unique way, and there are only a handful of standard OID/MIB interfaces which are available across all types of devices. This makes the task of using SNMP data in a comprehensive monitoring or management system a non-trivial undertaking. SNMP management systems tend to be large, unweildly and tremendously expensive systems,  and their complexity can make one question the benefits of using SNMP in the first place.</p>
<h2>SNMP and Security</h2>
<p>The introduction of any new protocol on the network merits some attention, and SNMP deserves more scrutinty than most. Unfortunately, the most popular implementations of SNMP (known as SNMP v1 and SNMP v2) are not particularly well known for their strong security. In fact, SNMP's security record is so dismal, it has picked up a  new dual meaning: Security Not My Problem (SNMP).</p>
<p>SNMP services and protocols are not necessarily a direct security threat themselves: attacks on SNMP are relatively uncommon. This is probably due to the fact that there are thousands of different implementations out there - any kind of attack would likely have to be narrowly focused at a single device, or class of devices.</p>
<p>However, a much  larger security threat exists with the information that SNMP makes available to a potential intruder. SNMP data is transmitted in clear text, which could pose a problem if you're sending certain kinds of information over a non-private, unprotected network such as the Internet. In fact, unfettered SNMP read access could allow an attacker to gather hundreds of configuration details about your network - and this data could very well expose the keys to the kingdom.</p>
<p>Many SNMP-capable devices are shipped and installed with weak (or non-existent) SNMP community strings. A community string is the closest thing to a password in SNMP v2 and earlier devices, so it's incredibly important to ensure that you change these strings to strong passwords that meet modern security standards.</p>
<p>Fortunately, some of the most pressing security issues have been resolved with SNMP v3, the latest and greatest implementation of this protocol. Encrypted traffic is now supported, along with much stronger authentication mechanisms. However, there are still relatively few devices which support this new implementation of the protocol, despite its age - nearly 7 years at the time of writing.</p>
<p>In the meantime, you should review  your managed devices, and evaluate their roles in your monitoring strategy. Check for the following:</p>
<ol>
  <li> Does the SNMP service on this device need to be active at all? Do I really need to gather performance data from this device? (In many cases the answer is <em>Yes</em>.) </li>
  <li>Is the Community String set to a strong password phrase?</li>
  <li>What kind of SNMP data is being polled from this device? Is it safe for this information to traverse the LAN/WAN/Internet?</li>
  <li>Have SNMP write operations been disabled?</li>
</ol>
<h2>SNMP's Role in Network Monitoring </h2>
<p>SNMP has a few warts, but can nevertheless occupy a very effective role in an overall network monitoring strategy.</p>
<p>Despite the rich variety of information it makes accessible, SNMP really shouldn't be used to monitor the network. Many monitoring and management systems use the SNMP protocol exclusively to gather information about the network, but if this is the only way you are monitoring, then you&rsquo;re likely to be missing out on the big picture.</p>
<p>Think about it. In most cases, you will probably value the integrity of your entire network over that of any individual host. SNMP is great to gather data about devices, but in these situations you just can't beat a packet sniffer to get a real understanding of your network's actual state. Nevertheless, SNMP plays an important role in an overall network monitoring strategy.</p>
<h2>Netmon and SNMP</h2>
<p>Netmon is capable of retrieving traffic-related information from a wide variety of SNMP-capable devices, and the nice part is that it can grab data for each distinct network interface. This is especially helpful for switches, firewalls and routers, where you&rsquo;ll want to monitor traffic levels across each physical port. To work with this information, you&rsquo;ll need to take two steps.</p>
<p>To gather SNMP traffic data from your device, first enable SNMP on your managed device, and configure it to allow SNMP read (or &quot;polling&quot;) operations. This process varies greatly by manufacturer. Some devices (like switches and routers) may need to be configured through a command line interface, while other devices (such as printers and other multifunction products) may provide a slick web interface. Be sure to specify a strong community string passphrase.</p>
<p>The second step is to add your SNMP device in Netmon&rsquo;s SNMP Device Explorer. You'll have to supply your device's community string to Netmon. Once you have added your device, the Netmon SNMP Service will begin polling that device for information. For additional configuration information, see the Netmon User Guide.</p>
<p>Once these steps are completed, you should start to see SNMP traffic data within a few minutes. Netmon&rsquo;s SNMP viewing tools allow you to easily spot trends and spikes for each distinct device interface, and you can historical charts and graphs as well. </p>
<h2>Troubleshooting Tips</h2>
<p>Having trouble seeing your SNMP data? Use Netmon to diagnose the problem. Turn on the built-in <em>SNMP Activity</em> traffic filter in the Visual Network Explorer, and you&rsquo;ll be able to tell if your managed device is generating SNMP traffic to Netmon over the correct UDP port(s). Netmon expects to receive SNMP traps over UDP port 162, and it polls devices on UDP port 161.</p>
<h2>Where to get more information</h2>
<p>Netmon&rsquo;s built-in <em>Protocol Atlas</em> contains more information about SNMP, and includes links to third-party web resources as well. Click the Protocol Atlas icon in the <em>Help &amp; Resources Panel </em>for more information on SNMP monitoring.</p>
</div>
</body>
</html>
