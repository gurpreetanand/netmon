<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Unknown Protocol</title>
<link href="assets/lib_css.css" rel="stylesheet" type="text/css">
<link href="assets/lib_css_printable.css" rel="stylesheet" type="text/css" media="print">
</head>
<body class="iframe">
<div class="panel white">
<h1>  <img src="assets/icons/icon_caution.gif" width="16" height="16" class="icon"> &nbsp;Unknown Protocol ({$smarty.get.transport_layer}-{$smarty.get.port_number})</h1>
<p>This protocol is not known to Netmon. You can <a href="http://www.google.ca/search?hl=en&q={$smarty.get.transport_layer} +{$smarty.get.port_number}&btnG=Google+Search&meta=" target="_blank">Google it now.</a> </p>

<p>TCP traffic over this port  could represent reply traffic on an ephemeral port. In every TCP network conversation, there is always request traffic and reply traffic. Requests are made to common service ports (such as port 80, for <a href="?module=mod_help&action=get_protocol_doc&transport_layer=TCP&port_number=80">HTTP</a>) but the response data usually arrives back to the local host using a system-assigned TCP port. Before you get too suspicious, see if this traffic can be matched with a (usually smaller) request message.</p>

</div>
</body>
</html>
