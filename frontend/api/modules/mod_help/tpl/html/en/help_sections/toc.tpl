{import_js files="yui.yahoo,yui.event,yui.dom,yui.treeview"}
<body class="white">
<div style="padding-top: 10px; padding-left: 10px; padding-bottom: 0px; margin-bottom: -5px;">
<h1><img src="assets/icons/book.gif" class="icon"> &nbsp;Netmon User Guide</h1>
</div>
{literal}
<script>
var tree;
function treeInit() {
	tree = new YAHOO.widget.TreeView("treeDiv1");
	var root = tree.getRoot();

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Welcome to Netmon", id:"welcome" } ;
   var welcome = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Introducing Netmon {/literal}{$netmonversion}{literal}", id:"welcome_1", href:"?module=mod_help&action=get_help&section=introducing_netmon&root_tpl=blank" } ;
   var welcome_1 = new YAHOO.widget.TextNode(myobj, welcome, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> What's New in Netmon {/literal}{$netmonversion}{literal}", id:"welcome_2", href:"?module=mod_help&action=get_help&section=whatsnew_{/literal}{$netmonversion|regex_replace:"/\./":""|regex_replace:"/\s/":""}{literal}&root_tpl=blank" } ;
   var welcome_2 = new YAHOO.widget.TextNode(myobj, welcome, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Key Features and Benefits", id:"welcome_3", href:"?module=mod_help&action=get_help&section=key_features&root_tpl=blank" } ;
   var welcome_3 = new YAHOO.widget.TextNode(myobj, welcome, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Help & Resources Panel", id:"welcome_5", href:"?module=mod_help&action=get_help&section=panel_help_resources&root_tpl=blank" } ;
   var welcome_5 = new YAHOO.widget.TextNode(myobj, welcome, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Deployment Guide", id:"deployment" } ;
   var deployment = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Planning Your Deployment", id:"deployment_plan" } ;
   var deployment_plan = new YAHOO.widget.TextNode(myobj, deployment, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Basic Preparation Tasks", id:"deployment_plan_prep", href:"?module=mod_help&action=get_help&section=basic_prep&root_tpl=blank" } ;
   var deployment_plan_prep = new YAHOO.widget.TextNode(myobj, deployment_plan, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Deployment Scenarios", id:"deployment_plan_scen", href:"?module=mod_help&action=get_help&section=deployment_scenarios&root_tpl=blank" } ;
   var deployment_plan_scen = new YAHOO.widget.TextNode(myobj, deployment_plan, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Single NIC or Dual NIC Deployments", id:"deployment_plan_nics", href:"?module=mod_help&action=get_help&section=multi_nics&root_tpl=blank" } ;
   var deployment_plan_nics = new YAHOO.widget.TextNode(myobj, deployment_plan, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Preparing Your Network", id:"deployment_prep" } ;
   var deployment_prep = new YAHOO.widget.TextNode(myobj, deployment, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Configuring a Switch for Traffic Monitoring", id:"deployment_prep_switch", href:"?module=mod_help&action=get_help&section=preparation_sniffing&root_tpl=blank" } ;
   var deployment_prep_switch = new YAHOO.widget.TextNode(myobj, deployment_prep, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Optimizing DHCP for Effective Monitoring", id:"deployment_prep_dhcp", href:"?module=mod_help&action=get_help&section=preparation_dhcp&root_tpl=blank" } ;
   var deployment_prep_dhcp = new YAHOO.widget.TextNode(myobj, deployment_prep, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Deploying Netmon", id:"deployment_netmon" } ;
   var deployment_netmon = new YAHOO.widget.TextNode(myobj, deployment, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Rack Mounting the Netmon Server", id:"deployment_netmon_rack", href:"?module=mod_help&action=get_help&section=server_mounting&root_tpl=blank" } ;
   var deployment_netmon_rack = new YAHOO.widget.TextNode(myobj, deployment_netmon, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Starting Up the Netmon Server", id:"deployment_netmon_start", href:"?module=mod_help&action=get_help&section=server_startup&root_tpl=blank" } ;
   var deployment_netmon_start = new YAHOO.widget.TextNode(myobj, deployment_netmon, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Netmon System Console", id:"deployment_netmon_shell", href:"?module=mod_help&action=get_help&section=shell_console&root_tpl=blank" } ;
   var deployment_netmon_shell = new YAHOO.widget.TextNode(myobj, deployment_netmon, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> IP Address Assignment", id:"deployment_netmon_ip", href:"?module=mod_help&action=get_help&section=ip_address_assignment&root_tpl=blank" } ;
   var deployment_ip = new YAHOO.widget.TextNode(myobj, deployment_netmon, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Changing the System Password", id:"deployment_netmon_pw", href:"?module=mod_help&action=get_help&section=change_os_password&root_tpl=blank" } ;
   var deployment_netmon_pw = new YAHOO.widget.TextNode(myobj, deployment_netmon, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Post Deployment Tasks", id:"deployment_post", href:"?module=mod_help&action=get_help&section=post_deployment&root_tpl=blank" } ;
   var deployment_post = new YAHOO.widget.TextNode(myobj, deployment, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Getting Started", id:"getstart" } ;
   var getstart = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Logging In to the Netmon Application", id:"getstart1", href:"?module=mod_help&action=get_help&section=app_login&root_tpl=blank" } ;
   var getstart1 = new YAHOO.widget.TextNode(myobj, getstart, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Performing Basic Setup Tasks", id:"getstart2", href:"?module=mod_help&action=get_help&section=setup_wizard&root_tpl=blank" } ;
   var getstart2 = new YAHOO.widget.TextNode(myobj, getstart, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Help & Resources Panel", id:"getstart3", href:"?module=mod_help&action=get_help&section=panel_help_resources&root_tpl=blank" } ;
   var getstart3 = new YAHOO.widget.TextNode(myobj, getstart, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Defining Your Local Network", id:"getstart4", href:"?module=mod_help&action=get_help&section=localnet_setup&root_tpl=blank" } ;
   var getstart4 = new YAHOO.widget.TextNode(myobj, getstart, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Setting Up Alert Conditionals", id:"getstart5", href:"?module=mod_help&action=get_help&section=conditionals_setup&root_tpl=blank" } ;
   var getstart5 = new YAHOO.widget.TextNode(myobj, getstart, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Setting Up Netmon Users", id:"getstart6", href:"?module=mod_help&action=get_help&section=manage_users&root_tpl=blank" } ;
   var getstart6 = new YAHOO.widget.TextNode(myobj, getstart, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Setting Up SNMP Devices", id:"getstart7", href:"?module=mod_help&action=get_help&section=snmp_autodisc&root_tpl=blank" } ;
   var getstart7 = new YAHOO.widget.TextNode(myobj, getstart, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Backing Up Your Configuration", id:"getstart8", href:"?module=mod_help&action=get_help&section=backup_config&root_tpl=blank" } ;
   var getstart8 = new YAHOO.widget.TextNode(myobj, getstart, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Netmon Home Dashboard", id:"dash" } ;
   var dash = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Introducing the Netmon Home Dashboard", id:"dash1", href:"?module=mod_help&action=get_help&section=home_page&root_tpl=blank" } ;
   var dash1 = new YAHOO.widget.TextNode(myobj, dash, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Recently Discovered Hosts Panel", id:"dash2", href:"?module=mod_help&action=get_help&section=network_autodiscovery&root_tpl=blank" } ;
   var dash2 = new YAHOO.widget.TextNode(myobj, dash, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Top Activity Snapshot Panel", id:"dash3", href:"?module=mod_help&action=get_help&section=panel_top_activity&root_tpl=blank" } ;
   var dash3 = new YAHOO.widget.TextNode(myobj, dash, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Top Web Destinations Panel", id:"dash4", href:"?module=mod_help&action=get_help&section=panel_top_webdest&root_tpl=blank" } ;
   var dash4 = new YAHOO.widget.TextNode(myobj, dash, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Top Web Users Panel", id:"dash5", href:"?module=mod_help&action=get_help&section=panel_top_webusers&root_tpl=blank" } ;
   var dash5 = new YAHOO.widget.TextNode(myobj, dash, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Top Ethernet Protocols Panel", id:"dash6", href:"?module=mod_help&action=get_help&section=panel_top_ethernet&root_tpl=blank" } ;
   var dash6 = new YAHOO.widget.TextNode(myobj, dash, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Network Activity", id:"network" } ;
   var network = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> How Netmon Monitors Traffic", id:"network0", href:"?module=mod_help&action=get_help&section=how_netmon_monitors&root_tpl=blank" } ;
   var network0 = new YAHOO.widget.TextNode(myobj, network, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using Netmon's Native Protocol Analyzers", id:"network1", href:"?module=mod_help&action=get_help&section=netmon_sniffers&root_tpl=blank" } ;
   var network1 = new YAHOO.widget.TextNode(myobj, network, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Collecting NetFlow Data Streams from Remote Devices", id:"network1", href:"?module=mod_help&action=get_help&section=netflow_intro&root_tpl=blank" } ;
   var network1 = new YAHOO.widget.TextNode(myobj, network, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Visual Network Explorer", id:"network1", href:"?module=mod_help&action=get_help&section=visual_net&root_tpl=blank" } ;
   var network1 = new YAHOO.widget.TextNode(myobj, network, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Active Connections Panel", id:"network2", href:"?module=mod_help&action=get_help&section=panel_active_connections&root_tpl=blank" } ;
   var network2 = new YAHOO.widget.TextNode(myobj, network, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Port Scan Tool", id:"network3", href:"?module=mod_help&action=get_help&section=port_scanner&root_tpl=blank" } ;
   var network3 = new YAHOO.widget.TextNode(myobj, network, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Capturing Raw Traffic", id:"network4", href:"?module=mod_help&action=get_help&section=raw_capture&root_tpl=blank" } ;
   var network4 = new YAHOO.widget.TextNode(myobj, network, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Host Names", id:"network6", href:"?module=mod_help&action=get_help&section=host_mgmt&root_tpl=blank" } ;
   var network6 = new YAHOO.widget.TextNode(myobj, network, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Network Services", id:"serv" } ;
   var serv = new YAHOO.widget.TextNode(myobj, root, false);

    myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Netmon Trackers Explorer", id:"serv1", href:"?module=mod_help&action=get_help&section=services_explorer&root_tpl=blank" } ;
   var serv1 = new YAHOO.widget.TextNode(myobj, serv, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Adding a New Tracker", id:"serv2", href:"?module=mod_help&action=get_help&section=services_add&root_tpl=blank" } ;
   var serv2 = new YAHOO.widget.TextNode(myobj, serv, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Attaching Alerts to a Tracker", id:"serv3", href:"?module=mod_help&action=get_help&section=services_alerts&root_tpl=blank" } ;
   var serv3 = new YAHOO.widget.TextNode(myobj, serv, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Modifying an Existing Tracker", id:"serv4", href:"?module=mod_help&action=get_help&section=services_modify&root_tpl=blank" } ;
   var serv4 = new YAHOO.widget.TextNode(myobj, serv, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Removing a Tracker", id:"serv5", href:"?module=mod_help&action=get_help&section=services_del&root_tpl=blank" } ;
   var serv5 = new YAHOO.widget.TextNode(myobj, serv, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Devices (SNMP)", id:"snmp" } ;
   var snmp = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Introduction to Simple Network Management Protocol (SNMP)", id:"snmp0", href:"?module=mod_help&action=get_nrk_item&doc=2005_01_snmp.htm&root_tpl=blank_panel" } ;
   var snmp0 = new YAHOO.widget.TextNode(myobj, snmp, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the SNMP AutoDiscovery Service", id:"snmp1", href:"?module=mod_help&action=get_help&section=snmp_autodisc&root_tpl=blank" } ;
   var snmp1 = new YAHOO.widget.TextNode(myobj, snmp, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Device Explorer", id:"snmp2", href:"?module=mod_help&action=get_help&section=snmp_device_explorer&root_tpl=blank" } ;
   var snmp2 = new YAHOO.widget.TextNode(myobj, snmp, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Device Toolbar", id:"snmp2a", href:"?module=mod_help&action=get_help&section=device_toolbar&root_tpl=blank" } ;
   var snmp2a = new YAHOO.widget.TextNode(myobj, snmp, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Interface Explorer", id:"snmp3", href:"?module=mod_help&action=get_help&section=snmp_interface_explorer&root_tpl=blank" } ;
   var snmp3 = new YAHOO.widget.TextNode(myobj, snmp, false);
   
      myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Device Dashboards ", id:"snmp3a", href:"?module=mod_help&action=get_help&section=snmp_device_dashboards&root_tpl=blank" } ;
   var snmp3a = new YAHOO.widget.TextNode(myobj, snmp, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Browsing SNMP MIBs", id:"snmp4", href:"?module=mod_help&action=get_help&section=snmp_mibs&root_tpl=blank" } ;
   var snmp4 = new YAHOO.widget.TextNode(myobj, snmp, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Custom SNMP MIBs", id:"snmp5", href:"?module=mod_help&action=get_help&section=snmp_mibupload&root_tpl=blank" } ;
   var snmp5 = new YAHOO.widget.TextNode(myobj, snmp, false);
 
    myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the OID Tracking Service ", id:"snmp5a", href:"?module=mod_help&action=get_help&section=oid_tracker&root_tpl=blank" } ;
   var snmp5a = new YAHOO.widget.TextNode(myobj, snmp, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing SNMP Traps", id:"snmp6", href:"?module=mod_help&action=get_help&section=snmp_traps&root_tpl=blank" } ;
   var snmp6 = new YAHOO.widget.TextNode(myobj, snmp, false);
   
   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Notes Manager", id:"snmp7", href:"?module=mod_help&action=get_help&section=notes_manager&root_tpl=blank" } ;
   var snmp7 = new YAHOO.widget.TextNode(myobj, snmp, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Windows Systems ", id:"win_srv" } ;
   var win_srv = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Windows Services", id:"win_srv1", href:"?module=mod_help&action=get_help&section=windows_services&root_tpl=blank" } ;
   var win_srv1 = new YAHOO.widget.TextNode(myobj, win_srv, false);
   
   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Windows Event Logs", id:"win_syslog", href:"?module=mod_help&action=get_help&section=windows_syslog&root_tpl=blank" } ;
   var win_syslog = new YAHOO.widget.TextNode(myobj, win_srv, false);
   
   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring SQL &amp; Exchange Metrics", id:"win_sql", href:"?module=mod_help&action=get_help&section=windows_sql&root_tpl=blank" } ;
   var win_sql = new YAHOO.widget.TextNode(myobj, win_srv, false);
   


   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring SYSLOG and Event Logs", id:"syslog" } ;
   var syslog = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Windows Event Logs", id:"syslog4", href:"?module=mod_help&action=get_help&section=syslog_windows&root_tpl=blank" } ;
   var syslog4 = new YAHOO.widget.TextNode(myobj, syslog, false);
   
      myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring SYSLOG Event Logs", id:"syslog4a", href:"?module=mod_help&action=get_help&section=syslog&root_tpl=blank" } ;
   var syslog4a = new YAHOO.widget.TextNode(myobj, syslog, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Event Log Explorer", id:"syslog1", href:"?module=mod_help&action=get_help&section=syslog_explorer&root_tpl=blank" } ;
   var syslog1 = new YAHOO.widget.TextNode(myobj, syslog, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Event Log Manager", id:"syslog2", href:"?module=mod_help&action=get_help&section=syslog_manager&root_tpl=blank" } ;
   var syslog2 = new YAHOO.widget.TextNode(myobj, syslog, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Event Log Viewer", id:"syslog3", href:"?module=mod_help&action=get_help&section=syslog_viewer&root_tpl=blank" } ;
   var syslog3 = new YAHOO.widget.TextNode(myobj, syslog, false);




  myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Disks and Partitions ", id:"disks_mon" } ;
   var disks_mon = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Windows Volumes", id:"disks_mon1", href:"?module=mod_help&action=get_help&section=disk_windows&root_tpl=blank" } ;
   var disks_mon1 = new YAHOO.widget.TextNode(myobj, disks_mon, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Linux and UNIX Partitions", id:"disks_mon2", href:"?module=mod_help&action=get_help&section=disk_nix&root_tpl=blank" } ;
   var disks_mon2 = new YAHOO.widget.TextNode(myobj, disks_mon, false);

  myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Monitoring Websites and Web Applications ", id:"webs" } ;
   var webs = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Introducing the URL Tracker Service", id:"webs1", href:"?module=mod_help&action=get_help&section=url_service&root_tpl=blank" } ;
   var webs1 = new YAHOO.widget.TextNode(myobj, webs, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Creating a New URL Tracker", id:"webs1a", href:"?module=mod_help&action=get_help&section=url_create&root_tpl=blank" } ;
   var webs1a = new YAHOO.widget.TextNode(myobj, webs, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Attaching Alerts to URL Trackers", id:"webs2", href:"?module=mod_help&action=get_help&section=url_alerts&root_tpl=blank" } ;
   var webs2 = new YAHOO.widget.TextNode(myobj, webs, false);




   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Netmon Reports", id:"report" } ;
   var report = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using Core Netmon Reports", id:"report1", href:"?module=mod_help&action=get_help&section=using_reports&root_tpl=blank" } ;
   var report1 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Creating and Saving Custom Reports", id:"report2", href:"?module=mod_help&action=get_help&section=custom_reports&root_tpl=blank" } ;
   var report2 = new YAHOO.widget.TextNode(myobj, report, false);
   
   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Report Scheduling &amp; Background Reports", id:"report2", href:"?module=mod_help&action=get_help&section=report_scheduling&root_tpl=blank" } ;
   var report2 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Network Activity Report", id:"report3", href:"?module=mod_help&action=get_help&section=report_network_activity&root_tpl=blank" } ;
   var report3 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Conversation Report", id:"report4", href:"?module=mod_help&action=get_help&section=report_conversation&root_tpl=blank" } ;
   var report4 = new YAHOO.widget.TextNode(myobj, report, false);
   
     myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Web Traffic Report", id:"report12", href:"?module=mod_help&action=get_help&section=report_web_traffic&root_tpl=blank" } ;
   var report12 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> UP/DOWN Time Report", id:"report5", href:"?module=mod_help&action=get_help&section=report_up_downtime&root_tpl=blank" } ;
   var report5 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Bandwidth Activity Report", id:"report6", href:"?module=mod_help&action=get_help&section=report_bandwidth_activity&root_tpl=blank" } ;
   var report6 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Bandwidth Consumption Report ", id:"report6a", href:"?module=mod_help&action=get_help&section=report_bandwidth_consumption&root_tpl=blank" } ;
   var report6a = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Disk Activity Report", id:"report7", href:"?module=mod_help&action=get_help&section=report_disk_activity&root_tpl=blank" } ;
   var report7 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Latency Report", id:"report8", href:"?module=mod_help&action=get_help&section=report_latency&root_tpl=blank" } ;
   var report8 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> OID Tracker Report", id:"report8a", href:"?module=mod_help&action=get_help&section=report_oid_tracker&root_tpl=blank" } ;
   var report8a = new YAHOO.widget.TextNode(myobj, report, false);
   
   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> URL Tracker Report", id:"report8b", href:"?module=mod_help&action=get_help&section=report_url_tracker&root_tpl=blank" } ;
   var report8b = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Port Scan Report", id:"report9", href:"?module=mod_help&action=get_help&section=report_portscan&root_tpl=blank" } ;
   var report9 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Alert History Report", id:"report10", href:"?module=mod_help&action=get_help&section=report_alert_history&root_tpl=blank" } ;
   var report10 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Netmon Login Report", id:"report11", href:"?module=mod_help&action=get_help&section=report_netmon_login&root_tpl=blank" } ;
   var report11 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Email Traffic Report", id:"report12", href:"?module=mod_help&action=get_help&section=report_mail_traffic&root_tpl=blank" } ;
   var report12 = new YAHOO.widget.TextNode(myobj, report, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Email Summary Report", id:"report13", href:"?module=mod_help&action=get_help&section=report_mail_summary&root_tpl=blank" } ;
   var report13 = new YAHOO.widget.TextNode(myobj, report, false);




   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> File Management", id:"files" } ;
   var files = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Files Manager", id:"files1", href:"?module=mod_help&action=get_help&section=files_manager&root_tpl=blank" } ;
   var files1 = new YAHOO.widget.TextNode(myobj, files, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing the Backups Folder", id:"files2", href:"?module=mod_help&action=get_help&section=files_backups&root_tpl=blank" } ;
   var files2 = new YAHOO.widget.TextNode(myobj, files, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing the Enterprise MIBs Folder", id:"files3", href:"?module=mod_help&action=get_help&section=files_mibs&root_tpl=blank" } ;
   var files3 = new YAHOO.widget.TextNode(myobj, files, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Netmon Log Files", id:"files4", href:"?module=mod_help&action=get_help&section=files_logs&root_tpl=blank" } ;
   var files4 = new YAHOO.widget.TextNode(myobj, files, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Traffic Capture Files", id:"files5", href:"?module=mod_help&action=get_help&section=files_captures&root_tpl=blank" } ;
   var files5 = new YAHOO.widget.TextNode(myobj, files, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Administration and Management", id:"admin" } ;
   var admin = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Settings Console", id:"admin1", href:"?module=mod_help&action=get_help&section=settings_console&root_tpl=blank" } ;
   var admin1 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Shell Control Panel", id:"admin2", href:"?module=mod_help&action=get_help&section=shell_console&root_tpl=blank" } ;
   var admin2 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Shutting Down and Restarting the Netmon Server Appliance", id:"admin3", href:"?module=mod_help&action=get_help&section=restart_box&root_tpl=blank" } ;
   var admin3 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Alert Conditionals", id:"admin4", href:"?module=mod_help&action=get_help&section=conditionals_setup&root_tpl=blank" } ;
   var admin4 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using Data Management Tools", id:"admin_data" } ;
   var admin_data = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Compacting the Database", id:"admin_data1", href:"?module=mod_help&action=get_help&section=database_compact&root_tpl=blank" } ;
   var admin_data1 = new YAHOO.widget.TextNode(myobj, admin_data, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Backing Up Your Configuration", id:"admin_data2", href:"?module=mod_help&action=get_help&section=backup_config&root_tpl=blank" } ;
   var admin_data2 = new YAHOO.widget.TextNode(myobj, admin_data, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Performing a Complete Backup", id:"admin_data3", href:"?module=mod_help&action=get_help&section=backup_complete&root_tpl=blank" } ;
   var admin_data3 = new YAHOO.widget.TextNode(myobj, admin_data, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Clearing the Database", id:"admin_dat5", href:"?module=mod_help&action=get_help&section=backup_clear&root_tpl=blank" } ;
   var admin_data5 = new YAHOO.widget.TextNode(myobj, admin_data, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing User Accounts", id:"admin5", href:"?module=mod_help&action=get_help&section=manage_users&root_tpl=blank" } ;
   var admin5 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Account Groups", id:"admin6", href:"?module=mod_help&action=get_help&section=manage_groups&root_tpl=blank" } ;
   var admin6 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Alert Message Templates", id:"admin7", href:"?module=mod_help&action=get_help&section=alert_templates&root_tpl=blank" } ;
   var admin7 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Alert Commands", id:"admin7a", href:"?module=mod_help&action=get_help&section=alert_commands&root_tpl=blank" } ;
   var admin7a = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing the Hostname Database", id:"admin8", href:"?module=mod_help&action=get_help&section=host_mgmt&root_tpl=blank" } ;
   var admin8 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Filter Collections", id:"admin9", href:"?module=mod_help&action=get_help&section=managing_filters&root_tpl=blank" } ;
   var admin9 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Local Network Definitions", id:"admin10", href:"?module=mod_help&action=get_help&section=localnet_setup&root_tpl=blank" } ;
   var admin10 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Using the Netmon Update Service", id:"admin9a", href:"?module=mod_help&action=get_help&section=netmon_updates&root_tpl=blank" } ;
   var admin9a = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing the Port Label Database", id:"admin11", href:"?module=mod_help&action=get_help&section=protocol_mgmt&root_tpl=blank" } ;
   var admin11 = new YAHOO.widget.TextNode(myobj, admin, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Managing Netmon System Services", id:"admin12", href:"?module=mod_help&action=get_help&section=managing_daemons&root_tpl=blank" } ;
   var admin12 = new YAHOO.widget.TextNode(myobj, admin, false);


   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Troubleshooting Guide", id:"trouble" } ;
   var trouble = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/book.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Where to Find Help", id:"trouble_help" } ;
   var trouble_help = new YAHOO.widget.TextNode(myobj, trouble, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Integrated Help & Resources Panel", id:"trouble_help1", href:"?module=mod_help&action=get_help&section=panel_help_resources&root_tpl=blank" } ;
   var trouble_help1 = new YAHOO.widget.TextNode(myobj, trouble_help, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Support for Active Product Subscribers", id:"trouble_help2", href:"?module=mod_help&action=get_help&section=support_subscribers&root_tpl=blank" } ;
   var trouble_help2 = new YAHOO.widget.TextNode(myobj, trouble_help, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Support for Non-Subscribers", id:"trouble_help3", href:"?module=mod_help&action=get_help&section=support&root_tpl=blank" } ;
   var trouble_help3 = new YAHOO.widget.TextNode(myobj, trouble_help, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Key Files and Their Locations", id:"trouble1", href:"?module=mod_help&action=get_help&section=key_files&root_tpl=blank" } ;
   var trouble1 = new YAHOO.widget.TextNode(myobj, trouble, false);

   myobj = { label: "<img src=\"assets/icons/bookopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Troubleshooting Traffic Sniffing", id:"trouble2", href:"?module=mod_help&action=get_help&section=trouble_sniffer&root_tpl=blank" } ;
   var trouble2 = new YAHOO.widget.TextNode(myobj, trouble, false);




   myobj = { label: "<img src=\"assets/icons/mib_table.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Appendix A: Database Reference", id:"db" } ;
   var db = new YAHOO.widget.TextNode(myobj, root, false);
{/literal}
{foreach from=$tables item=tab}
  {literal} myobj = { label: "<img src=\"assets/icons/mib_table.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Table: {/literal}{$tab}{literal}", id:"db_{/literal}{$tab}{literal}", href:"?module=mod_help&action=get_table&table_name={/literal}{$tab}{literal}&root_tpl=blank_panel&class=iframe" } ;
   var db_{/literal}{$tab}{literal} = new YAHOO.widget.TextNode(myobj, db, false);
{/literal}{/foreach}{literal}


   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Appendix B: Process Reference", id:"process" } ;
   var process = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>arpmond</strong>: ARP Probe Service", id:"process_arpmond", href:"?module=mod_help&action=get_help&section=daemon_arpmond&root_tpl=blank" } ;
   var process_arpmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>netscand</strong>: Background Port Scanning Service", id:"process_netscand", href:"?module=mod_help&action=get_help&section=daemon_netscand&root_tpl=blank" } ;
   var process_netscand = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>emailmond</strong>: Email Alert Service", id:"process_emailmond", href:"?module=mod_help&action=get_help&section=daemon_emailmond&root_tpl=blank" } ;
   var process_emailmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>ethmond</strong>: Ethernet Frame Analyzer", id:"process_ethmond", href:"?module=mod_help&action=get_help&section=daemon_ethmond&root_tpl=blank" } ;
   var process_ethmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>httpmond</strong>: HTTP Request Analyzer", id:"process_httpmond", href:"?module=mod_help&action=get_help&section=daemon_httpmond&root_tpl=blank" } ;
   var process_httpmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>netmond</strong>: IP Packet Analyzer - Master Processs", id:"process_netmond", href:"?module=mod_help&action=get_help&section=daemon_netmond&root_tpl=blank" } ;
   var process_netmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>netmond-slave</strong>: IP Packet Analyzer - Collector Processs", id:"process_netmond_slave", href:"?module=mod_help&action=get_help&section=daemon_netmond&root_tpl=blank" } ;
   var process_netmond_slave = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>resolvemond</strong>: Name Resolution Service", id:"process_resolvemond", href:"?module=mod_help&action=get_help&section=daemon_resolvemond&root_tpl=blank" } ;
   var process_resolvemond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>flowd</strong>: NetFlow Collector", id:"process_flowd", href:"?module=mod_help&action=get_help&section=daemon_flowd&root_tpl=blank" } ;
   var process_flowd = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong> oidmond</strong>: SNMP OID Tracker Service", id:"process_oidmond", href:"?module=mod_help&action=get_help&section=daemon_oidmond&root_tpl=blank" } ;
   var process_oidmond = new YAHOO.widget.TextNode(myobj, process, false);


   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong> pagermond</strong>: Pager Alert Service", id:"process_pagermond", href:"?module=mod_help&action=get_help&section=daemon_pagermond&root_tpl=blank" } ;
   var process_pagermond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>srvmond</strong>: Service Monitor", id:"process_srvmond", href:"?module=mod_help&action=get_help&section=daemon_srvmond&root_tpl=blank" } ;
   var process_srvmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>snmpautod</strong>: SNMP Auto Discovery Service", id:"process_snmpautod", href:"?module=mod_help&action=get_help&section=daemon_snmpautod&root_tpl=blank" } ;
   var process_snmpautod = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>snmpmond</strong>: SNMP Interface Monitor", id:"process_snmpmond", href:"?module=mod_help&action=get_help&section=daemon_snmpmond&root_tpl=blank" } ;
   var process_snmpmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>snmpmond</strong>: SNMP Proxy Service", id:"process_snmpproxyd", href:"?module=mod_help&action=get_help&section=daemon_snmpproxyd&root_tpl=blank" } ;
   var process_snmpproxyd = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>snmptrapd</strong>: SNMP Trap Handler", id:"process_snmptrapd", href:"?module=mod_help&action=get_help&section=daemon_snmptrapd&root_tpl=blank" } ;
   var process_snmptrapd = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>syslogmond</strong>: SYSLOG Server", id:"process_syslogmond", href:"?module=mod_help&action=get_help&section=daemon_syslogmond&root_tpl=blank" } ;
   var process_syslogmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>dfmond</strong>: UNIX Partition Monitoring Service", id:"process_dfmond", href:"?module=mod_help&action=get_help&section=daemon_dfmond&root_tpl=blank" } ;
   var process_dfmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>smbmond</strong>: Windows Share Monitoring Service", id:"process_smbmond", href:"?module=mod_help&action=get_help&section=daemon_smbmond&root_tpl=blank" } ;
   var process_smbmond = new YAHOO.widget.TextNode(myobj, process, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> <strong>webmond</strong>: URL Monitoring Service", id:"process_urlmond", href:"?module=mod_help&action=get_help&section=daemon_webmond&root_tpl=blank" } ;
   var process_urlmond = new YAHOO.widget.TextNode(myobj, process, false);



   tree.onExpand = function(node) {
      //alert(node.data.id + " was expanded");
   }

   tree.onCollapse = function(node) {
      //alert(node.data.id + " was collapsed");
   }

   tree.draw();
   
}
</script>
{/literal}

<div id="treeDiv1" class="tree" style="margin-left: 9px;"></div>
<script>
treeInit();
</script>


</body>
