{foreach from=$tables item=tab}


=={$tab.name}==

'''Table Overview'''

{$tab.desc}

'''Column Definitions'''

  {literal}{|{/literal} border="1"
      |-
      !Name
      !Type
      !Description
	{foreach from=$tab.column item=col}
	|-
	|{$col.name}
	|{$col.type}
	|{$col.desc}
	|-
	{/foreach}
  {literal}|}{/literal}

{/foreach}

