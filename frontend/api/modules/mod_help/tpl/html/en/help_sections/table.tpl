

<div class="panel white">
<h1><img src="assets/icons/mib_table.gif" width="16" height="16" class="icon"> &nbsp;&nbsp;Table: {$table_name}</h1>
<ul>
  <li><a href="#overview">Table Overview</a></li>
  <li><a href="#cols">Column Definitions</a></li>
</ul>
<h2><a name="overview"></a>Table Overview</h2>
<p>{$table_desc}</p>
<h2><a name="cols"></a>Column Definitions </h2>
<div class="datagrid">
  <table cellpadding="3" cellspacing="0" width="100%">
    <tbody>
      <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Description</th>
      </tr>
	{assign var="i" value="0"}
	{foreach from=$cols item=col}
      <tr class="tr{$i%2}">
	{foreach from=$col item=val}
        <td>{$val}</td>
        {/foreach}
	{math equation="x + 1" x=$i assign="i"}
      </tr>
	{/foreach}
    </tbody>
  </table>
  <!-- Constraint List -->
  <!-- Foreign Key Discovery -->
</div>
