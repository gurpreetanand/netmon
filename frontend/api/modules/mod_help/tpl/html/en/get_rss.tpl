<!-- {$smarty.template} ($Id$) -->
<div class="panel white">

<h1><img src="/assets/icons/rss.gif" width="14" height="14" class="icon" align="absmiddle"> Netmon Security & Monitoring News Center</h1>

<ul>
{foreach from=$rss item="channel"}
	<li><a href="#{$channel.info.title|truncate:5:""}">{$channel.info.title}</a></li>
{/foreach}
</ul>

{foreach from=$rss item="channel"}
	<h2><a name="{$channel.info.title|truncate:5:""}"></a>{$channel.info.title}</h2>
	{foreach from=$channel.items item="entry"}
	<dt><img src="assets/icons/news.gif" width="16" height="16" class="icon"> <strong>&nbsp;<a href="{$entry.link}" target="_blank">{$entry.title}</a></strong></dt>

	{* This hack should be sufficient to compensate for Microsoft's stupid unicode symbols in their RSS feeds *}
	<dd><p>{$entry.pubdate} - {$entry.description|escape:strip_tags|replace:"“":"&quot;"|replace:"”":"&quot;"|strip_tags}</p></dd>

	{/foreach}

{/foreach}
</div>
<script language="Javascript">
	hideProgressIndicator('progress', parent.document);
</script>
<!-- end of {$smarty.template} -->
