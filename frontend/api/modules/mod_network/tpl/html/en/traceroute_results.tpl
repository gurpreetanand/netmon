
<!-- {$smarty.template} ($Id$) -->

{if $hops}
<div class="datagrid">

<table width="auto" cellspacing="0" cellpadding="2" border="0" align="center">
	<tr>
		<th>Hop</th>
		<th>Router</th>
		<th>Latencies</th>
	</tr>
	{foreach from=$hops item="hop"}
	<tr>
		<td>{$hop.hopcount}</td>
		<td><div title="{$hop.ip}">{$hop.hostname}</div></td>
		<td>{foreach from=$hop.latencies item="latency"}
		{$latency|wordwrap:23:"<br />":TRUE}<br />
		{/foreach}</td>
	</tr>
	{/foreach}
</table>

</div>
{/if}

<!-- end of {$smarty.template} ($Id$) -->
