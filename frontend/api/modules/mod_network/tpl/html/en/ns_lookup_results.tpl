<!-- {$smarty.template} ($Id$) -->
<div class="printOnly">
	<table width="100%"  border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="/assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>DNS Loookup Tool: {$smarty.post.host}</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">{$smarty.now|date_format:"%Y-%m-%d %H:%M:%S"}</div></td>
	  </tr>

	</table>
	<br /><br />
</div>

<div class="datagrid">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
{foreach from=$rows item="row"}
	<tr>
		{foreach from=$row item="element"}
		<td>{$element}</td>
		{/foreach}
	</tr>
{/foreach}
</table>
</div>


<!-- end of {$smarty.template} -->
