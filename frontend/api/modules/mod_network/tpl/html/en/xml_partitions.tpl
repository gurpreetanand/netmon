<partitions>
{if $devs}
{foreach from=$devs item="dev"}
	<partition type="{$dev.type}" partition="{$dev.name}" ip="{$dev.ip}" displayname="{$dev.ip|resolve_ip}" id="{$dev.srv_id}" />
{/foreach}
{/if}
</partitions>
