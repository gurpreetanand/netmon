<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation"}
{literal}
<script language="Javascript">

hostname_success_handler = function(o) {
	hideProgressIndicator('hostname_progress');
	document.getElementById('hostname_field').value = o.responseText;
}

hostname_failure_handler = function(o) {
	hideProgressIndicator('hostname_progress');
	document.getElementById('hostname_field').value = o.document.getElementById('ip').value;
}

get_server_hostname = function() {
	ipElem = document.getElementById('ip');
	ip_addr = ipElem.value;
	
	url = "?module=mod_network&action=ajax_resolver_hook&root_tpl=blank&ip=" + ip_addr;
	
	showProgressIndicator('hostname_progress');
	YAHOO.util.Connect.asyncRequest('GET', url, {success:hostname_success_handler, failure:hostname_failure_handler}, null);
}

function toggle_divs(){
	if(document.getElementById("share_type").value == "smb"){
		document.getElementById("domain").disabled = false;
		document.getElementById("domain").className = "enabled";
		document.getElementById("username").disabled = false;
		document.getElementById("username").className = "enabled";
		document.getElementById("password").disabled = false;
		document.getElementById("password").className = "enabled";
		document.getElementById("add_disk").disabled = true;
		document.getElementById("add_disk").className = "disabled";
		//document.getElementById("share_input").innerHTML = "<input type=\"text\" id=\"share_name\" name=\"share_name\" value=\"\" disabled=\"true\">";
		TryCreds();
	} else if(document.getElementById("share_type").value == "df") {
		document.getElementById("domain").disabled = true;
		document.getElementById("domain").className = "disabled";
		document.getElementById("username").disabled = true;
		document.getElementById("username").className = "disabled";
		document.getElementById("password").disabled = true;
		document.getElementById("password").className = "disabled";
		document.getElementById("add_disk").disabled = false;
		document.getElementById("add_disk").className = "enabled";
		document.getElementById("share_input").innerHTML = "<input type=\"text\" id=\"share_name\" name=\"share_name\" value=\"{/literal}{$smarty.post.share_name|default:$smarty.post.partition}{literal}\"> Port: <input type=\"text\" size=\"3\" name=\"port\" value=\"\">";
	}
}

// AJAX handler
var req;
function loadXMLDoc(url) 
{
    // branch for native XMLHttpRequest object
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
        req.onreadystatechange = processReqChange;
        req.open("GET", url, true);
        req.send(null);
    // branch for IE/Windows ActiveX version
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
        if (req) {
            req.onreadystatechange = processReqChange;
            req.open("GET", url, true);
            req.send();
        }
    }
}

function processReqChange() 
{
    // only if req shows "complete"
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
            // ...processing statements go here...
			PopulateShares();
        } else {
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }
}

function TryCreds(){
	
	// This condition fixed bug #193
	if(document.getElementById("share_type").value == "smb"){
	
		//first we grab a reference to the DIV block which will be manipulated
		fieldContents = document.getElementById("share_input");
	
		try_ip = escape(document.getElementById("ip").value);
		try_domain = escape(document.getElementById("domain").value);
		try_username = escape(document.getElementById("username").value);
		try_password = escape(document.getElementById("password").value);
		// 
		if(try_ip.length > 1 && try_domain.length > 1 && try_username.length > 1 && try_password.length > 1){
			fieldContents.innerHTML="<span class=\"gray2\">Searching for shares...</span>";
			loadXMLDoc("?module=mod_network&action=ajax_auto_detect_smb_shares&username=" + try_username + "&password=" + try_password + "&domain=" + try_domain + "&ip=" + try_ip + "&root_tpl=blank");
		} else {
			fieldContents.innerHTML = "<input type=\"text\" id=\"share_name\" name=\"share_name\" value=\"\" disabled=\"true\">";
		}
	}
}

// fills a drop-down box with available share names
function PopulateShares(){
	if(req.responseText.length > 0){
		//first we grab a reference to the DIV block which will be manipulated
		fieldContents = document.getElementById("share_input");
		// we handle cases differently for smb or unix disks
		if(document.getElementById("share_type").value == "smb"){
			fieldContents.innerHTML = BuildSelectBox() + " <img src=\"assets/icons/up.gif\" width=\"16\" height=\"16\" style=\"vertical-align: middle;\" />";
			document.getElementById("add_disk").disabled = false;
			document.getElementById("add_disk").className = "enabled";
		}
	} else {
		fieldContents.innerHTML = "<img src=\"assets/icons/down.gif\" style=\"align: absmiddle;\" /> Unable to connect to share.";
		document.getElementById("add_disk").disabled = true;
		document.getElementById("add_disk").className = "disabled";
	}
}

function BuildSelectBox(){
		shareArray = req.responseText.split(",");
		boxHTML = "<select name=\"share_name\" id=\"share_name\">";
		for(i=0; i < shareArray.length - 1; i++){
			boxHTML = boxHTML + "<option value=\"" + shareArray[i] + "\">" + shareArray[i] + "</option>";
		}
		boxHTML = boxHTML + "</select>";
		return boxHTML;

}

</script>
{/literal}
<div class="panel">
<form action="?module=mod_network&action=process_add_disk" name="add_disk_form" method="POST">
<table width="100%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td width="40%" align="right">Disk Type:</td>
		<td width="60%">
			<select name="share_type" id="share_type" onChange="toggle_divs();">
				<option value="smb" {if $smarty.post.share_type == "smb"}selected{/if}>Windows Share</option>
				<option value="df" {if $smarty.post.share_type == "df"}selected{/if}>Unix/Linux Partition</option>
			</select>
		</td>
	</tr>
	<tr>
		<td align="right">Domain Name:</td>
		<td><input type="text" id="domain" {param name="domain"} value="{$smarty.post.domain}" onChange="TryCreds(); createCookie('DefaultDomain',this.value,365)"></td>
	</tr>
	<tr>
		<td align="right">IP Address:</td>
		<td><input type="text" id="ip" {param name="ip"} value="{$smarty.post.ip|default:$smarty.get.ip}" onChange="TryCreds(); get_server_hostname();"></td>
	</tr>
	<tr>
		<td align="right">Hostname:</td>
		<td><input type="text" {param name="servername"} id="hostname_field" value="{$smarty.post.servername|default:$smarty.get.servername}" /><img class="progress" id="hostname_progress" src="/assets/progress.gif" /></td>
	</tr>
	<tr>
		<td align="right">Username:</td>
		<td><input type="text" id="username" {param name="username"} value="" onChange="TryCreds()"></td>
		</td>
	</tr>
	<tr>
		<td align="right">Password:</td>
		<td><input type="password" id="password" {param name="password"} value="{$smarty.post.password}" onChange="TryCreds()"></td>
	</tr>

	<tr>
		<td align="right">Share/Partition Name:</td>
		<td><div id="share_input"><input type="text" id="share_name" name="share_name" value="" disabled="true"></div></td>
	</tr>
	<tr>
		<td align="right">Timeout:</td>
		<td><input type="text" size="3"id="timeout" {param name="timeout"} value="1"> minute(s)</td>
	</tr>
	<tr>
		<td align="right">Interval:</td>
		<td><input type="text" id="interval" size="3" {param name="interval"} value="300"> second(s)</td>
	</tr>
	<tr>
		<td align="right">Threshold:</td>
		<td><input type="text" id="threshold" size="3" {param name="threshold"} value="80">%</td>
	</tr>
	<tr>
		<td colspan="2" align="center" width="100%">{input type="submit" name="add_disk" id="add_disk" class="button" value="Add Disk" disabled="true"}</td>
	</tr>
</table>
</form>
</div>
{literal}
<script language="Javascript">
	toggle_divs();
	if(readCookie('DefaultDomain') != null){
		document.getElementById('domain').value = readCookie('DefaultDomain');
	}
</script>
{/literal}

<!-- end of {$smarty.template} -->
