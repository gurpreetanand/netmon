<!-- {$smarty.template} ($Id$) -->
<div class="printOnly">
	<table width="100%"  border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="/assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Netmon Port Scan Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">{$smarty.now|date_format:"%Y-%m-%d %H:%M:%S"}</div></td>
	  </tr>
	</table>
	<br /><br />
</div>

<div class="datagrid">
	<table width="100%" cellpadding="3" cellspacing="0">
		<tr>
			<td>{$header}</td>
		</tr>
		{if $ports}
		{foreach from=$ports item="port"}
		<tr>
			<td><a href="javascript:parent.showHelp('?module=mod_help&action=get_protocol_doc&transport_layer=TCP&port_number={$port.port}');"><img src="assets/colorblocks/block{$port.port|resolve_port_colour}.gif" class="icon" border="0" align="absmiddle"></a>&nbsp; <a href="javascript:parent.showHelp('?module=mod_help&action=get_protocol_doc&transport_layer=TCP&port_number={$port.port}');">{$port.name} ({$port.port})</a></td>
		</tr>
		{/foreach}
		{/if}
	</table>
</div>



<!-- end of {$smarty.template} -->
