
<!-- {$smarty.template} ($Id$) -->

	<div class="panel noPrint">
	<form action="?module=mod_network&action=perform_tcpdump" method="POST">
	<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
		<tr>
			<td>Number of packets:</td>
			<td>{html_options options=$packets name="packets"}</td>
		</tr>
		<tr>
			<td>Capture file label</td>
			<td>{input type="text" name="label"}</td>
		</tr>
		<tr>
			<td>Network Interface</td>
			<td>{html_options options=$interfaces name="interface"}</td>
		</tr>
		<tr>
			<td>IP Address (optional)</td>
			<td>{input type="text" name="ip_address"}</td>
		</tr>
		<tr>
			<td>Port Number (optional)</td>
			<td>{input type="text" size="5" name="port"}</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			{input type="submit" class="button" value="Begin Capture"}
			</td>
		</tr>
	</table>
	</form>
	</div>

<!-- end of {$smarty.template} -->
