<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation"}
{literal}
<script language="Javascript">
hostname_success_handler = function(o) {
	hideProgressIndicator('hostname_progress');
	document.getElementById('hostname_field').value = o.responseText;
}

hostname_failure_handler = function(o) {
	hideProgressIndicator('hostname_progress');
	document.getElementById('hostname_field').value = o.document.getElementById('ip').value;
}

get_server_hostname = function() {
	ipElem = document.getElementById('ip');
	ip_addr = ipElem.value;
	
	url = "?module=mod_network&action=ajax_resolver_hook&root_tpl=blank&ip=" + ip_addr;
	
	showProgressIndicator('hostname_progress');
	YAHOO.util.Connect.asyncRequest('GET', url, {success:hostname_success_handler, failure:hostname_failure_handler}, null);
}
// AJAX handler
var req;
function loadXMLDoc(url) 
{
    // branch for native XMLHttpRequest object
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
        req.onreadystatechange = processReqChange;
        req.open("GET", url, true);
        req.send(null);
    // branch for IE/Windows ActiveX version
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
        if (req) {
            req.onreadystatechange = processReqChange;
            req.open("GET", url, true);
            req.send();
        }
    }
}

function processReqChange() 
{
    // only if req shows "complete"
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
            // ...processing statements go here...
			PopulateShares();
        } else {
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }
}

function TryCreds(){
	//first we grab a reference to the DIV block which will be manipulated
	fieldContents = document.getElementById("share_input");

	try_ip = escape(document.getElementById("ip").value);
	try_domain = escape(document.getElementById("domain").value);
	try_username = escape(document.getElementById("username").value);
	try_password = escape(document.getElementById("password").value);
	// 
	if(try_ip.length > 1 && try_domain.length > 1 && try_username.length > 1 && try_password.length > 1){
		loadXMLDoc("?module=mod_network&action=ajax_auto_detect_smb_shares&username=" + try_username + "&password=" + try_password + "&domain=" + try_domain + "&ip=" + try_ip + "&root_tpl=blank");
	} else {
		fieldContents.innerHTML = "<input type=\"text\" id=\"share_name\" name=\"share_name\" value=\"\" disabled=\"true\">";
	}
}

// fills a drop-down box with available share names
function PopulateShares(){
	if(req.responseText.length > 0){
		//first we grab a reference to the DIV block which will be manipulated
		fieldContents = document.getElementById("share_input");
		// we handle cases differently for smb or unix disks
		fieldContents.innerHTML = BuildSelectBox() + " <img src=\"assets/icons/up.gif\" width=\"16\" height=\"16\" style=\"vertical-align: middle;\" />";
		document.getElementById("add_disk").disabled = false;
	} else {
		fieldContents.innerHTML = "<img src=\"assets/icons/down.gif\" style=\"align: absmiddle;\" /> Unable to connect to share.";
		document.getElementById("add_disk").disabled = true;
	}
}

function BuildSelectBox(){
		shareArray = req.responseText.split(",");
		boxHTML = "<select name=\"share\" id=\"share\">";
		for(i=0; i < shareArray.length - 1; i++){
			boxHTML = boxHTML + "<option value=\"" + shareArray[i] + "\">" + shareArray[i] + "</option>";
		}
		boxHTML = boxHTML + "</select>";
		return boxHTML;

}

</script>
{/literal}
<div class="panel">
<form action="?module=mod_network&action=process_update_disk&disk_type=smb&srv_id={$dm->get('srv_id')}" name="edit_disk_form" method="POST">
<table width="100%" cellpadding="3" cellspacing="0" border="0" align="center">
	<tr>
		<td align="right" width="40%">&nbsp;Domain name:</td>
		<td width="60%"><input type="text" id="domain" {param name="domain"} value="{$smarty.post.domain|default:$dm->get('domain')}" onChange="TryCreds()"></td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;IP Address:</td>
		<td width="60%"><input type="text" id="ip" {param name="ip"} value="{$smarty.post.ip|default:$dm->get('ip')}" onChange="TryCreds(); get_server_hostname();"></td>
	</tr>
	<tr>
		<td align="right" align="right">Hostname:</td>
		<td><input type="text" {param name="servername"} id="hostname_field" value="{$smarty.post.servername|default:$dm->get('servername')}" /><img class="progress" id="hostname_progress" src="/assets/progress.gif" /></td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;Username</td>
		<td width="60%"><input type="text" id="username" {param name="username"} value="{$smarty.post.username|default:$dm->get('username')}" onChange="TryCreds()"></td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;Password</td>
		<td width="60%"><input type="password" id="password" {param name="password"} value="{$smarty.post.password|default:$dm->get('password')}" onChange="TryCreds()"></td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;Share/Partition Name:</td>
		<td width="60%"><div id="share_input"><input type="text" name="share" {param name="share"} value="{$smarty.post.share|default:$dm->get('share')}"></div></td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;Timeout:</td>
		<td width="60%"><input type="text" size="3" name="timeout" {param name="timeout"} value="{$smarty.post.timeout|default:$dm->get('timeout')}"> minute(s)</td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;Interval:</td>
		<td width="60%"><input type="text" name="interval" size="3" {param name="interval"} value="{$smarty.post.interval|default:$dm->get('interval')}"> second(s) </td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;Threshold</td>
		<td width="60%"><input type="text" name="threshold" size="3" {param name="threshold"} value="{$smarty.post.threshold|default:$dm->get('threshold')}">%</td>
	</tr>
	<tr>
		<td colspan="2" align="center" width="100%">{input type="submit" name="add_disk" id="add_disk" class="button" value="Update Disk"}</td>
	</tr>
</table>
</form>
</div>

<!-- end of {$smarty.template} -->
