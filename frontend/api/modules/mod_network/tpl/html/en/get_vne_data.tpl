<!-- {$smarty.template} ($Id$) -->
<conversations hostfilter="" trafficfilter="">
{if $points}
{foreach from=$points item="point"}
	<conv sh="{$point.source.ip}" sname="{$point.source.hostname}" dh="{$point.dest.ip}" dname="{$point.dest.hostname}" rate="{$point.rate}" id="{counter}" stype="{$point.source.node_type|default:"default"}" dtype="{$point.dest.node_type|default:"default"}" />
{/foreach}
{/if}
</conversations>
<!-- end of {$smarty.template} -->
