
<!-- {$smarty.template} ($Id$) -->
{import_js files="AC_RunActiveContent"}
<script language="Javascript">
	parent.pnl_right.showEditor("?module=mod_devices&action=form_edit_snmp_interface&ip={$interface.ip_address}&interface={$interface.interface}");
</script>

<div class="titlebar_expanded">
Protocol Distribution on {if $interface.interface == -1}Local Traffic Sniffers{else}interface #{$interface.interface} on {$interface.label} ({$interface.ip_address}){/if}
 </div>

<!-- Toolbar -->
<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	Interface: {if $interface.interface == -1}Sniffers{else}{$interface.interface}{/if}
	<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	Speed: {$interface.speed}
    <img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	Mac Address: {if $interface.mac}<span id="mac" class="green">{$interface.mac}</span> {else} <span class="gray2"><strong>Unresolved</strong></span> {/if}
	<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	<img src="assets/buttons/button_help.gif" title="Help" alt="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('snmp_interface_explorer');">
</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	Connected IP/MAC: {if $interface.mac}<a href="#" onClick="parent.location = '?module=mod_layout&action=render_section&layout=network&ip={$interface.mac}&store_request=2'">{$interface.mac}</a>{else}<span class="gray2"><strong>Unresolved</strong></span>{/if}
	<img src="/assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	<img src="/assets/buttons/alerts.gif" width="24" height="24" border="0" class="icon" onClick="parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=snmp_iface_above_threshold&interface_id={$interface.id}')">
</div>


<div class="titlebar_expanded">
	Protocol Activity Breakdown
</div>

{capture assign="chart"}{if $protocol == 'netflow'}FC_2_3_StckdColumn3D.swf{else}FC_2_3_StckdArea.swf{/if}{/capture}

<div class="panel">
{assign var="height" value="430"}
{assign var="width"  value="470"}
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="{$width}" height="{$height}">
		  <param name="movie" value="assets/charts/{$chart}">
		  <param name="quality" value="">
		  <param name="bgcolor" value="#D4D0C8">
		  <param name="FlashVars" value="&dataURL=%3Fmodule=mod_network%26action=graph_get_protocols%26root_tpl=blank%26device={$smarty.get.device}%26interface={$smarty.get.interface}%26animation=1%26&chartWidth={$width}&chartHeight={$height}">
		  <embed src="assets/charts/{$chart}" bgcolor="#D4D0C8" FlashVars="&dataURL=%3Fmodule=mod_network%26action=graph_get_protocols%26root_tpl=blank%26device={$smarty.get.device}%26interface={$smarty.get.interface}%26animation=1%26&chartWidth={$width}&chartHeight={$height}" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="{$width}" height="{$height}"></embed>
		</object>
</div>



<!-- end of {$smarty.template} -->
