<?php


class Composite_Disk_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "none";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_network";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		require_class($this->module, "smb_disk_manager");
		require_class($this->module, "df_disk_manager");

		$smb = new SMB_Disk_Manager();
		$df  = new DF_Disk_Manager();

		$this->params->primitives = array_merge($smb->params->primitives, $df->params->primitives);

	}



	/**
	  * Returns an array containing the user ID of every user account in the DB
	  *
	  * @return mixed
	  */
	function get_all_ids() {
		$query = "
		select 'SMB' AS type, share AS name, srv_id, ip, servername, status,
		threshold, ((a.blocksize * a.total::int8) - (a.blocksize * a.available::int8))::int8 AS used,
		(a.blocksize * a.total::int8)::int8 AS total, timestamp, message
		FROM smb_servers a
		UNION
		select 'DF' AS type, partition AS name, srv_id, ip, servername, status,
		threshold, ((1024 * total::int8) - (1024 * available::int8))::int8 AS used, (1024 * total::int8)::int8 AS total,
		timestamp, message
		FROM df_servers b
		ORDER BY status DESC, srv_id ASC
		";

		$result = $this->db->select($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			return $result;
		}

	}

	function get_by_ip($ip) {
                $ip = $this->db->escape($ip);

		$query = "
		select 'SMB' AS type, share AS name, srv_id, ip, servername, status,
		threshold, ((a.blocksize * a.total::int8) - (a.blocksize * a.available::int8))::int8 AS used,
		(a.blocksize * a.total::int8)::int8 AS total, timestamp, message
		FROM smb_servers a WHERE ip=" . $ip . "
		UNION
		select 'DF' AS type, partition AS name, srv_id, ip, servername, status,
		threshold, ((1024 * total::int8) - (1024 * available::int8))::int8 AS used, (1024 * total::int8)::int8 AS total,
		timestamp, message
		FROM df_servers b WHERE ip=" . $ip . "
		ORDER BY status DESC, srv_id ASC
		";

		$result = $this->db->select($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			return $result;
		}

	}

}
?>
