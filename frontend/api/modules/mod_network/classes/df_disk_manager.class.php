<?php


class DF_Disk_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "df_servers";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "srv_id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_network";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		$this->params->add_primitive("ip",             "ip_address",       TRUE,   "IP Address",                       "IP Address");
		$this->params->add_primitive("servername",     "string",           FALSE,   "Hostname");
		$this->params->add_primitive("partition",      "string",           TRUE,   "Share/Partition name",             "Share/Partition name");
		$this->params->add_primitive("port",           "integer",          TRUE,  "Port Number",                      "Port Number");
		$this->params->add_primitive("timeout",        "integer",          TRUE,   "Timeout",                          "Timeout");
		$this->params->add_primitive("interval",       "integer",          TRUE,   "Interval",                         "Interval");
		$this->params->add_primitive("threshold",      "integer",          TRUE,   "Threshold",                        "Threshold");
    $this->params->add_primitive("homedisplay", "pg_bool", TRUE, "Homepage Display", "Display this Monitor on the home dashboard");
	}



	/**
	  * Returns an array containing the user ID of every user account in the DB
	  *
	  * @return mixed
	  */
	function get_all_ids() {
		$query = "SELECT {$this->pkey}, ip, threshold, status FROM {$this->table}";
		$result = $this->db->select($query);

		$data = $_POST[__CLASS__];
		
		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			return $result;
		}
	}

	function delete_by_ip($ip) {
	  $query = "SELECT {$this->pkey} FROM {$this->table} WHERE ip='{$ip}'";
	  $results = $this->db->select($query);
	  if ((DB_QUERY_ERROR == $results) || (DB_NO_RESULT == $results)) {
	    $results = array();
	  }
	  foreach ($results as $result) {
	    $this->delete($result[$this->pkey]);
	  }
	}

	function pre_insert($id = NULL) {
		
		/**
		  * Determine whether we are allowed to consume this IP slot
		  */
		if (NULL != $id) {
			$old_ip = $this->getBit($id, "ip");
		} else {
			$old_ip = NULL;
		}
		
		$core = require_module("core");
		
		if (!$core->check_device_limit($this->params->primitives['ip']['value'], $old_ip)) {
			$this->err->err_from_string("Unable to create tracker: You have exceeded the number of devices allowed in your license.");
			return FALSE;
		}
		/**
		  * at this point, we know we can consume an IP slot
		  */		
		

		$ip   = $this->db->escape($this->params->primitives['ip']['value']);
		$name = $this->db->escape($this->params->primitives['partition']['value']);

		$query = "SELECT {$this->pkey} FROM {$this->table} WHERE ip = $ip AND partition = $name";

		if ($id) {
			$query .= " AND {$this->pkey} <> $id";
		}

		$res  = $this->db->get_row($query);

		if (DB_NO_RESULT == $res) {
			return TRUE;
		} else {
			$this->err->err_from_string("This UNIX Partition is already monitored by " . SITE_NAME);
			return FALSE;
		}
	}

	function post_insert() {
		$this->setbit($this->meta['pkey_value'], 'timestamp', mktime());
		# Oops  ;)
		$this->setbit($this->meta['pkey_value'], 'pending', $this->db->escape('N'));
		return TRUE;
	}

	function pre_update($id) {
		return $this->pre_insert($id);
	}

	function pop($id) {
		$id = $this->db->escape($id);

		$query = "SELECT total::int8, available::int8, * FROM {$this->table} WHERE {$this->pkey} = $id";

		$result = $this->db->get_row($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			foreach($result as $key => $value) {
				$this->params->setval($key, $value);
			}
			return TRUE;
		}
	}

	function pre_delete($id) {
		$query = "DELETE FROM df_server_log WHERE srv_id = " . $this->db->escape($id);
		return $this->db->delete($query);
	}

	function get_disks_over_threshold() {
		$query = "select timestamp, message, ip, partition AS \"share\", threshold, 'DF' AS \"type\", status AS percent FROM df_servers WHERE ((status >= threshold) OR (status = -1))";
		$res = $this->db->select($query);

		if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
			return array();
		}
		return $res;
	}

}
?>