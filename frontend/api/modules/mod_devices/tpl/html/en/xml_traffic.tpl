<!-- {$smarty.template} ($Id$) -->
<!-- Step size: {$step} - Dataset size: {$size} -->
<graph caption="{$device_name} ({$smarty.get.ip})" subcaption="Interface {$smarty.get.interface}: {$description}" divlinecolor="C5C5C5" numberSuffix="bps" decimalPrecision="1" {if $smarty.get.showxaxis == 1}numdivlines="6"{else}numdivlines="3"{/if} showAreaBorder="1" areaBorderColor="444444" numberPrefix="" canvasBgColor="E1E1E1" canvasbordercolor="888888" canvasBorderThickness="2" showNames="{$smarty.get.showxaxis|default:0}" rotateNames="1" numVDivLines="25" vDivLineAlpha="30" showAlternateHGridColor="1" alternateHGridColor="C9C9C9" formatNumberScale="1" bgcolor="" animation="{$smarty.get.animation|default:0}" {if $smarty.get.showxaxis == 1}anchorRadius="5"{else}anchorRadius="3"{/if} showShadow="1" lineThickness="3" >
   <categories>
	{foreach from=$points item="point"}
	{counter assign="tscount"}
	  <category name="{$point.timestamp|date_format:"%H:%M:%S"}" {if $tscount % 2 == 0 && $smarty.get.showxaxis == 1}showName="1"{else}showName="0"{/if} />
	{/foreach}
   </categories>
   <dataset seriesname="Inbound Traffic" color="0066CC" showValues="0" showAreaBorder="1" areaBorderColor="0066CC" anchorsides="5" anchorBorderThickness="1" anchorBgColor="D4E5F6" >
	{foreach from=$points item="point"}
	<set value="{$point.in}" />
	{/foreach}
   </dataset>
   <dataset seriesname="Outbound Traffic" color="339999" showValues="0" showAreaBorder="1" areaBorderColor="339999" anchorsides="5" anchorBorderThickness="1" anchorBgColor="B2D1F0">
	{foreach from=$points item="point"}
	<set value="{$point.out}" />
	{/foreach}
   </dataset>
</graph>
<!-- end of {$smarty.template} -->
