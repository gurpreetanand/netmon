
<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.connection,ajax_oid,yui.utilities,yui.tabview,yui.animation"}
{import_css files="tabview"}



<script language="Javascript">
var oid_handler = new AJAX_OID({$device.id});

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}

var tabView = new YAHOO.widget.TabView('toolbar');
tabView.on('contentReady', function() {

	var updateTabChildren = function(e) {
		oid_handler.update_children(this.get('activeTab').get('contentEl').id);
	};
	
	this.on('activeTabChange', updateTabChildren);
    //this.getTab(6).set('dataSrc', '?module=mod_dashboard&action=get_oid_monitors&device_id={/literal}{$device.id}{literal}&root_tpl=blank_panel');
     
});


{/literal}
</script>


<div class="titlebar">
	<a name="top"></a>{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}





<div id="toolbar" class="yui-navset">
<div class="panel">
<ul class="yui-nav">
	<li class="selected"><a href="#general"><img src="/assets/icons/info.gif" class="icon" width="16" height="16">General</a></li>
	<li><a href="#database"><img src="/assets/icons/database.png" class="icon" width="16" height="16">SQL Server</a></li>
	<li><a href="#services"><img src="/assets/icons/services.png" class="icon" width="16" height="16">Services</a></li>
	<li><a href="#processes"><img src="/assets/icons/cpu.gif" class="icon" width="16" height="16">Processes</a></li>
	<li><a href="#storage"><img src="/assets/icons/database.png" class="icon" width="16" height="16">Storage</a></li>
	<li><a href="#custom">Custom</a></li>
</ul>
</div>


<div class="yui-content">


<div id="general">
<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>Name: {ajax_oid parent="general" class="gray2" frequency="0" oid="SNMPv2-MIB::sysName.0"} &nbsp; Domain: {ajax_oid class="gray2" frequency="0" oid="LanMgr-Mib-II-MIB::domPrimaryDomain.0"} &nbsp; Location: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysLocation.0"}</td>
	    <td rowspan="2" valign="top"><div align="right"><img src="/assets/devices/windows.gif" width="48" height="42"></div></td>
	</tr>
	<tr>
		<td>
		
	  <img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> System Uptime: {ajax_oid class="gray2" frequency="0" oid="DISMAN-EXPRESSION-MIB::sysUpTimeInstance"}&nbsp; </td>
    </tr>
	<tr>
	  <td colspan="2"> System Description: {ajax_oid parent="general" class="unresolved" frequency="0" oid="SNMPv2-MIB::sysDescr.0"}</td>
    </tr>
</table>


</fieldset>


<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td valign="top">
	

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>CPU</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td width="82" valign="top">
		  <div align="right"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&oid=INFORMANT-ADV::cpuPercentProcessorTime.%220%22&refresh=30&url={$smarty.const.TOP_DOMAIN}',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&oid=INFORMANT-ADV::cpuPercentProcessorTime.%220%22&refresh=30&url={$smarty.const.TOP_DOMAIN}'
			 ); //end AC code
			</script></div>
	  <p align="center" style="margin-bottom:0px;">CPU Utilization <a href="javascript:addOIDTracker({$device.id}, 'INFORMANT-ADV::cpuPercentProcessorTime.%220%22', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></p>
		  </div></td>
	    <td valign="top"><p>Processes: {ajax_oid parent="general" class="blue" frequency="30000" oid="HOST-RESOURCES-MIB::hrSystemProcesses.0" trackable=true}</p>
	      <p>Interrupts/Sec: {ajax_oid parent="general" class="blue" frequency="30000" oid='INFORMANT-ADV::cpuInterruptsPerSec."0"' trackable=true}</p>
		  <p>DPCs/Cycle: {ajax_oid parent="general" class="blue" frequency="30000" oid='INFORMANT-ADV::cpuDPCRate."0"' trackable=true}</p>
		  
		  </td>
	</tr>
</table>


</fieldset>

	</td>
	<td valign="top">

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>RAM</strong>&nbsp;</legend>

<table border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td width="82" valign="top">
		  <div align="right"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_windows_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_windows_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank'
			 ); //end AC code
			</script></div>
	  <p align="center" style="margin-bottom:0px;">RAM Utilization</p>
		  </div></td>
	    <td valign="top">
	      <p>Pg Faults/Sec: {ajax_oid parent="general" class="blue" frequency="30000" oid="INFORMANT-ADV::memoryPageFaultsPerSec.0" trackable=true}</p>
		  <p>Pg Reads/Sec: {ajax_oid parent="general" class="blue" frequency="30000" oid='INFORMANT-ADV::memoryPageReadsPerSec.0' trackable=true}</p>
		  <p>Pg Writes/Sec: {ajax_oid parent="general" class="blue" frequency="30000" oid='INFORMANT-ADV::memoryPageWritesPerSec.0' trackable=true}</p>
		  <p>Pgs Input/Sec: {ajax_oid parent="general" class="blue" frequency="30000" oid="INFORMANT-ADV::memoryPagesInputPerSec.0" trackable=true}</p>
	      <p>Pgs Output/Sec: {ajax_oid parent="general" class="blue" frequency="30000" oid="INFORMANT-ADV::memoryPagesOutputPerSec.0" trackable=true}</p>
		    
		  </td>
	</tr>
</table>

</fieldset>

	</td>
	</tr>
</table>
</div>
</div>



<div id="database" style="display: none;">
	<div class="panel">
		{ajax_proxy parent="database" dash="dash_windows_snmp_inf_sql" proc="ajax_get_sql_server_metrics" params="ip="|cat:$smarty.get.ip freq="120000" div="dbstats_proxy"}
	</div>
	
	{ajax_proxy parent="database" dash="dash_windows_snmp_inf_sql" proc="ajax_get_database_metrics" params="ip="|cat:$smarty.get.ip freq="120000" div="databases"}

</div>


<div id="services" style="display: none;">

<iframe src="/?module=mod_devices&action=ajax_proxy&profile=dash_windows_generic&proc=ajax_get_windows_services&ip={$smarty.get.ip}&root_tpl=blank_panel&device_id={$device.id}" id="services_iframe" name="services_iframe" width="100%" height="400" frameborder="0"></iframe>

</div>


<div id="processes" style="display: none;">

<iframe src="/?module=mod_devices&action=ajax_proxy&profile=dash_windows_generic&proc=ajax_get_windows_processes&ip={$smarty.get.ip}&root_tpl=blank_panel&device_id={$device.id}" id="processes_iframe" name="processes_iframe" width="100%" height="400" frameborder="0"></iframe>

</div>



<div id="storage" style="display: none;">
<div class="panel">
<fieldset>
<legend>&nbsp;<strong><img src="/assets/icons/drive_network.gif" width="16" height="16" class="icon"> Storage Resources</strong>&nbsp;</legend>
{ajax_proxy parent="storage" dash="dash_windows_generic" proc="ajax_get_storage_devices" params="ip="|cat:$smarty.get.ip freq="120000" div="storage_devs"}
</fieldset>
</div>
</div>


<div id="custom" style="display: none;">
{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}
</div>

</div>








<script language="Javascript">
	parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');
	oid_handler.init();
</script>


<!-- end of {$smarty.template} -->
