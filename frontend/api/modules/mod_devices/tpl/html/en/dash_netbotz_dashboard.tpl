
<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}
{literal}
<script>
var oid_handler = new AJAX_OID({/literal}{$device.id}{literal});

function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}



</script>
{/literal}

<div class="titlebar">
	{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}

<div class="panel">

<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td><img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> IP Address: <span class="gray2">{$device.ip_address}</span>
	    &nbsp;&nbsp;&nbsp;
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> Mac Address: <span class="gray2">{$device.mac|default:"<strong>Unresolved</strong>"}</span>
		
</td>
	</tr>

	
	
	

	<tr>
		<td>
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> 
	  System Description: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysDescr.0"}
</td>
	</tr>
	<tr>
	  <td><img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> System Location: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysLocation.0"}</td>
  </tr>
	
</table>


</fieldset>
</div>


<div class="panel">

	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_temp?lowthreshold=60&hithreshold=85&sensorname=Temperature&unit=F&device_id={$device.id}&oid=NETBOTZ-MIB%3A%3AnetBotz.100.4.1.1.1.9.1095346743&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_temp?lowthreshold=60&hithreshold=85&sensorname=Temperature&unit=F&device_id={$device.id}&oid=NETBOTZ-MIB%3A%3AnetBotz.100.4.1.1.1.9.1095346743&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>

	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_humidity?lowthreshold=25&hithreshold=60&sensorname=Humidity&device_id={$device.id}&oid=NETBOTZ-MIB%3A%3AnetBotz.100.4.1.2.1.8.1094232622&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_humidity?lowthreshold=25&hithreshold=60&sensorname=Humidity&device_id={$device.id}&oid=NETBOTZ-MIB%3A%3AnetBotz.100.4.1.2.1.8.1094232622&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>

</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" class="icon" /> <img src="/assets/icons/webcam.png" width="16" height="16" class="icon"> Motion Sensor 1: {ajax_oid class="gray2" frequency="0" oid="NETBOTZ-MIB::netBotz.100.4.2.3.1.4.434081418" trackable=true} &nbsp;&nbsp;&nbsp; Status: {ajax_oid class="blue2" frequency="60000" oid="NETBOTZ-MIB::netBotz.100.4.2.3.1.7.434081418" trackable=true}
</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" class="icon" /> <img src="/assets/icons/webcam.png" width="16" height="16" class="icon"> Motion Sensor 2: {ajax_oid class="gray2" frequency="0" oid="NETBOTZ-MIB::netBotz.100.4.2.3.1.4.2313127090" trackable=true} &nbsp;&nbsp;&nbsp; Status: {ajax_oid class="blue2" frequency="60000" oid="NETBOTZ-MIB::netBotz.100.4.2.3.1.7.2313127090" trackable=true}
</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" class="icon" /> <img src="/assets/icons/door_in.png" width="16" height="16" class="icon"> Door Switch 1: {ajax_oid class="gray2" frequency="0" oid="NETBOTZ-MIB::netBotz.100.4.2.2.1.4.432254604" trackable=true} &nbsp;&nbsp;&nbsp; Status: {ajax_oid class="blue2" frequency="60000" oid="NETBOTZ-MIB::netBotz.100.4.2.2.1.7.432254604" trackable=true}
</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" class="icon" /> <img src="/assets/icons/door_in.png" width="16" height="16" class="icon"> Door Switch 2: {ajax_oid class="gray2" frequency="0" oid="NETBOTZ-MIB::netBotz.100.4.2.2.1.4.2311300276" trackable=true} &nbsp;&nbsp;&nbsp; Status: {ajax_oid class="blue2" frequency="60000" oid="NETBOTZ-MIB::netBotz.100.4.2.2.1.7.2311300276" trackable=true}
</div>


<div class="panel">
	<img src="/assets/core/separator_double.gif" class="icon" /> Dry Contact Sensor 1: {ajax_oid class="gray2" frequency="0" oid="NETBOTZ-MIB::netBotz.100.4.2.1.1.4.1091100502" trackable=true} &nbsp;&nbsp;&nbsp; Status: {ajax_oid class="blue2" frequency="60000" oid="NETBOTZ-MIB::netBotz.100.4.2.1.1.7.1091100502" trackable=true}
</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" class="icon" /> Dry Contact Sensor 2: {ajax_oid class="gray2" frequency="0" oid="NETBOTZ-MIB::netBotz.100.4.2.1.1.4.1091100499" trackable=true} &nbsp;&nbsp;&nbsp; Status: {ajax_oid class="blue2" frequency="60000" oid="NETBOTZ-MIB::netBotz.100.4.2.1.1.7.1091100499" trackable=true}
</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" class="icon" /> Dry Contact Sensor 3: {ajax_oid class="gray2" frequency="0" oid="NETBOTZ-MIB::netBotz.100.4.2.1.1.4.1091100500" trackable=true} &nbsp;&nbsp;&nbsp; Status: {ajax_oid class="blue2" frequency="60000" oid="NETBOTZ-MIB::netBotz.100.4.2.1.1.7.1091100500" trackable=true}
</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" class="icon" /> Dry Contact Sensor 4: {ajax_oid class="gray2" frequency="0" oid="NETBOTZ-MIB::netBotz.100.4.2.1.1.4.1091100501" trackable=true} &nbsp;&nbsp;&nbsp; Status: {ajax_oid class="blue2" frequency="60000" oid="NETBOTZ-MIB::netBotz.100.4.2.1.1.7.1091100501" trackable=true}
</div>



{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}


<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');

oid_handler.init();


</script>



<!-- end of {$smarty.template} -->
