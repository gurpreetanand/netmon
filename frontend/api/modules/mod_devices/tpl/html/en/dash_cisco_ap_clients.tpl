
<!-- {$smarty.template} ($Id: dash_cisco_ap_clients.tpl 5001 2008-03-05 19:03:57Z doug $) -->

<div class="datagrid">
<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<th>IP</th>
		<th>Wep Status</th>
		<th>Association Status</th>
		<th>Signal Strength</th>
		<th>Signal Quality</th>
		<th>Data Received</th>
		<th>Data Sent</th>
		<th>Throughput</th>
	</tr>

{foreach from=$clients item="client"}
	<tr>
		<td>{$client.ip} </td>
		<td>{$client.WepEnabled}</td>
		<td>{$client.AssociationState}</td>
		<td>{$client.SignalStrength}</td>
		<td>{$client.SigQuality}</td>
		<td>{$client.BytesReceived}</td>
		<td>{$client.BytesSent}</td>
		<td>{$client.Throughput}</td>
	</tr>

{/foreach}
</table>
</div>

{literal}
<script>
function RefreshThisFrame(){
	window.location.replace(window.location.href);
}
setTimeout("RefreshThisFrame()", 60000);
</script>
{/literal}

<!-- end of {$smarty.template} ($Id: dash_cisco_ap_clients.tpl 5001 2008-03-05 19:03:57Z doug $) -->
