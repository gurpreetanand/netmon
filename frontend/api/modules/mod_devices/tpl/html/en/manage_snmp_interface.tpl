<!-- {$smarty.template} ($Id$) -->

{import_js files="AC_RunActiveContent"}

<script language="Javascript">
	parent.pnl_right.showEditor("?module=mod_devices&action=form_edit_snmp_interface&ip={$interface.ip_address}&interface={$interface.interface}");
</script>

<div class="titlebar">
{$interface.device_name} ({$interface.ip_address}/{$interface.interface})
 </div>
 
{include file="device_toolbar.tpl"}

<!-- Toolbar -->
<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	Interface: <span class="gray2">{$interface.interface}</span>
	<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	Speed: <span class="gray2">{$interface.speed|speed}/s</span>
    <img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	Mac Address: {if $interface.mac}<span id="mac" class="green">{$interface.mac}</span> {else} <span class="gray2"><strong>Unresolved</strong></span> {/if}
	<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	<img src="assets/buttons/button_help.gif" title="Help" alt="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('snmp_interface_explorer');">
</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	Connected IP/MAC: {if $interface.mac}<a href="#" onClick="parent.location = '?module=mod_layout&action=render_section&layout=network&ip={$interface.mac|resolve_max}&store_request=2'">{$interface.mac|resolve_max}</a>{else}<span class="gray2"><strong>Unresolved</strong></span>{/if}
	<img src="/assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	<img src="/assets/buttons/alerts.gif" width="24" height="24" border="0" class="icon" onClick="parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=snmp_iface_above_threshold&interface_id={$interface.id}')">
</div>
<div class="panel" align="center">
{if $interface.enable_logging == "t"}

<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','450',
	'height','360',
	'bgcolor','D4D0C8',
	'src','assets/charts/FC_2_3_MSLine_2?dataURL=%3Fmodule=mod_devices%26action=get_xml_interface_traffic%26root_tpl=blank%26ip={$interface.ip_address}%26interface={$interface.interface}%26animation=1%26showxaxis=1&chartWidth=450&chartHeight=360',
	'quality','high',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/charts/FC_2_3_MSLine_2?dataURL=%3Fmodule=mod_devices%26action=get_xml_interface_traffic%26root_tpl=blank%26ip={$interface.ip_address}%26interface={$interface.interface}%26animation=1%26showxaxis=1&chartWidth=450&chartHeight=360'
	 ); //end AC code
	</script>


{else}
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="450" height="360">
		  <param name="movie" value="assets/charts/FC_2_3_Column2D.swf">
		  <param name="quality" value="">
		  <param name="bgcolor" value="#D4D0C8">
		  <param name="FlashVars" value="&dataURL=%3Fmodule=mod_devices%26action=get_xml_snmp_throughput%26root_tpl=blank%26ip={$interface.ip_address}%26interface={$interface.interface}%26animation=1%26showxaxis=1&chartWidth=450&chartHeight=360">
		  <embed src="assets/charts/FC_2_3_Column2D.swf" bgcolor="#D4D0C8" FlashVars="&dataURL=%3Fmodule=mod_devices%26action=get_xml_snmp_throughput%26root_tpl=blank%26ip={$interface.ip_address}%26interface={$interface.interface}%26animation=1%26showxaxis=1&chartWidth=450&chartHeight=360" quality=high pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="450" height="360"></embed>
		</object>
{/if}
</div>

<!-- end of {$smarty.template} -->
