
<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}

{literal}
<script>
var oid_handler = new AJAX_OID({/literal}{$device.id}{literal});

function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}



</script>
{/literal}

<div class="titlebar">
	{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}


<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td><img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> IP Address: <span class="gray2">{$device.ip_address}</span>
	    &nbsp;&nbsp;&nbsp;
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> Mac Address: <span class="gray2">{$device.mac|default:"<strong>Unresolved</strong>"}</span>
		</td>
		<td><img src="/assets/devices/SensorHawk.gif" width="80" style="margin-right: 5px;"></td>
	</tr>
	<tr>
		<td colspan=2>
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> 
	  System Description: <span title="{$device.sysdescr|default:"N/A"}" class="gray2">{$device.sysdescr|default:"N/A"|truncate:70}</span>
	  </td>
	</tr>
</table>
</fieldset>

{*
{if $oids.portcount == "28"}
*}


{section name="ports_sensor" loop=$oids.portcount}

	{capture assign="sensorID"}{ldelim}$oids.sensor{$smarty.section.ports_sensor.index}_type{rdelim}{/capture}
	{capture assign="sensorType"}{eval var=$sensorID}{/capture}
	
	
	{if $sensorType == "temperature(1)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration}&nbsp; - Temperature Sensor <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeTempDegree.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong>&nbsp;</legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','225',
	'height','80',
	'src','assets/flash/sensorhawk_temperature?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_temperature%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_temperature?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_temperature%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "fourTo20mA(2)"}
	test2
	{/if}

	{if $sensorType == "humidity(3)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Temperature <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::SensorProbeTempDegree.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a> / Humidity <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::SensorProbeHumidityPercent.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a> Combo Sensor</strong>&nbsp;</legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','225',
	'height','80',
	'src','assets/flash/sensorhawk_temperature?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_temperature%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_temperature?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_temperature%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','225',
	'height','80',
	'src','assets/flash/sensorhawk_humidity?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_humidity%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_humidity?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_humidity%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "water(4)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Liquid Detection Sensor <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeSwitchStatus.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong></legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','250',
	'height','60',
	'src','assets/flash/sensorhawk_liquid?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_liquid?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "atod(5)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - DC Voltage Sensor <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeHumidityPercent.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong></legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','380',
	'height','80',
	'src','assets/flash/sensorhawk_voltdc?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_voltage_dc%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_voltdc?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_voltage_dc%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "security(6)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Security Sensor <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeSwitchStatus.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong></legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','250',
	'height','60',
	'src','assets/flash/sensorhawk_security?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_security?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "airflow(8)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Airflow Sensor <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::SensorProbeHumidityPercent.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong>&nbsp;</legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','275',
	'height','80',
	'src','assets/flash/sensorhawk_airflow?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_airflow%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_airflow?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_airflow%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "siren(9)"}
	{/if}

	{if $sensorType == "dryContact(10)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Dry Contact Sensor<a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeSwitchStatus.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong></legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','250',
	'height','60',
	'src','assets/flash/sensorhawk_drycontact?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_drycontact?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "voltage(12)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - AC Voltage Sensor<a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::SensorProbeHumidityPercent.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong></legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','275',
	'height','80',
	'src','assets/flash/sensorhawk_voltac?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_voltage%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_voltac?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_voltage%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "relay(13)"}
	{/if}

	{if $sensorType == "motion(14)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration}&nbsp; - Motion Sensor<a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeSwitchStatus.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong></legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','275',
	'height','80',
	'src','assets/flash/sensorhawk_motion?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_motion?command=module=mod_devices%26action=ajax_proxy%26profile=dash_sensorhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

{/section}

{*
{/if}
*}

</div>
{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}


<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');
oid_handler.init();
</script>


<!-- end of {$smarty.template} -->
