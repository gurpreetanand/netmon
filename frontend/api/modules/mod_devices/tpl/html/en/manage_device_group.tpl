
{import_js files="yui.yahoo,yui.event,yui.utilities,yui.connection,yui.animation,yui.dom"}

<script type="text/javascript">
<!--
	var gid = {$smarty.get.gid};
	var rootNode = parent.pnl_left.iframe_snmp_explorer.tree.getRootNode();
	//var groupNode = rootNode.findChild('id', gid);
	var groupNode = parent.pnl_left.iframe_snmp_explorer.getTreeNodeById(gid);
	console.log(groupNode);

{literal}
	confirm_delete = function() {
		document.getElementById('deleteButtonDiv').style.display = 'none';
		document.getElementById('confirm_delete_dialog').style.display = 'block';
		
	}
	
	disable_delete_buttons = function() {
		document.getElementById('deleteWithDevices').disabled = true;
		document.getElementById('deleteWithoutDevices').disabled = true;
	}
	
	deleteWithDevices = function() {
		document.location='/?module=mod_devices&action=delete_device_group&delete_devices=1&gid='+{/literal}{$group.id}{literal};
	}
	
	deleteWithoutDevices = function() {
		document.location='/?module=mod_devices&action=delete_device_group&gid='+{/literal}{$group.id}{literal};
	}
	
	update_group_name = function() {
		showProgressIndicator('updateProgress');
		document.getElementById('renameButton').disabled = true;
		query = "?module=mod_devices&action=ajax_rename_device_group&root_tpl=blank&gid={/literal}{$group.id}{literal}";
		payload="name="+encodeURIComponent(document.getElementById('newName').value);
		
		callback = {success:success_handler,failure:failure_handler}
		YAHOO.util.Connect.asyncRequest('POST', query, callback, payload);
	}
	
	success_handler = function(o) {
		hideProgressIndicator('updateProgress');
		res = o.responseXML;
			
		if (res) {
			status = res.documentElement.getElementsByTagName('status')[0].getAttribute('value');
			if ('OK' == status) {
				groupNode.setText(document.getElementById('newName').value);
				document.getElementById('dialog').innerHTML = '<fieldset><legend><strong>Group Renamed</strong></legend>Device Group Renamed Successfully </fieldset>';
			return true;
			}
		}
		return failure_handler(o);		
	}
	
	failure_handler = function(o) {
		hideProgressIndicator('updateProgress');
		if (o.responseXML) {
			err = o.responseXML.documentElement.getElementsByTagName('error')[0].getAttribute('value');
		} else {
			err = "No response from server";
		}
		
		alert("Unable to create new device group: "+err); 
	}
{/literal}	
//-->
</script>


<div class="titlebar">Group Management</div>

<div id="dialog">
<fieldset>
	<legend><strong>Rename this Group</strong></legend>
	If you would like to assign a new label to this group, please enter it in the text field below and click on the 'Update' button.
	<br /><br />
	<div align="center">Group Name: {input id="newName" type="text" name="group_name" value=$group.name} <button id="renameButton" onClick="update_group_name();">Update</button><img class="progress" src="/assets/progress.gif" id="updateProgress" /></div>
</fieldset>

<fieldset>
	<legend><strong>Delete this Group</strong></legend>
	If you would like to delete this group, click on the 'Delete' button below. You will be prompted for confirmation, and asked whether you would like all devices in this group deleted as well.
	<br /><br />
	<div align="center" id="deleteButtonDiv"><button id="deleteButton" onClick="confirm_delete();">Delete Group</button></div>
	<div id="confirm_delete_dialog" style="display: none;">
	<br /><br />
	<strong>Would you like to delete all devices that belong to this group?</strong><br /><br />
	If you do not want the devices in this group to be deleted, they will be automatically placed in the root group once this group has been deleted.
	<br /><br />
	{literal}
	<div align="center">
	<button id="deleteWithDevices" onClick="if (confirm('Are you sure you want to delete the group and all its devices?')) { disable_delete_buttons(); deleteWithDevices(); }"><strong>YES</strong>, Delete Devices</button>
	<button id="deleteWithoutDevices" onClick="if (confirm('Are you sure you wish to delete this group?')) { disable_delete_buttons(); deleteWithoutDevices(); }"><strong>NO</strong>, Delete Group Only</button>
	</div> 
	{/literal}
	</div>
</fieldset>
</div>