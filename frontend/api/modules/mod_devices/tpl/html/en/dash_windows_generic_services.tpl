
<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("servs").className = 'sort-table';
	var types = ['CaseInsensitiveString', 'CaseInsensitiveString'];
	var report = new SortableTable(document.getElementById("servs"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="datagrid">
<table width="100%" border="0" cellpadding="3" cellspacing="0" id="servs">
	<thead>
	<tr>
		<td>Service Name</td>
		<td>Operating State</td>
	</tr>
	</thead>

{foreach from=$services item="service"}
	<tr>
		<td>{$service.Name} [<a href="#">Info</a> | <a href="http://www.google.ca/search?hl=en&q=windows+service+{$service.Name|escape:"url"}&btnG=Search" target="_blank">Google Search</a>]</td>
		<td align="center">{if $service.OperatingState == "active"}<span class="green">{else}<span class="red">{/if}{$service.OperatingState}</span><a href="javascript:parent.addOIDTracker({$smarty.get.device_id}, 'LanMgr-Mib-II-MIB::svSvcOperatingState.{$service.Name|escape:"url"}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></td>
	</tr>
{/foreach}
</table>
</div>

{literal}
<script>
function RefreshThisFrame(){
	window.location.replace(window.location.href);
}
setTimeout("RefreshThisFrame()", 60000);
</script>
{/literal}

<!-- end of {$smarty.template} ($Id$) -->