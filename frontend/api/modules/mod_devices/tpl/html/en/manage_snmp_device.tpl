<!-- {$smarty.template} ($Id$) -->
{import_js files="jquery-1.4.2.min,ajax_oid"}
<script language="Javascript">
var oid_handler = new AJAX_OID({$device.id});

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}

interface_status_callback = function(val) {
	if ((val == '1') || (val == "up(1)")) {
		return '<span class="green"><strong>UP</strong></span>';
	} else {
		return '<span class="red"><strong>DOWN</strong></span>';
	}
}

var interface_manager = function(config) {

	this.config = config;
	this.device_id = {$device.id};
	this.interfaces = {};


	$.ajax({url: '/?module=mod_devices&action=json_rfc1213_ifaces&device_id='+this.device_id+'&root_tpl=blank',
			timeout: 60,
			context: this,
			success: function(data, status, request) {
				
			}
		});
}

{/literal}
</script>


{literal}
<style>
DIV.interfacebox {
	width: 32px;
	height: 32px;
	padding-top: 11px;
	vertical-align: middle;
	color: #000000;
	text-align: center;
	background-image: url('/assets/icons/interface_lg.gif');
	background-position: center center;
	background-repeat: no-repeat;
	cursor: hand;
	cursor: pointer;
}

SPAN.gray3 {
	font-size: 11px;
	padding: 2px;
	background-color: #AAAAAA;
	color: #EEEEEE;
	vertical-align: absmiddle;
	margin-right: 2px;
	padding-left: 4px;
}

SPAN.blue2 {
	font-size: 11px;
	padding: 2px;
	background-color: #0066FF;
	color: #FFFFFF;
	vertical-align: absmiddle;
	margin-right: 2px;
	padding-left: 4px;
}

SPAN.green2 {
	font-size: 11px;
	padding: 2px;
	background-color: #339999;
	color: #FFFFFF;
	vertical-align: absmiddle;
	margin-right: 2px;
	padding-left: 4px;
}

</style>
{/literal}

<div class="titlebar">SNMP Device: {$device.label} ({$device.ip_address})</div>

{include file="device_toolbar.tpl"}

{if $device}
	
	{if not $smarty.get.norefresh}
	<script language="Javascript">
		parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');
	</script>
	{/if}
	<!-- Toolbar -->


<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td><img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> IP Address: <span class="gray2">{$device.ip_address}</span>
	    &nbsp;&nbsp;&nbsp;
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> Mac Address: <span class="gray2">{$device.mac|default:"<strong>Unresolved</strong>"}</span>
		
</td>
	</tr>

	
	
	

	<tr>
		<td>
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> 
	  System Description: <span title="{$device.sysdescr|default:"N/A"}" style="color: #666666">{$device.sysdescr|default:"N/A"}</span>
</td>
	</tr>
	
</table>


</fieldset>

	

	
		{capture assign="dev_id"}{$device.id}{/capture}
		<!--{input type="button" class="button" value="Delete Device" onClick="parent.pnl_right.showEditor('?module=mod_devices&action=delete_snmp_device&id=$dev_id');"}-->

	</div>



	{if $interfaces}

	{foreach from=$interfaces item="interface"}
		<div class="panel">
		<table width="100%" cellpadding="1" cellspacing="0">
			<tr>
				<td width="56" rowspan="2" align="left">
					<div class="interfacebox" style="margin-left: 5px;" onClick="location.href='?module=mod_devices&action=manage_snmp_interface&ip={$device.ip_address}&interface={$interface.interface}'">{$interface.interface}</div>
					
				</td>
				<td width="100">
					{if $interface.last_inbound_throughput != 0}
					<span class="green2">&nbsp;In: &nbsp;</span> {$interface.last_inbound_throughput|speed}ps
					{else}
					<span class="gray3">&nbsp;In: &nbsp;</span> <span class="unresolved">N/A</span>
					{/if}
										
				</td>
				<td>
					<div align="left">
					Label: <span class="gray2">{$interface.description}</span> &nbsp;Name: <span class="gray2">{$interface.name}</span><br />
					</div>
				</td>

			</tr>
			<tr>
			  <td>
			  {if $interface.last_outbound_throughput != 0}
			  <span class="blue2">Out:</span> {$interface.last_outbound_throughput|speed}ps
			  {else}
			  <span class="gray3">Out:</span> <span class="unresolved">N/A</span>
			  {/if}
			  </td>
		      <td>Status: {ajax_oid callback="interface_status_callback" class="grey" frequency="15000" oid="IF-MIB::ifOperStatus."|cat:$interface.interface}<a href="javascript:addOIDTracker({$device.id}, 'IF-MIB::ifOperStatus.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a>  Connected IP/MAC: {if $interface.mac|resolve_max != ""} <a href="#" onClick="parent.location = '?module=mod_layout&action=render_section&layout=network&ip={$interface.mac|resolve_mac}&store_request=2'">{$interface.mac|resolve_max}{else}<span class="unresolved">Unresolved</span>{/if}</a></td>
		  </tr>
		</table>

		</div>
	{/foreach}

	{else}

	{"No interface available for this SNMP device at the moment"|message_bar}

	{/if}


{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}

<script language="Javascript">
	//parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');
	oid_handler.init();
</script>

{else}
{"The system was not able to retrieve any SNMP data for the specified device. Please try again later."|message_bar}
{/if}


<!-- end of {$smarty.template} -->
