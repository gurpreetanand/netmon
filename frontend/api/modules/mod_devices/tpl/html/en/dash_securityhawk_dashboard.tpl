
<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}

{literal}
<script>
var oid_handler = new AJAX_OID({/literal}{$device.id}{literal});

function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}



</script>
{/literal}

<div class="titlebar">
	{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}



<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>System Name: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysName.0"}  &nbsp; Location: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysLocation.0"}</td>
	    <td rowspan="2" valign="top"><div align="right"><img src="/assets/devices/SensorHawk.gif" width="80" style="margin-right: 5px;"></div></td>
	</tr>
	<tr>
		<td>
		
	      <img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> System Uptime: {ajax_oid class="gray2" frequency="0" oid="DISMAN-EXPRESSION-MIB::sysUpTimeInstance"}&nbsp;
      </td>
    </tr>
	<tr>
	  <td colspan="2"> System Description: {ajax_oid class="unresolved" frequency="0" oid="SNMPv2-MIB::sysDescr.0"}</td>
    </tr>
	<tr>
	  <td colspan="2"><img src="/assets/icons/chart_bar.gif" width="16" height="16" class="icon"> Load Average 1 min: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::laLoad.1" trackable=true} 5 min: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::laLoad.2" trackable=true} 15 min: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::laLoad.3" trackable=true}</td>
    </tr>
</table>


</fieldset>


<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td width="50%" valign="top">
	

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>CPU</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td width="82">
		  <div align="right"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&oid=UCD-SNMP-MIB::ssCpuIdle.0&refresh=30&url={$smarty.const.TOP_DOMAIN}&mod=inverse',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&oid=UCD-SNMP-MIB::ssCpuIdle.0&refresh=30&url={$smarty.const.TOP_DOMAIN}&mod=inverse'
			 ); //end AC code
			</script></div>
	  <p align="center" style="margin-bottom:0px;">CPU Utilization</p>
		  </div></td>
	    <td valign="top"><p>Processes: {ajax_oid class="blue" frequency="30000" oid="HOST-RESOURCES-MIB::hrSystemProcesses.0" trackable=true}</p>
	        <p>Idle: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::ssCpuIdle.0" trackable=true text="%"}</p>
	        <p>System: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::ssCpuSystem.0" trackable=true text="%"}</p>
	        <p>User: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::ssCpuUser.0" trackable=true text="%"} </p>
	      
	      </td>
	</tr>
</table>


</fieldset>

	</td>
	<td width="50%" valign="top">

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>RAM</strong>&nbsp;</legend>

<table border="0" cellpadding="3" cellspacing="0">
  <tr>
    <td width="82" valign="middle"><div align="right">
        <div align="right"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_windows_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_windows_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank'
			 ); //end AC code
			</script></div>
        <p align="center" style="margin-bottom:0px;">RAM Utilization </p>
    </div></td>
    <td valign="middle"><p>{ajax_oid class="blue" frequency="15000" oid="UCD-SNMP-MIB::memAvailReal.0" trackable=true} Kbytes available of {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::memTotalReal.0" trackable=true} total Kbytes</p></td>
  </tr>
</table>


</fieldset>

	
	</td>
	</tr>
</table>


<fieldset>
<legend>&nbsp;<strong><img src="/assets/icons/drive_network.gif" width="16" height="16" class="icon"> Resources</strong>&nbsp;</legend>

{ajax_proxy dash="dash_windows_generic" proc="ajax_get_storage_devices" params="ip="|cat:$smarty.get.ip freq="120000" div="storage_devs"}

</fieldset>


{*
{if $oids.portcount == "28"}
*}


{section name="ports_sensor" loop=$oids.portcount}

	{capture assign="sensorID"}{ldelim}$oids.sensor{$smarty.section.ports_sensor.index}_type{rdelim}{/capture}
	{capture assign="sensorType"}{eval var=$sensorID}{/capture}
	
	
	{if $sensorType == "temperature(1)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration}&nbsp; - Temperature Sensor<a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeTempDegree.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong>&nbsp;</legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','225',
	'height','80',
	'src','assets/flash/sensorhawk_temperature?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_temperature%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_temperature?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_temperature%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "fourTo20mA(2)"}
	test2
	{/if}

	{if $sensorType == "humidity(3)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Temperature <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeTempDegree.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a> / Humidity <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeHumidityPercent.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a> Combo Sensor</strong>&nbsp;</legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','225',
	'height','80',
	'src','assets/flash/sensorhawk_temperature?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_temperature%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_temperature?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_temperature%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','225',
	'height','80',
	'src','assets/flash/sensorhawk_humidity?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_humidity%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_humidity?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_humidity%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "water(4)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Liquid Detection Sensor&nbsp;</strong></legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','250',
	'height','60',
	'src','assets/flash/sensorhawk_liquid?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_liquid?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "atod(5)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - DC Voltage Sensor&nbsp;</strong></legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','380',
	'height','80',
	'src','assets/flash/sensorhawk_voltdc?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_voltage_dc%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_voltdc?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_voltage_dc%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "security(6)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Security Sensor&nbsp;</strong></legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','250',
	'height','60',
	'src','assets/flash/sensorhawk_security?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_security?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "airflow(8)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Airflow Sensor <a href="javascript:addOIDTracker({$device.id}, 'SPAGENT-MIB::sensorProbeHumidityPercent.{$smarty.section.ports_sensor.index}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></strong>&nbsp;</legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','275',
	'height','80',
	'src','assets/flash/sensorhawk_airflow?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_airflow%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_airflow?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_airflow%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "siren(9)"}
	{/if}

	{if $sensorType == "dryContact(10)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - Dry Contact Sensor</strong>&nbsp;</legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','250',
	'height','60',
	'src','assets/flash/sensorhawk_drycontact?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_drycontact?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "voltage(12)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration} &nbsp; - AC Voltage Sensor</strong>&nbsp;</legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','275',
	'height','80',
	'src','assets/flash/sensorhawk_voltac?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_voltage%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_voltac?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_voltage%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

	{if $sensorType == "relay(13)"}
	{/if}

	{if $sensorType == "motion(14)"}
	<fieldset>
	<legend>&nbsp;<img src="/assets/icons/interface.gif" width="16" height="16" class="icon"> <strong>Port {$smarty.section.ports_sensor.iteration}&nbsp; - Motion Sensor</strong>&nbsp;</legend>
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','275',
	'height','80',
	'src','assets/flash/sensorhawk_motion?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/sensorhawk_motion?command=module=mod_devices%26action=ajax_proxy%26profile=dash_securityhawk%26proc=populate_motion%26ip={$device.ip_address}%26root_tpl=blank_panel%26device_index={$smarty.section.ports_sensor.index}%26root_tpl=blank&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
	</fieldset>
	{/if}

{/section}

{*
{/if}
*}

</div>
{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}


<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');
oid_handler.init();
</script>


<!-- end of {$smarty.template} -->
