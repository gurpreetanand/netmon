
<!-- {$smarty.template} ($Id$) -->


<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<th>&nbsp;</th>
		<th><div align="left">Group Name</div></th>
		<th>Active Users</th>
		<th>Inbound</th>
		<th>Outbound</th>
	</tr>
{foreach from=$vpn_sessions item="vpn_session"}
	<tr>
		<td><img src="/assets/icons/groups.png" width="16" height="16" class="icon"></td>
		<td align="left"><span class="gray2">{$vpn_session.index}</span></td>
		<td align="center"><span class="blue2">{$vpn_session.NumUsers} users</span></td>
		<td align="center">{$vpn_session.pInPkts}</td>
		<td align="center">{$vpn_session.pOutPkts}</td>
	</tr>
{/foreach}
</table>

<!-- end of {$smarty.template} ($Id$) -->