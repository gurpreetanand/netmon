
<!-- {$smarty.template} ($Id$) -->

{literal}
<script language="Javascript">
	function checkbox_update(checkbox, fieldname) {
		field = document.getElementById(fieldname);
		if (checkbox.checked == true) {
			field.value = "true";
		} else {
			field.value = "false";
		}
	}
</script>
{/literal}


<div class="panel">
<form action="?module=mod_devices&action=process_create_oid_watcher" method="POST" name="oid_form">
<input type="hidden" name="device_id" value="{$smarty.get.device_id}" />
<input type="hidden" name="oid" value="{$smarty.get.oid|escape:html}" />
<input type="hidden" name="datatype" value="{$smarty.get.type|escape:html}" />

<input type="hidden" name="enable_logging" id="enable_logging" value="{if ($smarty.post.enable_logging == "true")}true{else}false{/if}" />
<input type="hidden" name="homedisplay" id="homedisplay" value="{if ($smarty.post.homedisplay == "true")}true{else}false{/if}" />
{capture assign="oid"}{$smarty.get.oid_txt|regex_replace:"/(.*)::(.*)\.(.*)/":"\$2"}{/capture}
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<th colspan="2" align="center">New OID Tracker<br />{$oid|wordwrap:35:"<br />":True}</th>
		</tr>
		<tr>
			<td>Label</td>
			<td><input type="text" {param name="label" class="input"} value="{$smarty.post.label|default:$oid|escape:html}" /></td>
		</tr>
		<tr>
			<td>Sample Every</td>
			<td><input size="4" type="text" {param name="interval" class="input"} value="{$smarty.post.interval|default:3600}" /> seconds</td>
		</tr>
		<tr>
			<td>Enable Logging</td>
			<td><input type="checkbox" name="chk_logging" value="true" {if $smarty.post.enable_logging == "true"}checked{/if} onchange="checkbox_update(this, 'enable_logging');" /></td>
		</tr>
		<tr>
			<td>Display on Home Dashboard</td>
			<td><input type="checkbox" name="chk_homedisplay" value="true" {if $smarty.post.homedisplay == "true"}checked{/if}  onchange="checkbox_update(this, 'homedisplay');" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">{input class="button" type="submit" value="Add Tracker"}</td>
		</tr>
	</table>

</form>
</div>

<!-- end of {$smarty.template} -->
