<!-- {$smarty.template} ($Id$) -->

<div class="noPrint">
<div class="titlebar">
	SNMP Traps - {$device.label} {if $traps}({$device.ip_address} - {array_size array=$traps} Traps){/if}
</div>


<div class="panel">
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	
	<img src="/assets/buttons/button_resource_centre.gif" width="24" height="24" class="icon" title="Device Home Dashboard" alt="Device Home Dashboard" onClick="location.href='?module=mod_devices&action=dashboard&profile=dash_ups_apc&ip={$smarty.get.ip}'"> 
	
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	
	<img src="/assets/buttons/notes.gif" width="24" height="24" class="icon" title="Device Notes" alt="Device Notes" onClick="location.href='?module=mod_devices&action=manage_device_notes&id={$device.id}'">
		
	<img src="/assets/buttons/interface.gif" width="24" height="24" class="icon" title="Network Activity" alt="Network Activity" onClick="location.href='?module=mod_devices&action=dashboard&profile=snmp&ip={$smarty.get.ip}'">
	
	<img src="/assets/buttons/log.gif" width="24" height="24" class="icon" title="Events and Logs" alt="Events and Logs" onClick="location.href='?module=mod_syslog&action=view_syslog_data&ip={$smarty.get.ip}'">
	
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	
	<img src="/assets/buttons/snmp_walk.gif" width="24" height="24" class="icon" title="Browse SNMP MIBs" alt="Browse SNMP MIBs" onClick="showProgressIndicator('progress'); location.href='?module=mod_devices&action=snmp_walk&domain=iso&id={$device.id}&class=white'">
	
	<img src="/assets/buttons/enterprise_mibs.gif" width="24" height="24" class="icon" title="Browse Enterprise MIBs" alt="Browse Enterprise MIBs" onClick="showProgressIndicator('progress'); location.href='?module=mod_devices&action=snmp_walk&domain=enterprises&id={$device.id}&class=white'">
	
	<img src="/assets/buttons/oid_trackers.gif" width="24" height="24" class="icon" title="SNMP Object (OID) Trackers" alt="SNMP Object (OID) Trackers" onClick="location.href='?module=mod_devices&action=manage_oid_watchers&id={$device.id}'">
	<img src="/assets/buttons/snmp_traps.gif" width="24" height="24" class="icon" title="SNMP Trap Messages" alt="SNMP Trap Messages" onClick="location.href='?module=mod_devices&action=view_traps&ip={$smarty.get.ip}'">
	
	
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon"> 

	<img src="/assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Help" alt="Help" onClick="parent.pnl_right.showHelp('snmp_traps');">	
	
	<img class="progress" id="progress" src="/assets/progress.gif" />
	
</div>


</div>
{if $traps}

	{foreach from=$traps item="trap"}
	{capture assign="oid"}{$trap.snmpoid|translate_oid}{/capture}
	<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	Trap OID: <span class="gray2"><strong>{$oid}</strong></span>
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">


	{capture assign="ip"}{$trap.ip}{/capture}

	{if $trap.store == "t"}

		Logging: <span class="green"><strong>Active</strong></span>
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">

		{input value="Disable Logging" type="button" class="button" onClick="parent.pnl_right.showEditor('?module=mod_devices&action=store_trap_oid&val=f&oid=`$trap.snmpoid`&ip=$ip');"}
	{else}

		Logging: <span class="gray2"><strong>Off</strong></span>
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">

		{input value="Enable Logging" type="button" class="button" onClick="parent.pnl_right.showEditor('?module=mod_devices&action=store_trap_oid&val=t&oid=`$trap.snmpoid`&ip=$ip');"}
	{/if}

	{if $trap.store == 't'}
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<a href="javascript:parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=snmp_trap&trap_id={$trap.id}');"><img src="assets/buttons/alerts.gif" border="0" class="icon" width="24" height="24"></a>
	{/if}

</div>


		{if $trap.logs}
		<div class="datagrid center">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<th>Time</th>
				<th>OID</th>
				<th>Payload</th>
			</tr>
			{foreach from=$trap.logs item="log"}
			<tr>
				<td>{$log.timestamp|date_format:"%b %e, %Y %H:%M:%S"}</td>
				<td>{$log.trapoid|translate_oid}</td>
				<td>{$log.value|wordwrap:38:"<br />":true}</td>
			</tr>
			{/foreach}
		</table>
		</div>
		{/if}
		</td>
		<td valign="top"><strong>

		</strong></td>
	</tr>
	{/foreach}

{else}
{"This device has not sent any SNMP Trap messages to your Netmon system."|message_bar}
{/if}

<!-- end of {$smarty.template} -->
