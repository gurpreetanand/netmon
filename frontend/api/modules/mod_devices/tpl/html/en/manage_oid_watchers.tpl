
<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("oids").className = 'sort-table';
	var types = [null, 'CaseInsensitiveString', 'CaseInsensitiveString', 'CaseInsensitiveString', 'netmon_date', null];
	var report = new SortableTable(document.getElementById("oids"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="titlebar">Manage SNMP Object (OID) Trackers</div>
{include file="device_toolbar.tpl"}
{if $oids}
<div class="datagrid center">
<table id="oids" width="100%" align="center" border="0" cellpadding="2" cellspacing="0">
	<thead>
	<tr>
		<td>&nbsp;</td>
		<td>OID/Label</td>
		<td>Datatype</td>
		<td>Value</td>
		<td>Last Checked</td>
		<td>Actions</td>
	</tr>
	</thead>
	{foreach from=$oids item="oid"}
	<tr>
		<td><a href="javascript:parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_oid_watcher&id={$oid.id}');"><img src="/assets/icons/oid.gif" class="icon" width="16" height="16" border="0"></a></td>
		<td><div title="{$oid.oid}">{$oid.label|default:$oid.oid}</div></td>
		<td>{$oid.datatype|default:"Unknown"}</td>
		<td><strong>{$oid.message}</strong></td>
		<td>{if $oid.timestamp}<span class="unresolved">{$oid.timestamp|date_format:"%b %e, %Y %H:%M:%S"}</span>{/if}</td>
		<td><a href="javascript:parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=snmp_oid_match&reference_pkey_val={$oid.id}');">Alerts</a> |
		<a href="javascript:parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_oid_watcher&id={$oid.id}');">Edit</a> |
		<a href="javascript:if (confirm('Are you sure you wish to delete the OID Tracker titled {$oid.label|escape:hex}?')) {ldelim} parent.pnl_right.showEditor('?module=mod_devices&action=process_delete_oid_watcher&id={$oid.id}'); {rdelim}">Del</a></td>
	</tr>
	{/foreach}
</table>
</div>
{else}
{"There are no OID Trackers associated to this device at the moment"|message_bar}
{/if}

<!-- end of {$smarty.template} -->
