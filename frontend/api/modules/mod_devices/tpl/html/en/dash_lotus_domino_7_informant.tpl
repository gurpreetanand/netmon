
<!-- {$smarty.template} ($Id$) -->
{*import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"*}
{import_js files="YAHOO,event,dom,utilities,connection,animation,ajax_oid"}

<script language="Javascript">
var oid_handler = new AJAX_OID({$device.id});

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}

	makeHumanReadable = function(val) {
	  if (val >= 1073741824) { return Math.round(val/1073741824) + " Gb"; }
 	  else if (val >= 1048576) { return Math.round(val/1048576) + " Mb";}
	  else if (val >= 1024) { return Math.round(val/1024) + " Kb"; }
	  return val + " b";
}
{/literal}
</script>



<div class="titlebar">
	<a name="top"></a>{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}

<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>Host Name: {ajax_oid class="gray2" frequency="0" oid="NOTES-MIB::lnDominoCfgHostName.0"} &nbsp; Domain: {ajax_oid class="gray2" frequency="0" oid="NOTES-MIB::lnMailDomain.0"} &nbsp; </td>
	    <td rowspan="2" valign="top"><div align="right"><img src="/assets/devices/LN.jpg" width="48" height="42"></div></td>
	</tr>
	<tr>
		<td>
		
	  OS Version: {ajax_oid class="gray2" frequency="0" oid="NOTES-MIB::lnServerSystemVersion.0"} </td>
    </tr>

</table>


</fieldset>


<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td valign="top">
	

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>CPU</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td width="82">
		  <div align="right"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&oid=INFORMANT-STD::cpuPercentProcessorTime.%220%22&refresh=30&url={$smarty.const.TOP_DOMAIN}',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&oid=INFORMANT-STD::cpuPercentProcessorTime.%220%22&refresh=30&url={$smarty.const.TOP_DOMAIN}'
			 ); //end AC code
			</script></div>
	  <p align="center" style="margin-bottom:0px;">CPU Utilization<a href="javascript:addOIDTracker({$device.id}, 'INFORMANT-STD::cpuPercentProcessorTime.%220%22', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></p>
		  </div></td>
	    <td valign="top"><p>Processes: {ajax_oid class="blue" frequency="30000" oid="HOST-RESOURCES-MIB::hrSystemProcesses.0" trackable=true}</p>
	      <p>Threads: {ajax_oid class="blue" frequency="30000" oid="INFORMANT-STD::objectsThreads.0" trackable=true}</p>
	      <p>Interrupts/Sec: {ajax_oid class="blue" frequency="30000" oid='INFORMANT-STD::cpuInterruptsPerSec."0"' trackable=true}</p>
		  <p>DPCs/Cycle: {ajax_oid class="blue" frequency="30000" oid='INFORMANT-STD::cpuDPCRate."0"' trackable=true}</p>
		  
		  </td>
	</tr>
</table>


</fieldset>

	</td>
	<td valign="top">

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>RAM</strong>&nbsp;</legend>

<table border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td width="82">
		  <div align="right"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_windows_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_windows_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank'
			 ); //end AC code
			</script></div>
	  <p align="center" style="margin-bottom:0px;">RAM Utilization</p>
		  </div></td>
	    <td valign="top"><p>Pages/Sec: {ajax_oid class="blue" frequency="30000" oid="INFORMANT-STD::memoryPagesPerSec.0" trackable=true}</p>
	      <p>Pg Faults/Sec: {ajax_oid class="blue" frequency="30000" oid="INFORMANT-STD::memoryPageFaultsPerSec.0" trackable=true}</p>
		  <p>Pgs Input/Sec: {ajax_oid class="blue" frequency="30000" oid="INFORMANT-STD::memoryPagesInputPerSec.0" trackable=true}</p>
	      <p>Pgs Output/Sec: {ajax_oid class="blue" frequency="30000" oid="INFORMANT-STD::memoryPagesOutputPerSec.0" trackable=true}</p>
		  
		  
		  </td>
	</tr>
</table>


</fieldset>

	
	</td>
	</tr>
</table>



<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td valign="top">
	

<fieldset>
<legend>&nbsp;<strong><img src="/assets/icons/email.png" width="16" height="16" class="icon"> Domino Server</strong>&nbsp;</legend>
<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td valign="top">
		<p><strong><img src="/assets/icons/device.png" width="16" height="16" class="icon"></strong> Availability Index: {ajax_oid class="blue" frequency="30000" oid="NOTES-MIB::lnServerAvailabilityIndex.0" trackable=true}</p>
		<p><strong><img src="/assets/icons/email_go.png" width="16" height="16" class="icon"></strong> Delivered Msgs: {ajax_oid class="blue" frequency="30000" oid="NOTES-MIB::lnMailDeliveries.0" trackable=true}</p>
<p><strong><img src="/assets/icons/email_error.png" width="16" height="16" class="icon"></strong> Dead Mail: {ajax_oid class="blue" frequency="30000" oid="NOTES-MIB::lnDeadMail.0" trackable=true}</p>
		
		<p><strong><img src="/assets/icons/email_go.png" width="16" height="16" class="icon"></strong> Messages in Queue: {ajax_oid class="blue" frequency="30000" oid="NOTES-MIB::lnWaitingMail.0" trackable=true}</p>	
		<p><strong><img src="/assets/icons/email_go.png" width="16" height="16" class="icon"></strong> Failed Replications: {ajax_oid class="blue" frequency="30000" oid="NOTES-MIB::lnRepFailed.0" trackable=true}</p>	
		<p><strong><img src="/assets/icons/email_go.png" width="16" height="16" class="icon"></strong> Successful Replications: {ajax_oid class="blue" frequency="30000" oid="NOTES-MIB::lnRepSuccessful.0" trackable=true}</p>	
</td><td>
		<p> <img src="/assets/icons/email.gif" width="16" height="16" class="icon"> Total Mail Routed: {ajax_oid class="blue" frequency="30001" oid="NOTES-MIB::lnTotalRoutedMail.0" trackable=true}</p>
		<p><strong><img src="/assets/icons/groups.png" width="16" height="16" class="icon"></strong> Peak Users (last 5 mins): {ajax_oid class="blue" frequency="30000" oid="NOTES-MIB::lnServerUsers5MinPeak.0" trackable=true}</p>
		<p><strong><img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"></strong> Avg. Delivery Time: {ajax_oid class="blue" frequency="30000" oid="NOTES-MIB::lnAverageMailDeliverTime.0" trackable=true}</p>
		<p><strong><img src="/assets/icons/lightning.png" width="16" height="16" class="icon"></strong> Peak Transfer Rate: {ajax_oid class="blue" callback="makeHumanReadable" frequency="30000" oid="NOTES-MIB::lnMailPeakByteTransferRate.0" trackable=true}</p>
		<p><strong><img src="/assets/icons/email.png" width="16" height="16" class="icon"></strong> Peak Transfer Rate (Msgs): {ajax_oid class="blue" frequency="30000" oid="NOTES-MIB::lnMailPeakMessagesTransferred.0" trackable=true}</p>
	    </tr>
</table>


</fieldset>





<fieldset>
<legend>&nbsp;<strong><img src="/assets/icons/drive_network.gif" width="16" height="16" class="icon"> Resources</strong>&nbsp;</legend>

{ajax_proxy dash="dash_windows_generic" proc="ajax_get_storage_devices" params="ip="|cat:$smarty.get.ip freq="120000" div="storage_devs"}
</fieldset>


</div>




<div class="titlebar">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td><a name="services"></a>Services Summary</td>
		<td align="right"><img src="assets/icons/back_to_top.gif" class="button" width="14" height="10" onClick="location.href='#top';" title="Back to Top"></td>
	</tr>
</table>
</div>

<div class="panel" id="service_list">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<a style="color: #0066CC; text-decoration: underline;" onClick="document.getElementById('service_list').style.display='none';document.getElementById('service_list_iframe').style.display='block';services.location.href='/?module=mod_devices&action=ajax_proxy&profile=dash_windows_generic&proc=ajax_get_windows_services&ip={$smarty.get.ip}&root_tpl=blank_panel&device_id={$device.id}';">Click here to see a list of Windows Services.</a>
</div>

<div id="service_list_iframe" style="display:none;">
<iframe height="150px" id="services" name="services" width="100%" frameborder="0"></iframe>
</div>

<div class="titlebar">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td><a name="process"></a>Process Summary</td>
		<td align="right"><img src="assets/icons/back_to_top.gif" class="button" width="14" height="10" onClick="location.href='#top';" title="Back to Top"></td>
	</tr>
</table>
</div>

<div class="panel" id="process_list">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<a style="color: #0066CC; text-decoration: underline;" onClick="document.getElementById('process_list').style.display='none';document.getElementById('process_list_iframe').style.display='block'; processes.location.href='/?module=mod_devices&action=ajax_proxy&profile=dash_windows_generic&proc=ajax_get_windows_processes&ip={$smarty.get.ip}&root_tpl=blank_panel&device_id={$device.id}';">Click here to see a list of processes.</a>
</div>

<div id="process_list_iframe" style="display:none;">
<iframe height="150px" id="processes" name="processes" width="100%" frameborder="0"></iframe>
</div>



<script language="Javascript">
	parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');
	oid_handler.init();


</script>

{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}
<!-- end of {$smarty.template} -->
