
<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{* This determines if we are looking at Windows processes, in which case we should not show the CPU utilization column *}
{foreach from=$processes item="process"}{capture assign="windows_sucks"}{if $process.CPU != -1}1{else}0{/if}{/capture}{/foreach}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("procs").className = 'sort-table';
	var types = ['CaseInsensitiveString', {/literal}{if 1 == $windows_sucks}{literal}'percentage',{/literal}{/if}{literal} 'netmon_capacity'];
	var report = new SortableTable(document.getElementById("procs"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="datagrid">
<table width="100%" border="0" cellpadding="3" cellspacing="0" id="procs">
	<thead>
	<tr>
		<td>Process Name</td>
		{if 1 == $windows_sucks}
		<td>CPU Utilization</td>
		{/if}
		<td>RAM Utilization</td>
	</tr>
	</thead>

{foreach from=$processes item="process"}
	<tr>
		<td>{$process.Name} [<a href="http://www.google.ca/search?hl=en&q=process+{$process.Name|escape:"url"}&btnG=Search" target="_blank">Google Search</a>]</td>
		{if 1 == $windows_sucks}<td align="center">{$process.CPU}%</td>{/if}
		<td align="center">{$process.Mem|process_size}</td>
	</tr>
{/foreach}
</table>
</div>

{literal}
<script>
function RefreshThisFrame(){
	window.location.replace(window.location.href);
}
setTimeout("RefreshThisFrame()", 60000);
</script>
{/literal}

<!-- end of {$smarty.template} ($Id$) -->