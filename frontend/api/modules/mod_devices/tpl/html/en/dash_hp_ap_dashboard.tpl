<!-- {$smarty.template} ($Id$) -->

{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}
<script>
var oid_handler = new AJAX_OID({$device.id});
var device_id = {$device.id};

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}
</script>
{/literal}

<div class="titlebar">
	<a name="top"></a>{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}

<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td><img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> IP Address: <span class="gray2">{$device.ip_address}</span>
	    &nbsp;&nbsp;&nbsp;
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> MAC: <span class="gray2">{$device.mac|default:"Unresolved"}</span>
		
</td>
	    <td rowspan="2"><div align="right"><img src="/assets/devices/hp.png" alt="Logo: HP" ></div></td>
	</tr>
	<tr>
		<td>
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> 
	  System Description: {ajax_oid class="gray2" frequency="0" oid="javascript: addOidWatcher('SNMPv2-MIB::sysDescr.0', 'STRING');"} 
       </td>
    </tr>
	<tr>
      <td colspan="2"> <img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> Serial #: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysName.0"} </td>
    </tr>
	<tr>
      <td colspan="2"><hr /></td>
    </tr>
	<tr>
	<td colspan="2">
	<img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> System Uptime: {ajax_oid class="gray2" frequency="600000" oid="DISMAN-EXPRESSION-MIB::sysUpTimeInstance"}
	
	</td>
	</tr>
</table>


</fieldset>
		{capture assign="dev_id"}{$device.id}{/capture}
		<!--{input type="button" class="button" value="Delete Device" onClick="parent.pnl_right.showEditor('?module=mod_devices&action=delete_snmp_device&id=$dev_id');"}-->



<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td width="75%" valign="top">
	

<fieldset>
<legend>&nbsp;<strong>RAM</strong>&nbsp;</legend>

<table border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td valign="middle">
        <div align="center" style="margin-top: 5px;">
              <script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge_inverse?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_hp_switch%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge_inverse?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_hp_switch%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank'
			 ); //end AC code
			</script>
            </div>
        <p align="center" style="margin-bottom:0px;">RAM Utilization</p>        
		</td>
    </tr>
</table>


</fieldset>

	
	</td>
	</tr>
</table>


</div>

{section name=radios loop=$radio_count}

	
<div class="panel" id=iface{$interface.interface}>
	<table width="100%" cellpadding="1" cellspacing="0">

		<td width="56" rowspan="2" align="left">
			<img src="/assets/icons/wifi.png">{$radio_count[radios]}
					
		</td>
		<td>
		    SSID: <span class="gray2">{$ssids[$radio_id]}</span>		    
		    <br>
		    Encryption: {ajax_oid  class="gray2" frequency="30000" oid="COLUBRIS-VIRTUAL-AP-MIB::coVirtualApSecurity.3.`$radio_count[radios]`"}
		</td>
	</tr>


</table>
</div>
	{/section}

{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}

	<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');

oid_handler.init();
    </script>


{if $interfaces}

{foreach from=$interfaces item="interface"}

	
<div class="panel" id=iface{$interface.interface}>
	<table width="100%" cellpadding="1" cellspacing="0">

		<td width="56" rowspan="2" align="left">
			<div class="interfacebox" style="margin-left: 5px;" onClick="location.href='?module=mod_devices&action=manage_snmp_interface&ip={$device.ip_address}&interface={$interface.interface}'">{$interface.interface}</div>
					
		</td>
		<td width="100">
			{if $interface.last_inbound_throughput != 0}
				<span class="green2">&nbsp;In: &nbsp;</span> {$interface.last_inbound_throughput|speed}ps
			{else}
				<span class="gray3">&nbsp;In: &nbsp;</span> <span class="unresolved">N/A</span>
			{/if}
										
		</td>
		<td>
			<div align="left">
				Label: <span class="gray2">{$interface.description}</span> &nbsp;Name: <span class="gray2">{$interface.name}</span> {capture name='ifindex' assign='ifindex'}{$etherstats[$interface.interface].index}{/capture} {ajax_oid callback="duplex_status_callback" frequency="30000" oid="EtherLike-MIB::dot3StatsDuplexStatus."|cat:$ifindex}<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsDuplexStatus.{$etherstats[$interface.interface].index}', 'STRING')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a><br />
			</div>
		</td>

	</tr>
	<tr>
		  <td>
			  {if $interface.last_outbound_throughput != 0}
				  <span class="blue2">Out:</span> {$interface.last_outbound_throughput|speed}ps
			  {else}
				  <span class="gray3">Out:</span> <span class="unresolved">N/A</span>
			  {/if}
		</td>
		<td>
			Status: {ajax_oid callback="interface_status_callback" frequency="15000" oid="IF-MIB::ifOperStatus."|cat:$interface.interface}<a href="javascript:addOIDTracker({$device.id}, 'IF-MIB::ifOperStatus.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>   Connected IP/MAC: {if $interface.mac|resolve_max != ""} <a href="#" onClick="parent.location = '?module=mod_layout&action=render_section&layout=network&ip={$interface.mac|resolve_mac}&store_request=2'">{$interface.mac|resolve_max}{else}<span class="unresolved">Unresolved</span>{/if}</a>
		</td>
	</tr>
	<tr>
		<td colspan=3>
			<a style="color: #0066CC; text-decoration: underline;" onclick="toggleProgressIndicator('errors{$interface.interface}');"> Toggle Error Panel</a><br>
			<div  id="errors{$interface.interface}" style="display:none;" >
				<table width=100%>
					<tr>
						<td colspan=4>
							<hr />
						</td>
					</tr>
					<tr>
						<td>
							Alignment Errors:
						</td>
						<td> 
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000" oid="EtherLike-MIB::dot3StatsAlignmentErrors.`$ifindex`" }<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsAlignmentErrors.{$ifindex}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>
						</td>
						<td>
							FCS Errors: 
						</td>
						<td>
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000"  oid="EtherLike-MIB::dot3StatsFCSErrors.`$ifindex`"}<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsFCSErrors.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>
						</td>
					</tr>
					<tr>
						<td>
							Single Collision Frames: 
						</td>
						<td> 
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000" oid="EtherLike-MIB::dot3StatsSingleCollisionFrames.`$etherstats[$interface.interface].index`" }<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsSingleCollisionFrames.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>
						</td>
						<td>
							Multiple Collision Frames:
						</td>
						<td>
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000" oid="EtherLike-MIB::dot3StatsMultipleCollisionFrames.`$etherstats[$interface.interface].index`" }<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsMultipleCollisionFrames.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>
						</td>
					</tr>
					<tr>
						<td>
							Deferred Transmission:
						</td>
						<td> 
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000" oid="EtherLike-MIB::dot3StatsDeferredTransmissions.`$etherstats[$interface.interface].index`" }<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsDeferredTransmissions.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>
						</td>
						<td>
							Excessive Collisions:
						</td>
						<td> 
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000" oid="EtherLike-MIB::dot3StatsExcessiveCollisions.`$etherstats[$interface.interface].index`" }
							<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsExcessiveCollisions.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>
						</td>
					</tr>
					<tr>
						<td>
							Internal MAC Transmit Errors: 
						</td>
						<td>
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000" oid="EtherLike-MIB::dot3StatsInternalMacTransmitErrors.`$etherstats[$interface.interface].index`" }<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsInternalMacTransmitErrors.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>
						</td>
						<td>
							Internal MAC Receive Errors:
						</td>
						<td>
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000" oid="EtherLike-MIB::dot3StatsInternalMacReceiveErrors.`$etherstats[$interface.interface].index`" }<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsInternalMacReceiveErrors.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>
						</td>
					</tr>
					<tr>
						<td>
							Carrier Sense Errors:
						</td>
						<td>
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000" oid="EtherLike-MIB::dot3StatsCarrierSenseErrors.`$etherstats[$interface.interface].index`" }<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsCarrierSenseErrors.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a>
						</td>
						<td>
							Frame Too Long Errs: 
						</td>
						<td>
							{ajax_oid class="gray2" parent="errors`$interface.interface`" frequency="30000" oid="EtherLike-MIB::dot3StatsFrameTooLongs.`$etherstats[$interface.interface].index`" }<a href="javascript:addOIDTracker({$device.id}, 'EtherLike-MIB::dot3StatsFrameTooLongs.{$interface.interface}', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" galt="Track This Item" border="0"></a><br>
						</td>
					</tr>
					<tr>
						<td colspan=4>
							<hr />
						</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>

	</tr>
</table>
</div>
	{/foreach}

	{else}

	{"No interface available for this SNMP device at the moment"|message_bar}

	{/if}


  <!-- end of {$smarty.template} -->



