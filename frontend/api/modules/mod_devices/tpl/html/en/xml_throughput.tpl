<!-- {$smarty.template} ($Id$) -->
<!-- Step size: {$step} - Dataset size: {$size} -->
<graph caption="{$row.label} ({$smarty.get.ip})" subcaption="Interface {$smarty.get.interface}: {$row.name}" divlinecolor="C5C5C5" numberSuffix="bps" decimalPrecision="1" numdivlines="3" showAreaBorder="1" areaBorderColor="444444" numberPrefix="" canvasBgColor="" canvasbordercolor="888888" canvasBorderThickness="2" showNames="1" rotateNames="0" numVDivLines="25" vDivLineAlpha="30" showAlternateHGridColor="1" alternateHGridColor="C9C9C9" formatNumberScale="1" bgcolor="" animation="{$smarty.get.animation|default:0}" anchorRadius="4" showShadow="1" lineThickness="3" showValues="0" showBarShadow="0" {if $row.last_inbound_throughput == 0 && $row.last_outbound_throughput ==0}yAxisMaxValue="100000000"{/if} >
{if $row}
   <set name="IN" value="{$row.last_inbound_throughput}" color="0066CC" />
   <set name="OUT" value="{$row.last_outbound_throughput}" color="339999" />
{/if}
</graph>
<!-- end of {$smarty.template} -->
