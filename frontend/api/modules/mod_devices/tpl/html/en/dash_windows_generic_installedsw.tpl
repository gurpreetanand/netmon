
<!-- {$smarty.template} ($Id$) -->

<div class="datagrid">
<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<th align="left">Application Name</th>
		<th>Type</th>
		<th>Install Date</th>
	</tr>

{foreach from=$devs item="dev"}
	<tr>
		<td>{$dev.Name}</td>
		<td align="center">{$dev.Type}</td>
		<td align="center">{$dev.Date}</td>
	</tr>
{/foreach}
</table>
</div>

<!-- end of {$smarty.template} ($Id$) -->