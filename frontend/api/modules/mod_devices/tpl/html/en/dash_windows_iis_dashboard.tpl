
<!-- {$smarty.template} ($Id$) -->

{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}
<script>
var oid_handler = new AJAX_OID({$device.id});
var device_id = {$device.id};

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}


</script>
{/literal}


<div class="titlebar">
	<a name="top"></a>{$device.label} ({$smarty.get.ip})
</div>

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">  
	{literal}
	<input type="button" class="button" value="System Info" onClick="location.href='#systeminfo'" onMouseOver ="if (!is_locked('Submit')) { this.className='button_over'; }" onMouseOut="if (!is_locked('Submit')) { this.className='button'; }" onfocus="this.className='button_selected'; lock_field('Submit');" onblur="this.className='button'; unlock_field('Submit');" />
	<input type="button" class="button" value="Services Summary" onClick="location.href='#services'" onMouseOver ="if (!is_locked('Submit')) { this.className='button_over'; }" onMouseOut="if (!is_locked('Submit')) { this.className='button'; }" onfocus="this.className='button_selected'; lock_field('Submit');" onblur="this.className='button'; unlock_field('Submit');" />
	<input type="button" class="button" value="Process Summary" onClick="location.href='#process'" onMouseOver ="if (!is_locked('Submit')) { this.className='button_over'; }" onMouseOut="if (!is_locked('Submit')) { this.className='button'; }" onfocus="this.className='button_selected'; lock_field('Submit');" onblur="this.className='button'; unlock_field('Submit');" />
	<input type="button" class="button" value="Custom OID Trackers" onClick="location.href='#custom'" onMouseOver ="if (!is_locked('Submit')) { this.className='button_over'; }" onMouseOut="if (!is_locked('Submit')) { this.className='button'; }" onfocus="this.className='button_selected'; lock_field('Submit');" onblur="this.className='button'; unlock_field('Submit');" />
	{/literal}
</div>

<div class="titlebar">
	<a name="systeminfo"></a>System Info
</div>

<div class="panel">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td>

	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	System Name: {ajax_oid class="grey2" frequency="0" oid="SNMPv2-MIB::sysName.0"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Domain/Workgroup: {ajax_oid class="grey2" frequency="0" oid="LanMgr-Mib-II-MIB::domPrimaryDomain.0"}


		</td>
		<td align="right"><img src="/assets/devices/windows.gif" width="32" height="32"></td>
	</tr>
</table>

</div>


<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Uptime: {ajax_oid class="blue2" frequency="30000" oid="DISMAN-EXPRESSION-MIB::sysUpTimeInstance"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Processes: {ajax_oid class="blue2" frequency="30000" oid="HOST-RESOURCES-MIB::hrSystemProcesses.0" trackable=true}

</div>


{ajax_proxy dash="dash_windows_generic" proc="ajax_get_storage_devices" params="ip="|cat:$smarty.get.ip freq="120000" div="storage_devs"}




<div class="titlebar">
	Internet Information Server (IIS) Statistics
</div>



<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Data Sent: {ajax_oid class="blue2" frequency="30000" oid="HTTPSERVER-MIB::totalBytesSentLowWord.0" trackable=true text="bytes"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Data Received: {ajax_oid class="blue2" frequency="30000" oid="HTTPSERVER-MIB::totalBytesReceivedLowWord.0" trackable=true text="bytes"}
</div>

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Current Anonymous Users: {ajax_oid class="blue2" frequency="30000" oid="HTTPSERVER-MIB::currentAnonymousUsers.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Current Logged In Users: {ajax_oid class="blue2" frequency="30000" oid="HTTPSERVER-MIB::currentNonAnonymousUsers.0" trackable=true}
</div>

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Current Connections: {ajax_oid class="blue2" frequency="30000" oid="HTTPSERVER-MIB::currentConnections.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Logon Attempts: {ajax_oid class="blue2" frequency="30000" oid="HTTPSERVER-MIB::logonAttempts.0" trackable=true}
</div>

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	404 (Not Found) Errors: {ajax_oid class="blue2" frequency="30000" oid="HTTPSERVER-MIB::totalNotFoundErrors.0"}
</div>

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	CGI Requests: {ajax_oid class="blue2" frequency="60000" oid="HTTPSERVER-MIB::totalCGIRequests.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	BGI Requests: {ajax_oid class="blue2" frequency="60000" oid="HTTPSERVER-MIB::totalBGIRequests.0" trackable=true}
</div>

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	GET Requests: {ajax_oid class="blue2" frequency="60000" oid="HTTPSERVER-MIB::totalGets.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	POST Requests:{ajax_oid class="blue2" frequency="60000" oid="HTTPSERVER-MIB::totalPosts.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Head Requests: {ajax_oid class="blue2" frequency="60000" oid="HTTPSERVER-MIB::totalHeads.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Other Requests: {ajax_oid class="blue2" frequency="60000" oid="HTTPSERVER-MIB::totalOthers.0" trackable=true}
</div>

<div class="titlebar">
	File Transfer Protocol (FTP) Server Statistics
</div>
<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Files Sent: {ajax_oid class="blue2" frequency="30000" oid="FtpServer-MIB::totalFilesSent.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Files Received: {ajax_oid class="blue2" frequency="30000" oid="FtpServer-MIB::totalFilesReceived.0" trackable=true}
</div>

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Data Sent: {ajax_oid class="blue2" frequency="30000" oid="FtpServer-MIB::totalBytesSentLowWord.0" trackable=true text="bytes"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Data Received: {ajax_oid class="blue2" frequency="30000" oid="FtpServer-MIB::totalBytesReceivedLowWord.0" trackable=true text="bytes"}
</div>

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Current Anonymous Users: {ajax_oid class="blue2" frequency="30000" oid="FtpServer-MIB::currentAnonymousUsers.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Current Logged In Users: {ajax_oid class="blue2" frequency="30000" oid="FtpServer-MIB::currentNonAnonymousUsers.0" trackable=true}
</div>

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Current Connections: {ajax_oid class="blue2" frequency="60000" oid="FtpServer-MIB::currentConnections.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Logon Attempts: {ajax_oid class="blue2" frequency="60000" oid="FtpServer-MIB::logonAttempts.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	Connection Attempts: {ajax_oid class="blue2" frequency="60000" oid="FtpServer-MIB::connectionAttempts.0" trackable=true}

</div>

<div class="titlebar">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td><a name="services"></a>Services Summary</td>
		<td align="right"><img src="assets/icons/back_to_top.gif" class="button" width="14" height="10" onClick="location.href='#top';" title="Back to Top"></td>
	</tr>
</table>
</div>

<div class="panel" id="service_list">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<a href="#" onClick="document.getElementById('service_list').style.display='none';document.getElementById('service_list_iframe').style.display='block';services.location.href='/?module=mod_devices&action=ajax_proxy&profile=dash_windows_generic&proc=ajax_get_windows_services&ip={$smarty.get.ip}&root_tpl=blank_panel';">Click here to see a list of Windows Services.</a>
</div>

<div id="service_list_iframe" style="display:none;">
<iframe height="150px" id="services" name="services" width="100%" frameborder="0"></iframe>
</div>

<div class="titlebar">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td><a name="process"></a>Process Summary</td>
		<td align="right"><img src="assets/icons/back_to_top.gif" class="button" width="14" height="10" onClick="location.href='#top';" title="Back to Top"></td>
	</tr>
</table>
</div>

<div class="panel" id="process_list">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<a href="#" onClick="document.getElementById('process_list').style.display='none';document.getElementById('process_list_iframe').style.display='block'; processes.location.href='/?module=mod_devices&action=ajax_proxy&profile=dash_windows_generic&proc=ajax_get_windows_processes&ip={$smarty.get.ip}&root_tpl=blank_panel';">Click here to see a list of processes.</a>
</div>

<div id="process_list_iframe" style="display:none;">
<iframe height="150px" id="processes" name="processes" width="100%" frameborder="0"></iframe>
</div>


<div class="titlebar">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td>Installed Software</td>
		<td align="right"><img src="assets/icons/back_to_top.gif" class="button" width="14" height="10" onClick="location.href='#top';" title="Back to Top"></td>
	</tr>
</table>
</div>

<div class="panel" id="installed_sw_list">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<a style="color: #0066CC; text-decoration: underline;" onClick="document.getElementById('installed_sw_list').style.display='none';document.getElementById('installed_sw_list_iframe').style.display='block'; installed_sw.location.href='/?module=mod_devices&action=ajax_proxy&profile=dash_windows_generic&proc=ajax_get_installed_sw&ip={$smarty.get.ip}&root_tpl=blank_panel&device_id={$device.id}';">Click here to see a list of installed software.</a>
</div>

<div id="installed_sw_list_iframe" style="display:none;">
<iframe height="150px" id="installed_sw" name="installed_sw" width="100%" frameborder="0"></iframe>
</div>



{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}


<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');


oid_handler.init();

</script>


<!-- end of {$smarty.template} -->
