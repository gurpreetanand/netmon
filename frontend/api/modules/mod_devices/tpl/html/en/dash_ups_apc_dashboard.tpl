
<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}

<script language="Javascript">
var oid_handler = new AJAX_OID({$device.id});

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}
{/literal}
</script>



<div class="titlebar">
	<a name="top"></a>{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}

<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td><img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> Name: {ajax_oid class="gray2" frequency="0" oid="PowerNet-MIB::upsBasicIdentName.0"} &nbsp; Model: {ajax_oid class="gray2" frequency="0" oid="PowerNet-MIB::upsBasicIdentModel.0"} &nbsp; Serial No: {ajax_oid class="gray2" frequency="0" oid="PowerNet-MIB::upsAdvIdentSerialNumber.0"}</td>
	</tr>
	<tr>
		<td>
		
	  <img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> Firmware Version: {ajax_oid class="gray2" frequency="0" oid="PowerNet-MIB::upsAdvIdentFirmwareRevision.0"}&nbsp; <img src="/assets/icons/calendar.png" width="16" height="16" class="icon"> Last Diagnostic Test: {ajax_oid class="gray2" frequency="0" oid="PowerNet-MIB::upsAdvTestLastDiagnosticsDate.0" trackable=true}</td>
	</tr>
</table>


</fieldset>

<fieldset>
<legend>&nbsp;<img src="/assets/icons/lightning.png" width="16" height="16" class="icon"> <strong>Battery Status</strong>&nbsp;</legend>
  <table border="0" cellpadding="4" cellspacing>
	<tr>
	  <td valign="middle"><script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'bgcolor','D4D0C8',
	'src','assets/flash/gauge_temp?temp={$oids.upsAdvBatteryTemperature}&lowthreshold=20&hithreshold=38&sensorname=Internal%20Battery&unit=C',
	'quality','high',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_temp?temp={$oids.upsAdvBatteryTemperature}&lowthreshold=20&hithreshold=38&sensorname=Internal%20Battery&unit=C'
	 ); //end AC code
	</script></td>
		<td valign="middle"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge_inverse?device_id={$device.id}&oid=PowerNet-MIB::upsAdvBatteryCapacity.0&refresh=30&url={$smarty.const.TOP_DOMAIN}',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge_inverse?device_id={$device.id}&oid=PowerNet-MIB::upsAdvBatteryCapacity.0&refresh=30&url={$smarty.const.TOP_DOMAIN}'
			 ); //end AC code
			</script></div>
	  <p align="center" style="margin-bottom:0px;">Charge Level<a href="javascript:addOIDTracker({$device.id}, 'PowerNet-MIB::upsAdvBatteryCapacity.0', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></p></td>
		<td valign="middle"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&oid=PowerNet-MIB::upsAdvOutputLoad.0&refresh=30&url={$smarty.const.TOP_DOMAIN}',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&oid=PowerNet-MIB::upsAdvOutputLoad.0&refresh=30&url={$smarty.const.TOP_DOMAIN}'
			 ); //end AC code
			</script></div>
			<p align="center" style="margin-bottom: 0px;">Output Load<a href="javascript:addOIDTracker({$device.id}, 'PowerNet-MIB::upsAdvOutputLoad.0', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a></p>
	  </td>
	</tr>
	<tr>
	  <td colspan="3" valign="middle"><hr /></td>
    </tr>

	<tr>
		<td valign="top"><img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> Runtime Remaining: {ajax_oid class="blue" frequency="30000" oid="PowerNet-MIB::upsAdvBatteryRunTimeRemaining.0" trackable=true}</td>
	    <td valign="top" colspan="2"><img src="/assets/icons/info.png" width="16" height="16" class="icon"> {ajax_oid class="blue" frequency="30000" oid="PowerNet-MIB::upsBasicOutputStatus.0" trackable=true}</td>
	</tr>
	<tr>
	  <td valign="top"><img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> Time on Battery: {ajax_oid class="blue" frequency="30000" oid="PowerNet-MIB::upsBasicBatteryTimeOnBattery.0" trackable=true}</td>
      <td valign="top" colspan="2"><img src="/assets/icons/info.png" width="16" height="16" class="icon"> {ajax_oid class="blue" frequency="30000" oid="PowerNet-MIB::upsBasicBatteryStatus.0" trackable=true}</td>
	</tr>
	<tr>
	  <td valign="top"><img src="/assets/icons/calendar.png" width="16" height="16" class="icon"> Last Battery Replacement: {ajax_oid class="gray2" frequency="0" oid="PowerNet-MIB::upsBasicBatteryLastReplaceDate.0" trackable=true}
      </td>
      <td valign="top" colspan="2"><img src="/assets/icons/info.png" width="16" height="16" class="icon"> {ajax_oid class="blue" frequency="30000" oid="PowerNet-MIB::upsAdvBatteryReplaceIndicator.0" trackable=true}</td>
	</tr>
</table>
</fieldset>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="50%" valign="middle">
		
		<fieldset>
<legend>&nbsp;<img src="/assets/icons/lightning.png" width="16" height="16" class="icon"> <strong>Power Input</strong>&nbsp;</legend>
		
		<div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','200',
			'height','100',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge_voltage?device_id={$device.id}&oid=PowerNet-MIB::upsAdvInputLineVoltage.0&refresh=30&url={$smarty.const.TOP_DOMAIN}&label=Input Line Voltage&refresh=30',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge_voltage?device_id={$device.id}&oid=PowerNet-MIB::upsAdvInputLineVoltage.0&refresh=30&url={$smarty.const.TOP_DOMAIN}&label=Input Line Voltage&refresh=30'
			 ); //end AC code
			</script></div>
			<p align="center">Phase: {ajax_oid class="gray2" frequency="0" oid="PowerNet-MIB::upsBasicInputPhase.0" trackable=true}</p>
			
		  </fieldset>
	  </td>
		<td width="50%" valign="middle">
		<fieldset>
<legend>&nbsp;<img src="/assets/icons/lightning.png" width="16" height="16" class="icon"> <strong>Power Output</strong>&nbsp;</legend>
		<div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','200',
			'height','100',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge_voltage?device_id={$device.id}&oid=PowerNet-MIB::upsAdvOutputVoltage.0&refresh=30&url={$smarty.const.TOP_DOMAIN}&label=Output Voltage&refresh=30',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge_voltage?device_id={$device.id}&oid=PowerNet-MIB::upsAdvOutputVoltage.0&refresh=30&url={$smarty.const.TOP_DOMAIN}&label=Output Voltage&refresh=30'
			 ); //end AC code
			</script></div>
			<p align="center">Phase: {ajax_oid class="gray2" frequency="0" oid="PowerNet-MIB::upsBasicOutputPhase.0" trackable=true}</p>
	  </fieldset>
	  
	  </td>
	</tr>
</table>


</div>


<script language="Javascript">
	parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');
	oid_handler.init();
</script>

{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}
<!-- end of {$smarty.template} -->
