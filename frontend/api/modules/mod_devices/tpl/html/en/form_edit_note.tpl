
<!-- {$smarty.template} ($Id$) -->

<div class="panel">
<form method="POST" action="?module=mod_devices&action=process_update_note&id={$note->get('id')}">
<input type="hidden" name="device_id" value="{$note->get('device_id')}">
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<td>
				<div align="center"><strong>Subject:</strong><br />
				<input type="text" {param name="subject" class="input"} value="{$smarty.post.subject|default:$note->get('subject')}"></div>
			</td>
		</tr>


		<tr>
			<td>
			<div align="center"><strong>Note:</strong><br />
			<textarea rows="14" cols="32" {param name="note" class="input"}>{$smarty.post.note|default:$note->get('note')}</textarea>
			</div>
			</td>
		</tr>

		<tr>
			<td colspan="2" align="center">{input class="button" type="submit" value="Save Changes"}</td>
		</tr>
	</table>
</form>
</div>

<!-- end of {$smarty.template} -->
