<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{* This determines if we are looking at Windows processes, in which case we should not show the CPU utilization column *}
{literal}
<script language="Javascript">

init_sort = function() {
        document.getElementById("vms").className = 'sort-table';
        var types = ['CaseInsensitiveString','CaseInsensitiveString','CaseInsensitiveString','Number','Number','CaseInsensitiveString'];
        var report = new SortableTable(document.getElementById("vms"), types);
        report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}

{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}
<script>
var oid_handler = new AJAX_OID({$device.id});
var device_id = {$device.id};

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}
{/literal}

</script>


<div class="titlebar">
	<a name="top"></a>{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}

<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>System Name: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysName.0"}  &nbsp; Location: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysLocation.0"}</td>
	    <td rowspan="2" valign="top"><div align="right"><img src="/assets/devices/vmware_esxi.gif" style="margin-right: 5px;"></div></td>
	</tr>
	<tr>
		<td>
		
	      <img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> System Uptime: {ajax_oid class="gray2" frequency="0" oid="DISMAN-EXPRESSION-MIB::sysUpTimeInstance"}
      </td>
    </tr>
	<tr>
	  <td colspan="2"> System Description: {ajax_oid class="unresolved" frequency="0" oid="SNMPv2-MIB::sysDescr.0"}</td>
    </tr>
</table>


</fieldset>


<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
  <td width="100%" valign="top">
	
    <fieldset>
      <legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>Host Information</strong>&nbsp;</legend>

      <table border="0" cellpadding="3" cellspacing="5">
        <tr>
        <td valign="middle"><p>VMWare Version:</p></td><td align="right">{ajax_oid class="blue" frequency="300000" oid="VMWARE-SYSTEM-MIB::vmwProdVersion.0"}</td>
	</tr><tr>
        <td valign="middle"><p>Build Number:</p></td><td align="right">{ajax_oid class="blue" frequency="300000" oid="VMWARE-SYSTEM-MIB::vmwProdBuild.0"}</td>
	</tr><tr>
        <td valign="middle"><p>Total System Memory:</p></td><td align="right">{ajax_oid class="blue" frequency="300000" oid="VMWARE-RESOURCES-MIB::vmwMemSize.0"}</td><td>Kbytes</td>
        </tr><tr>
        <td valign="middle"><p>Reserved for ESX OS:</p></td><td align="right">{ajax_oid class="blue" frequency="300000" oid="VMWARE-RESOURCES-MIB::vmwMemCOS.0"}</td><td>KBytes</td>
        </tr><tr>
        <td valign="middle"><p>Memory Available for VMs:</p></td><td align="right">{ajax_oid class="blue" frequency="300000" oid="VMWARE-RESOURCES-MIB::vmwMemAvail.0" trackable=true}</td><td>KBytes</td>
	</tr>
     </table>



  </tr>
</table>
</fieldset>

{if $vminfo}

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td width="100%" valign="top">
<fieldset>
<legend>&nbsp;<strong>Virtual Machines</strong>&nbsp;</legend>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td>

        <div class="datagrid">
        <table border="0" cellpadding="0" cellspacing="0" id="vms">
          <thead>
                <td>Power State</td>
                <td>Name</td>
                <td>VM ID</td>
                <td>Allocated CPU(s)</td>
                <td>Allocated Memory</td>
                <td>Guest OS</td>
          </thead>
            {foreach from=$vminfo item="vm"}
                <tr>
		{if $vm.State == "poweredOff"}
                <td><font color="red">Powered Off</font></td>
		{elseif $vm.State == "poweredOn"}
		<td><font color="green">Powered On</font></td>
		{else}
		<td><font color="green">{$vm.State}</font></td>
		{/if}
                <td>{$vm.DisplayName}</td>
                <td>{$vm.VMID}</td>
                <td>{$vm.Cpus}</td>
                <td>{$vm.MemSize}</td>
                <td>{$vm.GuestOS}</td>
                </tr>
            {/foreach}
       </table></div>

</td></tr>
</table>
</fieldset>
</td></tr>
</table>

{else}
  {"No VM Information is available from this SNMP device at the moment."|message_bar}
{/if}

	
	</td>
	</tr>
</table>


</div>



{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}

	<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');

oid_handler.init();
    </script>


  <!-- end of {$smarty.template} -->



