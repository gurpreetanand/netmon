
<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}
{literal}
<script>
var oid_handler = new AJAX_OID({/literal}{$device.id}{literal});

function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}



</script>
{/literal}

<div class="titlebar">
	{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}

<div class="panel">

	<!--{literal} {if $oids.firmware_version} {/literal} -->
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	Firmware Version: v{ajax_oid class="gray2" frequency="0" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::version.0"}&nbsp;<a href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3Aversion.0%20&root_tpl=blank_panel&class=white');" style="color: #0066CC;">[?]</a>
	<!-- {literal} {/if} {/literal} -->

	<!-- {literal} {if $oids.location}  {/literal} -->
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	Location: {ajax_oid class="gray2" frequency="0" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::sysLocation.0"}&nbsp;<a href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3AsysLocation.0%20&root_tpl=blank_panel&class=white');" style="color: #0066CC;">[?]</a>
	<!--{literal}  {/if} {/literal} -->

</div>

<div class="panel">

	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_temp?lowthreshold={$oids.temp1_lowthresh}&hithreshold={$oids.temp1_highthresh}&sensorname={$oids.temp1_name|escape:"url"}&unit={$oids.temp_unit|truncate:1:""}&device_id={$device.id}&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3AtemperatureSensor1CurrentValue.0&refresh=1&enviro=1&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_temp?lowthreshold={$oids.temp1_lowthresh}&hithreshold={$oids.temp1_highthresh}&sensorname={$oids.temp1_name|escape:"url"}&unit={$oids.temp_unit|truncate:1:""}&device_id={$device.id}&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3AtemperatureSensor1CurrentValue.0&refresh=1&enviro=1&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>


	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_temp?lowthreshold={$oids.temp2_lowthresh}&hithreshold={$oids.temp2_highthresh}&sensorname={$oids.temp2_name|escape:"url"}&unit={$oids.temp_unit|truncate:1:""}&device_id={$device.id}&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3AtemperatureSensor2CurrentValue.0&refresh=1&enviro=1&url={$smarty.server.SERVER_NAME}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_temp?lowthreshold={$oids.temp2_lowthresh}&hithreshold={$oids.temp2_highthresh}&sensorname={$oids.temp2_name|escape:"url"}&unit={$oids.temp_unit|truncate:1:""}&device_id={$device.id}&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3AtemperatureSensor2CurrentValue.0&refresh=1&enviro=1&url={$smarty.server.SERVER_NAME}'
	 ); //end AC code
	</script>

	
</div>

<div class="panel">

	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_humidity?lowthreshold={$oids.hum1_lowthresh}&hithreshold={$oids.hum1_highthresh}&sensorname={$oids.hum1_name|escape:"url"}&device_id={$device.id}&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3AhumiditySensor1CurrentValue.0&refresh=1&enviro=1&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_humidity?lowthreshold={$oids.hum1_lowthresh}&hithreshold={$oids.hum1_highthresh}&sensorname={$oids.hum1_name|escape:"url"}&device_id={$device.id}&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3AhumiditySensor1CurrentValue.0&refresh=1&enviro=1&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>


	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_humidity?lowthreshold={$oids.hum2_lowthresh}&hithreshold={$oids.hum2_highthresh}&sensorname={$oids.hum2_name|escape:"url"}&device_id={$device.id}&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3AhumiditySensor2CurrentValue.0&refresh=1&enviro=1&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_humidity?lowthreshold={$oids.hum2_lowthresh}&hithreshold={$oids.hum2_highthresh}&sensorname={$oids.hum2_name|escape:"url"}&device_id={$device.id}&oid=NETWORK-TECHNOLOGIES-GLOBAL-REG%3A%3AhumiditySensor2CurrentValue.0&refresh=1&enviro=1&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>

</div>

<div class="panel">
<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
{if $oids.water_status == "On"}
<img src="/assets/icons/alert_active.gif" width="16" height="16" class="icon">&nbsp;
{/if}

	Water Sensor: {ajax_oid class="gray2" frequency="0" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::waterName.0"}<!-- {$oids.water_name} -->
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	Status: {ajax_oid class="blue" frequency="60000" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::waterStatus.0" trackable=true}

</div>

<div class="panel">
<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">

{if $oids.dry1_status == "Off"}
	<img src="/assets/icons/alert_active.gif" width="16" height="16" class="icon">&nbsp;
{/if}

	Dry Contact:  {ajax_oid class="gray2" frequency="0" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact1Name.0"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">	
	Status: {ajax_oid class="blue" frequency="15000" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact1Status.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	Alert: {ajax_oid class="blue" frequency="15000" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact1Alert.0" trackable=true}
	
</div>

<div class="panel">
<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">

{if $oids.dry2_status == "Off"}
	<img src="/assets/icons/alert_active.gif" width="16" height="16" class="icon">&nbsp;
{/if}

	Dry Contact:  {ajax_oid class="gray2" frequency="0" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact2Name.0"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">	
	Status: {ajax_oid class="blue" frequency="15000" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact2Status.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	Alert: {ajax_oid class="blue" frequency="15000" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact2Alert.0" trackable=true}

</div>

<div class="panel">
<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">

{if $oids.dry3_status == "Off"}
	<img src="/assets/icons/alert_active.gif" width="16" height="16" class="icon">&nbsp;
{/if}

	Dry Contact:  {ajax_oid class="gray2" frequency="0" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact3Name.0"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">	
	Status: {ajax_oid class="blue" frequency="15000" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact3Status.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	Alert: {ajax_oid class="blue" frequency="15000" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact3Alert.0" trackable=true}

</div>

<div class="panel">
<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">

{if $oids.dry4_status == "Off"}
	<img src="/assets/icons/alert_active.gif" width="16" height="16" class="icon">&nbsp;
{/if}

	Dry Contact:  {ajax_oid class="gray2" frequency="0" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact4Name.0"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">	
	Status: {ajax_oid class="blue" frequency="15000" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact4Status.0" trackable=true}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
	Alert: {ajax_oid class="blue" frequency="15000" oid="NETWORK-TECHNOLOGIES-GLOBAL-REG::dryContact4Alert.0" trackable=true}

</div>

{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}


<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');

oid_handler.init();


</script>



<!-- end of {$smarty.template} -->
