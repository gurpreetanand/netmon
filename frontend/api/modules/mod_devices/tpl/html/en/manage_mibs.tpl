<!-- {$smarty.template} ($Id$) -->


<div class="titlebar">MIB File Manager</div>
	<script language="Javascript">
		parent.pnl_right.showEditor('?module=mod_devices&action=form_upload_mib');
	</script>

	<!-- Toolbar -->
	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
		<img src="assets/buttons/button_help.gif" title="View Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('snmp_mibupload');">
		<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
		{input type="button" class="button" value="Upload new MIB" onClick="parent.pnl_right.showEditor('?module=mod_devices&action=form_upload_mib');"}
	</div>


{if $mibs}
<div class="datagrid center">
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
	<th>File Name</th>
	<th>Actions</th>
</tr>
{foreach from=$mibs item="mib"}
<tr>
	<td><div align="left"><a href="?module=mod_devices&action=view_mib_file&mib_file={$mib|escape:url}"><img src="/assets/icons/mib.gif" class="icon" width="16" height="16"></a> <a href="?module=mod_devices&action=view_mib_file&mib_file={$mib|escape:url}">{$mib}</a></div></td>
	<td><a href="?module=mod_devices&action=view_mib_file&mib_file={$mib|escape:url}">View</a> | <a href="?module=mod_devices&action=delete_mib&mib_file={$mib|escape:url}">Delete</a></td>
</tr>
{/foreach}
</table>
</div>
{else}
{"You have not yet uploaded any MIB files to this repository"|message_bar}
{/if}


<!-- end of {$smarty.template} -->
