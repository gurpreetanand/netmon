<devices>
{if $devices}
{foreach from=$devices item="device"}
  <device id="{$device.id}" ip="{$device.ip}" name="{$device.name}">
{if $device.interfaces}{foreach from=$device.interfaces item="interface"}
    <interface ip="{$device.ip}" name="{$interface.description}" iface="{$interface.interface}" />
{/foreach}{/if}
  </device>
{/foreach}
{/if}
</devices>
