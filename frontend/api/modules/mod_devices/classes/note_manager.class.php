<?php


class Note_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "devices_notes";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_devices";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		$this->params->add_primitive("owner_id",  "integer", TRUE, "Owner ID",  "Owner ID");
		$this->params->add_primitive("device_id", "integer", TRUE, "Device ID", "Device ID");
		$this->params->add_primitive("subject",   "string",  TRUE, "Subject",   "Subject");
		$this->params->add_primitive("note",      "string",  TRUE, "Note Body", "Note Body");
	}


	function pop($id) {
		$id = $this->db->escape($id);
		$query = "SELECT * FROM {$this->table} WHERE id = $id";
		return $this->map($query);
	}

	function fetch_by_device($id) {
		$dev = $this->db->escape($id);
		$query = "SELECT t.*, u.username FROM {$this->table} t LEFT JOIN users u ON u.id = t.owner_id WHERE t.device_id = $dev ORDER BY t.last_modified DESC";

		$res = $this->db->select($query);

		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			return FALSE;
		}
		return $res;
	}

}

?>