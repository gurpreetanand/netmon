<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * $Id$
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


// mod_settings.class.php

/**
  * SNMP MCO
  *
  * This module is used to perform any application-level change and management
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class mod_devices extends MadnetModule {

  /**
   * Device Dashboard Manager Object
   *
   * @var Device_Dashboard_Manager
   */
  var $dashboard_manager;
  
  /**
   * Cache OID to ASCII translations
   */
  var $oid_cache = array();

  /**
    * Initializes all module variables
    *
    * @return void
    */
  function init() {
    require_class($this->module, "device_manager");
    require_class($this->module, "snmp_interface_manager");
    require_class($this->module, "device_dashboard_manager");


    $this->dashboard_manager = $this->registry->get_singleton($this->module, "device_dashboard_manager");
  }

  /**
   * Displays the SNMP Tree
   *
   * @param pointer $index_content
   * @return unknown
   */
  function get_snmp_tree(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("devices", $devices);
    $index_content .= $parser->fetch("snmp_tree.tpl");
  }

  /**
   * Displays the Device Creation form
   *
   * @param pointer $index_content
   */
  function form_create_snmp_device(&$index_content) {

    $sdm = new device_manager();


    $parser = new Parser($this->tpl_dir);
    $parser->set_params($sdm->params);
    $parser->assign("profiles", $this->dashboard_manager->_smarty_device_profiles());
    $index_content .= $parser->fetch("form_create_snmp_device.tpl");
    return;
  }

  function process_create_snmp_device(&$index_content) {
    $sdm = new device_manager();
    if ($sdm->insert()) {
      $parser = new Parser($this->tpl_dir);
      $parser->assign("reload_tree", TRUE);
      $parser->assign("item", "Device");
      $parser->assign("action_taken", "created");
      $index_content .= $parser->fetch("item_modified.tpl");
    } else {
      $index_content .= $this->form_create_snmp_device($index_content);
    }
  }

  function process_create_device() {
    $sdm = new device_manager();
    if ($sdm->insert()) {
      $lastId = $sdm->last_insert_id();
      print "{}";
    } else {
      $this->err->err_from_code(400, "Could not insert");
    }
    $ms = require_module("mod_settings");
    $ms->_sig_procmond("restart oidmond");
  }

  function form_edit_snmp_device(&$index_content) {

    $sdm = new device_manager();

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("A Device ID was not passed along with this request.");
      return;
    }

    if (!$sdm->fetch($id)) {
      $this->err->err_from_string("Unable to fetch SNMP device information.");
      return;
    }

    $parser = new Parser($this->tpl_dir);

    $parser->assign("profiles", $this->dashboard_manager->_smarty_device_profiles());
    $parser->assign_by_ref("sdm", $sdm);
    $parser->set_params($sdm->params);
    $index_content .= $parser->fetch("form_edit_snmp_device.tpl");
    return;
  }

  function update_snmp_device(&$index_content) {
    $sdm = new device_manager();
    if ($sdm->update()) {
      $parser = new Parser($this->tpl_dir);
      $parser->assign("action_taken", "updated");
      $parser->assign("item", "Device");
      $parser->assign("reload_tree", TRUE);
      $index_content .= $parser->fetch("item_modified.tpl");
    } else {
      $index_content .= $this->form_edit_snmp_device($index_content);
    }
  }

  function update_device() {
    $sdm = new device_manager();
    if ($sdm->update()) {
      // TODO: All operations should really do this.
      print json_encode($sdm->autoinsert_array, true);
    } else {
      $this->err->err_from_code(400, "Could not update");
    }
  }

  /**
   * Displays the management dashboard for a specific device profile
   *
   * @param pointer $index_content
   */
  function dashboard(&$index_content) {
    $profile = $_GET['profile'] ? $_GET['profile'] : NULL;
    return $this->dashboard_manager->get_dashboard($profile, $index_content);
  }

  function json_dashboard(&$index_content) {
    require_class("core", "memcached_manager");
    $memcache = new Memcached_Manager();
    $dashboard = require_module("mod_dashboard");
    $settings = require_module("mod_settings");

    $sdm = new device_manager();
    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("A Device ID was not passed along with this request.");
      return;
    }

    $device = $sdm->fetch($id);
    
    $ip = $device['ip_address'];
    
    $profile = $device["profile"];

    $dashjson = json_decode(file_get_contents("/apache/dashboards.json"), true);
    
    $profilejson = $dashjson[$profile];
    
    $oids = $profilejson["oids"];
    $walks = $profilejson["walks"];

    $payload = $device;

    $payload["alertTrackers"] = $settings->get_alerts_object($ip);

    require_class($this->module, "note_manager");
    require_class($this->module, "snmp_interface_manager");
    require_class("mod_services", "services_manager");
    require_class("mod_syslog", "syslog_manager");
    require_class($this->module, 'oid_watcher_manager');


    $payload["hasPanelError"] = $this->add_interface_info($payload, $dashjson, $memcache, true);

    $sm = new Services_Manager();
    $payload["servers"] = $sm->get_by_ip($device['ip_address']);

    $slm = new Syslog_Manager();
    $payload["syslog"] = $slm->get_entries_by_ip($device['ip_address']);

    $nm = new Note_Manager();
    $notes = $nm->fetch_by_device($id);
    // Resolve user ID
    $payload["notes"] = $notes;
    
    $query = "SELECT * FROM df_servers WHERE ip='" . $device['ip_address'] . "'";
    $res = $this->db->select($query);
    if (!is_array($res))
      $res = array();
    $payload["df_servers"] = $res;
    
    $query = "SELECT * FROM smb_servers WHERE ip='" . $device['ip_address'] . "'";
    $res = $this->db->select($query);
    if (!is_array($res))
      $res = array();
    $payload["smb_servers"] = $res;

    foreach ($oids as &$oid) {
      $memcacheKey = $device["ip_address"] . ":" . $oid["oid"];
      $oid["result"] = json_decode($memcache->get($memcacheKey), true);
    }
    
    foreach ($walks as &$walk) {
      $memcacheKey = $device["ip_address"] . ":" . $walk["oid"];
      $walk["result"] = json_decode($memcache->get($memcacheKey), true);
      if ($walk["result"]["value"]) {
        $walk["result"]["value"] = json_decode($walk["result"]["value"], true);
      }
    }
    
    $payload["reference"] = $oids;
    $payload["walks"] = $walks;
    
    $owm = new OID_Watcher_Manager;
    $payload["oid_trackers"] = $owm->get_oids_by_device($device['id']);
    
    $query = "SELECT id, name FROM device_groups";
    $groups = $this->db->select($query);
    if (!is_array($groups)) {
      $groups = array();
    }

    $payload['groups'] = $groups;
    
    if ($profilejson['tables']) { 
      $payload['tables'] = $profilejson['tables'];
    } else {
      $payload['tables'] = array();
    }
    
    $syslog = $this->db->get_row("SELECT * FROM syslog_access WHERE ip='$ip'");

    if (sizeof($syslog) > 1) {
      $payload['enable_syslog'] = "t";
      $payload['syslog_facility'] = $syslog['facility'];
      $payload['syslog_severity'] = $syslog['severity'];
      $payload['syslog_id'] = $syslog['syslog_id'];
    } else {
      $payload['enable_syslog'] = "f";
    }
    
    $payload['hasRam'] = $profilejson['hasRam'] || false;

    $ipalerts = $this->alerts_by_ip();
    if ($ipalerts[$ip]) {
      $payload['recent_alerts'] = $ipalerts[$ip];
    } else {
      $payload['recent_alerts'] = array();
    }


    
    print json_encode($payload);
  }

  function add_interface_info(&$payload, $dashjson, $memcache, $resolve = false) {
    require_class($this->module, "snmp_interface_manager");
    $dashboard = require_module("mod_dashboard");

    $hasPanelError = false;

    $sim = new SNMP_Interface_Manager();
    $payload["interfaces"] = $sim->get_device_interfaces($payload["id"]);


    foreach ($payload["interfaces"] as &$int) {
      $intErrors = array();
      if ($resolve) {
        // Resolve mac address
        $int['ip'] = $dashboard->resolve_mac($int['mac']);
        $int['hostname'] = $dashboard->resolve_ip($int['ip']);
      }

      if ($payload['show_iface_problems'] == 't') {
        foreach ($dashjson["networkCachePanel"] as $intError) {
          $intOid = $intError['oid'] . $int['interface'];

          $memcacheKey = $payload['ip_address'] . ":" . $intOid;
          $intErrors[$intError['oidPretty']] = $memRes = json_decode($memcache->get($memcacheKey), true);

          if (
            intval($memRes["warning"]) > 0 &&
            intval($memRes["value"]) > 0 &&
            intval($memRes["value"] >= $memRes["warning"])
          ) {
            $hasPanelError = true;
          }
        }
      }
      $int["interfaceCache"] = $intErrors;
    }
    return $hasPanelError;
  }


  /**
   * Acts as a proxy for AJAX requests to call methods in a specific dashboard object
   *
   * @param pointer $index_content
   */
  function ajax_proxy(&$index_content) {
    $profile = $_GET['profile'];
    $proc = $_GET['proc'];
    
    $dev_profile = $this->dashboard_manager->get_profile($profile);
    if ($dev_profile == FALSE) { return FALSE; }

    # First, handle cases where a callback was supplied
    if ($dev_profile['callback'] != NULL) {
      # If the callback is an array, it is a callback to an object method
      # (either &$this or a foreign object)
      if (is_array($dev_profile['callback'])) {
        $object = $dev_profile['callback'][0];
        
        /**
         * NEW: Retrieve the device passed in $_GET['ip'] and assign it to the profile
         * so it can access device info.
         */
        
        $ip = $_GET['ip'];
    
        $dm = new Device_Manager();
        $object->device = $dm->fetch_by_ip($ip);
        
        if (method_exists($object, $proc)) {
          $index_content .= $object->$proc();
        }
      }
    }
    return TRUE;
  }


  function delete_device() {
    $sdm = new device_manager();

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_code(400, "A Device ID was not passed along with this request.");
    }

    if ($sdm->delete($id)) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to delete device");
    }
  }

  function delete_devices() {
    $sdm = new device_manager();

    $ids = explode(",", $_GET['ids']);

    if (sizeof($ids) <= 0) {
      $this->err->err_from_code(400, "No Device IDs were passed along with this request.");
      return;
    }


    foreach ($ids as $id) {
      if (!$sdm->delete(intval($id))) {
        $this->err->err_from_code(400, "Unable to delete device");
        return;
      }
    }

    print '{}';
  }

  function manage_snmp_interface(&$index_content) {
    $parser = new Parser($this->tpl_dir);

    $sim = new SNMP_Interface_Manager();
    if (!$sim->pop($_GET['ip'], $_GET['interface'])) {
      return "Error";
    }
    $sim->params->build_autoinsert_array();
    $this->debugger->add_hit("Values", NULL, NULL, vdump($sim->params->autoinsert_array));
    $parser->assign_by_ref("interface", $sim->params->autoinsert_array);

    $dashboard = require_module("mod_dashboard");
    $parser->register_modifier("resolve_mac", array(&$dashboard, "resolve_mac"));
    $parser->register_modifier("resolve_max", array(&$dashboard, "resolve_max"));

    $index_content .= $parser->fetch("manage_snmp_interface.tpl");
    return "SNMP Interface Manager";
  }
  
  function json_snmp_table() {
    require_class($this->module, "device_manager");
    $dm = new Device_Manager();

    $device = $dm->fetch_by_ip($_GET['ip']);
    if (!$device) {
      $this->err->err_from_code(400, "Invalid device specified");
    }
    require_class($this->module, "mib_manager");
    $mm = new MIB_Manager();
    
    $table = $mm->snmp_table($device['ip_address'], $device['snmp_community'], $_GET['table'], false);
    print json_encode($table, true);
  }

  /**
   * Returns an XML document w/ throughput information for an SNMP interface
   *
   * @param pointer $index_content
   */
  function get_xml_snmp_throughput(&$index_content) {
    $ip = $this->db->escape($_GET['ip']);
    $interface = $this->db->escape($_GET['interface']);
    $query = "SELECT i.name, d.label, i.last_inbound_throughput::int8, i.last_outbound_throughput::int8
    FROM interfaces i, devices d
    WHERE d.id = i.device_id
    AND d.ip_address = $ip
    AND i.interface = $interface
    LIMIT 1";

    $res = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return;
    }

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("row", $res);
    $index_content .= $parser->fetch("xml_throughput.tpl");
    header("Content-type: application/xml");
    return;
  }

  function form_edit_snmp_interface(&$index_content) {

    $sim = new SNMP_Interface_Manager();
    if (!$sim->pop($_GET['ip'], $_GET['interface'])) {
      return;
    }


    $parser = new Parser($this->tpl_dir);
    $parser->set_params($sim->params);
    $parser->assign_by_ref("sim", $sim);
    $index_content .= $parser->fetch("form_edit_snmp_interface.tpl");
    return;

  }

  function update_snmp_interface() {
    $sim = new SNMP_Interface_Manager();
    if (!$sim->update()) {
      $this->err->err_from_code(400, "Unable to update interface");
    } else {
      print '{}';
    }
  }

  function get_interface_traffic($ip, $interface) {

    $ip = $this->db->escape($ip);
    $interface = $this->db->escape(intval($interface));

                $query = "
                        SELECT a.inoctets, a.outoctets, a.timestamp, b.speed
                        FROM snmp_log a, interfaces b, devices c
                        WHERE a.ip_address = $ip
                        AND a.interface = $interface
                        AND c.id = b.device_id
                        AND c.ip = a.ip_address
                        AND b.interface = a.interface
                        ORDER BY a.timestamp DESC
                        LIMIT 2";

    $res = $this->db->select($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return array();
    }

    $in_diff  =  $res[0]['inoctets']  - $res[1]['inoctets'];
    $out_diff  = $res[0]['outoctets'] - $res[1]['outoctets'];
    $time_diff = $res[0]['timestamp'] - $res[1]['timestamp'];

    # Get the bits per second value
    $in  = ceil(($in_diff  / $time_diff)*8);
    $out = ceil(($out_diff / $time_diff)*8);

    $payload = array();
    $payload["in"] = $in;
    $payload["out"] = $out;
    return $payload;

  }

  /**
   * Outputs a PHP Object containing traffic information for the specified network interface.
   * 
   *
   * @param pointer $index_content
   */
  function get_object_interface_traffic($ip, $interface) {
    $ip = $this->db->escape($ip);
    $interface = $this->db->escape($interface);

    # Each 'reset' increment means the buffer was filled that many times
    $snmp_buffer_size = 4294967296;

    $table = "snmp_log";

    $query = "SELECT timestamp, inoctets, outoctets, inresets, outresets
          FROM $table
          WHERE ip = $ip
          AND interface = $interface
          order by timestamp DESC LIMIT 30";
    $res = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return;
    } else {
      $res = array_reverse($res);
    }

    $ds = array();
    #$max = 0;

    # For large data sets, we don't show every single data point. (the client would choke trying to render them)
    $size = sizeof($res);

    # Plot scaling
    if     ($size < 1000)  { $step = 1;          }
    elseif ($size < 2500)  { $step = 5;          }
    elseif ($size < 5000)  { $step = 10;         }
    elseif ($size < 7500)  { $step = 15;         }
    elseif ($size < 10000) {$step = 25;          }
    elseif ($size < 25000) {$step = 75;          }
    elseif ($size < 50000) {$step = 100;         }
    else                   { $step = $size/1000; }

    
    

    for ($i = 1; $i < $size; $i += $step) {
      $in_row  = ($res[$i]['inoctets']  + ($snmp_buffer_size * intval($res[$i]['inresets'])));
      $out_row = ($res[$i]['outoctets'] + ($snmp_buffer_size * intval($res[$i]['outresets'])));


      if ($i >= $step) {
        
        $in_prev  = ($res[$i-$step]['inoctets'] +  ($snmp_buffer_size * intval($res[$i-$step]['inresets'])));
        $out_prev = ($res[$i-$step]['outoctets'] + ($snmp_buffer_size * intval($res[$i-$step]['outresets'])));

        $time_delta = ($res[$i]['timestamp'] - $res[$i-$step]['timestamp']);
      } else {
        $in_prev = 0; $out_prev = 0; $time_delta = $res[$i]['timestamp'];
      }

      # SNMP Measures traffic in octets, not bytes
      $in_delta  = round(8*($in_row - $in_prev)   / $time_delta);
      $out_delta = round(8*($out_row - $out_prev) / $time_delta);

      array_push($ds, array('in' => $in_delta, 'out' => $out_delta, 'date' => date("M j, Y G:i:s", $res[$i]['timestamp']),
            'timestamp' => $res[$i]['timestamp']));

      #$max = max($max, ceil($in_delta), ceil($out_delta));
    }


    require_class($this->module, "snmp_interface_manager");
    $ifm = new SNMP_Interface_Manager();
    $ifm->pop($ip, $interface);

    $object = array();
    $object['device_name'] = $ifm->get('device_name');
    $object['description'] = $ifm->get('description');
    $object['points'] = $ds;
    $object['step'] = $step;
    $object['size'] = $size;
    
    return $object;
  }
  
  function get_json_interface_traffic() {
    print json_encode($this->get_object_interface_traffic($_GET['ip'], $_GET['interface']), true);
  }

  /**
    * Outputs an XML document containing traffic information for the specified network interface.
    *
    *
    * @param pointer $index_content
    */
  function get_xml_interface_traffic(&$index_content) {
    $information = $this->get_object_interface_traffic($_GET['ip'], $_GET['interface']);
    $parser = new Parser($this->tpl_dir);

    $parser->assign("device_name", $information['device_name']);
    $parser->assign("description", $information['description']);

    $points = $information['points'];
    $parser->assign_by_ref("points", $points);
    $parser->assign("step", $information['step']);
    $parser->assign("size", $information['size']);

    $index_content .= $parser->fetch("xml_traffic.tpl");
    header("Content-type: application/xml");
    return;

  }

  /**
   * Displays the auto-discovered devices dialog.
   *
   * @param pointer $index_content
   * @deprecated
   */
  function manage_snmp_autodiscovered(&$index_content) {
    $query = "SELECT a.*, (SELECT value FROM daemonsconfig WHERE daemon_id = '1' AND var = 'community') AS \"community\" FROM snmpautodiscovered a WHERE a.ip NOT IN (SELECT ip FROM devices)";
    $res = $this->db->select($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return;
    }

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("hosts", $res);
    $index_content .= $parser->fetch("manage_snmp_autodiscovered.tpl");
    return;
  }

  function view_traps(&$index_content) {
    $ip = $_GET['ip'];

    $dm = new device_manager();
    $entry = $dm->fetch_by_ip($ip);
    $traps = $dm->get_traps($ip);

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("traps", $traps);
    $parser->assign("device", $entry);

    $parser->register_modifier("translate_oid", array(&$this, "translate_oid"));

    $index_content .= $parser->fetch("view_traps.tpl");
  }

  function view_trackers(&$index_content) {
    $ip = $_GET['ip'];

    $parser = new Parser($this->tpl_dir);

    $dm = new device_manager();
    $entry = $dm->fetch_by_ip($ip);

    $parser->assign("device", $entry);

    $index_content .= $parser->fetch("view_trackers.tpl");
  }

  function get_ping_log() {
    $srv_id = intval($_GET['id']);
    $query = "SELECT * FROM server_log WHERE srv_id=$srv_id ORDER BY timestamp DESC LIMIT 60";
    $payload = array();
    $payload['ping'] = array();
    $payload['ping'] = $this->db->select($query);
    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      $payload['ping'] = array();
    }
    $payload['ping'] = array_reverse($payload['ping']);
    print json_encode($payload);
  }

  function show_trackers(&$index_content) {
    $ip = $_GET['ip'];

    $parser = new Parser($this->tpl_dir);

    $dm = new device_manager();
    $entry = $dm->fetch_by_ip($ip);

    $parser = new Parser(get_tpl_dir("mod_services"));

    # Handle services from servers table (ICMP, TCP)
    $icmp = $dm->get_services($ip, "ICMP");
    $tcp = $dm->get_services($ip, "TCP");

    if ($icmp == NULL) {
      $icmp = array();
    }
    if ($tcp == NULL) {
      $tcp = array();
    }

    # Handle disk trackers
    $disks = $dm->get_disks($ip);

    $payload = array();
    $payload[$ip]["tcpicmp"] = array();

    $all_trackers = array_merge($tcp, $icmp);

    foreach ($all_trackers as $t) {
      array_push($payload[$ip]["tcpicmp"], $t);
    }



    $payload[$ip]["disk"] = $disks;

    $parser->assign("trackers", $payload);

    $index_content .= $parser->fetch("all_services.tpl");
    return;

  }

  function get_xml_snmp_devices(&$index_content) {
    $_GET['root_tpl'] = "blank";

    $query = "SELECT id, ip_address AS \"ip\", label AS \"name\"
    FROM devices
    WHERE enable_snmp = true
    ORDER BY name ASC";
    $res  = $this->db->select($query);


    if (DB_QUERY_ERROR == $res) {
      return;
    } elseif (DB_NO_RESULT == $res) {
      $res = NULL;
    }

    $size = sizeof($res);

    for($i = 0; $i < $size; $i++) {
      $query = "SELECT name, description, interface
      FROM interfaces
      WHERE device_id = ".$this->db->escape($res[$i]['id'])."
      AND enable_logging = true
      ORDER BY id ASC";
      $sub_res = $this->db->select($query);

      if ((DB_NO_RESULT == $sub_res) || (DB_QUERY_ERROR == $sub_res)) {
        #$res[$i]['interfaces'] = array();
        unset($res[$i]);
        continue;
      } else {
        $res[$i]['interfaces'] = $sub_res;
      }
    }


    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("devices", $res);
    $index_content .= $parser->fetch("xml_snmp_devices.tpl");
    header("Content-type: application/xml");

  }

  function manage_mibs(&$index_content) {
    require_class($this->module, "mib_manager");
    $mm = new MIB_Manager();

    $mibs = $mm->get_all_MIBs();

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("mibs", $mibs);
    $index_content .= $parser->fetch("manage_mibs.tpl");
  }

  function form_upload_mib(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $index_content .= $parser->fetch("form_upload_mib.tpl");
  }

  function process_upload_mib() {
    require_class($this->module, "mib_manager");
    $mm = new MIB_Manager();

    if ($mm->upload_mib()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to upload MIB");
    }
  }

  function view_mib_file(&$index_content) {
    # We let the MIB manager handle sandboxing for security purposes
    require_class($this->module, "mib_manager");
    $mm = new MIB_Manager();

    if ($content = $mm->get_mib_content(urldecode($_GET['mib_file']))) {
      $parser = new Parser($this->tpl_dir);
      $parser->assign("file_content", $content);
      $index_content .= $parser->fetch("view_mib_content.tpl");
    } else {
      $this->err->err_from_string("Unable to retrieve content for file " . urldecode($_GET['mib_file']));
    }
    return;
  }

  function delete_mib(&$index_content) {
    # We let the MIB manager handle sandboxing for security purposes
    require_class($this->module, "mib_manager");
    $mm = new MIB_Manager();

    if ($mm->delete_mib(urldecode($_GET['mib_file']))) {
      $index_content .= message_bar("MIB file has been deleted successfully.");
    }
    return $this->manage_mibs($index_content);
  }

  /**
   *
   *
   */
  function snmp_walk() {
    require_class($this->module, "mib_manager");
    $mm = new MIB_Manager();

    $domain = ((strcmp($_GET['domain'], "enterprises") == 0) ? "enterprises" : "iso");
    $mib = $mm->browse_mib($_GET['id'], $domain);

    #$parser->register_function("render", array(&$this, "_render_datatype"));

    print json_encode($mib);
  }



  function _render_datatype($params) {

    $type = trim(strtoupper($params['type']));
    $payload = $params['payload'];


    switch ($type) {
      case "TIMETICKS":       return "<img src=\"assets/icons/mib_timetick.gif\" width=\"16\" height=\"16\" align=\"absmiddle\" border=\"0\" /> " . $payload;
      case "NETWORK ADDRESS": return "<img src=\"assets/icons/mib_networkinterface.gif\" width=\"16\" height=\"16\" align=\"absmiddle\" border=\"0\" /> " . $payload;
      case "IPADDRESS":       return "<a href=\"#\" onClick=\"parent.location='?module=mod_layout&action=render_section&layout=network&ip=" . $payload . "&store_request=2';\"><img src=\"assets/icons/device.gif\" width=\"16\" height=\"16\" align=\"absmiddle\" border=\"0\" /></a> <a title=\"" . $payload ."\" href=\"#\" onClick=\"parent.location='?module=mod_layout&action=render_section&layout=network&ip=" . $payload . "&store_request=2';\">" . $payload;
      case "GAUGE32":         return "<img src=\"assets/icons/mib_gauge.gif\" width=\"16\" height=\"16\" align=\"absmiddle\" border=\"0\" /> " . $payload;
      case "INTEGER":         return "<span class=\"blue\">" . $payload . "</span>";
      case "OID":             return "<a href=\"javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=" . urlencode($payload) . "&root_tpl=blank_panel&class=white');\"><strong>[ <img src=\"assets/icons/mibtree.gif\" width=\"16\" height=\"16\" align=\"absmiddle\" border=\"0\" /> ]</strong></a> <span class=\"gray2\">" . $payload ."</span>";
      case "STRING":          return "<span class=\"green\">" . $payload ."</span>";
      case "HEX-STRING":      return "<span class=\"pink\">" . $payload . "</span> ";
      case "COUNTER32":       return "<span class=\"gray\">" . $payload ."</span>";
      default:                return "<span class=\"gray2\">" . $payload . "</span>";
    }
  }


  /**
   * Turns On/Off OID storage mechanism for snmptrapd
   *
   * @param pointer $index_content
   */
  function store_trap_oid(&$index_content) {
    $value = $_GET['val'];
    $oid = $_GET['oid'];
    $ip = $_GET['ip'];

    if ((strcmp($value, 't') <> 0) && (strcmp($value, 'f') <> 0)) {
      $this->err->err_from_string("Invalid value specified");
      return;
    }

    $query = "UPDATE snmpoids SET store = " . $this->db->escape($value) . "
    WHERE snmpoid = " . $this->db->escape($oid) . "
    AND ip = " . $this->db->escape($ip);

    if ($this->db->update($query) != DB_QUERY_ERROR) {
      $parser = new Parser($this->tpl_dir);
      $parser->assign("item", "Monitor");
      $parser->assign("action_taken", ($value == 't' ? "Turned On" : "Turned Off"));
      $parser->assign("reload_tree", FALSE);
      $index_content .= $parser->fetch("item_modified.tpl");
    }

  }

  function translate_oid($oid) {
    if ($this->oid_cache[$oid]) {
      return $this->oid_cache[$oid];
    }
    
    $query = "SELECT trap_name AS name FROM snmp_traps_trans WHERE trap_oid ILIKE " . $this->db->escape($oid . '%');
    $this->db->commit_transaction();
    $this->db->enable_caching();
    $res = $this->db->get_row($query);
    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      #return $oid;
      require_class($this->module, "mib_manager");
      $name = MIB_Manager::translate_oid_name($oid);
      
      if (strcmp($name, "") <> 0) {
        $query = "INSERT INTO snmp_traps_trans(trap_oid, trap_name)
        VALUES(" . $this->db->escape($oid) . ", " . $this->db->escape($name) . ")";
        $this->db->insert($query);
        $this->oid_cache[$oid] = $name;
        return $name;
      } else {
        $this->oid_cache[$oid] = $oid;
        return $oid;
      }
      
      #return $name ? $name : $oid;
    }
    $this->oid_cache[$oid] = $res['name'];
    return $res['name'];
  }

  /**
   * Creates a note
   *
   * @param pointer $index_content
   */
  function process_create_note(&$index_content) {
    $GLOBALS['json_post'] = json_decode(file_get_contents("php://input"), true);
    $GLOBALS['json_post']['owner_id'] = $_SESSION['id'];

    require_class($this->module, "note_manager");
    $nm = new Note_Manager();

    if ($nm->insert()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to create note");
    }

  }

  /**
   * Updates a note
   *
   * @param pointer $index_content
   */
  function process_update_note(&$index_content) {
    $GLOBALS['json_post'] = json_decode(file_get_contents("php://input"), true);
    $GLOBALS['json_post']['owner_id'] = $_SESSION['id'];
    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_code(400, "Invalid note identifier. Aborting.");
    }

    require_class($this->module, "note_manager");
    $nm = new Note_Manager();

    if ($nm->update($id)) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to update note");
    }
  }

  /**
   * Deletes a note
   *
   * @param pointer $index_content
   */
  function process_delete_notes() {
    if (is_array($_POST['ids'])) {
      $seq = '';
      foreach($_POST['ids'] as $current) {
        $seq .= $this->db->escape($current) . ", ";
      }
      $query = "DELETE FROM devices_notes WHERE id IN (" . substr($seq, 0, strlen($seq)-2) . ")";

      $res = $this->db->delete($query);
      if ((DB_QUERY_ERROR == $res) || (DB_MISC_ERROR == $res)) {
        $this->err->err_from_code(400, "Unable to delete notes");
      }
    }
    print "{}";
  }

  /**
   * Displays the notes-management dialog for the specified device
   *
   * @param pointer $index_content
   */
  function manage_device_notes(&$index_content) {

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("Invalid device identifier. Aborting.");
      return;
    }

    require_class($this->module, "note_manager");
    $nm = new Note_Manager();

    $notes = $nm->fetch_by_device($id);

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("notes", $notes);
    $parser->register_modifier("resolve_user_id", array(&$this, "_resolve_user_id"));
    $index_content .= $parser->fetch("manage_device_notes.tpl");
  }

  /**
   * Resolves a user account ID in first/last name
   *
   * @param pointer $params
   */
  function _resolve_user_id($id) {

    if ($id <= 0) {
      return "N/A";
    }

    $query = "SELECT (last_name || ', ' || first_name) AS name FROM users WHERE id = $id";
    $res = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return "Unknown";
    }

    return $res['name'];
  }

  /**
   * Translates an SNMP OID into its human-readable structure definition
   *
   * @param pointer $index_content
   */
  function translate_snmp_oid(&$index_content) {
    $oid = trim($_GET['oid']);

    require_class($this->module, "mib_manager");
    $mm = new MIB_Manager();

    $desc = $mm->translate_oid($oid);

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("desc", $desc);


    $index_content .= $parser->fetch("oid_info.tpl");
  }


  /**
   * Displays the OID-watcher creation form
   *
   * @param pointer $index_content
   */
  function form_create_oid_watcher(&$index_content) {
    require_class($this->module, "mib_manager");
    require_class($this->module, "oid_watcher_manager");

    $mm  = new MIB_Manager();
    $owm = new OID_Watcher_Manager();

    $_GET['oid'] = $mm->translate_oid_numeric($_GET['oid_txt']);

    if ($owm->oid_already_tracked($_GET['device_id'], $_GET['oid'])) {
      $this->err->err_from_string("This OID is already being tracked against this device.");
      return FALSE;
    }

    $parser = new Parser($this->tpl_dir);
    $parser->set_params($owm->params);
    $index_content .= $parser->fetch("form_create_oid_watcher.tpl");
    return TRUE;
  }

  /**
   * Display the OID-watcher edit form
   *
   * @param pointer $index_content
   */
  function form_edit_oid_watcher(&$index_content) {

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("Invalid OID identifier specified. Aborting.");
      return FALSE;
    }

    require_class($this->module, "oid_watcher_manager");
    $owm = new OID_Watcher_Manager();

    if (!$owm->fetch($id)) {
      $this->err->err_from_string("Unable to retrieve the specified OID Tracker.");
      return FALSE;
    }

    $parser = new Parser($this->tpl_dir);
    $parser->set_params($owm->params);
    $parser->assign_by_ref("owm", $owm);
    $index_content .= $parser->fetch("form_edit_oid_watcher.tpl");
    return TRUE;
  }

  /**
   * Creates a new OID-watcher
   *
   * @param pointer $index_content
   */
  function process_create_oid_watcher(&$index_content) {
    require_class($this->module, "oid_watcher_manager");
    $owm = new OID_Watcher_Manager();

    $input = json_decode(file_get_contents("php://input"), true);

    if (is_array($input['oids'])) {
      // To support multiple, we're going to do a little hack where we overwrite
      // json_post with the current attributes and write it... Kind of hacky
      // but we're working around this ancient framework and it's just easier.

      foreach ($input['oids'] as $oid) {
        $GLOBALS['json_post'] = $input;
        $GLOBALS['json_post']['datatype'] = $oid['datatype'];
        $GLOBALS['json_post']['type'] = $oid['type'];
        $GLOBALS['json_post']['oid'] = $oid['id'];
        $GLOBALS['json_post']['label'] = $oid['label'];
        if (!$owm->insert()) {
          $this->err->err_from_code(400, "Unable to create OID tracker");
          return;
        }
      }
      print '{}';

    } else {
      $GLOBALS['json_post'] = $input;

      if ($owm->insert()) {
        print '{}';
      } else {
        $this->err->err_from_code(400, "Unable to create OID tracker");
      }
    }
    $ms = require_module("mod_settings");
    $ms->_sig_procmond("restart oidmond");
  }

  /**
   * Updates an existing OID-watcher
   *
   * @param pointer $index_content
   */
  function process_update_oid_watcher(&$index_content) {
    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("Invalid OID identifier. Aborting.");
      return FALSE;
    }

    require_class($this->module, "oid_watcher_manager");
    $owm = new OID_Watcher_Manager();

    if ($owm->update($id)) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to update OID tracker");
    }
  }

  /**
   * Deletes an OID-watcher
   *
   * @param pointer $index_content
   */
  function process_delete_oid_watcher(&$index_content) {
    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("Invalid OID identifier. Aborting.");
      return FALSE;
    }

    require_class($this->module, "oid_watcher_manager");
    $owm = new OID_Watcher_Manager();

    if (!$owm->delete($id)) {
      $this->err->err_from_code(400, "Unable to delete watcher");
    } else {
      print '{}';
    }
  }

  /**
   * Displays the management list for OID-watchers
   *
   * @param pointer $index_content
   */
  function manage_oid_watchers(&$index_content) {
    $id = $_GET['id'];

    if ($id <= 0) {
      $this->err->err_from_string("Invalid Device identifier. Aborting.");
      return;
    }

    require_class($this->module, "oid_watcher_manager");
    $owm = new OID_Watcher_Manager();

    $oids = $owm->get_oids_by_device($id);
    

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("oids", $oids);
    $index_content .= $parser->fetch("manage_oid_watchers.tpl");
    return;
  }
  
  /**
   * Outputs an XML document describing the evolution of the specified OID's
   * values over time.
   *
   * @param pointer $index_content 
   */
  function get_xml_oid_values(&$index_content) {
    $oid_id = intval($_GET['oid_id']);

    if ($oid_id <= 0) {
      $this->err->err_from_code(400, "Invalid Object (OID) identifier specified.");
      return;
    }

    require_class($this->module, "oid_watcher_manager");
    $oid = new OID_Watcher_Manager();
    if (!$oid->fetch($oid_id)) {
      $this->err->err_from_code(400, "Unable to retrieve the specified OID. Aborting.");
      return;
    }

    #$start_ts = (intval($_GET['start_ts']) > 0) ? intval($_GET['start_ts']) : time() - TIME_HOUR*.5;
    #$end_ts   = (intval($_GET['end_ts'])   > 0) ? intval($_GET['end_ts'])   : time();
    
    #$query = "SELECT message, timestamp FROM oid_log WHERE oid_id = " . $this->db->escape($oid_id) . " AND timestamp BETWEEN $start_ts AND $end_ts";
    $query = "SELECT message, timestamp FROM oid_log WHERE oid_id = " . $this->db->escape($oid_id) . " ORDER BY timestamp DESC LIMIT 30";
    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return;
    }
    
    $min = $max = floatval($res[0]['message']);
    $res = array_reverse($res);
    foreach ($res as $current) {
      $min = min(floatval($current['message']), $min);
      $max = max(floatval($current['message']), $max);
    }
    
    if ($min == $max) { $min = floor($min*.99); $max = ceil($max*1.01); }
    if ($min == $max) { --$min; ++$max; }

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("points", $res);
    $parser->assign_by_ref("oid", $oid);
    $parser->assign("min", $min);
    $parser->assign("max", $max);
    $index_content .= $parser->fetch("xml_oid_values.tpl");
    header("Content-type: application/xml");
  }
  
  /**
   * Returns the value associated to the specified OID
   *
   * @param pointer $index_content
   */
  function ajax_get_oid_value(&$index_content) {
    /**
     * Please note that it is possible that at some point, we might
     * need to have the Dashboard object make changes to the values retrieved
     * from OIDs, and they will no longer match the values from this method
     * at that point since the method doesn't query the dashboard manager but
     * instead queries the MIB_Manager object directly. If we ever need the dashboards
     * to present an overloaded get_oid() method, we will need to change this method
     * to use it instead of using MIB_Manager, or the ajax queries will return messed-up
     * values.
     */
    
    # Step 1: Perform input filtering and error-control
    $oid = $_GET['oid'];
    $id = $_GET['id'];
    
    $dev = new Device_Manager();
    $dev->fetch($id);
    
    require_class($this->module, "mib_manager");
    $mm = new MIB_Manager();

    if ($_GET['src'] == "flash") {
      $index_content .= "oidVal=";
    }
    
    
    $index_content .= $mm->snmp_get($dev->get('ip_address'), $dev->get('snmp_community'), $oid);
    return;
      
  }
  
  /**
   * Retrieves the contents of an OID array or a MIB walk against a device
   *
   * @param pointer $index_content
   */
  function ajax_snmp_walk(&$index_content) {
    # Step 1: Perform input filtering and error-control
    $oid = $_GET['oid'];
    $id = $_GET['id'];
    
    
    
    require_class($this->module, "mib_manager");
    $mm = new MIB_Manager();

    $vals = $mm->browse_mib($id, $oid);
    $vals = $vals[0];
    unset($vals['mib']);


    foreach($vals as $current) {
      $index_content .= $current[0]['payload'] . "\n";
    }
    #$index_content .= $mm->snmp_get($ip, $community, $oid);
    return;
  }
  
  function alerts_by_ip() {
    $query = "SELECT ap.trigger_timestamp, ap.dispatch_timestamp, ap.retries_processed,
               ap.parsed_alert_message, ap.sent, u.first_name, u.last_name, u.email,
               am.name AS \"media\", ah.required_retries, ah.trigger_id,
               at.reference_table_name, at.reference_pkey_val
               FROM alert_pending ap, alert_handlers ah, alert_medias am, alert_triggers at, users u
               WHERE ah.id = ap.handler_id
               AND am.id = ah.media_id
               AND at.trigger_id = ah.trigger_id
               AND u.id = ah.user_id
               AND at.reference_table_name != 'arptable'
               AND ap.trigger_timestamp > extract(epoch from 'now'::timestamp - '1 day'::interval)
               ORDER BY ap.trigger_timestamp DESC
               LIMIT 14";

    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    }
    
    $ipmap = array();
    
    foreach ($res as &$alert) {
      if ($alert['reference_pkey_val'] && $alert['reference_table_name']) {
        $query = "SELECT s.*, d.id device_id FROM " . $alert['reference_table_name'] . " s, devices d 
                  WHERE srv_id=" . $alert['reference_pkey_val'] . "
                  AND d.ip_address = s.ip";
        $service = $this->db->select($query);

        $alert['service'] = $service[0];

        if ($ipmap[$service[0]['ip']]) {
          array_push($ipmap[$service[0]['ip']], $alert);
        } else {
          $ipmap[$service[0]['ip']] = array($alert);
        }
      }
    }
    return $ipmap;
  }
  /**
   * Returns an XML document listing all devices that have associated OID trackers
   *
   * @param pointer $index_content
   */
  function xml_devs_with_oids(&$index_content) {
    require_class($this->module, "oid_watcher_manager");
    $own = new OID_Watcher_Manager();
    
    $devs = $own->get_devices_with_oids();
    
    if (FALSE === $devs) {
      $devs = NULL;
    } else {
      for ($i = 0; $i < sizeof($devs); $i++) {
        $oids = $own->get_logging_oids_by_device($devs[$i]['id']);
        if ($oids == NULL) {
          unset($devs[$i]);
        } else {
          $devs[$i]['oids'] = $oids;
          foreach($devs[$i]['oids'] as $key => $val) {
            $devs[$i]['oids'][$key]['cat'] = $own->_get_datatype_category($devs[$i]['oids'][$key]['datatype']);
          }
        }
      }
    }
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("devs", $devs);
    $index_content .= $parser->fetch("xml_devs_with_oids.tpl");
    header("Content-type: application/xml");
    return;
  }
  
  /**
   * Returns a JS data-structure of all registered SNMP devices, used
   * by netmon 6
         *
   * @param pointer $index_content
   */
  function json_get_devices_dashboard() {
    require_class($this->module, "device_manager");

    $dashjson = json_decode(file_get_contents("/apache/dashboards.json"), true);

    $dm = new Device_Manager();
    $devices = $dm->get_devices_array();
    
    $memcache = new Memcache;
    $memcache->connect("localhost");

    $ipalerts = $this->alerts_by_ip();
    
    foreach ($devices as &$device) {
      $mcAlertKey = $device["ip_address"] . "-alerts";
      $dalerts = $memcache->get($mcAlertKey);
      if ($dalerts) {
        $device["alerts"] = json_decode($dalerts, true);
      } else {
        $device["alerts"] = array();
      }

      $device["hasPanelError"] = $this->add_interface_info($device, $dashjson, $memcache, false);

      
      $downServers = $this->db->select("SELECT count(*) FROM servers WHERE ip='" . $device['ip_address'] . "' AND status='DOWN'");
      if (intval($downServers[0]['count']) > 0) {
        array_push($device["alerts"], array("subject" => $downServers[0]['count'] . " tracker(s) down"));
      }
      
      if ($ipalerts[$device["ip_address"]]) {
        $device["recent_alerts"] = $ipalerts[$device["ip_address"]];
      } else {
        $device["recent_alerts"] = array();
      }
    }
    
    $query = "SELECT * FROM device_groups";
    $groups = $this->db->select($query);
    if (!is_array($groups)) {
      $groups = array();
    }
    
    print json_encode(array(
      "devices" => $devices,
      "groups" => $groups
    ));
    return;
  }

  /**
   * Returns a JS data-structure of all registered SNMP devices
   *
   * @param pointer $index_content
   */

  function json_get_devices(&$index_content) {
    require_class($this->module, "device_manager");
    $dm = new Device_Manager();
    
    
    if (strcmp('source', $_REQUEST['node']) == 0) {
      unset($_REQUEST['node']);
    }
    
    if (ereg('dev_([0-9]+)_interfaces', $_REQUEST['node'])) {
      $tree = $dm->get_device_interfaces($_REQUEST['node']);
    } else {
      $devices = $dm->get_all_devices($_REQUEST['node']);
      $groups =  $dm->get_device_groups($_REQUEST['node']);
      $tree = array_merge_recursive($groups, $devices);
    }


    
    $index_content .= json_encode($tree);
    return;
  }
  
  function get_groups() {
    $groups = $this->db->select("SELECT * FROM device_groups");
    if (!$groups) {
      $groups = array();
    }
    print json_encode($groups, true);
  }
  
  /**
   * Updates the group association for a node in the devices tree
   *
   * @param pointer $index_content
   */
  function update_node_group() {
    $gid = intval($_GET['gid']);
    $node = explode('_', $_GET['node_id']);

    if (0 == $gid) {
      $gid = 'NULL';
    } else {
      $gid = $this->db->escape($gid);
    }

    if (strcmp('dev', $node[0]) == 0) {
      $node_id = $this->db->escape(intval($node[1]));
      $query = "UPDATE devices SET group_id = $gid WHERE id = $node_id";
    } else {
      $group = $this->db->escape(intval($node[0]));
      $query = "UPDATE device_groups SET parent_id = $gid WHERE id = $group";
    }

    $res = $this->db->update($query);
    
    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_code(400, "Invalid group information");
    } else {
      print '{}';
    }
  }

  /**
    * Updates group association for multiple nodes in the devices tree
    *
    */
  function update_multiple_node_group() {
    $node_ids =  explode(",", $_GET['node_ids']);
    $gid = intval($_GET['gid']);

    if (0 == $gid) {
      $gid = 'NULL';
    } else {
      $gid = $this->db->escape($gid);
    }

    foreach ($node_ids as $node_id) {
      $node = explode('_', $node_id);

      if (strcmp('dev', $node[0]) == 0) {
        $node_id = $this->db->escape(intval($node[1]));
        $query = "UPDATE devices SET group_id = $gid WHERE id = $node_id";
      } else {
        $group = $this->db->escape(intval($node[1]));
        $query = "UPDATE device_groups SET parent_id = $gid WHERE id = $group";
      }

      $res = $this->db->update($query);

      if (DB_QUERY_ERROR == $res) {
        $this->err->err_from_code(400, "Invalid group information");
        return;
      }
    }

    print '{}';
  }
  
  /**
   * Creates a new device group
   *
   */
  function process_create_device_group() {
    $input = json_decode(file_get_contents("php://input"), true);
    $group = $this->db->escape($input['name']);
    $query = "INSERT INTO device_groups(name) VALUES($group)";
    
    $res = $this->db->insert($query);
    
    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_code(400, "Unable to create device group");
    } else {
      print '{}';
    }
  }
  
  /**
   * Renames a group
   *
   * @param pointer $index_content
   */
  function process_update_device_group(&$index_content) {
    $input = json_decode(file_get_contents("php://input"), true);
    $gid = intval($_GET['gid']);
    
    // Boolean status flag for rename operation.
    $status = TRUE;
    
    
    if ($gid <= 0) {
      $this->err->err_from_code(400, "Invalid group identifier");
    }
    
    $query = "UPDATE device_groups SET name=" . $this->db->escape($input['name']) . " WHERE id = " . $this->db->escape($gid);
    
    $res = $this->db->update($query);
    
    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_code(400, "Database update error");
    }
    
    if ($status) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to update device group");
    }
  }
  
  function process_delete_device_group() {
    $gid = intval($_GET['gid']);
    
    
    if ($gid <= 0) {
      $this->err->err_from_code(400, "Invalid group identifier specified.");
    }
    
    $query = "UPDATE devices SET group_id = NULL WHERE group_id = " . $this->db->escape($gid);
    $res = $this->db->update($query);
    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_code(400, "Unable move child devices to the root of the tree.");
    }
    
    $query = "DELETE FROM device_groups WHERE id = " . $this->db->escape($gid);
    $res = $this->db->update($query);
    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_code(400, "Unable to delete device group.");
    }
    print '{}';
  }
  
  function get_ram() {
    require_class($this->module, "ram_manager");
    require_class($this->module, "device_manager");
    $dm = new Device_Manager();
    $device = $dm->fetch($_GET['device_id']);

    $ram = new Ram_Manager();
    print json_encode($ram->get($device), true);
  }

  function get_windows_services() {
    require_class($this->module, "device_manager");
    require_class($this->module, "mib_manager");

    $ip = $_GET['ip'];
    $dm = new Device_Manager();
    $mm = new MIB_Manager();

    $device = $dm->fetch_by_ip($_GET['ip']);
    $community = $device['snmp_community'];

    $services = $mm->snmp_table($ip, $community,  "LanMgr-Mib-II-MIB::svSvcTable");

    print json_encode($services, true);
    return;
  }

  function get_windows_processes() {
    require_class($this->module, "device_manager");
    require_class($this->module, "mib_manager");
    $ip = $_GET['ip'];

    $dm = new Device_Manager();
    $mm = new MIB_Manager();

    $device = $dm->fetch_by_ip($_GET['ip']);
    $community = $device['snmp_community'];

    $processes = $mm->snmp_table($ip, $community, "HOST-RESOURCES-MIB::hrSWRunTable");
    $perf1 = $mm->snmp_table($ip, $community, "HOST-RESOURCES-MIB::hrSWRunPerfTable");
    # Wait one second before retrieving the performance metrics again, which will allow us
    # to determine CPU utilization percentage for each process.
    sleep(2);
    $perf2 = $mm->snmp_table($ip, $community, "HOST-RESOURCES-MIB::hrSWRunPerfTable");

    $ret = array();

    foreach($perf2 as $key => $val) {
      $perf2[$key]['CPU'] = ($perf2[$key]['CPU'] - $perf1[$key]['CPU'])/20;
    }
    unset($perf1);

    # Disable the CPU column if we're dealing with the crappy Windows SNMP server
    $windows_sucks = TRUE;
    foreach($perf2 as $key => $val) {
      if (intval($val['CPU']) > 0) {
        $windows_sucks = FALSE;
      }
    }

    if (TRUE == $windows_sucks) {
      foreach($perf2 as $key => $val) {
        # The template will know what to do with this.
        $perf2[$key]['CPU'] = -1;
      }
    }


    foreach($processes as $key => $val) {
      foreach($perf2[$key] as $inner_key => $inner_val) {
        $processes[$key][$inner_key] = $inner_val;
      }
    }

    $res = $processes;
    $mandatory_keys = array("Mem", "CPU", "Name");
    foreach($res as $rkey => $rval) {
      # Discard processes with missing info   
      foreach($mandatory_keys as $key) {
        if (strcmp($rval[$key], "") == 0) {
          unset($res[$rkey]);
          continue 2;
        }
      }

      $res[$rkey]['Mem'] = eregi_replace("([0-9]+).*", "\\1", $rval['Mem']);
    }

    uasort($res, array(&$this, "sort_processes"));
    print json_encode($res, true);
  }

  # Attempts to resolve the hostname associated to an IP.
  function resolve_ip($ip) {

    $q_ip = $this->db->escape($ip);

    # Allocate some space for our cache
    if (!is_array($this->cache['ips'])) {
      $this->cache['ips'] = array();
    }

    if ($this->cache['ips'][$ip]) {
      $this->debugger->add_hit("$ip -> " . $this->cache['ips'][$ip]['hostname'] . " (cached)");
      return $this->cache['ips'][$ip]['hostname'];
    }

    $query = "SELECT hostname, ip, host_name_type
          FROM hosts
          WHERE ip = $q_ip
          ORDER BY timestamp DESC";

    $this->db->enable_caching();
    $res = $this->db->select($query);
    $this->db->restore_previous_state();


    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      $this->cache['ips'][$ip] = array("hostname" => $ip);
      return $ip;
    }

    foreach($res as $current) {
      if ($current['host_name_type'] == 'CUSTOM') {
        $this->cache['ips'][$ip] = array("hostname" => $current['hostname']);
        return $current['hostname'];
      }
    }


    $this->cache['ips'][$ip] = array("hostname" => $res[0]['hostname']);
    $this->debugger->add_hit("$ip -> " . $res[0]['hostname']);
    return $res[0]['hostname'];

  }

}


?>
