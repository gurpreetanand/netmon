<?php


class OID_Watcher_Manager extends MadnetElement {

  /**
    * Database table associated with this subclass
    *
    * @var $table
    * @access protected
    */
  var $table = "oids";
  /**
    * Name of the primary key in the table
    *
    * @var string $pkey
    * @access protected
    */
  var $pkey = "id";/**
    * Name of the module this MadnetElement subclass belongs to
    *
    * @var string $module
    * @access protected
    */
  var $module = "mod_devices";
  /**
    * Name of the class containing the business logic for this Element
    *
    * @var string $element
    * @access protected
    */
  var $element = __CLASS__;

  /**
    * Meta-structure (see MadnetElement for more info)
    *
    * @var hashtable $meta
    * @access private
    */
  var $meta;

  /**
   * Initialization Method
   *
   */
  function init() {
    $this->params->add_primitive("device_id",      "integer", TRUE,  "Device");
    $this->params->add_primitive("oid",            "string",  TRUE,  "OID");
    $this->params->add_primitive("interval",       "integer", TRUE,  "Polling Interval");
    $this->params->add_primitive("enable_logging", "pg_bool", TRUE, "Historical Logging");
    $this->params->add_primitive("homedisplay",    "pg_bool", TRUE, "Display on Home Dashboard");
    $this->params->add_primitive("label",          "string",  TRUE,  "Label");
    $this->params->add_primitive("datatype",       "string",  TRUE,  "OID Datatype");
  }

  /**
   * We might not need this.
   *
  function get_all_oids() {

  }
  */

  /**
   * Returns a data-structure representing all OID trackers
   * associated with the specified device
   *
   * @param integer $id
   * @return mixed
   */
  function get_oids_by_device($id) {
    $query = "SELECT a.*, b.ip_address, b.label AS device_label
    FROM oids a, devices b
    WHERE a.device_id = b.id
    AND b.id = " . $this->db->escape($id) . "
    ORDER BY a.label ASC, a.oid ASC";

    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    return $res;
  }
  
  /**
   * Returns a data-structure representing all OID trackers
   * associated with the specified device
   *
   * @param integer $id
   * @return mixed
   */
  function get_logging_oids_by_device($id) {
    $query = "SELECT a.*, b.ip_address, b.label AS device_label
    FROM oids a, devices b
    WHERE a.device_id = b.id
    AND a.enable_logging  = true
    AND b.id = " . $this->db->escape($id) . "
    ORDER BY a.label ASC, a.oid ASC";

    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    }
    return $res;
  }

  /**
   * Returns TRUE if an OID is already being tracked, FALSE otherwise
   *
   * @param integer $device_id
   * @param integer $oid
   * @return bool
   */
  function oid_already_tracked($device_id, $oid) {
    $query = "SELECT * FROM oids WHERE device_id = " . $this->db->escape($device_id) . "
    AND oid = " . $this->db->escape($oid);

    $res = $this->db->select($query);

    return (DB_NO_RESULT != $res);
  }

  /**
   * Retrieves the category the datatype of the newly-fetched
   * OID, as well as the last received status message and timestamp
   * from oidmond regarding the OID.
   *
   * @param integer $id
   * @return bool
   */
  function post_fetch($id) {
    $this->debugger->add_hit("Post-fetch reached");
    require_class($this->module, "mib_manager");

    $this->params->setval("category", $this->_get_datatype_category($this->get('datatype')));
    $this->params->setVal("oid_name", MIB_Manager::translate_oid_name($this->get('oid')));

    $query = "SELECT message, timestamp FROM oid_log
    WHERE oid_id = " . $this->db->escape($id) . "
    ORDER BY id DESC LIMIT 1";

    $this->map($query);
    
    $query = "SELECT label AS device_label, ip_address FROM devices WHERE id = " . $this->get('device_id');
    $this->map($query);
    return TRUE;
  }

  /**
   * Helper method used to determine how to handle OID
   * alerts and how to render OIDs based on their datatype.
   * The 2 OID categories available are "numeric" and "string".
   *
   * @param string $type
   * @return string
   */
  function _get_datatype_category($type) {
    switch(strtolower(trim($type))) {
      case "integer":
      case "counter32":
      case "gauge32":
      case "enum":
        return "numeric";
      default:
        return "string";
    }
  }
  
  function pre_insert($id = NULL) {
    $cmd = "sudo nice -n2  snmptranslate -Ir -M /usr/share/snmp/mibs:/usr/local/share/mibs/site:/usr/local/share/mibs/ietf:/usr/local/share/mibs/iana:/usr/local/share/mibs/irtf:/usr/local/share/mibs/tubs -PduRc -O qn \"" . addslashes($this->params->primitives['oid']['value']) . "\" 2> /dev/null";
    $result = `$cmd`;
    $result = str_replace("\n", '', $result);
    $this->params->setval("oid", $result);
    if ($this->oid_already_tracked($this->get('device_id'), $result)) {
      $this->err->err_from_code(400, "This OID already has a tracker set up.");
      return FALSE;
    }

    return TRUE;
  }
  
  function post_insert($id = NULL) {
    if ($id == NULL) {
      $id = $this->db->escape($this->meta['pkey_value']);
    } else {
      $id = $this->db->escape($id);
    }
    
    $query = "UPDATE {$this->table} SET timestamp = 0 WHERE {$this->pkey} = $id";
    return $this->db->update($query);
  }
  
  function post_update($id) {
    return $this->post_insert($id);
  }
  
  function pre_delete($id) {
    $this->db->start_transaction();

    $this->db->delete("DELETE FROM oid_log WHERE oid_id = {$id}");

    $query = "DELETE FROM alert_handlers WHERE trigger_id IN (select trigger_id from alert_triggers WHERE reference_table_name = 'oids' AND reference_pkey_val = $id)";
    $this->db->delete($query);

    $query = "DELETE FROM alert_triggers WHERE reference_table_name = 'oids' AND reference_pkey_val = $id";
    $this->db->delete($query);

    $this->db->delete("DELETE FROM alert_pending WHERE sent = '0' AND handler_id NOT IN (SELECT id FROM alert_handlers)");

    return $this->db->delete($query);
  }

  function post_delete($id) {
    return $this->db->commit_transaction();
  }

  function get_devices_with_oids() {
    $query = "SELECT d.ip_address, d.label, d.id 
    FROM devices d, oids o
    WHERE d.id = o.device_id
    AND o.enable_logging = true
    GROUP BY d.id, d.ip_address, d.label
    ORDER BY d.label ASC";
    
    $res = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    
    return $res;
  }
}

?>
