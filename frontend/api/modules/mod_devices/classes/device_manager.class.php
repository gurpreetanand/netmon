<?php


class Device_Manager extends MadnetElement {

  /**
    * Database table associated with this subclass
    *
    * @var $table
    * @access protected
    */
  var $table = "devices";
  /**
    * Name of the primary key in the table
    *
    * @var string $pkey
    * @access protected
    */
  var $pkey = "id";/**
    * Name of the module this MadnetElement subclass belongs to
    *
    * @var string $module
    * @access protected
    */
  var $module = "mod_devices";
  /**
    * Name of the class containing the business logic for this Element
    *
    * @var string $element
    * @access protected
    */
  var $element = __CLASS__;

  /**
    * Meta-structure (see MadnetElement for more info)
    *
    * @var hashtable $meta
    * @access private
    */
  var $meta;

  /**
   * Initialization Method
   *
   */
  function init() {
    $this->params->add_primitive("ip_address",     "ip_address", TRUE, "IP Address",       "IP Address");
    $this->params->add_primitive("label",          "string",     TRUE, "Label",            "Label");
    $this->params->add_primitive("snmp_community", "string",     FALSE, "Community String", "Community String");
    $this->params->add_primitive("interval",       "integer",    FALSE, "Polling Interval", "Polling Interval");
    $this->params->add_primitive("snmp_port",      "integer",    TRUE, "Port",             "Port");
    $this->params->add_primitive("enable_snmp",    "pg_bool",    FALSE, "Enable SNMP",      "Enable SNMP");
    $this->params->add_primitive("enable_netflow", "pg_bool",    TRUE, "Enable Netflow",   "Enable Netflow");
    $this->params->add_primitive("profile",        "string",     TRUE, "Device Type",      "Device Type");
    $this->params->add_primitive("enable_sflow",   "pg_bool",    TRUE, "Enable sFlow",     "Enable sFlow");
    $this->params->add_primitive("show_iface_problems",   "pg_bool",    TRUE, "Show interface errors",     "Show interface errors");
    $this->params->add_primitive("group_id",       "integer",    FALSE, "Group ID",     "Group ID");
  }
  
  function get_device_interfaces($device_id) {
    require_class("core", "parser");
    $dash   = require_module("mod_dashboard");
    $parser = new Parser;
    
    #$id = $this->db->escape($device_id);
    
    $parts = array();
    if (TRUE == ereg('dev_([0-9]+)_interfaces', $device_id, $parts)) {
      $id = $this->db->escape(intval($parts[1]));
    } else {
      return array();
    }
        
    $interfaces = array();
        
    $query = "SELECT a.id, a.interface, a.name, a.description, a.enable_shm, 
    b.ip_address, b.enable_netflow, b.enable_sflow, a.speed, a.mac
    FROM interfaces a, devices b
    WHERE a.device_id = $id
    and b.id = a.device_id
    ORDER BY interface ASC";
    $if_res = $this->db->select($query);
    if (is_array($if_res)) {
      // Transform the interface nodes here
      foreach($if_res as $interface) {
        $tip_body = "Interface: ".$interface['interface'] . " - " . $interface['description'] . "<br />";
        $tip_body .= "Speed: " .  $parser->translate_speed($interface['speed']) . "/s<br />";
        $tip_body .= "Connected to: " . $dash->resolve_max($interface['mac']);
        
        $iface = array('id' => $id . '_iface_' . $interface['interface'],
                 'hrefTarget' => 'pnl_middle',
                 'href' => '?module=mod_devices&action=manage_snmp_interface&ip='.$interface['ip_address'].'&interface='.$interface['interface'],
                 'text' => $interface['interface'] . ': ' . $interface['description'],
                 //'qtipCfg' => array('title' => 'Interface Details:', 'text' => $tip_body),
                'draggable' => false);
        // Icon selection
        if (($interface['enable_shm'] == 't') && ($interface['enable_netflow'] == 't')) {
          $iface['icon'] = '/assets/icons/interface_netflow.gif';
          // Add the extra netflow-specific child nodes
          $iface['children'] = array();
          
          // Bandwidth Activity Graph
          $bandwidth_graph = array('id' => $id . '_bw_graph_'. $interface['interface'],
                       'text' => "Bandwidth Activity Graph",
                       'hrefTarget' => 'pnl_middle',
                       'icon' => '/assets/icons/activity.gif',
                       'href' => '?module=mod_devices&action=manage_snmp_interface&ip='.$interface['ip_address'].'&interface='.$interface['interface'],
                       'leaf' => TRUE,
                       'draggable' => false
          );
          $iface['children'][] = $bandwidth_graph;
          
          // Visual Interface Explorer
          $vie = array('id' => $id . '_vie_' . $interface['interface'],
                 'text' => "Visual Interface Explorer",
                 'hrefTarget' => 'pnl_middle',
                 'icon' => '/assets/icons/activity.gif',
                 'href' => '?module=mod_network&action=mini_vne&device='.$id.'&interface='.$interface['interface'],
                 'leaf' => TRUE,
                 'draggable' => false
          );
          $iface['children'][] = $vie;
          
          // Protocol Activity Graph
          $prot_graph = array('id' => $id.'_proto_'.$interface['interface'],
                    'text' => "Protocol Activity Graph",
                    'hrefTarget' => 'pnl_middle',
                    'icon' => '/assets/icons/activity.gif',
                    'href' => '?module=mod_network&action=view_protocol_graph&device='.$id.'&interface='.$interface['interface'],
                    'leaf' => TRUE,
                    'draggable' => false
          );
          $iface['children'][] = $prot_graph;
          
        } elseif (($interface['enable_shm'] == 't') && ($interface['enable_sflow'] == 't')) {
          $iface['icon'] = '/assets/icons/interface_sflow.gif';
          // Add the extra netflow-specific child nodes
          $iface['children'] = array();
          
          // Bandwidth Activity Graph
          $bandwidth_graph = array('id' => $id . '_bw_graph_'. $interface['interface'],
                       'text' => "Bandwidth Activity Graph",
                       'hrefTarget' => 'pnl_middle',
                       'icon' => '/assets/icons/activity.gif',
                       'href' => '?module=mod_devices&action=manage_snmp_interface&ip='.$interface['ip_address'].'&interface='.$interface['interface'],
                       'leaf' => TRUE,
                       'draggable' => false
          );
          $iface['children'][] = $bandwidth_graph;

          
          // Protocol Activity Graph
          $prot_graph = array('id' => $id.'_proto_'.$interface['interface'],
                    'text' => "Protocol Activity Graph",
                    'hrefTarget' => 'pnl_middle',
                    'icon' => '/assets/icons/activity.gif',
                    'href' => '?module=mod_network&action=view_protocol_graph&device='.$id.'&interface='.$interface['interface'],
                    'leaf' => TRUE,
                    'draggable' => false
          );
          $iface['children'][] = $prot_graph;
        } else {
          $iface['icon'] = '/assets/icons/interface.gif';
          $iface['leaf'] = TRUE;
          
        }
        
        array_push($interfaces, $iface);
      }
      
    }
    
    return $interfaces;
    
  }

  /**
   * Returns a list of all devices (used to display the Devices Tree)
   *
   * @return unknown
   */
  function get_all_devices($group_id = NULL) {
    require_class("core", "parser");
    $dash = require_module("mod_dashboard");
    $parser = new Parser;
    
    $payload = array();
    
    if (NULL != $group_id) {
      $where = "WHERE group_id = " . $this->db->escape($group_id);
    } else {
      $where = "WHERE group_id IS NULL";
    }
    
    $query = "SELECT d.id, d.ip_address, d.label, d.enable_snmp, d.enable_sflow,
    d.enable_netflow, d.sysdescr, d.profile, d.status, s.syslog_id, d.show_iface_problems,
      (SELECT COUNT(id) FROM oids WHERE device_id = d.id) AS oids
    FROM devices d
    LEFT OUTER JOIN syslog_access s ON (s.ip = d.ip_address)
    $where
    ORDER BY index ASC, status ASC, label ASC, ip_address ASC";

    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    } else {
      $devices = array();
      foreach($res as $device) {

        // Transform the device node here
        $node = array('text' => '');
        
        if ($device['status'] == 'down') {
          $node['icon'] = '/assets/icons/device_offline.gif';
          $tip_title = 'No response from this device';
          $node['cls'] = 'x-tree-node-disabled';
          //$node['disabled'] = TRUE;
          $node['draggable'] = TRUE;
        } elseif ($device['enable_netflow'] == 't') {
          $node['icon'] = '/assets/icons/device_netflow.gif';
          $tip_title = 'Netflow-enabled device';
        } elseif ($device['enable_sflow'] == 't') {
          $node['icon'] = '/assets/icons/device_sflow.gif';
          $tip_title = 'sFlow-enabled device';
        } elseif ($device['enable_snmp'] == 't') {
          $node['icon'] = '/assets/icons/device_snmp.gif';
          $tip_title = 'SNMP-enabled device';
        } else {
          $node['icon'] = '/assets/icons/device_dynamic.gif';
          $tip_title = 'Auto-Discovered device';
        }
        
        $node['draggable'] = TRUE;
        $node['id'] = 'dev_' . $device['id'];
        $node['allowDrop'] = FALSE;
        $node['hrefTarget'] = ($device['status'] != 'down') ?  'pnl_middle' : '';
        $node['href'] =  ($device['status'] != 'down') ? '?module=mod_devices&action=dashboard&ip='.$device['ip_address'] : "javascript:parent.parent.pnl_right.showEditor('/?module=mod_devices&action=form_edit_snmp_device&id=".$device['id']."');";
        $tip_body = ($device['label'] ? $device['label'] : $device['sysdescr']);
        $node['text'] .= ($device['label'] ? $device['label'] : substr($device['sysdescr'], 0, 15)) . ' ('.$device['ip_address'].')';
        //$node['qtipCfg'] = array('title' => $tip_title, 'text' => $tip_body);
        
        if ($device['status'] == 'down') {
          $node['text'] .= " <img src='/assets/icons/caution_active.gif' class='icon' />";
        }
        
        #$node['text'] = "<div style='display: inline' title='$tip_title - $tip_body'>".$node['text']."</span>";
        # Add static child nodes for devices
        $node['children'] = array();

        
        # Dashboard:
        $dashboard = array('id'=> $node['id'].'_dash',
                   'hrefTarget' => $node['hrefTarget'],
                   'href' => $node['href'],
                   'text' => "Device Dashboard",
                   'expandable' => FALSE,
                   'leaf' => TRUE,
                   'icon' => '/assets/icons/resource_centre.gif',
                  'draggable' => false);
        array_push($node['children'], $dashboard);
        
        #Device Notes:
        $notes = array('id'=> $node['id'].'_notes',
                 'hrefTarget' => 'pnl_middle',
                 'href' => '?module=mod_devices&action=manage_device_notes&id='.$device['id'],
                'text' => "Device Notes",
                'expandable' => FALSE,
                'leaf' => TRUE,
                 'icon' => '/assets/icons/notepad.gif',
                'draggable' => false);
        array_push($node['children'], $notes);
        
        # Interfaces List
        $interfaces = array('id' => $node['id'] . '_interfaces',
                    'text' => 'Network Interfaces',
                  'icon' => '/assets/icons/interface.gif',
                  'hrefTarget' => 'pnl_middle',
                  'href' => '?module=mod_devices&action=dashboard&profile=snmp&ip=' . $device['ip_address'],
#                  'children' => array(),
                  'draggable' => false
                  );
                  
        array_push($node['children'], $interfaces);    
        
        
        // Syslog
                /*
        if ($device['syslog_id']) {
          $syslog = array('id' => $device['id'].'_syslog',
                  'text' => "Events and Logs",
                  'hrefTarget' => 'pnl_middle',
                  'icon' => '/assets/icons/log.gif',
                  'href' => '?module=mod_syslog&action=view_syslog_data&filter=ip&src=device&id='.$device['syslog_id'],
                  'leaf' => TRUE,
                  'draggable' => false
          );
          $node['children'][] = $syslog;
        }
                */
        // SNMP Walk
        $snmpwalk = array('id' => $device['id'].'_snmpwalk',
                  'text' => 'SNMP Walk (full)',
                  'hrefTarget' => 'pnl_middle',
                  'icon' => '/assets/icons/snmp_walk.gif',
                  'href' => '?module=mod_devices&action=snmp_walk&domain=iso&class=white&id='.$device['id'],
                  'leaf' => TRUE,
                  'draggable' => false
        );
        $node['children'][] = $snmpwalk;
        
        // Enterprise Walk
        $snmpent = array('id' => $device['id'].'_entwalk',
                   'text' => 'SNMP Walk (Enterprise)',
                 'hrefTarget' => 'pnl_middle',
                 'icon' => '/assets/icons/enterprise_mibs.gif',
                 'href' => '?module=mod_devices&action=snmp_walk&domain=enterprises&class=white&id='.$device['id'],
                 'leaf' => TRUE,
                'draggable' => false
        );
        $node['children'][] = $snmpent;
        
        
        
        // OID Trackers
        $trackers = array('id' => $device['id'].'_trackers',
                    'text' => 'SNMP Object (OID) Trackers',
                  'hrefTarget' => 'pnl_middle',
                  'icon' => '/assets/icons/oid_tracker.gif',
                  'href' => '?module=mod_devices&action=manage_oid_watchers&id='.$device['id'],
                  'leaf' => TRUE,
                'draggable' => false
        );
        $node['children'][] = $trackers;
        
        // SNMP Traps
        $traps = array('id' => $device['id'].'_traps',
                   'text' => 'SNMP Trap Messages',
                 'hrefTarget' => 'pnl_middle',
                 'icon' => '/assets/icons/trap.gif',
                 'href' => '?module=mod_devices&action=view_traps&ip='.$device['ip_address'],
                 'leaf' => TRUE,
                'draggable' => false
        );
        $node['children'][] = $traps;


        // Device Trackers
        $trackers = array('id' => $device['id'].'_trackers',
                   'text' => 'Device Trackers',
                 'hrefTarget' => 'pnl_middle',
                 'icon' => '/assets/icons/services.png',
                 'href' => '?module=mod_devices&action=view_trackers&ip='.$device['ip_address'],
                 'leaf' => TRUE,
                'draggable' => false
        );
        $node['children'][] = $trackers;


        // Device Syslog
        /*
        $trackers = array('id' => $device['id'].'_syslog',
                   'text' => 'Syslog Entries',
                 'hrefTarget' => 'pnl_middle',
                 'icon' => '/assets/icons/syslog_severity_2.gif',
                 'href' => '?module=mod_devices&action=view_syslog&ip='.$device['ip_address'],
                 'leaf' => TRUE,
                'draggable' => false
        );
        $node['children'][] = $trackers;
        */

        
        
        array_push($payload, $node);
      }
      return $payload;
    }
  }

    /**
     * Returns a list of all devices (used to display the Devices Tree)
     *
     * @return unknown
     */
    function get_devices_array($group_id = NULL) {
      require_class("core", "parser");
      $dash = require_module("mod_dashboard");
      $parser = new Parser;

      $payload = array();

      if (NULL != $group_id) {
        $where = "WHERE group_id = " . $this->db->escape($group_id);
      }

      $query = "SELECT d.id, d.ip_address, d.label, d.enable_snmp, d.enable_sflow,
        d.enable_netflow, d.sysdescr, d.profile, d.status, s.syslog_id, d.show_iface_problems,
        (SELECT COUNT(id) FROM oids WHERE device_id = d.id) AS oids,
        (SELECT name FROM device_groups WHERE id = d.group_id) AS group_name,
        d.group_id
        FROM devices d
        LEFT OUTER JOIN syslog_access s ON (s.ip = d.ip_address)
        $where
        ORDER BY index ASC, status ASC, label ASC, ip_address ASC";

      $res = $this->db->select($query);

      if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
        return array();
      } else {
        foreach($res as $device) {
          array_push($payload, $device);
        }
        return $payload;
      }
    }
  
  function get_device_groups($parent_id = NULL) {
    if (NULL != $parent_id) {
      $where = "WHERE parent_id = " . $this->db->escape($parent_id);
    } else {
      $where = "WHERE parent_id IS NULL";
    }
    
    $query = "SELECT * FROM device_groups $where ORDER BY index ASC, id ASC";
    
    $res = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    }
    
    $groups = array();
    
    foreach($res as $group) {
      $node = array('id' => $group['id'],
              'text' => $group['name'],
              'cls' => 'folder',
              //'qtipCfg' => array('title' => $group['name'], 'text' => 'Device Group'),
              'hrefTarget' => 'pnl_middle',
              'href' => '?module=mod_devices&action=manage_device_group&gid=' . $group['id']
      );
      $groups[] = $node;
    }
    return $groups;
  }
  

  /**
   * Returns a data structure containing all the interfaces linked to a device.
   * The device's IP address must be specified through $_GET['ip_address']
   *
   * @return mixed
   */
  function get_all_device_interfaces($ip = NULL) {

    if (NULL == $ip) {
      $ip = $_GET['ip'];
    }
    $ip = $this->db->escape($ip);

    $query = "SELECT a.* FROM interfaces a, devices b where a.device_id = b.id and b.ip_address = $ip ORDER BY interface ASC";


    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }

    return $res;
  }

  /**
   * Collects device data based on the IP address of the SNMP-enabled device.
   *
   * @param string $ip
   * @return mixed
   */
  function fetch_by_ip($ip) {
    $ip = $this->db->escape($ip);

    #$query = "SELECT * FROM devices WHERE ip_address = $ip";
    $query = "SELECT a.*, b.mac FROM devices a LEFT OUTER JOIN arptable b ON a.ip_address = b.ip WHERE ip_address = $ip";

    $res = $this->db->get_row($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return FALSE;
    } else {
      foreach($res as $key => $value) {
        $this->params->setval($key, $value);
      }
    }

    return $res;

  }
  
  /*
  function __check_device_limit() {
    $query = 'SELECT COUNT(DISTINCT a.ip_address) AS "devices", b.devices AS "limit" FROM devices a, netmon b WHERE a.enable_snmp = true GROUP BY b.devices';
    
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    
    if ($res['devices'] >= $res['limit']) {
      # if the device we're adding is already in the list, return true.
      $ip = $this->db->escape($this->params->primitives['ip_address']['value']);
      $query = "SELECT ($ip IN (SELECT ip_address FROM devices WHERE enable_snmp = true GROUP BY ip_address)) AS \"listed\"";
      
      if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res) || ($res['listed'] == 'f')) {
        return FALSE;
      } else {
        return TRUE;
      }
    }
    
    return TRUE;
  }
  */

  function pre_insert($id = null) {
    $success = TRUE;
    $ip = $this->db->escape($this->params->primitives['ip_address']['value']);
    
    # Check for device restrictions only if we are enabling SNMP
    $this->debugger->add_hit("enable_snmp status: " . $this->get("enable_snmp"));
    /**
      * Determine whether we are allowed to consume this IP slot
      */
    if (NULL != $id) {
      $old_ip = $this->getBit($id, "ip_address");
    } else {
      $old_ip = NULL;
    }
      
    $core = require_module("core");
      
    if (!$core->check_device_limit($this->params->primitives['ip_address']['value'], $old_ip)) {
      $this->err->err_from_string("Unable to enable SNMP on this device: You have exceeded the number of devices allowed in your license.");
      return FALSE;
    }
    /**
      * at this point, we know we can consume an IP slot
      */        
    
    $query = "SELECT id FROM {$this->table} WHERE ip_address = $ip";

    $res = $this->db->get_row($query);

    if ((is_array($res)) && ($res[$this->pkey] <> intval($id))) {
      $this->err->err_from_string("A device with the IP address " . htmlentities($ip) . " already exists.");
      $success = FALSE;
    }
    
    $type = "";
    $this->dashboard_manager = $this->registry->get_singleton($this->module, "device_dashboard_manager");
    $dev_profile = $this->dashboard_manager->get_profile($this->get('profile'));
                if ($dev_profile['callback'] != NULL) {
                        # If the callback is an array, it is a callback to an object method
                        # (either &$this or a foreign object)
                        if (is_array($dev_profile['callback'])) {
                                $object = $dev_profile['callback'][0];
        $type = $object->get_icon();
      } else {
        $type = "default";
      }
    } else {
      $type = "default";
    } 


    $query = "INSERT INTO hosts(ip, host_name_type, timestamp, hostname, node_type)
      VALUES(" . $this->db->escape($this->get('ip_address')) . ", 'CUSTOM', " . 
      $this->db->escape(mktime()) . ", " . $this->db->escape($this->get('label')) .
      ", " . $this->db->escape($type) . ")";
      
    $this->db->insert($query);
    

    return $success;
  }

  function post_insert() {
    $query = "UPDATE {$this->table} SET timestamp = 0 WHERE {$this->pkey} = " . $this->db->escape($this->meta['pkey_value']);
    $this->db->update($query);
    
    if ($GLOBALS['json_post']['enable_syslog'] == "true") {
      $ip = $this->get('ip_address');

      $facility = $GLOBALS['json_post']['syslog_facility'];
      $severity = $GLOBALS['json_post']['syslog_severity'];

      $query = "SELECT * FROM syslog_access WHERE ip='{$this->get('ip_address')}'";
      $res = $this->db->get_row($query);

      if (sizeof($res) <= 1) {
        $query = "INSERT INTO syslog_access (ip, facility, severity) VALUES ('%s', %s, %s)";
        $query = sprintf(
          $query,
          $ip,
          $facility,
          $severity
        );
        $this->db->insert($query);
      } else {
        $query = "UPDATE syslog_access SET facility=" . $facility . ", severity=" . $severity . " WHERE syslog_id=" . $res['syslog_id'];
        $this->db->update($query);
      }
    } else {
      $this->db->delete("DELETE FROM syslog_access WHERE ip='{$this->get('ip_address')}'");
    }
    $this->add_ping_form();
    return TRUE;
  }
  
  function post_update() {
    $this->add_ping_form();
    return $this->post_insert();
  }

  function add_ping_form() {
    if ($GLOBALS['json_post']['enable_ping'] == "true") {
      $query = "SELECT * FROM servers WHERE protocol='ICMP' AND ip='{$this->get('ip_address')}'";
      $res = $this->db->select($query);

      $label = $GLOBALS['json_post']['ping_name'];
      $interval = $GLOBALS['json_post']['ping_interval'];
      $timeout = $GLOBALS['json_post']['ping_timeout'];
      $log_timeout = $GLOBALS['json_post']['ping_log_timeout'];

      if (empty($interval)) {
        $interval = 60;
      }
      if (empty($timeout)) {
        $timeout = 1;
      }
      if (is_null($log_timeout)) {
        $log_timeout = 0;
      }

      if (sizeof($res) == 0 || (DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
        $query = "INSERT INTO servers (ip, name, protocol, interval, timeout, log_timeout) VALUES ('{$this->get('ip_address')}', '$label', 'ICMP', $interval, $timeout, $log_timeout)";
        $this->db->insert($query);
      }

    }
  }

  function pop($id) {
    $id = $this->db->escape($id);

    $query = "SELECT * FROM {$this->table} WHERE id = $id";

    $result = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      foreach($result as $key => $value) {
        $this->params->setval($key, $value);
      }
      return TRUE;
    }
  }

  function pre_delete($id) {
    # All these cleanup operations should be migrated to triggers or to one stored-procedure
    $this->db->start_transaction();
    $query = "DELETE FROM interfaces WHERE device_id = " . $this->db->escape($id);
    $res = $this->db->delete($query);
    $this->db->commit_transaction();

    # Experimental fix
    $entity = $this->fetch($id);
    if ($entity['enable_netflow'] == 't') {
      $this->db->start_transaction();
      $query = "DELETE FROM agg_netflow WHERE flow_src = (SELECT ip_address FROM devices WHERE id = " . $this->db->escape($id) . " LIMIT 1)";
      $this->db->delete($query);
      $this->db->commit_transaction();
    }

    $this->db->delete("DELETE FROM oid_log WHERE oid_id IN (SELECT id FROM oids WHERE device_id = $id)");
    $this->db->delete("DELETE FROM oids WHERE device_id = $id");
    $this->db->delete("DELETE FROM snmp_log WHERE ip = (SELECT ip_address FROM devices WHERE id = $id)");
    $this->db->delete("DELETE FROM snmptrap_log WHERE ip = (SELECT ip_address FROM devices WHERE id = $id)");
    $this->db->delete("DELETE FROM devices_notes WHERE device_id = $id");

    require_class("mod_services", "services_manager");
    require_class("mod_network", "df_disk_manager");
    require_class("mod_network", "smb_disk_manager");

    $sm = new Services_Manager();
    $dfm = new DF_Disk_Manager();
    $smbm = new SMB_Disk_Manager();

    $ip = $entity['ip_address'];

    $sm->delete_by_ip($ip);
    $dfm->delete_by_ip($ip);
    $smbm->delete_by_ip($ip);

    $this->db->delete("DELETE FROM syslog_access WHERE ip='$ip'");

    return $res;

  }

  function post_delete() {
    return $this->db->commit_transaction();
  }

  function get_traps($ip) {

    $query = "SELECT * FROM snmpoids WHERE ip = " . $this->db->escape($ip) . " ORDER BY snmpoid DESC";
    $res = $this->db->select($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return FALSE;
    }

    $return = array();
    foreach($res as $current) {
      if ($traps = $this->_traps_log($current['snmpoid'], $ip)) {
        $current['logs'] = $traps;
      }
      array_push($return, $current);
    }
    #var_dump($return);

    return $return;

  }

  /**
   * Retrieves a list of traps matching a specified trap OID
   *
   * @param string $trapoid
   * @return mixed
   */
  function _traps_log($trapoid, $ip) {

    /* Added a LIMIT clause to keep performace reasonable until a better fix is in place */
    $query = "SELECT a.*, b.*
    FROM snmptrap_log a, snmptrapoids b
    WHERE b.log_id = a.id
    AND a.trapoid LIKE " . $this->db->escape($trapoid . "%") . "
    AND a.ip = " . $this->db->escape($ip) . "
    ORDER BY b.log_id DESC LIMIT 100";

    $res = $this->db->select($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return FALSE;
    }

    return $res;
  }

  function pre_update($id) {
    if ( (strcmp($this->getBit($id, 'label'), $this->get('label')) <> 0) || 
                     (strcmp($this->getBit($id, 'profile'), $this->get('profile')) <> 0) ) {

      $type = "";
      $this->dashboard_manager = $this->registry->get_singleton($this->module, "device_dashboard_manager");
      $dev_profile = $this->dashboard_manager->get_profile($this->get('profile'));
      if ($dev_profile['callback'] != NULL) {
        # If the callback is an array, it is a callback to an object method
        # (either &$this or a foreign object)
        if (is_array($dev_profile['callback'])) {
          $object = $dev_profile['callback'][0];
          $type = $object->get_icon();
        } else {
          $type = "default";
        }
      } else {
        $type = "default";
      } 

      $query = "INSERT INTO hosts(ip, host_name_type, timestamp, hostname, node_type)
      VALUES(" . $this->db->escape($this->get('ip_address')) . ", 'CUSTOM', " . 
      $this->db->escape(mktime()) . ", " . $this->db->escape($this->get('label')) .
      ", " . $this->db->escape($type) . ")";
      
      $this->db->insert($query);
    }
    
    
    $query = "SELECT count(interface) AS \"if_count\" FROM interfaces WHERE device_id = $id";
    $res = $this->db->get_row($query);

    if (intval($res['if_count']) > 0) {
      if ($this->get('enable_netflow') == "false") {
        $query = "UPDATE interfaces SET enable_shm = false WHERE device_id = $id";
        $this->db->update($query);
      }

      if ($this->get('enable_snmp') == "false") {
        $query = "UPDATE interfaces SET enable_logging = false WHERE device_id = $id";
        $this->db->update($query);
      }
    }
    
    $this->debugger->add_hit("enable_snmp status: " . $this->get("enable_snmp"));
    /**
      * Determine whether we are allowed to consume this IP slot
      */
    $this->debugger->add_hit("Verifying device restrictions.");
    $core = require_module("core");
      
    if (!$core->check_device_limit($this->get('ip_address'), $this->getBit($id, 'ip_address'))) {
      $this->err->err_from_string("Unable to enable SNMP on this device: You have exceeded the number of devices allowed in your license.");
      return FALSE;
    }
    /**
      * at this point, we know we can consume an IP slot
      */

    return TRUE;
  }
  
  function get_ip($param) {
    require_class("core", "validator");
    $validator = new Validator();
    
    $this->debugger->add_hit("Param: $param");
    if ($validator->validate_ip_address($param)) {
      return $param;
    }
    
    $query = "SELECT ip_address FROM {$this->table} WHERE id = " . $this->db->escape($param);
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    
    return $res['ip_address'];
  }
  
  function get_id($param) {
    require_class("core", "validator");
    $validator = new Validator();

    if (!$validator->validate_ip_address($param)) {
      $this->debugger->add_hit("Param: $param - Intval: " . intval($param));
      return $param;
    }
    
    $query = "SELECT id FROM {$this->table} WHERE ip_address = " . $this->db->escape($param);
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    
    return $res['id'];
  }

  function get_services($ip, $type=NULL) {
    require_class("mod_services", "services_manager");
    $sm = new Services_Manager();
    $services = $sm->get_by_ip($this->ip_address, $type);
    return $sm->get_by_ip($ip, $type);
  }
  
  function get_urls($ip) {
    require_class("mod_services", "url_manager");
    $um = new Url_Manager();
    return $um->get_by_ip($ip);
  }  

  function get_disks($ip) {
    require_class("mod_network", "composite_disk_manager");
    $cm = new Composite_Disk_Manager();
    return $cm->get_by_ip($ip);
  }


}

?>
