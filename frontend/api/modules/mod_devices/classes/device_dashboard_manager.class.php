<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    4.1a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2006, Netmon Inc. (netmon.ca)
  */




/**
  * @package    MADNET
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    4.1a
  * Device Dashboard Manager
  */
class Device_Dashboard_Manager extends MadnetModule {


	/**
	 * Device Profiles collection
	 *
	 * @var array
	 */
	var $profiles = array(
          "dash_akcpsp2",
          "dash_securityhawk",
          "dash_sensorhawk",
          "dash_barracuda_spam",
          "dash_checkpoint",
          "dash_netbotz",
          "dash_sun_cobalt",
          "dash_cisco_ap",
          "dash_hp_ap",
          "dash_hp_msm",
          "dash_cisco_asa",
          "dash_cisco_firewall",
          "dash_cisco_generic",
          "dash_cisco_vpn",
          "dash_sonicwall_router",
          "dash_cisco_switch",
          "dash_hp_switch",
          "dash_linux_adv",
          "dash_linux_generic",
          "dash_lotus_domino_7",
          "dash_lotus_domino_informant_7",
          "dash_printer_generic",
          "dash_printer_hp",
          "dash_ups_apc",
          "dash_ups_tripplite",
          "dash_vmware_esxi",
          "dash_windows_generic",
          "dash_windows_iis",
          "dash_windows_snmp_inf_adv",
          "dash_windows_snmp_exch07_std",
          "dash_windows_snmp_exch_std",
          "dash_windows_snmp_inf_iis",
          "dash_windows_snmp_inf_sql",
          "dash_windows_snmp_inf_std"
	);

	/**
	 * Initialization method
	 *
	 *
	 * @return void
	 * @access public
	 */
	function init() {
		$this->_set_module("mod_devices");
		require_class($this->module, "device_manager");
		require_class($this->module, "snmp_interface_manager");
	}

	/**
	 * Returns TRUE when profile string is in the allowed profiles
	 *
	 * @param string $profile
	 * @return boolean
	 */
	function get_profile($profile) {
		# Basic error control
		if (!in_array(strtolower($profile), array_keys($this->profiles))) {
			$this->err->err_from_string("Unable to locate dashboard for the &quot;$profile&quot; device profile");
			return FALSE;
		}

		return TRUE;
	}


	/**
	 * SNMP Custom-dashboard generator.
	 *
	 * @param string $profile
	 * @param pointer $index_content
	 * @return boolean
	 */
	function get_dashboard($profile = NULL, &$index_content) {
		
		$ip = $_GET['ip'];
		
		$dm = new Device_Manager();
		$device = $dm->fetch_by_ip($ip);
		
		/**
		 * Use the profile configured for the device, or use the default
		 * snmp profile if none can be found.
		 */
		if ($profile == NULL) {
			$profile = $device['profile'];
			$this->debugger->add_hit("Profile value: $profile");
			if (strcmp($profile, "") == 0) {
				$profile = "snmp";
			}
		} else {
			$this->debugger->add_hit("Requesting profile $profile");
		}
		
		/**
		 * Retrieves a reference to the profile manager (created when the manager registered itself)
		 */
		$dev_profile = $this->get_profile($profile);
		if ($dev_profile == FALSE) { return FALSE; }

		# First, handle cases where a callback was supplied (in the profile)
		if ($dev_profile['callback'] != NULL) {
			# If the callback is an array, it is a callback to an object method
			# (either &$this or a foreign object)
			if (is_array($dev_profile['callback'])) {
				# Retrieve a reference to the object
				$object = $dev_profile['callback'][0];
				# Retrieve the name of the method
				$method = $dev_profile['callback'][1];
				
				/**
				 * Retrieve the device passed in $_GET['ip'] and assign it to the profile
				 * so it can access device info.
				 */
				$object->device = $device;
				
			# If it is just a string, it must be a call to a function in a loaded library.
			} else {
				$object = NULL;
				$method = $dev_profile['callback'];
			}
		# If no callback was provided, we use naming convention to find the appropriate
		# dashboard in the local scope.
		} else {
			$method = "get_" . $profile . "_dashboard";
			$object = $this;
		}

		# Handle function callbacks
		if ($object == NULL) {
			if (function_exists($method)) {
				return $method($index_content);
			} else {
				$this->err->err_from_string("The specified callback ($method) does not exist. Aborting.");
				return FALSE;
			}
		}

		# Handle method callbacks
		if (method_exists($object, $method)) {
			return $object->$method($index_content);
		} else {
			$this->err->err_from_string("Unable to locate dashboard for the &quot;$profile&quot; device profile.");
			return FALSE;
		}



	}

	/**
	 * Displays the dashboard for generic SNMP Devices
	 *
	 * @param pointer $index_content
	 */
	function get_snmp_dashboard(&$index_content) {
		
		
		# Get the interfaces from the DB first, then from RFC1213-MIB.interfaces, 
		# and merge the results.
		$sdm = new device_manager();
		$if = $sdm->get_all_device_interfaces();
		$dev = $sdm->fetch_by_ip($_GET['ip']);
		
		#$interfaces = $this->rfc1213_ifaces($sdm->get('ip_address'), $sdm->get('snmp_community'));
		#$this->debugger->add_hit("RFC1213 Merged Ifaces:", NULL, NULL, vdump($interfaces));
		
		
		$parser = new Parser($this->tpl_dir);
		$parser->assign_by_ref("interfaces", $if);
		#$parser->assign('ifaces', $interfaces);
		$parser->assign_by_ref("device", $dev);

		$dashboard = require_module("mod_dashboard");
		$parser->register_modifier("resolve_mac", array(&$dashboard, "resolve_mac"));
		$parser->register_modifier("resolve_max", array(&$dashboard, "resolve_max"));
		$parser->register_function("ajax_oid", array(&$this, "ajax_oid_hook"), FALSE, FALSE);
		$parser->register_function("ajax_proxy", array(&$this, "ajax_proxy_hook"), FALSE, FALSE);

		$index_content .= $parser->fetch("manage_snmp_device.tpl");
		return;
	}
	
	
	/*
	 * Merges the interface list obtained by querying RFC1213-MIB.interfaces.ifTable
	 * and the list of interfaces we have in the DB for the specified device
	 */
	function rfc1213_ifaces($ip) {
		# Get the interfaces from the DB first, then from RFC1213-MIB.interfaces, 
		# and merge the results.
		$sdm = new device_manager();
		$if = $sdm->get_all_device_interfaces($ip);
		$dev = $sdm->fetch_by_ip($ip);
		
		$mib = new MIB_Manager();
		$snmp_if = $mib->snmp_table($ip, $sdm->get('snmp_community'), '.1.3.6.1.2.1.2.2');
		
		# TODO: Reindex $if and $snmp_if to remove cartesian product, then use something like:
		#$merged = array_intersect_uassoc($snmp_if, $if, array(&$this, 'ifaces_merge_callback'));
		
		# Create empty slots based on the source of the data
		$interfaces = array('db' => array(), 'snmp' => array());
		
		
		for ($i = 0; $i < sizeof($if); $i++) {
			for ($s = 0; $s < sizeof($snmp_if); $s++) {
				if (intval($if[$i]['interface']) == intval($snmp_if[$s]['Index'])) {
					$interfaces[$i]['snmp'] = $snmp_if[$s];
					$interfaces[$i]['db'] = $if[$i];
					break 1;
				}
			}
		}
		
		return $interfaces;
		
	}
	
	/* Returns all merged data from RFC1213-MIB and the DB for all ifaces
	 * 
	 * Returns the data in json_encoded format.
	 */
	function json_rfc1213_ifaces(&$index_content) {
		$ip = $_GET['ip'];
		
		$ifaces = $this->rfc1213_ifaces($ip);
		$index_content = json_encode($ifaces);
		return $index_content;
	}
	
	
	
	function ajax_oid_hook($params) {
		$oid = $params['oid'];
		$class= $params['class'];
		$freq = $params['frequency'];
		$parent = $params['parent'] ? "'".$params['parent']."'" : "null";
		$callback = $params['callback'] ? $params['callback'] : "null";
		$trackable = $params['trackable'] ? true : false;
		$text = $params['text'] ? $params['text'] : "";
		

		$ret = <<<EOM
<img class="progress" src="/assets/progress.gif" id="{$oid}_progress" />
<span id='$oid' class='$class'>
<script language="Javascript">
	oid_handler.add_oid('$oid', $freq, $parent, $callback);
</script>
</span>
$text
EOM;

		if ($trackable)
		{
			$ret .= <<< EOM
<span id="{$oid}_trackable"></span>
EOM;
		}

		return $ret;
	}
	
	function ajax_proxy_hook($params) {
		$dash  = $params['dash'];
		$proc  = $params['proc'];
		$freq  = $params['freq'];
		$div   = $params['div'];
		$parms = $params['params'];
		$parent = $params['parent'] ? "'".$params['parent']."'" : "null";

		return <<<EOM
<div id="$div" align="center">
		<img class="icon" src="/assets/progress.gif" /> <span class="unresolved">Loading... Please Wait</span>
		<img class="progress_big" src="/assets/progress_big.gif" id="{$div}_progress" />
		<script language="Javascript">
			oid_handler.add_proxy('$dash', '$proc', '$parms', $freq, '$div', $parent);
		</script>
</div>
EOM;
	}

}


?>
