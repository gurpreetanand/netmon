
<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("files").className = 'sort-table';
	var types = ['CaseInsensitiveString', 'CaseInsensitiveString', 'netmon_capacity', 'netmon_date', null];
	var report = new SortableTable(document.getElementById("files"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="titlebar">
	File Browser
</div>

{if $files}
<div class="datagrid center">
<table width="100%" cellpadding="0" cellspacing="0" align="center" border="0" id="files">
	<thead>
	<tr>
		<td><div align="left">Label</div></td>
		<td>File Name</td>
		<td>Size</td>
		<td>Last Updated</td>
		<td>Actions</td>
	</tr>
	</thead>
	{foreach from=$files item="file"}
	<tr>
		<td><div align="left">
		{if $file.busy != 't'}
			{if $file.type == "bin" or $file.type == "unknown"}
			<a href="download_manager.php?module=mod_vfs&action=download_file&id={$file.id}">{$file.label}</a>{/if}
			{if $file.type == "ascii"}
			<a href="?module=mod_vfs&action=view_file&id={$file.id}&root_tpl=blank_panel">{$file.label}</a>{/if}
		{else}
			{$file.label}
		{/if}
		</div></td>
		<td>
		{if $file.busy != 't'}
			{if $file.type == "bin" or $file.type == "unknown"}
			<a href="download_manager.php?module=mod_vfs&action=download_file&id={$file.id}">{$file.filename}</a>{/if}
			{if $file.type == "ascii"}
			<a href="?module=mod_vfs&action=view_file&id={$file.id}&root_tpl=blank_panel">{$file.filename}</a>{/if}
		{else}
			{$file.filename}
		{/if}
		</td>
		<td>{$file.size|capacity}</td>
		
		<td>{$file.mtime|date_format:"%b %e, %Y %H:%M:%S"}</td>
		<td>
		{if $file.type == "unknown"}
		<img src="assets/icons/icon_caution.gif" alt="Warning" title="Unknown File Type">
		{/if}
		
		{if $file.busy != 't'}
			{if $file.type == "bin" or $file.type == "unknown"}
			<a href="download_manager.php?module=mod_vfs&action=download_file&id={$file.id}">Download</a> |{/if}
			{if $file.type == "ascii"}
			<a href="?module=mod_vfs&action=view_file&id={$file.id}&root_tpl=blank_panel">View</a> [<a href="#" onclick="window.open('/?module=mod_vfs&action=view_file&id={$file.id}&root_tpl=blank_panel&expanded=1', 'Saved Document', 'toolbar=no,scrollbars=yes');"><img border="0" src="/assets/icons/expand.gif" alt="New Window" title="Open in New Window" /></a>] |{/if}
			<a href="?module=mod_vfs&action=delete_file&id={$file.id}">Delete</a></td>
		{else}
			File not yet available
		{/if}
	</tr>
	{/foreach}
</table>
</div>
{else}
{"This directory is currently empty"|message_bar}
{/if}

<!-- end of {$smarty.template} -->
