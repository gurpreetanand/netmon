<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


// sample.class.php

/**
  * File Manager MCO
  *
  * This module is used to perform any application-level change and management
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class mod_vfs extends MadnetModule {

  /**
   * File Manager
   *
   * @var File_Manager
   */
  var $fm;

  /**
    * Initializes all module variables
    *
    * @return void
    */
  function init() {
    $this->module = "mod_vfs";
    require_class($this->module, "File_Manager");
    $this->fm = new File_Manager();
  }

  /**
   * Displays the directory-list.
   *
   * @param pointer $index_content
   */
  function get_dir_tree(&$index_content) {
    $dirs = $this->fm->get_directories();
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("dirs", $dirs);
    $index_content .= $parser->fetch('directory_tree.tpl');
  }

  /**
   * View files belonging to the specified directory
   *
   * @param pointer $index_content
   */
  function view_dir_files(&$index_content) {
          $this->fm->sync_directory(intval($_GET['dir_id']));
          $payload = array(
            "files" => $this->fm->get_dir_files(intval($_GET['dir_id']))
          );
          print json_encode($payload);
  }
  
  function get_file_list() {
    $dirs = $this->fm->get_directories();
    $payload = array();

    foreach ($dirs as &$dir) {
      $dir_id = intval($dir['id']);
      $this->fm->sync_directory($dir_id);
      $dir['files'] = $this->fm->get_dir_files($dir_id);
    }
    print json_encode($dirs, true);
  }

  /**
   * Form used to update a file.
   *
   * @param pointer $index_content
   */
  function form_edit_file(&$index_content) {
    ;;
  }

  /**
   * Updates file info
   *
   * @param pointer $index_content
   */
  function process_update_file(&$index_content) {
    ;
  }

  /**
   * Deletes a file from the FS and the DB
   *
   * @param pointer $index_content
   */
  function delete_file(&$index_content) {
    foreach ($_POST['ids'] as $id) {
      $id = intval($id);
      $file = $this->fm->get_file($id);
      $_GET['dir_id'] = $file['dir_id'];
    
    
      if (has_perm("Guest/Demo Account")) {
        $this->debugger->add_hit("Demo mode enabled");
        $this->err->err_from_code(400, "This operation is not available in Demo mode");
        return;
      }

      if ($id <= 0) {
        $this->err->err_from_code(400, "Invalid file identifier specified in query string.");
        return;
      }
      $out = $ret = "";
      $filename = escapeshellarg($file['filename']);
    
      exec("sudo rm -rf $filename", $out, $ret);

    }
    print '{}';
  }

  /**
   * Displays the content of a file
   *
   * @param pointer $index_content
   */
  function view_file(&$index_content) {
    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_code(400, "Invalid file identifier specified in query string.");
      return;
    }

    $file = $this->fm->get_file($id);
    $_GET['dir_id'] = $file['dir_id'];
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_code(400, "This operation is not available in Demo mode", FALSE);
      return;
    }
    
    print file_get_contents($file['path']);
  }

  /**
   * Downloads a file
   *
   * @param pointer $index_content
   */
  function download_file(&$index_content) {
    $id = intval($_GET['id']);

    if ($id <= 0) {
      echo "Invalid file identifier specified in query string.";
      return;
    }

    $file = $this->fm->get_file($id);
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->handle_fatal("This operation is not available in Demo mode");
      return $this->view_dir_files($index_content);
    }

    header("Content-type: application/x-octets");
    header('Content-Disposition: attachment; filename="' . basename($file['filename']) . '"');
    $fp = fopen($file['path'], "rb");
    fpassthru($fp);
    fclose($fp);

    return;

  }
}

?>
