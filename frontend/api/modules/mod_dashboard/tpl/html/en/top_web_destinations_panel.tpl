<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,sortabletable,sort_lib,yui-ext"}
{import_css files="_print,yui-ext/grid"}
{literal}
<script language="Javascript">

//setup datamodel

var grid_init = {
	
	init : function (){
		{/literal}{* Build the JS Data Array *}{literal}
		var arr=[{/literal}{capture assign="len"}{array_size array=$data}{/capture}{assign var='i' value='0'}
		{foreach from=$data item=entry}{if $i < $len-1}
			['<a title="{$entry[0]}" href="#" onClick="parent.parent.location=\'?module=mod_layout&action=render_section&layout=network&ip={$entry[0]}&store_request=2\'">{$entry[1]}</a> <a href="http://{$entry[1]}" target="_blank"><img class="icon" border="0" title="Open {$entry[1]} in a new page" src="/assets/icons/link.png" alt="Go to {$entry[1]}" /></a>', '{$entry[2]|speed}', '{$entry[3]}'],
			{else}
			['<a title="{$entry[0]}" href="#" onClick="parent.parent.location=\'?module=mod_layout&action=render_section&layout=network&ip={$entry[0]}&store_request=2\'">{$entry[1]}</a> <a href="http://{$entry[1]}" target="_blank"><img class="icon" border="0" title="Open {$entry[1]} in a new page" src="/assets/icons/link.png" alt="Go to {$entry[1]}" /></a>', '{$entry[2]|speed}', '{$entry[3]}']
			{/if}
			{math equation="x + 1" x=$i assign="i"}
		{/foreach}
		{literal}
		];
		
		var dataModel = new YAHOO.ext.grid.DefaultDataModel(arr);
		var sort = YAHOO.ext.grid.DefaultColumnModel.sortTypes;
		var colModel = new YAHOO.ext.grid.DefaultColumnModel([
				{header: "Destination", width: 250, sortable: true, sortType: netmon_string_sort_with_html},
				{header: "Transfered", sortable: true, sortType: netmon_capacity_sort},
				{header: "Rate", width: 900, sortable: true, sortType: netmon_throughput_sort}
			]);
		
		var grid = new YAHOO.ext.grid.Grid('griddiv', dataModel, colModel);
		grid.autoSizeColumns = true;
		grid.trackMouseOver = true;
		grid.render();
		
	}
}


</script>
{/literal}

{if $data}
<div class="printOnly">
	<table width="100%"  border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="/assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Top Web Destinations Snapshot</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">{$smarty.now|date_format:"%Y-%m-%d %H:%M:%S"}</div></td>
	  </tr>
	</table>
	<br /><br />
</div>

<div id="griddiv" style="width: 100%; overflow: hidden; height: 200px;"></div>
<script>
YAHOO.ext.EventManager.onDocumentReady(grid_init.init, grid_init, true);
</script>
{else}
{"There is no data available in this section at the moment."|message_bar}
{/if}
</div>



<!-- end of {$smarty.template} -->
