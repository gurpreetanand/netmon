<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,sortabletable,sort_lib,yui-ext"}
{import_css files="_print,yui-ext/grid"}
 {literal}

<script language="Javascript">

//setup datamodel

var grid_init = {
	
	init : function (){
		var arr=[{/literal}{capture assign="len"}{array_size array=$data}{/capture}{assign var='i' value='0'}
		{foreach from=$data item="entry"}{if $i < $len-1}
			['<a title="{$entry[0]}" href="#" onClick="parent.parent.location=\'?module=mod_layout&action=render_section&layout=network&ip={$entry[0]}&store_request=2\';">{$entry[0]|resolve_ip|escape:"javascript"}</a>', '<a title="{$entry[1]}" href="#" onClick="parent.parent.location=\'?module=mod_layout&action=render_section&layout=network&ip={$entry[1]}&store_request=2\';">{$entry[1]|resolve_ip|escape:"javascript"}</a>', '{$entry[3]}'],
			{else}
			['<a title="{$entry[0]}" href="#" onClick="parent.parent.location=\'?module=mod_layout&action=render_section&layout=network&ip={$entry[0]}&store_request=2\';">{$entry[0]|resolve_ip|escape:"javascript"}</a>', '<a title="{$entry[1]}" href="#" onClick="parent.parent.location=\'?module=mod_layout&action=render_section&layout=network&ip={$entry[1]}&store_request=2\';">{$entry[1]|resolve_ip|escape:"javascript"}</a>', '{$entry[3]}']
			{/if}
			{math equation="x + 1" x=$i assign="i"}
		{/foreach}
		{literal}
		];
		
		var dataModel = new YAHOO.ext.grid.DefaultDataModel(arr);
		var sort = YAHOO.ext.grid.DefaultColumnModel.sortTypes;
		var colModel = new YAHOO.ext.grid.DefaultColumnModel([
				{header: "Source", width: 250, sortable: true,sortType: netmon_string_sort_with_html},
				{header: "Destination", sortable: true, sortType: netmon_string_sort_with_html},
				{header: "Rate", sortable: true, sortType: netmon_throughput_sort}
			]);
		
		var grid = new YAHOO.ext.grid.Grid('griddiv', dataModel, colModel);
		grid.autoSizeColumns = true;
		grid.trackMouseOver = true;
		grid.render();
		
	}
}

//YAHOO.ext.EventManager.onDocumentReady(grid_init.init, grid_init, true);

</script>
{/literal}

{if $data}
<div class="printOnly">
	<table width="100%"  border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="/assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Top Activity Snapshot</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">{$smarty.now|date_format:"%Y-%m-%d %H:%M:%S"}</div></td>
	  </tr>
	</table>
	<br /><br />
</div>

<div id="griddiv" style="width: 100%; overflow: hidden; height: 200px;"></div>
<script>
YAHOO.ext.EventManager.onDocumentReady(grid_init.init, grid_init, true);
</script>
{else}
{"There is no data available in this section at the moment."|message_bar}
{/if}
</div>

<!-- end of {$smarty.template} -->
