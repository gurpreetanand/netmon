<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,sortabletable,sort_lib,yui-ext"}
{import_css files="_print,yui-ext/grid"}
{literal}

<script language="Javascript">

function SetAllCheckBoxes(FormName, FieldName, CheckValue)
{
	if(!document.forms[FormName])
		return;
	var objCheckBoxes = document.forms[FormName].elements[FieldName];
	if(!objCheckBoxes)
		return;
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
		objCheckBoxes.checked = CheckValue;
	else
		// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
			objCheckBoxes[i].checked = CheckValue;
}

parent.document.getElementById("goback").style.display = "none";

//setup datagrid

var grid_init = {
	
	init : function (){
		{/literal}{* Build the JS Data Array *}{literal}
		var arr=[{/literal}{capture assign="len"}{array_size array=$hosts}{/capture}{assign var='i' value='0'}
		{foreach from=$hosts item=host}{if $i < $len-1}
			['<input type="checkbox" name="host_ids[]" value="{$host.id}">', '<a title="{$host.ip}" href="#" onClick="parent.parent.location=\'?module=mod_layout&action=render_section&layout=network&ip={$host.ip}&store_request=2\';">{$host.ip}</a>', '{$host.mac}', '{$host.timestamp|date_format:"%b %e, %Y %H:%M:%S"}'],
			{else}
			['<input type="checkbox" name="host_ids[]" value="{$host.id}">', '<a title="{$host.ip}" href="#" onClick="parent.parent.location=\'?module=mod_layout&action=render_section&layout=network&ip={$host.ip}&store_request=2\';">{$host.ip}</a>', '{$host.mac}', '{$host.timestamp|date_format:"%b %e, %Y %H:%M:%S"}']
			{/if}
			{math equation="x + 1" x=$i assign="i"}
		{/foreach}
		{literal}
		];
		
		var dataModel = new YAHOO.ext.grid.DefaultDataModel(arr);
		var sort = YAHOO.ext.grid.DefaultColumnModel.sortTypes;
		var colModel = new YAHOO.ext.grid.DefaultColumnModel([
				{header: "&nbsp;", width:30, sortable: false},
				{header: "IP Address", sortable: true,sortType: netmon_ipv4_sort_with_html}, 
				{header: "MAC Address", sortable: true},
				{header: "Discovered",  sortable: true, sortType: netmon_date_sort}
			]);
		
		var grid = new YAHOO.ext.grid.Grid('griddiv', dataModel, colModel);
		grid.autoSizeColumns = true;
		grid.trackMouseOver = true;
		grid.render();
		
	}
}

//YAHOO.ext.EventManager.onDocumentReady(grid_init.init, grid_init, true);

</script>
{/literal}
<div class="printOnly">
	<table width="100%"  border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="/assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Recently Discovered Hosts Report</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">{$smarty.now|date_format:"%Y-%m-%d %H:%M:%S"}</div></td>
	  </tr>
	</table>
	<br /><br />
</div>

<form name="hosts_form" method="POST" action="?module=mod_dashboard&action=clear_discovered_hosts&root_tpl=blank_panel">

{if $hosts}
<div id="griddiv" style="width: 100%; overflow: hidden; height: 200px;"></div>
<script>
YAHOO.ext.EventManager.onDocumentReady(grid_init.init, grid_init, true);
</script>
{/if}

</form>

<!-- end of {$smarty.template} -->
