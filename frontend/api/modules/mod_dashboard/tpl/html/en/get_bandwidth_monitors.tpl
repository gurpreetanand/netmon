<!-- {$smarty.template} ($Id$) -->
{import_js files="graphs,AC_RunActiveContent"}
{import_css files="_print"}
{if $ds}

<div class="titlebar">
	Bandwidth Monitors
</div>

	{foreach from=$ds item="interface"}
	<span id="device{$interface.ip_address}{$interface.interface}">	
		{if $interface.enable_logging == "t"}
		<!-- time-based throughput widget -->

		
		<script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','270',
			'height','200',
			'src','assets/charts/FC_2_3_MSLine_2?dataURL=%3Fmodule=mod_devices%26action=get_xml_interface_traffic%26root_tpl=blank%26ip={$interface.ip_address}%26interface={$interface.interface}&chartWidth=270&chartHeight=200',
			'quality','high',
			'salign','LT',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/charts/FC_2_3_MSLine_2?dataURL=%3Fmodule=mod_devices%26action=get_xml_interface_traffic%26root_tpl=blank%26ip={$interface.ip_address}%26interface={$interface.interface}&chartWidth=270&chartHeight=200'
			 ); //end AC code
			 gm.register_graph('device{$interface.ip_address}{$interface.interface}');			 
		</script>
		

		{else}
		<!-- throughput widget -->

		<script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','180',
			'height','200',
			'src','assets/charts/FC_2_3_Column2D?dataURL=%3Fmodule=mod_devices%26action=get_xml_snmp_throughput%26root_tpl=blank%26ip={$interface.ip_address}%26interface={$interface.interface}&chartWidth=180&chartHeight=200',
			'quality','high',
			'salign','LT',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/charts/FC_2_3_Column2D?dataURL=%3Fmodule=mod_devices%26action=get_xml_snmp_throughput%26root_tpl=blank%26ip={$interface.ip_address}%26interface={$interface.interface}&chartWidth=180&chartHeight=200'
			 ); //end AC code
			 gm.register_graph('device{$interface.ip_address}{$interface.interface}');
		</script>

		{/if}
	</span>
	{/foreach}
{else}
There are no bandwidth monitors configured.
{/if}
<!-- end of {$smarty.template} -->
