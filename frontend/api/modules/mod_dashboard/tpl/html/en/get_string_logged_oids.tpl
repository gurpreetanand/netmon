{import_js files="ajax_oid"}
<!-- {$smarty.template} ($Id$) -->

{if $rows}

<div align="center">
<strong>
<span style="font-size: 10px;">{$smarty.get.oid_label}
<br />
on {$smarty.get.device_label}</span>
<br />
<br />
</strong>
</div>
<div class="datagrid center">
<table width="99%" cellpadding="2" cellspacing="0">
<tr>
	<th>Time</th>
	<th>Value</th>
</tr>
{foreach from=$rows item="row"}

	<tr>
		<td>{$row.timestamp|date_format:"%b %e, %Y %H:%M:%S"}</td>
		<td><div align="left">{$row.message}</div></td>
	</tr>
{/foreach}
</table>
</div>

{/if}

<!-- end of {$smarty.template} -->