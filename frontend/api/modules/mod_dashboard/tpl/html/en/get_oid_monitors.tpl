<!-- {$smarty.template} ($Id$) -->
{import_js files="graphs,AC_RunActiveContent,ajax_oid"}
{import_css files="_print"}
{literal}
<script>

/*
  This will prevent race-conditions since this template is included into another
  one with similar functionality
*/
if (!gm) {
	var standalone = 1;
	var gm = new GraphManager();
} else {
	var standalone = 0;
}

function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}

function addOidWatcher(oid, datatype) {
	url = "?module=mod_devices&action=form_create_oid_watcher&device_id={/literal}{$smarty.get.id}{literal}&oid_txt=" + escape(oid) + "&type=" + datatype;
	parent.pnl_right.showEditor(url);
}


</script>
{/literal}
{if $ds}
<div class="titlebar">
	Device Monitors (SNMP OID Trackers)
</div>





	{foreach from=$ds item="oid"}
	
		{if $oid.enable_logging == "t"}
			{if $oid.cat_type == 'numeric'}
	
				<!-- time-based throughput widget -->
				<span id="span_{$oid.id}"><script type="text/javascript">
					AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
					'id','oid_{$oid.id}',
					'name','oid_{$oid.id}',
					'width','270',
					'height','200',
					'swliveconnect','true',
					'bgcolor','D4D0C8',
					'src','assets/charts/FC_2_3_MSLine_2?dataURL=%3Fmodule=mod_devices%26action=get_xml_oid_values%26root_tpl=blank%26oid_id={$oid.id}&chartWidth=270&chartHeight=200',
					'quality','high',
					'salign','LT',
					'pluginspage','http://www.macromedia.com/go/getflashplayer',
					'movie','assets/charts/FC_2_3_MSLine_2?dataURL=%3Fmodule=mod_devices%26action=get_xml_oid_values%26root_tpl=blank%26oid_id={$oid.id}&chartWidth=270&chartHeight=200'
					 ); //end AC code
					 
					 gm.register_graph('span_{$oid.id}');
					 
				</script></span>
			{else}
				<iframe width="270" height="180" frameborder="0" scrolling="auto" src="?module=mod_dashboard&action=get_string_logged_oid&oid_id={$oid.id}&root_tpl=blank_panel&device_label={$oid.device_label}&device_ip={$oid.ip_address}&oid_label={$oid.label}"></iframe>
			{/if}

		{else}

		<!-- throughput widget -->
		<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
		<strong>{$oid.device_label}</strong> ({$oid.ip_address}): <span class="gray2">{$oid.label}</span> <a href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid={$oid.oid}&root_tpl=blank_panel&class=white');">[?]</a> <img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
		Value: <span class="gray2">{$oid.message}</span>
		{if $oid.message != $oid.prev_message and $oid.cat_type == 'numeric'}
		<!--<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">-->
			{if $oid.message > $oid.prev_message}
				Change: <span class="blue2">+ {$oid.message-$oid.prev_message}</span>
		 	{else}
				Change: <span class="orange">- {$oid.prev_message-$oid.message}</span>
			{/if}

		{/if}
		</div>
		
		{/if}

	{/foreach}
{else}
	{if $smarty.get.device_id > 0}
	<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> No custom OID trackers have been defined for this device.
	</div>
	{/if}

{/if}

<script language="Javascript">
{literal}
	if (standalone == 1) {
		gm.init();
	}
{/literal}
</script>

<!-- end of {$smarty.template} -->