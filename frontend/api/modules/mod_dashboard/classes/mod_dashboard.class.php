<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


// mod_dashboard.class.php

/**
  * Dashboard MCO
  *
  * This module is used to perform any application-level change and management
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class mod_dashboard extends MadnetModule {


  /**
    * Data structure used for caching of NS lookups
    */
  var $cache = array();

  function get_dashboard() {
    $dash = array();
    $dash['bandwidth'] = $this->get_bandwidth_monitors();
    $dash['oid'] = $this->get_oid_monitors();
    $dash['alerts'] = $this->get_recent_alerts();
    $dash['current_problems'] = $this->get_current_issues();
    $dash['sniff'] = array(
                  "recent_hosts" => $this->get_discovered_hosts_array(),
                  "top_conversations" => $this->get_top_conversations_json(),
                  "web_destinations" => $this->get_top_web_destinations_json(),
                  "web_users" => $this->get_top_web_users_json()
    );
    $dash['disks'] = $this->get_disk_monitors();
    print(json_encode($dash));
    return;
  }
  
  function get_recent_alerts() {

    $query = "SELECT ap.trigger_timestamp, ap.dispatch_timestamp, ap.retries_processed,
               ap.parsed_alert_message, ap.sent, u.first_name, u.last_name, u.email,
               am.name AS \"media\", ah.required_retries, ah.trigger_id,
               at.reference_table_name, at.reference_pkey_val
               FROM alert_pending ap, alert_handlers ah, alert_medias am, alert_triggers at, users u
               WHERE ah.id = ap.handler_id
               AND am.id = ah.media_id
               AND at.trigger_id = ah.trigger_id
               AND u.id = ah.user_id
               AND at.reference_table_name != 'arptable'
               AND ap.trigger_timestamp > extract(epoch from 'now'::timestamp - '1 day'::interval)
               ORDER BY ap.trigger_timestamp DESC
               LIMIT 14";

    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return NULL;
    }
    
    foreach ($res as &$alert) {
      if ($alert['reference_pkey_val'] && $alert['reference_table_name']) {
        $query = "";
        if ($alert['reference_table_name'] == "oids") {
          $query = "SELECT s.*, d.ip_address \"ip\"  FROM " . $alert['reference_table_name'] .
                    " s, devices d WHERE s.id=" . $alert['reference_pkey_val'] . " AND s.device_id=d.id";
        } else {
          $query = "SELECT s.*, d.id device_id FROM " . $alert['reference_table_name'] . " s, devices d
                    WHERE srv_id=" . $alert['reference_pkey_val'] . " AND d.ip_address = s.ip";
        }

        $service = $this->db->select($query);
        $alert['service'] = $service[0];
        $alert['service']['hostname'] = $this->resolve_ip($alert['service']['ip']);
      }
    }
    
    return $res;

  }
  
  function get_current_issues() {
    require_class("mod_devices", "device_manager");
    $dm = new Device_Manager();
    $devices = $dm->get_devices_array();
    
    $memcache = new Memcache;
    $memcache->connect("localhost");
    
    $issues = array();

    foreach ($devices as $index => &$device) {
    
      $mcAlertKey = $device["ip_address"] . "-alerts";
      $dalerts = $memcache->get($mcAlertKey);
      if ($dalerts) {
        $device["daemon"] = json_decode($dalerts, true);
      } else {
        $device["daemon"] = array();
      }
      
      $downServers = $this->db->select("SELECT * FROM servers WHERE ip='" . $device['ip_address'] . "' AND status='DOWN'");
      if (!is_array($downServers)) { $downServers = array(); }
      $device["servers"] = $downServers;

      $downSmb = $this->db->select("SELECT * FROM smb_servers WHERE (status=-1 OR status > threshold) AND ip='" . $device['ip_address'] . "'");
      $downDf = $this->db->select("SELECT * FROM df_servers WHERE (status=-1 OR status > threshold) AND ip='" . $device['ip_address'] . "'");
      if (!is_array($downSmb)) { $downSmb = array(); }
      if (!is_array($downDf)) { $downDf = array(); }
      $device['disks'] = array_merge($downSmb, $downDf);

      require_class("mod_devices", "snmp_interface_manager");

      $sim = new SNMP_Interface_Manager();
      $device["interfaces"] = $sim->get_device_interfaces($device["id"]);

      $hasPanelError = false;


      if ($device["enable_snmp"] == "t" && $device["show_iface_problems"] == "t") {
        $dashjson = json_decode(file_get_contents("/apache/dashboards.json"), true);

        foreach ($device["interfaces"] as &$int) {
          $intErrors = array();

          foreach ($dashjson["networkCachePanel"] as $intError) {
            $intOid = $intError['oid'] . $int['interface'];

            $memcacheKey = $device['ip_address'] . ":" . $intOid;
            $intErrors[$intError['oidPretty']] = $memRes = json_decode($memcache->get($memcacheKey), true);

            if (
              intval($memRes["warning"]) > 0 &&
              intval($memRes["value"]) > 0 &&
              intval($memRes["value"] >= $memRes["warning"])
            ) {
              $hasPanelError = true;
            }
          }

          $int["interfaceCache"] = $intErrors;
        }
      }


      if (
        $hasPanelError ||
        sizeof($device["daemon"]) + sizeof($device["servers"]) + sizeof($device['disks']) > 0 || 
        ($device['status'] == 'down' && $device['enable_snmp'] == 't')
      ) {
        $device["hostname"] = $this->resolve_ip($device["ip_address"]);
        array_push($issues, $device);
      }
    }

    $downUrls = $this->db->select("SELECT * FROM urls WHERE status='DOWN' OR status='NO MATCH'");
    foreach ($downUrls as $url) {
      $url['type'] = 'url';
      array_push($issues, $url);
    }

    return $issues;
  }

  function get_disk_monitors() {
    $payload = array();

    $query = "SELECT s.*, d.id\"device_id\" FROM df_servers s, devices d WHERE (s.homedisplay='t' OR s.status=-1 OR s.status > s.threshold) AND d.ip_address=s.ip;";
    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $payload["df_servers"] = array();
    } else {
      $payload["df_servers"] = $res;
    }

    $query = "SELECT s.*, d.id\"device_id\" FROM smb_servers s, devices d WHERE (s.homedisplay='t' OR s.status=-1 OR s.status > s.threshold) AND d.ip_address=s.ip;";
    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $payload["smb_servers"] = array();
    } else {
      $payload["smb_servers"] = $res;
    }

    return $payload;
  }

  function get_bandwidth_monitors($device_id = NULL) {
    require_class("mod_devices", "snmp_interface_manager");
    require_module("mod_devices");
    $mod_devices = new mod_devices();
    $sim = new SNMP_Interface_Manager();

    $query = "SELECT d.ip_address, i.interface, i.description
    FROM interfaces i, devices d
    WHERE d.id = i.device_id
    and i.homedisplay = 'Y'
    ORDER BY enable_logging DESC, ip_address ASC, interface ASC, description ASC";
    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    }


    $ds = array();

    foreach($res as $if) {
      if ($sim->pop($if['ip_address'], $if['interface'])) {
        $sim->params->build_autoinsert_array();
        $bw = $sim->params->autoinsert_array;
        $bw['graph_data'] = $mod_devices->get_object_interface_traffic($if['ip_address'], $if['interface']);
        array_push($ds, $bw);
      }
    }

    return $ds;
  }
  
  function get_oid_monitors($device_id = NULL) {  
    require_class("mod_devices", "oid_watcher_manager");
    $oid = new OID_Watcher_Manager();
    
    // Try to get from GET
    if ($device_id == NULL) { 
      $device_id = intval($_GET['device_id']);
    }
    
    if ($device_id >= 1) {
      $seq = "WHERE device_id = " . $this->db->escape($device_id);
    } else {
      $seq = "WHERE homedisplay IS TRUE";
    }
    
    
    $query = "SELECT id, enable_logging, datatype FROM oids 
    $seq
    ORDER BY enable_logging ASC, device_id ASC, id ASC";
    
    $res = $this->db->select($query);
    
    if (DB_QUERY_ERROR == $res) {
      return;
    } elseif (DB_NO_RESULT == $res) {
      $res = array();
    }
    
    $ds = array();
    
    foreach($res as $tracker) {
      if ($oid->fetch($tracker['id'])) {
        $oid->params->build_autoinsert_array(TRUE);
        array_push($ds, $oid->params->autoinsert_array);
        $ds[sizeof($ds)-1]['cat_type'] = OID_Watcher_Manager::_get_datatype_category($tracker['datatype']);
        #var_dump($oid->params->autoinsert_array);
      }
    }
    
    return $ds;
  }

  
  /**
   * Displays a string OID's last sampled values in a div
   *
   * @param pointer $index_content
   */
  function get_string_logged_oid(&$index_content) {
    $id = intval($_GET['oid_id']);
    
    if ($id <= 0) {
      $this->err->err_from_string("Invalid OID identifier specified. Aborting.");
      return;
    }
    
    $query = "SELECT timestamp, message FROM oid_log WHERE oid_id = " . $this->db->escape($id) . " ORDER BY id DESC LIMIT 30";
    $res = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return;
    }
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("rows", $res);
    $index_content .= $parser->fetch("get_string_logged_oids.tpl");
  }

  function get_top_conversations(&$index_content) {
    $this->shm = $this->registry->get_singleton("core", "shm_manager");
    $data = $this->shm->get_data_structure(SHM_TOP_CONVERSATIONS);
    $this->debugger->add_hit("Content of SHM", NULL, NULL, vdump($data));

    $parser = new Parser($this->tpl_dir);
    $parser->register_modifier("resolve_max", array(&$this, "resolve_max"));
    $parser->register_modifier("resolve_mac", array(&$this, "resolve_mac"));
    $parser->register_modifier("resolve_ip", array(&$this, "resolve_ip"));


    if (is_array($data)) {
      $parser->assign_by_ref("data", &$data);
    }

    $index_content .= $parser->fetch("top_activity_panel.tpl");
    return;
  }
  
  function get_top_conversations_json() {
    $deviceId = intval($_GET['device_id']);
    $interface = intval($_GET['interface']);

    $data = array();
    $payload = array();
    $type = "regular";

    $this->shm = $this->registry->get_singleton("core", "shm_manager");

    if ($deviceId <= 0 && $interface <= 0) {
      $data = $this->shm->get_data_structure(SHM_TOP_IP_STREAMS);
    } else {
      $query = "SELECT * FROM interfaces WHERE device_id=$deviceId and interface=$interface";
      $res = $this->db->get_row($query);
      if (sizeof($res['shm_key']) > 0) {
        $type = 'interface';
        $data = $this->shm->get_data_structure($res['shm_key']);
      }
    }

    $net = require_module("mod_network");

    foreach ($data as $point) {
      $port = min(array($point[2], $point[3])); 
      array_push($payload,
        array(
          "type" => $type,
          "source" => array (
            "ip" => $point[0],
            "hostname" => $this->resolve_ip($point[0])
          ),
          "destination" => array(
            "ip" => $point[1],
            "hostname" => $this->resolve_ip($point[1])
          ),
          "port" => $port,
          "portName" => $net->_resolve_port($port),
          "rate" => $point[6]
        )
      );
    }

    return $payload;
  }

  function get_recent_activity_json() {
    print json_encode($this->get_top_conversations_json());
  }

  function get_top_activity(&$index_content) {
    $this->shm = $this->registry->get_singleton("core", "shm_manager");

    /**
      * @todo Add key for top protocol activity in conf_shm.inc.php (0xf5000000)
      */
    $data = $this->shm->get_data_structure(0xf5000000);

    # Filter the results based on the GET query
    $results = array();
    if (is_array($data)) {
      foreach($data as $current) {
        # Pass/Fail flag. If you fail one test, you are out.
        $pass = TRUE;

        # Check the frame type
        if ($this->fix_frame_type_val($current[2]) != $_GET['ethernet_frame_type']) {
          $pass = FALSE;
        }

        # Check the IP protocol if any
        if (!empty($_GET['ip_protocol'])) {
          if (intval($_GET['ip_protocol']) <> $current[3]) {
            $pass = FALSE;
          }
        }

        # If the entry passed the tests, add it to the results struct.
        if ($pass) { array_push($results, $current); }

      }
    }



    $parser = new Parser($this->tpl_dir);
    $parser->register_modifier("resolve_max", array(&$this, "resolve_max"));
    $parser->register_modifier("resolve_mac", array(&$this, "resolve_mac"));
    $parser->register_modifier("resolve_ip", array(&$this, "resolve_ip"));


    if (is_array($results)) {
      $parser->assign_by_ref("data", &$results);
    }

    $index_content .= $parser->fetch("top_activity_by_proto_panel.tpl");
    return;
  }

  function get_top_web_destinations(&$index_content) {
    $this->shm = $this->registry->get_singleton("core", "shm_manager");
    $data = $this->shm->get_data_structure(SHM_TOP_WEB_SERVERS);

    $parser = new Parser($this->tpl_dir);
    $parser->register_modifier("resolve_max", array(&$this, "resolve_max"));
    $parser->register_modifier("resolve_mac", array(&$this, "resolve_mac"));
    $parser->register_modifier("resolve_ip", array(&$this, "resolve_ip"));

    if (is_array($data)) {
      $parser->assign_by_ref("data", &$data);
    }

    $index_content .= $parser->fetch("top_web_destinations_panel.tpl");
    return;
  }

  function get_top_web_destinations_json() {
    $this->shm = $this->registry->get_singleton("core", "shm_manager");
    $data = $this->shm->get_data_structure(SHM_TOP_WEB_SERVERS);

    $payload = array();
    foreach ($data as $point) {
      array_push($payload, array(
        "ip" => $point[0],
        "hostname" => $point[1],
        "total" => $point[2],
        "rate" => $point[3]
      ));
    }
    return $payload;
  }
  
  function get_top_web_users(&$index_content) {
    $this->shm = $this->registry->get_singleton("core", "shm_manager");
    $data = $this->shm->get_data_structure(SHM_TOP_WEB_CLIENTS);

    $parser = new Parser($this->tpl_dir);
    $parser->register_modifier("resolve_max", array(&$this, "resolve_max"));
    $parser->register_modifier("resolve_mac", array(&$this, "resolve_mac"));
    $parser->register_modifier("resolve_ip", array(&$this, "resolve_ip"));

    if (is_array($data)) {
      $parser->assign_by_ref("data", &$data);
    }

    $index_content .= $parser->fetch("top_web_users_panel.tpl");
    return;
  }
  
  function get_top_web_users_json() {
    $this->shm = $this->registry->get_singleton("core", "shm_manager");
    $data = $this->shm->get_data_structure(SHM_TOP_WEB_CLIENTS);

    $payload = array();
    foreach ($data as $point) {
      array_push($payload, array(
        "source" => $this->resolve_ip($point[0]),
        "total" => $point[1],
        "rate" => $point[2]
      ));
    }
    return $payload;
  }

  function get_top_ethernet_protocols(&$index_content) {
    $this->shm = $this->registry->get_singleton("core", "shm_manager");
    $data = $this->shm->get_data_structure(SHM_TOP_PROTOCOLS);

    $parser = new Parser($this->tpl_dir);
    $parser->register_function("translate_frame_type", array(&$this, "translate_frame_type"));
    $parser->register_modifier("fix_frame_type_val",   array(&$this, "fix_frame_type_val"));
    $parser->assign_by_ref("data", &$data);

    $index_content .= $parser->fetch("top_protocols_panel.tpl");
    return;
  }

  function translate_frame_type($params, &$smarty) {

    $type = trim($params['type']);
    $proto = $params['proto'];

    if ($type == 800) {
        switch ($proto) {
          case 1:  return "IPv4 (ICMP)";
          case 2:  return "IPv4 (IGMP)";
          case 6:  return "IPv4 (TCP)";
          case 11: return "IPv4 (UDP)";
          default: return "IPv4 (".$proto.")";
        }
      }

      if ($type < 282) {
        return "Ethernet Length Field";
      }

    $ds = array(
      '0200' => 'Xerox PUP (conflicts with 802.3 Length Field range) (see 0A00)',
      '0201' => 'Xerox PUP Address Translation (conflicts ...) (see 0A01)',
      '0600' => 'Xerox NS IDP',
      '0601' => 'XNS Address Translation (3Mb only)',
      '0800' => 'DOD Internet Protocol (IP)',
      '0801' => 'X.75 Internet',
      '0802' => 'NBS Internet',
      '0803' => 'ECMA Internet',
      '0804' => 'CHAOSnet',
      '0805' => 'X.25 Level 3',
      '0806' => 'Address Resolution Protocol (ARP) (for IP and for CHAOS)',
      '0807' => 'XNS Compatibility',
      '081C' => 'Symbolics Private',
      '0900' => 'Ungermann-Bass network debugger',
      '0A00' => 'Xerox IEEE802.3 PUP',
      '0A01' => 'Xerox IEEE802.3 PUP Address Translation',
      '0BAD' => 'Banyan Systems',
      '0BAF' => 'Banyon VINES Echo',
      '1000' => 'Berkeley Trailer negotiation',
      '1234' => 'DCA - Multicast',
      '1600' => 'VALID system protocol',
      '1989' => 'Artificial Horizons ("Aviator" dogfight simulator [on Sun])',
      '1995' => 'Datapoint Corporation (RCL lan protocol)',
      '3C00' => '3Com NBP virtual circuit datagram (like XNS SPP) not registered',
      '3C01' => '3Com NBP System control datagram not registered',
      '3C02' => '3Com NBP Connect request (virtual cct) not registered',
      '3C03' => '3Com NBP Connect repsonse not registered',
      '3C04' => '3Com NBP Connect complete not registered',
      '3C05' => '3Com NBP Close request (virtual cct) not registered',
      '3C06' => '3Com NBP Close response not registered',
      '3C07' => '3Com NBP Datagram (like XNS IDP) not registered',
      '3C08' => '3Com NBP Datagram broadcast not registered',
      '3C09' => '3Com NBP Claim NetBIOS name not registered',
      '3C0A' => '3Com NBP Delete Netbios name not registered',
      '3C0B' => '3Com NBP Remote adaptor status request not registered',
      '3C0C' => '3Com NBP Remote adaptor response not registered',
      '3C0D' => '3Com NBP Reset not registered',
      '4242' => 'PCS Basic Block Protocol',
      '424C' => 'Information Modes Little Big LAN diagnostic',
      '4321' => 'THD - Diddle',
      '4C42' => 'Information Modes Little Big LAN',
      '5208' => 'BBN Simnet Private',
      '6000' => 'DEC unassigned, experimental',
      '6001' => 'DEC Maintenance Operation Protocol (MOP) Dump/Load Assistance',
      '6002' => 'DEC Maintenance Operation Protocol (MOP) Remote Console',
      '6003' => 'DECNET Phase IV, DNA Routing',
      '6004' => 'DEC Local Area Transport (LAT)',
      '6005' => 'DEC diagnostic protocol (at interface initialization?)',
      '6006' => 'DEC customer protocol',
      '6007' => 'DEC Local Area VAX Cluster (LAVC), System Communication Architecture (SCA)',
      '6008' => 'DEC AMBER',
      '6009' => 'DEC MUMPS',
      '6010' => '3Com Corporation',
      '6011' => '3Com Corporation',
      '6012' => '3Com Corporation',
      '6013' => '3Com Corporation',
      '6014' => '3Com Corporation',
      '7000' => 'Ungermann-Bass download',
      '7001' => 'Ungermann-Bass NIUs',
      '7002' => 'Ungermann-Bass diagnostic/loopback',
      '7003' => 'Ungermann-Bass ??? (NMC to/from UB Bridge)',
      '7005' => 'Ungermann-Bass Bridge Spanning Tree',
      '7007' => 'OS/9 Microware',
      '7009' => 'OS/9 Net?',
      '7030' => 'Racal-Interlan',
      '7031' => 'Prime NTS (Network Terminal Service)',
      '7034' => 'Cabletron',
      '8003' => 'Cronus VLN',
      '8004' => 'Cronus Direct',
      '8005' => 'HP Probe protocol',
      '8006' => 'Nestar',
      '8008' => 'AT&T/Stanford Univ. Local use',
      '8010' => 'Excelan',
      '8013' => 'Silicon Graphics diagnostic',
      '8014' => 'Silicon Graphics network games',
      '8015' => 'Silicon Graphics reserved',
      '8016' => 'Silicon Graphics XNS NameServer, bounce server',
      '8019' => 'Apollo DOMAIN',
      '802E' => 'Tymshare',
      '802F' => 'Tigan, Inc.',
      '8035' => 'Reverse Address Resolution Protocol (RARP)',
      '8036' => 'Aeonic Systems',
      '8037' => 'IPX (Novell Netware?)',
      '8038' => 'DEC LanBridge Management',
      '8039' => 'DEC DSM/DDP',
      '803A' => 'DEC Argonaut Console',
      '803B' => 'DEC VAXELN',
      '803C' => 'DEC DNS Naming Service',
      '803D' => 'DEC Ethernet CSMA/CD Encryption Protocol',
      '803E' => 'DEC Distributed Time Service',
      '803F' => 'DEC LAN Traffic Monitor Protocol',
      '8040' => 'DEC PATHWORKS DECnet NETBIOS Emulation',
      '8041' => 'DEC Local Area System Transport',
      '8042' => 'DEC unassigned',
      '8044' => 'Planning Research Corp.',
      '8046' => 'AT&T',
      '8047' => 'AT&T',
      '8048' => 'DEC Availability Manager for Distributed Systems DECamds (but someone at DEC says not)',
      '8049' => 'ExperData',
      '805B' => 'VMTP (Versatile Message Transaction Protocol, RFC-1045) (Stanford) [was Stanford V Kernel, experimental]',
      '805C' => 'Stanford V Kernel, version 6.0',
      '805D' => 'Evans & Sutherland',
      '8060' => 'Little Machines',
      '8062' => 'Counterpoint Computers',
      '8065' => 'University of Mass. at Amherst',
      '8066' => 'University of Mass. at Amherst',
      '8067' => 'Veeco Integrated Automation',
      '8068' => 'General Dynamics',
      '8069' => 'AT&T',
      '806A' => 'Autophon',
      '806C' => 'ComDesign',
      '806D' => 'Compugraphic Corporation',
      '807A' => 'Matra',
      '807B' => 'Dansk Data Elektronik',
      '807C' => 'Merit Internodal (or Univ of Michigan?)',
      '8080' => 'Vitalink TransLAN III Management',
      '809B' => 'EtherTalk (AppleTalk over Ethernet)',
      '809F' => 'Spider Systems Ltd.',
      '80A3' => 'Nixdorf Computers',
      '80C6' => 'Pacer Software',
      '80C7' => 'Applitek Corporation',
      '80D5' => 'IBM SNA Services over Ethernet',
      '80DD' => 'Varian Associates',
      '80F2' => 'Retix',
      '80F3' => 'AppleTalk Address Resolution Protocol (AARP)',
      '80F7' => 'Apollo Computer',
      '8102' => 'Wellfleet; BOFL (Breath OF Life) pkts [every 5-10 secs.]',
      '8103' => 'Wellfleet Communications',
      '812B' => 'Talaris',
      '8130' => 'Waterloo Microsystems Inc.',
      '8131' => 'VG Laboratory Systems',
      '8137' => 'Novell (old) NetWare IPX (ECONFIG E option)',
      '8138' => 'Novell, Inc.',
      '813F' => 'M/MUMPS data sharing',
      '8145' => 'Vrije Universiteit (NL) Amoeba 4 RPC (obsolete)',
      '8146' => 'Vrije Universiteit (NL) FLIP (Fast Local Internet Protocol)',
      '8147' => 'Vrije Universiteit (NL) [reserved]',
      '814C' => 'SNMP over Ethernet (see RFC1089)',
      '814F' => 'Technically Elite Concepts Network Professor',
      '8191' => 'PowerLAN.  NetBIOS/NetBEUI (PC)',
      '817D' => 'XTP',
      '81D6' => 'Artisoft Lantastic',
      '81D7' => 'Artisoft Lantastic',
      '8390' => 'Accton Technologies (unregistered)',
      '852B' => 'Talaris multicast',
      '8582' => 'Kalpana',
      '86DD' => 'IP version 6',
      '8739' => 'Control Technology Inc. RDP Without IP',
      '873A' => 'Control Technology Inc. Mcast Industrial Ctrl Proto.',
      '873B' => 'Control Technology Inc. Proprietary',
      '873C' => 'Control Technology Inc. Proprietary',
      '8820' => 'Hitachi Cable (Optoelectronic Systems Laboratory)',
      '8856' => 'Axis Communications AB. proprietary bootstrap/config',
      '8888' => 'HP LanProbe test?',
      '9000' => 'Loopback (Configuration Test Protocol)',
      '9001' => '3Com (Formerly Bridge Communications), XNS Systems Management',
      '9002' => '3Com (Formerly Bridge Communications), TCP/IP Systems Management',
      '9003' => '3Com (Formerly Bridge Communications), loopback detection',
      'AAAA' => 'DECNET? Used by VAX 6220 DEBNI',
      'FAF5' => 'Sonix Arpeggio',
      'FF00' => 'BBN VITAL-LanBridge cache wakeups',
      '34525' => "IPv6"
    );

    #$type = str_pad($type, 4, "0", STR_PAD_LEFT);
    $type = $this->fix_frame_type_val($type);

    if (in_array($type, array_keys($ds))) {
      return $ds[$type];
    } else {
      return "Unknown [$type]";
    }
  }

  function fix_frame_type_val($val) {

    $val = trim($val);

    if (intval($val) > 0) {
      return sprintf("%04d", $val);
    } else {
      return sprintf("%04s", $val);
    }
  }


  # Attempts to resolve a MAC address to the best possible match (hostname) and fails-over to IP.
  function resolve_max($mac) {
    # Allocate some space for our cache
    if (!is_array($this->cache['macs'])) {
      $this->cache['macs'] = array();
    }


    if ($this->cache['macs'][$mac]) {
      return $this->resolve_ip($this->cache['macs'][$mac]['ip']);
    }


    $q_mac = $this->db->escape($mac);

    $query = "SELECT ip FROM arptable WHERE mac = $q_mac ORDER BY timestamp DESC LIMIT 1";

    $res = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return $mac;
    }

    $this->cache['macs'][$mac] = $res;

    return $this->resolve_ip($res['ip']);
  }

  # Tries to resolve a MAC address into either an IP, or a hostname if possible.
  function resolve_mac($mac) {

    # Allocate some space for our cache
    if (!is_array($this->cache['macs'])) {
      $this->cache['macs'] = array();
    }


    if ($this->cache['macs'][$mac]) {
      return $this->cache['macs'][$mac]['ip'];
    }


    $q_mac = $this->db->escape($mac);

    $query = "SELECT ip FROM arptable WHERE mac = $q_mac ORDER BY timestamp DESC LIMIT 1";

    $res = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return $mac;
    }

    $this->cache['macs'][$mac] = $res;

    return $res['ip'];


  }

  # Attempts to resolve the hostname associated to an IP.
  function resolve_ip($ip) {

    $q_ip = $this->db->escape($ip);

    # Allocate some space for our cache
    if (!is_array($this->cache['ips'])) {
      $this->cache['ips'] = array();
    }

    if ($this->cache['ips'][$ip]) {
      $this->debugger->add_hit("$ip -> " . $this->cache['ips'][$ip]['hostname'] . " (cached)");
      return $this->cache['ips'][$ip]['hostname'];
    }

    $query = "SELECT hostname, ip, host_name_type
          FROM hosts
          WHERE ip = $q_ip
          ORDER BY timestamp DESC";

    $this->db->enable_caching();
    $res = $this->db->select($query);
    $this->db->restore_previous_state();


    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      $this->cache['ips'][$ip] = array("hostname" => $ip);
      return $ip;
    }

    foreach($res as $current) {
      if ($current['host_name_type'] == 'CUSTOM') {
        $this->cache['ips'][$ip] = array("hostname" => $current['hostname']);
        return $current['hostname'];
      }
    }


    $this->cache['ips'][$ip] = array("hostname" => $res[0]['hostname']);
    $this->debugger->add_hit("$ip -> " . $res[0]['hostname']);
    return $res[0]['hostname'];

  }

  function get_discovered_hosts(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $res = $this->get_discovered_hosts_array();
    if ($res) {
                  $parser->assign_by_ref("hosts", &$res);
                  $index_content .= $parser->fetch("discovered_hosts.tpl");
                }
    return;
  }
  
        function get_discovered_hosts_array() {
          $query = "SELECT * FROM arptable WHERE is_new = 't' ORDER BY timestamp DESC LIMIT 2000";
          $res = $this->db->select($query);

          if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
                  return NULL;
          }
          return $res;
        }

  function clear_discovered_hosts(&$index_content) {

    if ((is_array($_POST['host_ids'])) && (sizeof($_POST['host_ids']) > 0)) {
      $seq = "";
      foreach($_POST['host_ids'] as $host) {
        $seq .= $this->db->escape($host) . ", ";
      }
      $seq = substr($seq, 0, strlen($seq)-2);

      $query = "UPDATE arptable SET is_new = 'f' WHERE id IN ($seq)";

      $res = $this->db->update($query);

      if (DB_QUERY_ERROR == $res) {
        $this->err->err_from_string("Unable to delete specified hosts from the recently discovered hosts table");
        return;
      }
    }

    if (FALSE !== $res) {
      $index_content .= message_bar("Specified host records cleared successfully");
    }
    return $this->get_discovered_hosts($index_content);

  }
  
  /**
   * Displays the local traffic sniffers' protocol breakdown data
   * only if the local traffic sniffers (mod_ip) are turned on.
   * 
   * @param pointer $index_content
   * @author xavier@netmon.ca
   */
  function get_protocol_graph(&$index_content) {
    $ms = require_module("mod_settings");
    $services = $ms->_get_services();
    $up = FALSE;
    
    # Look for netmond
    foreach($services['services']['service'] as $service) {
      if ($service['name'] == 'netmond') {
        # if netmond isn't running, don't even look at mod_ip
        if (strcmp($service['status'], 'up') <> 0) {
          break;
        }
        
        foreach($service['plugins']['plugin'] as $plugin) {
          if ($plugin['name'] == 'mod_ip') {
            $ifaces_root = $plugin['interfaces']['interface'];
            // This could be an individual interface, or a list of interfaces.
            // Thanks to PEAR::XML_Unserializer, there's no way to know until it's too late!

            if (in_array('iface', array_keys($ifaces_root), TRUE)) {
              $status = $ifaces_root['status'];
              $this->debugger->add_hit("status for root iface " . $ifaces_root['name'] . " is " . $ifaces_root['status'], NULL, NULL, vdump(array_keys($ifaces_root)));
            } else {
              foreach($ifaces_root as $iface) {
                $status = $iface['status'];
                $this->debugger->add_hit("Interface " . $iface['iface'], NULL, NULL, vdump($iface));
                $this->debugger->add_hit("status for iface " . $iface['iface'] . " is " . $status);
                if (strtoupper($status) == 'UP') {
                  break;
                }
              }
            }
            
            if ('up' == strtolower($status)) {
              $up = TRUE;
              $this->debugger->add_hit("Some interfaces are up");
            }
          } else {
            $this->debugger->add_hit($plugin['name'] . " != mod_ip");
          }
        }
      }
    }
    
    if (TRUE == $up) {
      $parser = new Parser($this->tpl_dir);
      $index_content .= $parser->fetch("get_protocol_graph.tpl");
    }
  }
}

?>
