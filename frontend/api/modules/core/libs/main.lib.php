<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  * Finds a module and instanciate it.
  *
  * This function will attempt to locate a module based on
  * naming convention. It will then instanciate the module
  * and return a copy of it (since PHP doesn't seem to support
  * returning direct references?!?
  *
  * Additionally, please note that error control is not yet
  * implemented at this point
  *
  * @param string $mod
  * @return MadnetModule
  */
function require_module($mod) {
	$mod = strtolower($mod);

	if (!file_exists(MODS_PATH . "$mod/classes/$mod.class.php")) {
		$registry = Registry::get_registry();
		$err = $registry->get_singleton("core", "error_manager");
		$err->err_from_string("The module $mod does not exist or does not have a MCO controller.");
		return FALSE;
	}

	require_once(MODS_PATH . "$mod/classes/$mod.class.php");
	return new $mod;
}

/**
  * Finds a class and load it it.
  *
  * Through the use of naming convention, this function
  * will attempt to find a class based on a module name
  * and class name. It then calls require_once on that
  * file to load it in memory.
  *
  * @param string $module
  * @param string $class
  * @return void
  */
function require_class($module, $class) {
	$module = strtolower($module);
	$class = strtolower($class);

	if (!file_exists(MODS_PATH . "$module/classes/$class.class.php")) {
		$registry = Registry::get_registry();
		$err = $registry->get_singleton("core", "error_manager");
		$err->err_from_string("The class $class could not be found in module $module.");
		return FALSE;
	}

	require_once(MODS_PATH . "$module/classes/$class.class.php");
	return TRUE;
}

/**
  * Breaks up a string and capitalize it
  *
  * This function replaces underscores with spaces in the
  * argument string, and capitalize every word in the string.
  * converting authentication_module by "Authentication Module"
  *
  * Since MADNET makes extensive use of strict naming convention,
  * this allows you to use native object or variable names as
  * friendly labels for the end-user.
  *
  * @param string $string
  * @return string
  */
function mktitle($string) {
	return ucwords(str_replace("_", " ", $string));
}

/**
  * PHP5 Only - Loads a module automatically
  *
  * If a developer attempts to instanciate an object
  * without loading the appropriate class in memory,
  * the PHP interpreter will typically crash.
  *
  * With PHP5, the __autoload() function is called before
  * the interpreter dies. We use this to attempt to find
  * the appropriate class and load it in memory, but this
  * will not work without PHP5.
  *
  * @param string $mod class name
  * @return void
  */
function __autoload($mod) {
	$registry = Registry::get_registry();
	$debugger = $registry->get_singleton("core", "debugger");
	$debugger->add_hit("Autoload function called on module $mod");
	require_class("core", $mod);
}

/**
  * Performs stripslashes on every level of a variable or array
  *
  * Magic_quotes is one of the few PHP arguments that cannot be set
  * through .htaccess or ini_set() and can only be set at the php.ini
  * level. Magic Quotes are EVIL.
  *
  * This function will go through all the recursion levels of an
  * array and remove all the magic quotes.
  *
  * @example "Removing all magic_quotes recursively" sope($_GET); sope($_POST); sope($_COOKIE);
  * @param mixed/array $var
  * @return mixed/array
  */
function sope($var) {
	if (is_array($var)) {
		$var = array_map("sope", $var);
		return $var;
	} else {
		return stripslashes($var);
	}
}

/**
  * Returns the result of var_dump
  *
  * Typically, the var_dump function does a dump of a variable
  * directly in the output of the script. This means you cannot
  * capture the content of a var_dump in a variable.
  *
  * PHP5 allows you to specify an argument and capture it but
  * PHP4 does not. This function initiates output buffering,
  * calls var_dump, then captures the content of the output
  * buffer in a variable, cleans up the buffer and returns the
  * new value.
  *
  * @example "Storing the var dump of the Session stack" $session = vdump($_SESSION);
  * @param mixed $var
  * @return string
  */
function vdump($var) {
	if (__DEBUG__ == True) {
		ob_start();
		var_dump($var);
		return ob_get_clean();
	}
	return;
}



/**
  * Returns the template directory associated with a module
  *
  * @param string $mod This is the name of the module
  * @return string
  *
  */
function get_tpl_dir($mod) {
	return get_x_dir($mod, "tpl") . FORMAT . "/" . LANG . "/";
}

/**
  * Returns the classes directory associated with a module
  *
  * @param string $mod This is the name of the module
  * @return string
  *
  */
function get_classes_dir($mod) {
	return get_x_dir($mod, "classes");
}

/**
  * Returns the configuration directory associated with a module
  *
  * @param string $mod This is the name of the module
  * @return string
  *
  */
function get_conf_dir($mod) {
	return get_x_dir($mod, "conf");
}

/**
  * Returns the libraries directory associated with a module
  *
  * @param string $mod This is the name of the module
  * @return string
  *
  */
function get_libs_dir($mod) {
	return get_x_dir($mod, "libs");
}

/**
  * Returns the root directory associated with a module
  *
  * @param string $mod This is the name of the module
  * @return string
  *
  */
function get_root_dir($mod) {
	return get_x_dir($mod, "");
}

/**
  * Returns a dynamic directory name based on a module name and dir type
  *
  * @param string $mod This is the name of the module
  * @param string $x name of the directory
  * @return string
  *
  */
function get_x_dir($mod, $x) {
	return MODS_PATH . strtolower($mod) . "/" . strtolower($x) . "/";
}

/**
  * Returns TRUE of a module has been installed, FALSE otherwise
  *
  * @param  string $module Name of the module
  * @return boolean
  */
function module_exists($module) {
	# We must check that there is a directory allocated for this module
	# But also that the module is valid and healthy.
	# For now, we'll just check that we have something in there.
	return is_file(MODS_PATH . $module . "/classes/" . $module . ".class.php");
}

/**
  * Logs a string to log.txt
  *
  * Great for session debugging, it will also be useful once we implement
  * the TXTFILE back-end for the debugger.
  *
  * @param string $str
  * @return void
  *
  */
function logstr($str) {
	$fp = fopen("/var/log/netmon/middleware.log", "a");
	fputs($fp, date("D/m/y G:i:s") . " - " . $str . "\n");
	fclose($fp);
}

/**
  * Smarty handler to return a number with 3-digit precision (for debugger)
  *
  * @param float $num
  * @return float
  */

function dec_fix($num) {
	return number_format($num, 3);
}


/**
  * Returns true if current user has a specific permission bit turned on
  *
  * This function itterates through the session permission stack.
  * It will attempt to locate a permission bit or permission GROUP with
  * identical name and return TRUE as soon as it finds one.
  *
  * @param string $perm
  * @return boolean
  */
function has_perm($perm) {
	$perm = strtolower(str_replace(" ", "_", $perm));
	if (is_array($_SESSION['perms'])) {
		if (in_array($perm, array_keys($_SESSION['perms']))) { return TRUE; }
		else {
			foreach($_SESSION['perms'] as $current) {
				if (in_array($perm, $current)) { return TRUE; }
			}
		}
	}
	return FALSE;
}



function message_bar($str) {
	return <<<EOM
	<div class="panel" style="background-color: #FFFF80"><img src="assets/icons/info.gif" width="16" height="16" align="absmiddle">&nbsp; $str</div>
EOM;
}


function resolve_ip() {

	if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown")) {
		# PHP Educated guess.
		return $_SERVER['REMOTE_ADDR'];
	}

	if(getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) {
		# Direct request
		return getenv("REMOTE_ADDR");
	}

	if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) {
		# Proxy or NAT transparent proxy
		return getenv("HTTP_CLIENT_IP");
	}

	if(getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) {
		# Proxy redirection
		return getenv("HTTP_X_FORWARDED_FOR");
	}
	return "unknown IP";
}


function get_system_load() {
	$parts = array();
	eregi(".*load average: ([0-9\.]+), [0-9\.]+, [0-9\.]+", `uptime`, $parts);
	return $parts[1];
}

?>
