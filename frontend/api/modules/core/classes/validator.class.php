<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */




/**
  * @package    MADNET
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * Validator Object
  */
class Validator {


  /*** Attributes: ***/

  var $err;

  var $registry;

  /**
   * Validator class constructor
   *
   * Instanciates the registry as well as an error handler singleton
   *
   * @return bool
   * @access public
   */
  function validator() {

    # Instanciate the registry (Abstract Factory & Singleton)
    $this->registry = Registry::get_registry();

    # Error handler (MUST be a singleton)
    $this->err    = $this->registry->get_singleton("core", "error_manager");

  }


  /**
   * Main validation rules wrapper
   *
   * This method determines if a validation rule exists for
   * the specified datatype. If it does, passes a pointer
   * to the data structure to validate to the proper
   * method.
   *
   * @param string $type
   * @param pointer $val
   * @return boolean
   */
  function validate($type, &$val) {
    $method = "validate_" . $type;
    if (method_exists($this, $method)) {
    #if (in_array($type, $this->rules)) {

      #return call_user_func_array($method, array(&$val));

      return $this->$method($val);
    } else {
      $this->err->err_from_string("No validation rule exists for the data type &quot;$type&quot;");
      return FALSE;
    }
  }


  /**
   * Validates strings
   *
   * Returns TRUE if the argument is a string, FALSE otherwise
   *
   * @param pointer $string
   * @return boolean
   */
  function validate_string(&$string) {
    return is_string($string);
  }

  /**
   * Validates Integers
   *
   * Returns TRUE if the argument is an integer, FALSE otherwise
   *
   * @param pointer $int
   * @return boolean
   */
  function validate_integer(&$int) {

    # is_int seems to look at the variable type. this looks at content.
    if (eregi("[0-9]{1,10}", $int)) {
      $int = intval($int);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Validates Email addresses
   *
   * Returns TRUE if the argument is a proper email address, FALSE otherwise
   * At this point, the method does not attempt to fix the argument pointer.
   *
   * @param pointer $string
   * @return boolean
   */
  function validate_email(&$string) {
    $reg = "^['_a-z0-9-]+(\.['_a-z0-9-]+)*@[a-z0-9-]+(\.[_a-z0-9-]+)*(\.[a-z]{2,4})$";
    $string = strtolower($string);
    return (eregi($reg, $string) ? TRUE : FALSE);
  }


  /**
   * Validates Passwords
   *
   * Returns TRUE if the argument is a proper password, FALSE otherwise
   *
   * @param pointer $string
   * @return boolean
   */
  function validate_password(&$string) {
    
    if (strlen($string) < 4) {
      if (strlen($string) > 0) {
        $this->err->err_from_string("Password field must be at least 4 characters in length");
      }
      return FALSE;
    }
    

    # If the password is valid, encrypt it. (This works since we were passed a pointer)
    $this->encrypter = $this->registry->get_singleton("core", "encryption_manager");

    $string = $this->encrypter->encrypt($string);


    return TRUE;
  }


  function validate_ip_address(&$ip) {
    #return ereg("([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})", $ip);
    $ip = trim($ip);
    $regex = '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)';
    return ereg($regex, $ip);
  }



  function validate_char(&$char) {
    return strlen($char) == 1;
  }


  function validate_pg_bool(&$str) {
    $str = strtolower($str);
    if (strcmp($str, 'true') != 0 && strcmp($str, 't') != 0) {
      $str = 'false';
    } 
    return TRUE;
  }


  function validate_url(&$str) {
    return preg_match('_^(?:([^:/?#]+):)?(?://([^/?#]*))?([^?#]*)(?:\?([^#]*))?(?:#(.*))?$_', $str, $tmp);
  }
  
  function validate_xss_free_string(&$str) {
    $str = htmlentities($str);
    return $this->validate_string($str);
  }


}


?>