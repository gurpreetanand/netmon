<?php

/** WARNING - This class is used for validation purposes only  **/
class Activation_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "netmon";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = FALSE;/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "core";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	/**
	 * Initialization Method
	 *
	 */
	function init() {
		$this->params->add_primitive("registration_key",   "string", TRUE,  "Registration Key");
		$this->params->add_primitive("company_name",       "string", TRUE,  "Company Name");
		$this->params->add_primitive("company_address",    "string", TRUE,  "Company Address");
		$this->params->add_primitive("company_city",       "string", TRUE,  "Company City");
		$this->params->add_primitive("company_country",    "string", TRUE,  "Company Country");
		$this->params->add_primitive("company_state",      "string", FALSE, "Company State/Province");
		$this->params->add_primitive("contact_first_name", "string", TRUE,  "Contact First Name");
		$this->params->add_primitive("contact_last_name",  "string", TRUE,  "Contact Last Name");
		$this->params->add_primitive("contact_email",      "email",  TRUE,  "Contact Email Address");
		$this->params->add_primitive("contact_phone",      "string", TRUE,  "Contact Phone Number");
		$this->params->add_primitive("contact_phone_ext",  "string", FALSE, "Contact Phone Extension");
	}
}
