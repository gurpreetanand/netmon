<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
 * class Filters_Manager
 * Performs filter related operation with the filters XML files as models
 */
class Filters_Manager
{

  var $filter_containers = array();

  /** Aggregations: */

  /** Compositions: */

  /*** Attributes: ***/

  function Filters_Manager() {
    $this->options = array(
      'complexType'       => 'array',
      'parseAttributes'   => TRUE,
      'attributesArray'   => FALSE,
      'contentName'       => 'panels'
    );

    # Initializes the singleton registry
    $this->registry = Registry::get_registry();

    # Error manager singleton
    $this->err   = $this->registry->get_singleton("core", "error_manager");

    # Debugger singleton
    $this->debug = $this->registry->get_singleton("core", "debugger");


    # Load the XML Unserializer in memory
    require_once "XML/Unserializer.php";

    # Instanciate the XML Unserializer object
    $this->xml = new XML_Unserializer($this->options);





    $dir = opendir("/apache/registry/filters/");

    # Create the keys for our hash-table based on the file entries in the registry
    while ($file = readdir($dir)) {
      # Filter out junk file entries
      if (!in_array($file, array(".", ".."))) {
        # Make sure we are dealing with an XML file
        if (strcmp(substr($file, strlen($file)-4), ".xml") == 0) {
          # Unserialize the content of the file.
          $result = $this->xml->unserialize('/apache/registry/filters/' . $file, true);

          # Error control
          if (PEAR::isError($result)) {
            $this->err->err_from_string("Unable to parse the $filter filter. Please ensure it is valid XML data and that I have permission to access it");
          }

          # Create a hash-table key and dump the unserialized data-structure inside.
          $this->filter_containers[strtolower(str_replace(".xml", "", $file))] = $this->xml->getUnserializedData();
        }
      }
    }

    closedir($dir);

    $this->debug->add_hit("Filters Registry " . "[" . ((__STACK_TRACE__ == TRUE) ? (xdebug_call_class() . "/" . xdebug_call_function()) : "Unknown") . "]", NULL, NULL, vdump(&$this->filter_containers));

  }

  function is_valid_filter($filter_type, $filter_name) {
    $filters = ($this->filter_containers[strtolower($filter_type)]);

    # If we are dealing with a multi-dimensional array, we have multiple filters
    # Otherwise, just one.

    if (!$filters || !is_array($filters) || !is_array($filters['filter'])) {
      return FALSE;
    } else if (is_array($filters['filter'][0])) {
      foreach($filters['filter'] as $current) {
        if ($current['name'] == $filter_name) {
          return TRUE;
        }
      }
    } else {
      if ($filters['filter']['name'] == $filter_name) {
        return TRUE;
      }
    }

    return FALSE;

  }

    function get_filter_data($filter_type, $filter_name) {
    $filters = ($this->filter_containers[strtolower($filter_type)]);

    # If we are dealing with a multi-dimensional array, we have multiple filters
    # Otherwise, just one.
    if (is_array($filters['filter'][0])) {
      foreach($filters['filter'] as $filter) {
        if (strcmp(trim(strtolower($filter['name'])), strtolower($filter_name)) == 0) {
          if (is_array($filter['param'])) {
            if (($filter_type == 'traffic') && (!is_array($filter['param'][0]))) {
              return array($filter['param']);
            } else {
              return $filter['param'];
            }
          } else {
            return array($filter['param']);
          }
        }
      }
    } else {
      if (strcmp(trim(strtolower($filters['filter']['name'])), trim(strtolower($filter_name))) == 0) {
        return (is_array($filters['filter']['param']) ? $filters['filter']['param'] : array($filters['filter']['param']));
      } else {
        $this->debug->add_hit(strtolower($filters['filter']['name']) . " != " . strtolower($filter_name));
      }
    }
    return FALSE;
  }


} // end of Filters_Manager
?>
