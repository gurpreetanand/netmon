<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  * Environment Manager.
  *
  * Handles the Application Environment  This class is responsible for maintaining a
  * coherent operating environment.  It is in charge of loading and maintaining the
  * application's configuration file(s) and cleaning up GET/POST/COOKIE variables.
  *
  * @package MADNET
  * @authod Xavier Spriet
  *
  */
class Env_Manager
{

	/** Aggregations: */

	/** Compositions: */

	 /*** Attributes: ***/

	 /**
	   * Env_Manager constructor method
	   *
	   * Loads in memory the core module objects that we will need to interact with
	   * during the request handling lifecycle.
	   *
	   * @return Env_Manager
	   */
	 function Env_Manager() {
	 	@include_once(MODS_PATH . "core/libs/main.lib.php");
	 	@include_once(MODS_PATH . "core/libs/load_dependencies.php");


	 	# Core function library (shortcut functions for module access and control)
	 	if (!@include_once(INCLUDE_PATH . "DB.php")) {
	 		require_class("core", "registry"          );
	 		require_class("core", "error_manager"     );
			$em = new Error_Manager();
			$em->handle_fatal("You must install PEAR::DB in order to run this application.");
	 	}



	 	$this->env_clean();
	 	require_class("core", "error_manager"     );
	 	require_class("core", "registry"          );
	 	require_class("core", "debugger"          );
	 	require_class("core", "dispatcher"        );
		require_class("core", "session_manager"   );
		require_class("core", "madnetmodule"      );
		require_class("core", "madnetelement"     );
		require_class("core", "madnetsubelement"  );
		require_class("core", "params_manager"    );
		require_class("core", "parser"            );
	 }




	/**
	 * Cleans up the operating environment.
	 * Recursively gets rid of the stupid magic_quotes on GET/POST/COOKIE,
	 * perform basic typecasting and type conversion on common query parameters,
	 * and deletes junk parameters.
	 *
	 * @return bool
	 * @access private
	 */
	function env_clean( )
	{
		/**
		  * Force magic_quote_gpc = off
		  */
		  if (get_magic_quotes_gpc() == TRUE) {
			$_POST = sope($_POST);
			$_GET = sope($_GET);
			$_COOKIE = sope($_COOKIE);
		}
	} // end of member function env_clean



} // end of Env_Manager
?>
