<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * Session Manager Class
  *
  * Session Handling Facility  This class uses the Bridge pattern to instanciate
  * session drivers on the fly.  Session drivers are very easy to write and can use
  * any back-end available on the underlying operating system such as SHM, file
  * system, cache daemons, PEAR-DB compliant RDBMS, etc...
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class Session_Manager
{

  /** Aggregations: */

  /** Compositions: */

   /*** Attributes: ***/

  /**
   * Validity path for the session cookie
   * @var $cookie_path
   * @access private
   */
  var $cookie_path = SESSION_PATH;
  /**
   * Name of the driver to use for session handling
   * @var $driver_type
   * @access private
   */
  var $driver_type = SESSION_DRIVER;
  /**
   * Content of the session data stack
   * @var $stack
   * @access private
   */
  var $stack = array();
  /**
   * Name of the session cookie
   * var $name
   * @access private
   */
  var $name = SESSION_NAME;
  /**
   * Time To Live (TTL) of the session
   * @var $TTL
   * @access private
   */
  var $TTL = SESSION_DEFAULT_TTL;

  /**
   * Initiates a new session  This constructor initiates all the settings of the new
   * session (name, TTL, cookie paths, etc..) It then initiates the actual session,
   * sets itself  up as the session handler, and initiates the session driver
   * specified.
   *
   * @return Session_Manager
   * @access public
   */
  function Session_Manager()
  {

    require_class("core", "session_drivers/" . $this->driver_type);

    $this->set_name(SESSION_NAME);
    $this->set_cookie_path(SESSION_PATH);
    $this->set_TTL(SESSION_DEFAULT_TTL);


    # Initialize the driver
    $this->driver = new $this->driver_type;

    $this->driver->_set_ttl($this->get_TTL());
    # Set the session name


    session_name($this->name);



    # Session handler
    session_set_save_handler(
              array(&$this, "open"),
              array(&$this, "close"),
              array(&$this, "read"),
              array(&$this, "write"),
              array(&$this, "destroy"),
              array(&$this, "gc")
              );

    #session_set_cookie_params($this->get_TTL(), $this->cookie_path, TOP_DOMAIN);
    #session_cache_limiter('no-cache');
    session_set_cookie_params($this->get_TTL());

    #setcookie("MADNET", session_id(), time() + $this->get_ttl());
    session_cache_expire($this->get_TTL()/TIME_MINUTE);
    #
    session_start();
    setcookie(session_name(), session_id(), time()+$this->get_TTL(), '/');
    

    if ($_SESSION['id']) {
      $this->set_user_id($_SESSION['id']);
    }

  } // end of member function Session_Manager


  /**
   * Opens the session handler
   *
   * This method is called when a session gets created.
   * We use it here to call the garbage collector and
   * pass it over to our driver in case it needs it for
   * anything
   *
   * @param path (useless)
   * @param name (useless)
   * @return unknown
   */
  function open($path, $name) {
    $this->gc(0);
    return $this->driver->_open($path, $name);
  }

  /**
   * Closes the session (does not destroy it.)
   *
   * @return boolean
   */
  function close() {
    return $this->driver->_close();
  }

  /**
   * Asks the driver to destroy the current session
   *
   * @return boolean
   */
  function destroy($session_id) {
    return $this->driver->_destroy($session_id);
  }

  /**
   * Returns the current TTL
   *
   * @return int
   */
  function get_TTL() {
    return $this->TTL;
  }

  /**
   * Sets the new TTL for the new session
   *
   * This does not affect sessions that have already been started
   *
   * @return void
   */
  function set_TTL($ttl) {
    $this->TTL = $ttl;
  }

  /**
   * Returns the path for the session cookie
   *
   * @return string
   */
  function get_cookie_path() {
    return $this->cookie_path;
  }

  /**
   * Sets the path for the session cookie
   *
   * @return void
   */
  function set_cookie_path($cookie_path) {
    $this->cookie_path = $cookie_path;
  }

  /**
   * Returns the name of the session handling driver
   *
   * @return string
   */
  function get_driver_type() {
    return $this->driver_type;
  }

  /**
   * Sets the driver name for session handling
   *
   * @return void
   */
  function set_driver_type($driver) {
    $this->driver_type = $driver;
  }

  /**
   * Returns the name of the session cookie
   *
   * @return string
   */
  function get_name() {
    return $this->name;
  }

  /**
   * Sets the name of the session cookie
   *
   * @return string
   */
  function set_name($name) {
    $this->name = $name;
  }

  /**
   * Returns the session data stack (pulls it from driver)
   *
   * @return string
   */
  function read($session_id) {
    // read the whole session, not just an element
    return $this->driver->_read($session_id);
  }

  /**
   * Push the session data to the driver for a commit
   *
   * @return void
   */
  function write($session_id, $stack) {
    // write the whole session, not just an element
    return $this->driver->_write($session_id, $stack);
  }

  /**
   * Garbage collection (statistical-probability based)
   *
   * @return boolean
   */
  function gc($useless_session_ttl) {
    // Perform garbage collection based on statistical probability approach
    #if (rand(0, 100) <= 10) {
      return $this->driver->_gc($this->get_TTL());
    #}
  }

  /**
    *
    * Tracks system users in session tables or storage system so they
    * can be deleted in real-time when an account is deleted.
    *
    * @return boolean
    *
    */
  function set_user_id($id) {
    return $this->driver->set_user_id($id);
  }

} // end of Session_Manager
?>
