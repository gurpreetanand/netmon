<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

// core.class.php

/**
  * Core module
  *
  * This is a public MadnetModule object.
  *
  * @package MADNET
  * @author  Xavier Spriet
  */
Class Core extends MadnetModule {

	/**
	  * This is initialized by the MadnetModule constructor.
	  *
	  * @return void
	  */
	function init() {
		# XML Unserializer options
		$this->options = array(
			'complexType'       => 'array',
			'parseAttributes'   => TRUE,
			'attributesArray'   => FALSE,
			'contentName'       => 'panels'
		);
	}

	/**
	  * Returns parsed template with authentication dialog
	  *
	  * @param pointer $index_content
	  * @return string
	  * @access public
	  */
	function login_dialog(&$index_content) {

		if (!$this->_device_registered()) {
			return $this->activation_dialog($index_content);
		}
		
		$query = "SELECT n.*, EXTRACT('epoch' FROM expires) AS expire_epoch FROM netmon n";
		$res = $this->db->get_row($query);

		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			$this->err->handle_fatal("Unable to retrieve registration information. The database server may be down or your registration key is invalid");
			return "Error";
		}
		
		$count_query = "SELECT count_unique_ips() AS \"ips\"";
		
		$count_res = $this->db->get_row($count_query);
		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			$res['devices_used'] = '0';
		} else {
			$res['devices_used'] = $count_res['ips'];
		}
		
		$now = mktime();
		if ($res['expire_epoch'] <= $now) {
			$res['days_remaining'] = 0;
		} else {
			$res['days_remaining'] = ceil(($res['expire_epoch'] - $now) / TIME_DAY);
		}
		
		
		$query = "SELECT count(id) AS user_count FROM users";
		$u_res = $this->db->get_row($query);
		
		if (intval($u_res['user_count']) > 0) {
			$first_launch = FALSE;
		} else {
			$first_launch = TRUE;
		}

		$parser = new Parser($this->tpl_dir);
		$parser->assign_by_ref("ds", $res);
		$parser->assign_by_ref("first_launch", $first_launch);
		$index_content .= $parser->fetch("login_dialog.tpl");
		return "Authentication Required";
	}

	/**
	  * Displays a parsed template for the AJAX debugger
	  *
	  * @param pointer $index_content
	  * return string
	  */
	function display_ajax_debugger(&$index_content) {
		$parser = new Parser($this->tpl_dir);
		$index_content .= $parser->fetch("ajax_debugger.tpl");
	}

	/**
	  * Displays a parsed template for the Netmon Toolbar
	  *
	  * @param pointer $index_content
	  * return string
	  */
	function display_toolbar(&$index_content) {
		$parser = new Parser($this->tpl_dir);
		$parser->assign("perms", $this->get_perms());
		$index_content .= $parser->fetch("toolbar.tpl");
	}

	function blank(&$index_content) {
		$index_content .= "&nbsp;";
		return "&nbsp;";
	}

	/**
	 * Returns the parsed XML for components (for flash toolbar)
	 *
	 * @param pointer $index_content
	 * @DEPRECATED - We have replaced the flash toolbar.
	 */
	function get_components(&$index_content) {

		#var_dump($_SESSION);
		$parser = new Parser($this->tpl_dir);
		$parser->assign("perms", $this->get_perms());
		$index_content .= $parser->fetch("toolbar_components.xml.tpl");
		return;
	}

	function _has_perm($params) {
		return (has_perm($params['perm']));
	}

	function get_perms() {
		$perms = $_SESSION['perms'];
		$res = array();

		foreach($perms as $key => $val) {
			$res[$key] = array();
			foreach($val as $v_key => $v_val) {
				$res[$key][$v_val] = '1';
			}
		}
		return $res;
	}
	
	/** Does nothing. Used by AJAX keepalive system embeded in the toolbar.
	  * The AJAX ping knows that if we get SESS_OK back, we can schedule
	  * the next ping, otherwise stop all scheduling
	  */
	
	function ping() {
		echo "SESS_OK";
		return;
	}
	
	/**
	 * Displays the device activation dialog
	 *
	 * @param pointer $index_content
	 */
	function activation_dialog(&$index_content) {
		
		
		$query = "SELECT n.*, EXTRACT('epoch' FROM expires) AS expire_epoch FROM netmon n";
		$res = $this->db->get_row($query);
		
		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			$res = array();
			$res['days_remaining'] = 0;
		} else {
			$now = mktime();
			if ($res['expire_epoch'] <= $now) {
				$res['days_remaining'] = 0;
			} else {
				$res['days_remaining'] = ceil(($res['expire_epoch'] - $now) / TIME_DAY);
			}
		}
		
		
		$parser = new Parser($this->tpl_dir);
		$parser->assign("activation", $res);
		$index_content .= $parser->fetch("activation_dialog.tpl");
		return "Product Activation";
	}
	
	/**
	 * Process activation tokens
	 *
	 * @param pointer $index_content
	 */
	function activate(&$index_content) {
		
		require_class($this->module, "activation_manager");
		$activator = new Activation_Manager();

		if (!$activator->params->validate_primitives()) {
			foreach($this->err->errors_collection as $error) {
				echo '<strong>' . $error['message'] . "</strong><br />\n";
			}
			return;
		}
		
		
		$fields = array("registration_key", "company_name", "company_address", "company_city",
						"company_state", "company_country", "contact_first_name",
						"contact_last_name", "contact_email", "contact_phone",
						"contact_phone_ext");
		
		$seq = "";
		$input = json_decode(file_get_contents("php://input"), true);

		foreach($fields as $field) {
			if (($input[$field]) && (strcmp("", $input[$field]) <> 0)) {
				$cli_field = str_replace("_", "-", $field);
				$seq .= "--$cli_field " . escapeshellarg($input[$field]) . " ";
			}
		}
		$cmd = "sudo python /apache/cli/activate.py -q $seq";
		logstr($cmd);
		echo trim(`$cmd`);
	}
	
	/**
	 * Returns TRUE if the device appears to be registered, and FALSE otherwise.
	 * This is not used to enforce registration, but only to inform users of what
	 * the problem might be if their registration data has gone MIA.
	 *
	 * @return bool
	 */
	function _device_registered() {
		$query = "SELECT n.*, EXTRACT('epoch' FROM expires) AS expire_epoch FROM netmon n";
		$res = $this->db->get_row($query);

		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			return FALSE;
		}
		
		if (('1' == $res['is_trial']) && (mktime(23,59,59) >= $res['expire_epoch'])) {
			return FALSE;
		}
		
		if (strcmp($res['activation_key'], '') == 0) {
			return FALSE;			
		}
		
		return TRUE;
	}
	
	/**
	 * Returns TRUE if the device restrictions in the Netmon license for
	 * this system allow the addition of the specified device.
	 *
	 * @param string $new_ip
	 * @param string $old_ip
	 */
	function check_device_limit($new_ip, $old_ip = NULL) {
		
		#if (strcmp($new_ip, $old_ip) == 0) {
		#	return TRUE;
		#}
		
		$this->debugger->add_hit("Checking device restrictions...");
		$this->debugger->add_hit("New IP: $new_ip -  Old IP: $old_ip");
		
		$services = array("devices", "df_servers", "servers", "smb_servers", "syslog_access");
		
		$query = "SELECT * FROM list_unique_ips()";
		
		$res = $this->db->select($query);
		
		if (DB_QUERY_ERROR == $res) {
			$this->err->err_from_string("Unable to fetch device restriction information from the database. Please contact Technical Support.");
			return FALSE;
		}

		
		
			
		foreach($res as $key => $row) {
			# Determine how many times that IP has been encountered
			$count = 0;
			foreach($services as $service) {
				$count += intval($row[$service]);
			}
			$res[$key]['count'] = $count;
			$this->debugger->add_hit("IP " . $row['ip'] . " encountered $count time(s)");
			
			# If the old IP is not used anywhere else ($count == 1), return TRUE (it frees a slot)
			if ((strcmp($old_ip, $row['ip']) == 0) && ($count == 1)) {
				$this->debugger->add_hit("Updating. Old IP not used anywhere else, so freeing a slot.");
				return TRUE;
			}
			
			# If the new IP is already in the list, return TRUE (doesn't use a new slot)
			if (strcmp($new_ip, $row['ip']) == 0) {
				$this->debugger->add_hit("New IP already slotted.");
				return TRUE;
			}
		}
		
		/**
		  * If we have reached this point, we will need a new slot.
		  * We fetch the device limit from the DB and compare it with the number of
		  * assigned IPs.
		  */
		  $query = "SELECT devices FROM netmon";
		  $limit_res = $this->db->get_row($query);
		  
		  if ((DB_QUERY_ERROR == $limit_res) || (DB_NO_RESULT == $limit_res)) {
			  $this->err->err_from_string("Unable to fetch device restriction information from the database. Please contact Technical Support.");
			  return FALSE;
		  }
		  $this->debugger->add_hit("Inserting new IP. " . ((sizeof($res) < intval($limit_res['devices'])) ? " using a free slot" : " no free slots left"));
		  return (sizeof($res) < intval($limit_res['devices']));
	}
	
	/**
	 * Setup wizard Proxy
	 * 
	 * This proxy hook will check if we are allowed to be running the wizard,
	 * authenticate the user to the original user account, and render wizard
	 * forums through AJAX lookups
	 *
	 * @param pointer $index_content
	 */
	function setup_wizard_proxy(&$index_content) {
		$hook = '__ajax_' . $_GET['hook'];
		if (method_exists($this, $hook)) {
			return $this->$hook($index_content);
		} else {
			echo "This AJAX hook does not exist.";
		}
	}
	
	
	/**
	 * Private AJAX hook to process a new user account
	 *
	 * @param pointer $index_content
	 */
	function __ajax_admin_account_processor(&$index_content) {
		require_class('mod_user', 'account_manager');

		// Check if the passwords match
		if ($_POST['passwd'] != $_POST['passwd2']) {
			$errorMsg = 'The two passwords do not match.  Please enter and confirm your requested password.';
			$response = array('success' => FALSE, 'errors' => NULL, 'errorInfo' => $errorMsg);
			$index_content .= json_encode($response);
			return;
		}
		
		$acm = new Account_Manager();
		
		# Force the password field for new inserts, but not for updates.
		$acm->params->primitives['passwd']['mandatory'] = TRUE;

		if (!$acm->insert()) {
			$err_msg = '';
			foreach($this->err->errors_collection as $err) {
				$err_msg .= $err['message'] . "<br />";
			}
			$response = array('success' => FALSE, 'errors' => NULL, 'errorInfo' => $err_msg);
		} else {
			$response = array('success' => TRUE);
		}
		
		$index_content .= json_encode($response);
	}
	
	/**
	 * Private AJAX call to add a new network range to localnets
	 *
	 * @param pointer $index_content
	 */
	function __ajax_network_ranges_processor(&$index_content) {
		
		require_class('mod_settings', 'localnets_manager');
		$lm = new LocalNets_Manager();
		
		foreach(array('enable_snmp_discovery', 'enable_portscan') as $val) {
			if (!$_POST[$val]) {
				$_POST[$val] = 'false';
			}
		}
		
		if ($lm->insert()) {
			$response = array('success' => TRUE);
		} else {
			$err_msg = '';
			foreach($this->err->errors_collection as $err) {
				$err_msg .= $err['message'] . "<br />";
			}
			$response = array('success' => FALSE, 'errors' => NULL, 'errorInfo' => $err_msg);
		}
		
		$index_content .= json_encode($response);
		
	}
	
	/**
	 * Private AJAX call to register initial SNMP auto-discovery settings
	 *
	 * @param pointer $index_content
	 */
	function __ajax_snmp_settings_processor(&$index_content) {
		
		# Load the XML Unserializer in memory
		require_once "XML/Unserializer.php";
		
		# Initialize the validator object.
		$validator = $this->registry->get_singleton("core", "validator");
		
		
		$query = "SELECT a.var, a.docstring_xml, b.component_type
		FROM daemonsconfig a, daemons b
		WHERE a.daemon_id = b.id
		AND b.name = 'snmpautod'";
		
		$res = $this->db->select($query);

		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			$err_msg .= "Unable to retrieve setting properties in the database. Please contact technical support.";
			$response = array('success' => FALSE, 'errors' => NULL, 'errorInfo' => $err_msg);
			$index_content .= json_encode($response);
			return;
		}

		

		# Initialize the XML Unserializer object
		$this->xml = new XML_Unserializer($this->options);
		

		# Unserialize the XML structure
		foreach($res as $row) {
			
			if (in_array($row['var'], array_keys($_POST))) {
			
				$xml_res = $this->xml->unserialize($row['docstring_xml'], FALSE);
				$type = $row['component_type'];
			
	
				# Error control
				if (PEAR::isError($xml_res)) {
					$err_msg = "Unable to parse service variables registry.";
					$response = array('success' => FALSE, 'errors' => NULL, 'errorInfo' => $err_msg);
					$index_content .= json_encode($response);
					return;
				}
	
				# Assign unserialized data to object
				$meta = $this->xml->getUnserializedData();
	
			


				# Update the value in the DB
				if ($validator->validate($meta['type'], $_POST[$row['var']])) {
					$query = "UPDATE daemonsconfig SET value = " . $this->db->escape($_POST[$row['var']]) . "
					WHERE daemon_id = (SELECT id FROM daemons WHERE name = 'snmpautod') 
					AND var = " . $this->db->escape($row['var']);
		
					$update_res = $this->db->update($query);
		
					# And restart the service if the update was successful.
					if (DB_QUERY_ERROR == $update_res){
						$err_msg = $query;
						$response = array('success' => FALSE, 'errors' => NULL, 'errorInfo' => $err_msg);
						$index_content .= json_encode($response);
						return;
					}
				} else {
					$err_msg = vdump($row);
					$response = array('success' => FALSE, 'errors' => NULL, 'errorInfo' => $err_msg);
					$index_content .= json_encode($response);
					return;
				}
			}
		}
		
		
		
		$response = array('success' => TRUE);
		$index_content .= json_encode($response);
		return;
		
	}
	
	/**
	 * Private AJAX hook that loads a configuration diagnostic on the device
	 *
	 * @param pointer $index_content
	 */
	function __ajax_detect_port_mirroring(&$index_content) {
		$parser = new Parser($this->tpl_dir);
		
		# Detect whether port mirroring is enabled or not
		$out = array();
		$return = NULL;
		exec("sudo python /apache/cli/interfaces.py -m -s 50", $out, $return);

		if (intval($return) == 0) {
			$parser->assign("mirroring", TRUE);
		} else {
			$parser->assign("mirroring", FALSE);
		}
		$parser->assign("mirroring_msg", join("\n", $out));
		
		$index_content .= $parser->fetch("ajax_net_diagnostic.tpl");
		return;
	}
	
	/**
	 * Private AJAX hook to determine if we can rsync to dev.netmon.ca
	 *
	 * @param pointer $index_content
	 */
	function __ajax_detect_rsync(&$index_content) {
		$mod = require_module("mod_settings");
		$status = $mod->_get_rsync_port_status();
		
		$tpl = new Parser($this->tpl_dir);
		$tpl->assign("status", $status);
		$index_content .= $tpl->fetch('ajax_rsync_diagnostic.tpl');
	}
	
	/**
	 * Private AJAX hook to start Netmon services and display deployment instructions.
	 *
	 * @param pointer $index_content
	 */
	function __ajax_start_services(&$index_content) {
		$cmd = "sudo /etc/init.d/netmon start";
		#exec($cmd, $out, $return);
		
		$tpl = new Parser($this->tpl_dir);
		$index_content .= $tpl->fetch("ajax_start_services.tpl");
		
	}
	
	


}

?>
