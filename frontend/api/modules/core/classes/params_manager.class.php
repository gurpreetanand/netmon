<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
 * class Params_Manager
 *
 * Manages Parameters for MadnetElement objects.  This object provides a
 * centralized facility for the management and streamlining of user input based on
 * GET/POST/COOKIE/SESSION.  It performs validation (based on validation regexs,
 * custom validator handlers, or high-level built-in validators), error handling and
 * reporting, etc...
 *
 * Additionally, it interacts with a stack of SubElement objects in order to process
 * more complex relationships than simply stacks of data and table rows.
 *
 * @package MADNET
 * @author Xavier Spriet
 */
class Params_Manager
{

  /** Aggregations: */

  /** Compositions: */

  /*** Attributes: ***/

  /**
   * Collection of Parameter primitives.
   *
   * Primitives are organized in a simple data-structure
   * based on an array of Hash Tables that contain validation data, raw data, friendly
   * input name, documentation, etc...
   * @var $primitives
   * @access private
   */
  var $primitives = array();


  /**
    * Collection of MadnetSubElement objects
    *
    * There is one array for each insert/update phase.
    * There cannot be one for the main insert phase as this phase is
    * reserved for primitives but there is a PRE and POST element.
    *
    * @var array $subs
    * @access private
    */
  var $subs = array();


  /**
    * Structure used to store key/value pairs for all primitive attributes
    *
    * @var array $autoinsert_array
    * @access private
    */
  var $autoinsert_array = array();


  /**
    * Reference to an array instanciated by the caller.
    *
    * This is the META structure exported by MadnetElement objects
    *
    * @var $element
    * @access protected
    */
  var $element;


  /**
    * Params_Manager constructor method
    *
    * Initializes the processing stacks (PRE and POST processors), as well as
    * aggregated objects and singletons.
    *
    * @return Params_Manager
    */
  function Params_Manager() {

    // Elements that must be processed BEFORE read/write operations belong in that array
    $this->subs['pre'] = array();
    // Elements that must be processed AFTER read/write operations belong in that array
    $this->subs['post'] = array();
    // Elements that HAVE BEEN processed belong in that array
    $this->subs['processed'] = array();

    # Instanciate the registry (Abstract Factory & Singleton)
    $this->registry = Registry::get_registry();

    # Error handler (MUST be a singleton)
    $this->err    = $this->registry->get_singleton("core", "error_manager");

    # DB handler (MUST be a singleton)
    $this->db    = $this->registry->get_singleton("core", "db_manager");

    # Validator
    $this->validator = $this->registry->get_singleton("core", "validator");

    # Validator
    $this->debugger = $this->registry->get_singleton("core", "debugger");

    # Insert/Update phases
    #$this->subs['pre']       = array();

    #$this->subs['post']      = array();



  }

  /**
    * Creates a new Primitive meta-data structure and stores it in the object's primitives stack
    *
    * @param string $name RAW Name of the primitive value (as found in POST or the DB table)
    * @param string $type Data-type expected for this value (used for validation and storage)
    * @param boolean $mandatory Whether or not this field is mandatory (used for validation)
    * @param string $friendly_name User-friendly name for this field (used for error reporting)
    * @param string $doc Longer documentation explaining what this field does. (used for tooltips)
    * @return void
    * @access protected
    */
  function add_primitive($name, $type = NULL, $mandatory = FALSE, $friendly_name = NULL, $doc = NULL) {
    $this->primitives[$name] =
          array("type"           => $type,
              "mandatory"      => (bool) $mandatory,
              "friendly_name"  => htmlentities($friendly_name),
              "doc"            => addslashes($doc),
              "value"          => NULL);
  }

  /**
   * Adds a new sub-element object to the stack
   *
   * Sub-elements can be registered here and they will be processed by the params_manager
   * which will first attempt to validate the object using the object's validate() method,
   * then, it will call the object's insert() method.
   *
   * *when* the method is called depends on the $PHASE parameter. It can either be processed
   * PRE-insert/update/delete (phase=pre) or POST-insert/update/delete (phase=post)
   *
   *
   * @param string $name Name of the object (will be used as sub_$name)
   * @param boolean $mandatory Determines if a value MUST be present in this field
   * @param string $phase When should this element be processed ("pre" or "post" insert/update)
   * @param array $params Additional payload/parameters to be passed to the object's constructor
   */
  function add_sub($module, $name, $mandatory, $phase = "post", $params = NULL, $key = NULL) {

    $name = "sub_" . $name;
    $key = $key ? $key : $name;
    $this->debugger->add_hit("Instanciating new SubElement -> $module/$name ($key)");


    require_class($module, $name);
    $this->subs[$phase][$key] =& new $name($params);
    $this->subs[$phase][$key]->element =& $this->element;
  }

  /**
    * Attempts to find the value associated with a key
    *
    * This method will first itterate through all the primitive keys
    * in an attempt to locate the value associated with the specified key.
    *
    * If it cannot locate the proper key, it will query all the MadnetSubElements
    * in the stack for that key. If it can find that element, it will return the
    * return value of the getVal() method on that SubElement.
    * If what was located was a primitive, it will return the "value" field of
    * the primitive structure.
    *
    * If the method is unable to locate a key, it will add a hit to the error manager
    * and return FALSE.
    *
    * @param string $key
    * @return mixed
    * @access public
    */
  function getval($key) {
    $param = $this->get_param($key);

    if (is_array($param)) {
      return $param['value'];

    } else {
      if (is_object($param)) {
        $val = $param->getVal($this->primitives[$this->element['pkey']]['value']);
        return $val;
      } else {
        $this->err->err_from_string("Unable to locate parameter $key");
        return FALSE;
      }
    }
  }

  /**
    * Associates a value to a primitive's meta-data structure based on that primitive's key
    *
    * WARNING: This will ONLY work with primitives. SubElements don't work that way
    *
    * @param string $key
    * @param string $val
    * @return void
    */
  function setval($key, $val) {
    #$val = $val ? $this->db->escape($val) : $val;
    $this->primitives[$key]['value'] = $val;
  }

  /**
    * Return a data-structure matching the specified key.
    *
    * Returns the complete meta-data structure associated with an element based
    * On the element's key.
    * Note that this method is not limited to searching for primitive structures
    * but also MadnetSubElement objects based on their name.
    *
    * @param string $param
    * @return boolean
    */
  function get_param($param) {

    # Attempt to locate the param in our primitives stack
    foreach($this->primitives as $key => $val) {
      if (strcmp($param, $key) == 0) {
        return $this->primitives[$key];
      }
    }

    # If this failes, attempt to find it in one of the subs
    foreach($this->subs as $phase => $vals) {
      foreach($this->subs[$phase] as $key => $val) {
        if (strcmp($param, $key) == 0) {
          return $this->subs[$phase][$key];
        }
      }
    }

    # if we are still unable to locate the item, return FALSE
    return FALSE;
  }


  /**
    * Returns a hash-table of all the MadnetSubElements
    *
    * This method looks both in PRE and POST insert/update processing
    * phases stacks and return a merged array of these stacks.
    *
    * @return array
    */
  function get_all_subs() {
    return array_merge_recursive($this->subs['pre'], $this->subs['post']);
  }

  /**
    *
    * Validated all values in our primitives stack
    *
    * Iterates through all the primitives and insures that all mandatory
    * primitives have a value associated with them in POST.
    * If this test is passed, it will rely on the aggregated MADNET::Validator
    * object to determine if the value associated with that primitive is valid.
    *
    * The informal data-type specified while adding the primitive to the params_manager
    * will determine what validation method the Validator object should use.
    *
    * It will also run that validity test against optional fields.
    *
    * @return boolean
    */

  function validate_primitives() {
    $result = TRUE;
    $this->debugger->add_hit("Validating primitives in " . __CLASS__);

    if (!$GLOBALS['json_post']) {
      $GLOBALS['json_post'] = json_decode(file_get_contents("php://input"), true);
    }

    foreach($this->primitives as $key => $current) {
      # Assign a value to the primitive from POST
      $this->setval($key, $GLOBALS['json_post'][$key]);
      
      # Create a shortcut reference.
      $entry = &$this->primitives[$key];
      
      $this->debugger->add_hit("Validating primitive $key");
      if ($entry['mandatory'] == FALSE) {
        # If it is not mandatory and there is no value, we're cool
        # We'll still put it through the validator to change the pg_bool values
        # if necessary
        if (!$entry['value']) { 
          @$this->validator->validate($entry['type'], $entry['value']);
          continue;
        }


        # If it is not mandatory and there's an invalid value, we're not cool
        elseif (!$this->validator->validate($entry['type'], $entry['value'])) {
          $result = FALSE;
          $this->err->err_from_code(400, "Invalid value specified for the field titled &quot;" . $entry['friendly_name'] . "&quot;");
        }

        # If it's not mandatory and there's a valid value, that's cool

      } else {
        # If it's mandatory and there's no value, we're not cool
        if ((!isset($entry['value'])) || (strcmp("", $entry['value']) == 0)) {
          $result = FALSE;
          $this->err->err_from_code(400, "No value specified for the mandatory field &quot;" . $entry['friendly_name'] . "&quot;");


          # If it's mandatory and there's an invalid value, we're not cool
        } elseif (!$this->validator->validate($current['type'], $entry['value'])) {
          $result = FALSE;
          $this->err->err_from_code(400, "Invalid value specified for the field titled &quot;" . $entry['friendly_name'] . "&quot;");
        }
        # If it's mandatory and we have a valid value, we're cool
      }
    }
    return $result;
  }


  /**
   * Processes all the phases of the insert process
   *
   * @return boolean
   */

  function insert() {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }
    
    $return = TRUE;

    $this->build_autoinsert_array();

    # Pre-insert phase
    foreach($this->subs['pre'] as $key => $obj) {
      if (($obj->validate()) && ($obj->insert())) {
        // Push a pointer to the object in the processed array
        array_push($this->subs['processed'], &$obj);
      } else {
        $this->debugger->add_hit("Invalid object in PRE", NULL, NULL, vdump($this->subs['pre']));
        if ($obj->mandatory == TRUE) { $return = FALSE; }
      }
    }

    if ((!$return) || ($return == FALSE)) { return FALSE; } // No need to rollback

    # Insert phase
    $result = $this->db->auto_insert($this->element['table'], $this->autoinsert_array);

    $this->element['pkey_value'] = $this->db->get_last_insert_id($this->element['table'], $this->element['pkey']);

    // Rollback if the query didn't work out.
    if ((DB_QUERY_ERROR == $result) || (DB_MISC_ERROR == $result)) {
      $this->undo();
      $this->debugger->add_hit("Unable to autoinsert... rolling back transaction");
      return FALSE;
    }


    # Post-insert phase
    $this->debugger->add_hit("Validating POST phase");
    foreach($this->subs['post'] as $key => $obj) {
      if (($obj->validate()) && ($obj->insert())) {
        // Push a pointer to the object in the processed array
        array_push($this->subs['processed'], &$obj);
      } else {
        if ($obj->mandatory == TRUE) { $return = FALSE; }
      }
    }

    if ((!$return) || ($return == FALSE)) { $this->undo(); return FALSE; } // No need to rollback

    $this->debugger->add_hit("Insert completed - returns " . vdump($return));

    return $return;

  }

  /**
    * Rolls back the last transaction at the application level
    *
    * The Insert method creates a reference of every object from PRE or POST
    * insert/update phases in the PROCESSED stack once they have returned TRUE
    * from their insert/update method.
    *
    * This function itterates through the object references in this stack
    * and call the delete() method from that object.
    * This insures that all dependencies associated with our main data-set
    * will be destroyed in the event that our transaction is canceled.
    *
    * It will then DELETE from the element's main table
    *
    * This is equivalent to an RDBMS' rollback() on transaction but is a lot
    * more portable and reliable than to rely on an unstable transaction
    * handler.
    *
    * @deprecated We are not using this at the moment (dangerous and experimental)
    * @todo Experiment with the (unsafe | experimental | not-a-good-idea) madnet-based transaction mechanism
    * @return void
    */
  function undo() {
    if (is_array($this->subs['processed'])) {
      foreach($this->subs['processed'] as $current) {
        $current->delete();
      }
    }

    $query = "DELETE FROM " . $this->element['table'] . " WHERE " . $this->element['pkey'] . " = " . $this->element['pkey_value'];
    $this->db->delete($query);
  }

  /**
    * Builds a key/value pair hash-table based on the content of the primitives stack
    *
    * This method itterates through all the object's primitives and creates
    * a hash-table containing key/value pairs for all the primitives.
    *
    * PEAR::DB (and thus, our DB_Manager object) will use that data-structure
    * to build an PREPARE/EXECUTE SQL statement based on that input and will
    * take care of all the escaping, error management and optimization for
    * the RDBMS driver we are using.
    *
    * @return void
    */
  function build_autoinsert_array($insert_empty_values = FALSE) {
    foreach(array_keys($this->primitives) as $key) {
      if ($insert_empty_values == TRUE) {
        $this->autoinsert_array[$key] = $this->primitives[$key]['value'];
      } else {
        if ((strcmp($this->primitives[$key]['value'], "") > 0) && (isset($this->primitives[$key]['value']))) {
          $this->autoinsert_array[$key] = $this->primitives[$key]['value'];
        }
      }
    }
    $this->debugger->add_hit("Autoinsert Array:", NULL, NULL, vdump($this->autoinsert_array));
    return $this->autoinsert_array;
  }

  /**
   * Processes all the phases of the update process
   *
   * @return boolean
   */
  function update() {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
      return FALSE;
    }
    $return = TRUE;

    $this->build_autoinsert_array();

    # Pre-insert phase
    foreach($this->subs['pre'] as $key => $obj) {
      if (($obj->validate()) && ($obj->update())) {
        // Push a pointer to the object in the processed array
        array_push($this->subs['processed'], &$obj);
      } else {
        $this->debugger->add_hit("Invalid object in PRE", NULL, NULL, vdump($this->subs['pre']));
        $return = FALSE;
      }
    }

    if (!$return) { $this->debugger->add_hit("Update dies on " . __CLASS__); return FALSE; } // No need to rollback

    # Insert phase
    $result = $this->db->auto_update($this->element['table'], $this->autoinsert_array, "{$this->element['pkey']} = {$this->element['pkey_value']}");

    #$this->element['pkey_value'] = $this->db->get_last_insert_id($this->element['table'], $this->element['pkey']);

    #$this->err->err_from_string

    // Rollback if the query didn't work out.
    if ((DB_QUERY_ERROR == $result) || (DB_MISC_ERROR == $result)) {
      #$this->undo();
      // Undo after updates are very tricky
      $this->debugger->add_hit("Unable to autoupdate... rolling back transaction");
      return FALSE;
    }


    # Post-insert phase
    $this->debugger->add_hit("Validating POST phase");
    foreach($this->subs['post'] as $key => $obj) {
      if (($obj->validate()) && ($obj->update())) {
        // Push a pointer to the object in the processed array
        array_push($this->subs['processed'], &$obj);
      } else {
        $return = FALSE;
      }
    }

    $this->debugger->add_hit(__FUNCTION__ . " returns " . vdump($return));

    if (!$return) { /* $this->undo(); */ return FALSE; } // No need to rollback

    return TRUE;
  }

  /**
   * Processes all the phases of the delete process
   *
   * @return boolean
   */
  function delete($id) {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }

    $return = TRUE;

    # Pre-delete phase
    foreach($this->subs['pre'] as $key => $obj) {
      if ($obj->delete()) {
        // Push a pointer to the object in the processed array
        array_push($this->subs['processed'], &$obj);
      } else {
        $this->debugger->add_hit("Invalid object in PRE", NULL, NULL, vdump($this->subs['pre']));
        $return = FALSE;
      }
    }

    # Due to referencial integrity constraints, even POST objects will be deleted
    # during the PRE phase.

    foreach($this->subs['post'] as $key => $obj) {
      if ($obj->delete()) {
        // Push a pointer to the object in the processed array
        array_push($this->subs['processed'], &$obj);
      } else {
        $this->debugger->add_hit("Invalid object in POST", NULL, NULL, vdump($this->subs['pre']));
        $return = FALSE;
      }
    }

    $query = "DELETE FROM {$this->element['table']} WHERE {$this->element['pkey']} = " . $this->db->escape($id);

    $result = $this->db->delete($query);


    if (DB_QUERY_ERROR == $result) {
      $this->err->err_from_string("Unable to delete main record ");
      $return = FALSE;
    }

    return $return;



  }

  /**
    * Experimental Fetching Mechanism
    *
    * @return bool
    *
    */
  function fetch($id) {
    $id = $this->db->escape($id);

    $query = "SELECT * FROM {$this->element['table']} WHERE {$this->element['pkey']} = $id";

    $result = $this->db->get_row($query);
    $this->debugger->add_hit("Fetch results", NULL, NULL, vdump($result));

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      foreach($result as $key => $value) {
        $this->setval($key, $value);
      }
      return $result;
    }
  }






} // end of Params_Manager



?>
