<?php


/**
 * Dead-simple data-structure cache.
 */
class Cache_Manager {
	
	/**
	 * Singleton Registry
	 * 
	 * @var Registry registry
	 */
	 var $registry;
	
	/**
	 * Error handler
	 * 
	 * @var Error_Manager err
	 */
	var $err;
	
	/**
	 * Debugger instance
	 * 
	 * @var Debugger debugger
	 * @access private
	 */
	var $debugger;
	
	/**
	 * Cache stack
	 * 
	 * @var array stack
	 */
	 var $stack = array();

    function Cache_Manager() {
    	
    	# Singleton Registry
    	$this->registry = &Registry::get_registry();
    	
    	# Error handler (MUST be a singleton)
		$this->err      = &$this->registry->get_singleton("core", "error_manager");

		# Debugger
		$this->debugger = &$this->registry->get_singleton("core", "debugger");
    }
    
    /**
     * Creates a new namespace $objName in the stack, and dump $objValue into it
     */
    function set($objName, &$objValue) {
    	//echo '<hr />' . $objName . " => " . vdump($objValue) . '<hr /><br />';
    	$this->stack[$objName] = &$objValue;
    	return TRUE;
    }
    
    /**
     * Modifies the namespace $objName and change its value to $objValue
     */
    function &get($objName) {
    	if (in_array($objName, array_keys($this->stack))) {
    		return $this->stack[$objName];
    	}
    	return FALSE;
    }
    
    
}
?>