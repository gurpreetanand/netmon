<?php

class System_Manager
{
  public function getMeminfo() {
      $data = explode("\n", file_get_contents("/proc/meminfo"));
      $meminfo = array();
      foreach ($data as $line) {
        list($key, $val) = explode(":", $line);
        $meminfo[$key] = trim($val);
      }
      return $meminfo;
  }

  public function areDaemonsUp() {
    # Establish a local socket connection to procmond
    if (phpversion() >= 5) {
      $sock_url = "unix:///var/run/.netmon.s";
    } else {
      $sock_url = "/var/run/.netmon.s";
    }

    $sock = fsockopen($sock_url, -1, $errno, $errstr, 10);

    if (FALSE === $sock) {
      return false;
    }

    @fclose($sock);
    return true;
  }
}

?>