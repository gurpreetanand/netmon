<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
 * Request Handler and Query dispatcher
 *
 * Provides basic request-handling facility  The Dispatcher is responsible for
 * marshalling request parameters, performing session authentication (if necessary),
 * locating the appropriate module control object (MCO), dispatching the request to
 * the MCO, and finally returning the output of the MCO to the caller.
 *
 * @package MADNET
 * @author Xavier Spriet
 *
 *
 */
class Dispatcher
{


	/**
	  * Singleton objects registry
	  *
	  * @var Registry $registry
	  * @access private
	  */
	var $registry;

	/**
	  * Error Manager Singleton object
	  *
	  * @var Error_Manager $error_manager
	  * @access private
	  */
	var $error_manager;

	/**
	  * Dispatcher object constructor
	  *
	  * Instanciates the registry and error manager objects,
	  * then add a few dispatching-related error codes to the
	  * error map.
	  *
	  * @return Dispatcher
	  */
	function Dispatcher() {
		$this->registry = Registry::get_registry();
		$this->error_manager = $this->registry->get_singleton("core", "error_manager");

		$this->error_manager->add_error_to_map(404, "Not Found");
		$this->error_manager->add_error_to_map(403, "Forbidden");

	}

	/**
	 *
	 * Main request dispatch facility
	 *
	 * This method implements a lot of business logic.
	 * It will attempt to find out if the user has access to
	 * the requested module and method. Certain modules are
	 * registered as public and should be accessible to anyone,
	 * while other modules will require a certain set of permissions.
	 * The dispatcher knows to fall back on default modules if it cannot
	 * establish a relationship between the user's security clearance and
	 * the module he is trying to access.
	 *
	 * Once the dispatcher has determined which module/action pair to
	 * use (default modules or requested modules), it will instanciate
	 * a new module and dispatch the request to it.
	 *
	 * @param pointer $index_content
	 * @return bool
	 * @access public
	 */
	function dispatch(&$index_content) {

		if (($_GET['debug'] == '1') || (__DEBUG_DEFAULT__ == TRUE)) {
			$_SESSION['debug'] = '1';
		}

		define("__DEBUG__", ($_SESSION['debug'] == '1'));

		$m = $_GET['module']; $a = $_GET['action'];
		$this->store_request();

		if ($this->perform_auth($index_content)) {
			$dm = DEFAULT_LOGGED_IN_MODULE;
			$da = DEFAULT_LOGGED_IN_ACTION;
		} else {
			$dm = DEFAULT_MODULE;
			$da = DEFAULT_ACTION;
			if ($this->is_private($m, $a)) {
				$m = mktitle(str_replace("mod_", "", $m));
				$this->error_manager->err_from_code(403, "Authentication required. <strong>Your session may have expired.</strong>");
				$m = $dm;
				$a = $da;
			}
		}

		if (($m == "") && ($a == "")) {
			$m = $dm;
			$a = $da;
		}




		if (module_exists($m)) {
			$mod = &require_module($m);
		} else {
			$this->error_manager->err_from_code(404, "The specified module <font color=\"#ff0000\">&quot;" . mktitle($m) . "&quot;</font> does not exist");
			#$mod = require_module($dm);
			#$a   = $da;

			$mod = require_module("core"); $a = $da = "blank";
		}

		# Private method handling
		if (strcmp('_', substr($a, 0, 1)) == 0) {
			$this->error_manager->err_from_code(403, "The method you have requested is a private method and cannot be called from within this context.");
			$mod = require_module("core"); $a = $da = "blank";
		}



		if (method_exists($mod, $a)) {
			return $mod->$a($index_content);
		} else {
			$this->error_manager->err_from_code(404, "The specified method <font color=\"#ff0000\">&quot;".mktitle($a)."&quot;</font> does not exist");
			$dm = "core"; $a = $da = "blank";
			$mod = require_module($dm);
			return $mod->$da($index_content);
		}
	}




	/**
	 * Returns true of the user is authenticated, false otherwise
	 *
	 * This method will also refresh the session data-structure if it
	 * detects a versionning conflict.
	 *
	 * @param pointer $index_content
	 * @return bool
	 * @access private
	 */
	function perform_auth(&$index_content)
	{


		$_SESSION['sessid'] = session_id();

		# Session auto-refresh upon structure change
		if ((intval($_SESSION['id']) > 0) && (intval($_SESSION['VERSION']) < SESSION_VERSION)) {
			$mod_user = require_module("mod_user");
			$mod_user->refresh_session($index_content);
		}

		// Store the previous request in the session
		if ($_SESSION['AUTHID'] != sha1(APP_AUTHID)) {
			return false;
		} else {
			return TRUE;
		}
	} // end of member function perform_auth

	/**
	  * Determines whether or not to store the last request in session
	  *
	  * It is sometimes useful to know what the last request before the
	  * current one was. This function determines whether or not it should
	  * store this last request. For example, if the last request is blank,
	  * it knows to store the default modules, or if the ignore_request bit
	  * has been passed, it will ignore it alltogether.
	  *
	  * @todo Overhaul the request storage system
	  * @return void
	  */
	function store_request() {
		if ($_GET['store_request']) {
			$request = array();


			foreach(array_keys($_GET) as $key) {
				$request[$key] = $_GET[$key];
			}

			if (empty($request)) {
				$stupid_hack = FALSE;
				if ($this->perform_auth($stupid_hack)) {
					$request = array("module" => DEFAULT_LOGGED_IN_MODULE, "action" => DEFAULT_LOGGED_IN_ACTION);
				} else {
					$request = array("module" => DEFAULT_MODULE, "action" => DEFAULT_ACTION);
				}
			}

			if (sizeof(array_diff($request, (array) $_SESSION['request'])) > 0) {
				$_SESSION['last_request'] = $_SESSION['request'];
				$_SESSION['request'] = $request;
			} else {
				return;
			}
		}
	}


	/**
	  * Determines whether a module/action pair is private (requires authentication) or public
	  *
	  * If you are building a website, the default policy should be public, so simply
	  * return TRUE here, and perform your has_perm(*) checks in your module methods.
	  *
	  * If you are building a private app, store here those few hooks that are legit for
	  * anyone (like the login dialog, debugger, authentication processor, etc...)
	  *
	  * @todo  Formalize the public/private module handling
	  * @param string $module
	  * @param string $action
	  * @return boolean
	  */
	function is_private($module, $action) {
                // Everything but auth is private.
                return (!($module == "mod_user" && $action == "auth"));
	}


} // end of Dispatcher


?>
