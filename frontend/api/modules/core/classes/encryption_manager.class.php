<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */



/**
  * Encryption manager class
  *
  * This class performs all encryption/decryption related operations
  *
  * @package MADNET
  * @author  Xavier Spriet
  */
class Encryption_Manager {

	/**
	  * Private Encryption key
	  * @var string $key
	  * @access private
	  */
	var $key;

	/**
	  * Encryption cipher
	  * @var string $cipher
	  * @access private
	  */
	var $cipher;

	/**
	  * Encryption Codebook
	  * @var string $cb
	  * @access private
	  */
	var $cb;

	/**
	  * Singleton objects registry
	  * @var Registry $registry
	  * @access private
	  */
	var $registry;

	/**
	  * Debugger singleton object
	  * @var Debugger $debugger
	  * @access private
	  */
	var $debugger;

	/**
	  * Encryption Manager constructor
	  *
	  * Initializes the encryption parameters to the proper values
	  * and also instanciate the registry and debugger singleton.
	  *
	  * @return Encryption_Manager
	  */
	function Encryption_Manager() {
		$this->key    = CRYPT_KEY;
		$this->cipher = CRYPT_CIPHER;
		$this->cb     = CRYPT_CODEBOOK;

		$this->registry = Registry::get_registry();
		$this->debugger  = $this->registry->get_singleton("core", "debugger");

	}

	/**
	  * MCrypt library init method
	  *
	  * This method should be called before any encryption/decryption operation
	  * It is an expensive operation. Never forget to call mcrypt_deinit()
	  * once you are done with the encryption modules
	  *
	  * @return void
	  */
	function init_mcrypt() {
		$this->mod    = mcrypt_module_open ($this->cipher,'',$this->cb,'');

		/* Create an Initialization Vector
		based on a size and a source.
		The source can be custom, but some constants
		are available.
		Defining the size of the vector depends on the
		module being used */
		$iv_size = mcrypt_enc_get_iv_size($this->mod);

		/* The initialization vector will be based on $size
		characters from the source /dev/random */
		$this->iv = mcrypt_create_iv($iv_size,MCRYPT_DEV_RANDOM);

		/* The next step is to ensure that the key is not
		too big and truncate it if necessary */
		$max_key_size = mcrypt_enc_get_key_size($this->mod);

		$key = substr($this->key,0,$max_key_size);
		/* You must then initialize the encryption
		mechanism through mcrypt_generic_init */
		mcrypt_generic_init ($this->mod, $key, $this->iv);



	}

	/**
	  * Frees all the encryption modules and buffers and perform any GC required
	  *
	  * @return void
	  */
	function deinit_mcrypt() {
		mcrypt_generic_deinit ($this->mod);
		mcrypt_module_close ($this->mod);
	}

	/**
	  * Encrypts the specified string
	  *
	  * @param string $str
	  * return string
	  */
	function encrypt($str) {

		if (__STACK_TRACE__ == TRUE) {
			$caller = xdebug_call_class() . "/" . xdebug_call_function();
		} else {
			$caller = "Unknown caller";
		}

		$this->debugger->add_hit("Encrypting $str from $caller");

		$this->init_mcrypt();
		/* You can now encrypt your data through
		the use of mcrypt_generic. The function
		will return your encrypted data */
		$e =  mcrypt_generic($this->mod,$str);
		$this->deinit_mcrypt();
		return base64_encode($e);
	}

	/**
	  * Decrypt the specified encrypted string
	  *
	  * @param string $str
	  * @return string
	  */
	function decrypt($str) {
		$str = base64_decode($str);
		$this->init_mcrypt();
		#$padded = mcrypt_decrypt($this->cipher, $key, $str, $this->codebook, $this->iv);
		$padded = mdecrypt_generic($this->mod, $str);
		/* At this point, our decrypted string has been
		   zero-padded so we need to remove the extra \0s */
		$e = str_replace("\0","",$padded);
		$this->deinit_mcrypt();
		return $e;
	}
}