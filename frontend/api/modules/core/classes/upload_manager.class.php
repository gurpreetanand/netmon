<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */




/**
  * @package    MADNET
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * Upload Manager Object
  */
class Upload_Manager {


	/*** Aggregates: ***/

	var $err;

	var $registry;
	
	var $debugger;

	/**
	 * UploadManager class constructor
	 *
	 * Instanciates the registry as well as an error handler singleton
	 *
	 * @return bool
	 * @access public
	 */
	function Upload_Manager() {

		# Instanciate the registry (Abstract Factory & Singleton)
		$this->registry = Registry::get_registry();

		# Error handler (MUST be a singleton)
		$this->err    = $this->registry->get_singleton("core", "error_manager");
		
		$this->debugger = $this->registry->get_singleton("core", "debugger");
	}

	function retrieve_file($field_name) {
		
		if (has_perm("Guest/Demo Account")) {
			$this->debugger->add_hit("Demo mode enabled");
			$this->err->err_from_string("This operation is not available in Demo mode", FALSE);
			return FALSE;
		}
		
		
		$return = array("mime" => "",
						"ext"  => "",
						"tmp"  => "",
						"name" => "");
		if ((!is_uploaded_file($_FILES[$field_name]['tmp_name'])) || ($_FILES[$field_name]['size'] == 0)) {
			return FALSE;
		} else {
			$chunks = explode(".", $_FILES[$field_name]['name']);

			$return['ext']  = $chunks[sizeof($chunks)-1];
			$return['tmp']  = $_FILES[$field_name]['tmp_name'];
			$return['mime'] = $_FILES[$field_name]['type'];
			$return['name'] = $_FILES[$field_name]['name'];

			return $return;
		}
	}

	function retrieve_file_with_content($field_name) {
		
		if (has_perm("Guest/Demo Account")) {
			$this->debugger->add_hit("Demo mode enabled");
			$this->err->err_from_string("This operation is not available in Demo mode", FALSE);
			return FALSE;
		}
		
		
		if (!$file = $this->retrieve_file($field_name)) {
			return FALSE;
		}

		$file['data'] =  file_get_contents($file['tmp']);
		unlink($file['tmp']);
		unset($file['tmp']);
		return $file;
	}

	# Note: The image will *always* be returned as a JPEG
	function retrieve_image($field_name, $format) {
		
		if (has_perm("Guest/Demo Account")) {
			$this->debugger->add_hit("Demo mode enabled");
			$this->err->err_from_string("This operation is not available in Demo mode", FALSE);
			return FALSE;
		}
		
		
		static $idx = 0;
		$idx++;

		if (!$file = $this->retrieve_file($field_name)) {
			return FALSE;
		}

		$path = pathinfo($file['tmp']);


		$tmp_name = $path['dirname'] . "/madnet_" . $path['basename'] . $idx . "__" . ".jpg";

		$cmd = "env convert -thumbnail $format " . $file['tmp'] . " -interlace Line -enhance " .
		"-comment \"MADNET Upload Manager\"  $tmp_name";

		$out = $ret = "";
		$cmd = escapeshellcmd($cmd);
		exec($cmd, $out, $ret);

		# Store the original path so the caller can clean up the temp dir.
		$file['orig'] = $file['tmp'];

		$file['tmp'] = $tmp_name;
		return $file;
	}


	# Note: The image will *always* be returned as a JPEG
	function retrieve_image_with_content($field_name, $format) {
		
		if (has_perm("Guest/Demo Account")) {
			$this->debugger->add_hit("Demo mode enabled");
			$this->err->err_from_string("This operation is not available in Demo mode", FALSE);
			return FALSE;
		}
		
		if (!$file = $this->retrieve_image($field_name, $format)) {
			return FALSE;
		}
		$file['data'] =  file_get_contents($file['tmp']);
		unlink($file['tmp']);
		unset($file['tmp']);
		return $file;
	}


}

?>
