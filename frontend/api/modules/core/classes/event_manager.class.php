<?php

class Event_Manager extends MadnetElement
{
	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "events";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "id";
	/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "core";
	/**
	  * Name of the class containing the domain logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	function init() {
		$this->params->add_primitive("occurrences", "integer",  FALSE,  "Number of Occurrences");
		$this->params->add_primitive("schedule_start_date", "string", FALSE, "Start Date");
		$this->params->add_primitive("schedule_stop_date", "string", FALSE, "End Date");
		$this->params->add_primitive("schedule_hour", "integer", TRUE, "Scheduled Hour");
		$this->params->add_primitive("schedule_month", "integer", FALSE, "Scheduled Month");
		$this->params->add_primitive("schedule_day", "integer", FALSE, "Scheduled Day");
		$this->params->add_primitive("notify_users", "string", FALSE, "List of users to notify upon completion");
		$this->params->add_primitive("recurrence_unit", "string", TRUE, "Frequency Unit");
		$this->params->add_primitive("schedule_week", "string", FALSE, "Day of Week");
		$this->params->add_primitive("event_type", "string", TRUE, "Event Type");
	}
	
	function pre_insert($id = null) {
		$_POST['notify_users'] = '{' . $_POST['notify_users'] . '}';
		$this->params->setval("notify_users", $_POST['notify_users']);
		return TRUE;
	}
	
	function pre_update($id) {
		return $this->pre_insert($id);
	}


}

?>