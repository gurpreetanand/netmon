<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * Module handler (MCO - Module Control Object) Superclass
  *
  * This class initializes all module variables and aggregated
  * objects properly so the subclass module can do its job without
  * having to worry about any of its internal parameters
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class MadnetModule {

	/**
	 * Path to the module's template directory
	 *
	 * @var string
	 */
	var $tpl_dir;

	/**
	 * Path to the module's configuration repository
	 *
	 * @var string
	 */
	var $conf_dir;

	/**
	 * Path to the module's libraries repository
	 *
	 * @var string
	 */
	var $libs_dir;

	/**
	 * Path to the module's service layer and model object classes
	 *
	 * @var string
	 */
	var $classes_dir;

	/**
	 * Registry
	 *
	 * @var Registry
	 */
	var $registry;

	/**
	 * Error Manager
	 *
	 * @var Error_Manager
	 */
	var $err;

	/**
	 * DB Manager
	 *
	 * @var DB_Manager
	 */
	var $db;

	/**
	 * Debugger
	 *
	 * @var Debugger
	 */
	var $debugger;

	/**
	 * Current Module
	 *
	 * @var string
	 */
	var $module;

	/**
	  * MadnetModule object constructor method
	  *
	  * This method calls the init() method on the subclass
	  * and initializes the directory variables with the proper
	  * values.
	  *
	  * It also initializes any aggregated object such as the registry,
	  * error manager, db manager and debugger.
	  *
	  * @return MadnetModule
	  *
	  */
	function MadnetModule() {
		$this->registry = Registry::get_registry();

		$this->err = $this->registry->get_singleton("core", "error_manager");

		$this->db  = $this->registry->get_singleton("core", "db_manager");

		$this->debugger  = $this->registry->get_singleton("core", "debugger");

		/**
		 * Typically, a module will be in a directory that matches its name, so
		 * we use the name of the module as the default to set the directory paths.
		 *
		 * If a MadnetModule child is located inside another module, it can simply
		 * call _set_module() from its init() method.
		 */
		$this->_set_module(get_class($this));

		$this->init();
	}

	/**
	 * Initializes module-specific variables
	 *
	 * @param string $name
	 */
	function _set_module($name) {
		$this->module = strtolower($name);
		$this->tpl_dir = get_tpl_dir($this->module);
		$this->conf_dir = get_conf_dir($this->module);
		$this->libs_dir = get_libs_dir($this->module);
		$this->classes_dir = get_classes_dir($this->module);
	}

	/**
	  * Does nothing
	  *
	  * This declaration is simply here to prevent the
	  * application to crash or interrupt the process if
	  * the subclass did not implement this interface properly.
	  *
	  * @return void
	  */
	function init() {
		;;
	}
}

?>