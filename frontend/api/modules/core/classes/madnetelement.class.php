<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  * Element superclass
  *
  * This class will be inherited by all the MadnetElement sub-classes
  * and performs all the processing logic that is common to all these
  * top-level entities.
  *
  * Sub-level entities are handled by sub-classes of the MadnetSubElement
  * class and operate in a simar way except that they ship their
  * own validation rules and processing logic since they are complex
  * objects often storing relationships between different elements,
  * but I digress
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class MadnetElement {

  /**
    * The META structure is maintained by the MadnetElement object
    * and is SHARED with its params_manager and all its sub_elements.
    *
    * The object should use the META structure to inform all "observer"
    * objects of certain properties such as table name, primary key name,
    * primary key value (during delete/update or after an insert based on
    * the db_manager object's last_insert_id return value), etc...
    *
    * @var hashtable $meta
    * @access private
    */
  var $meta;

  /**
   * DB Manager
   *
   * @var DB_Manager
   */
  var $db;

  /**
   * Debugger
   *
   * @var Debugger
   */
  var $debugger;

  /**
   * Params Manager
   *
   * @var Params_Manager
   */
  var $params;

  /**
   * Error Manager
   *
   * @var Error_Manager
   */
  var $err;


  /**
    * MadnetElement constructor method
    *
    * This method instanciates all the aggregated objects that
    * the sub-classes will need in order to implement their business
    * logic, it will then initialize the META structure with the
    * appropriate data, instanciate a unique params_manager for the
    * object, point the META structure to it, then call $this->init()
    * which will result in the init() method of the subclass to be
    * called
    *
    * @return MadnetElement
    */
  function MadnetElement() {

    # Instanciate the registry (Abstract Factory & Singleton)
    $this->registry = Registry::get_registry();

    # DB Manager (SHOULD be a singleton)
    $this->db     = $this->registry->get_singleton("core", "db_manager");
    # Error handler (MUST be a singleton)
    $this->err    = $this->registry->get_singleton("core", "error_manager");
    # Debugger
    $this->debugger    = $this->registry->get_singleton("core", "debugger");

    # This cannot be a singleton. All Element objects will need their own.
    $this->params = new Params_Manager();
    # Continue the init process


    $this->meta = array("table"      => $this->table,
              "pkey"       => $this->pkey,
              "module"     => $this->module,
              "name"       => $this->element,
              "pkey_value" => "");


    $this->params->element = &$this->meta;



    $this->init();

  }


  /**
    * Returns the pkey value of the last element inserted
    *
    * @todo Make last_insert_id() on MadnetElement use last_insert_id() in db_manager
    * @return mixed
    */
  function last_insert_id() {
    $query = "SELECT {$this->meta['pkey']} AS \"id\" FROM {$this->meta['table']} ORDER BY {$this->meta['pkey']} DESC LIMIT 1";
    $res = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $res) || (DB_MISC_ERROR == $res)) {
      $this->debugger->add_hit("No last insert id... ");
      return FALSE;
    } else {
      $this->debugger->add_hit("Last insert id: " . $res['id']);
      return $res['id'];
    }


  }

  /**
    * Returns the value associated with a key in the element's params_manager
    *
    * @param string $key
    * @return mixed
    */
  function get($key) {
    return $this->params->getVal($key);
  }

  /**
    * Maps the params_manager structure to a SQL query's resultset
    *
    * This function retrieves the values from an SQL query and will
    * use the params_manager as a mapper for the resultset.
    *
    * This is very useful for fetching records
    *
    * @todo use map() on individual MadnetElement subclasses
    * @return boolean
    * @param string $query
    *
    */
  function map($query) {

    $result = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    }

    foreach($result as $key => $value) {
      $this->params->setval($key, $value);
    }
    return TRUE;

  }

  /** Data Manipulation calls **/

  /**
    * Inserts data from POST automagically
    *
    * This function first validates all the primitives
    * registered in the params_manager, then calls the
    * subclass' pre_insert method as a hook for your pre-processing
    * business logic. It then automatically inserts the data based
    * on the params_manager's content (which is itself divided into
    * 3 different phases, so you can implement further business logic
    * directly in your MadnetSubElement subclasses)
    *
    * Once this is done, it calls the post_insert() method in the
    * subclass to give back control to your class.
    *
    * Each of your homebrew processing function must return TRUE,
    * otherwise, the process will be interrupted. This also means that
    * if you intend to return FALSE, you should let the error_manager
    * know why you are stopping the insert process.
    *
    * @return boolean
    *
    */
  function insert() {

    return (
      $this->params->validate_primitives()
      && ($this->pre_insert())
      && ($this->params->insert())
      && ($this->post_insert()));
  }

  /**
    * Updates the database based on new POST data
    *
    * Works in a similar way to that of $this->insert() but also
    * sets the proper values in the META structure, and insures that
    * a valid ID variable has been passed to GET
    *
    *
    * @return boolean
    * @todo clean-up update process and ID passing on madnetElement
    */
  function update($ref_id = NULL) {

    $this->debugger->add_hit("Initiating update process from " . $this->element);

    $id = ($ref_id != NULL) ? $ref_id :  intval($_GET[$this->pkey]);

    if ($id == 0) {
      $this->err->err_from_code(400, "An identifier has been lost between HTTP requests. This must be an application error. Please report a bug.");
      return FALSE;
    }

    $this->meta['pkey_value'] = $id;

    # I have just changed the order of the steps... needs testing.
    return (
      ($this->params->validate_primitives())
      && ($this->pre_update($id))
      && ($this->params->update($id))
      && ($this->post_update($id)));
  }


  /**
    * Deletes an item and all its relationships from the DB
    *
    * Works in a way similar to that of update or insert
    *
    * @return boolean
    */
  function delete($id) {
    $this->meta['pkey_value'] = $id;
    return (
      ($this->pre_delete($id))
      && ($this->params->delete($id))
      && ($this->post_delete($id)));
  }

  /**
    * Experimental - now returns the pop value also.
    *
    *
    * @return boolean
    */
  function fetch($id) {
    $this->meta['pkey_value'] = $id;
    $ret = FALSE;

    if (
    ($this->pre_fetch($id))
    && (false !== ($ret = $this->params->fetch($id)))
    && ($this->post_fetch($id))) {
      return $ret;
    } else {
      return FALSE;
    }
  }

  /** PROTOTYPES **/

  /**
    * Does nothing
    *
    * This declaration is simply here to prevent the
    * application to crash or interrupt the process if
    * the subclass did not implement this interface properly.
    *
    * @return boolean
    */
  function pre_insert()  { return TRUE; }
  /**
    * Does nothing
    *
    * This declaration is simply here to prevent the
    * application to crash or interrupt the process if
    * the subclass did not implement this interface properly.
    *
    * @return boolean
    */
  function post_insert() { return TRUE; }

  /**
    * Does nothing
    *
    * This declaration is simply here to prevent the
    * application to crash or interrupt the process if
    * the subclass did not implement this interface properly.
    *
    * @return boolean
    */
  function pre_update()  { return TRUE; }
  /**
    * Does nothing
    *
    * This declaration is simply here to prevent the
    * application to crash or interrupt the process if
    * the subclass did not implement this interface properly.
    *
    * @return boolean
    */
  function post_update() { return TRUE; }

  /**
    * Does nothing
    *
    * This declaration is simply here to prevent the
    * application to crash or interrupt the process if
    * the subclass did not implement this interface properly.
    *
    * @return boolean
    */
  function pre_fetch()   { return TRUE; }
  /**
    * Does nothing
    *
    * This declaration is simply here to prevent the
    * application to crash or interrupt the process if
    * the subclass did not implement this interface properly.
    *
    * @return boolean
    */
  function post_fetch()  { return TRUE; }

  /**
    * Does nothing
    *
    * This declaration is simply here to prevent the
    * application to crash or interrupt the process if
    * the subclass did not implement this interface properly.
    *
    * @return boolean
    */
  function pre_delete()  { return TRUE; }
  /**
    * Does nothing
    *
    * This declaration is simply here to prevent the
    * application to crash or interrupt the process if
    * the subclass did not implement this interface properly.
    *
    * @return boolean
    */
  function post_delete() { return TRUE; }

  /**
    * Does nothing
    *
    * This declaration is simply here to prevent the
    * application to crash or interrupt the process if
    * the subclass did not implement this interface properly.
    *
    * @return boolean
    */
  function init()        { }

  /**
    * Returns a value from the DB based on the value of the pkey
    *
    * This method uses the pkey_value variable to build an SQL SELECT
    * query. It will return the element associated with the $bitname
    * variable from the resultset.
    *
    * @param string $pkey_value
    * @param string $bitname
    * @return mixed
    */
  function getBit($pkey_value, $bitname) {
    $query = "SELECT $bitname FROM {$this->table} WHERE {$this->pkey} = $pkey_value";

    $result = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      $this->err->err_from_string("Unable to retrieve a value for the bit named $bitname where {$this->pkey} is $bitname");
    }

    return $result[$bitname];

  }

  /**
    * Sets a field in the DB to a specific value based on pkey value
    *
    * This field updates the DB table to change the value of one element
    * in a row. It uses the value of the pkey specified as argument to determine
    * which row to update, and the $bitname parameter to determine which field to update.
    *
    * @param string $pkey_value
    * @param string $bitname
    * @param string $bitvalue
    * @return mixed
    */
  function setBit($pkey_value, $bitname, $bitvalue) {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }
    
    
    $query = "UPDATE {$this->table} SET {$bitname} = {$bitvalue} WHERE {$this->pkey} = {$pkey_value}";

    $result = $this->db->update($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      $this->err->err_from_string("Unable to retrieve a value for the bit named $bitname where {$this->pkey} is $bitname");
    }

    return $result;

  }

}

?>