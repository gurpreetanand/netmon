<?php
/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */



/**
 * Controler Facade Class
 *
 * Simplifies View/Controler interaction  The Controler_Facade object provides a
 * simplified interface to the controler.  It is meant to provide a simple interface
 * to allow the view to interact with controler objects.
 *
 * @package MADNET
 * @author Xavier Spriet
 *
 */
class Controler_Facade
{


   /*** Attributes: ***/

  /**
   * Aggregated Session singleton.
   * @var object $session_manager
   * @access private
   */
  var $session_manager;
  /**
   * Aggregated Env_Manager singleton.
   * @var object $env_manager
   * @access private
   */
  var $env_manager;
  /**
   * Aggregated Dispatcher singleton.
   * @var object $dispatcher
   * @access private
   */
  var $dispatcher;

  /**
    * Aggregated Debugger singleton.
    * @var object $debugger
    * @access private
    */
  var $debugger;


  /**
    * Controler Facade constructor
    *
    * The controler facade object is the only onject that the view (index/renderer)
    * need to interact with to implement the business logic of the application.
    * In turn, the controler region interacts with different model facades
    * that all follow a strict interfaces. Those are the MadnetModules
    *
    * @return Controler_Facade
    */
  function Controler_Facade() {
    require_once(MODS_PATH . "core/classes/env_manager.class.php");
    $this->env_manager = new Env_Manager();

    $registry = Registry::get_registry();

    # Instantiate the debugger right away to start the timer.
    $this->debugger        = $registry->get_singleton("core", "debugger");

    # Start the session manager before we start the actual session
    $this->session_manager = $registry->get_singleton("core", "session_manager");

    # Instanciate the dispatcher
    $this->dispatcher      = $registry->get_singleton("core", "dispatcher");

    # Prepare a DB connection
    $this->db              = $registry->get_singleton("core", "db_manager");


  }

  /**
   * Interacts with the Dispatcher object to process the user request
   *
   * The dispatcher will take the $index_content pointer and send it to the
   * appropriate module. The module will return an action title to the dispatcher
   * which will in turn return this value to process_query, which sends it back
   * to the caller (typically, the index)
   *
   * @param  pointer $index_content
   * @return string
   * @access public
   */
  function process_query(&$index_content)
  {
    return $this->dispatcher->dispatch($index_content);

  } // end of member function process_query







} // end of Controler_Facade
?>
