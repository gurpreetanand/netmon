<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
 * Debugger object
 *
 * Tracks application status and performance.  The Debugger object is responsible
 * for tracking the status of the application at any point in time. This includes
 * interaction with the Error_Handler, response to debug hooks throughout the
 * application through the add_hit function, as well as creating a performance
 * profile for the application.
 *
 * @package MADNET
 * @author  Xavier Spriet
 */
class Debugger
{

	/** Aggregations: */

	/** Compositions: */

	/*** Attributes: ***/

	/**
	 * Collection of Hits
	 *
	 * A "hit" is the basic tracking unit for the debugging engine.
	 * It is a simple data structure implemented as a Hash Table that contains event
	 * properties such as execution time, event description, event type, severity, and
	 * additional information  (payload)
	 *
	 * @var array $hits_collection
	 * @access private
	 */
	var $hits_collection = array();

	/**
	  * Initial timestamp
	  *
	  * @var float $start_time
	  * @access private
	  */
	var $start_time;

	/**
	  * Final timestamp
	  *
	  * @var float $end_time
	  * @access private
	  */
	var $end_time;


	/**
	  * Debugger constructor
	  *
	  * Initiates the timer and adds the initial hit to the hits collection
	  *
	  * @return Debugger
	  */
	function Debugger() {
		# Start the clock
		$this->start_time = $this->microtime_float();
		$this->add_hit("Script execution started", "Start", NULL, NULL);
	}

	/**
	 * Adds a new hit in the collection.
	 *
	 * @param string $event_description Short description of the event
	 * @param string $event_type Type of event
	 * @param integer $severity Severity value from 1 to 10 (1-3=notice, 4-6=warnings,7-10=error)
	 * @param string $payload Payload to attach to the hit
	 * @return void
	 * @access public
	 */
	function add_hit($event_description, $event_type = "Message", $severity = SEVERITY_NOTIFY, $payload = NULL)
	{
		if ((defined("__DEBUG__")) && (__DEBUG__ == TRUE)) {
			$hit = array(
					'description' => str_replace("\"", "\\\"", str_replace("\n", " ",$event_description)),
					'type'        => $event_type,
					'severity'    => intval($severity),
					'payload'     => str_replace("\n", "\\n", $payload),
					'time'        => $this->microtime_float() - $this->start_time,
					'caller'      => (__STACK_TRACE__ == TRUE) ? ("[" . xdebug_call_class() . "/" . xdebug_call_function() . "]") : "unknown"
					);
			$hit['time_diff'] = $this->hits_collection[sizeof($this->hits_collection)-1]['time'] - $hit['time'];
			array_push($this->hits_collection, $hit);
		}
	} // end of member function add_hit

	/**
	 * Generate profiling summary.
	 *
	 * The profiling summmary includes RAM utilization for
	 * the current PHP process, complete execution time for the process, and a formatted
	 * summary of events based on the hits collection.
	 *
	 * @return void
	 * @access public
	 */
	function get_profile( )
	{
		$this->end_time = (sizeof($this->hits_collection) > 0) ? $this->hits_collection[sizeof($this->hits_collection)-1]['time'] : $this->microtime_float();
		$parser = new Parser();
		$parser->assign_by_ref("collection", $this->hits_collection);
		return $parser->fetch("debugger.tpl");
	} // end of member function get_profile



	/**
	 * Generates a diagnostic based on profiling information.
	 *
	 * This method stops the timer and generates performance the diagnostic
	 *
	 * @todo Performance diagnostic methods in debugger
	 * @return void
	 * @access private
	 */
	function get_diag( )
	{
		$this->end_time = (sizeof($this->hits_collection) > 0) ? $this->hits_collection[sizeof($this->hits_collection)-1]['time'] : $this->microtime_float();
	} // end of member function get_diag

	/**
	  * Returns a float value for the current timestamp (high precision)
	  *
	  * @return float
	  * @access public
	  */
	function microtime_float()
	{
	   list($usec, $sec) = explode(" ", microtime());
	   return ((float)$usec + (float)$sec);
	}


	/**
	  * Returns the timestamp of the last hit in the collection
	  *
	  * @return float
	  * @access public
	  */
	function get_end_time() {
		$this->end_time = (sizeof($this->hits_collection) > 0) ? $this->hits_collection[sizeof($this->hits_collection)-1]['time'] : $this->microtime_float();
		return $this->end_time;
	}

	/**
	  * Dumps a bunch of useful variables to the collection.
	  *
	  * This method dumps the following structures to the debugger:
	  * - POST
	  * - GET
	  * - SESSION
	  * - COOKIE
	  * - The session cookie data
	  *
	  * @return void
	  */
	function stop() {
		if ((defined("__DEBUG__")) && (__DEBUG__ == TRUE)) {
			$this->add_hit("POST Data", "Dump", NULL, vdump($_POST));
			$this->add_hit("GET Data", "Dump", NULL, vdump($_GET));
			$this->add_hit("SESSION Data", "Dump", NULL, vdump($_SESSION));
			$this->add_hit("FILES Data", "Dump", NULL, vdump($_FILES));
			$this->add_hit("COOKIE Data", "Dump", NULL, vdump($_COOKIE));
			$this->add_hit("Session cookie", "Dump", NULL, vdump(session_get_cookie_params()));
		}
	}


} // end of Debugger
?>
