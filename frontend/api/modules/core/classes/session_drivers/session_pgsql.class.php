<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  * PostgreSQL driver for MADNET session handler
  *
  * This driver complies with the MADNET Session handling interface
  * It uses the PostgreSQL database as a back-end to store and manage
  * session data in real-time.
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class Session_PgSQL {


	/**
	  * Session driver constructor method
	  *
	  * Initializes all the attributes to the proper values.
	  *
	  * @return Session_PgSQL
	  * @access public
	  */
	function Session_PgSQL() {
		$this->table = "user_sessions";
		$this->registry    = Registry::get_registry();

		$this->db    = $this->registry->get_singleton("core", "db_manager");
		$this->error = $this->registry->get_singleton("core", "error_manager");
		#echo "Error manager in " . __CLASS__ . ":<br>";
		#var_dump($this->error);

	}

	/**
	 * Does nothing
	 *
	 * @param string $path
	 * @param string $name
	 * @return boolean
	 */
	function _open($path, $name) {
		return TRUE;
	}

	/**
	 * Does nothing
	 *
	 * @return boolean
	 */
	function _close() {
		return TRUE;
	}

	/**
	 * Deletes a session from the database
	 *
	 * @param string $session_id ID of the session to terminate
	 * @return boolean
	 */
	function _destroy($session_id) {
		$query = "DELETE FROM " . $this->table . " WHERE session_id = '" . $session_id . "'";
		return $this->db->delete($query, TRUE);
	}

	/**
	 * Inserts/Update a session and associated data in the SQL table
	 *
	 * @param string $session_id ID of the session
	 * @param string $stack Session data stack
	 * @return boolean
	 */
	function _write($session_id, $stack) {
		$query = "SELECT session_id FROM " . $this->table . " WHERE session_id = '" . $session_id . "'";

		$result = $this->db->get_row($query, TRUE);


		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			$query = " INSERT INTO " . $this->table
				   . " (session_id, session_ttl, session_start, session_time, session_stack)"
				   . " VALUES('" . $session_id . "', '" . $this->ttl . "', '" . mktime()
				   . "', '" . mktime() . "', '" . pg_escape_string($stack) . "')";
			return $this->db->insert($query, TRUE);
		} else {
			$query = "UPDATE " . $this->table . " SET"
			       . "  session_time = '" . time() . "'"
			       . ", session_stack = '" . pg_escape_string($stack) . "'"
			       . "  WHERE session_id = '" . $session_id ."'";
			return $this->db->update($query, TRUE);
		}
	}

	/**
	 * Reads a session and associated data from the database
	 *
	 * @param string $session_id Session Identifier
	 * @return String
	 */
	function _read($session_id) {
		$query = "SELECT session_stack, session_userid FROM " . $this->table . " WHERE session_id = '" . $session_id . "'";
		$result = $this->db->get_row($query);

		if (($result == DB_QUERY_ERROR) || ($result == DB_NO_RESULT)) {
			return "";
		} else {
			$this->user_id = $result['session_userid'];
			return $result['session_stack'];
		}

	}

	/**
	 * Performs garbage collection
	 *
	 * Calculates which records are expired and delete them
	 * Needs testing
	 *
	 * @param int $ttl
	 * @return boolean
	 */
	function _gc($ttl) {
		return $this->db->delete("DELETE FROM " . $this->table . " WHERE (" . time() . " - session_time) > session_ttl", TRUE);
	}

	/**
	 * Determines the TTL of the current session
	 *
	 * This needs to be done before session_start because the session
	 * driver will use the return value of this method to re-set the
	 * session TTL to the appropriate value, otherwise, PHP will
	 * automatically re-set this value to the default at the next request.
	 *
	 * @return boolean
	 * @todo Performance optimization on _find_cookie_ttl()
	 */
	function _find_cookie_ttl() {
		# Make sure that there is already an existing session
		if (!empty($_COOKIE[SESSION_NAME])) {
			$id = $_COOKIE[SESSION_NAME];
			$query = "SELECT session_ttl FROM " . $this->table . " WHERE session_id = '$id'";
			$result = $this->db->get_row($query);

			if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
				return FALSE;
			} else {
				return $result['session_ttl'];
			}
		} else {
			return FALSE;
		}
	}

	/**
	 * Updates the TTL of the current session to $ttl
	 *
	 * This method sets the TTL in the database to the proper value.
	 *
	 * This has some overhead because we are running an update withoug
	 * knowing if it is necessary, however this is more efficient than
	 * calculating if the update is necessary or not.
	 *
	 * @param int $ttl Time to Live
	 * @return boolean
	 * @todo Performance optimization on _set_ttl()
	 */
	function _set_ttl($ttl) {

		$this->ttl = $ttl;
		# Make sure that there is already an existing session
		if (!empty($_COOKIE[SESSION_NAME])) {

			# Grab the ID of the session and use it to update the TTL
			$id = $_COOKIE[SESSION_NAME];
			$this->ttl = $ttl;
			$query = "UPDATE " . $this->table . " SET session_ttl = '$ttl', session_time = " . $this->db->escape(time()) . " WHERE session_id = '$id'";
			return $this->db->update($query, TRUE);
		} else {
			return FALSE;
		}
	}

	/**
	  * Associates a session row with a MADNET user ID
	  *
	  * @param integer $id
	  * @return boolean
	  */
	function set_user_id($id) {
		if (!($this->user_id == $id)) {
			$query = "UPDATE " . $this->table . " SET session_userid = '$id' WHERE session_id = '" . session_id() . "'";
			return $this->db->update($query, TRUE);
		} else {
			return TRUE;
		}
	}

}

?>