<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
 * Database abstraction manager
 *
 * This class interacts with PEAR::DB to perform database operations
 * but also performs its own error management, operation caching, autoinsert,
 * autoupdates, smarty drop-down selects, character escaping, transaction
 * support, on-demand connection, last insert ID, and prepared statement support.
 *
 * @package MADNET
 * @author  Xavier Spriet
 *
 */
class DB_Manager
{
	/**
	  * Database connection resource
	  *
	  * @var object $datalink
	  * @access private
	  */
	var $datalink;

	/**
	  * Aggregated error manager singleton
	  *
	  * @var object $error_manager
	  * @access private
	  */
	var $error_manager;

	/**
	  * Query & Results cache
	  *
	  * @var array $cache
	  * @access private
	  */
	var $cache;

	/**
	  * Number of rows affected by the last operation
	  *
	  * @var integer $affected_rows
	  * @access public
	  */
	var $affected_rows = 0;

	/**
	  * State of caching before a caching enable() or disable() call
	  *
	  * @var bool $previous_state
	  * @access public
	  * @see restore_previous_state
	  */
	var $previous_state;

	/**
	 * DSN String
	 *
	 * @var string
	 */
	var $dsn;


	/** Aggregations: */

	/** Compositions: */

	 /*** Attributes: ***/

	/**
	  * DB Manager constructor
	  *
	  * The constructor will instanciate all the singleton objects
	  * and initialize the DB connection.
	  *
	  * Since the DB_Manager is usually instanciated as a singleton, there
	  * is only one instance and the constructor is instanciated only once,
	  * therefore, we don't need to check for existing resources or to perform
	  * garbage collection
	  *
	  * @param boolean $enable_caching
	  * @return DB_Manager
	  */
	function DB_Manager($enable_caching = TRUE, $dsn = DB_DSN) {

		$this->dsn = $dsn;
		$this->enable_caching = $enable_caching;
		$this->registry = Registry::get_registry();

		$this->error_manager = $this->registry->get_singleton("core", "error_manager");

		$this->debugger      = $this->registry->get_singleton("core", "debugger");

		$this->error_manager->add_error_to_map(DB_QUERY_ERROR, "Database Query Error");
		$this->error_manager->add_error_to_map(DB_CONNECT_ERROR, "Database connection error");

		#echo "Error manager in " . __CLASS__ . ":<br>";
		#var_dump($this->error_manager);

		$this->db_connect();

		if (DB_CONNECT_ERROR == $this->datalink) {
			$this->error_manager->handle_fatal('dbconnect');
		}

		$this->cache = array();
	}

	/**
	  * Initializes the DB connection based on DSN
	  *
	  * @return void
	  */
	function db_connect() {
		#$dsn = DB_DSN;
		$dblink = DB::connect($this->dsn, __PCONNECT__);


		if (DB::isError($dblink)) {
			$this->error_manager->err_from_code(DB_CONNECT_ERROR, $dblink->getDebugInfo());
			#$dblink = DB_CONNECT_ERROR;
			$this->error_manager->err_from_string("Unable to establish a database connection");
			$this->error_manager->handle_fatal("Database connection error using DSN " . DB_DSN . " - " . $dblink->getMessage());
			#return DB_CONNECT_ERROR;
		} else {
			$dblink->setFetchMode(DB_FETCHMODE_ASSOC);
		}
		$this->datalink = $dblink;
	}

	/**
	  * Performs a SELECT query against the database
	  *
	  * This method first checks the query cache and returns any hit on
	  * that cache. If no hit was found, it executes the query and
	  * returns a result-set structured as a hash-table
	  *
	  * @param string $query
	  * @return mixed
	  */
	function select($query) {

		// Return cached results if we have any.
		if (($result = $this->is_cached($query)) != FALSE) {
			$this->debugger->add_hit("Cache hit on query", NULL, NULL, htmlentities($query));
			return $result;
		} else {
			$this->debugger->add_hit("Cache miss on query", NULL, NULL, htmlentities($query));
		}

		$result = $this->datalink->getAll($query, NULL, DB_FETCHMODE_ASSOC);

		if (PEAR::isError($result)) {
			$this->error_manager->err_from_code(DB_QUERY_ERROR, $result->getMessage());
			return DB_QUERY_ERROR;
		} else {
			if (sizeof($result) == 0) {
				return DB_NO_RESULT;
			}
			$this->debugger->add_hit("Successful query", NULL, NULL, htmlentities($query));
		}

		$this->add_to_cache($query, &$result);

		return $result;
	}


	/**
	  * Performs a SELECT query and return output structured for SMARTY drop-downs
	  *
	  * Smarty templates do not know how to work with hash-tables returned by select()
	  * because it does not know which key is the index and which key is the value.
	  *
	  * This function will return the hash-table in a format that smarty can use.
	  *
	  * @param string $query
	  * @param string $index
	  * @param string $value
	  * @access public
	  */
	function select_smarty($query, $index, $value) {
		$result = $this->select($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			$temp = array();
			#var_dump($result);
			foreach($result as $current) {
				$temp[$current[$index]] = $current[$value];
			}
			return $temp;
		}

	}

	/**
	  * Returns cache results if a query is in the cache, FALSE otherwise
	  *
	  * @param string $query
	  * @return mixed
	  */
	function is_cached($query) {
		if (!$this->enable_caching) { return FALSE; }
		return $this->cache[md5($query)] ? $this->cache[md5($query)] : FALSE;
	}


	function add_to_cache($query, &$result) {
		if (!$this->enable_caching) { return FALSE; }
		$this->cache[md5($query)] = &$result;
	}

	function enable_caching() {
		$this->previous_state = $this->enable_caching;
		$this->enable_caching = TRUE;
	}

	function disable_caching() {
		$this->previous_state = $this->enable_caching;
		$this->enable_caching = FALSE;
	}

	function restore_previous_state() {
		$this->enable_caching = $this->previous_state;
	}

	/**
	  * Returns one row of a SQL select query as a hash-table
	  *
	  * This method first analyses the query to determine if the
	  * LIMIT 1 parameter has been specified. If it has not, the
	  * method will insert it automatically (which means this method
	  * will not play well with Oracle, MSSQL or DB2). It will then
	  * run the query and return the results as a hash table.
	  *
	  * Note that using this offset system is a lot more efficient
	  * than just selecting everything and returning the first row of
	  * the result-set
	  *
	  * @param string $query
	  * @return mixed
	  */
	function get_row($query) {

		// Add the LIMIT 1 if necessary
		if (substr($query, strlen($query)-strlen("LIMIT 1")) != "LIMIT 1") {
			$query .= " LIMIT 1";
		}

		// Return cached results if we have any.
		if (($result = $this->is_cached($query)) != FALSE) {
			$this->debugger->add_hit("Cache hit on query", NULL, NULL, htmlentities($query));
			return $result;
		} else {
			$this->debugger->add_hit("Cache miss on query", NULL, NULL, htmlentities($query));
		}


		$result = $this->datalink->getRow($query);

		if (PEAR::isError($result)) {
			$this->error_manager->err_from_code(DB_QUERY_ERROR, $query . " - " . $result->getMessage());
			return DB_QUERY_ERROR;
		}


		if (NULL == $result) {
			return DB_NO_RESULT;
		}
		$this->add_to_cache($query, &$result);
		return $result;

	}

	/**
	  * Performs an SQL INSERT INTO query
	  *
	  * @param string $query
	  * @return mixed
	  */
	function insert($query, $force=FALSE) {
		
		
		if (FALSE == $force) {
			if (has_perm("Guest/Demo Account")) {
				$this->debugger->add_hit("Demo mode enabled");
				$this->error_manager->err_from_string("This operation is not available in Demo mode", FALSE);
				return FALSE;
			}
		}
		
		
		
		if (eregi("insert", $query)) {
			$this->debugger->add_hit("Insert query", NULL, NULL, htmlentities($query));
		}

		$res = $this->datalink->query($query);

		if (PEAR::isError($res)) {
			$this->error_manager->err_from_code(DB_QUERY_ERROR, $query . " " . $res->getMessage());
			return DB_QUERY_ERROR;
		}

		$this->affected_rows = $this->datalink->affected_rows;

		return $res;
	}

	/**
	  * Performs an SQL UPDATE query
	  *
	  * @param string $query
	  * @return mixed
	  */
	function update($query, $force=FALSE) {
		$this->debugger->add_hit("Update query", NULL, NULL, htmlentities($query));
		return $this->insert($query, $force);
	}

	/**
	  * Performs an SQL DELETE query
	  *
	  * @param string $query
	  * @return mixed
	  */
	function delete($query, $force=FALSE) {
		$this->debugger->add_hit("Delete query", NULL, NULL, htmlentities($query));
		return $this->insert($query, $force);
	}

	/**
	  * Creates a prepared statement based on a SQL query template
	  *
	  * This method will RETURN the compiled prepare statement.
	  * Please keep that in mind.
	  *
	  * @param string $stmt
	  * @return object
	  * @see $this->execute()
	  */
	function prepare($stmt) {
		$pstmt = $this->datalink->prepare($stmt);

		if (PEAR::isError($pstmt)) {
			$this->error_manager->err_from_code(DB_QUERY_ERROR, $stmt);
			return DB_MISC_ERROR;
		}

		return $pstmt;
	}

	/**
	  * This method executes a prepared statement
	  *
	  * You will need to provide this method with the compiled
	  * prepare statement and an indexed array (properly ordered)
	  * with your data-elements.
	  *
	  * @param object $pstmt
	  * @param array  $data
	  * @see $this->prepare()
	  */
	function execute($pstmt, $data = array()) {
		$result = $this->datalink->execute($pstmt, $data);

		if (PEAR::isError($result)) {
			$this->error_manager->err_from_code(DB_QUERY_ERROR, "prepared statement");
			return DB_QUERY_ERROR;
		}

		return $result;
	}

	/**
	  * Inserts data in a table based on a hash-table
	  *
	  * The key->value pairs in the hash-table are used
	  * to create an SQL INSERT INTO query. DB performs all the
	  * string escaping and executes the generated query automagically
	  * against the table specified as the first argument.
	  *
	  * @param string    $table
	  * @param hashtable $hash
	  * @return mixed
	  */
	function auto_insert($table, $hash) {
		$result = $this->datalink->autoExecute($table, $hash, DB_AUTOQUERY_INSERT);

		if (PEAR::isError($result)) {
			$this->error_manager->err_from_code(DB_QUERY_ERROR, "auto-insert error - " . $result->getUserInfo());
			return DB_QUERY_ERROR;
		}

		return $result;
	}


	/**
	  * Updates a row in a DB table using values from a hash-table
	  *
	  * The concept is similar to auto_insert(). DB will generate an SQL UPDATE
	  * query using key->value pairs from the hash-table specified, taking care
	  * of any required escaping. It will use the condition passed as the $where
	  * argument for the WHERE clause (e.g "test = '16'").
	  *
	  * @param string    $table
	  * @param hashtable $hash
	  * @param string    $where
	  * @return mixed
	  */
	function auto_update($table, $hash, $where) {

		$result = $this->datalink->autoExecute($table, $hash, DB_AUTOQUERY_UPDATE, $where);

		if (PEAR::isError($result)) {
			$this->error_manager->err_from_code(DB_QUERY_ERROR, $result->getDebugInfo());
			return DB_QUERY_ERROR;
		}

		return $result;
	}

	/**
	  * Escapes a string to prevent SQL injection attacks
	  *
	  * Please read more about SQL injection to understand what this method does
	  *
	  * @param string $str
	  * @return string
	  */
	function escape($str) {
		return $this->datalink->quoteSmart($str);
	}
	
	

	/**
	  * Escapes a column, database, or table identifier
	  *
	  * Certain RDBMS use a different escaping character for identifiers (keys)
	  * as they do for standard strings. We let the DB driver figure this all
	  * out for us.
	  *
	  * @param string $id
	  * @return string
	  */
	function quote_key($id) {
		return $this->datalink->quoteIdentifier($id);
	}

	/**
	  * Returns the primary key value of the last object inserted in a table
	  *
	  * Simply specify the table name and primary key value
	  * and this method will build a get_row() query to find out what
	  * the highest value of that primary key is. This will not work if you
	  * are not using predictable sequences in your tables.
	  *
	  * @param string $table
	  * @param string $pkey
	  * @return string
	  */
	function get_last_insert_id($table, $pkey) {
		$query = "SELECT {$pkey} AS \"id\" FROM {$table} ORDER BY id DESC LIMIT 1";
		$res = $this->get_row($query);

		if ((DB_QUERY_ERROR == $res) || (DB_MISC_ERROR == $res)) {
			#$this->debugger->add_hit("No last insert id... ", NULL, NULL, $res->getUserInfo());
			$this->error_manager->err_from_string("Unable to retrieve ID of new record");
			return FALSE;
		} else {
			#$this->debugger->add_hit("Last insert id: " . $res['id']);
			return $res['id'];
		}
	}

	function start_transaction() {
		$this->debugger->add_hit("Initializing transaction");
		$this->datalink->autocommit(FALSE);
		return TRUE;
	}

	function commit_transaction() {
		$this->debugger->add_hit("Committing last transaction");
		$res = $this->datalink->commit();
		$this->datalink->autocommit(TRUE);

		if (PEAR::isError($res)) {
			$this->error_manager->err_from_code(DB_QUERY_ERROR, $result->getDebugInfo());
			return FALSE;
		} else {
			return TRUE;
		}
	}

} // end of DB_Manager
?>