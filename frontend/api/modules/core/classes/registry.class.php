<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * Object Registry.
  *
  * The Registry is a static singleton object that
  * maintains a stack of singleton objects.
  *
  * If a procedure requests a singleton object to be
  * dispatched, the registry will create a hash based on
  * the name of the requested object, check its stack and
  * dispatch a REFERENCE to the EXISTING singleton previously
  * stored. If it cannot find it, it will instanciate that object,
  * store a reference to the object in its stack, and dispatch this
  * reference. This means that singleton objects are simply
  * passed around by references, which makes the whole process very
  * lightweight.
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class Registry {

	/**
	  * Our singleton objects stack
	  *
	  * @var hashtable $stack
	  * @access private
	  */
	var $stack;

	/**
	  * Set up a static stack data-structure to store singleton objects
	  *
	  * PHP4 has a really hard time understanding the concept of reference
	  * handling. If we create the stack externally, it will destroy it,
	  * if we create the stack before the constructor, it will reset it every
	  * time the registry is queried... so we create it at the constructor level
	  * from stratch as a static structure.
	  *
	  * @return Registy
	  * @access public
	  */
	function Registry() {
		static $stack = FALSE;
		if (!$stack) { $stack = array(); }
		$this->stack = &$stack;
	}

	/**
	  * Returns a pointer to a static (singleton) instance of the registry
	  *
	  * @example "Obtaining a reference to the Registry Singleton" $registry = Registry::get_registry();
	  * @return Registry
	  */
	function get_registry() {
		static $_reg = FALSE;

		if (!$_reg) {
			$_reg = new Registry;
		}

		return $_reg;
	}

	/**
	  * Dispatch a reference to the requested singleton
	  *
	  * This function checks if a singleton object matching the
	  * specified specs already exists and returns a ref to it
	  * if it does. Otherwise, it instanciates the object,
	  * adds a reference to it in the stack and dispatch the
	  * reference. This makes the whole process completely transparent
	  * for the object that requested a singleton.
	  *
	  * @param string $module
	  * @param string $class
	  * @return reference
	  */
	function &get_singleton($module, $class) {
		$varname = 'singleton_' . $module . '_' . $class;

		# Push the singleton reference if we already have one in the stack
		if ($this->entry_exists($varname)) {
			return $this->get_entry($varname);
		}

		# Require_once modules/$module/$class.class.php
		require_class($module, $class);

		# Note that we are creating a REFERENCE
		$instance = new $class;
		$this->add_entry($varname, &$instance);
		return $instance;
	}


	/**
	  * Adds the REFERENCE passed to the stack
	  *
	  * This method takes in a reference to an object
	  * and a key indicating where to store it, and it
	  * will store that reference in the
	  * stack (it may look like we are storing a reference
	  * to the reference, but NO. That's how f****d-up PHP4 is
	  * when it comes to reference handling... You have to
	  * explicitely state that what you are storing is a
	  * reference.
	  *
	  * @param string $key
	  * @param reference $value
	  * @return boolean
	  */
	function add_entry($key, &$value) {
		$this->stack[md5($key)] = &$value;
		return TRUE;
	}

	/**
	  * Returns a REFERENCE to a singleton in the stack
	  *
	  * Remember how we had to be explicit about the fact that
	  * we were storing a reference to the stack in $this->add_entry()?
	  *
	  * Forget all that. Since PHP doesn't support returning refs
	  * (it's not a proper syntactical construct), you just dispatch
	  * the value directly from the stack.
	  *
	  * Then to explicitely specify that you're returning a reference, you
	  * specify that directly in the function declaration... :-\
	  *
	  * @param string $key
	  * @return reference
	  */
	# (since we stored a reference in the stack in the first place)
	function &get_entry($key) {
		return $this->stack[md5($key)];
	}

	/**
	  * Returns TRUE if the singleton class is in the stack, FALSE otherwise.
	  *
	  * @param string $key
	  * @return boolean
	  */
	function entry_exists($key) {
		return ($this->get_entry($key) !== NULL);
	}




}


?>