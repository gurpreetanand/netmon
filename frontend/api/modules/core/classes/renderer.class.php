<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * Output handler
  *
  * Presentation Layer for the View  This object handles output filtering as well as
  * final display of data based on the templating engine.
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class Renderer
{

	/**
	 * Cache Manager
	 * 
	 * @var Cache_Manager cache
	 */
	var $cache;

	/**
	 * Smarty Wrapper
	 *
	 * @access private
	 * @var Parser $parser
	 */
	var $parser;

	/**
	  * Error manager singelton
	  *
	  * @var $error_manager
	  * @access private
	  */
	var $error_manager;

	/**
	  * Render class constructor method
	  *
	  * Initializes the registry, error manager, debugger and parser.
	  *
	  * @return Renderer
	  */
	function Renderer() {
		require_class("core", "Parser");
		$this->registry      = &Registry::get_registry();
		$this->cache         = &$this->registry->get_singleton("core", "cache_manager");
		$this->error_manager = &$this->registry->get_singleton("core", "error_manager");
		$this->debugger      = &$this->registry->get_singleton("core", "debugger");
		
		$this->parser = new Parser;
	}

	/**
	 * Parses the specified template
	 * HTML-Tidy filtering also happens here.
	 *
	 * @param string $template
	 * @param string $title
	 * @param string $content
	 * @return string
	 * @access public
	 */
	function parse_template($template, $title, $content)
	{
		$this->parser->assign("content", $content);
		$this->parser->assign("title", $title);
			
		

		$this->parser->assign_by_ref("errors", $this->error_manager->errors_collection);
		$this->debugger->stop();


		$this->parser->assign_by_ref("debugger", $this->debugger);

		$this->parser->register_modifier("dec_fix", "dec_fix");

/*
		$js = $this->cache->get("js_files");
		$css = $this->cache->get("css_files");
		
		$this->debugger->add_hit("JS: ", NULL, NULL, vdump($js));
		$this->debugger->add_hit("CSS: ", NULL, NULL, vdump($css));
		echo 'Stack: '; var_dump($this->cache->stack);
		
		$this->parser->assign("js_imports", $js);
		$this->parser->assign("css_imports", $css);
*/
		$content = $this->parser->fetch($template);
		if (ENABLE_TIDY_EXT == TRUE) {

			/*
			$tidy_conf = array(
					   'output_html'      => TRUE,
					   'add-xml-decl'     => false,
					   'doctype'          => "auto",
					   'hide-endtags'     => false,
					   'logical-emphasis' => true,
					   'clean'            => FALSE,
			           'indent'           => true,
#					   'output-xhtml'     => false,
			           'wrap'             => 250,
			           'output-encoding'  => 'ascii',
			           'tidy-mark'        => false,
			           'newline'          => 'LF',
			           'output-bom'       => false,

			           );

			foreach ($tidy_conf as $key => $value) {
			   tidy_setopt($key,$value);
			}

			tidy_set_encoding('ascii');
			tidy_parse_string($content);
			tidy_clean_repair();
			#print_r(tidy_get_config());
			*/

			/**********************************************************
			*
			* The OO Wrapper for tidy (and most other constructs)
			* is not supported < PHP5
			*
			* $tidy = new tidy;
			* $tidy->parseString($content, $tidy_conf, 'ascii');
			* $tidy->cleanRepair();
			*
			**********************************************************/

			echo $content;
			#echo tidy_get_output();
		} else {
			echo $content;
		}


	} // end of member function parse_template



	/**
	 * Performs any required post-filtering on the output.
	 *
	 * For the time being, this simply implies replacing any instance
	 * of the master DB password that could have slipped in the output
	 * through some kind of malicious attack by a bunch of pretty stars.
	 *
	 * @param string output
	 * @return void
	 * @access private
	 */
	function post_filter( $output )
	{
		return str_replace(DB_PASS, "****************", $output);
	} // end of member function post_filter



} // end of Renderer
?>
