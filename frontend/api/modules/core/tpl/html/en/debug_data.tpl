<!-- {$smarty.template} ($Id$) -->


<script language="Javascript">

// Array elements in order: timestamp, abstract, severity, payload

{if $debugger}
DEBUG_DATA = [

	{foreach from=$debugger->hits_collection item="hit"}
		["{$hit.time|dec_fix}", "{$hit.type|default:"Message"}", {$hit.severity}, "{$hit.description}", "{$hit.payload|escape:"javascript"|replace:'\n':"<br>"}", "{$hit.caller|default:"Anonymous"}"],
	{/foreach}
		["{$debugger->get_end_time()|dec_fix}", "Completed", 0, "Script execution completed", "", "System"]
	];

{/if}
</script>

<!-- end of debug_data.tpl -->
