{literal}
<html>
<head>
<title>Netmon Debugging Engine Report</title>
	<link href="/assets/css/_core.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/_print.css" rel="stylesheet" type="text/css" media="print">
	<script src= "/assets/jscript/_core.js"></script>
	<script src = "/assets/jscript/YAHOO.js" ></script>

	<script src = "/assets/jscript/treeview.js" ></script>
	<script src = "/assets/jscript/event.js" ></script>
	<script src = "/assets/jscript/dom.js" ></script>
	<script src="/assets/jscript/_debugger.js"></script>
	
<!--<link rel="stylesheet" href="style.css" type="text/css">-->
</head>
<body>
<div class="panel noPrint">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<a href="javascript:window.opener.location.reload(); window.close();"><img src="assets/buttons/button_refresh.gif" width="24" height="24" class="icon" title="Refresh"></a> 
	<img src="assets/buttons/button_print.gif" class="icon" onClick="window.print();" title="Print this page"> 

</div>
<div class="printOnly">
	<h1>Netmon Debugging Engine Report</h1>
	<br /><br />
</div>
<div class="datagrid center">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
	<th width="10%">Exec. Time</th>
	<th width="13%">Caller</th>
	<th width="20%">Event</th>

	<th width="7%">Sev.</th>
	<th width="50%"><div align="left">Details</div></th>
</tr>
{DEBUG_ITEMS}

<tr>
  <td colspan="5">
  <strong>$Id$</strong>
  </td>
</tr>
</table>

<!--<script>myWindow.document.close()</script>-->

</div>
</body>
</html>
{/literal}