<!-- {$smarty.template} ($Id$) -->

	<table width='100%' height='100%' align='center' border='0'>
		<tr>
			<td align='left' width='75px'><img src='/assets/images/{if FALSE === $mirroring}down{else}up{/if}.png' width='48px' height='48px' /></td>
			<td align='left'>{$mirroring_msg}
			{if FALSE === $mirroring}
			<br /><a href="http://wiki.netmon.ca/index.php/How_to_set_up_port_spanning_on_Cisco_devices">Instructions for Cisco Devices</a><br />
			<a href="http://wiki.netmon.ca/index.php/How_to_set_up_port_spanning_on_HP_devices">Instructions for HP Devices</a>
			{/if}
			</td>
		</tr>
	</table>

<!-- End of {$smarty.template} -->
