<!-- {$smarty.template} ($Id$) -->

{import_js files="yui.yahoo,yui.utilities,yui.event,yui.dom,yui.animation,yui.container,yui.dragdrop,ext/adapter/ext/ext-base,ext/ext-all-debug,setup_wizard"}
{import_css files="yui_sam.container,yui_sam.button"}

{if TRUE === $first_launch}
{import_css files="ext.ext-all,ext.xtheme-gray"}
{/if}

{literal}
<script language="JavaScript">
// Ensure this doesn't appear within a frame
if (parent.frames.length > 0){parent.location.href = location.href;}
</script>

<style type="text/css">
.loginwindow{
display:block
}
.loginwindow *{
display:block;
height:1px;
overflow:hidden;
background:#FFF;
}
.loginwindow1{
border-right:1px solid #959390;
padding-right:1px;
margin-right:3px;
border-left:1px solid #959390;
padding-left:1px;
margin-left:3px;
background:#b8b5af;
}
.loginwindow2{
border-right:1px solid #71706f;
border-left:1px solid #71706f;
padding:0px 1px;
background:#bfbbb5;
margin:0px 1px;
}
.loginwindow3{
border-right:1px solid #bfbbb5;
border-left:1px solid #bfbbb5;
margin:0px 1px;
}
.loginwindow4{
border-right:1px solid #959390;
border-left:1px solid #959390;
}
.loginwindow5{
border-right:1px solid #b8b5af;
border-left:1px solid #b8b5af;
}
.loginwindow_content{
padding:0px 5px;
background:#FFF;
}
DIV.backup_panel {
	border-left: 1px solid White;
	border-top: 1px solid White;
	border-bottom: 1px solid #808080;
	background-color: #D4D0C8;
	padding: 4px;
	font-family: Tahoma, sans-serif;
	font-size: 11px;
}

#wait.yui-panel .bd { 
	background-color: #D4D0C8; 
	padding: 0px;
	border: none;
	text-align: center;
}
		
#wait.yui-panel .hd { 
	background-color: #333399;
	
	background-image: url('/assets/core/bg_titlebar.gif');
	background-position: top left; 
	background-repeat: repeat-x; 
	
	padding: 6px;
	border: none;
	text-align: center;
	vertical-align: middle;
	color: #ffffff;
}

#wait.yui-panel .ft {
	background-color: #D4D0C8;
	padding: 3px;
	border: none;
	text-align: center;
}

</style> 

{/literal}

<script language="Javascript">
document.body.className = 'yui-skin-sam';

{literal}
// This is used to detect a time-drift between the server and the client.
// See http://www.netmon.ca/support/technotes/2006_01.htm for more info.
function calcTimeDrift() {
	objDate = new Date();
	serverTime = {/literal}{$smarty.now}{literal};
	clientTime = Number(objDate)/1000;
	diff = Math.round(Math.abs(clientTime-serverTime));

	if (diff >= 3300) {
		document.writeln('{/literal}{"Warning: Time drift detected between your browser and the server. If you are unable to authenticate, please read <a href=\"http://www.netmon.ca/support/technotes/2006_01.htm\">Technote #2006-1</a>."|message_bar}{literal}');
	}
}

calcTimeDrift();

{/literal}
{if TRUE === $first_launch}
Ext.onReady(wizard_launch);
{/if}
{literal}

function handleOK() {
	YAHOO.netmon.panels.wait.hide();
}

function showDaysRemaining(){
YAHOO.namespace("netmon.panels");
		YAHOO.netmon.panels.wait = new YAHOO.widget.Dialog("wait",
		{
			width:       '300px',
			fixedcenter: true,
			modal:       true,
			{/literal}
			{if $ds.days_remaining > 0}
			close: true,
			buttons:    [ {ldelim} text:"OK", handler:handleOK, isDefault:false {rdelim} ],
			{else}
			close: false,
			{/if}
			{literal}
			effect:      {effect:YAHOO.widget.ContainerEffect.FADE, duration:0.5}
			
		}
		);
{/literal}	
		YAHOO.netmon.panels.wait.setHeader('Trial License');
		{if $ds.days_remaining > 0}
		YAHOO.netmon.panels.wait.setBody('You are using a time-limited trial version of Netmon.  You have {$ds.days_remaining} days remaining.');
		{else}
		YAHOO.netmon.panels.wait.setBody('Your trial license for Netmon SE has expired. We hope you have enjoyed using our software. Please contact <a href="mailto:sales@netmon.ca">sales@netmon.ca</a> to purchase a retail license.');
		{/if}
{literal}
		YAHOO.netmon.panels.wait.render(document.body);
		YAHOO.netmon.panels.wait.show();
}
{/literal}

{if (1 == $ds.is_trial)}
	showDaysRemaining();
{/if}
</script>
<form action="?module=mod_user&action=auth" method="POST" name="{$smarty.const.SITE_NAME}_auth" onsubmit="showProgressIndicator('progress');">
<div align="center">
<div style="width: 500px; margin-left: 10%; margin-right: 10%; margin-bottom: 10%; margin-top: 10%;">
<b class="loginwindow">
<b class="loginwindow1"><b></b></b>
<b class="loginwindow2"><b></b></b>
<b class="loginwindow3"></b>
<b class="loginwindow4"></b>
<b class="loginwindow5"></b>
</b> <div class="loginwindow_content">


          <table border="0" cellpadding="10" cellspacing="0">
            <tr>
             <td><div align="left" style="line-height: 175%; color: #666666;">Licensed To: {$ds.company_name}<br>
               Registration Code: {$ds.registration_key}<br />
               License Type: {$ds.devices_used}/{$ds.devices} devices
               </div></td>
            <td><div class="version-title">Version</div><div class="version-number">{$smarty.const.VERSION}</div></td>

            </tr>
            <tr valign="bottom"> 
              <td colspan="2"><div align="center"><img src="assets/toolbar/netmon-logo.gif" alt="Netmon" width="196" height="87">
				  				  				  </div></td>
            </tr>
            <tr> 
              <td width="45%"><div align="right"><strong>User ID:</strong> <br>
                <br>
                <strong>Password:</strong></div></td>
             <td width="55%" ><div align="left">{input type="text" name="username" size="15" class="login_input" value=$smarty.post.username doc="Username"}
               <br>
              <br>
              {input type="password" name="password" size="15" class="login_input" value=$smarty.post.password doc="Password"}</div></td>
            </tr>
            <tr> 
              <td colspan="2" align="center"> 
              
              		<img style="float: right; display: inline" src="/assets/progress_big.gif" id="progress" class="progress_big" />
                    {input name="Submit" type="submit" class="button" value="Log In Now"}
			  </td>
            </tr>
            <tr>
              <td><span style="color: #999999">&copy; {$smarty.now|date_format:"%Y"}  {$smarty.const.COMPANY} All rights reserved.</span></td>
              <td><div align="right"></div></td>
            </tr>
          </table>
      </div>

<b class="loginwindow">
<b class="loginwindow5"></b>
<b class="loginwindow4"></b>
<b class="loginwindow3"></b>
<b class="loginwindow2"><b></b></b>
<b class="loginwindow1"><b></b></b>
</b>
</div> 

</form>


<script language="Javascript">
	document.{$smarty.const.SITE_NAME}_auth.username.focus();
</script>

<!-- end of {$smarty.template} -->
