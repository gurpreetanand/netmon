<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,yui.event,yui.dom,yui.animation,yui.container,yui.dragdrop,yui.connection"}
{import_css files="yui_sam.container,yui_sam.button"}
{literal}
<script language="JavaScript">
// Ensure this doesn't appear within a frame
if (parent.frames.length > 0){parent.location.href = location.href;}
</script>

<style type="text/css">
.loginwindow{
display:block
}
.loginwindow *{
display:block;
height:1px;
overflow:hidden;
background:#D4D0C8
}
.loginwindow1{
border-right:1px solid #959390;
padding-right:1px;
margin-right:3px;
border-left:1px solid #959390;
padding-left:1px;
margin-left:3px;
background:#b8b5af;
}
.loginwindow2{
border-right:1px solid #71706f;
border-left:1px solid #71706f;
padding:0px 1px;
background:#bfbbb5;
margin:0px 1px;
}
.loginwindow3{
border-right:1px solid #bfbbb5;
border-left:1px solid #bfbbb5;
margin:0px 1px;
}
.loginwindow4{
border-right:1px solid #959390;
border-left:1px solid #959390;
}
.loginwindow5{
border-right:1px solid #b8b5af;
border-left:1px solid #b8b5af;
}
.loginwindow_content{
padding:0px 5px;
background:#D4D0C8;
}

TD {
line-height: 0px;
}

</style> 

{/literal}


<form id="{$smarty.const.SITE_NAME}_activation" name="{$smarty.const.SITE_NAME}_activation" onSubmit="return performActivation();">
<div align="center" style="opacity: 0; filter='alpha(opacity=0)';" id="actDialog">
<div style="width: 500px; margin-left: 10%; margin-right: 10%; margin-bottom: 1%; margin-top: 1%;">
<b class="loginwindow">
<b class="loginwindow1"><b></b></b>
<b class="loginwindow2"><b></b></b>
<b class="loginwindow3"></b>
<b class="loginwindow4"></b>
<b class="loginwindow5"></b>
</b> <div class="loginwindow_content">


<table border="0" cellpadding="10" cellspacing="0" width="100%">
  <tr>
    <td><div align="left" style="line-height: 175%; color: #000000;"><strong>Product Activation</strong></div></td>
    <td><div class="version-title">Version</div><div class="version-number">{$smarty.const.VERSION}</div></td>
  </tr>
  <tr valign="bottom"> 
    <td colspan="2" align="center"><div align="center"><img src="assets/core/logo_gray.gif" alt="Netmon" width="196"></div>
    </td>
  </tr>
</table>

<fieldset>
<legend>System Registration</legend>
<table border="0" cellpadding="10" cellspacing="0" width="100%">
  <tr> 
    <td width="35%"><div align="right"><strong>Registration Key:</strong></div></td>
    <td width="65%"><div align="left">{input type="text" name="key" size="16" class="login_input" value=$smarty.post.key|default:$activation.registration_key doc="Registration Key"}
  </tr>
</table>
</fieldset>

<fieldset>
<legend>Corporate Information</legend>
<table border="0" cellpadding="10" cellspacing="0" width="100%">
  <tr>
  	<td width="35%"><div align="right">Company Name</div></td>
  	<td width="65%"><div align="left">{input type="text" name="company_name" size="30" class="login_input" value=$smarty.post.company_name|default:$activation.company_name doc="Company Name"}</div></td>
  </tr>
  <tr>
  	<td width="35%"><div align="right">Address</div></td>
  	<td width="65%"><div align="left"><textarea name="company_address" cols="30" rows="4" class="login_input" title="Company Address">{$smarty.post.company_address|default:$activation.company_address}</textarea></div></td>
  </tr>
  <tr>
  	<td width="35%"><div align="right">City</div></td>
  	<td width="65%"><div align="left">{input type="text" name="company_city" size="30" class="login_input" value=$smarty.post.company_city|default:$activation.company_city doc="Company City"}</div></td>
  </tr>
  <tr>
  	<td width="35%"><div align="right">Province/State</div></td>
  	<td width="65%"><div align="left">{input type="text" name="company_state" size="30" class="login_input" value=$smarty.post.company_state|default:$activation.company_state doc="Company State/Province"}</div></td>
  </tr>
  
  <tr>
  	<td width="35%"><div align="right">Country</div></td>
  	<td width="65%"><div align="left">{input type="text" name="company_country" size="30" class="login_input" value=$smarty.post.company_country|default:$activation.company_country doc="Company Country"}</div></td>
  </tr>
</table>
</fieldset>

<fieldset>
<legend>Contact Information</legend>
<table border="0" cellpadding="10" cellspacing="0" width="100%">
  <tr>
  	<td width="35%"><div align="right">First Name</div></td>
  	<td width="65%"><div align="left">{input type="text" name="contact_first_name" size="15" class="login_input" value=$smarty.post.contact_first_name|default:$activation.contact_first_name doc="Contact First Name"}</div></td>
  </tr>
  <tr>
    <td width="35%"><div align="right">Last Name</div></td>
  	<td width="65%"><div align="left">{input type="text" name="contact_last_name" size="15" class="login_input" value=$smarty.post.contact_last_name|default:$activation.contact_last_name}</div></td>
  </tr>
  <tr>
    <td width="35%"><div align="right">Email Address</div></td>
  	<td width="65%"><div align="left">{input type="text" name="contact_email" size="15" class="login_input" value=$smarty.post.contact_email|default:$activation.contact_email}</div></td>
  </tr>
  <tr>
    <td width="35%"><div align="right">Phone</div></td>
  	<td width="65%"><div align="left">{input type="text" name="contact_phone" size="12" class="login_input" value=$smarty.post.contact_phone|default:$activation.contact_phone} (Ext: {input size="4" type="text" name="contact_phone_ext" class="login_input" doc="Phone Extension" value=$smarty.post.contact_phone_ext|default:$activation.contact_phone_ext})</div></td>
  </tr>
  
</table>
</fieldset>

<div id="actStatus">&nbsp;</div>

<table border="0" cellpadding="10" cellspacing="0"  width="100%">
            <tr> 
              <td colspan="2" align="center"> 
              		<img style="float: right; display: inline" src="/assets/progress_big.gif" id="progress" class="progress_big" />
                    {input name="Submit" type="submit" class="button" value="Activate"}
			  </td>
            </tr>
            <tr>
              <td><span style="color: #999999">&copy; {$smarty.now|date_format:"%Y"}  {$smarty.const.COMPANY} All rights reserved.</span></td>
              <td><div align="right"></div></td>
            </tr>
          </table>
      </div>

<b class="loginwindow">
<b class="loginwindow5"></b>
<b class="loginwindow4"></b>
<b class="loginwindow3"></b>
<b class="loginwindow2"><b></b></b>
<b class="loginwindow1"><b></b></b>
</b>
</div> 

</form>


<script language="Javascript">
document.body.className = 'yui-skin-sam';
	
	// Sets the focus to the reg key input field
	initFocus = function() {ldelim}
		document.{$smarty.const.SITE_NAME}_activation.key.focus();
	{rdelim}

	{literal}
	
	// Submits form payload through an AJAX POST request
	performActivation = function() {
		console.log("Activating...");
		showProgressIndicator('progress');
		formObj = document.getElementById('{/literal}{$smarty.const.SITE_NAME}{literal}_activation');
		YAHOO.util.Connect.setForm(formObj);
		callback = {success: ajax_success, failure: ajax_failure};
		conn = YAHOO.util.Connect.asyncRequest('POST', '?module=core&action=activate&root_tpl=blank', callback);
		return false;
	}
	
	// Success handler
	ajax_success = function(o) {
		resp = o.responseText;
		
		divObj = document.getElementById('actStatus');
		divEl = new YAHOO.util.Element('actStatus'); 
		//divObj.style.opacity = 0;
		//divObj.style.filter = "alpha(opacity=0)";
		divEl.setStyle('opacity', '0.1');
		
		var transition_attributes = {
			opacity: {to: 1},
			duration: 1
		}
		
		var transition = new YAHOO.util.Anim(divObj, transition_attributes);
		
		if ("OK" == resp) {
			divObj.innerHTML = '<strong><font color="#00AC27">ACTIVATION SUCCESSFUL</font></strong>';
			transition.onComplete.subscribe(toLoginDialog);
		} else {
			divObj.innerHTML = '<fieldset style="text-align: left"><legend><strong><font color="#ff0000">Error Report:</font></strong></legend><strong>'+resp+'</font></strong></fieldset>';
			transition.onComplete.subscribe(initFocus);			
		}
		
		
		hideProgressIndicator('progress');
		transition.animate();
	}
	
	toLoginDialog = function() {
		document.location = '?module=core&action=login_dialog';
	}
	
	// Failure handler
	ajax_failure = function(o) {
		hideProgressIndicator('progress');
		divObj = document.getElementById('actStatus');
		resp = o.statusText;
		
		divObj = document.getElementById('actStatus');
		divObj.style.opacity = 0;
		divObj.style.filter = "alpha(opacity=0)";
		
		divObj.innerHTML = '<strong><font color="#ff0000">ERROR: '+resp+'</font></strong>';
		
		var transition_attributes = {
			opacity: {to: 1},
			duration: 1
		}
		
		var transition = new YAHOO.util.Anim(divObj, transition_attributes);
		transition.onComplete.subscribe(initFocus);
		transition.animate();
		
	}
	
	// Fade in the login dialog and calls initFocus() once the fading has completed
	showDialog = function(e) {
		var transition_attributes = {
			opacity: {to: 1},
			duration: 1
		}
		
		var transition = new YAHOO.util.Anim(document.getElementById('actDialog'), transition_attributes);
		transition.onComplete.subscribe(initFocus);
		transition.animate();
	}
	
	// Call showDialog() once the div has finished rendering w/ its opacity set to 0
	YAHOO.util.Event.onAvailable("actDialog", showDialog);

	
	
	
	function handleOK() {
		YAHOO.netmon.panels.wait.hide();
	}
	
	function showDaysRemaining(){
	YAHOO.namespace("netmon.panels");
			
			YAHOO.netmon.panels.wait = new YAHOO.widget.Dialog("wait",
			{
				width:       '300px',
				fixedcenter: true,
				modal:       true,
				close: true,
				buttons:    [ { text:"OK", handler:handleOK, isDefault:false } ],
				effect:      {effect:YAHOO.widget.ContainerEffect.FADE, duration:0.5}
				
			}
			);
	
			YAHOO.netmon.panels.wait.setHeader('Trial License Expired');
			YAHOO.netmon.panels.wait.setBody('<b>Your trial license for Netmon SE has expired.</b><br />We hope you have enjoyed using our software!<br />Visit <a href="http://store.netmon.ca">store.netmon.ca</a> to purchase a retail license key.');
			YAHOO.netmon.panels.wait.render(document.body);
			YAHOO.netmon.panels.wait.show();
	}
	{/literal}
	
	{if ((1 == $activation.is_trial) and (0 >= $activation.days_remaining))}
		showDaysRemaining();
	{/if}
	
	
</script>

<!-- end of {$smarty.template} -->
