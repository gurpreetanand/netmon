<!-- {$smarty.template} (Fri Apr 22 09:53:20 EDT 2005 2005 @ 09:53:20) -->

{if $errors}
<div class="panel" style="background-color: #FFFF80">
				{foreach from=$errors item="error"}
					{strip}
						<div>
						<img src="assets/icons/icon_error.gif" width="16" height="16" border="0" align="absmiddle" /> &nbsp;
						<strong>Error
						{if $error.code}
							&nbsp;{$error.code}
						{/if}</strong>
						:&nbsp;
						
						{if $error.message}
							{$error.message}
						{/if}
					
						{if $error.abstract}
							&nbsp;-&nbsp;{$error.abstract}
						{/if}
						</div>
					{/strip}
				{/foreach}
</div>
{/if}
<!-- end of {$smarty.template} -->

