<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- {$smarty.template} ($Id$) -->

<head>
	<title>Netmon Fatal Error</title>
	<link href="assets/css/_core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/_print.css" rel="stylesheet" type="text/css" media="print">
</head>

<body class="white">
<table width="400" border="0" align="center" cellpadding="15" cellspacing="0">
	<tr>
		<td colspan="2"><div align="center"><img src="assets/core/logo.gif"><br></div></td>
    </tr>
	<tr>
	  <td colspan="2"><div align="center"><span class="cell_red">&nbsp; CRITICAL ERROR&nbsp;</span></div></td>
  </tr>
	<tr>
	  <td colspan="2"><div align="center">
	    <p>The application has encountered a critical error while attempting to process your request. Please contact the server administrator at {mailto address=$smarty.const.ADMIN} and send a copy of the error messages below in order to resolve this issue.<br>
	      <br>
	      <strong>Error Details:
          </strong> </p>
	  </div></td>
  </tr>
</table>
<table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td style="border-top: 1px solid #808080; border-left: 1px solid #808080; border-right: 1px solid #808080;">
			{include file="error_handler.tpl" errors=$errors}
			
			<div class="panel" style="background-color: #FFFF80">
			<div><img src="assets/icons/icon_error.gif" width="16" height="16" border="0" align="absmiddle" /> 
			<strong>FATAL ERROR:</strong> {$message}</div>
			
			</div>
		</td>
	</tr>
</table>



</body>
<!-- end of {$smarty.template} -->
</html>
