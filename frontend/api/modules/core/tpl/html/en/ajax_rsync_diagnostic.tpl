<!-- {$smarty.template} ($Id$) -->

	<table width='100%' height='100%' align='center' border='0'>
		<tr>
			<td align='left' width='75px'><img src='/assets/images/{if FALSE === $status}down{else}up{/if}.png' width='48px' height='48px' /></td>
			<td align='left'>
			{if FALSE === $status}
				Unable to connect to contact the Netmon Automatic Update service.<br />
				<a href="http://wiki.netmon.ca/index.php/User_Guide:Administration_and_Management#Using_the_Netmon_Update_Service">Using the Netmon Update Service</a>
			{else}
				Netmon Update Service is accessible
			{/if}
			</td>
		</tr>
	</table>

<!-- End of {$smarty.template} -->
