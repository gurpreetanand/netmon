<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
{import_css files="_core" op="unshift"} 
{import_js files="_debugger,_core" op="unshift"}
<!-- {$smarty.template} ($Id$) -->

<head>
	<title>{$title} - {$smarty.const.SITE_NAME}</title>
	{get_imports type="js"}
	{get_imports type="css"}
	
	<link rel="SHORTCUT ICON" href="assets/icons/fav.ico">
	
	<!-- CSS Declarations -->
{foreach from=$css_files item="file"}
	<link type="text/css" rel="stylesheet" href="{$file}"  {if '/assets/css/_print.css' == $file}media="print"{/if} />
{/foreach}
	<!-- End of CSS Declarations -->
	
	
	<!-- Javascript Dependencies -->
{foreach from=$js_files item="file"}
	<script src="{$file}"></script>
{/foreach}
	<!-- End of Javascript Dependencies -->


	{literal}		
	<script language="Javascript">
	function showHelp(topicRef){
		document.getElementById("iframe_help").src = "?module=mod_help&action=get_help&root_tpl=blank_panel&section=" + topicRef;
		activatePanel(document.getElementById("Netmon_Help"));
	}
	</script>
	{/literal}
	
	{if (($title === "Authentication Required") || ($title === "Logout successful") || ($title === "Product Activation"))}
		{literal}
		<style>
	
		BODY {
			background-color: #666666;
			color: #282828;
			font-family: Tahoma, Arial, Helvetica, sans-serif;
			font-size: 11px;
		}
		
		TABLE.login {
			background-color: #D4D0C8;
			border: 1px solid #555555;
		}
		
		TD {
			font-family: Tahoma, Arial, Helvetica, sans-serif;
			font-size: 11px;
		}
	
				
		</style>
		{/literal}
	{/if}
{include file="debug_data.tpl"}
</head>
<body {if $smarty.get.class} class="{$smarty.get.class}"{/if}>
{include file="error_handler.tpl" errors=$errors}
{$content}
{if $smarty.const.__DEBUG__ == TRUE}
			<a href="javascript:loadDebugger('?module=core&action=display_ajax_debugger&root_tpl=blank&ignore_request=1')"><img class="debug" src="assets/icons/debug.gif" width="7" height="8" border="0" style="position: absolute; top: 5px; right: 5px;" title="Open Debug Window" /></a>
{/if}
</body>
<!-- end of {$smarty.template} ($Id$) -->
</html>
