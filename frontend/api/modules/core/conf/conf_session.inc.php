<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  * Session Handling
  */
  
/**
  * Name of the driver to use for session tracking
  * @name SESSION_DRIVER
  */
define("SESSION_DRIVER",                     "session_pgsql");
/**
  * Path for which the session tracking cookie is valid
  * @name SESSION_PATH
  */
define("SESSION_PATH",                       SUBDIR);
/**
  * Default Time To Live (TTL) for a session token
  * @name SESSION_DEFAULT_TTL
  */
define("SESSION_DEFAULT_TTL",                TIME_HOUR);
/**
  * Name of the session cookie
  * @name SESSION_NAME
  */
define("SESSION_NAME",                       "MADNET");
/**
  * Session storage data-structure versioning information
  * @name SESSION_VERSION
  */
define("SESSION_VERSION",                    20050609);

/**
  * Key to use for Quick Authentication from Session
  * @name APP_AUTHID
  */
define("APP_AUTHID",                         "MAD_AUTH_ID");

?>