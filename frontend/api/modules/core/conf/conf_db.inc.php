<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  * Establish Persistent connection to the database server
  * @name __PCONNECT__
  */
define("__PCONNECT__",                       TRUE);

/**
  * Database hostname
  * @name DB_HOST
  *
  */
define("DB_HOST",                            '127.0.0.1');
/**
  * Database driver
  * @name DB_TYPE
  */
define("DB_TYPE",                            'pgsql');
/**
  * Database account username
  * @name DB_USER
  */
define("DB_USER",                            'postgres');
/**
  * Database account password
  * @name DB_PASS
  */
define("DB_PASS",                            '');
/**
  * Database Name
  * @name DB_NAME
  */
define("DB_NAME",                            'netmon35');
/**
  * Connection protocol (unix or tcp)
  * @name DB_PROTOCOL
  */
define("DB_PROTOCOL",                        'tcp');

/**
 * Port to use while connecting to the DB through TCP/IP
 * @name DB_PORT
 */
define("DB_PORT",                            '5432');

/**
  * Base error code for DB related errors
  * @name DB_BASE_ERROR_CODE
  */
define("DB_BASE_ERROR_CODE", -100);

/**
  * Return value from the SQL class in case of a SQL Query execution error
  * @name DB_QUERY_ERROR
  */
define("DB_QUERY_ERROR",                          DB_BASE_ERROR_CODE-1);

/**
  * Return value from the SQL class while working on an empty resultset
  * @name DB_NO_RESULT
  */
define("DB_NO_RESULT",                      DB_BASE_ERROR_CODE-2);

/**
  * Return value from the SQL class in case of a DB connection error
  * @name DB_NO_RESULT
  */
define("DB_CONNECT_ERROR",                  DB_BASE_ERROR_CODE-3);
/**
  * Return value from the SQL class in case of a miscelaneous DB error
  * @name DB_MISC_ERROR
  */
define("DB_MISC_ERROR",                     DB_BASE_ERROR_CODE-4);


/**
  * DSN for the DB driver
  * @name DB_DSN
  */
#pgsql://user:pass@tcp(localhost:5433)/db_name
define("DB_DSN", DB_TYPE . "://" . DB_USER . ":" . DB_PASS . "@" . DB_PROTOCOL . "(".DB_HOST . ":". DB_PORT .")/" . DB_NAME);


?>
