<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * Our encryption key
  * @name CRYPT_KEY (as random as possible)
  */
define("CRYPT_KEY", '/.,097dz__p1v!sd q2codi][qw97*/xz YTDO &@*&!@WQ_DFCcsd@()*#^c 8y XSIOAUG%%%% _{(@yfp9w hg""');

/**
  * Our encryption cipher. Twofish is secure and performs well
  * @name CRYPT_CIPHER
  */
define("CRYPT_CIPHER", "twofish");

/**
  * Our encryption codebook
  * @name CRYPT_CODEBOOK
  */
define("CRYPT_CODEBOOK", "ecb");


?>