<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/* SNMP Module configuration (Do not change these)                    */

/**
  * Base path for the SNMP MIB Repository
  * @name MIB_PATH
  */
define("MIB_PATH",                         "/usr/local/share/mibs/site/");

/**
 * Should SNMP operations be cached? (Recommended)
 * @name USE_SNMP_CACHE
 */
define("USE_SNMP_CACHE", TRUE);

?>
