<!-- $Id$ -->

{import_js files="ext/adapter/ext/ext-base,ext/ext-all-debug,maintenance_scheduler"}
{import_css files="ext.ext-all,ext.xtheme-gray-extend"}

<div id="scheduler_div"></div>
{literal}
<script language="Javascript">
	Ext.onReady(function() {
		var scheduler = new maintenance_scheduler({
			renderTo: Ext.get('scheduler_div'),
			trigger_id: {/literal}'{$smarty.get.trigger_id}'{literal}
		});
		
		scheduler.render_scheduler();
	});
	
	
	
</script>
{/literal}
