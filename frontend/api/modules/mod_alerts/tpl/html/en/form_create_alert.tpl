<!-- {$smarty.template} ($Id$) -->

<script language="Javascript">
{literal}

var _commands_text = [];
var _commands_ids  = [];

add_command = function() {
	dd = document.getElementById("commands_dropdown");
	if (dd.length > 0) {
		opt = dd.options[dd.selectedIndex];
		_commands_text.push(opt.text);
		_commands_ids.push(opt.value);
		dd.options[dd.selectedIndex] = null;
	}
	
	document.getElementById('commands_div').innerHTML = list_commands();
	document.getElementById('sub_handler_command').value = _commands_ids.join(",");
}

remove_command = function(cmd_id) {
	dd = document.getElementById("commands_dropdown");
	for (i = 0; i < _commands_ids.length; i++) {
		if (_commands_ids[i] == cmd_id) {
			// 1- add it back into the dropdown
			dd.options[dd.length] = new Option(_commands_text[i], _commands_ids[i], true);
	
			// 2- remove it from the list
			_commands_text.splice(i, 1);
			_commands_ids.splice(i, 1);
			break;
		}
	}
	document.getElementById('commands_div').innerHTML = list_commands();
	document.getElementById('sub_handler_command').value = _commands_ids.join(",");
}

list_commands = function() {
	out = '';
	for (i = 0; i < _commands_ids.length; i++) {
		out += "<p><span class='blue2'>" + _commands_text[i] + "</span><a href='javascript: remove_command(" + _commands_ids[i] + ");'><img border='0' src='/assets/icons/bullet_delete.png' title='Remove' class='icon' /></a></p>"
	}
	if(_commands_ids.length < 1){
	 out = "<p><span class=\"gray2\">None</span></p>";
	}
	return out;
}


{/literal}
</script>


{if $alerts}
<div class="datagrid center">
<table width="100%" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<th>Label</th>
		<th>Recipient</th>
		<th>Media</th>
		<th>Actions</th>
	</tr>
{foreach from=$alerts item="alert"}
	<tr>
		<td><strong>{$alert.label}</strong></td>
		<td>{$alert.name} {if $alert.threshold > 0}(threshold:{if $alert.comp_exp}{$alert.comp_exp}{/if} {$alert.threshold}{$threshold_unit}){/if}</td>
		<td>{$alert.media|capitalize}</td>
		<td><a href="?module=mod_alerts&action=form_edit_alert&trigger_id={$alert.trigger_id}">Edit</a> | <a href="?module=mod_alerts&action=delete_alert&handler_id={$alert.id}">Del</a></td>
	</tr>
{/foreach}
</table>
</div>
{/if}
{capture assign="params"}{foreach from=$alert_params item="param" key="key"}&{$key}={$param}{/foreach}{/capture}
<form action="?module=mod_alerts&action=process_create_alert&alert_type={$type}{$params}" method="POST">
	<input id="sub_handler_command" type="hidden" name="sub_handler_command" value="" />
	{foreach from=$alert_params item="param" key="key"}
	<input type="hidden" name="{$key}" value="{$param}" />
	{/foreach}
	<input type="hidden" name="required_retries" value="10" />
	<input type="hidden" name="trigger_timeout" value="5" />

<div class="panel">

			<fieldset>
			<legend>&nbsp;<img src="/assets/icons/alert.png" width="16" height="16" class="icon">&nbsp; <strong>Alert Message</strong>&nbsp;</legend>

	        <table width="100%" border="0" cellspacing="5" cellpadding="0">
		<tr>
			<td>Label</td>
			<td><input name="label" value="{$smarty.post.label}" /></td>
		</tr>
		<tr>
			<td>Recipient</td>
			<td>{html_options name="user_id" options=$users selected=$smarty.post.user_id|default:$smarty.session.id}</td>
		</tr>

		<tr>
			<td>Media</td>
			<td>{html_options name="media_id" options=$medias selected=$smarty.post.media_id}</td>
		</tr>
		{if $component}
			{include file="form_component_"|cat:$component|cat:".tpl"}
		{/if}
		
	</table>
	{if sizeof($commands) > 0}
	</fieldset>
				
			<fieldset>
			<legend>&nbsp;<img src="/assets/icons/alert_command.png" width="16" height="16" class="icon">&nbsp; <strong>Alert Commands</strong>&nbsp;</legend>
			
			
			  
			  <p>		    	    Command: 
			    <select id="commands_dropdown" title="Mouse-over an entry to see the command" name="alert_command_id">
	    	    
			    		{foreach from=$commands item="command"}
			    		<option title="{$command.command|escape:html}" value="{$command.id}">{$command.label}</option>
			    		{/foreach}
			    	
		    	    </select>
		    	    <a href="javascript:add_command();"><img src="/assets/buttons/add_mini.gif" width="50" height="20" border="0" class="icon" /></a>
	          </p>
			  <div align="center">
			  <hr />
			  <p>The following command(s) will run when the alert is triggered:</p>
			  <div id="commands_div" align="center"><p><span class="gray2">None</span></p></div>
			</div>
			</fieldset>
			
	{/if}
			
			<div align="center">{input type="submit" class="button" value="Add New Alert"}</div>

</div>



</form>
{literal}
<script>
// this function is here because some parent frames call it (i.e. recently discovered hosts panel)
function SetAllCheckBoxes(arg1, agr2, arg3){};
</script>
{/literal}
<!-- end of {$smarty.template} -->
