<!-- {$smarty.template} ($Id$) -->

<tr>
	<td>Max Utilization</td>
	<td><input type="text" size="3" name="trigger_threshold" {if $cam}value="{$cam->get('trigger_threshold')}"{/if}>%</td>
</tr>
<tr>
	<td>Direction</td>
	<td>
	{capture assign="pattern_val"}{if $cam}{$cam->get('pattern')}{else}{$smarty.post.pattern}{/if}{/capture}
	{html_options options=$snmp_directions name="pattern" selected=$pattern_val}
	</td>
</tr>

<!-- end of {$smarty.template} -->
