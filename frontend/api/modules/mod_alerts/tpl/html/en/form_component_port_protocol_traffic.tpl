<!-- {$smarty.template} ($Id$) -->
<tr>
	<td colspan="2">
	{capture assign="port"}{if $pm}{$pm->get('port')}{else}{$smarty.get.port|default:$smarty.post.port}{/if}{/capture}
	{capture assign="transport"}{if $pm}{$pm->get('protocol')}{else}{$smarty.get.transport|default:$smarty.post.transport}{/if}{/capture}
	{"Alerting when traffic on port <strong>$port/$transport</strong> is detected"|message_bar}
	<input type="hidden" name="port" value="{$port}" />
	<input type="hidden" name="transport" value="{$transport}" />
	<input type="hidden" name="trigger_threshold" value="-1">
	</td>
</tr>
<!-- end of {$smarty.template} -->
