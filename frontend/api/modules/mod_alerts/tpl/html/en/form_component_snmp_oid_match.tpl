
<!-- {$smarty.template} ($Id$) -->

{if $owm->get('category') == "numeric"}
<tr>
	<td colspan="2">
	
	<fieldset>
	<legend><strong>&nbsp;<img src="/assets/icons/alert.png" width="16" height="16" class="icon">&nbsp; Alert Parameters&nbsp;</strong></legend>
	
	<div align="center">
	  <p>Send an alert message when the value of this OID is <br>
	  {capture assign="comp"}{if $cam}{$cam->get('comp_exp')}{else}{$smarty.post.comp_exp}{/if}{/capture}
	    {html_options name="comp_exp" options=$comp_exps selected=$comp}<br />
	    <br />
      the following value:</p>
	    {capture assign="threshold"}{if $cam}{$cam->get('trigger_threshold')}{else}{$smarty.post.trigger_threshold}{/if}{/capture}
	  <p>{input name="trigger_threshold" value=$threshold size="15"}<br />
	    <br />
	
	    </p>
	</div>
	</fieldset>
	
	</td>
</tr>

{else}
<tr>
	<td colspan="2">
	
	<fieldset>
	<legend><strong>&nbsp;<img src="/assets/icons/alert.png" width="16" height="16" class="icon">&nbsp; Alert Parameters&nbsp;</strong></legend>
	
	<div align="center">
	  <p>Send an alert message when the value of this OID has changed from the following keyword/pattern:</p>
	  {capture assign="pattern"}{if $cam}{$cam->get('pattern')}{else}{$smarty.post.pattern}{/if}{/capture}
	  <p><input type="hidden" name="trigger_threshold" value="-1" /><input type="text" {param name="pattern"} value=$pattern><br />
	    <br />
	
	    </p>
	</div>
	</fieldset>
	
	</td>

</tr>
{/if}
<!-- end of {$smarty.template} -->
