<!-- {$smarty.template} ($Id$) -->
<tr>
	<td>Severity</td>
	<td>{html_options name="trigger_threshold" options=$severities selected=$sm->get('severity')|default:$smarty.post.trigger_threshold}</td>
</tr>
<tr>
	<td>Pattern</td>
	{capture assign="pattern"}{if $cam}{$cam->get('pattern')}{else}{$smarty.post.pattern}{/if}{/capture}
	<td><input type="text" {param name="pattern"} value="{$pattern}" /></td>
</tr>
<!-- end of {$smarty.template} -->
