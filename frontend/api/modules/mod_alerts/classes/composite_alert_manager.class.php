<?php


class Composite_Alert_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "none";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_alerts";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;
	
	/**
	 * Alert Handler model
	 * @var Alert_Handler
	 */
	var $ah;
	
	/**
	 * Alert Trigger model
	 * @var Alert_Trigger
	 */
	var $at;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		require_class($this->module, "alert_trigger");
		require_class($this->module, "alert_handler");

		$this->at = new Alert_Trigger();
		$this->ah  = new Alert_Handler();

		$this->params->primitives = array_merge($this->at->params->primitives, $this->ah->params->primitives);

	}
	
	function fetch_by_trigger_id($trigger_id) {
		if ($this->at->fetch($trigger_id) && $this->ah->fetch_by_trigger_id($trigger_id)) {
			$this->params->primitives = array_merge($this->at->params->primitives, $this->ah->params->primitives);
			return TRUE;
		} else {
			return FALSE;
		}
	}


}
?>