<?php

class Maintenance_Manager extends MadnetElement
{
    /**
      * Database table associated with this subclass
      *
      * @var $table
      * @access protected
      */
    var $table = "alert_maintenance";
    /**
      * Name of the primary key in the table
      *
      * @var string $pkey
      * @access protected
      */
    var $pkey = "id";
    /**
      * Name of the module this MadnetElement subclass belongs to
      *
      * @var string $module
      * @access protected
      */
    var $module = "mod_alerts";
    /**
      * Name of the class containing the domain logic for this Element
      *
      * @var string $element
      * @access protected
      */
    var $element = __CLASS__;

    function init() {
    	$this->params->add_primitive("occurrences", "integer",  FALSE,  "Number of Occurrences");
		$this->params->add_primitive("schedule_start_date", "string", FALSE, "Start Date");
		$this->params->add_primitive("schedule_stop_date", "string", FALSE, "End Date");
		$this->params->add_primitive("schedule_hour", "integer", TRUE, "Scheduled Hour");
		$this->params->add_primitive("schedule_month", "integer", FALSE, "Scheduled Month");
		$this->params->add_primitive("schedule_day", "integer", FALSE, "Scheduled Day");
		$this->params->add_primitive("notify_users", "string", FALSE, "List of users to notify upon completion");
		$this->params->add_primitive("recurrence_unit", "string", TRUE, "Frequency Unit");
		$this->params->add_primitive("schedule_week", "string", FALSE, "Day of Week");
		$this->params->add_primitive("event_type", "string", TRUE, "Event Type");
		
        $this->params->add_primitive("maintenance_duration", "integer",  TRUE,  "Maintenance Duration");
        $this->params->add_primitive("trigger_id",   "integer",  TRUE,  "Alert Trigger ID");
        $this->params->add_primitive("event_id",      "integer", FALSE, "Event ID");
    }
    
    /*
    function post_fetch($id) {
        if ($this->get("event_id") != NULL) {
            require_class("core", "Event_Manager");
            $em = new Event_Manager();
            if ($em->fetch($this->get("event_id"))) {
                $this->params->setval("event", $em->params->build_autoinsert_array());
            }
        }
        
        return TRUE;
    }
    
    function post_delete($id) {
        if ($this->get('event_id') != NULL) {
            require_class("core", "Event_Manager");
            $em = new Event_Manager();
            return $em->delete($this->get('event_id'));
        }
        return TRUE;
    }
    
    
    
    function getAll() {
        $query = "SELECT event_id, trigger_id, maintenance_duration, id FROM ".$this->table." ORDER BY id ASC";
        $res = $this->db->select($query);
        
        if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
            return FALSE;
        }
        return $res;
    }
    */

}

?>
