<?php


class Alert_Handler extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "alert_handlers";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_alerts";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		$this->params->add_primitive("media_id",           "integer",       TRUE,   "Media",         "Media");
		$this->params->add_primitive("conditional_id",     "integer",       FALSE,  "Conditional",   "Conditional");
		$this->params->add_primitive("user_id",            "integer",       TRUE,   "User",          "User");
		$this->params->add_primitive("required_retries",   "integer",       TRUE,   "Retries",       "Retries");
		$this->params->add_primitive("trigger_id",         "integer",       TRUE,   "Trigger (internal)",       "Trigger (internal)");
		$this->params->add_sub($this->module, "handler_command", FALSE, "post");
	}



	/**
	  * Returns an array containing the user ID of every user account in the DB
	  *
	  * @return mixed
	  */
	function get_all_ids() {
		$query = "SELECT {$this->pkey}, ip, threshold, status FROM {$this->table}";
		$result = $this->db->select($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			return $result;
		}
	}

	function pre_insert($id = NULL) {
		return TRUE;
	}


	function pre_update($id) {
		return $this->pre_insert($id);
	}

	function pop($id) {
		$id = $this->db->escape($id);

		$query = "SELECT * FROM {$this->table} WHERE {$this->pkey} = $id";

		$result = $this->db->get_row($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			foreach($result as $key => $value) {
				$this->params->setval($key, $value);
			}
			return TRUE;
		}
	}

	function delete($id) {
		$query = "SELECT trigger_id FROM alert_handlers WHERE id = " . $this->db->escape($id);

		$res = $this->db->get_row($query);

		if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
			return FALSE;
		}

		$query = "DELETE FROM alert_handlers WHERE id = " . $this->db->escape($id);
		if (!$this->db->delete($query)) {
			return FALSE;
		}

		$query = "DELETE FROM alert_triggers WHERE trigger_id = " . $this->db->escape($res['trigger_id']);
		return $this->db->delete($query);
	}
	
	
	/**
	 * Performs the same type of mapping as MadnetElement::fetch() but looks up the record using trigger_id
	 * @param $trigger_id
	 * @return bool
	 */
	function fetch_by_trigger_id($trigger_id) {

		$query = "SELECT * FROM alert_handlers WHERE trigger_id = " . $this->db->escape($trigger_id);
		
		$res = $this->db->get_row($query);
		
		if ((DB_NO_RESULTS == $res) || (DB_QUERY_ERROR == $res)) {
			return FALSE;
		}
		
		foreach($res as $key => $val) {
			$this->params->setval($key, $val);
		}
		
		return TRUE;
		
		
	}

}
?>