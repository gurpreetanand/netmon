<?php


class Alert_Command_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "alert_commands";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "id";
	/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_alerts";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		$this->params->add_primitive("alert_type_id",       "integer", TRUE,  "Alert Type");
		$this->params->add_primitive("label",               "string",  TRUE,  "Label");
		$this->params->add_primitive("command",             "string",  TRUE,  "Command");
		$this->params->add_primitive("async",               "pg_bool", TRUE,  "Process Asynchronously");
		$this->params->add_primitive("timeout",             "integer", FALSE, "Command timeout");
		$this->params->add_primitive("perform_on_failure",  "pg_bool", FALSE,  "Execute on failure condition");
		$this->params->add_primitive("perform_on_recovery", "pg_bool", FALSE,  "Execute when the alert condition recovers");
	}

	function get_commands_by_alert_type($alert_type_id) {
		$id = $this->db->escape($alert_type_id);
		$query = "SELECT * FROM {$this->table} WHERE alert_type_id = $id ORDER BY id DESC";
		$res = $this->db->select($query);

		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			return;
		}

		return $res;

	}

}
?>