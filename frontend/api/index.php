<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @package    MADNET
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */



/**
  * @author Xavier Spriet
  * Loads core configuration file
  */
require_once("config.inc.php");



/* Initiates the stack tracing process if specified in config file */
if (defined("__STACK_TRACE__") && __STACK_TRACE__ == TRUE) {
  if (!extension_loaded('xdebug')) {
    if (dl('xdebug.so')) {
      /**
        * Initialize the XDebug profiler
        */
      xdebug_enable();

      xdebug_start_trace("/apache/debug/trace/madnet.trace", 1);
    }
  }
}

/**
  * @author Xavier Spriet
  * Load the Controler Facade
  */
require_once(ROOT . "/modules/core/classes/controler_facade.class.php");

/**
  * @package    MADNET
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    4.0
  * Main Request Handler  This is the facade for the View from the user perspective.
  */
class Index
{

  /*** Attributes: ***/

  /**
   * Final Output buffer
   * @access public
   * @var    string $output
   */
  var $output;


  /**
   * Page title
   * @access public
   * @var    string $title
   */
  var $title;


  /**
   * Aggregated renderer object.
   * @access private
   * @var    object $renderer
   */
  var $renderer;


  /**
   * Aggregated controler facade object.
   * @access private
   * @var    object $controler
   */
  var $controler;


  /**
   * Constructor and main request handler
   *
   * @return Index
   * @access public
   */
  function Index( )
  {
    /**
      * Initialization of aggregate objects
      *
      */
    $this->controler = new Controler_Facade;

    $registry = Registry::get_registry();



    /**
      * Initialization of session and config information
      *
      */
    $this->env_init();

    /**
      * Main Query Processing
      * The controler will return the TITLE while the module will access $index_content.
      */
    $this->title = $this->controler->process_query(&$this->output);
    $this->renderer = &$registry->get_singleton("core", "renderer");
    /**
      * Final output
      *
      */
    $this->display_output();
  } // end of member function Index



  /**
   * Initializes the Operating Environment  Through the aggregated controler facade
   * object, this method initializes the environment required to complete the request
   * cycle.
   *
   * It first dumps a set of HTTP headers to force all browsers to drop cache
   *
   * @return bool
   * @access private
   */
  function env_init( )
  {
    ############     PHP.NET HACK    ############
    // Date in the past                         //
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

    // always modified
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

    // HTTP/1.1
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Access-Control-Allow-Origin: *");


    // HTTP/1.0
    header("Pragma: no-cache");
    //                                         //
    ############ END OF PHP.NET HACK ############

    ini_set('memory_limit','512M');


  } // end of member function env_init


  /**
   * Displays the final output
   *
   * @return bool
   * @access private
   */
  function display_output()
  {
    $root_tpl = $_GET['root_tpl'] ? $_GET['root_tpl'] . ".tpl" : 'index.tpl';
    $this->renderer->parse_template($root_tpl, $this->title, $this->output);
  } // end of member function display_output



} // end of Index

/* Instanciate the index object */
$index = new Index;

/* Stop the stack tracing process if necessary */
if (defined("__STACK_TRACE__") && __STACK_TRACE__ == TRUE) {
  if (extension_loaded('xdebug')) {
    @xdebug_stop_trace();
  }
}


?>
