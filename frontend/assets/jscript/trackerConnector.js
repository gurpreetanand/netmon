/*
 * Copyright (c) 2008, Netmon Inc. All Rights Reserved Author: $Author$ ($Id$)
 */

 
 draw2d.trackerConnector = function(uiFigure) {
 	
 	this.ajax_fail = function(resp, options) {
		console.log("AJAX request failed");
	}
	
	this.ajax_confirm_connection_saved = function(resp, options) {
		if ("OK" == resp.responseText) {
			console.log("New coordinates saved successfully");
		} else {
			console.log("Unable to save new coordinates:");
			console.log(resp.responseText);
		}
	}
 	
 	draw2d.Port.call(this, uiFigure);
 	
 	
 	return this;
 }
 
 draw2d.trackerConnector.prototype = new draw2d.Port;
 
 draw2d.trackerConnector.prototype.onDrop = function(port) {
 	// From the MyOutputPort.js example file:
	if (this.parentNode.id == port.parentNode.id) {
		// same parentNode -> do nothing
	} else {
		var command = new draw2d.CommandConnect(this.parentNode.workflow, this, port);
		route = new draw2d.Connection();
		route.setRouter(new draw2d.BezierConnectionRouter());
		command.setConnection(route);
		this.parentNode.workflow.getCommandStack().execute(command);
		console.log("New connection is up");
		
		console.log(command);
		
		Ext.Ajax.request({
			url: '/?module=mod_network&action=ajax_save_node_connection&root_tpl=blank',
			success: this.ajax_confirm_connection_saved,
			failure: this.ajax_fail,
			params: {parent: command.source.parentNode.node['ip'], child:command.target.parentNode.node['ip']}
		});
		
		
		console.log(this.parentNode);
	}
 }