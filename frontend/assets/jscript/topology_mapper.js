/*
Copyright (c) 2008, Netmon Inc. All Rights Reserved
Author: $Author$
($Id$)
*/

Topology_Mapper = function() {
	Ext.QuickTips.init();
	
	this.top_obj = [];
	this.ungrouped = [];
	this.ungrouped_container = null;
	this.max_x_offset = 100;
	this.node_manager = new Node_Manager();
	
	/**
	 * Ensure loading indicator is visible and update its status text
	 */
	this.status = function(str) {
		console.log("GUI Update: " + str)
		Ext.get('loading').show();
		document.getElementById('status').innerHTML = str;
	};
	
	/**
	 * Initialization method
	 */
	this.init = function() {
		
		this.status("Initializing...")
		
		/* Set AJAX request timeout to 3min */
		Ext.Ajax.timeout = 320000;
	};
	
	/**
	 * Maximizes the window
	 */
	this.maximize = function(ev, tool, panel) {
		Ext.getCmp('main_window').maximize();
	};
	
	/**
	 * Toggle window maximization
	 */
	this.unmaximize = function(ev, tool, panel) {
		Ext.getCmp('main_window').toggleMaximize();
	};
	
	/**
	 * Reload the full JSON payload
	 */
	this.refresh = function(resp, options) {
		/*
		console.log("Refreshing...");
		for (var i = 0; i < this.ungrouped.length; i++) {
			this.map.removeFigure(this.ungrouped[i]);
		}
		
		for (var i = 0; i < this.top_obj.length; i++) {
			this.map.removeFigure(this.top_obj[i]);
		}
		*/
		
		window.location.reload();
	};
	
	
	
	
	this.map = new draw2d.Workflow('workflow');
	
	this.map.html.style.backgroundImage="url(/assets/jscript/draw2d/grid_10.png)";
	this.map.setSnapToGrid();
	this.map.setEnableSmoothFigureHandling(false);
	
	
	

	
	

	
	/**
	 * Container for the panels
	 */
	this.container = new Ext.Panel({
		id: 'main_container',
		region: 'center',
		autoScroll: true,
		tbar: [{
			id: 'refresh',
			text: 'Refresh',
			scope: this,
			handler: this.refresh
		}],
		contentEl: Ext.get('map')
	});
	
	// this.container.add(Ext.get('map'));
	
	
	/**
	 * Main UI window + decorations
	 */
	this.main_window = new Ext.Window({
		id: 'main_window',
		title: 'Network Topology Mapper',
		closable: false,
		width: '95%',
		height:450,
		modal: true,
		layout: 'border',
		items: [this.container],
		tools: [{
			id: 'refresh',
			handler: this.refresh,
			scope: this,
			qtip: 'Refresh the Topology Map'
		}, {
			id: 'maximize',
			scope: this,
			handler: this.maximize,
			qtip: 'Maximize the Topology Window'
		}, {
			id: 'restore',
			scope: this,
			handler: this.unmaximize,
			qtip: 'Reduce Window Size'
		}]
	});
	
	
	
	
	/**
	 * Load the UI Components and maximize our window
	 */
	this.load_ui = function() {
		this.status("Building User Interface");
		this.main_window.show(this);
		this.maximize();
		this.map.setViewPort(document.getElementById('map').parentNode.id);
	};
	
	
	
	/**
	 * JSON Extractor
	 */
	this.extract_json_topology = function(resp, options) {
		this.status('Processing topology data');
		
		
		// Extract the JSON data
		obj = eval('(' + resp.responseText + ')');
		//console.log(obj);
		
		
		// Initialize a few settings vars (height/width, start offsets, etc...)
		col_width = row_height = 75;
		row_max_nodes = 5;
		
		offsets = {'x': this.max_x_offset, 'y': 50};
		
		// Build the grouped and ungrouped nodes data-structures
		for (var i in obj) {
			if (obj[i]['nodes']) {
				//node = new draw2d.NetmonNode(obj[i]['type'], obj[i]['ip'], obj[i]['trackers']);
				node = new draw2d.NetmonNode(obj[i], this.node_manager);
				node['nodes'] = []
				
				for (var n in obj[i]['nodes']) {
					//console.log(obj[i]['nodes'][n]);
					subNode = new draw2d.NetmonNode(obj[i]['nodes'][n], this.node_manager);
					node['nodes'].push(subNode);
				}
				
				
				this.top_obj.push(node);
			} else {
				this.ungrouped.push(new draw2d.NetmonNode(obj[i], this.node_manager));
			}
		}
		
		// Draw the grouped nodes
		for (var i = 0; i < this.top_obj.length; i++) {
			offsets['y'] = 50;
			offsets = this.render_node(this.top_obj[i], offsets);
			offsets['x'] += 100;
			this.max_x_offset = Math.max(this.max_x_offset, offsets['x']);
		}
		
		// Set start position for ungrouped nodes
		offsets['y'] = -40;
		offsets['x'] = this.max_x_offset + 160;
		x_base = offsets['x'];
		
		// Create ungrouped container
		this.ungrouped_container = new draw2d.Rectangle(120*row_max_nodes, 120*(Math.ceil(this.ungrouped.length/row_max_nodes))+60);
		this.ungrouped_container.setCanDrag(false);
		this.ungrouped_container. setResizeable(false);
		this.ungrouped_container.setSelectable(false);
		this.map.addFigure(this.ungrouped_container, offsets['x']-50, 20);
		
		
		var titlebar = new draw2d.Label("Ungrouped Devices");
		titlebar.setDimension(this.ungrouped_container.getWidth()-2,15);
		titlebar.setResizeable(false);
		titlebar.setSelectable(false);
		//titlebar.style.background = '#eeeeee';
		this.map.addFigure(titlebar,offsets['x']-49, 21);
		
		// Render the ungrouped nodes data-structure on the canvas
		//console.log("Processing ungrouped entries");
		for (var j = 0; j < this.ungrouped.length; j++) {
			if ((j % row_max_nodes) == 0) {
				offsets['y'] += 120;
				offsets['x'] = x_base;
			} else {
				offsets['x'] += 100;
			}
			offsets = this.render_node(this.ungrouped[j], offsets);
		}
		
		/* Remove the loading indicator */
		Ext.get('loading').hide();
		this.node_manager.init();
	};
	
	
	
	this.render_node = function(node, offsets, parent) {
		row_max_nodes = 5;
		//console.log("Rendering new icon: " + node['node']['ip']);
		//console.log(node);
		//console.log(offsets);
		
		root_offsets = {'x': offsets['x'], 'y': offsets['y']}
		
		if (node['nodes']) {
			if (node['nodes'].length >= row_max_nodes) {
				root_offsets['x'] = (root_offsets['x'] + ((row_max_nodes * 120)) / 2);
			} else {
				root_offsets['x'] = (root_offsets['x'] + ((node['nodes'].length * 120)) / 2);
			}
			
			
			if (node['node']['coords']) {
				//console.log(node['node']['coords']);
				this.map.addFigure(node, node['node']['coords'][0]['node_x'], node['node']['coords'][0]['node_y']);
			} else {
				this.map.addFigure(node, root_offsets['x'], root_offsets['y']);
			}
			
			
			//console.log("Processing sub-tree nodes");
			x_base = offsets['x'];
			for (var i = 0; i < node['nodes'].length; i++) {
				if ((i % row_max_nodes) == 0) {
					offsets['y'] += 120;
					offsets['x'] = x_base;
				} else {
					offsets['x'] += 100;
				}
				offsets = this.render_node(node['nodes'][i], offsets, node);
				this.max_x_offset = Math.max(this.max_x_offset, offsets['x']);
			}
		} else {
			if (node['node']['coords']) {
				this.map.addFigure(node, node['node']['coords'][0]['node_x'], node['node']['coords'][0]['node_y']);
			} else {
				this.map.addFigure(node, offsets['x'], offsets['y']);
			}
		}
		
		if (null != parent) {
			var connector = new draw2d.Connection();
			connector.setRouter(new draw2d.BezierConnectionRouter());
			connector.setSource(parent.outputPort2);
			connector.setTarget(node.outputPort1);
			this.map.addFigure(connector);
		}
		
		//offsets['x'] = this.max_x_offset;
		
		
		
		return offsets;
	};
	
	/**
	 * AJAX request failure handler
	 */
	this.ajax_fail = function(resp, options) {
		this.status('Process aborted.');
		/* Remove the loading indicator */
		Ext.get('loading').hide();
	};
	
	/**
	 * Sends the AJAX query to retrieve topology data in JSON format
	 */
	this.ajax_load_topology = function() {
		this.status("Retrieving Topology Data...");
		Ext.Ajax.request({
			url: '/?module=mod_network&action=ajax_load_json_topology&root_tpl=blank',
			success: this.extract_json_topology,
			failure: this.ajax_fail,
			scope: this
		});
	};
	
	this.ajax_refresh = function() {
		this.status("Refreshing Topology Data...");
		Ext.Ajax.request({
			url: '/?module=mod_network&action=ajax_load_json_topology&root_tpl=blank',
			success: this.refresh,
			failure: this.ajax_fail,
			scope: this
		});
	};
	
	
}
