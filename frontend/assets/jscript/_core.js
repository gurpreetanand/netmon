// Keeps track of locked input fields to make CSS play nicely
var fields = new Array();

// Function: Create Cookie
function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}

// Function: Read Cookie
function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

// Activates a collapsible UI panel
function activatePanel(panel){
	// First ensure the panel isn't already open
	// The first line below is a way to check for the existence of an object without raising errors
	if(window.activePanel){
		if(activePanel.id != panel.id){		
			// Now we close any previously opened panels, if any
			closePanel(activePanel);
			// Open up the new panel
			openPanel(panel);
		}
	} else {
		// Open up the new panel
		openPanel(panel);
	}
}

// Retrieves browser codes for all supported browsers
// No, links isn't technically supported, it mostly works though.
function getBrowser() {
	var ua = navigator.userAgent.toLowerCase();
	if      (ua.indexOf('opera') > -1)  { return 'opera';     }
	else if (ua.indexOf('safari') > -1) { return 'safari';    }
	else if (window.ActiveXObject)      { return 'ie';        }
	else if (ua.indexOf('khtml') > -1)  { return 'konqueror'; }
	else if (ua.indexOf('gecko') != -1) { return 'gecko';     }
	else if (ua.indexOf('links') != -1) { return 'links';     }
	return false;
}

function openPanel(panel){
	//mozilla & ie hack. IE ignores whitespace while Mozilla considers it a node
	//alert(YAHOO.widget.Calendar_Core._getBrowser());
	//alert(navigator.userAgent.toLowerCase());
	
	
	if (getBrowser() == "ie") {
		panel.nextSibling.className = "region, open";		
	} else {
		panel.nextSibling.nextSibling.className = "region, open";
	}
	
	
	panel.className = "titlebar_expanded";
	createCookie(document.title + "_activePanel", panel.id, 365);
	activePanel = panel;
}

function closePanel(panel){
	//mozilla & ie hack. IE ignores whitespace while Mozilla considers it a node
	
	if (getBrowser() == "ie") {
		panel.nextSibling.className = "region, closed";
	} else {
		panel.nextSibling.nextSibling.className = "region, closed";
	}
	panel.className = "titlebar_collapsed";
}

// Sets the currently active panel (from a set of collapsible ones) after page load
function setInitPanel(){
	//check for the cookie value, if there is one.
	if(readCookie(document.title + "_activePanel") != null){
		//check to make sure that panel specified in the cookie still exists
		pnl = readCookie(document.title + "_activePanel");
		if(pnl){
			activePanel = document.getElementById(readCookie(document.title + "_activePanel"));
			openPanel(activePanel);
		}	
	} else {
		// As a default, we open the Help panel
		activePanel = document.getElementById("Netmon_Help");
		openPanel(activePanel);
	}
}

function toggleSiblingDIV(iconRef){
	divRef = iconRef.parentNode.parentNode;
	
	//alert(divRef.nodeName);
	if(divRef.nextSibling.className == "open"){
		divRef.nextSibling.className = "closed";
		iconRef.src = "assets/icons/plus.gif";
	} else {
		divRef.nextSibling.className = "open";
		iconRef.src = "assets/icons/minus.gif";
	}
}

// This function prints the contents of an inline frame. Takes the ID attribute of a frame as input.
function printFrame(frameRef){
	eval(frameRef).print();
}

// Return true if the second parameter is part of the array in parameter 1
function in_array(array, val) {
        var i;
        for (i = 0; i < array.length; i++) {
                if (array[i] == val) {
                        return true;
                }
        }
        return false;
}

// Deletes the array element in argument 1 that has the value passed in argument 2
function array_del(array, val) {
        var i;
        for (i = 0; i < array.length; i++) {
                if (array[i] == val) {
                        array[i] = '';
                }
        }
}

// Pushes the value of argument 2 in array in argument 1
function array_push(array, val) {
	array.push(val);
    //array[array.length] = val;
}

// Returns the human-readable representation of an array's content
function dump(array) {
        var temp = '';
        var i;

        for (i = 0; i < array.length; i++) {
                temp += i + " => " + array[i] + "\n";
        }

        return temp;
}

// Returns the last element of the array that has a value, then deletes it.
function array_pop(array) {
        var i;
        for (i = array.length; i > 0; i--) {
                if (array[i] != '') {
                        var temp = array[i];
                        array[i] = '';
                        return temp;
                }
        }
}

// Returns True if a field is in locked state, false otherwise
function is_locked(field) {
        return in_array(fields, field);
}

// Sets a field in lock state.
// This prevents onMouseOut to override onFocus and onBlur
function lock_field(field) {
        array_push(fields, field);
}

// Unlocks a field (use this on onBlur)
function unlock_field(field) {
        array_del(fields, field);
}

// Checks if the email address passed as argument is valid or not
function isValidEmail(str) {
        var email=/^[A-Za-z0-9]+([_\.-][A-Za-z0-9]+)*@[A-Za-z0-9]+([_\.-][A-Za-z0-9]+)*\.([A-Za-z]){2,4}$/i;
        return(email.test(str));
}

//This function refreshes data in a Flash chart
// Pass in a named reference to the Flash object, and the new XML to render
function setFCNewData(objFlash, strXML) {
  //This function updates the data of a FusionCharts present on the page
  //Get a reference to the movie 
  var FCObject = getObject(objFlash);
  //Set the data
  //Set dataURL to null
  FCObject.SetVariable('_root.dataURL',"");
  //Set the flag
  FCObject.SetVariable('_root.isNewData',"1");
  //Set the actual data
  FCObject.SetVariable('_root.newData',strXML);
  //Go to the required frame
  FCObject.TGotoLabel('/', 'JavaScriptHandler'); 
}
   
// Cross-browser function to retrieve object references.
function getObject(objectName) {
      if (navigator.appName.indexOf ("Microsoft") !=-1) {
         return window[objectName]
      } else {
         return document[objectName]
      }
}

// Shows an existing progress indicator image
function showProgressIndicator(img_id, targetDocument) {
	if (targetDocument == null)
		targetDocument = document;
	
	if ((targetDocument.getElementById(img_id).style.display == 'none') || (targetDocument.getElementById(img_id).style.display == '')) {
		targetDocument.getElementById(img_id).style.display = 'inline';
	}
	
	targetDocument.getElementById(img_id).style.visibility = 'visible';
}

// Hides a visible progress indicator element
function hideProgressIndicator(img_id, targetDocument) {
	if (targetDocument == null)
		targetDocument = document;
	targetDocument.getElementById(img_id).style.visibility = 'hidden';
}


// Get rid of JS errors in IE while using firebug.
if (!window.console || !console.firebug)
{
    var names = ["log", "debug", "info", "warn", "error", "assert", "dir", "dirxml",
    "group", "groupEnd", "time", "timeEnd", "count", "trace", "profile", "profileEnd"];

    window.console = {};
    for (var i = 0; i < names.length; ++i)
        window.console[names[i]] = function() {}
}