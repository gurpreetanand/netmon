/*
Copyright (c) 2006, Netmon Inc. All Rights Reserved
Author: $Author$
($Id$)
*/


// Constructor
AJAX_OID = function(device_id) {
	this.device_id = device_id;
};


// Object Prototype
AJAX_OID.prototype = {
	
	// Device identifier
	device_id: null,
	
	// OID stack declaration
	_oids: [],

	
	// Initiate the lookup for all OIDs registered in the stack.
	init: function() {
		for (var i = 0; i < this._oids.length; i++) {
			this.add_tracker(i);
			this.update_oid(i);
		}
	},
	
	// Add an OID to the stack
	add_oid: function(oid, frequency, parent_div, callback) {
		new_size = this._oids.push({"type": "oid", 
						 "oid": oid, 
						 "frequency": frequency 
		});
		
		this._oids[new_size-1]['url'] = this.get_action_uri(new_size-1);
		
		if (parent_div != null) {
			this._oids[new_size-1]['parent'] = parent_div;
		}
		
		if (callback != null) {
			this._oids[new_size-1]['callback'] = callback;
		}
		
		
		console.log(this._oids);
	},
	
	// Adds a proxy callback to the list
	add_proxy: function(dashboard, proc, params, frequency, div_id, parent_div) {
		new_size = this._oids.push({ "type"     : "proxy",
						  "dashboard": dashboard,
						  "frequency": frequency,
						  "div"      : div_id,
						  "params"   : params,
						  "proc"     : proc
		});
		
		if (parent_div != null) {
			this._oids[new_size-1]['parent'] = parent_div;
		}
	},

	add_tracker: function(oid_idx) {
                oid = this.get_div(oid_idx);
		div_id = oid + "_trackable";
		if (document.getElementById(div_id) != null) {
			document.getElementById(div_id).innerHTML = "<a href=\"javascript:addOIDTracker(" + this.device_id + ", '" + oid + "', 'INTEGER')\"><img src=\"/assets/icons/tracker_add.gif\" width=\"11\" height=\"18\" class=\"icon\" title=\"Track This Item\" alt=\"Track This Item\" border=\"0\"></a>";
		}
	},
	
	// Retrieves the ID of the progress indicator
	show_progress_indicator: function(oid_idx) {
		div_id = this.get_div(oid_idx) + "_progress";
		newColor = document.getElementById(this.get_div(oid_idx)).backgroundColor;
		document.getElementById(this.get_div(oid_idx)).style.color = newColor;		
	},
	
	// Hides the process indicator for the associated OID block
	hide_progress_indicator: function(oid_idx) {
		div_id = this.get_div(oid_idx) + "_progress";
		document.getElementById(this.get_div(oid_idx)).className=document.getElementById(this.get_div(oid_idx)).className;
	},
	
	// Gets the callback URI for an OID
	get_action_uri: function(oid_idx) {
		if (this._oids[oid_idx]['type'] == "oid") {
			return '/?module=mod_devices&action=ajax_get_oid_value&id=' + this.device_id + '&oid=' + this._oids[oid_idx]['oid'] + '&root_tpl=blank';
		} else {
			dash = this._oids[oid_idx]['dashboard'];
			proc = this._oids[oid_idx]['proc'];
			params = "&" + this._oids[oid_idx]['params'];
			return '/?module=mod_devices&action=ajax_proxy&profile='+dash+'&proc='+proc+params+'&root_tpl=blank';
		}
	},
	
	// Returns the ID of the div associated with an OID of proxy call
	get_div: function(oid_idx) {
		return (this._oids[oid_idx]['type'] == "oid") ? this._oids[oid_idx]['oid'] : this._oids[oid_idx]['div'];
	},
	
	// Retrieves the frequency associated with an OID
	get_frequency: function(oid_idx) {
		return this._oids[oid_idx]['frequency'];
	},
	
	// Schedule the next iteration
	schedule_update: function(oid_idx) {
		if (this.get_frequency(oid_idx) > 0) {
			aoid_obj = this;
			setTimeout(function() {aoid_obj.update_oid(oid_idx) }, aoid_obj._oids[oid_idx]['frequency']);
		}
	},
	
	// Success handler for AJAX requests
	success_handler: function(o) {
	
		/** This compensates for a YUI bug that was recently introduced.
		  * If you pass 0 as an AJAX connection argument, 
		  * you do not get your argument back.
		**/
		if (!o.argument) {
			o.argument = 0;
		} 
		
		oid = this._oids[o.argument];
		if (oid['callback']) {
			res = this._oids[o.argument]['callback'](o.responseText);
			
		} else {
			res = o.responseText;
		}

		document.getElementById(this.get_div(o.argument)).style.opacity = 0.01;
		document.getElementById(this.get_div(o.argument)).style.filter = "alpha(opacity=1)";
		document.getElementById(this.get_div(o.argument)).innerHTML = res;
		this.hide_progress_indicator(o.argument);
		
		var transition_attributes = {
			opacity: {to: 1},
			duration: 1
		}
		
		var transition = new YAHOO.util.Anim(document.getElementById(this.get_div(o.argument)), transition_attributes);
		transition.animate();
		this.schedule_update(o.argument);
	},
	
	// Failure handlers for AJAX requests
	failure_handler: function(o) {
		document.getElementById(this.get_div(o.argument)).innerHTML = "N/A";
		this.hide_progress_indicator(o.argument);
		
		//this.schedule_update(o.argument);
	},
	
	// Returns true if the 'parent' element of the specified OID is in a visible state,
	// False otherwise.
	is_visible: function(oid_idx) {
		if (this._oids[parseInt(oid_idx)]['parent'] == null) {
			return true;
		} else {
			parent_div = document.getElementById(this._oids[oid_idx]['parent']);
			display = parent_div.style.display;
			visibility = parent_div.style.visibility;
			if ((display != 'none') && (visibility != 'hidden')) {
				console.log(parent_div.id + " is visible");
				return true;
			}
		}
		console.log(parent_div.id + " is hidden");
		return false;
	},
	
	// Finds all the elements that are bound to the element 'parent_id'
	// and triggers an update against these elements
	update_children: function(parent_id) {
		for (var i = 0; i < this._oids.length; i++) {
			if (this._oids[i]['parent'] == parent_id) {
				console.log("Updating all children for div " + parent_id)
				this.update_oid(i);
			}
		}
	},
	
	// Trigger an AJAX lookup against an oid
	// If the 'frequency' of the OID is greater than
	// 0, schedule a refresh for the value in $freq ms
	update_oid: function(oid_idx) {
		if (this.is_visible(oid_idx)) {		
			this.show_progress_indicator(oid_idx);
			uri = this.get_action_uri(oid_idx);
			YAHOO.util.Connect.asyncRequest('GET', uri, {success:this.success_handler, failure:this.failure_handler, argument:parseInt(oid_idx), scope: this}, null);
		}
	},
	
	// Toggles a DIV's display style
	toggle_div: function(element_id) {
		element = document.getElementById(element_id);
		if (element.style.display == 'none') {
			element.style.display = 'block';
			this.update_children(element_id);
		} else {
			element.style.display = 'none';
		}
	}
};


// This function creates an OID tracker
function addOIDTracker(deviceid, oid, datatype) {
	url = "?module=mod_devices&action=form_create_oid_watcher&device_id=" + deviceid + "&oid_txt=" + escape(oid) + "&type=" + datatype + "&root_tpl=blank_panel";
	parent.pnl_right.showEditor(url);
}



/* ($Id$) */
