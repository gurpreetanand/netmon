var req;
function loadXMLDoc(url) 
{
    // branch for native XMLHttpRequest object
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
        req.onreadystatechange = processReqChange;
        req.open("GET", url, true);
        req.send(null);
    // branch for IE/Windows ActiveX version
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
        if (req) {
            req.onreadystatechange = processReqChange;
            req.open("GET", url, true);
            req.send();
        }
    }
}

function processReqChange() 
{
    // only if req shows "complete"
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
            // ...processing statements go here...
				BuildDebugWindow();
        } else {
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }
}

function BuildDebugWindow()
{
	template = req.responseText;
	//output rows are built here
	var oStr = "";
	for(i=0;i<DEBUG_DATA.length;i++)
	{
		oStr+="<tr>\n<td>" + DEBUG_DATA[i][0] + "</td>";
		oStr+="<td>" + DEBUG_DATA[i][5] + "</td>";
		oStr+="<td>" + DEBUG_DATA[i][1] + "</td>";
		oStr+="<td><img src=\"assets/icons/" + ReturnErrorSymbolByCode(DEBUG_DATA[i][2]) + "\" /></td>";
		
		if (DEBUG_DATA[i][4] == "") {
			oStr+="<td><div align=\"left\">" + DEBUG_DATA[i][3] + "</div></td>\n</tr>\n";
		} else {
			oStr+="<td><div class=\"expandingGroup\" align=\"left\"><nobr><img onClick=\"toggleSiblingDIV(this)\" src=\"assets/icons/icon_plus_collapsed.gif\" class=\"icon_control\"> <a href=\"#\" onClick=\"toggleSiblingDIV(this)\">" + DEBUG_DATA[i][3] + "</a></nobr></div><div class=\"closed\" align=\"left\">" + DEBUG_DATA[i][4] + "</div></td></tr>";
		}
	}
	oHTML = template.replace(/{DEBUG_ITEMS}/g,oStr);
	myWindow = window.open('', "debug", "scrollbars=yes,width=720,height=640,resizable=yes");
	myWindow.document.write(oHTML);
	myWindow.document.close();
}

function ReturnErrorSymbolByCode(err_code){
	switch(err_code)
	{
		case 0:  img="icon_info.gif";    break;
		case 1:  img="icon_info.gif";    break;
		case 2:  img="icon_info.gif";    break;
		case 3:  img="icon_info.gif";    break;
		case 4:  img="icon_caution.gif"; break;
		case 5:  img="icon_caution.gif"; break;
		case 6:  img="icon_caution.gif"; break;
		case 7:  img="icon_error.gif";   break;
		case 8:  img="icon_error.gif";   break;
		case 9:  img="icon_error.gif";   break;
		case 10: img="icon_error.gif";   break;
		default: img="icon_info.gif";    break;     
	}
	return img;
}