/*
 * Copyright (c) 2008, Netmon Inc. All Rights Reserved Author: $Author$ ($Id$)
 */


draw2d.NetmonNode = function(node, node_manager) {
	// node contains: type, ip, trackers[], then custom fields
	this.node = node;
	this.node_manager = node_manager;
	
	imgPath = "/assets/topology/"+this.node['type']+".gif";
	draw2d.ImageFigure.call(this,  imgPath);
	this.outputPort = null;
	this.setDimension(64,84);
	this.setName(this.node['ip']);
	this.setSelectable(false);
	
	this.ajax_refresh = function(myIndex, timeout) {
		Ext.Ajax.request({
			url: '/?module=mod_network&action=ajax_update_node_trackers&node='+this.node['ip']+'&root_tpl=blank',
			success: this.ajax_update_trackers,
			failure: this.ajax_fail,
			scope: this,
			params: {index:myIndex,timeout:timeout}
		});
	};
	
	this.ajax_update_trackers = function(resp, options) {
		this.node['trackers'] = eval('(' + resp.responseText + ')');
		items = 0;
	    down = 0;
	    for (var i in this.node['trackers']) {
	    	if (this.node['trackers'][i]['status']) {
		    	items++;
		    	if ((this.node['trackers'][i]['type'] == 'DF') || (this.node['trackers'][i]['type'] == 'SMB')) {
		    		if (parseInt(this.node['trackers'][i]['status']) >= parseInt(this.node['trackers'][i]['threshold'])) {
		    			console.log("Detected DOWN disk tracker");
		    			down = 1;
		    			this.html.style.border = "2px dashed #C1001B";
		    		}
		    	} else {
			    	if (this.node['trackers'][i]['status'] == 'DOWN') {
			    		console.log("Detected DOWN tracker");
			    		down = 1;
			    		this.html.style.border = "2px dashed #C1001B";
			    	}
		    	}
	    	}
	    }
	    
	    if ((items >= 1) && (down == 0)) {
	    	this.html.style.border = "2px dashed #0C9524";
	    } else if (items == 0) {
	    	this.html.style.border = 'none';
	    }
	    
	    
	    scopeObj = this.node_manager;
	    fnCall = "scopeObj.refresh_node("+options.params['index']+","+options.params['timeout']+")";
	    setTimeout(fnCall, options.params['timeout']);
	}
	
	this.ajax_confirm_node_saved = function(resp, options) {
		if ("OK" == resp.responseText) {
			console.log("New coordinates saved successfully");
		} else {
			console.log("Unable to save new coordinates:");
			console.log(resp.responseText);
		}
	}
	
	this.onDragend = function() {
		console.log("NODE DRAG END!!!");
		Ext.Ajax.request({
			url: '/?module=mod_network&action=ajax_save_node_location&root_tpl=blank',
			success: this.ajax_confirm_node_saved,
			failure: this.ajax_fail,
			params: {ip:this.node['ip'], x: this.getX(), y: this.getY()}
		});
		draw2d.Figure.prototype.onDragend.call(this);
	}
	
	this.ajax_fail = function(resp, options) {
		console.log("AJAX request failed");
	}
	
	
	return this;
}


draw2d.NetmonNode.prototype = new draw2d.ImageFigure;
draw2d.NetmonNode.prototype.type="NetmonNode";

draw2d.NetmonNode.prototype.setName = function(name) {
	this.label.innerHTML = name;
}


draw2d.NetmonNode.prototype.createHTMLElement=function()
{
    var item = draw2d.ImageFigure.prototype.createHTMLElement.call(this);
    
    item.style.backgroundRepeat="no-repeat";
    item.style.backgroundPosition="center center";
    item.style.padding = '3px';
    
    this.label = document.createElement("div");
    this.label.style.width="100%";
    this.label.style.height="10px";
    this.label.style.position="absolute";
    this.label.style.textAlign="center";
    this.label.style.top="0px";
    this.label.style.left="0px";
    this.label.style.bottom="0px";
    this.label.style.fontSize="8pt";
    
    items = 0;
    down = 0;
    for (var i in this.node['trackers']) {
	   	if (this.node['trackers'][i]['status']) {
	    	items++;
	    	if ((this.node['trackers'][i]['type'] == 'DF') || (this.node['trackers'][i]['type'] == 'SMB')) {
	    		if (parseInt(this.node['trackers'][i]['status']) >= parseInt(this.node['trackers'][i]['threshold'])) {
	    			down = 1;
	    			item.style.border = "2px dashed #C1001B";
	    		}
	    	} else {
		    	if (this.node['trackers'][i]['status'] == 'DOWN') {
		    		console.log("Detected DOWN tracker");
		    		down = 1;
		    		item.style.border = "2px dashed #C1001B";
		    	}
	    	}
	   	}
	}
    
    if ((items >= 1) && (down == 0)) {
    	item.style.border = "2px dashed #0C9524";
    } else if (items == 0) {
	    item.style.border = 'none';
	}
    
    this.node_manager.register_node(this);    
    
    return item;
}






draw2d.NetmonNode.prototype.setWorkflow = function(workflow) {
	draw2d.ImageFigure.prototype.setWorkflow.call(this, workflow);

	if (workflow != null && this.outputPort == null) {
		this.outputPort = new draw2d.trackerConnector();
		this.outputPort.setWorkflow(workflow);
		this.outputPort.setBackgroundColor(new draw2d.Color(115, 115, 245));
		this.addPort(this.outputPort, this.width+5, this.height / 2);

		this.outputPort1 = new draw2d.trackerConnector();
		this.outputPort1.setWorkflow(workflow);
		this.outputPort1.setBackgroundColor(new draw2d.Color(115, 115, 245));
		this.addPort(this.outputPort1, this.width / 2, -5);

		this.outputPort2 = new draw2d.trackerConnector();
		this.outputPort2.setWorkflow(workflow);
		this.outputPort2.setBackgroundColor(new draw2d.Color(115, 115, 245));
		this.addPort(this.outputPort2, this.width / 2, this.height+5);

		this.outputPort3 = new draw2d.trackerConnector();
		this.outputPort3.setWorkflow(workflow);
		this.outputPort3.setBackgroundColor(new draw2d.Color(115, 115, 245));
		this.addPort(this.outputPort3, -5, this.height / 2);
	}
}

draw2d.NetmonNode.prototype.paint = function() {
	draw2d.ImageFigure.prototype.paint.call(this);
	this.label.style.top = (this.getHeight() - parseInt(this.label.style.height)) + 'px';
	this.html.appendChild(this.label);
	
}

draw2d.NetmonNode.prototype.onMouseEnter = function() {
	console.log("Displaying trackers tooltip:");
	console.log(this.node);
	this.getWorkflow().showTooltip(new draw2d.trackerTooltip(this.node['trackers']), false);
}

