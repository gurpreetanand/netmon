/*
 * Copyright (c) 2008, Netmon Inc. All Rights Reserved Author: $Author$ ($Id$)
 */
 
 draw2d.trackerTooltip = function(trackers) {
 
 	//console.log("Trackers:");
 	//console.log(trackers);
 	this.trackers = trackers;
 	this.items = 0;
 	this.status = '';
 	
 	for (var key in trackers) {
 		
 		if ((trackers[key]['type'] == 'DF') || (trackers[key]['type'] == 'SMB')) {
 			this.status += "<tr><td>";
 			
 			if (parseInt(trackers[key]['status']) >= parseInt(trackers[key]['threshold'])) {
 				this.status += "<img src='/assets/icons/down.gif' alt='DOWN' title='DOWN'> ";
 			} else {
 				this.status += "<img src='/assets/icons/up.gif' alt='UP' title='UP'> ";
 			}
 			this.status += "</td><td>";
 			
 			this.status += trackers[key]['type']+" - "+trackers[key]['name']+" ("+trackers[key]['status']+"% used)</td></tr>";
 			this.items++;
 			
 		} else {
	 		if (trackers[key]['status']) {
		 		this.status += "<tr><td>";
		 		if (trackers[key]['status'] == 'DOWN') {
		 			this.status += "<img src='/assets/icons/down.gif' alt='DOWN' title='DOWN'> ";
		 		} else {
		 			this.status += "<img src='/assets/icons/up.gif' alt='UP' title='UP'> ";
		 		}
		 		this.status += "</td><td>";
		 		
		 		this.status += trackers[key]['protocol'];
		 		if (trackers[key]['protocol'] == 'ICMP') {
		 			this.status += "<br />";
		 		} else {
		 			this.status += "/" + trackers[key]['port'] + "<br />";
		 		}
		 		this.status += "</td></tr>"
		 		
		 		this.items++;
		 	}
 		}
 	}
 	
 	if ("" !=this. status) {
 		this.status = "<table width='150px' bgcolor='#eeeeee' border='0' style='border: 1px solid #000000'><thead><td align='center' colspan='2'><b>Device Trackers</b></td></thead><tbody>" + this.status + "</tbody></table>";
 	} else {
 		this.status = "<table width='150px' bgcolor='#eeeeee' border='0' style='border: 1px solid #000000'><tr><td>No trackers associated to this device</td></tr></table>";
 	}
 	
 	
 	
 	draw2d.Figure.call(this);
	/*
	this.setCanDrag(false);
	this.setFontSize(8);
	this.setSelectable(false); 
	this.setDeleteable(false);
	this.setBorder(new draw2d.LineBorder(1));
	*/
 }
 
 draw2d.trackerTooltip.prototype = new draw2d.Figure();
 draw2d.trackerTooltip.prototype.type = "trackerTooltip";
 
 draw2d.trackerTooltip.prototype.createHTMLElement = function() {
 	var tip = draw2d.Figure.prototype.createHTMLElement.call(this);
 	tip.innerHTML = this.status;
 	/*
 	tip.style.width = '280px';
 	tip.style.height = 18 + (16*(this.items)) + "px";
 	tip.style.border = '1px solid #000000';
 	tip.style.background = '#eeeeee';
 	tip.style.margin = '3px';
 	tip.style.padding = '3px';
 	tip.style.paddingLeft = '3px';
 	*/
 	tip.style.zIndex=(draw2d.Figure.ZOrderIndex+1);
 	
 	return tip; 	
 }