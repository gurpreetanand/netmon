/*
Copyright (c) 2006, Netmon Inc. All Rights Reserved
Author: $Author$
($Id$)
*/


// Constructor
KeepAlive = function() {};


// Object Prototype
KeepAlive.prototype = {
	
	// Device identifier
	ping_time: 60000,
	
	// AJAX KeepAlive Query
	query: '?module=core&action=ping&root_tpl=blank',
	
	// Start pinging the web-server
	init: function() {
		this.ping()
	},

	// Send an AJAX request whose response will be discarded
	ping: function() {
		YAHOO.util.Connect.asyncRequest('GET', this.query, {success:this.responseHandler, failure:this.responseHandler, scope: this}, null);
	},
	
	responseHandler: function(o) {
		if (o.responseText == 'SESS_OK') {
			console.log("Scheduling next ping");
			keepalive_obj = this;
			setTimeout(function() { keepalive_obj.ping() }, this.ping_time);
		} else {
			console.log("Session has died.");
		}
	}
};




/* ($Id$) */