/*
Copyright (c) 2006, Netmon Inc. All Rights Reserved
Author: $Author$
($Id$)
*/

var _months = {"Jan": 1, "Feb": 2, "Mar": 3, "Apr": 4, "May": 5, "Jun": 6, "Jul": 7, "Aug": 8, "Sep": 9, "Oct": 10, "Nov": 11, "Dec": 12};
	
var _sec   = 60;
var _min   = 60*_sec;
var _hour  = 60*_min;
var _day   = 24*_hour;
var _month = 31*_day;
var _year  = 365*_day;

debug = function(str) {
	document.getElementById("debug").value = str + "\n" + document.getElementById("debug").value;
}

netmon_date_sort = function(n_date) {
	
	regex = /([a-z]{3})\s+([0-9]+)\,\s+([123][0-9]{3})\s+([012][0-9]):([0-9]{2}):([0-9]{2})/i;

	
	res = n_date.match(regex);
	
	if (res != null) {		
		months  = _months[res[1]];
		days    = parseFloat(res[2]);
		years   = parseFloat(res[3]);
		
		hours   = parseFloat(res[4]);
		minutes = parseFloat(res[5]);
		seconds = parseFloat(res[6]);

		return _month*months+ _day*days+ _hour*hours + _min*minutes + seconds + _year*years;
	}
	return 0;
}


netmon_capacity_sort = function(n_capacity) {
	regex = /([0-9]+\.?[0-9]*)\s?([kmgt]?b)/i;
	res = n_capacity.match(regex);
	
	if (res != null) {
		base = res[1];
		mult = 1;
		unit = ""+res[2];
		unit = unit.toUpperCase();
		
		if (unit == "KB") {
			mult = 1024;
		} else if (unit == "MB") {
			mult = 1048576;
		} else if (unit == "GB") {
			mult = 1073741824;
		} else if (unit == "TB") {
			mult = 1099511627776;
		}
		return parseFloat(base)*mult;
	}
	return 0;
}

netmon_ipv4_sort = function(n_ip) {
	// Extract the first IP address in the string
	// [1-9]|[1-9][0-9]|1[0-9][0-9]|25[0-5]|2[0-4][0-9]
	regex = /([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/g;
	res = n_ip.match(regex);
	
	if (res != null) {
		n_ip = res[0];
	}
	
	//console.log("IP to sort: " + n_ip);
	
	try {
		var arr_ip = n_ip.split('.');
		ret = 0;
		for (i = 0; i < arr_ip.length; i++) {
			factor = Math.pow(256, (3-i));
			ret += parseFloat(arr_ip[i]*(factor));
		}
		return ret;
	} catch (exception) {
		return 0;
	}
}


netmon_throughput_sort = function(n_throughput) {
	regex = /([0-9]+\.?[0-9]*)\s+([KMGT]?bps)/i;
	res = n_throughput.match(regex);
	
	if (res != null) {
		base = res[1];
		mult = 1;
		if (res[2] == "Kbps") {
			mult = 1024;
		} else if (res[2] == "Mbps") {
			mult = 1048576;
		} else if (res[2] == "Gbps") {
			mult = 1073741824;
		} else if (res[2] == "Tbps") {
			mult = 1099511627776;
		}
		return parseFloat(base)*mult;
	}
	return 0;
}

netmon_time_sort = function(n_time) {
	time = _min*_extract_instances(n_time, 'minute') + 
				_hour*_extract_instances(n_time, 'hour') +
				_day*_extract_instances(n_time, 'day') +
				_month*_extract_instances(n_time, 'month') +
				_year*_extract_instances(n_time, 'year') +
				_extract_instances(n_time, 'second');
	return time;
}

_extract_instances = function(str, keyword) {
	regex = '([0-9]+)[\\s]+'+keyword;
	var reg = new RegExp(regex, 'gi');
	res = reg.exec(str);
	if (res != null) {
		return parseFloat(res[1]);
	}
	return 0;
	
}

percentage_sort = function(n_percent) {
	return parseFloat(n_percent.replace('%', ''));
}

netmon_latency_sort = function(n_latency) {
	return parseFloat(n_latency.replace(' ms', ''));
}

netmon_retries_sort = function(n_retries) {
	regex = /([0-9]+)\/[0-9]+/i;
	res = n_retries.match(regex);
	
	if (res != null) {
		return parseFloat(res[1]);
	}
	
	return 0;
}

netmon_ipv4_sort_with_html = function(s){
	//strip off tags
	var re = new RegExp("<([^<>\s]*)(\s[^<>]*)?>", "g");
	s = s.replace(re,"");
	var arr_ip = s.split('.');
	var ret=0;
	for (i=0;i<arr_ip.length;i++){
		ret += parseInt(String(arr_ip[arr_ip.length - i - 1])) * Math.pow(256, (i));
		//alert (s+":"+ret)
	}
	return ret;
}

netmon_string_sort_with_html = function(s){
	//strip off tags
	var re = new RegExp("<([^<>\s]*)(\s[^<>]*)?>", "g");
	s = s.replace(re,"");
	return String(s).toUpperCase();
}

SortableTable.prototype.addSortType("netmon_date", netmon_date_sort);
SortableTable.prototype.addSortType("netmon_capacity", netmon_capacity_sort);
SortableTable.prototype.addSortType("ipv4_address", netmon_ipv4_sort);
SortableTable.prototype.addSortType("netmon_throughput", netmon_throughput_sort);
SortableTable.prototype.addSortType("netmon_time", netmon_time_sort);
SortableTable.prototype.addSortType("percentage", percentage_sort);
SortableTable.prototype.addSortType("latency", netmon_latency_sort);
SortableTable.prototype.addSortType("retries", netmon_retries_sort);
SortableTable.prototype.addSortType("netmon_string_with_html", netmon_string_sort_with_html);