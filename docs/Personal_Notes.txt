1. Install
	a. Install Postgres SQL 8.4
		a.i Create root user in Postgres
		a.ii Create database 'nemton35'
		a.iii Modify pg_hba.conf and set all connections to 'trust'
		a.iv Run SQL query 'select pg_conf_load();'
		a.v Restart SQL server
	b. Install Third-Party support libraries for daemons
		b.i Download libsmi2 from URL: https://www.ibr.cs.tu-bs.de/projects/libsmi/download/libsmi-0.4.1.tar.gz and perform tar, configure, make and make install operations
		b.ii Download snmp++-3.3.11a from URL: https://www.agentpp.com/download/snmp++-3.3.11a.tar.gz and perform tar, config, make and make install operations.
		b.iii Download tre-0.8.0 from URL:http://laurikari.net/tre/tre-0.8.0.tar.gz and perform tar, configure, make and make install operations
	c. Run install.sh in Netmon directory
