#ifndef __SYSLOG_ALERT_MANAGER__
#define __SYSLOG_ALERT_MANAGER__

#include "alertmanager.h"
#include "alertutil.h"

namespace netmon
{
  class syslog_alert_manager : public alert_manager
  {
    public:
      syslog_alert_manager (db *mydb)
        : alert_manager (mydb, false)
      {}

      virtual ~syslog_alert_manager ()
      {}

      unsigned int getAlerts (unsigned int, unsigned int, const char *, values_map &);

    protected:
      virtual void getReferenceTable (std::string &table)
      {
        table = "syslog_access";
      }

      virtual void getOverName (std::string &name)
      {
        name = "SYSLOG_MSG";
      }

      virtual void getBelowName (std::string &/*name*/)
      {
      }

      virtual void getIDName (std::string &id)
      {
        id = "syslog_id";
      }

      virtual bool match (const char *, const char *);
  };
}

#endif // __SYSLOG_ALERT_MANAGER__
