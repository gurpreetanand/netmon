#ifndef __SYSLOG_MONITOR__
#define __SYSLOG_MONITOR__

#include "authlist.h"
#include "msgqueue.h"

#include <string>

namespace netmon
{
  class db;
  class syslog_alert_manager;
  class syslogmon_daemon;

  class syslog_monitor
  {
    public:
      syslog_monitor (db *, syslogmon_daemon *);
      ~syslog_monitor ();

      void run ();
      void reloadAuthList ();

    protected:
      void init ();
      void reload ();
      void buildAuthorizedList ();
      void receiveMessages ();
      void logMessage (char *, char *, int, int);
      void checkAlerts (unsigned int, char *, int, const char *);

      static int getPriority (char *, int);
      static char *getMessage (char *, int);
      static std::string severityToString (int);

      db *m_db;
      syslog_alert_manager *m_alertManager;
      authlist m_authList;
      int m_fd;
      syslogmon_daemon *m_daemon;
      static const unsigned short m_syslogPort = 514;

      enum { MAXMESGLEN = 1500 };

    private:
      struct daemon_msg : public msg
      {
        char m_data[20];
      };

      static const unsigned long queue_id = 0xff00cacf;
      msgqueue m_queue;
  };
}

#endif // __SYSLOG_MONITOR__
