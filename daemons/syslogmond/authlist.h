#ifndef __AUTH_LIST__
#define __AUTH_LIST__

#include <list>
#include <stdint.h>

namespace netmon
{
  class authlist
  {
    protected:
      struct authentry
      {
        authentry (uint32_t ip, int facility, int severity, unsigned int id)
          : m_ip (ip), m_facility (facility), m_severity (severity), m_id (id)
        {}
        uint32_t m_ip;
        int m_facility;
        int m_severity;
        unsigned int m_id;
      };

      typedef std::list<authentry> authlistT;

    public:
      authlist () {}
      ~authlist ()
      {
        clear ();
      }
      void add (uint32_t, int, int, unsigned int);
      bool isAuthorized (uint32_t, int, int, unsigned int &);
      void clear ();

    protected:
      bool find (uint32_t, int, int, unsigned int &);

      authlistT m_list;
  };
}

#endif // __AUTH_LIST__
