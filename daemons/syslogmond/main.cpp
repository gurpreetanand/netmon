#include <errno.h>
#include <signal.h>
#include <sys/time.h>

#include <string>
#include <cstdio>
#include <cstring>

#include "syslogmond.h"
#include "exception.h"
#include "log.h"

sig_atomic_t to_reload = 1;

static int installTimer ();

int main (int argc, char **argv)
{
  try
  {
    netmon::syslogmon_daemon *mon = new netmon::syslogmon_daemon(argc, argv);
    mon->init();
    if (installTimer() == -1)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Cannot install timer");
      return -1;
    }
    mon->start();
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
  }
  catch (int e)
  {
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, strerror (e));
  }
  return 0;
}

void alrmhandler (int /*sig*/)
{
  to_reload = 1;
}

static int installTimer ()
{
  struct sigaction saction;

  saction.sa_handler = alrmhandler;
  if (sigemptyset (&saction.sa_mask) == -1)
    return -1;

  saction.sa_flags = 0;
  if (sigaction (SIGALRM, &saction, NULL) == -1)
    return -1;

  itimerval timer;
  timer.it_interval.tv_sec = 60;
  timer.it_interval.tv_usec = 0;
  timer.it_value.tv_sec = 60;
  timer.it_value.tv_usec = 0;

  if (setitimer (ITIMER_REAL, &timer, 0) == -1)
    return -1;

  return 0;
}
