#include "syslogmonitor.h"
#include "syslogalertmgr.h"
#include "authlist.h"
#include "db.h"
#include "log.h"
#include "syslogmond.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>

#include <cstdlib>
#include <sstream>

#include <unistd.h>

extern sig_atomic_t to_reload;

namespace netmon
{
  syslog_monitor::syslog_monitor (db *mydb, syslogmon_daemon *d)
    : m_db (mydb), m_daemon (d)
  {
    init();
  }

  syslog_monitor::~syslog_monitor ()
  {
    close (m_fd);
  }

  void syslog_monitor::run ()
  {
    daemon_msg msg;

    msg.m_type = 1;
    strcpy (msg.m_data, "syslogmond");

    while (!m_daemon->termSignaled())
    {
      if (to_reload)
      {
        reload();
        m_queue.send (msg, sizeof (msg), IPC_NOWAIT);
      }
      receiveMessages();
    }
  }

  void syslog_monitor::receiveMessages ()
  {
    static char mesg[MAXMESGLEN];
    sockaddr_in cli_addr;
    int clilen = sizeof (cli_addr);

    int n = recvfrom (m_fd, mesg, MAXMESGLEN, 0, (struct sockaddr *) &cli_addr, (socklen_t *) &clilen);

    if (n < 0)
      return;

    mesg[n] = '\0';

    int priority = getPriority (mesg, n);
    char *message = getMessage (mesg, n);

    if (priority < 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "Could not get priority");
      return;
    }

    int facility = priority / 8;
    int severity = priority % 8;

    unsigned int id;
    if (!m_authList.isAuthorized (cli_addr.sin_addr.s_addr, facility, severity, id))
      /* We don't accept messages from this host */
      return;

    logMessage (message, inet_ntoa (cli_addr.sin_addr), facility, severity);

    checkAlerts (id, inet_ntoa(cli_addr.sin_addr), severity, message);
  }

  void syslog_monitor::logMessage (char *message, char *ip, int facility, int severity)
  {
    static std::ostringstream sql;
    char *escaped = new char[strlen (message) * 2 + 1];
    PQescapeString (escaped, message, strlen (message));

    sql.str("");
    sql << "INSERT INTO SYSLOG (TIMESTAMP, IP, FACILITY, SEVERITY, MESSAGE) VALUES("
        << time(NULL) << ", '" <<  ip << "'," << facility
        << ", " << severity << ", '" << escaped << "')";

    delete[] escaped;
    m_db->execSQL (sql.str().c_str());
  }

  void syslog_monitor::checkAlerts (unsigned int id, char *ip, int severity, const char *message)
  {
    values_map vmap;
    vmap[netmon::alerthandler::eHostIP] = keyword_value (ip);
    vmap[netmon::alerthandler::eSysLogMsg] = keyword_value (message);
    vmap[netmon::alerthandler::eSysLogSeverity] = keyword_value (severityToString(severity));
    m_alertManager->getAlerts (id, severity, message, vmap);
  }

  void syslog_monitor::reload ()
  {
    netmon::log::instance()->add(netmon::_eLogDebug, "Re-reading list of allowed hosts...");

    buildAuthorizedList();

    m_alertManager->reload();
    netmon::log::instance()->add(netmon::_eLogDebug, "Done re-reading.");

    to_reload = 0;
  }

  void syslog_monitor::buildAuthorizedList ()
  {
    m_authList.clear();

    int i_fac, i_sev;
    uint32_t i_ip;
    unsigned int id;
    struct in_addr s_ip;

    try
    {
      PGresult *res = m_db->execQuery ("SELECT IP, FACILITY, SEVERITY, SYSLOG_ID FROM SYSLOG_ACCESS");
      for (int n = 0; n < PQntuples (res); ++n)
      {
        inet_aton (PQgetvalue (res, n, 0), &s_ip);
        i_ip  = s_ip.s_addr;
        i_fac = atoi (PQgetvalue (res, n, 1));
        i_sev = atoi (PQgetvalue (res, n, 2));
        id = atoi (PQgetvalue(res, n, 3));

        m_authList.add (i_ip, i_fac, i_sev, id);
      }
      PQclear (res);
    }
    catch (netmon::db_exception &e)
    {
      netmon::log::instance()->add(netmon::_eLogError, e.what());
    }
  }

  void syslog_monitor::init ()
  {
    if ((m_fd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Could not open socket.");
      exit (-1);
    }

    sockaddr_in serv_addr;
    bzero ((char *) &serv_addr, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl (INADDR_ANY);
    serv_addr.sin_port = htons (m_syslogPort);

    if (bind (m_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Bind failed.");
      exit(-1);
    }

    m_alertManager = new syslog_alert_manager (m_db);
    m_alertManager->init();

    m_queue.create (queue_id, 0600);
  }

  int syslog_monitor::getPriority (char *buff, int len)
  {
    static char result[1024];
    int i = 0;
    char *p = buff;

    while ((*p != '<') && (i < len))
    {
      ++p;
      ++i;
    }

    if (*p != '<') 
      return -1;

    ++p;

    strncpy (result, p, len - i);

    p = result;
    i = 0;

    while ((*p != '>') && (i < len - i))
    {
      ++p;
      ++i;
    }

    if (*p != '>') 
      return -2;

    *p = '\0';

    return atoi (result);
  }

  char *syslog_monitor::getMessage (char *buff, int len)
  {
    int i = 0;
    char *p = buff;

    while ((*p != '>') && (i < len))
    {
      ++p;
      ++i;
    }

    if (*p != '>')
      return 0;

    ++p;

    return p;
  }

  std::string syslog_monitor::severityToString (int severity)
  {
    switch (severity)
    {
      case 0:
        return "Emergency";
      case 1:
        return "Alert";
      case 2:
        return "Critical";
      case 3:
        return "Error";
      case 4:
        return "Warning";
      case 5:
        return "Notice";
      case 6:
        return "Info";
      case 7:
        return "Debug";
      default:
        return "Unknown";
    }
  }
}
