#include "authlist.h"

namespace netmon
{
  void authlist::add (uint32_t ip, int facility, int severity, unsigned int id)
  {
    unsigned int dumb;

    if (find (ip, facility, severity, dumb))
      return;
    m_list.push_back (authentry (ip, facility, severity, id));
  }

  bool authlist::isAuthorized (uint32_t ip, int facility, int severity, unsigned int &id)
  {
    return find (ip, facility, severity, id);
  }

  void authlist::clear ()
  {
    m_list.clear ();
  }

  bool authlist::find (uint32_t ip, int facility, int severity, unsigned int &id)
  {
    authlistT::const_iterator it;
    for (it = m_list.begin (); it != m_list.end (); ++it)
    {
      const authentry &e = *it;
      if (e.m_ip == ip && (e.m_facility == facility || e.m_facility == -1) && e.m_severity >= severity)
      {
        id = e.m_id;
        return true;
      }
    }
    return false;
  }
}
