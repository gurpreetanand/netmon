#ifndef __SERIALIZER__
#define __SERIALIZER__

#include <list>
#include <set>
#include <string>
#include <sstream>

namespace netmon
{
  class serializer
  {
    public:
      typedef std::pair<std::string, bool> ifaces_pair_t;
      typedef std::list<ifaces_pair_t> ifaces_simple_t;

      serializer () {}
      virtual ~serializer () {}

      virtual void serializeIFace (const std::string &);
      virtual void serializeProc (const std::string &, const std::string &, bool, bool, bool = false);
      virtual void serializePlugin (const std::string &, const std::string &, const ifaces_simple_t &, const std::set<std::string> &, const std::set<std::string> &);

      virtual void start () {}
      virtual void finish () {}
      virtual void startIfaces () {}
      virtual void finishIfaces () {}
      virtual void startProcs () {}
      virtual void finishProcs () {}
      virtual void startPlugins () {}
      virtual void finishPlugins () {}

      std::string getBuffer () const
      {
        return m_buffer.str();
      }

    protected:
      std::ostringstream m_buffer;
  };

  class xml_serializer : public serializer
  {
    public:
      virtual void serializeIFace (const std::string &);
      virtual void serializeProc (const std::string &, const std::string &, bool, bool, bool = false);
      virtual void serializePlugin (const std::string &, const std::string &, const ifaces_simple_t &, const std::set<std::string> &, const std::set<std::string> &);

      virtual void start ()
      {
        m_buffer << "<daemon_status>" << std::endl;
      }

      virtual void finish ()
      {
        m_buffer << "</daemon_status>" << std::endl;
      }

      virtual void startIfaces ()
      {
        m_buffer << "<running_interfaces>" << std::endl;
      }

      virtual void finishIfaces ()
      {
        m_buffer << "</running_interfaces>" << std::endl;
      }

      virtual void startProcs ()
      {
        m_buffer << "<services>" << std::endl;
      }

      virtual void finishProcs ()
      {
        m_buffer << "</services>" << std::endl;
      }

      virtual void startPlugins ()
      {
        m_buffer << "<plugins>" << std::endl;
      }

      virtual void finishPlugins ()
      {
        m_buffer << "</plugins>" << std::endl << "</service>" << std::endl;
      }
  };
}

#endif // __SERIALIZER__
