#ifndef __CHECKER_THREAD__
#define __CHECKER_THREAD__

#include "../netmond/thread.h"
#include "msgqueue.h"
#include <sys/sysinfo.h>
#include <sys/time.h>
#include <stdint.h>


#include <map>
#include <list>
#include <string>

namespace netmon
{
  class db;
  class master_proc;
  class daemon_container;
  class procmon_daemon;

  class checker_thread : public thread
  {
    public:
      checker_thread (master_proc *, daemon_container *, procmon_daemon *);
      virtual ~checker_thread ();

    protected:
      virtual void run ();

      bool isDBConnectionAlive ();
      void checkLimitsValid ();
      void checkDBPartition ();
      void checkCrashes (bool = true);
      void checkFreezes ();
      void checkMemUsage ();
      bool dbPartitionFull ();
      void restartDaemons ();
      void init ();

      void connectToDB ();
      void createQueue ();

      db *m_db;
      master_proc *m_masterProc;
      daemon_container *m_container;
      procmon_daemon *m_daemon;
      msgqueue m_queue;

      typedef std::list<std::string> daemon_list_t;
      daemon_list_t m_daemons;

      static const unsigned long queue_id = 0xff00cacf;
      static const unsigned int m_timeOut = 5 * 60; // timeout 5 mins

    private:
      struct daemon_msg : public msg
      {
        char mdata[20];
      };

      typedef std::map<std::string, unsigned int> dmap_t;
      dmap_t m_daemonTimeStamp;

      struct status
      {
        std::string m_name;
        uint32_t m_peak;
        uint32_t m_size;
        pid_t m_pid;
      };
      
      struct timeval lastSnap;
      bool daemonsStopped;

      void checkFreeze (dmap_t::iterator &);
      bool memOverLimit ();
  };
}

#endif // __CHECKER_THREAD__
