#include "daemoncontainer.h"
#include "db.h"
#include "../netmond/lock.h"

#include <algorithm>
#include <vector>

namespace netmon
{
  daemon_container::daemon_container (db *mydb)
    : m_db (mydb)
  {
    init();
  }

  daemon_container::~daemon_container ()
  {
    m_daemons.clear();
  }

  void daemon_container::getDaemonList (std::list<std::string> &list)
  {
    for (daemon_map_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i)
      list.push_back (i->first);
  }

  void daemon_container::getDaemons (daemon_list_t &list)
  {
    mutex_lock l(m_mutex);

    std::vector<daemon_data> v;
    for (daemon_map_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i)
      v.push_back (daemon_data (i->first, i->second.m_desc, i->second.m_autoStart));

    sort (v.begin(), v.end(), daemon_data_order());

    for (unsigned int i = 0; i < v.size(); ++i)
      list.push_back (v[i]);
  }

  void daemon_container::shouldBeRunning (const char *daemon, bool running)
  {
    mutex_lock l(m_mutex);

    daemon_map_t::iterator i = m_daemons.find (std::string (daemon));
    if (i == m_daemons.end())
      return;

    i->second.m_isRunning = running;
  }

  bool daemon_container::shouldBeRunning (const char *daemon)
  {
    mutex_lock l(m_mutex);

    daemon_map_t::iterator i = m_daemons.find (std::string (daemon));
    if (i == m_daemons.end())
      return false;

    return i->second.m_isRunning;
  }

  void daemon_container::updateAutoStartFlag ()
  {
    mutex_lock l(m_mutex);

    daemon_map_t::iterator it;
    PGresult *res = m_db->execQuery ("select name, start_auto from daemons where component_type='daemon'");
    for (int i = 0; i < PQntuples (res); ++i)
    {
      it = m_daemons.find (std::string (PQgetvalue (res, i, 0)));
      if (it == m_daemons.end())
        continue;
      it->second.setAuto (PQgetvalue (res, i, 1));
    }
    PQclear (res);
  }

  void daemon_container::init ()
  {
    PGresult *res = m_db->execQuery ("select name, description, start_auto from daemons where component_type='daemon'");
    for (int i = 0; i < PQntuples (res); ++i)
      m_daemons[std::string (PQgetvalue (res, i, 0))] = daemon (PQgetvalue (res, i, 1), PQgetvalue (res, i, 2));

    PQclear (res);
  }
}
