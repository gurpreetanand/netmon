#include "procmond.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <pcre.h>

#include <sstream>

#include "checkerthread.h"
#include "daemoncontainer.h"
#include "masterproc.h"
#include "serializer.h"
#include "serial.h"
#include "log.h"

extern sig_atomic_t is_running;
extern sig_atomic_t is_int;

namespace netmon
{
  static const char *sock_path = "/var/run/.netmon.s";
  static const char *lock_file = "/var/run/procmond.pid";
  static const char *path = "/usr/local/sbin/";

  procmon_daemon::procmon_daemon (int argc, char **argv)
    : daemon (argc, argv), m_myName (argv[0])
  {
  }

  procmon_daemon::~procmon_daemon ()
  {
    close (m_fd);
    close (m_lockfd);
    unlink (sock_path);
    unlink (lock_file);
    pcre_free (m_re);
  }

  void procmon_daemon::init ()
  {
    daemon::init();

    if (!createLockFile())
      exit (-1);

    createSocket();

    if (m_path.size() == 0)
    {
      size_t pos = m_myName.rfind ('/');
      if (pos == std::string::npos)
        m_path = std::string (path);
      else
      {
        std::string tmp = std::string (m_myName, 0, pos + 1);
        if (tmp.size() <= 2)
          m_path = std::string (path);
        else
          m_path = tmp;
      }
    }
    printf ("path: %s\n", m_path.c_str());
    m_container = new daemon_container (m_db);
    m_master = new master_proc (m_db, m_container, m_path.c_str());

    const char *err;
    int erroffset;

    m_re = pcre_compile ("\\s*(dump|restart|start|stop|status)\\s*([a-zA-Z0-9\\-]+)?\\s*([a-zA-Z0-9]*)\\s*", 0, &err, &erroffset, NULL);
    if (m_re == 0)
    {
      throw (netmon_exception ("cannot create regexp"));
      return;
    }
  }

  void procmon_daemon::usage ()
  {
    printf ("Usage: %s [-h] [-f] [-D]\n -f: run in foreground\n"
            " -h: gives this help\n -D <path>: path to netmon"
            " daemons\n", getDaemonName());
  }

  void procmon_daemon::onOption (char c)
  {
    daemon::onOption (c);
    if (c == 'D')
    {
      m_path = std::string (optarg);
      if (m_path[m_path.size() - 1] != '/')
        m_path += "/";
    }
  }

  const char *procmon_daemon::getShortOptions ()
  {
    m_opts = daemon::getShortOptions();
    m_opts += "D:";
    return m_opts.c_str();
  }

  int procmon_daemon::run ()
  {
    checker_thread ct (m_master, m_container, this);
    ct.start();

    while (is_running)
    {
      sockaddr_un client;
      socklen_t client_len;
      int c_fd;

      if (is_int)
        handleSigInt();

      /* Accept a connection. */
      client_len = sizeof (client);
      c_fd = accept (m_fd, (sockaddr *) &client, &client_len);
      if (c_fd == -1)
        perror ("accept");

      /* Handle the connection. */
      handleClient (c_fd);

      /* Close our end of the connection. */
      close (c_fd);
    }
    return 0;
  }

  void procmon_daemon::createSocket ()
  {
    unlink (sock_path); // remove old socket

    // Create the socket

    sockaddr_un name;
    m_fd = socket (PF_LOCAL, SOCK_STREAM, 0);

    if (m_fd < 0)
    {
      perror ("socket");
      exit (-1);
    }

    // Indicate that this is a server.
    name.sun_family = AF_LOCAL;
    strcpy (name.sun_path, sock_path);
    if (bind (m_fd, (const sockaddr *) &name, SUN_LEN (&name)) < 0)
    {
      perror ("bind");
      exit (-1);
    }

    // make it world readable
    chmod (sock_path, 0777);

    // Listen for connections.
    listen (m_fd, 5);
  }

  bool procmon_daemon::createLockFile ()
  {
    m_lockfd = open (lock_file, O_RDWR | O_CREAT, 0600);
    if (m_lockfd == -1)
      return false;

    chmod (lock_file, 0666);

    flock lock;
    lock.l_type = F_WRLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 5;

    if (fcntl (m_lockfd, F_SETLK, &lock) == -1)
    {
      close (m_lockfd);
      log::instance()->add(netmon::_eLogFatal, "Cannot create pid file. Another instance of procmond running?");
      return false;
    }

    char pid[10];
    snprintf (pid, 9, "%d\n", (int) getpid());
    write (m_lockfd, (void *) pid, strlen (pid));

    return true;
  }

  void procmon_daemon::handleClient (int fd)
  {
    static char line[512];
    int s = 0;

    while (true)
    {
      alarm (2); // 2 secs timeout
      int ret = read (fd, line + s, sizeof (line) - s);
      alarm(0);
      if (ret <= 0)
      {
        return;
      }
      s += ret;
      if (s == sizeof (line)) // client sent too much data, disconnect it
      {
        return;
      }
      line[s] = '\0';
      if (strchr ((const char *) line, '\n') != 0) // \n marks the end of cmd
      {
        bool ret = runCmd (line);
        std::string msg = ret == true ? "OK " : "ERR ";
        msg += m_msg;
        msg += "\n";

        write (fd, msg.c_str (), msg.size ());

        m_msg = "";
        return;
      }
    }
  }

  void procmon_daemon::handleSigInt ()
  {
    is_int = 0;
    if (!m_sn->valid())
    {
      m_master->stopAllDaemons();
    }
  }

  bool procmon_daemon::runCmd (char *arg)
  {
    std::string dname;
    std::string darg;
    std::string cmd;

    if (!parseCmd (arg, dname, darg, cmd))
    {
      printf ("cannot parse cmd (%s)\n", arg);
      return false;
    }

    printf("Got command: [%s] dname: [%s] darg: [%s]\n", cmd.c_str(), dname.c_str(), darg.c_str());

    if (cmd == "status" && dname == "")
      return execCmd (cmd, "", "");

    if (!m_master->isDaemon (dname.c_str ())) // don't handle non-netmon stuff
    {
      m_msg = dname + std::string (" is not a valid daemon name.");
      return false;
    }

    return execCmd (cmd, dname, darg);
  }

  bool procmon_daemon::parseCmd (char *arg, std::string &dname, std::string &darg, std::string &cmd)
  {
    int vec[30];
    int ret = pcre_exec (m_re, 0, arg, strlen (arg), 0, 0, vec, 30);
    if (ret < 0 || ret != 4)
      return false;

    const char *c;

    for (c = arg + vec[2]; c < arg + vec[3]; ++c)
      cmd.push_back (*c);

    for (c = arg + vec[4]; c < arg + vec[5]; ++c)
      dname.push_back (*c);

    for (c = arg + vec[6]; c < arg + vec[7]; ++c)
      darg.push_back (*c);

    return true;
  }

  bool procmon_daemon::execCmd (const std::string &cmd, const std::string &dname, const std::string &darg)
  {
    const char *arg = 0;
    if (darg.size () > 0)
      arg = darg.c_str();
    
    bool retval = false;

    if (cmd == "start")
    {
      retval = m_master->startDaemon (dname.c_str(), arg);
      m_container->shouldBeRunning (dname.c_str(), true);
    }
    if (cmd == "stop")
    {
      bool ret = m_master->stopDaemon (dname.c_str(), arg) == 0 ? false : true;
      if (ret)
      {
        std::string tmp = dname + " killed by 'stop' command";
        netmon::log::instance()->add(netmon::_eLogUrgent, tmp.c_str());
      }
      retval = ret;
      m_container->shouldBeRunning (dname.c_str(), false);
    }
    if (cmd == "restart")
    {
      if (m_master->stopDaemon (dname.c_str(), arg))
        retval = m_master->startDaemon (dname.c_str(), arg);
    }
    if (cmd == "status");
    {
      if (dname == "")
      {
        xml_serializer xml;
        m_master->serializeStatus (xml);
        m_msg = xml.getBuffer();
        retval = true;
      } else { 
        int pid = m_master->getDaemonStatus (dname.c_str(), arg);
        std::ostringstream tmp;
        tmp << (int) pid;
        m_msg = tmp.str();

        retval = true;
      }
    }
    return retval;
  }
}
