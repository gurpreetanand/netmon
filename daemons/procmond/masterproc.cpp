#include "masterproc.h"

#include <sys/wait.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>

#include <algorithm>
#include <sstream>

#include "daemoncontainer.h"
#include "serializer.h"
#include "db.h"
#include "log.h"
#include "../netmond/lock.h"

//#define DEBUG

namespace netmon
{
  master_proc::master_proc (db *mydb, daemon_container *container, const char *path)
    : m_db (mydb), m_container (container), m_path (path)
  {
    init();
  }

  master_proc::~master_proc ()
  {
    m_ifaces.clear();
  }

  int master_proc::startDaemon (const char *daemon, const char *arg /* = 0 */)
  {
    mutex_lock l(m_mutex);

    if (!isDaemon (daemon))
      return 0;

    if (getDaemonStatus(daemon, arg) == 0) {
      printf("Daemon %s not started, starting!\n", daemon);
      log::instance()->add(_eLogFatal, "Daemon %s not started, starting!", daemon);
      std::string cmd = m_path + std::string (daemon);
      char *args[] = { (char *) cmd.c_str(), (char *) arg, 0 };
      pid_t pid = spawn(args);
      sleep(2);
      updateProc(daemon);
      return (int) pid;
    }
    return 0;
  }

  int master_proc::stopDaemon (const char *daemon, const char *arg /* = 0 */)
  {
    mutex_lock l(m_mutex);

    pid_t pid;
    pid = getDaemonPID (daemon, arg);
    if (pid == 0)
    {
      return 0;
    }
    kill (pid, SIGKILL);
    procMap.erase(daemon);
    return (int) pid;
  }

  void master_proc::stopAllDaemons ()
  {
    for (daemon_list_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i)
      stopDaemon ((*i).m_name.c_str());
  }

  int master_proc::getDaemonStatus (const char *daemon, const char *arg /* = 0 */)
  {
    int daemonPID = getDaemonPID (daemon, arg);
    if (daemonPID == 0)
    {
      return 0;
    }
    std::ostringstream fname;
    fname.str("");
    fname << "/proc/" << daemonPID << "/stat";

    printf("Trying to open stat file %s... \n", fname.str().c_str());
    int fd = open (fname.str().c_str(), O_RDONLY);
    if (fd == -1)
      return 0;

    char sinfo[128];
    alarm (5); // 5 sec timeout
    int rval = read (fd, sinfo, sizeof (sinfo) - 1);
    close (fd);
    alarm (0);
    if (rval <= 0)
      return 0;

    std::string comparator(sinfo);
    
    if (comparator.find(daemon) != std::string::npos) {
      printf("...Success!\n");
      return daemonPID;
    } else {
      procMap[daemon] = 0;
      return 0;
    }
  }

  void master_proc::serializeStatus (serializer &s)
  {
    m_container->updateAutoStartFlag();
    getDaemons();

    s.start();
    s.startIfaces();

    for (iface_list_t::iterator i = m_ifaces.begin(); i != m_ifaces.end(); ++i)
      if ((*i).m_up)
        s.serializeIFace ((*i).m_name);

    s.finishIfaces();
    s.startProcs();

    bool up;

    for (daemon_list_t::iterator i = m_daemons.begin (); i != m_daemons.end(); ++i)
    {
      if (getDaemonStatus ((*i).m_name.c_str()) != 0)
        up = true;
      else
        up = false;
      if ((*i).m_name != "netmond")
        s.serializeProc ((*i).m_name, (*i).m_desc, up, (*i).m_start);
      else
      {
        s.serializeProc ((*i).m_name, (*i).m_desc, up, (*i).m_start, true);

        std::list<plugin> list;
        getPlugins ((*i).m_name, list);

        serializer::ifaces_simple_t ifaces;
        for (iface_list_t::iterator k = m_ifaces.begin(); k != m_ifaces.end(); ++k)
          ifaces.push_back (std::make_pair ((*k).m_name, (*k).m_up));

        s.startPlugins();

        for (std::list<plugin>::iterator j = list.begin(); j != list.end(); ++j)
        {
          std::set<std::string> setr;
          std::set<std::string> sets;

          if (up)
            parse_devs ((*j).m_rundevs, setr);
          parse_devs ((*j).m_devs, sets);
          s.serializePlugin ((*j).m_name, (*j).m_desc, ifaces, setr, sets);
        }

        s.finishPlugins();
      }
    }

    s.finishProcs();
    s.finish();
  }

  void master_proc::parse_devs (const std::string &devs, std::set<std::string> &set)
  {
    if (devs.size () < 2)
      return;

    std::string tmp = devs;
    std::replace (tmp.begin(), tmp.end(), ',', ' ');
    std::istringstream s(tmp);
    std::string i;

    while (!s.eof())
    {
      s >> i;
      set.insert (i);
    }
  }

  void master_proc::init ()
  {
    getInterfaces();
    getDaemons();
    startDaemons();
    sleep(2);
    updateProcs();
  }

  void master_proc::getInterfaces ()
  {
    intf_t *intf;
    if ((intf = intf_open()) == 0)
    {
      perror ("intf_open");
      exit (1);
    }
    intf_loop (intf, ifCallback, this);
    intf_close (intf);
  }

  void master_proc::getDaemons ()
  {
    m_daemons.clear();
    m_container->getDaemons (m_daemons);
    for (daemon_list_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i)
      printf ("daemon: %s desc: '%s'\n", (*i).m_name.c_str(), (*i).m_desc.c_str());
  }

  void master_proc::getPlugins (const std::string &daemon, std::list<plugin> &list)
  {
    static std::ostringstream sql;
    sql.str("");

    sql << "select a.name, a.description, a.start_auto, start_ifaces, running_ifaces from "
           "plugins a, daemons b where daemon_id = b.id and b.name = '";
    sql << daemon << "' order by a.name";

    PGresult *res = m_db->execQuery (sql.str().c_str());
    for (int i = 0; i < PQntuples (res); ++i)
      list.push_back (plugin (PQgetvalue (res, i, 0), PQgetvalue (res, i, 1), PQgetvalue (res, i, 2), PQgetvalue (res, i, 3), PQgetvalue (res, i, 4)));

    PQclear (res);
  }

  void master_proc::startDaemons ()
  {
    for (daemon_list_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i)
    {
      if (!(*i).m_start)
        continue;
      std::string cmd = m_path + (*i).m_name;
      char *argv[] = { (char *) cmd.c_str(), 0 };
      spawn (argv);
    }
  }

  void master_proc::updateProcs ()
  {
    DIR *proc;

    proc = opendir ("/proc");
    if (proc == 0)
    {
      log::instance()->add(_eLogFatal, "Cannot open /proc for reading!");
      return;
    }

    struct dirent *proc_entry;

    proc_entry = readdir (proc);
    if (proc_entry == 0)
    {
      closedir (proc);
      return;
    }
    char *name = proc_entry->d_name;

    while (name != NULL) {
      if (strspn (name, "0123456789") == strlen (name)) { // only digits
        pid_t pid = (pid_t) atoi (name);
        char *fprocname = getProcName(pid);
        if (fprocname != NULL) {
          std::string exename = std::string(fprocname);
          for (daemon_list_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i)
            if (exename.find((*i).m_name) != std::string::npos) {
              procMap[exename] = pid;
              printf("Inserting proc [%s] with PID [%d]\n", exename.c_str(), pid);
            }
          delete [] fprocname;
        }
      }
      
      proc_entry = readdir (proc);
      if (proc_entry == 0) {
        closedir (proc);
        return;
      }
      name = proc_entry->d_name;
    }
    closedir (proc);
  }
  
  void master_proc::updateProc (const char* daemon) {
    DIR *proc;

    proc = opendir ("/proc");
    if (proc == 0)
    {
      log::instance()->add(_eLogFatal, "Cannot open /proc for reading!");
      return;
    }

    struct dirent *proc_entry;

    proc_entry = readdir (proc);
    if (proc_entry == 0)
    {
      closedir (proc);
      return;
    }

    char *name = proc_entry->d_name;
    while (name != NULL) {
      if (strspn (name, "0123456789") == strlen (name)) {// only digits
        pid_t pid = (pid_t) atoi (name);
        char *thename = getProcName(pid);

        if (thename != NULL) {
          std::string exename = std::string(thename);
          delete [] thename;
          if (exename.find(daemon) != std::string::npos) {
            procMap[daemon] = pid;
            printf("Updating proc [%s] with PID [%d]\n", daemon, pid);
          }
        }
      }
      proc_entry = readdir (proc);
      if (proc_entry == 0) {
        closedir (proc);
        return;
      }

      name = proc_entry->d_name;

    }
    closedir (proc);
  }


  
  bool master_proc::isDaemon (const char *daemon)
  {
    if (daemon == 0)
      return false;
    
    for (daemon_list_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i) {
      if (strcmp (daemon, (*i).m_name.c_str()) == 0)
        return true;
    }
    return false;
  }

  void master_proc::addInterface (const std::string &name, bool up)
  {
    if (name != "lo")
      m_ifaces.push_back (iface (name, up));
  }

  int master_proc::ifCallback (const intf_entry *e, void *me)
  {
    master_proc *p = reinterpret_cast<master_proc *> (me);
    p->addInterface (std::string (e->intf_name), e->intf_flags & INTF_FLAG_UP);
    return 0;
  }

  int master_proc::spawn (char **argv)
  {
    pid_t pid = fork ();
    int status;

    if (pid < 0)
      return -1;

    if (pid == 0)
    {
      if (execvp (*argv, argv) < 0)
        exit (-1);
    }
    else
      waitpid (-1, &status, WNOHANG);

    return pid;
  }

  char *master_proc::getProcName (pid_t pid)
  {
    static char fname[64];

    snprintf (fname, sizeof (fname) - 1, "/proc/%d/stat", (int) pid);

    int fd = open (fname, O_RDONLY);
    if (fd == -1)
      return NULL;

    static char sinfo[256];

    alarm (5); // 5 sec timeout
    int rval = read (fd, sinfo, sizeof (sinfo) - 1);
    close (fd);
    alarm (0);

    if (rval <= 0)
      return NULL;

    sinfo[rval] = '\0';

    char *op, *cp;
    op = strchr (sinfo, '(');
    cp = strchr (sinfo, ')');
    if (op == 0 || cp == 0 || cp < op)
      return NULL;

    char *res = new char[cp - op];
    strncpy (res, op + 1, cp - op - 1);
    res[cp - op - 1] = '\0';
    return res;    
  }

  char *master_proc::getProcArgs (pid_t pid)
  {
    static char fname[64];

    snprintf (fname, sizeof (fname) - 1, "/proc/%d/cmdline", (int) pid);

    int fd = open (fname, O_RDONLY);
    if (fd == -1)
      return 0;

    static char sinfo[256];
    alarm (5); // 5 sec timeout
    int rval = read (fd, sinfo, sizeof (sinfo) - 1);
    close (fd);
    alarm (0);

    if (rval <= 0)
      return 0;

    sinfo[rval] = '\0';

    char *n;
    n = strchr (sinfo, '\0');
    if (n == sinfo + 255)
      return 0;

    if (*++n == '\0')
      return 0;

    char *res = new char[rval - (int)(n - sinfo)];
    strcpy (res, n);

    return res;
  }

  pid_t master_proc::getDaemonPID (const char *daemon, const char *arg /* = 0 */)
  {
    if (procMap.find(std::string(daemon)) != procMap.end()) {
      return procMap.find(std::string(daemon))->second;
    }
    return 0;
  }
}

#ifdef __TEST__

#include <cstdio>
#include <iostream>

#include "netmon.h"

using namespace netmon;
using namespace std;

class test : public master_proc
{
  public:
    test (db *mydb)
      : master_proc (mydb)
    {}
    void print ()
    {
      for (iface_list_t::iterator i = m_ifaces.begin(); i != m_ifaces.end(); ++i)
      {
        cout << (*i).m_name.c_str() << " ";
        if ((*i).m_up)
          cout << "up" << endl;
        else
          cout << "down" << endl;
      }
    }

    void printd ()
    {
      for (daemon_list_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i)
        cout << "daemon: " << (*i).m_name << " desc: " << (*i).m_desc
        << " start: " << (*i).m_start << endl;
    }
};

int main()
{
  char dbname[DBNAMELEN];
  char dbuser[DBUSERLEN];

  readConfig (dbname, dbuser);

  db *m_db = new db;
  m_db->connect (dbname, dbuser);

  test *p = new test(m_db);
  p->print();
  p->printd();
  delete p;
}

#endif // __TEST__
