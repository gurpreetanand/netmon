#ifndef __DAEMON_CONTAINER__
#define __DAEMON_CONTAINER__

#include <map>
#include <list>
#include <string>

#include "daemondata.h"
#include "../netmond/mutex.h"

namespace netmon
{
  class db;

  class daemon_container
  {
    public:
      daemon_container (db *);
      ~daemon_container ();

      void getDaemonList (std::list<std::string> &);
      void getDaemons (daemon_list_t &);
      void shouldBeRunning (const char *, bool);
      bool shouldBeRunning (const char *);
      void updateAutoStartFlag ();

    protected:
      void init ();

      struct daemon
      {
        daemon ()
          : m_desc (""), m_autoStart (false), m_isRunning (false)
        {}

        daemon (const char *desc, const char *start)
          : m_desc (desc)
        {
          setAuto (start);
          m_isRunning = m_autoStart;
        }

        void setAuto (const char *start)
        {
          if (start == 0 || strlen (start) == 0)
            m_autoStart = false;
          else
            m_autoStart = (*start == 't' ? true : false);
        }

        std::string m_desc;
        bool m_autoStart;
        bool m_isRunning;
      };

      typedef std::map<std::string, daemon> daemon_map_t;

      db *m_db;
      mutex m_mutex;
      daemon_map_t m_daemons;
  };
}

#endif // __DAEMON_CONTAINER__
