#include <errno.h>
#include <signal.h>
#include <sys/types.h>

#include <string>
#include <cstdio>
#include <cstring>

#include "procmond.h"
#include "exception.h"
#include "log.h"

sig_atomic_t is_running = 1;
sig_atomic_t is_int = 0;

void sighandler (int sig)
{
  if (sig == SIGTERM)
    is_running = 0;
  if (sig == SIGINT)
    is_int = 1;
}

void installSigHandler ()
{
  struct sigaction action;

  // install signal handler; ignore SIGPIPE

  action.sa_handler = sighandler;
  sigemptyset (&action.sa_mask);
  action.sa_flags = 0;

  sigaction (SIGTERM, &action, NULL);
  sigaction (SIGALRM, &action, NULL);
  sigaction (SIGINT, &action, NULL);

  action.sa_handler = SIG_IGN;

  sigaction (SIGPIPE, &action, NULL);
  action.sa_flags = SA_NOCLDSTOP;
  sigaction (SIGCHLD, &action, NULL);
}

int main (int argc, char **argv)
{
  try
  {
    netmon::procmon_daemon *mon = new netmon::procmon_daemon(argc, argv);
    mon->init();
    installSigHandler();
    mon->start();
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    netmon::log::instance()->add(netmon::_eLogFatal, "Error in main: %s", e.what());
  }
  catch (int e)
  {
    netmon::log::instance()->add(netmon::_eLogFatal, "int Error in main %s", strerror (e));
  }
  return 0;
}
