#ifndef __TRAP_STORE__
#define __TRAP_STORE__

#include <string>

#include "db.h"
#include "oidstore.h"

namespace netmon
{
  class trapoidstore : public storefunctor
  {
    public:
      trapoidstore (const std::string &, const std::string &);
      ~trapoidstore ();

      virtual void store (const std::string &, const oid &);
      void setFile (const std::string &, const std::string &);

    protected:
      void init ();
      void insertMIBFile ();
      unsigned int getMIBID ();
      void loadOIDs ();

      db m_db;
      unsigned int m_mibID;
      bool m_mibInserted;
      std::string m_dbname;
      std::string m_dbuser;
      std::string m_file;
      std::string m_path;
      int m_id;

    private:
      struct trap_data
      {
        unsigned int m_id;
        std::string m_name;
        std::string m_desc;
      };

      typedef hash_map<std::string, trap_data *> trap_data_hash_t;

      trap_data_hash_t m_data;
  };
}

#endif // __TRAP_STORE__
