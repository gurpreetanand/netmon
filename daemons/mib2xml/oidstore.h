#ifndef __OID_STORE__
#define __OID_STORE__

#include <string>

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
namespace __gnu_cxx
{
  template<> struct hash<std::string>
  {
    size_t operator()(std::string __s) const { return __stl_hash_string(__s.c_str ()); }
  };
}
#endif

namespace netmon
{
  struct oid
  {
    std::string m_name;
    std::string m_description;
  };

  class storefunctor
  {
    public:
      virtual ~storefunctor () {}
      virtual void store (const std::string &, const oid &) = 0;
  };

  class oidstore
  {
    public:
      static oidstore *instance ();
      void add (const char *, const char *, const char *);
      void store (storefunctor &);

    protected:
      oidstore () {}
      ~oidstore ();

      static oidstore *m_instance;
      hash_map<std::string, oid *> m_oids;
  };
}

#endif // __OID_STORE__
