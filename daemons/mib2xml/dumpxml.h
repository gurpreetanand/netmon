#ifndef __DUMP_XML__
#define __DUMP_XML__

#include <smi.h>

void dumpXml (SmiModule *modv, int flags, const char *output);

#endif // __DUMP_XML__
