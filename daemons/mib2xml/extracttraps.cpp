#include <cstdio>
#include <smi.h>

#include <cstring>
#include <string>
#include <sstream>

#include "netmon.h"
#include "exception.h"
#include "oidstore.h"
#include "trapoidstore.h"

void parseModule (SmiModule *);
void getNameAndPath (const char *, std::string &, std::string &);

static void errorHandler (char *path, int line, int severity, char *msg, char *tag)
{
  if (severity <= 1)
    fprintf (stderr, "Error: ");
  else
    fprintf (stderr, "Warning: ");

  if (path)
    fprintf (stderr, "%s:%d: ", path, line);

  fprintf (stderr, "%s\n", msg);
}

int main (int argc, char **argv)
{
  char dbname [DBNAMELEN];
  char dbuser [DBUSERLEN];

  if (argc < 2)
  {
    printf ("Usage: extracttraps [-d] MIB\n\n  -d: delete old OIDs from the db.\n");
    exit (0);
  }

  readConfig (dbname, dbuser);

  smiInit ("mib2xml");
  smiSetErrorHandler (errorHandler);
  int flags = smiGetFlags();
  flags |= SMI_FLAG_ERRORS;
  smiSetFlags (flags);

  std::string file;
  std::string path;

  char *modulename = smiLoadModule (argv[1]);
  if (modulename != 0)
  {
    getNameAndPath (argv[1], file, path);

    try
    {
      netmon::trapoidstore oidstore (dbname, dbuser);
      oidstore.setFile (file, path);

      SmiModule *smiModule = smiGetModule (modulename);
      if (smiModule)
      {
        parseModule (smiModule);
        netmon::oidstore::instance()->store (oidstore);
      }
    }
    catch (netmon::netmon_exception &e)
    {
      fprintf (stderr, "Error: %s\n", e.what());
      exit (1);
    }
  }

  smiExit ();
  return 0;
}

void parseModule (SmiModule *smiModule)
{
  SmiNode *smiNode = smiGetFirstNode (smiModule, SMI_NODEKIND_NOTIFICATION);

  while (smiNode)
  {
    std::ostringstream oid;
    for (unsigned int i = 0; i < smiNode->oidlen; ++i)
    {
      if (i > 0)
        oid << "." << smiNode->oid[i];
      else
        oid << smiNode->oid[i];
    }
    netmon::oidstore::instance()->add (oid.str().c_str(), smiNode->name, smiNode->description);
    smiNode = smiGetNextNode (smiNode, SMI_NODEKIND_NOTIFICATION);
  }
}

void getNameAndPath (const char *path, std::string &file, std::string &dir)
{
  const char *p = strrchr (path, '/');

  if (p != 0)
  {
    file = std::string (p + 1);
    dir = std::string (path);
  }
  else
  {
    file = std::string (path);
    dir = "";
  }
}
