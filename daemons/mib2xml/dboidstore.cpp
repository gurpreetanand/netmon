#include "dboidstore.h"

#include <sstream>

namespace netmon
{
  dboidstore::dboidstore (const std::string &dbname, const std::string &dbuser)
    : m_dbname (dbname), m_dbuser (dbuser)
  {
    init ();
  }

  dboidstore::~dboidstore ()
  {
    m_db.execSQL ("COMMIT");
    m_db.close ();
  }

  void dboidstore::store (const std::string &stroid, const oid &myoid)
  {
    char *esc = 0;
    if (myoid.m_description.size() > 0)
    {
      esc = new char[(myoid.m_description.size() * 2 + 1)];
      PQescapeString (esc, myoid.m_description.c_str(), myoid.m_description.size());
    }

    std::ostringstream sql;
    sql << "insert into snmp_oid_trans (id, oid, name, description) \
            values (default, '" << stroid << "', '" << myoid.m_name << "', '";
    if (esc != 0)
      sql << esc;

    sql << "')";

    m_db.execSQL (sql.str().c_str());
    delete[] esc;
  }

  void dboidstore::init ()
  {
    m_db.connect (m_dbname.c_str(), m_dbuser.c_str());
    m_db.execSQL ("BEGIN");
  }
}
