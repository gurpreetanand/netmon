#include <cstdio>
#include <cstring>
#include <string>
#include <stdexcept>

#include <signal.h>
#include <sys/types.h>

#include "exception.h"
#include "smbmond.h"
#include "log.h"

int running;

void sighandler (int sig)
{
  if (sig == SIGTERM)
    running = 0;
}

void installSigHandler ()
{
  struct sigaction action;

  // install signal handler; ignore SIGPIPE

  action.sa_handler = sighandler;
  sigemptyset (&action.sa_mask);
  action.sa_flags = 0;

  sigaction (SIGTERM, &action, NULL);
  sigaction (SIGALRM, &action, NULL);

  action.sa_handler = SIG_IGN;

  sigaction (SIGPIPE, &action, NULL);
  sigaction (SIGCHLD, &action, NULL);
}

int main (int argc, char **argv)
{
  try
  {
    netmon::smbmon_daemon *w = new netmon::smbmon_daemon (argc, argv);
    w->init();
    running = 1;
    installSigHandler();
    w->start();
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    return -1;
  }
  catch (int e)
  {
    printf ("Error: %s\n", strerror (e));
    return -1;
  }
  return 0;
}
