#include <stdio.h>
#include <string.h>

typedef const struct
{
  char *error_string;
  int   error_code;
} smb_err_code_struct;

smb_err_code_struct smb_errors[] =
{
  { (char *) "SMB_HOST_UNREACHABLE"           , 0xC000023D },
  { (char *) "SMB_NO_LOGON_SERVERS"           , 0xC000005E },
  { (char *) "SMB_LOGON_FAILURE"              , 0xC000006D },
  { (char *) "SMB_ACCOUNT_LOCKED_OUT"         , 0xC0000234 },
  { (char *) "SMB_BAD_SHARE_NAME"             , 0xC00000CC },
  { (char *) "SMB_TOO_MANY_SESSIONS"          , 0xC00000CE },
  { (char *) "SMB_MORE_PROCESSING_REQUIRED"   , 0xC0000016 },
  { (char *) "SMB_ACCESS_DENIED"              , 0xC0000022 },
  { NULL, 0 }
};

int getsmberror(char *description, int code)
{
  int index = 0;

  while (smb_errors[index].error_string != NULL)
  {
    if (smb_errors[index].error_code == code)
    {
      strcpy(description, smb_errors[index].error_string);
      return 1;
    }
    index++;
  }
  
  /* Error description not found */
  return 0;
}
