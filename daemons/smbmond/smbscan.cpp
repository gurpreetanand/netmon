#include <cstdio>
#include <cstdlib>
#include <signal.h>
#include <getopt.h>

#include <list>
#include <string>

#include "../lib/netmon.h"
#include "getspace.h"

char syslogbuff[SYSLOGLINELEN];
const char version[] = "0.0.4";

void sighandler (int);

static struct option const long_options[] =
{
  {"help",		no_argument, 0, 'h'},
  {"version",		no_argument, 0, 'v'},
  {"free",		no_argument, 0, 'f'},
  {"username",		required_argument, 0, 'u'},
  {"password", 		required_argument, 0, 'p'},
  {"domain", 		required_argument, 0, 'd'},
  {0, 0, 0, 0}
};

int printversion ()
{
  printf ("%s\n", version);
  return 0;
}

int usage ()
{
  printf ("Usage: smbscan [-h] [-v] [-f] [-u username] [-p password] IP-address\n");
  printf ("\t-h: gives this help\n");
  printf ("\t-v: prints version information\n");
  printf ("\t-f: prints the amount of free space\n");
  printf ("\t-u username: use username for SMB connection\n");
  printf ("\t-p password: use password for SMB connection\n");
  printf ("\t-d domain: use domain as domain name for SMB connection\n");

  return 1;
}

int main (int argc, char **argv)
{
  struct sigaction action;
  char error_message[1024];
  int result;

  if (argc < 2)
    return usage ();

  action.sa_handler = sighandler;
  sigemptyset (&action.sa_mask);
  action.sa_flags = 0;

  sigaction (SIGPIPE, &action, NULL);
  sigaction (SIGALRM, &action, NULL);

  std::string user;
  std::string pass;
  std::string domain;
  bool print_free = false;

  int c, longind;

  while ((c = getopt_long (argc, argv, "hvfp:u:d:", long_options, &longind)) != EOF)
  {
    switch (c)
    {
      case 'h':
        return usage ();
      case 'v':
        return printversion ();
      case 'f':
        print_free =true;
        break;
      case 'u':
        user = std::string (optarg);
        break;
      case 'p':
        pass = std::string (optarg);
        break;
      case 'd':
        domain = std::string (optarg);
        break;
      default:
        printf ("Illegal argument, use --help for help\n");
        exit (-1);
    }
  }

  if (optind >= argc)
  {
    printf ("Illegal number of arguments, use --help for help\n");
    exit (-1);
  }

  argc -= optind;
  argv += optind;

  std::list<std::string> list;
  int port = 445;

  result = getshares (argv[0], port, user.c_str(), pass.c_str(), "NETMON", domain.c_str(), list, error_message);

  /* If connection fails on port 445, try connecting to 139 */
  if (result < 0)
  {
    port = 139;
    result = getshares (argv[0], port, user.c_str(), pass.c_str(), "NETMON", domain.c_str(), list, error_message);
  }

  if (result != 0)
  {
//    printf ("Cannot get SMB name: %s (%d).\n", error_message, result);
    exit (-1);
  }

  if (!print_free)
  {
    for (std::list<std::string>::iterator it = list.begin(); it != list.end(); ++it)
      printf ("%s\n", (*it).c_str());
  }
  else
  {
    int t, a;
    int b;
    for (std::list<std::string>::iterator it = list.begin(); it != list.end(); ++it)
    {
      t = a = b = 0;
      result = getspace (argv[0], port, (*it).c_str(), user.c_str(), pass.c_str(), "NETMON", domain.c_str(), &t, &a, &b, error_message);
      if (result != 0)
        continue;
      unsigned long long res = (unsigned long long) a * (unsigned long long) b;
      printf ("%s %lld bytes free\n", (*it).c_str(), res);
    }
  }

  return 0;
}

void sighandler (int)
{
}
