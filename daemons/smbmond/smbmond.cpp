#include "smbmond.h"
#include "smbmonitor.h"
#include "log.h"

#include <unistd.h>

extern int running;

namespace netmon
{
  smbmon_daemon::smbmon_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  smbmon_daemon::~smbmon_daemon ()
  {
    delete m_smbMonitor;
  }

  void smbmon_daemon::init ()
  {
    daemon::init();
    m_smbMonitor = new smbmonitor (m_db);
  }

  int smbmon_daemon::run ()
  {
    while (running && !isTermSignaled())
    {
//      log::instance()->add(_eLogInfo, "New scan");// commented on 14/12/2018 by sameer
      m_smbMonitor->run();
      usleep (60);
    }
    return 0;
  }
}
