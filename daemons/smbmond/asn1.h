#ifndef __ASN_1__
#define __ASN_1__

#include <list>

#include "exception.h"
#include <stdio.h>

#define ASN1_APPLICATION(x) ((x)+0x60)
#define ASN1_SEQUENCE(x) ((x)+0x30)
#define ASN1_CONTEXT(x) ((x)+0xa0)
#define ASN1_GENERAL_STRING 0x1b
#define ASN1_OCTET_STRING 0x4
#define ASN1_OID 0x6
#define ASN1_BOOLEAN 0x1
#define ASN1_INTEGER 0x2
#define ASN1_ENUMERATED 0xa
#define ASN1_SET 0x31

namespace netmon
{
  typedef unsigned char uchar;
  struct nesting
  {
    off_t m_start;
    size_t m_taglen;
  };

  struct asn1data
  {
    uchar *m_data;
    size_t m_length;
    off_t m_offset;
    bool m_error;
    std::list<nesting> m_nesting;
  };

  class asn1exception : public netmon_exception
  {
    public:
      asn1exception (const std::string &err)
        : netmon_exception (err)
      {}
  };

  class asn1parser
  {
    public:
      asn1parser (const uchar *, unsigned int);
      virtual ~asn1parser ();

      void startTag (uchar);
      void endTag ();
      void readOID (std::string &);
      uchar *readOctetStr (unsigned int &);

    protected:
      void init (const uchar *, unsigned int);
      void read (void *, unsigned int);
      void readuchar (uchar &);
      int tagRemaining ();

      asn1data m_data;
  };
}

#endif // __ASN_1__
