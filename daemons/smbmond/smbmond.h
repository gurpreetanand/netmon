#ifndef __SMB_DAEMON__
#define __SMB_DAEMON__

#include "daemon.h"

namespace netmon
{
  class smbmonitor;

  class smbmon_daemon : public daemon
  {
    public:
      smbmon_daemon (int, char **);
      ~smbmon_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "smbmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/smbmond";
      }

      smbmonitor *m_smbMonitor;
  };
}

#endif //__SMB_DAEMON__
