#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <iostream>
#include <netinet/tcp.h>

#include "../lib/netmon.h"
#include "../lib/log.h"
#include "ntlm.h"
#include "smberror.h"
#include "spnegoasn1.h"
#include "getspace.h"

struct negProtResponse
{
  uint16_t DialectIndex;
  uint8_t  SecurityMode;
  uint16_t MaxMpxCount;
  uint16_t MaxNumberVCs;
  uint32_t  MaxBufferSize;
  uint32_t  MaxRawSize;
  uint32_t  SessionKey1;
  uint32_t  SessionKey2;
  uint32_t  Capabilities1;
  uint32_t  Capabilities2;
  uint32_t  SystemTimeLow;
  uint32_t  SystemTimeHigh;
  int16_t          ServerTimeZone;
  uint8_t  EncryptionKeyLength;
} __attribute((packed))__;

#define MAXLINE          4096
#define SMBERRMESSAGELEN 256

#define TYPE_HIDDEN 0x80000000
#define TYPE_SHARE 0x00

//#define DEBUG

unsigned short lendian(unsigned char first, unsigned char second);

char    uid_l, uid_h, tid_l, tid_h, fid_l, fid_h;
std::string    share_name, share_hostname;
int	unicode;
int	esn = 0;

int prepare_115_ESN(char *line, const char *username, const char *password, const char *domain, 
                    char *challenge, char *targetInformation, unsigned char ti_len,
                    unsigned char *userid);

int str_copy(char *dest, char *src, int len);
int unicode_copy(char *dest, char *src);
bool analyzeLANMAN (char *line, int size, std::list<std::string> &);
std::string readstring(char* line, int size);


int prepare_andx(unsigned char *line)
{
  unsigned char *pointer;
  static unsigned char data[] = {
0xff, 0x53, 0x4d, 0x42, 0x75, 0x00, 0x00, 0x00, 0x00,
0x08, 0x05, 0xc8, 0x00, 0x00, 0x86, 0x51, 0x1b, 0xdd,
0xf6, 0x8f, 0x31, 0x5b, 0x00, 0x00, 0xff, 0xff, 0x47,
0x03, 0x02, 0xa8, 0x04, 0x00, 0x04, 0xff, 0x00, 0x00,
0x00, 0x08, 0x00, 0x01, 0x00, 0x2f, 0x00, 0x00, 0x5c,
0x00, 0x5c, 0x00, 0x31, 0x00, 0x39, 0x00, 0x32, 0x00,
0x2e, 0x00, 0x31, 0x00, 0x36, 0x00, 0x38, 0x00, 0x2e,
0x00, 0x31, 0x00, 0x2e, 0x00, 0x31, 0x00, 0x31, 0x00,
0x5c, 0x00, 0x49, 0x00, 0x50, 0x00, 0x43, 0x00, 0x24,
0x00, 0x00, 0x00, 0x3f, 0x3f, 0x3f, 0x3f, 0x3f, 0x00
};

  const int size = sizeof(data)/sizeof(unsigned char);

  pointer = line;

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  *pointer++ = size; // fixed size block

  memcpy ((void *) pointer, data, size);
  *(line + 30) = 0x13;
  *(line + 31) = 0x13;
  /* We got UID from 115 */
  *(line + 32) = uid_l;
  *(line + 33) = uid_h;
  // MID is 4
  *(line + 34) = 4;
  *(line + 35) = 0;


  sign_smb_inplace(line, size, 2);
  return sizeof(data)+4;
}

int prepare_treeconnect(unsigned char *line)
{
  unsigned char *pointer;
  static unsigned char data[] = {
0xff, 0x53, 0x4d, 0x42, 0xa2, 0x00, 0x00, 0x00, 0x00, 
0x08, 0x05, 0xc8, 0x00, 0x00, 0xcd, 0xc5, 0x4e, 0x9c, 
0x55, 0x7d, 0xed, 0xba, 0x00, 0x00, 0x0e, 0x08, 0x13, 
0x13, 0x03, 0xd0, 0x05, 0x00, 0x18, 0xff, 0x00, 0x00, 
0x00, 0x00, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x9f, 0x01, 0x02, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 
0x11, 0x00, 0x00, 0x5c, 0x00, 0x73, 0x00, 0x72, 0x00, 
0x76, 0x00, 0x73, 0x00, 0x76, 0x00, 0x63, 0x00, 0x00, 
0x00

};

  const int size = sizeof(data)/sizeof(unsigned char);

  pointer = line;

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  *pointer++ = size; // fixed size block

  memcpy ((void *) pointer, data, size);
  *(line + 28) = tid_l;
  *(line + 29) = tid_h;
  *(line + 30) = 0x13;
  *(line + 31) = 0x13;
  /* We got UID from 115 */
  *(line + 32) = uid_l;
  *(line + 33) = uid_h;
  // MID is 5
  *(line + 34) = 5;
  *(line + 35) = 0;


//  sign_smb (line + 4, flen, 4, line + 26, size - 22, signature);
  //memcpy ((void *) (line + 18), signature, 8);
  sign_smb_inplace(line, size, 4);

  return sizeof(data)+4;

}




int prepare_bind(unsigned char *line)
{
  unsigned char *pointer;
  static unsigned char data[] = {

0xff, 0x53, 0x4d, 0x42, 0x25, 0x00, 0x00, 0x00, 0x00, 0x08,
0x05, 0xc8, 0x00, 0x00, 0x6e, 0xfb, 0x98, 0x9c, 0x68, 0xb4, 
0x84, 0x11, 0x00, 0x00, 0x04, 0x88, 0x47, 0x03, 0x02, 0xa8,
0x06, 0x00, 0x10, 0x00, 0x00, 0x48, 0x00, 0x00, 0x00, 0xb8,
0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x52, 0x00, 0x48, 0x00, 0x52, 0x00, 0x02, 
0x00, 0x26, 0x00, 0x0d, 0xc0, 0x57, 0x00, 0x00, 0x5c, 0x00,
0x50, 0x00, 0x49, 0x00, 0x50, 0x00, 0x45, 0x00, 0x5c, 0x00,
0x00, 0x00, 0x05, 0x00, 0x0b, 0x03, 0x10, 0x00, 0x00, 0x00,
0x48, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xb8, 0x10, 
0xb8, 0x10, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
0x00, 0x00, 0x01, 0x00, 0xc8, 0x4f, 0x32, 0x4b, 0x70, 0x16,
0xd3, 0x01, 0x12, 0x78, 0x5a, 0x47, 0xbf, 0x6e, 0xe1, 0x88,
0x03, 0x00, 0x00, 0x00, 0x04, 0x5d, 0x88, 0x8a, 0xeb, 0x1c, 
0xc9, 0x11, 0x9f, 0xe8, 0x08, 0x00, 0x2b, 0x10, 0x48, 0x60,
0x02, 0x00, 0x00, 0x00

};

  const int size = sizeof(data)/sizeof(unsigned char);

  pointer = line;

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  *pointer++ = size; // fixed size block

  memcpy ((void *) pointer, data, size);
  *(line + 28) = tid_l;
  *(line + 29) = tid_h;
  *(line + 30) = 0x13;
  *(line + 31) = 0x13;
  /* We got UID from 115 */
  *(line + 32) = uid_l;
  *(line + 33) = uid_h;
  // MID 6
  *(line + 34) = 6;
  *(line + 35) = 0;
  *(line + 63 + 4) = fid_l;
  *(line + 64 + 4) = fid_h;

  sign_smb_inplace(line, size, 6);

  return sizeof(data)+4;

}






int prepare_netshare(unsigned char *line)
{
  unsigned char *pointer;
  static unsigned char data[] = {

0xff, 0x53, 0x4d, 0x42, 0x25, 0x00, 0x00, 0x00, 0x00, 0x08,
0x05, 0xc8, 0x00, 0x00, 0xd3, 0x9b, 0x69, 0x01, 0x49, 0xb5, 
0xa5, 0xe1, 0x00, 0x00, 0x04, 0x88, 0x47, 0x03, 0x02, 0xa8, 
0x07, 0x00, 0x10, 0x00, 0x00, 0x64, 0x00, 0x00, 0x00, 0xb8, 
0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x52, 0x00, 0x64, 0x00, 0x52, 0x00, 0x02, 
0x00, 0x26, 0x00, 0x0d, 0xc0, 0x73, 0x00, 0x00, 0x5c, 0x00, 
0x50, 0x00, 0x49, 0x00, 0x50, 0x00, 0x45, 0x00, 0x5c, 0x00, 
0x00, 0x00, 0x05, 0x00, 0x00, 0x03, 0x10, 0x00, 0x00, 0x00, 
0x64, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x4c, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x02, 0x00, 
0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0d, 0x00, 
0x00, 0x00, 0x31, 0x00, 0x39, 0x00, 0x32, 0x00, 0x2e, 0x00, 
0x31, 0x00, 0x36, 0x00, 0x38, 0x00, 0x2e, 0x00, 0x31, 0x00, 
0x2e, 0x00, 0x31, 0x00, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x04, 0x00, 
0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0xff, 0xff, 0xff, 0xff, 0x08, 0x00, 0x02, 0x00, 0x00, 0x00, 
0x00, 0x00

};

  const int size = sizeof(data)/sizeof(unsigned char);

  pointer = line;

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  *pointer++ = size; // fixed size block

  memcpy ((void *) pointer, data, size);
  *(line + 28) = tid_l;
  *(line + 29) = tid_h;
  *(line + 30) = 0x13;
  *(line + 31) = 0x13;

  // MID 7
  *(line + 34) = 7;
  *(line + 35) = 0;
  *(line + 32) = uid_l;
  *(line + 33) = uid_h;
  *(line + 63 + 4) = fid_l;
  *(line + 64 + 4) = fid_h;

  sign_smb_inplace(line, size, 8);

  return sizeof(data)+4;

}

// OLD FAIL STUFF BEGIN

/******************************************************************************
* 
* Name: prepare_114
* Description: assembles SMB block 114 (SMB command = 0x72)
* 
* Arguments: Buffer to store SMB block
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int prepare_114(char *line)
{
  char *pointer, *smb_bct;
  int copied;

  pointer = line;

  memset(pointer, '\0', 1024);

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB size will be calculated later */
  pointer++;

  /* SMB header */
  *pointer++ = 255;
  *pointer++ = 'S';
  *pointer++ = 'M';
  *pointer++ = 'B';

  /* SMB command */
  *pointer++ = 0x72;

  /* Error code bytes */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB flags */
  *pointer++ = 8;

  /* SMB flags2 */
  *pointer++ = 0x01;
  // *pointer++ = 0;	// NTLM-V1
  *pointer++ = 0xD8;	// NTLM-V2, extended security negotiation	

  /* Skip IPX reserved area */
  pointer += 12;

  /* SMB TID is 0 at this moment */
  *pointer++ = 0;
  *pointer++ = 0;

  /* PID doesn't really matter for us, we'll assume 19*256 + 19 */
  *pointer++ = 19;
  *pointer++ = 19;

  /* UID is still 0 */
  *pointer++ = 0;
  *pointer++ = 0;

  /* MID is 1 , only one thread */
  *pointer++ = 1;
  *pointer++ = 0;

  /* Word count */
  *pointer++ = 0;

  /* Byte count */
  smb_bct = pointer;
  pointer++;
  *pointer++ = 0;

  /* Delimiter */
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "PC NETWORK PROGRAM 1.0", strlen ((char *) "PC NETWORK PROGRAM 1.0"));
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;

  /* Delimiter */
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "MICROSOFT NETWORKS 1.03", strlen ((char *) "MICROSOFT NETWORKS 1.03"));
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;

  /* Delimiter */
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "MICROSOFT NETWORKS 3.0", strlen ((char *) "MICROSOFT NETWORKS 3.0"));
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;

  /* Delimiter */
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "LANMAN1.0", strlen ((char *) "LANMAN1.0"));
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;

  /* Delimiter */
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "LM1.2X002", strlen ((char *) "LM1.2X002"));
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;

  /* Delimiter */
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "DOS LANMAN2.1", strlen ((char *) "DOS LANMAN2.1"));
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;

  /* Delimiter */
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "Samba", strlen ((char *) "Samba"));
  pointer += copied;
  /* Delimiter */
   *pointer++ = 0x00;

  /* Delimiter */
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "NT LANMAN 1.0", strlen ((char *) "NT LANMAN 1.0"));
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;

  /* Delimiter */
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "NT LM 0.12", strlen ((char *) "NT LM 0.12"));
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;

  /* SMB size */
  *(line+3) = pointer - line - 4;

  /* Byte number */
  *smb_bct  = pointer - smb_bct - 2;

  return pointer-line;
}

/******************************************************************************
* 
* Name: prepare_115
* Description: assembles SMB block 115 (SMB command = 0x73)
* 
* Arguments: Buffer to store SMB block
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int prepare_115(char *line, const char *username, const char *password, const char *domain, char *challenge)
{
  char *pointer, *smb_bct;
  int copied;
  unsigned char lmresp[128], ntresp[128];
  char calling_name[64];

  strcpy(calling_name, "NETMON");

  pointer = line;

  memset(pointer, '\0', 1024);

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB size will be calculated later */
  *pointer++;

  /* SMB header */
  *pointer++ = 255;
  *pointer++ = 'S';
  *pointer++ = 'M';
  *pointer++ = 'B';

  /* SMB command */
  *pointer++ = 0x73;

  /* Error code bytes */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB flags */
  *pointer++ = 8;

  /* SMB flags2 */
  // *pointer++ = 1;	// NTLMV1
  // *pointer++ = 192;	// NTLMV1
  *pointer++ = 0x01;	// NTLMV2, extended security negotiation
  *pointer++ = 0xD8;	// NTLMV2, extended security negotiation

  /* Process ID High */
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB signature, have no idea why these values */
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  /* Reserved */
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB TID is 0 at this moment */
  *pointer++ = 0;
  *pointer++ = 0;

  /* PID, we'll take 19,19 = 19*256 + 19 = 4883  */
  *pointer++ = 0x13;
  *pointer++ = 0x13;

  /* UID is still 0 */
  *pointer++ = 0;
  *pointer++ = 0;

  /* MID is 2 */
  *pointer++ = 2;
  *pointer++ = 0;

  /* Word count */
  if (esn == 1)
    *pointer++ = 12;
  else
    *pointer++ = 13;

  /* Parameter words */
  *pointer++ = 255;	*pointer++ = 0;
  *pointer++ = 0;	*pointer++ = 0;
  *pointer++ = 255;	*pointer++ = 255;
  *pointer++ = 2;	*pointer++ = 0;
  *pointer++ = 1;	*pointer++ = 0; /* VC number */
  *pointer++ = 0;	*pointer++ = 0;
  *pointer++ = 0;	*pointer++ = 0;

  if (esn == 0)
  {
    *pointer++ = 24;	/* LM password length = 24 */	
    *pointer++ = 0;

    *pointer++ = 24;	/* NTLM password length = 24 */
    *pointer++ = 0;
  }
  else
  {
    /* Security BLOB length */
    *pointer++ = 0x42 + strlen(domain) + strlen(calling_name);		
    *pointer++ = 0;
  }

  *pointer++ = 0;	*pointer++ = 0;		// Reserved	
  *pointer++ = 0;	*pointer++ = 0;		// Reserved
  // Capabilities
  *pointer++ = 0x5C;	
  *pointer++ = 0;	// Extended security is turned on
  *pointer++ = 0;	
  *pointer++ = 0x80;
  // Capabilities end

  /* Byte count */
  smb_bct = pointer;
  pointer++;
  *pointer++ = 0;

  if (esn == 0)
  {
    lmResponse(lmresp, (unsigned char *) challenge, (char *) password);
    ntlmResponse(ntresp, (unsigned char *) challenge, (char *) password);
  
    copied   = str_copy(pointer, (char *) lmresp, 24);
    pointer += copied;

    copied   = str_copy(pointer, (char *) ntresp, 24);
    pointer += copied;

    /* Delimiter */
    *pointer++ = 0x00;

    copied   = unicode_copy(pointer, (char *) username);
    pointer += copied;

    /* Delimiter */
    *pointer++ = 0x00;
    *pointer++ = 0x00;

    copied   = unicode_copy(pointer, (char *) domain);
    pointer += copied;

    /* Delimiter */
    *pointer++ = 0x00;
    *pointer++ = 0x00;

    copied   = unicode_copy (pointer, (char *) "Unix");
    pointer += copied;

    /* Delimiter */
    *pointer++ = 0x00;
    *pointer++ = 0x00;

    copied   = unicode_copy(pointer, (char *) "Netmon");
    // copied   = unicode_copy(pointer, "Samba");
    pointer += copied;

    /* Delimiter */
    *pointer++ = 0x00;
    *pointer++ = 0x00;
  }
  else
  {
    *pointer++ = 0x60;	/* Security BLOB begins */

    *pointer++ = 0x40 + strlen(domain) + strlen(calling_name);

    /* GSS-API = SPNEGO */
    *pointer++ = 0x06;
    *pointer++ = 0x06;
    *pointer++ = 0x2B;
    *pointer++ = 0x06;
    *pointer++ = 0x01;
    *pointer++ = 0x05;
    *pointer++ = 0x05;
    *pointer++ = 0x02;

    /* SPNEGO - Simple Protected Negotiation */
    *pointer++ = 0xA0;
    *pointer++ = 0x36 + strlen(domain) + strlen(calling_name);

    /* negTokenInit */
    *pointer++ = 0x30;

    /* negTokenInit length */
    *pointer++ = 0x34 + strlen(domain) + strlen(calling_name);  
  
    *pointer++ = 0xA0;
    *pointer++ = 0x0E;
    *pointer++ = 0x30;
    *pointer++ = 0x0C;
    /* negTokenInit header finished */ 
  
    /* mechType, we are choosing NTLMSSP */
    *pointer++ = 0x06;
    *pointer++ = 0x0A;
    *pointer++ = 0x2B;
    *pointer++ = 0x06;
    *pointer++ = 0x01;
    *pointer++ = 0x04;
    *pointer++ = 0x01;
    *pointer++ = 0x82;
    *pointer++ = 0x37;
    *pointer++ = 0x02;
    *pointer++ = 0x02;
    *pointer++ = 0x0A;

    /* Separator? */      
    *pointer++ = 0xA2;
    *pointer++ = 0x22 + strlen(domain) + strlen(calling_name);
    *pointer++ = 0x04;
    *pointer++ = 0x20 + strlen(domain) + strlen(calling_name);
  
    /* mechToken, NTLMSSP identifier */
    *pointer++ = 0x4E;
    *pointer++ = 0x54;
    *pointer++ = 0x4C;
    *pointer++ = 0x4D;
    *pointer++ = 0x53;
    *pointer++ = 0x53;
    *pointer++ = 0x50;

    *pointer++ = 0x00; /* Delimiter */
  
    /* NTLM Message Type, Negotiate */
    *pointer++ = 0x01;
    *pointer++ = 0x00;
    *pointer++ = 0x00;
    *pointer++ = 0x00;

    /* NTLM flags? */  
    *pointer++ = 0x15;
    *pointer++ = 0x02;
    *pointer++ = 0x08;
    *pointer++ = 0x60;

    /* Domain name length and max length */
    *pointer++ = strlen(domain);
    *pointer++ = 0x00;
    *pointer++ = strlen(domain);
    *pointer++ = 0x00;

    /* Domain name position offset */  
    *pointer++ = 0x20;
    *pointer++ = 0x00;
    *pointer++ = 0x00;
    *pointer++ = 0x00;
  
    *pointer++ = strlen(calling_name);
    *pointer++ = 0x00;
    *pointer++ = strlen(calling_name);
    *pointer++ = 0x00;
  
    /* Calling name position offset */
    *pointer++ = 0x20 + strlen(domain);
    *pointer++ = 0x00;
    *pointer++ = 0x00;
    *pointer++ = 0x00;
  
    /* Calling workstation domain */
    copied   = str_copy(pointer, (char *) domain, strlen(domain));
    pointer += copied;

    /* Calling workstation name */
    copied   = str_copy(pointer, calling_name, strlen(calling_name));
    pointer += copied;

    /* This is ugly, but SMB wants it that way */
    /* If the domain name length + calling name length is even, */
    /* one additional padding zero should be added */
    if (((strlen(domain) + strlen(calling_name)) % 2) == 0)
      *pointer++ = 0x00;

    copied   = unicode_copy(pointer, (char *) "Unix");
    pointer += copied;

    /* Delimiter */
    *pointer++ = 0x00;
    *pointer++ = 0x00;

    copied   = unicode_copy(pointer, (char *) "Netmon");
    // copied   = unicode_copy(pointer, "Samba");
    pointer += copied;

    /* Delimiter */
    *pointer++ = 0x00;
    *pointer++ = 0x00;
  }

  /* SMB size */
  *(line+3) = pointer - line - 4;

  /* Byte number */
  *smb_bct  = pointer - smb_bct - 2;

  return pointer-line; 
}

/******************************************************************************
* 
* Name: prepare_37
* Description: assembles SMB block 37 (SMB command = 0x25)
* 
* Arguments: Buffer to store SMB block
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/
int prepare_37(unsigned char *line)
{
  unsigned char *pointer;
  static unsigned char data[] = {
0xff, 0x53, 0x4d, 0x42, 0x25, 0x00, 0x00, 0x00, 0x00, 0x08,
0x01, 0xc8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x64, 0x23, 0x64, 0x00,
0x05, 0x00, 0x0e, 0x13, 0x00, 0x00, 0x00, 0x00, 0x04, 0xe0,
0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x13, 0x00, 0x5a, 0x00, 0x00, 0x00, 0x6d, 0x00, 0x00,
0x00, 0x2e, 0x00, 0x00, 0x5c, 0x00, 0x50, 0x00, 0x49, 0x00,
0x50, 0x00, 0x45, 0x00, 0x5c, 0x00, 0x4c, 0x00, 0x41, 0x00,
0x4e, 0x00, 0x4d, 0x00, 0x41, 0x00, 0x4e, 0x00, 0x00, 0x00,
0x00, 0x00, 0x57, 0x72, 0x4c, 0x65, 0x68, 0x00, 0x42, 0x31,
0x33, 0x42, 0x57, 0x7a, 0x00, 0x01, 0x00, 0xe0, 0xff};

  pointer = line;

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  int size = 109;

  *pointer++ = size; // fixed size block

  memcpy ((void *) pointer, data, size);

  *(line + 28) = tid_l;
  *(line + 29) = tid_h;
  *(line + 30) = 0x13;
  *(line + 31) = 0x13;
  /* We got UID from 115 */
  *(line + 32) = uid_l;
  *(line + 33) = uid_h;

  //sign_smb (line + 4, flen, 4, line + 26, 109 - 22, signature);
  //memcpy ((void *) (line + 18), signature, 8);
  sign_smb_inplace(line, size, 2);


  return 113;
}

/******************************************************************************
* 
* Name: prepare_115_ESN
* Description: assembles SMB block 115, extended security negotiation (SMB command = 0x73)
* 
* Arguments: Buffer to store SMB block
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int prepare_115_ESN(char *line, const char *username, const char *password, const char *domain, 
                    char *challenge, char *targetInformation, unsigned char ti_len,
                    unsigned char *userid)
{
  char *pointer, *smb_bct;
  int copied;
  unsigned char lmresp[128], ntresp[128], nt2resp[128], ntlm2response[128], 
  		sessionkey[128], sessionkey_encrypted[16];
  char calling_name[64], cl_challenge[24];
  int len, i;
  unsigned char len_high, len_low;

  strcpy(calling_name, "NETMON");

  pointer = line;

  memset(pointer, '\0', 1024);

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB size will be calculated later */
  *pointer++;
  *pointer++;

  /* SMB header */
  *pointer++ = 255;
  *pointer++ = 'S';
  *pointer++ = 'M';
  *pointer++ = 'B';

  /* SMB command */
  *pointer++ = 0x73;

  /* Error code bytes */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB flags */
  *pointer++ = 8;

  /* SMB flags2 */
  // *pointer++ = 1;	// NTLMV1
  // *pointer++ = 192;	// NTLMV1
  *pointer++ = 0x05;	// NTLMV2, extended security negotiation
  *pointer++ = 0xC8;	// NTLMV2, extended security negotiation

  /* Process ID High */
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB signature, have no idea why these values */
  *pointer++ = 0x42;
  *pointer++ = 0x53;
  *pointer++ = 0x52;
  *pointer++ = 0x53;
  *pointer++ = 0x50;
  *pointer++ = 0x59;
  *pointer++ = 0x4c;
  *pointer++ = 0x20;

  /* Reserved */
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB TID is 0 at this moment */
  *pointer++ = 0;
  *pointer++ = 0;

  /* PID, we'll take 19,19 = 19*256 + 19 = 4883  */
  *pointer++ = 0x13;
  *pointer++ = 0x13;

  /* UID */
  *pointer++ = (unsigned char) *userid;
  *pointer++ = (unsigned char) *(userid+1);

  /* MID is 3 */
  *pointer++ = 3;
  *pointer++ = 0;

  /* Word count */
  *pointer++ = 12;

  /* Parameter words */
  *pointer++ = 255;	*pointer++ = 0;
  *pointer++ = 0;	*pointer++ = 0;
  *pointer++ = 255;	*pointer++ = 255;
  *pointer++ = 2;	*pointer++ = 0;
  *pointer++ = 1;	*pointer++ = 0; /* VC number */
  *pointer++ = 0;	*pointer++ = 0;
  *pointer++ = 0;	*pointer++ = 0;

  /* Security BLOB length */
  *pointer++ = 0x8C + 2 * (strlen(domain) + strlen(calling_name) + strlen(username));
  *pointer++ = 0;

  *pointer++ = 0;	*pointer++ = 0;		// Reserved	
  *pointer++ = 0;	*pointer++ = 0;		// Reserved
  // Capabilities
  *pointer++ = 0x5C;	
  *pointer++ = 0;	// Extended security is turned on
  *pointer++ = 0;	
  *pointer++ = 0x80;
  // Capabilities end

  /* Byte count */
  smb_bct = pointer;
  pointer++;
  *pointer++ = 0;

  memset(cl_challenge, 0, 24);
  strcpy(cl_challenge, "MARIO123");
  lmResponse(lmresp, (unsigned char *) challenge, (char *) password);
  ntlmResponse(ntresp, (unsigned char *) challenge, (char *) password);
  ntlm2sessionResponse(ntlm2response, (unsigned char *) challenge, (unsigned char *) cl_challenge, (unsigned char *) password);
  ntlmv2Response(nt2resp, (char *) domain, (char *) username, (char *) password, (unsigned char *) challenge, (unsigned char *) cl_challenge,
                 (unsigned char *) targetInformation, ti_len); 
  sessionkeyResponse(sessionkey, (unsigned char *) challenge, (char *) password, cl_challenge);

  for (i=0; i<16; i++) sessionkey_encrypted[i] = i+1;
  SamOEMhash(sessionkey_encrypted, sessionkey, 16);

/********* DEBUG 
  printf("Client challenge: ");
  for (i=0; i<8; i++)
    printf("%.2X ", (unsigned char) cl_challenge[i]); 
  printf("\n");
  printf("lm response: ");
  for (i=0; i<24; i++)
    printf("%.2X ", lmresp[i]); 
  printf("\n");
  printf("ntlm response: ");
  for (i=0; i<24; i++)
    printf("%.2X ", ntresp[i]); 
  printf("\n");
  printf("ntlm2 session response: ");
  for (i=0; i<24; i++)
    printf("%.2X ", ntlm2response[i]); 
  printf("\n");
  printf("ntlmv2 response: ");
  for (i=0; i<24; i++)
    printf("%.2X ", nt2resp[i]); 
  printf("\n");
  printf("sessionkey response: ");
  for (i=0; i<16; i++)
    printf("%.2X ", sessionkey[i]); 
  printf("\n");
  printf("sessionkey_encrypted response: ");
  for (i=0; i<16; i++)
    printf("%.2X ", sessionkey_encrypted[i]); 
  printf("\n");
DEBUG ENDS *************/

  /* Security BLOB begins */

  /* GSS_API */
  *pointer++ = 0xA1;
  *pointer++ = 0x81;
  /* Length */
  *pointer++ = 0x89 + 2 * (strlen(domain) + strlen(calling_name) + strlen(username));

  /* negTokenTarg */
  *pointer++ = 0x30;
  *pointer++ = 0x81;
  /* Length */
  *pointer++ = 0x86 + 2 * (strlen(domain) + strlen(calling_name) + strlen(username));

  *pointer++ = 0xA2;
  *pointer++ = 0x81;
  /* Length */
  *pointer++ = 0x83 + 2 * (strlen(domain) + strlen(calling_name) + strlen(username));

  *pointer++ = 0x04;

  /* negTokenTarg header finished */ 
  
  /* responseToken */
  *pointer++ = 0x81;
  *pointer++ = 0x80 + 2 * (strlen(domain) + strlen(calling_name) + strlen(username));

  /* NTLMSSP */

  /* NTLMSSP identifier */

  *pointer++ = 0x4E;
  *pointer++ = 0x54;
  *pointer++ = 0x4C;
  *pointer++ = 0x4D;
  *pointer++ = 0x53;
  *pointer++ = 0x53;
  *pointer++ = 0x50;
  *pointer++ = 0x00;

  /* NTLM Message Type */

  *pointer++ = 0x03;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  /* Lan Manager Response length = 24 */
  *pointer++ = 0x18;
  *pointer++ = 0x00;
  /* Lan Manager Response max length = 24 */
  *pointer++ = 0x18;
  *pointer++ = 0x00;
  /* Lan Manager offset = 64 */
  *pointer++ = 0x40;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  /* NTLM Response length = 24 */
  *pointer++ = 0x18;
  *pointer++ = 0x00;
  /* NTLM Response max length = 24 */
  *pointer++ = 0x18;
  *pointer++ = 0x00;
  /* NTLM offset = 88 */
  *pointer++ = 0x58;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  /* Domain name length */
  *pointer++ = (unsigned char) 2 * strlen(domain);
  *pointer++ = 0x00;
  /* Domain name max length */
  *pointer++ = (unsigned char) 2 * strlen(domain);
  *pointer++ = 0x00;
  /* Domain name offset */
  *pointer++ = 0x70;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  /* User name length */
  *pointer++ = (unsigned char) 2 * strlen(username);
  *pointer++ = 0x00;
  /* User name max length */
  *pointer++ = (unsigned char) 2 * strlen(username);
  *pointer++ = 0x00;
  /* User name offset */
  *pointer++ = 0x70 + 2 * strlen(domain);
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  /* Host name length (hostname = NETMON) */
  *pointer++ = 0x0C;
  *pointer++ = 0x00;
  /* Host name max length */
  *pointer++ = 0x0C;
  *pointer++ = 0x00;
  /* Host name offset */
  *pointer++ = 0x70 + 2 * (strlen(username) + strlen(domain));
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  /* Session key length = 16 */
  *pointer++ = 0x10;
  *pointer++ = 0x00;
  /* Session key length = 16 */
  *pointer++ = 0x10;
  *pointer++ = 0x00;
  /* Session key offset */
  *pointer++ = 0x70 + 2 * (strlen(username) + strlen(domain) + strlen("NETMON"));
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  /* NTLM flags? */  
  *pointer++ = 0x15;
  *pointer++ = 0x02;
  *pointer++ = 0x08;
  *pointer++ = 0x60;

  /* Lan Manager Response */

  copied   = str_copy(pointer, cl_challenge, 24);
  pointer += copied;

  /* NTLM Manager Response */

  copied   = str_copy(pointer, (char *) ntlm2response, 24);
  pointer += copied;

  /* Domain name */

  copied   = unicode_copy(pointer, (char *) domain);
  pointer += copied;

  /* User name */

  copied   = unicode_copy(pointer, (char *) username);
  pointer += copied;

  /* Calling machine name */

  copied   = unicode_copy(pointer, calling_name);
  pointer += copied;

  /* Session key */

  copied   = str_copy(pointer, (char *) sessionkey_encrypted, 16);
  pointer += copied;
  
  /* Delimiter */
  *pointer++ = 0x00;

  copied   = unicode_copy(pointer, (char *) "Unix");
  pointer += copied;

  /* Delimiter */
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  copied   = unicode_copy(pointer, (char *) "Netmon");
  // copied   = unicode_copy(pointer, "Samba");
  pointer += copied;

  /* Delimiter */
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  /* SMB size */
  len = pointer - line - 4;
  len_high = (unsigned char) (len / 256);
  len_low  = (unsigned char) (len - len_high * 256);
  *(line+2) = (unsigned char) len_high;
  *(line+3) = (unsigned char) len_low;

  // printf("SMB size = %d\n", pointer - line - 4);

  /* Byte number */
  *smb_bct  = pointer - smb_bct - 2;

  return pointer-line; 
}


/******************************************************************************
* 
* Name: prepare_117
* Description: assembles SMB block 117 (SMB command = 0x75)
* 
* Arguments: Buffer to store SMB block
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int prepare_117(unsigned char *line, const char *share)
{
  unsigned char *pointer, *smb_bct, *smb_bct_r, *spointer;
  int firstbitslen, restofpacketlen;
  unsigned char firstbits[32], *fb_pointer;
  unsigned char restofpacket[1024], *rp_pointer;

  pointer = line;

  memset(pointer, '\0', 1024);

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB size, we'll handle this later */
  pointer++;

  fb_pointer = firstbits;

  /* SMB header */
  *pointer++ = 255;	*fb_pointer++ = 255;	
  *pointer++ = 'S';	*fb_pointer++ = 'S';
  *pointer++ = 'M';	*fb_pointer++ = 'M';
  *pointer++ = 'B';	*fb_pointer++ = 'B';

  /* SMB command */
  *pointer++ = 0x75;	*fb_pointer++ = 0x75;

  /* Error code bytes */
  *pointer++ = 0;	*fb_pointer++ = 0;
  *pointer++ = 0;	*fb_pointer++ = 0;
  *pointer++ = 0;	*fb_pointer++ = 0;
  *pointer++ = 0;	*fb_pointer++ = 0;

  /* SMB flags */
  *pointer++ = 8;	*fb_pointer++ = 8;

  /* SMB flags2 */
  *pointer++ = 1;	*fb_pointer++ = 1;
  *pointer++ = 192;	*fb_pointer++ = 192;

  /* Skip IP Process */
  *pointer++ = 0;	*fb_pointer++ = 0;
  *pointer++ = 0;	*fb_pointer++ = 0;

  firstbitslen = fb_pointer - firstbits;

  /* Skip SMB signature for now, we will update this later */
  spointer = pointer;
  pointer += 8;

  rp_pointer = restofpacket;

  /* Skip IPX reserved area */
  *pointer++ = 0;	*rp_pointer++ = 0;
  *pointer++ = 0;	*rp_pointer++ = 0;

  /* SMB TID is 0 at this moment */
  *pointer++ = 0;	*rp_pointer++ = 0;
  *pointer++ = 0;	*rp_pointer++ = 0;

  /* PID doesn't really matter for us, we'll assume 19*256 + 19 */
  *pointer++ = 19;	*rp_pointer++ = 19;
  *pointer++ = 19;	*rp_pointer++ = 19;

  /* We got UID from 115 */
  *pointer++ = uid_l;	*rp_pointer++ = uid_l;
  *pointer++ = uid_h;	*rp_pointer++ = uid_h;

  /* MID is 1 , only one thread */
  *pointer++ = 4;	*rp_pointer++ = 4;
  *pointer++ = 0;	*rp_pointer++ = 0;

  /* Word count */
  *pointer++ = 4;	*rp_pointer++ = 4;

  /* Parameter words */
  *pointer++ = 255;	*pointer++ = 0;	*rp_pointer++ = 255;	*rp_pointer++ = 0;
  *pointer++ = 0;	*pointer++ = 0;	*rp_pointer++ = 0;	*rp_pointer++ = 0;
  *pointer++ = 0;	*pointer++ = 0;	*rp_pointer++ = 0;	*rp_pointer++ = 0;
  *pointer++ = 1;	*pointer++ = 0;	*rp_pointer++ = 1;	*rp_pointer++ = 0;

  /* Byte count */
  smb_bct = pointer;	smb_bct_r = rp_pointer;
  pointer++;		rp_pointer++;
  *pointer++ = 0;	*rp_pointer++ = 0;	

  /* Delimiter */
  *pointer++ = 0x00;	*rp_pointer++ = 0x00;

  /* Share name */

  char fsn[300];
  fsn[0] = '\\';
  fsn[1] = '\0';
  fsn[2] = '\\';
  fsn[3] = '\0';
  int pos = 4;

  char* fsnp;

  fsnp = ((char*)fsn + pos);
  memcpy(fsnp, (char*)share_hostname.data(), share_hostname.length());
  pos += share_hostname.length();
  fsn[pos++] = '\\';
  fsn[pos++] = '\0';
  fsnp = ((char*)fsn + pos);
  int len = unicode_copy((char *) fsnp, (char *) share);
  pos += len;

  memcpy((char*)pointer, (char*)fsn, pos);
  pointer += pos;
  memcpy((char*)rp_pointer, (char*)fsn, pos);
  rp_pointer += pos;

  /* Delimiter */
  *pointer++ = 0x00;	*rp_pointer++ = 0x00;
  *pointer++ = 0x00;	*rp_pointer++ = 0x00;

  /* Have no idea why this is necessary, but it is */
  *pointer++ = 0x3F;	*rp_pointer++ = 0x3F;
  *pointer++ = 0x3F;	*rp_pointer++ = 0x3F;
  *pointer++ = 0x3F;	*rp_pointer++ = 0x3F;
  *pointer++ = 0x3F;	*rp_pointer++ = 0x3F;
  *pointer++ = 0x3F;	*rp_pointer++ = 0x3F;

  /* The end */
  *pointer++ = 0x00;	*rp_pointer++ = 0x00;

  int size;
  *(line+3) = size = pointer - line - 4;

  /* Byte number */
  *smb_bct   = pointer - smb_bct - 2;
  *smb_bct_r = rp_pointer - smb_bct_r - 2;

  restofpacketlen = rp_pointer - restofpacket;

  //sign_smb(firstbits, firstbitslen, 2, restofpacket, restofpacketlen, signature);
  //memcpy(spointer, signature, 8);
  sign_smb_inplace(line, size, 2);



  return pointer-line; 
}

/******************************************************************************
* 
* Name: prepare_128
* Description: assembles SMB block 128 (SMB command = 0x80)
* 
* Arguments: Buffer to store SMB block
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int prepare_128(unsigned char *line)
{
  unsigned char *pointer, *spointer;
  int firstbitslen, restofpacketlen;
  unsigned char firstbits[32], *fb_pointer;
  unsigned char restofpacket[1024], *rp_pointer;

  pointer = line;

  memset(pointer, '\0', 1024);

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;

  /* SMB size, we'll handle this later */
  pointer++;

  fb_pointer = firstbits;

  /* SMB header */
  *pointer++ = 255;	*fb_pointer++ = 255;	
  *pointer++ = 'S';	*fb_pointer++ = 'S';
  *pointer++ = 'M';	*fb_pointer++ = 'M';
  *pointer++ = 'B';	*fb_pointer++ = 'B';

  /* SMB command */
  *pointer++ = 0x80;	*fb_pointer++ = 0x80;

  /* Error code bytes */
  *pointer++ = 0;	*fb_pointer++ = 0;
  *pointer++ = 0;	*fb_pointer++ = 0;
  *pointer++ = 0;	*fb_pointer++ = 0;
  *pointer++ = 0;	*fb_pointer++ = 0;

  /* SMB flags */
  *pointer++ = 8;	*fb_pointer++ = 8;

  /* SMB flags2 */
  *pointer++ = 0x05;	*fb_pointer++ = 0x05;
  *pointer++ = 0xC8;	*fb_pointer++ = 0xC8;

  /* Skip IP Process */
  *pointer++ = 0;	*fb_pointer++ = 0;
  *pointer++ = 0;	*fb_pointer++ = 0;

  firstbitslen = fb_pointer - firstbits;

  /* Skip SMB signature for now, we will update this later */
  spointer = pointer;
  pointer += 8;

  rp_pointer = restofpacket;

  /* Skip IPX reserved area */
  *pointer++ = 0;	*rp_pointer++ = 0;
  *pointer++ = 0;	*rp_pointer++ = 0;

  /* We got SMB TID from 117 */
  *pointer++ = tid_l;	*rp_pointer++ = tid_l;
  *pointer++ = tid_h;	*rp_pointer++ = tid_h;

  /* PID doesn't really matter for us, we'll assume 19*256 + 19 */
  *pointer++ = 19;	*rp_pointer++ = 19;
  *pointer++ = 19;	*rp_pointer++ = 19;

  /* We got UID from 115 */
  *pointer++ = uid_l;	*rp_pointer++ = uid_l;
  *pointer++ = uid_h;	*rp_pointer++ = uid_h;

  /* MID is 7, don't know why, but it is */
  *pointer++ = 0x07;	*rp_pointer++ = 0x07;
  *pointer++ = 0;	*rp_pointer++ = 0;

  /* Word count */
  *pointer++ = 0;	*rp_pointer++ = 0;

  /* Byte count */
  *pointer++ = 0;	*rp_pointer++ = 0;
  *pointer++ = 0;	*rp_pointer++ = 0;

  /* The end */
  *pointer++ = 0x00;	*rp_pointer++ = 0;

  int size;
  *(line+3) = size = pointer - line - 4;

  restofpacketlen = rp_pointer - restofpacket;

  //sign_smb(firstbits, firstbitslen, 4, restofpacket, restofpacketlen, signature);
  //memcpy(spointer, signature, 8);
  sign_smb_inplace(line, size, 4);

  return pointer-line; 
}


// OLD FAIL STUFF END

/******************************************************************************
* 
* Name:unicode_copy
* Description: makes an unicode string from ascii
* 
* Arguments: Buffer to store unicode string, source ascii string
* Returns: length in bytes of destination string
*
*****************************************************************************/

int unicode_copy(char *dest, char *src)
{
  int len, counter;
  char *dstposition, *srcposition;

  len = strlen(src);

  dstposition = dest;
  srcposition = src;
  for (counter=0; counter<len; counter++)
  {
    *dstposition++ = *srcposition++;
    *dstposition++ = '\0';
  }

  return 2*len;
}

/******************************************************************************
* 
* Name: str_copy
* Description: does the same as strcpy, except that return values is different
* 
* Arguments: destination string, source string
* Returns: length in bytes of destination string
*
*****************************************************************************/

int str_copy(char *dest, char *src, int len)
{
  int counter;
  char *dstposition, *srcposition;

  dstposition = dest;
  srcposition = src;
  for (counter = 0; counter < len; counter++)
    *dstposition++ = *srcposition++;

  return len;
}

/******************************************************************************
* 
* Name: analyze_128
* Description: gets number of total and avail blocks, block size
*              from received 128 (0x80) SMB block
* 
* Arguments: Buffer that contains received SMB block
* Returns: 0
*
*****************************************************************************/

int analyze_128(char *line, int *total, int *available, int *blocksize)
{
  *total     = lendian(*(line+37), *(line+38));
  *available = lendian(*(line+43), *(line+44)); 
  *blocksize = lendian(*(line+39), *(line+40)) * lendian(*(line+41), *(line+42));

  return 0;
}

/******************************************************************************
* 
* Name: analyze_114
* Description: gets various information from received SMB block 114
* 
* Arguments: Buffer that contains received SMB block
* Returns: 0
*
*****************************************************************************/

int analyze_114(char *line, char *challenge)
{
  struct negProtResponse *response;

  /* Word count is always 17 for 114 SMB command */
  /* Challenge/response key is always 8 */
  /* So, our position is 81 */

  char *start, *pointer;
  char flags2, a1, a2;
  char name[128], domain[128];
  unsigned char *challenge_pointer;

  memset(name,   0, 128);
  memset(domain, 0, 128);

  /* Get the lower byte of flags2 */
  flags2 = *(line+14);

  if (flags2 & 0x01)
  {
    // printf("Unicode supported by server.\n");
    unicode = 1; 
  }
  else
  {
    // printf("Unicode not supported by server.\n");
    unicode = 0;
  }

  flags2 = *(line+15);

/*  if (flags2 & 0x08)
  {
    // printf("Extended security negotiation supported by server.\n");
    esn = 1;
  }
  else
  {
    // printf("Extended security negotiation not supported by server.\n");
    esn = 0;
  }
*/

  /* Word count is always on position 36, words start at 37 */
  response = (struct negProtResponse*)(line + 37);

/*
  printf("DialectIndex = %u\n", response->DialectIndex);
  printf("SecurityMode = %u\n", response->SecurityMode);
  printf("MaxMpxCount  = %hd\n", response->MaxMpxCount);
  printf("MaxNumberVCs = %hd\n", response->MaxNumberVCs);
  printf("Capabilities1 = 0x%x\n", response->Capabilities1);
  printf("Capabilities = 0x%x\n", response->SessionKey2);
  printf("ServerTimeZone = %hd\n", response->ServerTimeZone);
  printf("EncryptionKeyLength = %u\n", response->EncryptionKeyLength);
*/

  if (response->SessionKey2 & 0x80000000)
    esn = 1;

  if (esn == 0)		// Prior to Extended security negotiation 
  {
    /* Go to start of Buffer area */
    start   = line + 81;

    pointer = start;

    a1 = *pointer;
    a2 = *(pointer+1);

    while ((a1 != 0) || (a2 != 0))
    {
      strncat(domain, pointer, 1);

      pointer += 2;
      a1 = *pointer;
      a2 = *(pointer+1);
    }
    strncat(domain, "\0", 1);

    pointer += 2;
    a1 = *pointer;
    a2 = *(pointer+1);

    while ((a1 != 0) || (a2 != 0))
    {
      strncat(name, pointer, 1);
  
      pointer += 2;
      a1 = *pointer;
      a2 = *(pointer+1);
    }
    strncat(name, "\0", 1);

    // printf("Computer name: %s\n", name);
    // printf("Domain   name: %s\n", domain);

    challenge_pointer = (unsigned char *) challenge;
    *challenge_pointer = (unsigned char) *(line+73); challenge_pointer++;
    *challenge_pointer = (unsigned char) *(line+74); challenge_pointer++;
    *challenge_pointer = (unsigned char) *(line+75); challenge_pointer++;
    *challenge_pointer = (unsigned char) *(line+76); challenge_pointer++;
    *challenge_pointer = (unsigned char) *(line+77); challenge_pointer++;
    *challenge_pointer = (unsigned char) *(line+78); challenge_pointer++;
    *challenge_pointer = (unsigned char) *(line+79); challenge_pointer++;
    *challenge_pointer = (unsigned char) *(line+80); challenge_pointer++;
    *challenge_pointer = (unsigned char) 0; challenge_pointer++;

    // printf("Challenge: ");
    //   for (i=0; i<8; i++)
    //         printf("%.2X ", (unsigned char) challenge[i]);
    // printf("\n");              
  }
  else		// Extended security negotiation
  {

  // do nothing for now
  }
              
  return 0;
}

/******************************************************************************
* 
* Name: analyze_115
* Description: gets various information from received SMB block 115, 
*              most importanly - challenge key
* 
* Arguments: Buffer that contains received SMB block
* Returns: 0
*
*****************************************************************************/

int analyze_115(char *line, char *challenge, unsigned char *userid,
		char *targetInformation, unsigned char *ti_len)
{
  unsigned char wordcount;
  unsigned char *pointer;

  pointer = (unsigned char *) line;

  // printf("----- 115 BEGINS ------\n");

  /* Skip to userid */
  pointer += 32;

  *userid =     *pointer++;
  *(userid+1) = *pointer++;

  /* skip multiplex */
  pointer += 2;

  wordcount   = *pointer++;

  // printf("Word count = %u\n", wordcount);

  /* skip andxcommand, reserved, andxoffset, action fields */
  pointer += 6;

  unsigned short bloblen = *(unsigned short *)pointer;
  pointer += 4;

  try
  {
    netmon::spnegoasn1 s (pointer, bloblen);
    s.parse();
    unsigned int size;
    netmon::uchar *addrlist = s.getAddressList (size);
    memcpy (targetInformation, addrlist, size);
    memcpy (challenge, s.getChallenge (), 8);
    share_hostname = std::string(s.getTargetName());
    *ti_len = size;
  }
  catch (netmon::asn1exception &e)
  {
    netmon::log::instance()->add(netmon::_eLogError, e.what ());
  }

  return 0;
}

/******************************************************************************
* 
* Name: lendian
* Description: assembles a short integer from 2 bytes
* 
* Arguments: lower byte, higher byte
* Returns: result (short integer)
*
*****************************************************************************/

unsigned short lendian(unsigned char first, unsigned char second)
{
  unsigned short firstbyte, secondbyte;
  unsigned short result;

  firstbyte  = first;
  secondbyte = second;

  result = firstbyte | (secondbyte << 8);

  return result;
}

/******************************************************************************
* 
* Name: int_lendian
* Description: assembles an integer from 4 bytes
* 
* Arguments: 4 bytes, starting from lowest to the highest
* Returns: result (integer)
*
*****************************************************************************/

int int_lendian(unsigned char first, unsigned char second, unsigned third, unsigned forth)
{
  unsigned int firstbyte, secondbyte, thirdbyte, forthbyte;
  unsigned int result;

  firstbyte  = first;
  secondbyte = second;
  thirdbyte  = third;
  forthbyte  = forth;

  result = firstbyte | (secondbyte << 8) | (thirdbyte << 16) | (forthbyte << 24);

  return result;
}

/******************************************************************************
* 
* Name: check_error
* Description: checks if received packet is an SMB packet, and if it's 
*              status is OK
* 
* Arguments: Buffer that contains an SMB packet
* Returns: 0 if no error, -1 otherwise
*
*****************************************************************************/
          
int check_error(char *line, char *error_message, int port)
{
  int status;
  char smb_error[SMBERRMESSAGELEN];

  unsigned char err_1, err_2, err_3, err_4;
  unsigned char s, m, b;

  s = *(line+5);
  m = *(line+6);
  b = *(line+7);

  if ((s != 'S') || (m != 'M') || (b != 'B'))
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error, non-SMB packet received.", port);
    return -1;
  }

  err_1 = *(line+9);
  err_2 = *(line+10);
  err_3 = *(line+11);
  err_4 = *(line+12);

  status = int_lendian(err_1, err_2, err_3, err_4);

  if (status != 0)
  {
    if (getsmberror(smb_error, status) == 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "%d: SMB Error, code = %X\n",
                          port,
                          status);
    } else {
      netmon::log::instance()->add(netmon::_eLogError, "%d: SMB Error, code = %X (%s)\n",
                          port,
                          status,
                          smb_error);
    }
    return status;

#ifdef DEBUG
    printf("%d: Error code = (%d,%d,%d,%d).\n", 
                         port,
                         (unsigned int)err_1, (unsigned int) err_2,
                         (unsigned int)err_3, (unsigned int) err_4);
#endif
  }

  return 0;
}

/******************************************************************************
* 
* Name: NB_Encode
* Description: encodes string to NetBIOS preffered shape
* 
* Arguments: destination string, source string
* Returns: pointer to destination string
*
*****************************************************************************/
 
unsigned char *NB_Encode( unsigned char *dst, unsigned char *name )
  {
  int i = 0;
  int j = 0;

  /* Encode the name. */
  while( ('\0' != name[i]) && (i < 16) )
    {
    dst[j++] = 'A' + ((name[i] & 0xF0) >> 4);
    dst[j++] = 'A' + (name[i++] & 0x0F);
    }

  /* Encode the padding bytes. */
  while( j < 32 )
    {
    dst[j++] = 'C';
    dst[j++] = 'A';
    }

  /* Terminate the string. */
  dst[32] = '\0';
  return( dst );

} /* NB_Encode */

/******************************************************************************
* 
* Name: prepare_nbt_129
* Description: assembles Netbios packet 129 (command = 0x81)
* 
* Arguments: Buffer to store NetBIOS packet
* Returns:   size of packet
*
*****************************************************************************/

int prepare_nbt_129(char *line, const char *server_name)
{
  char *pointer;
  char called_name[128], calling_name[128];

  pointer = line;

  /* Type */
  *pointer++ = 129;

  /* Flags */
  *pointer++ = 0;

  /* Length - to be determined later */
  *pointer++ = 0;
  pointer++;

  NB_Encode((unsigned char *) called_name, (unsigned char *) server_name);
  NB_Encode((unsigned char *) calling_name, (unsigned char *) "NETMON");

  *pointer++ = 0x20;

  strcpy(pointer, called_name);
  pointer += strlen(called_name) + 1;

  *pointer++ = 0x20;

  strcpy(pointer, calling_name);
  pointer += strlen(calling_name) + 1;

  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;

  *(line+3) = pointer - line - 4;

  return pointer - line;
}

/******************************************************************************
* 
* Name: check_error_nbt
* Description: checks if received packet is a nbt response we expect
* 
* Arguments: Buffer that contains an NetBIOS packet
* Returns: 0 if no error, -1 otherwise
*
*****************************************************************************/

int check_error_nbt(char *line, char *error_message)
{
  /* Positive session response */

  if ((unsigned char)*(line) == 0x82)
    return 0;
  /* No error */

  /* Negative session response */
  if ((unsigned char)*(line) == 0x83)
  {
    netmon::log::instance()->add(netmon::_eLogInfo, "139: Negative session response, error code = %d.\n", (unsigned char)*(line+4)); 
    return -1;
  } 

  /* Retarget session response */
  if ((unsigned char)*(line) == 0x84)
  {
    sprintf(error_message, "139: Error: Netmon does not support retargeting.\n");
    return -1;
  }

  sprintf(error_message, "139: Error: Unknown session request response: %d.\n", (unsigned char)*(line));
  return -1;
}

static int smbconnect (const char *smbhost, int port)
{
  int                  sockfd;
  struct sockaddr_in   serv_addr;

  /* Fill the structure serv_addr with the actuall server address */

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family      = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(smbhost);
  serv_addr.sin_port        = htons(port);

  /* Open a TCP socket */
  if ((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Could not allocate socket. ", port);
    return -1;
  }

  int on = 1;
  setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (const char *)&on, sizeof(int));
  if(sockfd == -1)
  {
    perror("setsockopt");
    return -1;
  }

  /* Connect to the SMB server */
  if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogWarning, "%d: Cannot connect to SMB server. ", port);

    /* Close socket */
    close(sockfd);

    return -2;
  }
  return sockfd;
}

int establishNBSession (int &sockfd, const char *name, char *error_message)
{
  char line[MAXLINE*2];
  int nbt_len;
  int byteswritten;
  std::string srv(name);

  for (int i = 0; i < 3; ++i)
  {
    if (i == 1)
    {
      size_t pos = srv.find ('.');
      if (pos != std::string::npos)
        srv = std::string (srv, 0, pos);
      else
        srv = std::string ("*SMBSERVER");
    }
    if (i == 2)
      srv = std::string ("*SMBSERVER");

    memset(line, '\0', MAXLINE);

    /* 0x81 = SESSION REQUEST */
    nbt_len = prepare_nbt_129(line, srv.c_str());

    if ((byteswritten = write(sockfd, line, nbt_len)) != nbt_len)
    {
      sprintf(error_message, "139: Error while writing to SMB server (while sending NBT_129).\
                         Number of bytes sent: %d", byteswritten);
      return -1;
    }

    memset(line, '\0', MAXLINE);
    if ((byteswritten = read(sockfd, line, MAXLINE)) < 0)
    {
      netmon::log::instance()->add(netmon::_eLogWarning, "139: SMB server not responding. ");
      return -1;
    }

    if (check_error_nbt(line, error_message) != 0)
    {
      netmon::log::instance()->add(netmon::_eLogWarning, "139: Could not establish netbios session. ");
      close (sockfd);
      sockfd = smbconnect (name, 139);
      if (sockfd < 0)
        return -1;
      continue;
    }
    return 0;
  }
  return -1;
}

/******************************************************************************
* 
* Name: getspace
* Description: gets disk attributes on remote SMB server
* 
* Arguments: SMB host, port number, share name, username, password,
*            server name, domain name
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int getspace(const char *smbhost, int port, const char *share, 
             const char *username, const char *password, 
             const char *servername, const char *domain,
             int *total, int *available, int *blocksize,
             char *error_message)
{
  int                  sockfd, byteswritten, smb_len, status;
  char                 line[MAXLINE];
  char		       challenge[16];
  char		       targetInformation[2048];
  unsigned char	       ti_len, userid[2];  

  sockfd = smbconnect (smbhost, port);
  if (sockfd < 0)
  {
    sprintf (error_message, "Cannot connect to host");
    return -1;
  }

  /* If SMB is going over NetBIOS, establish session */
  if (port == 139)
  {
    if (establishNBSession (sockfd, smbhost, error_message) < 0)
    {
      close (sockfd);
      return -2;
    }
  } /* NetBIOS session */

  memset(line, '\0', MAXLINE);
  smb_len = prepare_114(line);

  if ((byteswritten = write(sockfd, line, smb_len)) != smb_len)
  {
    sprintf(error_message, "%d: Error while writing to SMB server (while sending 114).\
                         Number of bytes sent: %d", port, byteswritten);
  
    /* Close socket */
    close(sockfd);
  
    return 6;
  }

  /* Hey, I was friendly, let's see what you have to say */

  memset(line, '\0', MAXLINE);
  if (read(sockfd, line, MAXLINE) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogWarning, "%d: SMB server not responding. ", port);

    /* Close socket */
    close(sockfd);
  
    return 7;
  }

  if (check_error(line, error_message, port) != 0)
  {
    netmon::log::instance()->add(netmon::_eLogWarning, "%d, SMB Protocol negotiation failed. ", port);

    /* Close socket */
    close(sockfd);

    return 8;
  } 

  /* This is convenient for debugging, but we don't really need it */
  analyze_114(line, challenge);

  memset(line, '\0', MAXLINE);
  smb_len = prepare_115(line, username, password, domain, challenge);

  if ((byteswritten = write(sockfd, line, smb_len)) != smb_len)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 115).\
                         Number of bytes sent: %d", port, byteswritten);

    /* Close socket */
    close(sockfd);
  
    return 9;
  }

  memset(line, '\0', MAXLINE);
  if (read(sockfd, line, MAXLINE) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

    /* Close socket */
    close(sockfd);
  
    return 10;
  }

  status = check_error(line, error_message, port);
  if ((status != 0) && ((unsigned) status != 0xC0000016))
  {
    netmon::log::instance()->add(netmon::_eLogWarning, "%d: Login failed. ", port);

    /* Close socket */
    close(sockfd);

    return 11;
  }
  if ((unsigned) status == 0xC0000016)
  {
    /* 0xC0000016 = MORE_PROCESSING_REQUIRED, extended security negotiation */

    analyze_115(line, challenge, userid, targetInformation, &ti_len);

// DEBUG
/*
    challenge[0] = 0xfd;
    challenge[1] = 0x7a;  
    challenge[2] = 0x63;  
    challenge[3] = 0xed;  
    challenge[4] = 0xde;
    challenge[5] = 0xcc;  
    challenge[6] = 0xdc;  
    challenge[7] = 0x7c;
*/
  
    memset(line, '\0', MAXLINE);

    smb_len = prepare_115_ESN(line, username, password, domain, challenge, 
                              targetInformation, ti_len, userid);

    if ((byteswritten = write(sockfd, line, smb_len)) != smb_len)
    {
      netmon::log::instance()->add(netmon::_eLogWarning, "%d: Error while writing to SMB server (while sending 115).\
                           Number of bytes sent: %d", port, byteswritten);

      /* Close socket */
      close(sockfd);
  
      return 9;
    }

    memset(line, '\0', MAXLINE);
    if (read(sockfd, line, MAXLINE) < 0)
    {
      netmon::log::instance()->add(netmon::_eLogWarning, "%d: SMB server not responding. ", port);

      /* Close socket */
      close(sockfd);
  
      return 10;
    }

    status = check_error(line, error_message, port);
    if (status != 0)
    {
      netmon::log::instance()->add(netmon::_eLogWarning, "%d: Login failed. ", port);

      /* Close socket */
      close(sockfd);

      return 11;
    }
  }

  /* OK, now we have authenticated user id */
  uid_l = *(line+32);
  uid_h = *(line+33);

  memset(line, '\0', MAXLINE);
  share_name = std::string(share);
  smb_len = prepare_117((unsigned char *) line, share);

  if (write(sockfd, line, smb_len) != smb_len)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 117). ", port);

    /* Close socket */
    close(sockfd);
  
    return 12;
  }

  memset(line, '\0', MAXLINE);
  if (read(sockfd, line, MAXLINE) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

    /* Close socket */
    close(sockfd);
  
    return 13;
  }

  if (check_error(line, error_message, port) != 0)
  {
    netmon::log::instance()->add(netmon::_eLogWarning, "%d: Attaching to share failed. ", port);

    /* Close socket */
    close(sockfd);

    return 14;
  }
    
  /* OK, now we have tree id */
  tid_l = *(line+28);
  tid_h = *(line+29);

  memset(line, '\0', MAXLINE);
  smb_len = prepare_128((unsigned char *) line);

  if (write(sockfd, line, smb_len) != smb_len)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 128). ", port);

    /* Close socket */
    close(sockfd);
  
    return 15;
  }

  memset(line, '\0', MAXLINE);
  if (read(sockfd, line, MAXLINE) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d, SMB server not responding. ", port);
  
    /* Close socket */
    close(sockfd);
  
    return 16;
  }

  if (check_error(line, error_message, port) != 0)
  {
    netmon::log::instance()->add(netmon::_eLogWarning, "%d: Getting disk attributes failed. ", port);

    /* Close socket */
    close(sockfd);

    return 17;
  }


  analyze_128(line, total, available, blocksize);

  /* Disconnect */
  /* Close socket */

  close(sockfd);

  /* Everything went fine if we came to this point, return 0 */

  return 0;
}


int getshares (const char *smbhost, int port, 
             const char *username, const char *password, 
             const char *servername, const char *domain,
             std::list<std::string> &list,
             char *error_message)
{
  int                  sockfd, byteswritten, smb_len, status;
  struct sockaddr_in   serv_addr;
  char                 line[MAXLINE];
  char		       challenge[16];
  char		       targetInformation[2048];
  unsigned char	       ti_len, userid[2];  
    
  /* Fill the structure serv_addr with the actuall server address */

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family      = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(smbhost);
  serv_addr.sin_port        = htons(port);
        
  /* Open a TCP socket */
  if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Could not allocate socket. ", port);
    #ifdef DEBUG
      std::cout << "Couldn't allocate socket" << std::endl;
    #endif
    return -1;
  }

  /* Connect to the SMB server */
  if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Cannot connect to SMB server. ", port);
  
    /* Close socket */
    close(sockfd);

    #ifdef DEBUG
      std::cout << "Couldn't connect" << std::endl;
    #endif

    return -2;
  }

  /* If SMB is going over NetBIOS, establish session */
  if (port == 139)
  {
    int nbt_len;

    memset(line, '\0', MAXLINE);
  
    /* 0x81 = SESSION REQUEST */
    nbt_len = prepare_nbt_129(line, servername);
  
    if ((byteswritten = write(sockfd, line, nbt_len)) != nbt_len)
    {
      sprintf(error_message, "%d: Error while writing to SMB server (while sending NBT_129).\
                         Number of bytes sent: %d", port, byteswritten);
  
      /* Close socket */
      close(sockfd);
  
      #ifdef DEBUG
        std::cout << "Error writing to SMB Server" << std::endl;
      #endif
      return 3;
    }

    memset(line, '\0', MAXLINE);
    if (read(sockfd, line, MAXLINE) < 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

      /* Close socket */
      close(sockfd);

      #ifdef DEBUG
        std::cout << "SMB Server Not Responding" << std::endl;
      #endif
  
      return 4;
    }

    if (check_error_nbt(line, error_message) != 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "%d: Could not establish netbios session. ", port);
    
      /* Close socket */
      close(sockfd);

      #ifdef DEBUG
        std::cout << "Couldn't establish netbios session" << std::endl;
      #endif
    
      return 5;
    }
  
  } /* NetBIOS session */

  memset(line, '\0', MAXLINE);

  smb_len = prepare_114(line);

  if ((byteswritten = write(sockfd, line, smb_len)) != smb_len)
  {
    sprintf(error_message, "%d: Error while writing to SMB server (while sending 114).\
                         Number of bytes sent: %d", port, byteswritten);
  
    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "Error writing to SMB Server" << std::endl;
      #endif
  
    return 6;
  }

  /* Hey, I was friendly, let's see what you have to say */

  memset(line, '\0', MAXLINE);
  if (read(sockfd, line, MAXLINE) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "SMB Server not responding" << std::endl;
      #endif

  
    return 7;
  }

  if (check_error(line, error_message, port) != 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d, SMB Protocol negotiation failed. ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "SMB Proto negotiation failure" << std::endl;
      #endif

    return 8;
  } 

  /* This is convenient for debugging, but we don't really need it */
  analyze_114(line, challenge);

  memset(line, '\0', MAXLINE);
  smb_len = prepare_115(line, username, password, domain, challenge);

  if ((byteswritten = write(sockfd, line, smb_len)) != smb_len)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 115).\
                         Number of bytes sent: %d", port, byteswritten);

    /* Close socket */
    close(sockfd);
  

      #ifdef DEBUG
        std::cout << "Error writing to SMB Server" << std::endl;
      #endif

    return 9;
  }

  memset(line, '\0', MAXLINE);
  if (read(sockfd, line, MAXLINE) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "SMB Server not responding" << std::endl;
      #endif

    return 10;
  }

  status = check_error(line, error_message, port);
  if ((status != 0) && ((unsigned) status != 0xC0000016))
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Login failed. ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "Login failed" << std::endl;
      #endif

    return 11;
  }
  if ((unsigned) status == 0xC0000016)
  {
    /* 0xC0000016 = MORE_PROCESSING_REQUIRED, extended security negotiation */

    analyze_115(line, challenge, userid, targetInformation, &ti_len);

    memset(line, '\0', MAXLINE);

    smb_len = prepare_115_ESN(line, username, password, domain, challenge, 
                              targetInformation, ti_len, userid);

    if ((byteswritten = write(sockfd, line, smb_len)) != smb_len)
    {
      netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 115).\
                           Number of bytes sent: %d", port, byteswritten);

      /* Close socket */
      close(sockfd);

      #ifdef DEBUG
        std::cout << "Error writing to SMB Server" << std::endl;
      #endif
  
      return 9;
    }

    memset(line, '\0', MAXLINE);
    if (read(sockfd, line, MAXLINE) < 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

      /* Close socket */
      close(sockfd);

      #ifdef DEBUG
        std::cout << "SMB Server not Responding" << std::endl;
      #endif
  
      return 10;
    }

    status = check_error(line, error_message, port);
    if (status != 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "%d: Login failed. ", port);

      /* Close socket */
      close(sockfd);

      #ifdef DEBUG
        std::cout << "SMB Login failed" << std::endl;
      #endif


      return 11;
    }
  }

  /* OK, now we have authenticated user id */
  uid_l = *(line+32);
  uid_h = *(line+33);

  memset(line, '\0', MAXLINE);
  smb_len = prepare_andx((unsigned char *) line);

  if (write(sockfd, line, smb_len) != smb_len)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 117). ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "Error writing to SMB Server" << std::endl;
      #endif

  
    return 12;
  }

  memset(line, '\0', MAXLINE);
  if (read(sockfd, line, MAXLINE) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "SMB Server not responding" << std::endl;
      #endif
  
    return 13;
  }

  if (check_error(line, error_message, port) != 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Attaching to share failed. ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "Attaching to share failed" << std::endl;
      #endif

    return 14;
  }
    
  /* OK, now we have tree id */
  tid_l = *(line+28);
  tid_h = *(line+29);


  memset(line, '\0', MAXLINE);
  smb_len = prepare_treeconnect((unsigned char *) line);
  int wrote = write(sockfd, line, smb_len);

  if (wrote != smb_len)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 37). ", port);
    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "Error writing to SMB" << std::endl;
      #endif

    return 15;
  }

  memset(line, '\0', MAXLINE);
  int ret = 0;
  if ((ret = read(sockfd, line, MAXLINE)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d, SMB server not responding. ", port);
  
    /* Close socket */
    close(sockfd);
  
      #ifdef DEBUG
        std::cout << "SMB Not Responding" << std::endl;
      #endif

    return 16;
  }

  // Record the FID
  fid_l = *(line+42);
  fid_h = *(line+43);

  memset(line, '\0', MAXLINE);
  smb_len = prepare_bind((unsigned char *) line);

  if (write(sockfd, line, smb_len) != smb_len)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 117). ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "Error writing to SMB Server" << std::endl;
      #endif

  
    return 12;
  }

  memset(line, '\0', MAXLINE);
  if (read(sockfd, line, MAXLINE) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "SMB Server not responding" << std::endl;
      #endif
  
    return 13;
  }

  if (check_error(line, error_message, port) != 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Attaching to share failed. ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "Attaching to share failed" << std::endl;
      #endif

    return 14;
  }

    


  memset(line, '\0', MAXLINE);
  smb_len = prepare_netshare((unsigned char *) line);

  if (write(sockfd, line, smb_len) != smb_len)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 117). ", port);

    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "Error writing to SMB Server" << std::endl;
      #endif

  
    return 12;
  }

  memset(line, '\0', MAXLINE);

  int lanbytes = 0, totalbytes = 0;

  if ((lanbytes = read(sockfd, line, MAXLINE)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "SMB Read Error");
    close(sockfd);
    return 13;
  } else {
    totalbytes += lanbytes;
  }

  uint16_t total_size;
  memcpy(&total_size, (line+39), 2);

  while (totalbytes < total_size)
  {
    lanbytes = read(sockfd, &line[totalbytes], MAXLINE);
    if (lanbytes < 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "SMB Read Error");
      close(sockfd);
      return 13;
    }

    // Add to total bytes
    totalbytes += lanbytes;

    #ifdef DEBUG
      std::cout << "Read " << lanbytes << " bytes" << std::endl; 

      std::cout << "Total : " << totalbytes << std::endl;
    #endif
  }

  if (check_error(line, error_message, port) != 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Getting SMB shares failed. ", port);


    /* Close socket */
    close(sockfd);

      #ifdef DEBUG
        std::cout << "Getting SMB Shares failed!" << std::endl;
      #endif

    return 17;
  }

  /* Disconnect */
  /* Close socket */

  close(sockfd);

  analyzeLANMAN (line, totalbytes, list);


  /* Everything went fine if we came to this point, return 0 */

      #ifdef DEBUG
        std::cout << "Successful" << std::endl;
      #endif


  return 0;
}

// Convert little endian to host
uint32_t le2h(uint32_t data)
{
  uint32_t net = htonl(data);
  return (
    ((net << 24)  & 0xFF000000)|
    ((net << 8)  & 0x00FF0000) |
    ((net >> 8)  & 0x0000FF00) |
    ((net >> 24) & 0x000000FF)
  );
}

bool analyzeLANMAN (char *line, int size, std::list<std::string> &list)
{
  /* Superdebug
  for (int i=0; i < size; i++)
  {
    if (i % 4 == 0)
      printf("\n");
    printf("%.2X ", line[i]);
  }
  */

  // We can go ahead and skip 92 right to the referrant ID
  line +=  92; // Referrant ID

  // Next up is the count, number of share entries
  line +=   4; // Count
  uint32_t count;
  memcpy (&count, line, sizeof(uint32_t));

  // Move past first RID, max count
  line += 12;

  // Create an array of shareinfo structs to keep track of ish
  shareInfo shares[count];

  /* Read in RID, Type, which are the first things shown in the
  ** protocol
  */
  for (unsigned int i=0; i < count; i++)
  {
    
    /* The format of these is 3 4-byte packets:
    ** XX XX XX XX - RID of Share, fill in shareinfo
    ** XX XX XX XX - TYPE of Share, fill in shareinfo
    ** XX XX XX XX - RID Of comment, unnecessary 
    */

    // Read in the relevant share RID, jump ahead 32-bit int
    memcpy (&shares[i].share_rid, line, sizeof(uint32_t));
    line += sizeof(uint32_t);

    // Read in the relevant share type, jump ahead 32-bit int
    memcpy (&shares[i].type, line, sizeof(uint32_t));
    line += sizeof(uint32_t);

    // Read in the relevant comment RID, jump ahead 32-bit int
    memcpy (&shares[i].comment_rid, line, sizeof(uint32_t));
    line += sizeof(uint32_t);

  }

  for (unsigned int i=0; i < count; i++)
  {

    /***********  Share name handling ***********/

    // Skip 2 32-bit integers past max size & offset
    line += sizeof(uint32_t) * 2;

    memcpy(&shares[i].share_length, line, sizeof(uint32_t));

    // Move past size
    line += sizeof(uint32_t);

    // Copy the disk entry into string entry
    shares[i].name = readstring(line, shares[i].share_length);

    // Move the line on past the string
    line += shares[i].share_length * sizeof(uint16_t);

    // If the packet is odd-length, there is a 00 00 padding
    if (shares[i].share_length % 2 == 1)
      line += 2; // Padding.

    #ifdef DEBUG
      std::cout << "Share name length: " << shares[i].share_length << std::endl;
      std::cout << "Share name: " << shares[i].name << std::endl;
      std::cout << "Share name size: " << shares[i].name.length() << std::endl;
    #endif

    /********** Share comment handling *********/

    // Skip 2 32-bit integers past max size & offset
    line += sizeof(uint32_t) * 2;

    memcpy(&shares[i].comment_length, line, sizeof(uint32_t));

    // Move past size
    line += sizeof(uint32_t);

    // Copy the disk entry into string entry
    shares[i].comment = readstring(line, shares[i].comment_length);

    // Move the line on past the string
    line += shares[i].comment_length * sizeof(uint16_t);

    // If the packet is odd-length, there is a 00 00 padding
    if (shares[i].comment_length % 2 == 1)
      line += 2; // Padding.

    #ifdef DEBUG
      std::cout << "Share comment length: " << shares[i].comment_length << std::endl;
      std::cout << "Share comment" <<  shares[i].comment << std::endl;
    #endif

    /********** End comment handling ***********/

    // Push a share or a hidden one to our list
    if (shares[i].type == TYPE_SHARE || shares[i].type == TYPE_HIDDEN)
    {
      list.push_back(shares[i].name);
    }

  }
  return true;
}

// Reads a string from binary
std::string readstring(char* line, int size)
{
  std::string retval = "";
  char *ptr = line;
  for (int i=0; i < size; i++)
  {
    uint16_t character;
    memcpy(&character, ptr, sizeof(uint16_t));
    retval += (uint8_t) character;
    ptr += sizeof(uint16_t);
  }

  return retval;
}
