#ifndef __SPNEGO_ASN1__
#define __SPNEGO_ASN1__

#include "asn1.h"

#include <string>
#include <iostream>
#include <arpa/inet.h>

namespace netmon
{
  class spnegoasn1 : public asn1parser
  {
    public:
      spnegoasn1 (const uchar *, unsigned int);
      ~spnegoasn1 ();

      void parse ();

      uchar *getResponseToken () const
      {
        return m_responseToken;
      }

      unsigned int getResponseTokenSize () const
      {
        return m_rtSize;
      }

      std::string getOID () const
      {
        return m_oid;
      }

      uchar *getChallenge () const
      {
        if (m_responseToken == 0)
          return 0;
        if (m_rtSize < 24)
          return 0;
        return m_responseToken + 24;
      }

      std::string getTargetName() const;

      uchar *getAddressList (unsigned int &) const;

      short lendian_2(char *first, char *second) const
      {
        return *(first) | (*(second) << 8);
      }

    protected:
      uchar m_negResult;
      std::string m_oid;
      uchar *m_responseToken;
      uchar *m_mechList;
      unsigned int m_rtSize;
      unsigned int m_mlSize;
  };
}

#endif // __SPNEGO_ASN.1__

