#ifndef __NTLM__
#define __NTLM__

void hmac_md5(unsigned char *text, int text_len, 
	      unsigned char *key, int key_len, unsigned char *digest);
void lmResponse(unsigned char *response, unsigned char *challenge, 
                       char *password);
void ntlmResponse(unsigned char *response, unsigned char *challenge, char *password);

void ntlm2sessionResponse(unsigned char *response, 
                   unsigned char *challenge, 
                   unsigned char *cl_challenge, 
                   unsigned char *password);

void lmv2Response(unsigned char *response, char *domain, char *username, 
                  char *password, unsigned char *challenge, 
                  unsigned char *cl_challenge);

int ntlmv2Response(unsigned char *response, char *domain, char *username, 
                  char *password, unsigned char *challenge, 
                  unsigned char *cl_challenge, unsigned char *targetinformation,
                  int targetinformationlen);

void sessionkeyResponse(unsigned char *response, unsigned char *challenge, 
                        char *password, char *client_challenge);

void SamOEMhash( unsigned char *data, const unsigned char *key, int val);

int sign_smb(unsigned char *firstbits, int firstbitslen, unsigned char sequence,
             unsigned char *restofpacket, int restofpacketlen,
             unsigned char *destination);

int sign_smb_inplace (unsigned char* line, int size, int seq_num);



#endif // __NTLM__
