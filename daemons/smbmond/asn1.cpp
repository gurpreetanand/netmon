#include "asn1.h"

#include <cstring>
#include <sstream>

namespace netmon
{
  asn1parser::asn1parser (const uchar *data, unsigned int size)
  {
    init (data, size);
  }

  asn1parser::~asn1parser ()
  {
    delete[] m_data.m_data;
  }

  void asn1parser::init (const uchar *data, unsigned int size)
  {
    m_data.m_data = new uchar[size];
    memcpy (m_data.m_data, data, size);
    m_data.m_length = size;
    m_data.m_offset = 0;
    m_data.m_error = false;
  }

  void asn1parser::startTag (uchar tag)
  {
    uchar b;
    nesting mynesting;

    readuchar (b);

    if (b != tag)
    {
      m_data.m_error = true;
      throw asn1exception ("illegal tag");
    }

    readuchar (b);

    if (b & 0x80)
    {
      int n = b & 0x7f;
      readuchar (b);
      mynesting.m_taglen = b;
      while (n > 1)
      {
        readuchar (b);
        mynesting.m_taglen = (mynesting.m_taglen << 8) | b;
        n--;
      }
    }
    else
    {
      mynesting.m_taglen = b;
    }
    mynesting.m_start = m_data.m_offset;
    m_data.m_nesting.push_front (mynesting);
  }

  void asn1parser::endTag ()
  {
    if (tagRemaining () != 0)
    {
      m_data.m_error = true;
      throw asn1exception ("tag failuer");
    }
    m_data.m_nesting.pop_front ();
  }

  void asn1parser::readOID (std::string &oid)
  {
    uchar b;
    std::ostringstream oid_str;

    startTag (ASN1_OID);
    readuchar (b);

    oid_str << b / 40 << "." << b % 40 << ".";

    while (tagRemaining() > 0)
    {
      unsigned v = 0;
      do
      {
	readuchar (b);
	v = (v<<7) | (b&0x7f);
      }
      while (!m_data.m_error && b & 0x80);
      oid_str << v << ".";
    }

    endTag ();
    oid = std::string (oid_str.str().c_str(), oid_str.str().size() - 1);
  }

  uchar *asn1parser::readOctetStr (unsigned int &size)
  {
    unsigned int len;
    uchar *data;

    startTag (ASN1_OCTET_STRING);
    len = tagRemaining ();
    if (len < 0)
    {
      m_data.m_error = true;
      throw asn1exception ("illegal octet str");
    }
    data = new uchar[len];
    read ((void *)data, len);
    endTag ();
    size = len;
    return data;
  }

  int asn1parser::tagRemaining ()
  {
    if (m_data.m_error)
      return 0;
    if (m_data.m_nesting.size () == 0)
    {
      m_data.m_error = true;
      return -1;
    }
    return (m_data.m_nesting.front().m_taglen - (m_data.m_offset - m_data.m_nesting.front().m_start));
  }

  void asn1parser::read (void *p, unsigned int size)
  {
    if (m_data.m_offset + size > (unsigned) m_data.m_length)
      throw asn1exception ("illegal size");
    memcpy (p, m_data.m_data + m_data.m_offset, size);
    m_data.m_offset += size;
  }

  void asn1parser::readuchar (uchar &u8)
  {
    read ((void *) &u8, 1);
  }
}
