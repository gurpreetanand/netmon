#include "smbmonitor.h"
#include "smbalertmngr.h"
#include "db.h"
#include "getspace.h"
#include "log.h"

#include <errno.h>
#include <cstdlib>
#include <sstream>

namespace netmon
{
  smbmonitor::smbmonitor (db *mydb)
    : m_db(mydb)
  {
    resetFlags();
    m_alertManager = new smb_alert_manager (mydb);
    m_alertManager->init();
  }

  smbmonitor::~smbmonitor ()
  {
  }

  void smbmonitor::run ()
  {
    m_alertManager->reload();
    clearServerList();
    getServers();
    for (server_list::iterator i = m_list.begin(); i != m_list.end(); ++i)
      checkServer (*i);
  }

  void smbmonitor::createShareName (const std::string &ip, const std::string &share, std::string &ret)
  {
    std::string tmp("\\\\");
    tmp += ip;
    tmp += "\\";
    tmp += share;
    tmp += "\\";
    ret = tmp;
  }

  void smbmonitor::checkServer (server_data *d)
  {
    static char err[512];
    int total;
    int available;
    values_map vmap;

    setPendingFlag (d->m_id);

    vmap[alerthandler::eHostIP] = keyword_value (d->m_ip);
    vmap[alerthandler::eHostName] = keyword_value (d->m_serverName);
    vmap[alerthandler::ePartition] = keyword_value (d->m_share);

    int bs = 0; // blocksize
    int ret = getspace (d->m_ip.c_str(), 445, d->m_share.c_str(), d->m_user.c_str(), d->m_pass.c_str(), d->m_serverName.c_str(), d->m_domain.c_str(), &total, &available, &bs, err);
    if (ret != 0)
    {
      std::string tmp;
      createShareName (d->m_ip, d->m_share, tmp);
      ret = getspace (d->m_ip.c_str(), 445, tmp.c_str(), d->m_user.c_str(), d->m_pass.c_str(), d->m_serverName.c_str(), d->m_domain.c_str(), &total, &available, &bs, err);
    }
    if (ret != 0)
      ret = getspace (d->m_ip.c_str(), 139, d->m_share.c_str(), d->m_user.c_str(), d->m_pass.c_str(), d->m_serverName.c_str(), d->m_domain.c_str(), &total, &available, &bs, err);

    if (ret != 0)
    {
      if (ret < 0)
      {
        log::instance()->add(_eLogWarning, "Host %s, share %s, could not connect.",
                d->m_ip.c_str(), d->m_share.c_str());
      }
      else
      {
        log::instance()->add(_eLogWarning, "Host %s, share %s, connected, but error ocurred.",
                d->m_ip.c_str(), d->m_share.c_str());
      }

      updateDBFailure (d->m_id, err);
      if (d->m_status == -1) // this host was down before, just return
        return;
      vmap[alerthandler::eErrMsg] = keyword_value (err);
      vmap[alerthandler::eSysErr] = keyword_value (strerror (errno));
      m_alertManager->getDownAlerts (d->m_id, vmap);
    }
    else
    {
      int perc = (int) (((float)total - (float)available) * 100 / (float)total);

      updateDBSuccess (d->m_id, perc, total, available, bs);
      vmap[alerthandler::eNewStatus] = keyword_value (perc);
      m_alertManager->getAlerts (d->m_id, perc, vmap);
      if (d->m_status == -1) {
        m_alertManager->sendUp(d->m_id, vmap);
      }
    }
  }

  void smbmonitor::getServers ()
  {
    std::ostringstream sql;
    sql.str("");
    
    sql << "SELECT SRV_ID, IP, SHARE, USERNAME, PASSWORD, SERVERNAME, "
        << "DOMAIN, STATUS FROM SMB_SERVERS WHERE PENDING != 'Y' "
        << "AND (TIMESTAMP + INTERVAL) < " << (int) time(0);

    PGresult *res = m_db->execQuery (sql.str().c_str());
    for (int i = 0; i < PQntuples(res); ++i)
    {
      server_data *d = new server_data;
      d->m_id = atoi (PQgetvalue(res, i, 0));
      d->m_ip = std::string (PQgetvalue(res, i, 1));
      d->m_share = std::string (PQgetvalue(res, i, 2));
      d->m_user = std::string (PQgetvalue(res, i, 3));
      d->m_pass = std::string (PQgetvalue(res, i, 4));
      d->m_serverName = std::string (PQgetvalue(res, i, 5));
      d->m_domain = std::string (PQgetvalue(res, i, 6));
      d->m_status = atoi (PQgetvalue(res, i, 7));
      m_list.push_back (d);
    }
    PQclear (res);
  }

  void smbmonitor::resetFlags ()
  {
    m_db->execSQL ("UPDATE SMB_SERVERS SET MESSAGE = ' ', \
    TIMESTAMP = 0, TOTAL = 0, AVAILABLE = 0, BLOCKSIZE = 0, PENDING = 'N'");
  }

  void smbmonitor::setPendingFlag (unsigned int id)
  {
    std::ostringstream sql;
    sql.str("");
    sql << "UPDATE SMB_SERVERS SET PENDING = 'Y' WHERE SRV_ID = " << id;
    m_db->execSQL (sql.str().c_str());
  }

  void smbmonitor::updateDBFailure (unsigned int id, char *msg)
  {
    static std::ostringstream sql;
    sql.str("");
    sql << "UPDATE SMB_SERVERS SET STATUS = -1, TIMESTAMP = "
        << (int) time (0)
        << ", MESSAGE = '"
        << msg
        << "', PENDING = 'N' WHERE SRV_ID = "
        << id;

    m_db->execSQL (sql.str().c_str());

    sql.str("");
    sql << "INSERT INTO SMB_SERVER_LOG (SRV_ID, TOTAL, AVAILABLE, BLOCKSIZE, "
        << "TIMESTAMP) VALUES ("
        << id
        << ", -1, -1, -1, "
        << (int) time(0)
        << ")";

    m_db->execSQL (sql.str().c_str());
  }

  void smbmonitor::updateDBSuccess (int id, int perc, int total, int available, int bs)
  {
    static std::ostringstream sql;
    sql.str("");
    sql << "UPDATE SMB_SERVERS SET STATUS = "
        << perc
        << ", TIMESTAMP = "
        << (int) time (0)
        << ", TOTAL = "
        << total
        << ", AVAILABLE = "
        << available
        << ", MESSAGE = ' ', BLOCKSIZE = "
        << bs
        << ", PENDING = 'N' WHERE SRV_ID = "
        << id;

    m_db->execSQL (sql.str().c_str());

    sql.str("");
    sql << "INSERT INTO SMB_SERVER_LOG (SRV_ID, TOTAL, AVAILABLE, "
        << "BLOCKSIZE, TIMESTAMP) VALUES ("
        << id
        << ", "
        << total
        << ", "
        << available
        << ", "
        << bs
        << ", "
        << (int) time(0)
        << ")";

    m_db->execSQL (sql.str().c_str());
  }

  void smbmonitor::clearServerList ()
  {
    for (server_list::iterator i = m_list.begin(); i != m_list.end(); ++i)
      delete *i;
    m_list.clear();
  }
}
