#include "spnegoasn1.h"

namespace netmon
{
  spnegoasn1::spnegoasn1 (const uchar *buf, unsigned int size)
    : asn1parser (buf, size), m_responseToken (0), m_mechList (0)
  {
  }

  spnegoasn1::~spnegoasn1 ()
  {
    delete[] m_responseToken;
    delete[] m_mechList;
  }

  void spnegoasn1::parse ()
  {
    startTag (ASN1_CONTEXT(1));
    startTag (ASN1_SEQUENCE(0));

    while (!m_data.m_error && 0 < tagRemaining ())
    {
      switch (m_data.m_data[m_data.m_offset])
      {
        case ASN1_CONTEXT(0):
          startTag (ASN1_CONTEXT(0));
          startTag (ASN1_ENUMERATED);
          readuchar (m_negResult);
          endTag ();
          endTag ();
          break;
        case ASN1_CONTEXT(1):
          startTag (ASN1_CONTEXT(1));
          readOID (m_oid);
          endTag ();
          break;
        case ASN1_CONTEXT(2):
          startTag (ASN1_CONTEXT(2));
          m_responseToken = readOctetStr (m_rtSize);
          endTag ();
          break;
        case ASN1_CONTEXT(3):
          startTag(ASN1_CONTEXT(3));
          m_mechList = readOctetStr (m_mlSize);
          endTag ();
          break;
        default:
          m_data.m_error = true;
          break;
      }
    }

    endTag ();
    endTag ();
  }

      std::string spnegoasn1::getTargetName() const
      {
        if (m_responseToken == 0 || m_rtSize < 24)
          return std::string("");

        short len = *(unsigned char*)(m_responseToken + 12);
        short offset = *(unsigned char*)(m_responseToken + 16);
        #ifdef DEBUG
          std::cout << "Len: " << len << " offset: " << offset << std::endl;
          std::cout << std::string((const char*)(m_responseToken + offset), (size_t)len) << std::endl;
        #endif
        return std::string((const char*)(m_responseToken + offset), (size_t)len); 
      }


  uchar *spnegoasn1::getAddressList (unsigned int &size) const
  {
    if (m_responseToken == 0)
    {
      size = 0;
      return 0;
    }
    unsigned short len;
    len = *(unsigned short *)(m_responseToken + 40);
    size = len;
    unsigned int offset = *(unsigned int *)(m_responseToken + 44);
    return m_responseToken + offset;
  }
}

#ifdef __TEST__
#include <cstdlib>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

int main ()
{
  using namespace netmon;

  uchar *b;
  FILE *f;
  struct stat st;

  if (::stat ("spnego", &st) != 0)
  {
    perror ("stat");
    exit (-1);
  }

  printf ("size: %d\n", st.st_size);

  f = fopen ("spnego", "r");
  if (!f)
  {
    perror ("fopen");
    exit (-1);
  }

  b = new uchar[st.st_size];
  fread ((void *) b, st.st_size, 1, f);

  spnegoasn1 s (b, st.st_size);
  s.parse ();

  printf ("OID: %s\n", s.getOID().c_str());
  printf ("respToken size: %d\n", s.getResponseTokenSize ());

  FILE *out = fopen ("out", "w");
  if (!out)
  {
    perror ("fopen");
    exit (-1);
  }

  fwrite ((void *) s.getResponseToken (), s.getResponseTokenSize (), 1, out);
  fclose (out);

  out = fopen ("challenge", "w");
  fwrite ((void *) s.getChallenge (), 8, 1, out);
  fclose (out);

  out = fopen ("address", "w");
  unsigned int adrsize;
  uchar *size = s.getAddressList (adrsize);
  fwrite ((void *) size, adrsize, 1, out);
  fclose (out);

  delete[] b;
  return 0;
}

#endif // __TEST__
