#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <openssl/md4.h>
#include <openssl/md5.h>
#include <openssl/des.h>
#include <iostream>
#include <limits.h>

#define DESKEYARG(x) *x
#define DESKEY(x) &x

static void calc_resp(unsigned char *keys,
                      unsigned char *plaintext,
                      unsigned char *results);
static void setup_des_key(unsigned char *key_56,
                          DES_key_schedule DESKEYARG(ks));
void hmac_md5(unsigned char *text, int text_len, 
	      unsigned char *key, int key_len, unsigned char *digest);
void lmResponse(unsigned char *response, unsigned char *challenge, 
                       char *password);
void ntlmResponse(unsigned char *response, unsigned char *challenge, char *password);

void ntlm2sessionResponse(unsigned char *response, 
                   unsigned char *challenge, 
                   unsigned char *cl_challenge, 
                   unsigned char *password);

void lmv2Response(unsigned char *response, char *domain, char *username, 
                  char *password, unsigned char *challenge, 
                  unsigned char *cl_challenge);

int ntlmv2Response(unsigned char *response, char *domain, char *username, 
                  char *password, unsigned char *challenge, 
                  unsigned char *cl_challenge, unsigned char *targetinformation,
                  int targetinformationlen);

void sessionkeyResponse(unsigned char *response, unsigned char *challenge, 
                        char *password, char *client_challenge);

void lmResponse(unsigned char *response, unsigned char *challenge, char *password)
{
  unsigned char *pw;
  unsigned char lmbuffer[21], lmresp[24];

  static const unsigned char magic[] = {
    0x4B, 0x47, 0x53, 0x21, 0x40, 0x23, 0x24, 0x25
  };
  int i;
  int len = strlen(password);

  /* make it fit at least 14 bytes */
  pw = (unsigned char *) malloc (len < 7 ? 14 : len * 2);
  if(!pw)
    return; /* this will lead to a badly generated package */

  if (len > 14)
    len = 14;
  
  for (i=0; i<len; i++)
    pw  [i] = toupper(password[i]);

  for (; i<14; i++)
    pw  [i] = 0;

  {
    /* create LanManager hashed password */
    DES_key_schedule ks;

    setup_des_key(pw, DESKEY(ks));
    DES_ecb_encrypt((DES_cblock *)magic, (DES_cblock *)lmbuffer,
                    DESKEY(ks), DES_ENCRYPT);
  
    setup_des_key(pw+7, DESKEY(ks));
    DES_ecb_encrypt((DES_cblock *)magic, (DES_cblock *)(lmbuffer+8),
                    DESKEY(ks), DES_ENCRYPT);

    memset(lmbuffer+16, 0, 5);
  }

  /* create LM responses */
  calc_resp(lmbuffer, challenge, lmresp);

  for (i=0; i<24; i++)
    *(response + i) = lmresp[i];

  free(pw);
}

void ntlmResponse(unsigned char *response, unsigned char *challenge, char *password)
{
  unsigned char *pw;
  unsigned char ntbuffer[24], ntresp[24], user_session_key[24];
  int len, i;

  len = strlen(password);
  
  /* make it fit at least 14 bytes */
  pw = (unsigned char *) malloc ( len < 7 ? 14 : len * 2);
  if(!pw)
    return; /* this will lead to a badly generated package */  

  /*** create NT hashed password ***/
  MD4_CTX MD4;

  len = strlen(password);

  for (i=0; i<len; i++) {
    pw[2*i]   = password[i];
    pw[2*i+1] = 0;
  }

  MD4_Init(&MD4);
  MD4_Update(&MD4, pw, 2*len);
  MD4_Final(ntbuffer, &MD4);

  memset(ntbuffer+16, 0, 8);

  MD4_Init(&MD4);
  MD4_Update(&MD4, ntbuffer, 16);
  MD4_Final(user_session_key, &MD4);

  memset(user_session_key+16, 0, 8);

  calc_resp(ntbuffer, challenge, ntresp);

  for (i=0; i<24; i++)
    *(response + i) = ntresp[i];
  
  free(pw);
}

void ntlm2sessionResponse(unsigned char *response, 
                   unsigned char *challenge, 
                   unsigned char *cl_challenge, 
                   unsigned char *password)
{
  unsigned char nonce[16], sessionHash[16], ntbuffer[24];
  unsigned char pw[128];
  int i, len;
  
  for (i=0; i<8; i++)
    nonce[i] = *(challenge+i);
  for (i=0; i<8; i++)
    nonce[i+8] = *(cl_challenge+i);

  MD5_CTX MD5; 
   
  MD5_Init   (&MD5);
  MD5_Update (&MD5, nonce, 16); 
  MD5_Final  (sessionHash, &MD5);
  
  memset(sessionHash+8, 0, 24);
  
  len = strlen((const char *) password);
  
  for (i=0; i<len; i++)
  {
    pw[2*i] = *(password+i);
    pw[2*i+1] = 0;
  }

  MD4_CTX MD4;
  MD4_Init(&MD4);
  MD4_Update(&MD4, pw, 2*len);
  MD4_Final(ntbuffer, &MD4);
  memset(ntbuffer+16, 0, 8);

  calc_resp(ntbuffer, sessionHash, response);
  
}

void lmv2Response(unsigned char *response, char *domain, char *username, 
                  char *password, unsigned char *challenge, unsigned char *cl_challenge)
{
  unsigned char pw[128];
  unsigned char ntlm_password_hash[16], ntlmv2_hash[16];
  unsigned char userdomain[128], userdomainunicode[256];
  unsigned char bothchallenges[16];
  int i, len;

  MD4_CTX MD4;

  len = strlen(password);

  for (i=0; i<len; i++) {
    pw[2*i]   = *(password+i);
    pw[2*i+1] = 0;
  }

  MD4_Init(&MD4);
  MD4_Update(&MD4, pw, 2*len);
  MD4_Final(ntlm_password_hash, &MD4);

  strcpy ((char *) userdomain, username);
  strcat ((char *) userdomain, domain);
  
  for ( i = 0; i < (int) strlen ((const char *) userdomain); i++)
  {
    userdomainunicode [2*i] = toupper(userdomain[i]);
    userdomainunicode [2*i + 1] = 0;  
  }

  hmac_md5(userdomainunicode, 2 * strlen((const char *) userdomain), 
           ntlm_password_hash, 16, ntlmv2_hash);

  for (i=0; i<8; i++)
    bothchallenges[i] = challenge[i];
  for (i=0; i<8; i++)
    bothchallenges[i+8] = cl_challenge[i];

  hmac_md5(bothchallenges, 16, 
           ntlmv2_hash, 16, response);

  for (i=0; i<8; i++)
    *(response+i+16) = cl_challenge[i];
}


int ntlmv2Response(unsigned char *response, char *domain, char *username, 
                  char *password, unsigned char *challenge, 
                  unsigned char *cl_challenge, unsigned char *targetinformation,
                  int targetinformationlen)
{
  unsigned char pw[128];
  unsigned char ntlm_password_hash[16], ntlmv2_hash[16], ntlmv2[16];
  unsigned char userdomain[128], userdomainunicode[256];
  int i, len;
  static unsigned char blob[2048];
  unsigned char *pointer;
  long long timestamp;
  unsigned char *tspointer;

  pointer = blob;

  MD4_CTX MD4;

  len = strlen(password);

  for (i=0; i<len; i++) {
    pw[2*i]   = *(password+i);
    pw[2*i+1] = 0;
  }

  MD4_Init(&MD4);
  MD4_Update(&MD4, pw, 2*len);
  MD4_Final(ntlm_password_hash, &MD4);

  strcpy((char *) userdomain, username);
  strcat((char *) userdomain, domain);
  
  for (i=0; i < (int) strlen((const char *)userdomain); i++)
  {
    userdomainunicode [2*i] = toupper(userdomain[i]);
    userdomainunicode [2*i + 1] = 0;  
  }

  hmac_md5(userdomainunicode, 2 * strlen((const char *) userdomain), 
           ntlm_password_hash, 16, ntlmv2_hash);

  /* Start with BLOB construction */

  /* Server challenge */
    
  for (i=0; i<8; i++)
    *pointer++ = challenge[i];
  
  /* Blob signature */
  *pointer++ = 0x01; 
  *pointer++ = 0x01; 
  *pointer++ = 0x00; 
  *pointer++ = 0x00;

  /* Reserved */
  *pointer++ = 0x00;
  *pointer++ = 0x00; 
  *pointer++ = 0x00;
  *pointer++ = 0x00; 

  /* Timestamp */
  // timestamp = time(NULL);
  // DEBUG
  timestamp = 0;
  timestamp += 11644473600ll;
  timestamp *= 10000000;

  // printf("timestamp = %llu (%X)\n", timestamp, (long long int) timestamp);

  for (i=0; i<8; i++)
  {
    tspointer = (unsigned char *) &timestamp + i;
    *pointer++ = (unsigned char) *tspointer;
    // DEBUG
    // printf("bytevalue %d = %d (%.2X) \n", i, (unsigned char) *tspointer, 
    //                     (unsigned char) *tspointer);
  }
  
  /* Client challenge */
    
  for (i=0; i<8; i++)
    *pointer++ = cl_challenge[i];

  /* Unknown */
  *pointer++ = 0x00;
  *pointer++ = 0x00; 
  *pointer++ = 0x00;
  *pointer++ = 0x00; 

  /* Target information block */
  
  // printf("Target information block: ");
  for (i=0; i<targetinformationlen; i++)
  {
    *pointer++ = (unsigned char) *(targetinformation + i);
    // printf("%.2X", (unsigned char) *(targetinformation+i));
  }
  // printf("\n");

  /* Unknown */
  *pointer++ = 0x00;
  *pointer++ = 0x00; 
  *pointer++ = 0x00;
  *pointer++ = 0x00; 

  /* Blob construction finished */
  
  hmac_md5(blob, pointer-blob, 
           ntlmv2_hash, 16, ntlmv2);

  for (i=0; i<16; i++)
    *(response+i) = *(ntlmv2 + i);

  for (i=0; i<(pointer-blob)-8; i++)
    *(response+i+16) = *(blob + i + 8);    
    
  return (pointer-blob) + 8;
}


 /*
  * takes a 21 byte array and treats it as 3 56-bit DES keys. The
  * 8 byte plaintext is encrypted with each key and the resulting 24
  * bytes are stored in the results array.
  */
static void calc_resp(unsigned char *keys,
                      unsigned char *plaintext,
                      unsigned char *results)
{
  DES_key_schedule ks;

  setup_des_key(keys, DESKEY(ks));
  DES_ecb_encrypt((DES_cblock*) plaintext, (DES_cblock*) results,
                  DESKEY(ks), DES_ENCRYPT);

  setup_des_key(keys+7, DESKEY(ks));
  DES_ecb_encrypt((DES_cblock*) plaintext, (DES_cblock*) (results+8),
                  DESKEY(ks), DES_ENCRYPT);

  setup_des_key(keys+14, DESKEY(ks));
  DES_ecb_encrypt((DES_cblock*) plaintext, (DES_cblock*) (results+16),
                  DESKEY(ks), DES_ENCRYPT);
}

/*
 * Turns a 56 bit key into the 64 bit, odd parity key and sets the key.  The
 * key schedule ks is also set.
 */
static void setup_des_key(unsigned char *key_56,
                          DES_key_schedule DESKEYARG(ks))
{
  DES_cblock key;

  key[0] = key_56[0];
  key[1] = ((key_56[0] << 7) & 0xFF) | (key_56[1] >> 1);
  key[2] = ((key_56[1] << 6) & 0xFF) | (key_56[2] >> 2);
  key[3] = ((key_56[2] << 5) & 0xFF) | (key_56[3] >> 3);
  key[4] = ((key_56[3] << 4) & 0xFF) | (key_56[4] >> 4);
  key[5] = ((key_56[4] << 3) & 0xFF) | (key_56[5] >> 5);
  key[6] = ((key_56[5] << 2) & 0xFF) | (key_56[6] >> 6);
  key[7] =  (key_56[6] << 1) & 0xFF;

  DES_set_odd_parity(&key);
  DES_set_key(&key, ks);
}

/*
** Function: hmac_md5
*/

void hmac_md5(unsigned char* text,                 /* pointer to data stream */
		int             text_len,          /* length of data stream */
		unsigned char*  key,               /* pointer to authentication key */
		int             key_len,           /* length of authentication key */
		unsigned char*  digest             /* caller digest to be filled in */
             )
{
        MD5_CTX context;
        unsigned char k_ipad[65];    /* inner padding -
                                      * key XORd with ipad
                                      */
        unsigned char k_opad[65];    /* outer padding -
                                      * key XORd with opad
                                      */
        unsigned char tk[16];
        int i;
        /* if key is longer than 64 bytes reset it to key=MD5(key) */
        if (key_len > 64) {

                MD5_CTX      tctx;

                MD5_Init(&tctx);
                MD5_Update(&tctx, key, key_len);
                MD5_Final(tk, &tctx);

                key = tk;
                key_len = 16;
        }

        /*
         * the HMAC_MD5 transform looks like:
         *
         * MD5(K XOR opad, MD5(K XOR ipad, text))
         *
         * where K is an n byte key
         * ipad is the byte 0x36 repeated 64 times

         * opad is the byte 0x5c repeated 64 times
         * and text is the data being protected
         */

        /* start out by storing key in pads */
        bzero( k_ipad, sizeof k_ipad);
        bzero( k_opad, sizeof k_opad);
        bcopy( key, k_ipad, key_len);
        bcopy( key, k_opad, key_len);

        /* XOR key with ipad and opad values */
        for (i=0; i<64; i++) {
                k_ipad[i] ^= 0x36;
                k_opad[i] ^= 0x5c;
        }
        /*
         * perform inner MD5
         */
        MD5_Init(&context);                   /* init context for 1st
                                              * pass */
        MD5_Update(&context, k_ipad, 64);     /* start with inner pad */
        MD5_Update(&context, text, text_len); /* then text of datagram */
        MD5_Final(digest, &context);          /* finish up 1st pass */
        /*
         * perform outer MD5
         */
        MD5_Init(&context);                   /* init context for 2nd
                                              * pass */
        MD5_Update(&context, k_opad, 64);     /* start with outer pad */
        MD5_Update(&context, digest, 16);     /* then results of 1st
                                              * hash */
        MD5_Final(digest, &context);          /* finish up 2nd pass */
}

void SamOEMhash( unsigned char *data, const unsigned char *key, int val)
{
  unsigned char s_box[256];
  unsigned char index_i = 0;
  unsigned char index_j = 0;
  unsigned char j = 0;
  int ind;

  for (ind = 0; ind < 256; ind++)
  {
    s_box[ind] = (unsigned char)ind;
  }

  for( ind = 0; ind < 256; ind++)
  {
     unsigned char tc;

     j += (s_box[ind] + key[ind%16]);

     tc = s_box[ind];
     s_box[ind] = s_box[j];
     s_box[j] = tc;
  }
  for( ind = 0; ind < val; ind++)
  {
    unsigned char tc;
    unsigned char t;

    index_i++; 
    index_j += s_box[index_i];

    tc = s_box[index_i];
    s_box[index_i] = s_box[index_j];
    s_box[index_j] = tc;

    t = s_box[index_i] + s_box[index_j];
    data[ind] = data[ind] ^ s_box[t];
  }
}

void sessionkeyResponse(unsigned char *response, unsigned char *challenge, 
                        char *password, char *client_challenge)
{
  unsigned char *pw;
  unsigned char ntbuffer[24], user_session_key[24];
  unsigned char nonce[16];
  int len, i;

  len = strlen(password);
  
  /* make it fit at least 14 bytes */
  pw = (unsigned char *) malloc (len < 7 ? 14 : len * 2);
  if (!pw)
    return; /* this will lead to a badly generated package */  

  /*** create NT hashed password ***/
  MD4_CTX MD4;

  len = strlen(password);

  for (i=0; i<len; i++)
  {
    pw[2*i]   = password[i];
    pw[2*i+1] = 0;
  }

  MD4_Init(&MD4);
  MD4_Update(&MD4, pw, 2*len);
  MD4_Final(ntbuffer, &MD4);

  memset(ntbuffer+16, 0, 8);

  MD4_Init(&MD4);
  MD4_Update(&MD4, ntbuffer, 16);
  MD4_Final(user_session_key, &MD4);

  memset(user_session_key+16, 0, 8);

  for (i=0; i<8; i++)
    nonce[i] = *(challenge+i);
  for (i=0; i<8; i++)
    nonce[i+8] = *(client_challenge+i);

/**** DEBUG
  printf("session nonce is: ");
  for (i=0; i<16; i++)                                 
    printf("%.2X ", (unsigned char) nonce[i]);
  printf("\n");
***/

  hmac_md5(nonce, 16, user_session_key, 16, response);

  free(pw);
}

int sign_smb(unsigned char *firstbits, int firstbitslen, unsigned char sequence,
             unsigned char *restofpacket, int restofpacketlen,
             unsigned char *destination) 
{

  MD5_CTX context; 
  unsigned char my_key[16];
  unsigned char sequence_buf[8];
  int i;

  for (i=0; i<16; i++)
    my_key [i] = i+1;	// Don't ask me why, I just chose this to be the key

  sequence_buf[0] = 3;//sequence;
  for (i=1; i<8; i++)
    sequence_buf [i] = 0;

  MD5_Init(&context);

  MD5_Update(&context, my_key, 16);

  MD5_Update(&context, firstbits, firstbitslen);

  MD5_Update(&context, sequence_buf, 8);

  MD5_Update(&context, restofpacket, restofpacketlen);  

  MD5_Final(destination, &context);

  return 0;
}

int sign_smb_inplace (unsigned char* line, int size, int seq_num)
{
  unsigned char signature[16];

  const int HEADER_LEN = 14;

  MD5_CTX context;
  unsigned char my_key[16];
  unsigned char sequence_buf[8];
  int i;

  for (i=0; i<16; i++)
    my_key [i] = i+1;   // Don't ask me why, I just chose this to be the key

  sequence_buf[0] = seq_num;
  for (i=1; i<8; i++)
    sequence_buf [i] = 0;

  MD5_Init(&context);
  MD5_Update(&context, my_key, sizeof(my_key));
  MD5_Update(&context, (line+4), HEADER_LEN);
  MD5_Update(&context, sequence_buf, sizeof(sequence_buf));
  MD5_Update(&context, (line+26), size - 22);
  MD5_Final(signature, &context);

  // Copy the signature in
  memcpy ((void *) (line + 18), signature, 8);
  seq_num += 2;

  return 0;
}


