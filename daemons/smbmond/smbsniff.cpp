//#define _BSD_SOURCE 1

#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <syslog.h>
#include <string.h>
#include <time.h>

#define PROMISCOUS  1
#ifndef ETHER_ADDR_LEN
#define ETHER_ADDR_LEN 6
#endif

void CleanUp();
void RecordPacket(char *protocol, char *sip, int sport, char *dip, int dport, int bytes, int timestamp, void *data, int size);
void ProcessPacket(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void HandleSignal(int signo);
unsigned short lendian(unsigned char first, unsigned char second);
int int_lendian(unsigned char first, unsigned char second, unsigned third, unsigned forth);

char syslogbuff[256];
int daemon_init(void);

FILE *p;

/* Ethernet header */

struct sniff_ethernet 
{
  u_char  dhost[ETHER_ADDR_LEN];    /* Destination host address */
  u_char  shost[ETHER_ADDR_LEN];    /* Source host address */
  u_short type;                     /* IP? ARP? RARP? etc */
};

/* IP header */

struct sniff_ip 
{
  #if BYTE_ORDER == BIG_ENDIAN             /* Sparc, Motorola, etc */
  u_int   version:4,		           /* version */
          hdrlen:4;		           /* header length */
  #elif BYTE_ORDER == LITTLE_ENDIAN        /* Intel */
  u_int   hdrlen:4,		           /* header length */
          version:4;		           /* version */
  #else
  #error "Please fix endian.h"
  #endif  
  u_char  tos;		/* type of service */
  u_short len:16;	/* total length */
  u_short id;		/* identification */
  u_short offset;	/* fragment offset field */
  u_char  ttl;		/* time to live */
  u_char  protocol;	/* protocol */
  u_short sum;		/* checksum */
  struct in_addr src;   /* source address */
  struct in_addr dst;   /* dest address */
};

/* TCP header */

typedef u_int32_t tcp_seq;

struct sniff_tcp {
  u_short sport;                       /* source port */
  u_short dport;                       /* destination port */
  tcp_seq seq;                         /* sequence number */
  tcp_seq ack;                         /* acknowledgement number */
  #if BYTE_ORDER == LITTLE_ENDIAN      /* Intel platform */
    u_int unused:4,                    /* (unused) */
          offset:4;                    /* data offset */
  #elif BYTE_ORDER == BIG_ENDIAN       /* Sparc, Motorola, etc */
    u_int offset:4,                    /* data offset */
          unused:4;                    /* (unused) */
  #else
  #error "Please fix endian.h"
  #endif  
  u_char  flags;
  #define FIN  0x01
  #define SYN  0x02
  #define RST  0x04
  #define PSH  0x08
  #define ACK  0x10
  #define URG  0x20
  u_short win;                         /* window */
  u_short sum;                         /* checksum */
  u_short urp;                         /* urgent pointer */
};

/* UDP header */

struct sniff_udp {
  u_short sport;           /* source port */
  u_short dport;           /* destination port */
  u_short len; 	           /* udp package length */
  u_short sum;	           /* udp package checksum */
};

/******************************************************************************
 *
 * Name: main()
 * Description: Calls signal handler, goes daemon, opens the database,
 *              opens syslog and opens device for sniffing
 * Returns: 0, or 1 in case of error
 * Arguments:
 *
 *****************************************************************************/
int main(int argc, char *argv[])
{
  char *dev;                      /* Sniffing device */
  char errbuf[PCAP_ERRBUF_SIZE];  /* Error buffer */
  pcap_t *descr;                  /* Sniff handler */

  struct bpf_program fp;          /* hold compiled program */
  bpf_u_int32 maskp;              /* subnet mask */
  bpf_u_int32 netp;               /* ip */
  char filter_app[256];         /* We're interested in tcp and udp packets only */
  int numOfPackets = -1;          /* How many packets to sniff, negative value means infinite */

  if (argc < 3)
  {
    printf("Usage: %s <IP> <IP>\n", argv[0]);
    exit(1);
  }

  sprintf(filter_app, "(port 139 or port 445) and host %s and host %s", argv[1], argv[2]);
  // sprintf(filter_app, "port 139");

  /* Signal handler */
  
  signal(SIGTERM, HandleSignal);
  signal(SIGUSR1, HandleSignal);
  signal(SIGUSR2, HandleSignal);
  signal(SIGHUP, HandleSignal);

  /* Bye-bye console */

  // daemon_init();

  /* Set our device */
  
  dev = pcap_lookupdev(errbuf);
  pcap_lookupnet(dev, &netp, &maskp, errbuf);

  /* Print device to syslog */
  
  syslog(LOG_INFO, syslogbuff, 128);

  /* Open the device */
  
  descr = pcap_open_live(dev, BUFSIZ, PROMISCOUS, 0, errbuf);
  if (descr == NULL)
  {
    sprintf(syslogbuff, "pcap_open_live(): %s\n", errbuf);
    syslog(LOG_ERR, syslogbuff, 128);
    exit(1);
  }

  /* Apply the rules */

  if( pcap_compile(descr, &fp, filter_app, 0, netp) == -1)
  {
    sprintf(syslogbuff, "pcap_compile borqed\n");
    syslog(LOG_ERR, syslogbuff, 128);
    exit(1);
  }
  if (pcap_setfilter(descr, &fp) == -1)
  {
    sprintf(syslogbuff, "pcap_setfilter said 'eat shit'\n");
    syslog(LOG_ERR, syslogbuff, 128);
    exit(1);
  }

  /* Start the callback function */

  pcap_loop(descr, numOfPackets, ProcessPacket, NULL);
  pcap_close(descr);

  /* Clean up, close open handles */

  CleanUp();
  
  return 0;
}

/******************************************************************************
 *
 * Name: CleanUp
 * Description:  frees allocated memory, closes files and database connections
 * Arguments:
 * Returns: void
 *
 *****************************************************************************/
void CleanUp()
{
  // fclose(p);
  closelog();
}

/******************************************************************************
 *
 * Name: RecordPacket
 * Description:  insert IP packet's description to the database
 * Arguments: protocol (TCP/UDP), source IP address, source port number,
 *            destination IP address, destination port number,
 *            number of data bytes in packet, unix timestamp              
 * Returns: void, in case of error ExecuteSQL will report message and exit.
 *
 *****************************************************************************/
void RecordPacket(
  char *protocol, 
  char *sip, int sport, 
  char *dip, int dport, 
  int bytes, int timestamp, void *data, int size)
{
    unsigned char smb_com, smb_rcls, smb_reh;
    unsigned short smb_err;
    unsigned char smb_flg;
    unsigned short smb_flg2;
    unsigned short smb_tid, smb_pid, smb_uid, smb_mid;
    unsigned char smb_wct;
    unsigned short smb_vwv[128];
    unsigned short smb_bct;
    int passlen;
    
    char apacket[1600];
    int counter = 0;
    char c;
    char *pointer;
    int bcounter = 0;

    memcpy(apacket, data, size);
    apacket[size] = '\0';
    
    if ((apacket[17] == 'S') && (apacket[18] == 'M') && (apacket[19] == 'B'))
    {
      smb_com  = (unsigned char) apacket[20];
      smb_rcls = (unsigned char) apacket[21];
      smb_reh  = (unsigned char) apacket[22];
      smb_err  = lendian(apacket[23], apacket[24]);
      smb_flg  = (unsigned char) apacket[25];
      smb_flg2 = lendian(apacket[26], apacket[27]);
      smb_tid  = lendian(apacket[40], apacket[41]);
      smb_pid  = lendian(apacket[42], apacket[43]);
      smb_uid  = lendian(apacket[44], apacket[45]);
      smb_mid  = lendian(apacket[46], apacket[47]);
      smb_wct  = (unsigned char) apacket[48];
      
      printf("-------------------------------------\n");
      printf("Source IP = %s, Dest IP = %s, size = %d, SMB_COM = %d\n", 
        sip,
        dip,
        size,
        smb_com);    
      printf("data[0]  = %d    data[1]  = %d,   data[2] = %d    data[3] = %d   data[4] = %d\n",
              (unsigned char) apacket[12], (unsigned char) apacket[13], (unsigned char) apacket[14], 
              (unsigned char) apacket[15], (unsigned char) apacket[16]);
      printf("smb_rcls = %d    smb_reh  = %d    smb_err = %d\n", smb_rcls, smb_reh , smb_err);
      printf("smb_flg  = %d    smb_flg2 = %d    smb_tid = %d\n", smb_flg , smb_flg2, smb_tid);
      printf("smb_pid  = %d    smb_uid  = %d    smb_mid = %d\n", smb_pid , smb_uid , smb_mid);
      printf("smb_wct  = %d\n", smb_wct);       

      if (smb_wct > 0)
      {
        while (counter < 2*smb_wct)
        {
          smb_vwv[counter] = apacket[49 + counter];
          printf("smb_vwv[%d] = %d\n", counter+1, (unsigned char) smb_vwv[counter]);
          counter++;
        }
      }
        
      smb_bct = lendian(apacket[49 + counter], apacket[50 + counter]);
      printf("smb_bct = %d\n", smb_bct);

      if (smb_com == 114) // Negotiate protocol
      {
        printf("Dialect index: %d\n", lendian(smb_vwv[0], smb_vwv[1]));
      
        if (smb_vwv[2] & 0x01) printf("User Level.\n"); else printf("Share Level.\n");
        if (smb_vwv[2] & 0x02) printf("Challenge/response required.\n");
        if (smb_vwv[2] & 0x04) 
        {
          printf("Server can perform message signing.\n");
          if (smb_vwv[2] & 0x08) 
            printf("Message signing is required.\n");
          else
            printf("Message signing is optional.\n");
        }
        else
          printf("Message signing not supported.\n");
          
        printf("MaxMpxCount: %d.\n", lendian(smb_vwv[3], smb_vwv[4]));          
        printf("MaxNumberVCs: %d.\n", lendian(smb_vwv[5], smb_vwv[6]));          
        printf("MaxBufferSize: %d.\n", int_lendian(smb_vwv[7], smb_vwv[8], smb_vwv[9], smb_vwv[10]));          
        printf("MaxRawSize: %d.\n", int_lendian(smb_vwv[11], smb_vwv[12], smb_vwv[13], smb_vwv[14]));          
        printf("SessionKey: %d.\n", int_lendian(smb_vwv[15], smb_vwv[16], smb_vwv[17], smb_vwv[18]));          
        printf("Capabilities: %d.\n", int_lendian(smb_vwv[19], smb_vwv[20], smb_vwv[21], smb_vwv[22]));          
        printf("ServerTimeZone: %d.\n", lendian(smb_vwv[31], smb_vwv[32]));          
        printf("EncryptionKeyLength: %d.\n", (unsigned char)smb_vwv[33]); 
        for (bcounter=0; bcounter<(unsigned char)smb_vwv[33]; bcounter++)
        {
          c = smb_vwv[bcounter+34];
          if (((unsigned char) c >=48) && ((unsigned char) c <=122))
            printf("%c", c);
          else  
            printf("?");        
        }        
      }

      if (smb_com == 115) // Login
      {
        printf("PID: %d.\n", lendian(smb_vwv[8], smb_vwv[9]));          

        passlen = lendian(smb_vwv[14], smb_vwv[15]);

        printf("Passlen: %d.\n", passlen);          
        
        pointer = (char *) data + 49 + smb_wct * 2 + 2; 
        printf("Password: %s\n", pointer);
        for (bcounter=0; bcounter<passlen; bcounter++)
        {
          c = *pointer;
          if (((unsigned char) c >=48) && ((unsigned char) c <=122))
            printf("\t%d: %c (%d)\n", bcounter, c, (unsigned char) c);
          else  
            printf("\t%d:    (%d)\n", bcounter, (unsigned char) c);        
          pointer++;
        }        
      }        
      
      /* Print everything */
        
      pointer = (char *) data; 
      for (bcounter=0; bcounter<size; bcounter++)
      {
        c = *pointer;
        if (((unsigned char) c >=48) && ((unsigned char) c <=122))
          printf("%d: %c (%d)\n", bcounter, c, (unsigned char) c);
        else  
          printf("%d:    (%d)\n", bcounter, (unsigned char) c);        
        pointer++;
      }

      printf("\n-------------------------------------\n");

    }
    else if ((apacket[5] == 'S') && (apacket[6] == 'M') && (apacket[7] == 'B'))
    {
      smb_com  = (unsigned char) apacket[8];
      smb_rcls = (unsigned char) apacket[9];
      smb_reh  = (unsigned char) apacket[10];
      smb_err  = lendian(apacket[11], apacket[12]);
      smb_flg  = (unsigned char) apacket[13];
      smb_flg2 = lendian(apacket[14], apacket[15]);
      smb_tid  = lendian(apacket[28], apacket[29]);
      smb_pid  = lendian(apacket[30], apacket[31]);
      smb_uid  = lendian(apacket[32], apacket[33]);
      smb_mid  = lendian(apacket[34], apacket[35]);
      smb_wct  = (unsigned char) apacket[36];
      
      printf("-------------------------------------\n");
      printf("Source IP = %s, Dest IP = %s, size = %d, SMB_COM = %d\n", 
        sip,
        dip,
        size,
        smb_com);    
      printf("data[0]  = %d    data[1]  = %d,   data[2] = %d    data[3] = %d   data[4] = %d\n",
              (unsigned char) apacket[0], (unsigned char) apacket[1], (unsigned char) apacket[2], 
              (unsigned char) apacket[3], (unsigned char) apacket[4]);
      printf("smb_rcls = %d    smb_reh  = %d    smb_err = %d\n", smb_rcls, smb_reh , smb_err);
      printf("smb_flg  = %d    smb_flg2 = %d    smb_tid = %d\n", smb_flg , smb_flg2, smb_tid);
      printf("smb_pid  = %d    smb_uid  = %d    smb_mid = %d\n", smb_pid , smb_uid , smb_mid);
      printf("smb_wct  = %d\n", smb_wct);       

      if (smb_wct > 0)
      {
        while (counter < 2*smb_wct)
        {
          smb_vwv[counter] = apacket[37 + counter];
          printf("smb_vwv[%d] = %d\n", counter+1, (unsigned char) smb_vwv[counter]);          
          counter++;
        }
      }
        
      smb_bct = lendian(apacket[37 + counter], apacket[38 + counter]);
      printf("smb_bct = %d\n", smb_bct);

      if (smb_com == 114) // Negotiate protocol
      {
        printf("Dialect index: %d\n", lendian(smb_vwv[0], smb_vwv[1]));
      
        if (smb_vwv[2] & 0x01) printf("User Level.\n"); else printf("Share Level.\n");
        if (smb_vwv[2] & 0x02) printf("Challenge/response required.\n");
        if (smb_vwv[2] & 0x04) 
        {
          printf("Server can perform message signing.\n");
          if (smb_vwv[2] & 0x08) 
            printf("Message signing is required.\n");
          else
            printf("Message signing is optional.\n");
        }
        else
          printf("Message signing not supported.\n");
          
        printf("MaxMpxCount: %d.\n", lendian(smb_vwv[3], smb_vwv[4]));          
        printf("MaxNumberVCs: %d.\n", lendian(smb_vwv[5], smb_vwv[6]));          
        printf("MaxBufferSize: %d.\n", int_lendian(smb_vwv[7], smb_vwv[8], smb_vwv[9], smb_vwv[10]));          
        printf("MaxRawSize: %d.\n", int_lendian(smb_vwv[11], smb_vwv[12], smb_vwv[13], smb_vwv[14]));          
        printf("SessionKey: %d.\n", int_lendian(smb_vwv[15], smb_vwv[16], smb_vwv[17], smb_vwv[18]));          
        printf("Capabilities: %d.\n", int_lendian(smb_vwv[19], smb_vwv[20], smb_vwv[21], smb_vwv[22]));          
        printf("ServerTimeZone: %d.\n", lendian(smb_vwv[31], smb_vwv[32]));          
        printf("EncryptionKeyLength: %d.\n", (unsigned char)smb_vwv[33]); 
        for (bcounter=0; bcounter<(unsigned char)smb_vwv[33]; bcounter++)
        {
          c = smb_vwv[bcounter+34];
          if (((unsigned char) c >=48) && ((unsigned char) c <=122))
            printf("%c", c);
          else  
            printf("?");        
        }                
      }

      if (smb_com == 115) // Login
      {
        printf("PID: %d.\n", lendian(smb_vwv[8], smb_vwv[9]));          
        printf("Passlen: %d.\n", lendian(smb_vwv[14], smb_vwv[15]));          
        
        pointer = (char *) data + 37 + smb_wct * 2 + 2; 
        printf("Password = %s\n", pointer);
      }        
      
      /* Print everything */
        
      pointer = (char *) data;
      for (bcounter=0; bcounter<size; bcounter++)
      {
        c = *pointer;
        printf("%d: %c (%d)\n", bcounter, c, (unsigned char) c);
        pointer++;
      }

      printf("\n-------------------------------------\n");

    }
    else
    {
      printf("-------------------------------------\n");
      printf("Source IP = %s, Dest IP = %s, size = %d, type = %d\n", 
        sip,
        dip,
        size,
       (unsigned char) apacket[0]);    

      /* Print everything */
        
      pointer = (char *) data;
      for (bcounter=0; bcounter<size; bcounter++)
      {
        c = *pointer;
        if (((unsigned char) c >=48) && ((unsigned char) c <=122))
          printf("%d: %c (%d)\n", bcounter, c, (unsigned char) c);
        pointer++;
      }

      printf("-------------------------------------\n");
    }
}

/******************************************************************************
 *
 * Name: ProcessPacket
 * Description:  analyzing packet and logging it if it is of interest
 * Arguments: pcap arguments (not used), pcap header, packet
 * Returns: void, in case of error sub-functions will report message and exit.
 *
 *****************************************************************************/
void ProcessPacket(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
  const struct sniff_ethernet *ethernet;  /* The ethernet header */
  const struct sniff_ip *ip;              /* The IP header */
  const struct sniff_tcp *tcp;            /* The TCP header */
  void *data;

  int size_ethernet = sizeof(struct sniff_ethernet);
  int size_ip = sizeof(struct sniff_ip);
  int size_tcp = sizeof(struct sniff_tcp);

  int tcp_packet_size;
  char src_ip[15];
  char dst_ip[15];

  /* Point our structures to the right place in the packet */

  ethernet = (struct sniff_ethernet*)(packet);
  ip = (struct sniff_ip*)(packet + size_ethernet);
  tcp = (struct sniff_tcp*)(packet + size_ethernet + size_ip);
  data = (void *)(packet + size_ethernet + size_ip + size_tcp);

  /* Get IP's, inet_ntoa overwrites statically allocated buffer in subsequent calls */
  
  strcpy(src_ip, inet_ntoa(ip->src));
  strcpy(dst_ip, inet_ntoa(ip->dst));

  /* Check if this is an IP v4 packet */
  /* If not an IP v4 packet, return. Otherwise, continue. */

  if (ip->version != 4) return;
  else
  {
    /* IP packet with less than 20 bytes of headers?! Nonsense! */
    
    if (ip->hdrlen < 5) return;

    switch (ip->protocol)
    {
      /* OK, this looks like a TCP packet */    
    
      case 6:
        /* First, let's get the size of the packet */
        
        tcp_packet_size = ntohs(ip->len) - (ip->hdrlen + tcp->offset)*4;
        
        /* If TCP packet size is 0, we are not interested, except for SYNACK */
        
        if ((tcp_packet_size == 0) && 
           ((!(tcp->flags & SYN)) || (!(tcp->flags & ACK))))
        return;
        
	/* Let's record this packet in the database */
        
        RecordPacket("TCP", src_ip, ntohs(tcp->sport), dst_ip,
                             ntohs(tcp->dport), tcp_packet_size, (int) time(NULL), data, tcp_packet_size);     
        break;
        
      default:
      
        /* We are not really interested in anything other than TCP at this time */
        
        return;
    }
  }
}

/******************************************************************************
 *
 * Name: daemon_init
 * Description:  sinks our application in the background
 * Arguments:
 * Returns: 0 if succesfull, -1 if not
 *
 *****************************************************************************/
int daemon_init()
{
  pid_t  pid;
  
  /* Fork */
  
  if ( (pid = fork()) < 0)
    return -1;
  
  /* Terminate parent */
  
  else if (pid != 0)
    exit(0);
    
  /* Child continues */
  /* Become session leader */
  
  setsid();
  
  /* Change directory to "/" */
  chdir("/");
  
  /* Clear file creation mode mask */
  umask(0);
  
  return 0;
}

/******************************************************************************
 *
 * Name: HandleSignal
 * Description:  handles TERM, USR1, USR2 and HUP signals
 * Arguments: signal number
 * Returns: void
 *
 *****************************************************************************/
void HandleSignal(int signo)
{
  switch (signo)
  {
    case SIGTERM:

      /* This is a normal way for a daemon to terminate */
    
      CleanUp();
      
      /* Exit with 0, we are leaving with no hard feelings */
      
      exit(0);
    
    // default:
      /* Try to ignore */
      /* We don't want to be killed by USR1, USR2 or HUP signals */
  }
  return;
}

unsigned short lendian(unsigned char first, unsigned char second)
{
  unsigned short firstbyte, secondbyte;
  unsigned short result;
  
  firstbyte  = first;
  secondbyte = second;
  
  result = firstbyte | (secondbyte << 8);

  return result;
}

int int_lendian(unsigned char first, unsigned char second, unsigned third, unsigned forth)
{
  unsigned int firstbyte, secondbyte, thirdbyte, forthbyte;
  unsigned int result;
  
  firstbyte  = first;
  secondbyte = second;
  thirdbyte  = third;
  forthbyte  = forth;
  
  result = firstbyte | (secondbyte << 8) | (thirdbyte << 16) | (forthbyte << 24);

  return result;
}

