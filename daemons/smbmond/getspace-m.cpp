#include <stdio.h>
#include <stdlib.h>

#include "../lib/netmon.h"
#include "getspace.h"

char syslogbuff[SYSLOGLINELEN];

/* Test main function */
int main(int argc, char *argv[])
{
  char error_message[1024];
  int port;
  int total, available, blocksize;
  int result;

  if (argc < 7)
  {
    printf("Wrong number of arguments: %d\n", argc - 1);
    printf("Usage: %s <IP address> <Share name> <User name> <Password> <Server name> <Domain> [port]\n", argv[0]);
    exit(1);
  }

  if (argc == 8)
    port = atoi(argv[7]);
  else
    port = 445;

  result = getspace(argv[1], port, argv[2], argv[3], argv[4], argv[5], argv[6], 
                &total, &available, &blocksize, error_message);
  
  if (result != 0)            
    printf("An error ocurred: %s (%d).\n", error_message, result);
  else
  {
    printf("Total Blocks: %d\n", total);
    printf("Avail Blocks: %d\n", available);
    printf("Block size:   %d\n", blocksize);
  }
              
  return 0;
}
