#ifndef __NETSCAN_DAEMON__
#define __NETSCAN_DAEMON__

#include "daemon.h"

namespace netmon
{
  class alertscanner;

  class netscan_daemon : public daemon
  {
    public:
      netscan_daemon (int, char **);
      ~netscan_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "netscand";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/netscand";
      }

      void scan ();
      void installSigHandler ();
      void updateFound (int);

      static void sigHandler (int);

      alertscanner *m_scanner;
      int m_found;

      enum { sleepTime = 6 * 60 * 60 };
  };
}

#endif //__NETSCAN_DAEMON__
