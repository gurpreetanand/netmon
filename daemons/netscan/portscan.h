#ifndef __PORT_SCAN__
#define __PORT_SCAN__

#include <dumbnet.h>
#include <pcap.h>
#include <sys/time.h>
#include <stdint.h>
#include <event.h>

#include <list>
#include <map>
#include <string>
#include <set>

#include "portlist.h"

namespace netmon
{
  class portlist;

  class portscanner
  {
    public:
      portscanner ();
      portscanner (const char *);
      ~portscanner ();

      void scan (const portlist &, const char *);
      void scan (const char *, bool = true);

      const std::set <unsigned int> &getResults () const
      {
      	return m_result;
      }

      void clear ()
      {
        m_result.clear ();
      }

      void setTimeout (const timeval &to)
      {
        m_timeout = to;
      }

      void setSourceAddress (uint32_t addr)
      {
        m_source.addr_ip = addr;
        m_sourceSet = true;
      }

    protected:
      void init ();
      void setEvents ();

      static void doRead (int, short, void *);
      static void doWrite (int, short, void *);
      static void onTimeout (int, short, void *);

      void doRead (bool = false);
      void doWrite ();
      void onTimeout ();
      bool handleData (u_char *);

      void getDevice (const char *);
      void getLocalAddress ();

      void nextPort ();
      bool hasMorePorts ();

      std::set <unsigned int> m_result;
      std::string m_device;

      timeval m_timeout;
      event m_readEvent;
      event m_writeEvent;
      event m_toEvent;

      pcap_t *m_pcap;
      ip_t *m_ip;
      rand_t *m_rand;
      intf_t *m_iface;

      addr m_destination;
      addr m_source;
      bool m_sourceSet;

      char *m_errBuf;
      int m_pcapFD;
      int m_dllen;
      unsigned int m_currPort;

      enum mode { _eAllPorts, _ePortSet };
      mode m_mode;

      bool m_init;
      bool m_waitForTimeout;
      bool m_finished;
      unsigned char m_cycles;
      std::list<unsigned int> m_ports;
      std::list<unsigned int>::iterator m_it;

      enum portStatus { _eFiltered, _eClosed, _eOpen };
      std::map<unsigned int, portStatus> m_status;
  };
}

#endif // __PORT_SCAN__
