#include <map>
#include <sstream>
#include <cstdlib>

#include "portlist.h"
#include "exception.h"

namespace netmon
{
  portlist::portlist ()
  {
  }

  portlist::~portlist ()
  {
    for (unsigned int i = 0; i < m_portlist.size (); ++i)
      delete m_portlist[i];
    m_portlist.clear ();
  }

  void portlist::addPorts (std::istream &is)
  {
    if (!is)
      throw netmon_exception ("Cannot read from stream.");

    char line[1024];
    std::map <int, service *> smap; // use std::map for duplicate removing

    while (is.getline (line, 1023))
    {
      std::string str = line;
      if (str.size () == 0 || str[0] == '#') // discard empty lines or comments
        continue;

      std::istringstream ss (str);
      std::string sname, stype;
      int sport;

      ss >> sname >> sport >> stype;
      if (stype[0] == '/')
        stype = stype.substr (1, stype.size ());

      service::eproto prot = stype == "tcp" ? service::_eTCP : service::_eUDP;
      if (prot == service::_eUDP) // ignore UDP ports for now
        continue;
      if (smap.find (sport) != smap.end ()) // ignore duplicates
        continue;
      smap[sport] = new service (sname.c_str (), sport, prot);
  //    printf ("%s\t%d\t%s\n", sname.c_str (), sport, stype.c_str ());
    }

    for (std::map <int, service *>::iterator it = smap.begin (); it != smap.end (); ++it)
      m_portlist.push_back (it->second);
  }

  void portlist::addPorts (const char *ports)
  {
    std::string p = ports;

    size_t i; 
    if ((i = p.find ('-')) == std::string::npos) // no minus
    {
      int port = atoi (p.c_str ());
      if (port < 0 || port > 0xffff) // allowed port range is 0-65535
        throw netmon_exception ("Illegal port number.");
      m_portlist.push_back (new service ("", port, service::_eTCP));
    }
    else
    {
      if (i == 0)
        throw netmon_exception ("Illegal port number.");
      int from = atoi (p.substr (0, i).c_str ());
      int to = atoi (p.substr (i + 1, p.size ()).c_str ());

      if ((from < 0 || from > 0xffff) || (to < 0 || to > 0xffff || to < from))
        throw netmon_exception ("Illegal port number.");
      for (int j = from; j <= to; ++j)
        m_portlist.push_back (new service ("", j, service::_eTCP));
    }
  }

  void portlist::addPorts (int from, int to)
  {
    if ((from < 0 || from > 0xffff) || (to < 0 || to > 0xffff || to < from))
      throw netmon_exception ("Illegal port number.");
    for (int i = from; i <= to; ++i)
      m_portlist.push_back (new service ("", i, service::_eTCP));
  }

  void portlist::addPort (int port)
  {
    if ((port < 0 || port > 0xffff))
      throw netmon_exception ("Illegal port number.");
    m_portlist.push_back (new service ("", port, service::_eTCP));
  }
}
