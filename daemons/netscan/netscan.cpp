#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

#include <vector>
#include <fstream>

#include "../lib/netmon.h"
#include "portscan.h"
#include "exception.h"

/*
 * netscan version
 */
const char version[] = "netscan 0.2.0";

/*
 * Global array of options netscan uses
 */
static struct option const long_options[] =
{
  {"help",		no_argument, 0, 'h'},
  {"version",		no_argument, 0, 'v'},
  {"force",		no_argument, 0, 'f'},
  {"services", 		required_argument, 0, 's'},
  {"ports", 		required_argument, 0, 'p'},
  {"concurrency", 	required_argument, 0, 'c'},
  {0, 0, 0, 0}
};

/*
 * Default well-known ports list
 */
const char *services = "/etc/services";

void sighandler (int sig)
{
//  printf ("Signal: %d received\n", sig);
}

int printversion ()
{
  printf ("%s\n", version);
  return 0;
}

int usage ()
{
  printf ("Usage: hostscan [-h] [-f] [-v] [-s service-file] [-p ports] [-c conc] IP-address\n");
  printf ("\t-h: gives this help\n");
  printf ("\t-f: force scan (don't wait for PING response)\n");
  printf ("\t-s service-file: use service-file instead of /etc/services\n");
  printf ("\t-p ports: scan these ports\n");
  printf ("\t-c concurrency: scan this many ports concurrently\n");
  printf ("\t-v: prints version information\n");

  return 1;
}

int main (int argc, char *argv[])
{
  bool readsrv = true;
  int conc = 20;
  long rtime;
  struct sigaction saction;
  netmon::portlist plist;

  if (argc < 2)
    return usage ();

  int c, longind;
  bool force = false;

  while ((c = getopt_long (argc, argv, "hfvp:s:c:", long_options, &longind)) != EOF)
  {
    switch (c)
    {
      case 'h':
        return usage ();
      case 'f':
        force = true;
        break;
      case 'v':
        return printversion ();
      case 's':
        services = optarg;
        break;
      case 'p':
        try
        {
          plist.addPorts (optarg);
          readsrv = false;
        }
        catch (const netmon::netmon_exception &e)
        {
          printf ("%s\n", e.what());
          exit (-1);
        }
        break;
      case 'c':
        conc = atoi (optarg);
        if (conc < 1 || conc > 300)
        {
          printf ("Error: concurrency must be between 1 and 300.\n");
          exit (-1);
        }
        break;
      default:
        printf ("Illegal argument, use --help for help\n");
        exit (-1);
    }
  }

  if (optind >= argc)
  {
    printf ("Illegal number of arguments, use --help for help\n");
    exit (-1);
  }

  argc -= optind;
  argv += optind;

  try
  {
    if (readsrv)
    {
      std::ifstream f (services);
      if (!f)
      {
        fprintf (stderr, "Cannot read %s\n", services);
        exit (-1);
      }
      plist.addPorts (f);
    }

    saction.sa_handler = sighandler;
    sigemptyset (&saction.sa_mask);
    saction.sa_flags = 0;
    sigaction (SIGALRM, &saction, NULL);

    if (!force)
    {
      rtime = send_ping (argv[0]);
      if (rtime == 0)
      {
        printf ("Host is unresponsive or behind a firewall\n");
        exit (1);
      }
      else
        printf ("Host is alive, ping time: %.3f ms\n", (double) rtime / 1000);
    }

    netmon::portscanner pscanner;
    event_init();
    pscanner.scan (plist, argv[0]);
    std::set<unsigned int> v = pscanner.getResults ();
    for (std::set<unsigned int>::iterator i = v.begin(); i != v.end(); ++i)
      printf ("%d\n", *i);
  }
  catch (const netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    exit (-1);
  }

  return 0;
}
