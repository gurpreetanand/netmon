#ifndef __ALERT_SCANNER__
#define __ALERT_SCANNER__

#include "scanner.h"

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif 

namespace netmon
{
  class alertscanner : public scanner
  {
    public:
      alertscanner (db &mydb)
        : scanner (mydb)
      {}

      virtual void reload ();

    protected:
      virtual void queueAlert (const inetaddr &, const std::set<unsigned int> &);
      std::string makePortList (const std::set<unsigned int> &v);
      bool getPortName (unsigned int, std::string &);

      typedef hash_map<unsigned int, std::string> portmap;
      portmap m_map;
  };
}

#endif // __ALERT_SCANNER__
