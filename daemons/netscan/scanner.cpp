#include "scanner.h"
#include "netmon.h"
#include <libpq-fe.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <cstdio>
#include <fstream>
#include <sstream>
              
static const char *services = "/etc/services";

static std::string getTime (time_t t)
{
  char buff[32];
  struct tm *mytm = ::localtime (&t);
  ::strftime (buff, sizeof (buff) - 1, "%F %T", mytm);
  return std::string (buff);
}

static std::string createArray (const std::set<unsigned int> &myset)
{
  std::ostringstream ss;
  ss << "'{";
  bool isfirst = true;

  for (std::set<unsigned int>::iterator it = myset.begin(); it != myset.end(); ++it)
  {
    if (!isfirst)
      ss << ",";
    ss << *it;
    isfirst = false;
  }
  ss << "}'";
  return ss.str();
}

static void parseArray (const std::string &str, std::set<unsigned int> &ports)
{
  if (str.size() == 0)
    return;

  std::string tmp = str;
  replace (tmp.begin(), tmp.end(), ',', ' ');
  std::istringstream s(tmp);
  while (!s.eof())
  {
    unsigned int i;
    s >> i;
    ports.insert (i);
  }
}

namespace netmon
{
  scanner::scanner (db &mydb)
    : m_db (mydb)
  {
    init();
  }

  scanner::~scanner ()
  {
  }

  void scanner::init ()
  {
    std::ifstream f (services);
    if (!f)
    {
      fprintf (stderr, "Cannot read %s\n", services);
      throw netmon_exception ("Cannot read services file.");
      exit (-1);
    }
    m_plist.addPorts (f);
    getNetworks();
    removeOldServers();
    getServers();
  }

  void scanner::dump ()
  {
    printf ("Networks:\n");
    for (netlist::iterator it = m_networks.begin(); it != m_networks.end(); ++it)
      printf ("%s - %s\n", (*it).first.c_str(), (*it).second.c_str());
    printf ("Servers and ports:\n");
    for (scannedservers::iterator it = m_servers.begin(); it != m_servers.end(); ++it)
    {
      in_addr addr;
      addr.s_addr = it->first;
      printf ("%s - ", inet_ntoa (addr));
      const std::set<unsigned int> &pset = it->second.m_ports;
      for (std::set<unsigned int>::iterator j = pset.begin(); j != pset.end(); ++j)
        printf ("%u ", *j);
      printf ("\n");
    }
  }

  void scanner::scan (int &found)
  {
    int i = 0;
    for (netlist::iterator it = m_networks.begin(); it != m_networks.end(); ++it)
    {
      inetaddr from ((*it).first.c_str());
      inetaddr to ((*it).second.c_str());

      if (from == to)
        continue;
      while (from <= to)
      {
        if (!isAddressSeen (from))
        {
          if (scanHost (from))
            ++i;
        }
        ++from;
      }
    }
    found = i;
  }

  void scanner::reload ()
  {
    m_networks.clear();
    getNetworks();
  }

  bool scanner::scanHost (const inetaddr &addr)
  {
    printf ("scanning %s...\n", addr.addr());
    if (send_ping (addr.addr()) == 0)
    {
      printf ("timeout!\n");
      m_servers.erase (addr.iaddr());
      deleteServer (addr.iaddr());
      return false;
    }

    portscanner s;
    s.scan (m_plist, addr.addr());

    const std::set<unsigned int> &v = s.getResults();
    if (m_servers.find (addr.iaddr()) == m_servers.end())
    {
      hostentry e;
      e.m_time = ::time (0);
      e.m_ports = v;
      e.m_id = -1;
      m_servers[addr.iaddr()] = e;
      logServer (addr.iaddr());
    }
    else
      checkNewPorts (addr, v);

    return true;
  }

  void scanner::checkNewPorts (const inetaddr &addr, const std::set<unsigned int> &v)
  {
    bool added = false;
    std::set<unsigned int> &myset = m_servers.find (addr.iaddr())->second.m_ports;
    std::vector<unsigned int> newports;
    std::set<unsigned int> s1;
    std::set<unsigned int> s2;
    std::insert_iterator<std::set<unsigned int> > it1 (s1, s1.begin());
    std::insert_iterator<std::set<unsigned int> > it2 (s2, s2.begin());

    std::set_difference (v.begin(), v.end(), myset.begin(), myset.end(), it1);
    std::set_difference (myset.begin(), myset.end(), v.begin(), v.end(), it2);

    if (s1.size() > 0) // new open ports detected
    {
      logServer (addr.iaddr()); // make sure we log this
      added = true;
      for (std::set<unsigned int>::iterator j = s1.begin(); j != s1.end(); ++j)
        m_servers[addr.iaddr()].m_ports.insert (*j);
      m_servers[addr.iaddr()].m_time = ::time(0);
      queueAlert (addr, s1); // raise alert
    }
    if (s2.size() > 0) // openned ports are closed
    {
      if (!added)
      {
        logServer (addr.iaddr()); // make sure we log this
        m_servers[addr.iaddr()].m_time = ::time(0);
      }

      for (std::set<unsigned int>::iterator j = s2.begin(); j != s2.end(); ++j)
        m_servers[addr.iaddr()].m_ports.erase (*j);
    }
  }
  
  void scanner::logServer (uint32_t ip)
  {
    static std::ostringstream sql;
    hostentry &e = m_servers.find(ip)->second;
    const std::set<unsigned int> &myset = e.m_ports;
    std::string mytime = ::getTime (e.m_time);
    std::string arr = ::createArray (myset);
    in_addr ina;
    ina.s_addr = ip;
    std::string ipaddr = inet_ntoa (ina);
    sql.str("");

    if (e.m_id != -1)
    {
      sql << "UPDATE scan_log SET ports = " << arr << ", timestamp = '"
          << mytime << "' WHERE ID = " << e.m_id;
      m_db.execSQL (sql.str().c_str());
    }
    else
    {
      sql << "INSERT INTO scan_log (srv_ip, ports, timestamp) "
          << "VALUES ('" << ipaddr << "', " << arr << ", '" << mytime << "')";
      m_db.execSQL (sql.str().c_str());
      sql.str("");
      sql << "SELECT id FROM scan_log WHERE srv_ip = '" << ipaddr << "'";
      PGresult *res = m_db.execQuery (sql.str().c_str());
      e.m_id = atoi (PQgetvalue (res, 0, 0));
      PQclear (res);
    }
  }

  void scanner::deleteServer (uint32_t ip)
  {
    static std::ostringstream sql;
    in_addr ina;
    ina.s_addr = ip;
    std::string ipaddr = inet_ntoa (ina);

    sql.str("");
    sql << "DELETE FROM scan_log WHERE srv_ip = '" << ipaddr << "'";
    m_db.execSQL (sql.str().c_str());
  }

  void scanner::getNetworks ()
  {
    PGresult *res = m_db.execQuery ("SELECT NETWORK, BROADCAST FROM LOCALNETS where enable_portscan = 't'");
    for (int i = 0; i < PQntuples (res); ++i)
    {
      m_networks.push_back (std::make_pair (std::string (PQgetvalue (res, i, 0)),
                                            std::string (PQgetvalue (res, i, 1))));
    }
    PQclear (res);
  }

  void scanner::removeOldServers ()
  {
    static std::ostringstream sql;

    for (netlist::iterator i = m_networks.begin(); i != m_networks.end(); ++i)
    {
      sql.str("");
      sql << "DELETE FROM scan_log WHERE NOT (srv_ip >= '" << (*i).first
          << "' and srv_ip <= '" << (*i).second << "')";
      m_db.execSQL (sql.str().c_str());
    }
  }

  void scanner::getServers ()
  {
    PGresult *res = m_db.execQuery ("SELECT srv_ip, ports, id FROM scan_log ORDER BY ID");
    for (int i = 0; i < PQntuples (res); ++i)
    {
      const char *ip = PQgetvalue (res, i, 0);
      std::string ports = PQgetvalue (res, i, 1);
      ports.erase (0, 1);
      ports.erase (ports.size() - 1);
      std::set<unsigned int> pset;
      ::parseArray (ports, pset);
      in_addr addr;
      if (inet_aton (ip, &addr) == 0)
        continue;
      hostentry e;
      e.m_ports = pset;
      e.m_time = ::time (0);
      e.m_id = atoi (PQgetvalue (res, i, 2));
      m_servers[addr.s_addr] = e;
    }
    PQclear (res);
  }

  bool scanner::isAddressSeen (const inetaddr &a)
  {
    for (inet_list::iterator i = m_visitedNetworks.begin(); i != m_visitedNetworks.end(); ++i)
      if (i->first <= a && a < i->second)
        return true;
    return false;
  }
};
