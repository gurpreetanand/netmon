#include <dumbnet.h>

#include <cstdio>
#include <string>

#include "exception.h"

std::string getInterface (const char *ip)
{
  intf_t *i = intf_open ();
  if (i == 0)
    throw netmon::netmon_exception ("intf_open failed");
  addr a;
  intf_entry e;
  if (addr_aton (ip, &a) == -1)
    throw netmon::netmon_exception ("illegal address");
  if (intf_get_dst (i, &e, &a) == -1)
    throw netmon::netmon_exception ("cannot get interface");
  intf_close (i);

  return std::string (e.intf_name);
}

int cb (const intf_entry *e, void *)
{
  if ((e->intf_type & INTF_TYPE_ETH) && (e->intf_flags & INTF_FLAG_UP))
  {
    printf ("iface: %s\n", e->intf_name);
    printf ("address: %s\n", addr_ntoa (&(e->intf_addr)));
  }

  return 0;
}

void listInterfaces ()
{
  intf_t *i = intf_open ();
  if (i == 0)
    throw netmon::netmon_exception ("intf_open failed");

  intf_loop (i, cb, 0);
  intf_close (i);
}

int main ()
{
  try
  {
    listInterfaces ();
//    std::string i = getInterface ("10.0.2.202");
//    printf ("iface: %s\n", i.c_str());
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("error: %s\n", e.c_str());
  }
}
