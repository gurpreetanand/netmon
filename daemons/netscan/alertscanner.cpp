#include "alertscanner.h"
#include "alerts.h"

#include <cstring>

namespace netmon
{
  std::string alertscanner::makePortList (const std::set<unsigned int> &v)
  {
    std::ostringstream ss;
    std::string pname;

    for (std::set<unsigned int>::iterator i = v.begin(); i != v.end(); ++i)
    {
      ss << *i;
      if (getPortName (*i, pname))
        ss << " (" << pname << "), ";
      else
        ss << ", ";
    }

    std::string ports = ss.str();
    ports = std::string (ports, 0, ports.size () - 2);
    return ports;
  }

  void alertscanner::queueAlert (const inetaddr &addr, const std::set<unsigned int> &v)
  {
    std::string ports = makePortList (v);
    std::ostringstream sql;
    sql <<
    "select distinct a.id, d.default_template, d.default_subject, b.pattern, b.label \
    from alert_handlers a, alert_triggers b, alert_types d \
    where a.trigger_id = b.trigger_id \
    and d.name = 'OPEN_PORTS' \
    and b.active = 't' \
    and b.reference_table_name = 'scan_log'";

    PGresult *res = m_db.execQuery (sql.str().c_str());

    for (int i = 0; i < PQntuples(res); ++i)
    {
      netmon::alerthandler ah (&m_db);
      time_t timestamp = time (0);

      ah.addKeywordValue (netmon::alerthandler::eHostIP, addr.addr());
      ah.addKeywordValue (netmon::alerthandler::eLabel, PQgetvalue(res, i, 4));
      ah.addKeywordValue (alerthandler::eTime, ctime(&timestamp), true);

      ah.getMap()["srv_ip"] = std::string (addr.addr());
      ah.getMap()["ports"] = ports;

      if (strlen (PQgetvalue(res, i, 3)) > 0)
      {
        unsigned int p = atoi (PQgetvalue(res, i, 3));
        bool found = false;

        if (v.find(p) != v.end())
          found = true;

        if (found)
        {
          std::string pname;
          if (getPortName (p, pname))
          {
            std::ostringstream tmp;
            tmp << PQgetvalue(res, i, 3) << " (" << pname << ")";
            ah.addKeywordValue (netmon::alerthandler::eOpenPorts, tmp.str().c_str());
          }
          else
            ah.addKeywordValue (netmon::alerthandler::eOpenPorts, PQgetvalue(res, i, 3));
        }
        else
          continue;
      }
      else
        ah.addKeywordValue (netmon::alerthandler::eOpenPorts, ports.c_str());

      ah.setSubject (PQgetvalue(res, i, 2));
      int id = atoi (PQgetvalue(res, i, 0));
      ah.insertAlert (id, PQgetvalue(res, i, 1));
    }
    PQclear (res);
  }

  bool alertscanner::getPortName (unsigned int port, std::string &name)
  {
    portmap::iterator i = m_map.find (port);
    if (i == m_map.end ())
      return false;
    name = i->second;
    return true;
  }

  void alertscanner::reload ()
  {
    scanner::reload ();
    m_map.clear ();

    PGresult *res = m_db.execQuery ("select port, name from protocols where protocol = 'TCP'");

    for (int i = 0; i < PQntuples(res); ++i)
    {
      int p = atoi (PQgetvalue(res, i, 0));
      m_map[p] = std::string (PQgetvalue(res, i, 1));
    }

    PQclear (res);
  }
}
