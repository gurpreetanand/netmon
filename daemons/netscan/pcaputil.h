/*
 * pcaputil.h
 *
 * Copyright (c) 2001 Dug Song <dugsong@monkey.org>
 *
 * $Id$
 */

#ifndef PCAPUTIL_H
#define PCAPUTIL_H

int	pcap_dloff(pcap_t *pcap);

#endif /* PCAPUTIL_H */
