#include "netscand.h"
#include "dvars.h"
#include "alertscanner.h"
#include "log.h"

#include <string>
#include <signal.h>
#include <unistd.h>

namespace netmon
{
  netscan_daemon::netscan_daemon (int argc, char **argv)
    : daemon (argc, argv), m_found (0)
  {
  }

  netscan_daemon::~netscan_daemon ()
  {
    delete m_scanner;
  }

  void netscan_daemon::init ()
  {
    daemon::init();

    installSigHandler();

    dvars vars (*m_db, getDaemonName());
    vars.get ("found", m_found);

    m_scanner = new alertscanner (*m_db);

    event_init();
  }

  int netscan_daemon::run ()
  {
    while (!isTermSignaled())
    {
//      netmon::log::instance()->add(netmon::_eLogInfo, "New scan"); // commented on 14/12/2018 by sameer
      scan();
    }
    return 0;
  }

  void netscan_daemon::scan ()
  {
    m_scanner->reload();
    m_scanner->dump();

    int found = 0;
    time_t start = time (0);
    m_scanner->scan (found);
    time_t end = time (0);

    long tosleep;
    if ((unsigned) (end - start) < 30) // localnets is probably empty
      tosleep = 30;
    else
    {
      if ((unsigned) (end - start) > sleepTime)
        tosleep = 0;
      else
        tosleep = sleepTime - ((long) end - (long) start);
    }

    updateFound (found);
    usleep (tosleep); // sleep for no more that six hours
  }

  void netscan_daemon::installSigHandler ()
  {
    struct sigaction sa_new;

    sa_new.sa_handler = SIG_IGN;
    sa_new.sa_flags = SA_NOCLDSTOP;
    sigemptyset (&sa_new.sa_mask);
    sigaction (SIGCHLD, &sa_new, NULL);

    sa_new.sa_handler = sigHandler;
    sa_new.sa_flags = 0;
    sigaction (SIGALRM, &sa_new, 0);
  }

  void netscan_daemon::updateFound (int found)
  {
    if (m_found != 0)
      return;

    std::ostringstream sql;
    sql << "update daemonsconfig set value = '" << found << "' where var = '"
        << "found' and daemon_id = (select id from daemons where name = 'netscand')";

    try
    {
      m_db->execSQL (sql.str().c_str());
    }
    catch (db_exception &e)
    {
      printf ("Query failed: %s", e.what());
      log::instance()->add(netmon::_eLogError, e.what());
    }
  }

  void netscan_daemon::sigHandler (int)
  {
  }
}
