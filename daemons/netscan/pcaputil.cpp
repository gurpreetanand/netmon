#include <pcap.h>

#include "pcaputil.h"

int pcap_dloff (pcap_t *pd)
{
  int i;

  i = pcap_datalink (pd);

  switch (i)
  {
    case DLT_EN10MB:
      i = 14;
      break;
    case DLT_IEEE802:
      i = 22;
      break;
    case DLT_FDDI:
      i = 21;
      break;
#ifdef DLT_LOOP
    case DLT_LOOP:
#endif
    case DLT_NULL:
      i = 4;
      break;
    default:
      i = -1;
      break;
  }

  return i;
}
