#include "portscan.h"
#include "exception.h"
#include "pcaputil.h"

#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/poll.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <cstdlib>
#include <cstring>

#define DEFAULT_TIMEOUT 5000

namespace netmon
{
  portscanner::portscanner ()
  {
    m_errBuf = new char[PCAP_ERRBUF_SIZE];

    m_timeout.tv_sec = 0;
    m_timeout.tv_usec = DEFAULT_TIMEOUT;

    m_source.addr_ip = 0;
    m_sourceSet = false;

    m_ip = ip_open ();
    if (m_ip == 0)
      throw netmon_exception ("ip_open() failed");

    m_rand = rand_open ();

    m_iface = intf_open ();
    if (m_iface == 0)
      throw netmon_exception ("intf_open failed");

    m_init = false;
    m_finished = false;
    m_waitForTimeout = false;
    m_cycles = 0;
  }

  portscanner::~portscanner ()
  {
    delete[] m_errBuf;
    ip_close (m_ip);
    rand_close (m_rand);
    intf_close (m_iface);
    if (m_init)
    {
      pcap_close (m_pcap);
      event_del (&m_readEvent);
      event_del (&m_writeEvent);
    }
  }

  void portscanner::init ()
  {
    if (m_init)
      return;

    m_pcap = pcap_open_live ((char *) m_device.c_str(), 1500, 0, 500, m_errBuf);
    if (m_pcap == 0)
      throw netmon_exception ("pcap_open_live failed");

    m_pcapFD = pcap_fileno (m_pcap);
    m_dllen = pcap_dloff (m_pcap);

    setEvents ();
    m_init = true;
  }

  void portscanner::setEvents ()
  {
    event_set (&m_readEvent, m_pcapFD, EV_READ, doRead, (void *) this);
    event_add (&m_readEvent, &m_timeout);

    event_set (&m_writeEvent, m_pcapFD, EV_WRITE, doWrite, (void *) this);
    event_add (&m_writeEvent, &m_timeout);
  }

  void portscanner::scan (const portlist &pl, const char *host)
  {
    m_result.clear ();
    m_status.clear ();

    unsigned int size = pl.m_portlist.size ();
    if (size < 1)
      return;

    for (unsigned i = 0; i < size; ++i)
    {
      if (pl.m_portlist[i]->m_proto == service::_eTCP)
      {
        m_ports.push_back (pl.m_portlist[i]->m_port);
        m_status[pl.m_portlist[i]->m_port] = _eFiltered;
      }
    }

    m_mode = _ePortSet;
    if (m_ports.size() == 0)
      return;
    m_it = m_ports.begin();
    m_currPort = *m_it;
    scan (host, false);
  }

  void portscanner::scan (const char *host, bool scanall /* = true */)
  {
    getDevice (host);
    if (!m_sourceSet)
      getLocalAddress ();

    if (addr_aton (host, &m_destination) == -1)
      throw netmon_exception ("Illegal address");

    if (m_source.addr_ip == m_destination.addr_ip)
      m_device = "lo";

    init ();

    if (scanall)
    {
      m_result.clear ();
      m_status.clear ();
      m_mode = _eAllPorts;
      m_currPort = 1;
      for (unsigned int i = 0; i < 65536; ++i)
        m_status[i] = _eFiltered;
    }
    doRead (true);
    event_dispatch ();
  }

  void portscanner::doRead (int fd, short event, void *me)
  {
    portscanner *p = reinterpret_cast<portscanner *> (me);
    p->doRead (event & EV_TIMEOUT);
  }

  void portscanner::doWrite (int fd, short event, void *me)
  {
    portscanner *p = reinterpret_cast<portscanner *> (me);
    p->doWrite ();
  }

  void portscanner::onTimeout (int fd, short event, void *me)
  {
    portscanner *p = reinterpret_cast<portscanner *> (me);
    p->onTimeout ();
  }

  void portscanner::onTimeout ()
  {
    if (m_cycles > 2)
    {
      m_finished = true;
      return;
    }

    ++m_cycles;
    m_ports.clear ();

    for (std::map<unsigned int, portStatus>::iterator i = m_status.begin(); i != m_status.end(); ++i)
      if (i->second == _eFiltered)
        m_ports.push_back (i->first);

    if (m_ports.size () == 0)
    {
      m_finished = true;
      return;
    }

    m_mode = _ePortSet;
    m_it = m_ports.begin();
    m_currPort = *m_it;
    event_add (&m_readEvent, &m_timeout);
    event_add (&m_writeEvent, &m_timeout);
    evtimer_del (&m_toEvent);
    m_waitForTimeout = false;
  }

  void portscanner::doRead (bool timeout)
  {
    if (!hasMorePorts() && !m_waitForTimeout)
    {
      m_waitForTimeout = true;
      timeval to = { 1, 0 };
      evtimer_set (&m_toEvent, onTimeout, this);
      evtimer_add (&m_toEvent, &to);
    }
    if (m_waitForTimeout && m_finished)
      return;

    event_del (&m_readEvent);
    event_add (&m_readEvent, &m_timeout);

    if (timeout)
    {
      event_del (&m_writeEvent);
      event_add (&m_writeEvent, &m_timeout);
      for (unsigned int i = 0; i < 10; ++i)
        doWrite();
      return;
    }

    pcap_pkthdr ph;
    u_char *data;

    if ((data = (u_char *) pcap_next (m_pcap, &ph)) != 0)
    {
      if (handleData (data))
      {
        event_del (&m_writeEvent);
        event_add (&m_writeEvent, &m_timeout);
      }
    }
  }

  void portscanner::doWrite ()
  {
    static u_char buf[BUFSIZ];
    int len = IP_HDR_LEN + TCP_HDR_LEN;
    ip_hdr iph;
    tcp_hdr tcph;
    static int i = 1;

    if (!hasMorePorts())
      return;

    int dport = m_currPort;

    ip_pack_hdr (&iph, IP_TOS_LOWDELAY, len, rand_uint16 (m_rand), 0, 128, IP_PROTO_TCP,
                  m_source.addr_ip, m_destination.addr_ip);
    tcp_pack_hdr (&tcph, rand_uint16 (m_rand), dport,
		     rand_uint32 (m_rand), rand_uint32 (m_rand),
		     TH_SYN, 1024 * i, 0); //rand_uint16 (m_rand), 0);

    ++i;
    if (i > 4)
      i = 1;

    memcpy ((void *) buf, (void *) &iph, sizeof (iph));
    memcpy ((void *) (buf + sizeof (iph)), (void *) &tcph, sizeof (tcph));

    ip_checksum (buf, len);
    if (ip_send (m_ip, buf, len) < 0)
      printf ("ip_send in _send, short send");
    else
      nextPort();
  }

  void portscanner::nextPort ()
  {
    if (m_mode == _eAllPorts)
    {
      if (m_currPort > 65535)
        return;
      ++m_currPort;
    }
    else
    {
      ++m_it;
      if (m_it == m_ports.end())
        return;
      m_currPort = *m_it;
    }
  }

  bool portscanner::hasMorePorts ()
  {
    if (m_mode == _eAllPorts && m_currPort > 65535)
      return false;
    if (m_mode == _ePortSet && m_it == m_ports.end())
      return false;
    return true;
  }

  bool portscanner::handleData (u_char *data)
  {
    static ip_hdr *ip_h;
    addr ip_src;

    u_char *tmp = data + m_dllen;
    ip_h = (ip_hdr *) tmp;
    if (ip_h->ip_v != 4)
      return false;

    in_addr a;
    a.s_addr = ip_h->ip_src;
    addr_aton (inet_ntoa (a), &ip_src);

    if ((((addr_cmp (&ip_src, &m_destination)) == 0)) &&
        (ip_h->ip_p == IP_PROTO_TCP))
    {
      tcp_hdr *tcp_h = (struct tcp_hdr *) (tmp + IP_HDR_LEN);
      unsigned int port;

      switch (tcp_h->th_flags)
      {
        case 0x12:
          port = htons (tcp_h->th_sport);
          m_result.insert (port);
          m_status[port] = _eOpen;
          break;
        case 0x14:
          port = htons (tcp_h->th_sport);
          m_status[port] = _eClosed;
          break;
        default:
          break;
      }
      return true;
    }
    return false;
  }

  void portscanner::getDevice (const char *ip)
  {
    addr a;
    intf_entry e;

    memset ((void *) &e, 0, sizeof (e));
    e.intf_len = sizeof (e.intf_name);

    if (addr_aton (ip, &a) == -1)
      throw netmon_exception ("illegal address");
    if (intf_get_dst (m_iface, &e, &a) == -1)
      throw netmon_exception ("cannot get interface");

    m_device = std::string (e.intf_name);
  }

  void portscanner::getLocalAddress ()
  {
    intf_entry e;
    strcpy (e.intf_name, m_device.c_str());
    e.intf_len = sizeof (e);

    if (intf_get (m_iface, &e) == -1)
      throw netmon_exception ("cannot get local address");
    m_source = e.intf_addr;
  }
}

#ifdef __TEST__

#include <fstream>

const char *services = "/etc/services";

int main (int argc, char **argv)
{
  if (argc != 2)
    return -1;

  using namespace netmon;

  portlist plist;
  std::ifstream f (services);
  if (!f)
  {
    fprintf (stderr, "Cannot read %s\n", services);
    throw netmon_exception ("Cannot read services file.");
    exit (-1);
  }
  plist.addPorts (f);

  event_init ();
  try
  {
    portscanner *ps = new portscanner;
//    in_addr addr;
//    inet_aton ("10.0.2.26", &addr);
//    ps.setSourceAddress (addr.s_addr);
    ps->scan (plist, argv[1]);
    std::set <unsigned int> v = ps->getResults ();

    for (std::set<unsigned int>::iterator i = v.begin(); i != v.end(); ++i)
      printf ("%d\n", *i);

    printf ("done\n");
    delete ps;
  }
  catch (const netmon_exception &e)
  {
    printf ("%s\n", e.c_str());
  }

  return 0;
}

#endif // __TEST__
