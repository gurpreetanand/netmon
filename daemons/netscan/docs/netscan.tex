\documentclass{article}
\usepackage{tocloft}
\usepackage[pdftex]{graphicx}
\newcommand{\MyTabs}{ \hspace*{25.mm} \= \hspace*{25.mm} \= \hspace*{25.mm} \= \hspace*{25.mm} \= \hspace*{25.mm} \= \hspace*{25.mm} \kill }

\title {Netscan}
\author {Damir Ned\v zibovi\' c\\
e-mail: $<$damirn@gmail.com$>$\\
}
\date{08. February, 2005}      % Deleting this command produces today's date.


\begin{document}             % End of preamble and beginning of text.

\maketitle                   % Produces the title.

\tableofcontents

\newpage

\section {Main functionality}
\textbf {Netscan} is a simple TCP port scanner which uses \textbf {connect()}
 system call in order to determine whether a TCP port on a remote host is open
or not. All ports which are detected as open are printed on \textbf {stdout}
one per line. By default \textbf {Netscan} will scan only 'well known' ports
defined in \textbf {/etc/services}. If user wants to scan a specific port or
a range of ports it can instruct \textbf {Netscan} to do so. For better efficiency
\textbf {Netscan} tries to scan several ports at once (by default, 100 ports
are scanned at once). By using this method, \textbf {Netscan} is very fast - 
scanning of a host in local network which is not behind a firewall can last no
more than 1 second. Firewalled host can be scanned under 1 minute.

\section {Compiling and installing Netscan}
\textbf {Netscan} doesn't need any external libraries. In order to compile it
user needs to issue 'make' command from the command line prompt:
\vskip 0.5cm
\textbf {$[$root@localhost netscan$]$\# make}
\vskip 0.5cm
\noindent Complete \textbf {Makefile} can be found attached to this document
\footnote {See Appendix B.}.
In order to use \textbf {Netscan} from i.e. PHP scripts or as a non-root user,
\textbf {Netscan} needs to have suid bit set. As root issue these commands from
the command line prompt:
\vskip 0.5cm
\textbf {$[$root@localhost netscan$]$\# chown root netscan; chown u+s netscan}

\section {Using netscan}
\textbf {Netscan} has few command line options:
\begin {itemize}
\item $-$h $|$ $--$help Gives short help.
\item $-$v $|$ $--$version Prints version information.
\item $-$f $|$ $--$force Performs a force scan. Netscan will not send PING probes
before actual scan. Use with caution: scanning of dead or firewalled hosts can
take very long time.
\item $-$s $|$ $--$services $<$service-file$>$ Reads a list of well known ports
from $<$service-file$>$ instead of /etc/services.
\item $-$p $|$ $--$ports $<$port or port range$>$ Scans only the specified port
or port range which is in format \textbf {port1-port2}
\item $-$c $|$ $--$concurrency $<$number of ports$>$ How many ports should be 
scanned in one pass. Default value is 100, max value is 300. Basically, this 
number correspondents to a number of sockets which are created at once and used
for connect probe.
\end {itemize}
Some examples of usage of \textbf {Netscan:}
\vskip 0.5cm
\textbf {$[$root@localhost netscan$]$\# ./netscan 127.0.0.1}
\vskip 0.5cm
\noindent
\textbf {Netscan} will scan localhost (127.0.0.1) for well known ports. On
our box as a result we got:
\vskip 0.5cm
\noindent
Host is alive, ping time: 65\\
23\\
111\\
6000
\vskip 0.5cm
\noindent
The very first line of output tells us that localhost is up and that ping time
is 65ms. The following lines are ports which are detected as open on localhost
(in our case ports 23, 111 and 6000 are open).
\vskip 0.5cm
\textbf {$[$root@localhost netscan$]$\# ./netscan -p 10-30 127.0.0.1}
\vskip 0.5cm
\noindent
\textbf {Netscan} will scan localhost (127.0.0.1) for ports from 10 to 30. 
As a result we got:
\vskip 0.5cm
\noindent
Host is alive, ping time: 65\\
23
\vskip 0.5cm
\noindent
In this case only port 23 is detected as open.
\vskip 0.5cm
\textbf {$[$root@localhost netscan$]$\# ./netscan -p 1-3000 10.0.1.23}
\vskip 0.5cm
\noindent
\textbf {Netscan} will scan 10.0.1.23 for ports from 1 to 3000.
As a result we got:
\vskip 0.5cm
\noindent
Error: Host 10.0.1.23 is down.
\vskip 0.5cm
\noindent
Indeed, host 10.0.1.23 was down at that moment.
Sometimes we need to scan a host behind a firewall (i.e. a Windows XP box with
SP2 installed). In order to do it as fast as possible we could try something
like this:
\vskip 0.5cm
\textbf {$[$root@localhost netscan$]$\# ./netscan -c 250 192.168.209.12}
\vskip 0.5cm
\noindent
\textbf {Netscan} will scan 192.168.209.12 for well known ports using 250 sockets. 
As a result we got:
\vskip 0.5cm
\noindent
Host is alive, ping time: 139\\
80\\
139\\
691
\vskip 0.5cm
\noindent
We notice that the scan last some time more than usual, but we learn that ports
80, 139 and 691 are open (HTTP, netbios and MS Exchange).
Sometimes we need to scan a specific list of 
ports\footnote {See Appendix A for more details.} :
\vskip 0.5cm
\textbf {$[$root@localhost netscan$]$\# ./netscan -s my-list 10.0.2.202}
\vskip 0.5cm
\noindent
\textbf {Netscan} will scan 10.0.2.202 for ports listed in file \textbf {my-list}.
As a result we got:
\vskip 0.5cm
\noindent
Host is alive, ping time: 102\\
23\\
80\\
111\\
3306
\vskip 0.5cm

\newpage
\section {Netscan Flow Diagram}
\includegraphics[scale=0.65]{netscan.png}
\newpage
\section {Netscan internals}
\subsection {Command line parsing}
\textbf {Netscan} uses \it getopt\_long(3) \rm for command line parsing. We declare
a global array of supported options:
\vskip 0.5cm
\noindent
\verb+ static struct option const long_options[] = +\\
\verb+ {+\\
\verb+   {"help", no_argument, 0, 'h'},+\\
\verb+   {"force", no_argument, 0, 'f'},+\\
\verb+   {"version", no_argument, 0, 'v'},+\\
\verb+   {"services", required_argument, 0, 's'},+\\
\verb+   {"ports", required_argument, 0, 'p'},+\\
\verb+   {"concurrency", required_argument, 0, 'c'},+\\
\verb+   {0, 0, 0, 0}+\\
\verb+ };+
\vskip 0.5cm
\noindent
Parsing is done in \it main()\rm:
\vskip 0.5cm
\noindent
\verb+ while ((c = getopt_long (argc, argv, "hfvp:s:c:", long_options,+\\
\verb+                          &longind)) != EOF)+\\
\verb+ {+\\
\verb+   switch (c)+\\
\verb+   {+\\
\verb+     case 'h':+\\
\verb+       return usage ();+\\
\verb+     case 'f':+\\
\verb+       ...// use force scan;+\\
\verb+     case 'v':+\\
\verb+       return printversion ();+\\
\verb+     case 's':+\\
\verb+       services = optarg;+\\
\verb+       break;+\\
\verb+     case 'p':+\\
\verb+       ...+\\
\verb+       break;+\\
\verb+     case 'c':+\\
\verb+       conc = atoi (optarg);+\\
\verb+       if (conc < 1 || conc > 300)+\\
\verb+       {+\\
\verb+         printf ("Error: concurrency must be between+\\
\verb+                  1 and 300.\n");+\\
\verb+         exit (-1);+\\
\verb+       }+\\
\verb+       break;+\\
\verb+     dafault:+\\
\verb+       printf ("Illegal argument, use --help for help\n");+\\
\verb+       exit (-1);+\\
\verb+   }+\\
\verb+ }+
\vskip 0.5cm
\subsection {Implementation of list of ports to be scanned}
\textbf {/etc/services} provides more information on well known ports than
just a number: we can get a service name, a port number, a protocol and even
an alias. \textbf {Netscan} collects all that data in internal structure:
\vskip 0.5cm
\noindent
\verb+ struct service+\\
\verb+ {+\\
\verb+   enum eproto {_eTCP, _eUDP};+\\
\verb+   service (const char *name, unsigned int port, eproto proto)+\\
\verb+    : m_name (name), m_port (port), m_proto (proto)+\\
\verb+   {+\\
\verb+   }+\\
\verb+   std::string m_name;+\\
\verb+   unsigned int m_port;+\\
\verb+   eproto m_proto;+\\
\verb+ };+
\vskip 0.5cm
\noindent
Now we implement this list as \it std::vector \rm:
\vskip 0.5cm
\noindent
\verb+ typedef std::vector<service *> servicevec;+
\vskip 0.5cm
\subsection {Event demultiplexing}
\textbf {Netscan} tries to be fast and efficient, so it scans a large number
of ports at a time. It uses \it connect(2) \rm system call to try to connect
to a port which is scanned. If the connection is successful, the port is marked
as open; if the connection if refused, the port is marked as closed. But what
happens if the port is filtered by a firewall? Timeout can be very large in that
case and knowing that many ports must be scanned for a specific network host
this brings an answer to the question "why are so many ports scanned at a time?"

When dealing with many sockets at a time, the problem is how to demultiplex
events in an efficient way. We can use \it select(2) \rm or \it poll(2) \rm 
system calls, but they don't scale very well when it comes to several hundreds
of sockets. \textbf {Netscan} chooses POSIX real-time signals for efficient
event demultiplexing. We create sockets (\it socket(2)\rm) and put them in 
non-blocking mode (\it fcntl(2)\rm):
\vskip 0.5cm
\noindent
\verb+ int s, flags;+\\
\verb+ s = socket (AF_INET, SOCK_STREAM, 0);+\\
\verb+ flags = fcntl (s, F_GETFL);+\\
\verb+ flags |= O_NONBLOCK | O_ASYNC;+\\
\verb+ fcntl (s, F_SETFL, flags);+
\vskip 0.5cm
\noindent
We also set owner of the socket and assign it a real-time signal:
\vskip 0.5cm
\noindent
\verb+ fcntl (s, F_SETOWN, (int) getpid ());+\\
\verb+ fcntl (s, F_SETSIG, SIGRTMAX);+
\vskip 0.5cm
\noindent
When a socket is 'prepared' in this way, \it connect(2) \rm will return
immediately with an error, while \it errno \rm is set to \it EINPROGRESS \rm.
To receive signal notification we set process signal mask:
\vskip 0.5cm
\noindent
\verb+ sigset_t sig;+\\
\verb+ sigemptyset (&sig);+\\
\verb+ sigaddset (&sig, SIGRTMAX);+\\
\verb+ sigaddset (&sig, SIGIO);+\\
\verb+ sigprocmask (SIG_BLOCK, &sig, NULL);+
\vskip 0.5cm
\noindent
We use \it sigtimedwait(2) \rm to wait for event notification and set timeout
to 5 seconds. There is a possible problem here: if we connect to a port on
which listens a service with a 'greeting message' (try to telnet to SSH port
on some UNIX box to see what's going on) we will receive two signals. The first
one will notify us that the connection has been successful, and the second one
will try to 'tell' us that there are data to be read from the port. We will
be smart and will ignore the second signal. After the real-time signal has
been delivered we'll use \it getsockopt(2) \rm to find out whether the connection
has been successful or not:
\vskip 0.5cm
\noindent
\verb+ int j;+\\
\verb+ int ret, siz = sizeof (int);+\\
\verb+ const struct timespec timeout = {5, 0}; // timeout 5 secs+\\
\verb+ +\\
\verb+ j = sigtimedwait (sig, &sinfo, &timeout);+\\
\verb+ if (j == -1)+\\
\verb+ {+\\
\verb+   if (errno == EAGAIN)+\\
\verb+   {+\\
\verb+     break; // timeout+\\
\verb+   }+\\
\verb+ }+\\
\verb+ else+\\
\verb+ {+\\
\verb+   if (sinfo.si_band & POLLIN) // there's data on this port+\\
\verb+     continue; // ignore it+\\
\verb+   if (getsockopt (sinfo.si_fd, SOL_SOCKET, SO_ERROR, +\\
\verb+       (void *)&ret, (socklen_t *) &siz) < 0)+\\
\verb+   {+\\
\verb+     printf ("getsockopt error (sock %d).\n", sinfo.si_fd);+\\
\verb+     perror ("");+\\
\verb+     break;+\\
\verb+   }+\\
\verb+   if (ret == 0)+\\
\verb+   {+\\
\verb+     int flags;+\\
\verb+     // before we close this socket we'll remove us from+\\
\verb+     // any further notifications+\\
\verb+     if (fcntl (it->first, F_GETFL, &flags) != -1)+\\
\verb+     {+\\
\verb+       flags &= ~O_ASYNC;+\\
\verb+       fcntl (it->first, F_SETFL, flags);+\\
\verb+     }+\\
\verb+     close (it->first);+\\
\verb+     printf ("%d\n", smap[sinfo.si_fd]);+\\
\verb+   }+
\vskip 0.5cm
\noindent
\section {Appendix A - format of well-known port list}
This format follows the format of \textbf {/etc/services}, where each line
describes one service, and is of the form:
\vskip 0.5cm
\textbf {service-name  port/protocol  [aliases ...]   [\# comment]}
\vskip 0.5cm
\noindent
Here is a small piece of our \textbf {/etc/services}:
\bf
\begin{tabbing} \MyTabs
ftp \> 21/tcp\\
ftp \> 21/udp \>         fsp fspd\\
ssh \> 22/tcp \> \>                        \# SSH Remote Login Protocol\\
ssh \> 22/udp \> \>                        \# SSH Remote Login Protocol\\
telnet \> 23/tcp\\
telnet \> 23/udp
\end{tabbing}
\rm
A more complete list can be found at \it{http://www.iana.org/assignments/port-numbers}

\section {Appendix B - netscan Makefile}
\verb+ CPP = g+++\\
\verb+ CPPFLAGS = -O2 -pipe -g +\\
\verb+ LIBS =+\\
\verb+ LDFLAGS = +\\
\verb+ OBJS = netscan.o+\\
\verb+ BIN = netscan+\\
\verb+ +\\
\verb+ %.o : %.cpp +\\
\verb+         $(CPP) $(CPPFLAGS) -o $@ -c $<  +\\
\verb+  +\\
\verb+ $(BIN) : $(OBJS)  +\\
\verb+         $(CPP) -o $(BIN) $(OBJS) $(LIBS) $(LDFLAGS)  +\\
\verb+  +\\
\verb+ clean:  +\\
\verb+         rm -f $(OBJS) $(BIN)  +

\end{document}
