\contentsline {section}{\numberline {1}Main functionality}{2}
\contentsline {section}{\numberline {2}Compiling and installing Netscan}{2}
\contentsline {section}{\numberline {3}Using netscan}{2}
\contentsline {section}{\numberline {4}Netscan Flow Diagram}{5}
\contentsline {section}{\numberline {5}Netscan internals}{6}
\contentsline {subsection}{\numberline {5.1}Command line parsing}{6}
\contentsline {subsection}{\numberline {5.2}Implementation of list of ports to be scanned}{7}
\contentsline {subsection}{\numberline {5.3}Event demultiplexing}{7}
\contentsline {section}{\numberline {6}Appendix A - format of well-known port list}{9}
\contentsline {section}{\numberline {7}Appendix B - netscan Makefile}{10}
