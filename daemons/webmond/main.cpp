#include <string>
#include <cstring>

#include "webmond.h"
#include "exception.h"
#include "log.h"

int main (int argc, char **argv)
{
  try
  {
    netmon::webmon_daemon *w = new netmon::webmon_daemon (argc, argv);
    w->init();
    w->start();
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    return -1;
  }
  catch (int e)
  {
    printf ("Error: %s\n", strerror (e));
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, strerror (e));
    return -1;
  }

  return 0;
}
