#include "webmonitor.h"

#include <curl/curl.h>
// #include <curl/types.h>
#include <curl/easy.h>
#include <pcrecpp.h>
#include <sys/time.h>

#include <sstream>

#include "alerts.h"
#include "urlalertmngr.h"
#include "netmon.h"
#include "db.h"
#include "dvars.h"
#include "webmond.h"

#include <unistd.h>

namespace netmon
{
  webmonitor::webmonitor (db *mydb, const std::string &proxy, webmon_daemon *d)
    : m_db (mydb), m_proxy (proxy), m_daemon (d)
  {
    init();
  }

  webmonitor::~webmonitor ()
  {
    m_db->close();
    delete[] m_buf;
    delete m_alertManager;
    curl_global_cleanup();
  }

  void webmonitor::run ()
  {
    while (!m_daemon->termSignaled())
    {
      getURLs();
      check();
      usleep (30); // sleep 30 seconds
      m_alertManager->reload();
    }
  }

  void webmonitor::init ()
  {
    m_db->execSQL ("UPDATE URLS SET TIMESTAMP = 0");

    m_buf = new unsigned char[MaxSize];
    curl_global_init (CURL_GLOBAL_ALL);
    m_alertManager = new url_alert_manager (m_db);
    m_alertManager->init();
  }

  void webmonitor::getURLs ()
  {
    std::ostringstream sql;
    sql << "SELECT ID, URL, PATTERN, ENABLE_LOGGING FROM URLS WHERE (TIMESTAMP + INTERVAL) <= "
        << (int) time (0);

    PGresult *res = m_db->execQuery (sql.str().c_str());
    printf ("Got %d services to check\n", PQntuples (res));

    m_urls.clear();
    for (int i = 0; i < PQntuples (res); ++i)
    {
      urlentry e;
      e.m_id = atoi (PQgetvalue (res, i, 0));
      e.m_url = std::string (PQgetvalue (res, i, 1));
      e.m_pattern = std::string (PQgetvalue (res, i, 2));
      e.m_log = *PQgetvalue (res, i, 3) == 't' ? true : false;
      m_urls.push_back (e);
    }
    PQclear (res);
  }

  void webmonitor::check ()
  {
    for (urllist::iterator i = m_urls.begin (); i != m_urls.end (); ++i)
    {
      check (*i);
    }
  }

  void webmonitor::check (const urlentry &e)
  {
    CURL *curl_handle;
    char err[CURL_ERROR_SIZE] = "\0";

printf ("checking %s\n", e.m_url.c_str());

    m_size = 0;

    /* init the curl session */
    curl_handle = curl_easy_init();

    /* specify URL to get */
    curl_easy_setopt (curl_handle, CURLOPT_URL, e.m_url.c_str());

    curl_easy_setopt (curl_handle, CURLOPT_ERRORBUFFER, err);

    /* send all data to this function  */
    curl_easy_setopt (curl_handle, CURLOPT_WRITEFUNCTION, handleData);

    curl_easy_setopt (curl_handle, CURLOPT_WRITEDATA, (void *) this);

    curl_easy_setopt (curl_handle, CURLOPT_TIMEOUT, 60);

    /* some servers don't like requests that are made without a user-agent
       field, so we provide one */
    curl_easy_setopt (curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    curl_easy_setopt (curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_easy_setopt (curl_handle, CURLOPT_FOLLOWLOCATION, 1);

    // Accept all SSL certs
    curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYPEER, 0);

    if (m_proxy.size() > 0)
      curl_easy_setopt (curl_handle, CURLOPT_PROXY, m_proxy.c_str());

    /* get it! */
    timeval tstart;
    gettimeofday (&tstart, 0);

    CURLcode ret = curl_easy_perform (curl_handle);
    timeval tend;
    gettimeofday (&tend, 0);
    int latency = (tend.tv_usec - tstart.tv_usec) + (tend.tv_sec - tstart.tv_sec) * 1000000;

    if (ret != 0)
    {
      updateDB (e, eDown, err, latency);
      checkAlerts (e, false); // check alerts
    }
    else
    {
      if (m_size < MaxSize)
        m_buf[m_size] = '\0';

      if (match (e))
      {
        updateDB (e, eMatch, (char *) "Site OK", latency);
        checkAlerts (e, true);
      }
      else
      {
        updateDB (e, eNoMatch, (char *) "Site failure", latency);
        checkAlerts (e, false);
      }
    }

    /* cleanup curl stuff */
    curl_easy_cleanup (curl_handle);
  }

  bool webmonitor::match (const urlentry &e)
  {
    pcrecpp::RE_Options opt;
    opt.set_caseless (true);
    pcrecpp::RE ptrn (e.m_pattern, opt);
    if (ptrn.PartialMatch ((const char *) m_buf))
      return true;
    return false;
  }

  void webmonitor::checkAlerts (const urlentry &e, bool isok)
  {
    values_map vmap;
    vmap[alerthandler::eHostIP] = keyword_value (e.m_url);
    vmap[alerthandler::ePattern] = keyword_value (e.m_pattern);
    m_alertManager->getAlerts (e.m_id, isok, vmap);
  }

  void webmonitor::updateDB (const urlentry &e, status st, char *err, int latency)
  {
    char *esc = new char[strlen (err) * 2 + 1];
    PQescapeString (esc, err, strlen (err));
    if (strlen (esc) > 63)
      esc[63] = '\0';

    std::ostringstream sql;
    sql << "UPDATE URLS SET TIMESTAMP = " << (int) time(0)
        << ", LATENCY = " << latency;

    if (st == eMatch)
      sql << ", STATUS = 'MATCH', MESSAGE = ''";
    else
    {
      sql << ", STATUS = '";
      if (st == eNoMatch)
        sql << "NO MATCH";
      else
        sql << "DOWN";
      sql << "', MESSAGE = '" << esc << "'";
    }

    sql << " WHERE ID = " << e.m_id;

    m_db->execSQL (sql.str().c_str());
    sql.str("");

    if (e.m_log)
    {
      sql << "INSERT INTO URL_LOG (URL_ID, TIMESTAMP, MESSAGE, LATENCY, STATUS) VALUES ("
          << e.m_id << ", " << (int) time(0) << ", '";
      if (st != eMatch)
        sql << esc;
      sql << "', " << latency;
      if (st == eMatch)
        sql << ", 'MATCH'";
      else
        if (st == eNoMatch)
          sql << ", 'NO MATCH'";
        else
          sql << ", 'DOWN'";
      sql << ")";

      m_db->execSQL (sql.str().c_str());
    }

    delete[] esc;
  }

  size_t webmonitor::handleData (void *ptr, size_t size, size_t nmemb, void *data)
  {
    webmonitor *w = reinterpret_cast<webmonitor *> (data);
    size_t realsize = size * nmemb;
    return w->handleDataEx (ptr, realsize);
  }

  size_t webmonitor::handleDataEx (void *ptr, size_t size)
  {
    if (m_size + size >= MaxSize)
      return 0;

    memcpy ((void *) (m_buf + m_size), ptr, size);
    m_size += size;

    return size;
  }
}
