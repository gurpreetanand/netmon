#ifndef __WEB_MONITOR__
#define __WEB_MONITOR__

#include <list>
#include <string>

namespace netmon
{
  class db;
  class url_alert_manager;
  class webmon_daemon;

  class webmonitor
  {
    struct urlentry
    {
      int m_id;
      std::string m_url;
      std::string m_pattern;
      bool m_log;
    };

    enum status { eMatch, eNoMatch, eDown };

    public:
      webmonitor (db *, const std::string &, webmon_daemon *);
      ~webmonitor ();

      void run ();

    protected:
      void init ();
      void getURLs ();
      void check ();
      void check (const urlentry &);
      void checkAlerts (const urlentry &, bool);
      void updateDB (const urlentry &, status, char *, int);
      bool match (const urlentry &);

      static size_t handleData (void *, size_t, size_t, void *);
      size_t handleDataEx (void *, size_t);

      typedef std::list<urlentry> urllist;
      urllist m_urls;
      db *m_db;
      url_alert_manager *m_alertManager;
      std::string m_proxy;

      unsigned char *m_buf;
      unsigned int m_size;
      webmon_daemon *m_daemon;
      enum { MaxSize = 1024*1024 };
  };
}

#endif // __WEB_MONITOR__
