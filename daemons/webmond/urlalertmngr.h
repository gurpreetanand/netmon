#ifndef __URL_ALERT_MANAGER__
#define __URL_ALERT_MANAGER__

#include "alertmanager.h"

namespace netmon
{
  class conditional_manager;

  class url_alert_manager : public alert_manager
  {
    public:
      url_alert_manager (db *);
      ~url_alert_manager ();

      unsigned int getAlerts (unsigned int, bool, const values_map &);

      virtual void reload ();

    protected:
      virtual void getReferenceTable (std::string &table)
      {
        table = "urls";
      }

      virtual void getOverName (std::string &name)
      {
        name = "WEB_ALERT_UP";
      }

      virtual void getBelowName (std::string &name)
      {
        name = "WEB_ALERT_DOWN";
      }

      virtual bool isAlertValid (alert_data *);

      conditional_manager *m_condManager;
  };
}

#endif // __URL_ALERT_MANAGER__
