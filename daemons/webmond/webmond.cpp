#include "webmond.h"
#include "webmonitor.h"
#include "dvars.h"

#include <string>

namespace netmon
{
  webmon_daemon::webmon_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  webmon_daemon::~webmon_daemon ()
  {
    delete m_webMonitor;
  }

  void webmon_daemon::init ()
  {
    daemon::init();
    std::string proxy("");
    m_vars->get ("proxy", proxy);
    m_webMonitor = new webmonitor (m_db, proxy, this);
  }

  int webmon_daemon::run ()
  {
    m_webMonitor->run();
    return 0;
  }
}
