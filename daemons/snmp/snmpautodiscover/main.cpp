#include <errno.h>
#include <sys/types.h>

#include <string>
#include <cstdio>
#include <cstring>

#include "snmpautod.h"
#include "exception.h"
#include "log.h"

int main (int argc, char **argv)
{
  try
  {
    netmon::snmp_autod *d = new netmon::snmp_autod (argc, argv);
    d->init ();
    d->start ();
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
  }
  catch (int e)
  {
    errno = e;
    perror ("snmpautodiscoverd");
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, strerror (e));
  }
}
