#include "oidalertmgr.h"
#include <pcrecpp.h>

namespace netmon
{
  bool oid_alert_manager::match (const char *pattern, const char *value)
  {
    pcrecpp::RE_Options opt;
    opt.set_caseless (true);
    pcrecpp::RE ptrn (pattern, opt);
    if (ptrn.PartialMatch ((const char *) value))
      return true;
    return false;
  }
}
