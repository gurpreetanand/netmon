#ifndef __OID_ALERT_MANAGER__
#define __OID_ALERT_MANAGER__

#include "alertmanager.h"

namespace netmon
{
  class oid_alert_manager : public alert_manager
  {
    public:
      oid_alert_manager (db *mydb)
        : alert_manager (mydb)
      {}

      virtual ~oid_alert_manager ()
      {}

    protected:
      virtual void getReferenceTable (std::string &table)
      {
        table = "oids";
      }

      virtual void getOverName (std::string &name)
      {
        name = "SNMP_OID_NO_MATCH";
      }

      virtual void getBelowName (std::string &name)
      {
        name = "SNMP_OID_MATCH";
      }

      virtual bool match (const char *, const char *);
  };
}

#endif // __OID_ALERT_MANAGER__
