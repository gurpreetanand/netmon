#ifndef __OID_DAEMON__
#define __OID_DAEMON__

#include "daemon.h"
#include "alertutil.h"
#include "memcache.h"
#include "walkfunctor.h"
#include "../common/snmp_ignores.h"

#include <list>
#include <string>
#include <jsoncpp/json/value.h>

namespace netmon
{
  class snmp;
  class alert_manager;
  class oid_translator;
  class alerthandler;

  class oid_daemon : public daemon
  {
    struct oid_data
    {
      std::string m_ip;
      std::string m_community;
      std::string m_oid;
      std::string m_msg;
      unsigned short m_port;
      bool m_log;
      unsigned int m_id;
      bool m_isNumeric;
      bool m_isWalk;
    };

    public:
      oid_daemon ();
      oid_daemon (int, char **);
      ~oid_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "oidmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/oidmond";
      }
      
      typedef std::map<std::string, oid_data*> oidmap;

      void setOIDs ();
      void checkOIDs ();
      void getDashboardOIDs (oidmap& m_toAdd);
      void setDashboardWalks();
      void getTrackedOIDs (oidmap& m_toAdd);

      void getOID (oid_data *);
      void walkOID (oid_data *);
      void clearLists ();
      void logOID (oid_data *, const std::string &);
      void insertValues (values_map &, oid_data *, const std::string &);
      std::string getKey(oid_data *);

      typedef std::list<oid_data *> oid_list;
      typedef std::map<std::string, oid_data*> oid_map;
      oid_list m_oids;
      oid_list m_walks;
      
      Json::Value root;

      snmp *m_snmp;
      alert_manager *m_alertMgr;
      oid_translator *m_oidTranslator;
      snmp_walk_functor m_functor;

      Memcacher m_memcache;

      SnmpIgnores m_ignores;
  };
}

#endif // __OID_DAEMON__
