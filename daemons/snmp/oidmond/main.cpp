#include <unistd.h>

#include "oidd.h"
#include "log.h"
#include "exception.h"

int main(int argc, char **argv)
{
  try
  {
    netmon::oid_daemon *t = new netmon::oid_daemon (argc, argv);
    t->init();
    t->start();
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
   std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
  }
}
