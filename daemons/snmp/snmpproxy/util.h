#ifndef __SNMP_PROXY_UTIL__
#define __SNMP_PROXY_UTIL__

#include <map>

namespace netmon
{
  namespace util
  {
    void saveOutputFlags (std::map<int, int> &, int &, int &);
    void restoreOutputFlags (const std::map<int, int> &, int, int);
  }
}

#endif // __SNMP_PROXY_UTIL__
