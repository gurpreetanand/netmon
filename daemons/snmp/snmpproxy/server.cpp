#include "server.h"
#include "client.h"
#include "util.h"
#include "snmpproxyd.h"

#include <cstring>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/stat.h>

extern int running;

namespace netmon
{
  server::server (const std::string &path, const std::string &mib_path, snmp_proxyd *d)
    : m_path (path), m_mibPath (mib_path), m_daemon (d)
  {
    createSocket();
  }

  server::server (const char *path, const char *mib_path, snmp_proxyd *d)
    : m_path (path), m_mibPath (mib_path), m_daemon (d)
  {
    createSocket();
  }

  server::~server ()
  {
    close (m_fd);
    unlink (m_path.c_str());
  }

  void server::createSocket ()
  {
    unlink (m_path.c_str()); // remove old socket
    m_fd = socket (PF_LOCAL, SOCK_STREAM, 0);
    if (m_fd < 0)
      throw server_exc (strerror (errno));
    setFlags (m_fd);

    struct sockaddr_un name;
    // Indicate that this is a server.
    name.sun_family = AF_LOCAL;
    strcpy (name.sun_path, m_path.c_str());
    if (bind (m_fd, (const sockaddr *) &name, SUN_LEN (&name)) < 0)
      throw server_exc (strerror (errno));

    // make it world readable
    chmod (m_path.c_str(), 0777);

    // Listen for connections.
    listen (m_fd, 5);
  }

  void server::setFlags (int fd)
  {
    int flags;
    flags = fcntl (fd, F_GETFL);
    flags |= O_NONBLOCK | O_ASYNC;
    fcntl (fd, F_SETFL, flags);
  }

  void server::run ()
  {
    setenv ("MIBS", "ALL", 1);
    netsnmp_set_mib_directory (m_mibPath.c_str());
    setParseOpts();
    util::saveOutputFlags (m_outputFlags, m_outputStringFormat, m_outputOIDFormat);

    while (!m_daemon->termSignaled())
    {
      int max = 0;
      int block = 1;
      timeval to;

      FD_ZERO (&m_readSet);
      FD_ZERO (&m_writeSet);
      snmp_select_info (&max, &m_readSet, &to, &block);

      if (m_fd > max)
        max = m_fd;
      FD_SET (m_fd, &m_readSet);
      for (client_t::iterator i = m_clients.begin(); i != m_clients.end(); ++i)
      {
        if (i->first > max)
          max = i->first;
        FD_SET (i->first, &m_readSet);
        if (i->second->writeWillBlock())
          FD_SET (i->first, &m_writeSet);
      }
      max = select (max + 1, &m_readSet, &m_writeSet, NULL, block ? NULL : &to);
      if (max < 0 && errno != EINTR)
        throw server_exc (strerror (errno));
      if (max > 0)
      {
        handleReads();
        handleWrites();
      }
      else
        handleTimeouts();
    }
    printf ("exiting...\n");
  }

  void server::handleReads ()
  {
    if (FD_ISSET (m_fd, &m_readSet))
      handleAccept();
    for (client_t::iterator i = m_clients.begin(); i != m_clients.end(); ++i)
    {
      if (FD_ISSET (i->first, &m_readSet))
        handleRead (i->first);
    }
    snmp_read (&m_readSet);
    removeOldSessions();
  }

  void server::handleWrites ()
  {
    for (client_t::iterator i = m_clients.begin(); i != m_clients.end(); ++i)
    {
      if (FD_ISSET (i->first, &m_writeSet))
        if (i->second->write() == -1)
          i->second->end();
    }
    removeOldSessions();
  }

  void server::removeOldSessions ()
  {
    for (session_t::iterator i = m_sessions.begin(); i != m_sessions.end(); )
    {
      if (i->second->isDone() && !i->second->hasPendingData())
      {
        snmp_close (i->first);
        printf ("removing fd: %d\n", i->second->getFD());
        deleteSession (i->second);
        m_sessions.erase (i++);
      }
      else
        ++i;
    }
    for (client_t::iterator i = m_clients.begin(); i != m_clients.end();)
    {
      if (i->second->isDone() && !i->second->hasPendingData())
      {
        printf ("killing fd: %d\n", i->second->getFD());
        deleteSession (i++->second);
      }
      else
        ++i;
    }
  }

  void server::handleRead (int fd)
  {
    client_t::iterator i = m_clients.find (fd);
    if (i == m_clients.end())
      return;
    client *c = i->second;
    int ret = c->read();
    if (ret > 0 && c->parseInput())
      createSession (c);
  }

  void server::handleTimeouts ()
  {
    snmp_timeout();
    removeOldSessions();
  }

  void server::handleAccept ()
  {
    sockaddr_un cl;
    socklen_t client_len = sizeof (cl);

    int fd = accept (m_fd, (sockaddr *) &cl, &client_len);
    if (fd == -1)
    {
      perror ("err");
      return;
    }

    fprintf (stderr, "connection accepted (fd: %d)\n", fd);

    setFlags (fd);

    client *c = new client (fd);
    m_clients[fd] = c;
  }

  void server::createSession (client *c)
  {
    snmp_session *s = c->getSession();
    int argc = c->getArgc();
    char **argv = c->getArgs();

    if (strcmp (argv[0], "snmpget") == 0)
      c->setType (client::_eSNMP_GET);
    else if (strcmp (argv[0], "snmpwalk") == 0)
      c->setType (client::_eSNMP_WALK);

    if (argc < 2) // no args given
    {
      write (c->getFD(), (void *) "missing args\n", 14);
      c->done();
      return;
    }

    int arg;

    optind = 0;
    opterr = 1;
    optopt = '?';

    util::restoreOutputFlags (m_outputFlags, m_outputStringFormat, m_outputOIDFormat);

    switch (arg = snmp_parse_args (argc, argv, s, "C:", processArgs))
    {
      case -2: // error, fixme
        c->done();
        break;
      case -1: // illegal option, fixme
        c->done();
        return;
      default:
        break;
    }

    if (arg >= argc) // no OIDs given
    {
      write (c->getFD(), (void *) "missing object name\n", 21);
      c->done();
      return;
    }

    parseOIDs (c, arg, argc, argv);

    reparseArgs (argc, c->getArgs());

    if ((c->getType() == client::_eSNMP_GET && !c->hasMoreOIDs()) ||
        (c->getType() == client::_eSNMP_WALK && !c->isRootValid())) // no valid OIDs, remove this client
    {
      c->done();
      return;
    }

    c->saveOutputFlags();
    s->callback = callback;
    s->callback_magic = this;
    snmp_session *ws = snmp_open (s); // fixme: check errors
    m_sessions[ws] = c;

    snmp_pdu *req = createPDU (c);
    OID *o = c->getNextOID();
    snmp_add_null_var (req, o->m_oid, o->m_len);
    if (!snmp_send (ws, req))
    {
      snmp_perror ("snmp_send");
      snmp_free_pdu (req);
    }
  }

  void server::parseOIDs (client *c, int arg, int argc, char **argv)
  {
    if (c->getType() == client::_eSNMP_WALK)
    {
      OID o;
      o.m_name = std::string (argv[arg]);
      o.m_len = MAX_OID_LEN;
      if (!snmp_parse_oid (argv[arg], o.m_oid, &(o.m_len)))
      {
        snmp_perror (argv[arg]);
        const char *err = snmp_api_errstring (snmp_errno);
        if (err != 0)
        {
          if (write (c->getFD(), (void *) err, strlen (err)) == -1)
          {
            c->end();
            return;
          }
        }
      }
      else
      {
        c->setRootOID(o);
        c->rootValid();
      }
    }
    OID *o;
    for (; arg < argc; ++arg) // parse all OIDs
    {
      o = new OID;
      o->m_name = std::string (argv[arg]);
      o->m_len = MAX_OID_LEN;
      fprintf (stderr, "oid: %s\n", argv[arg]);
      if (!snmp_parse_oid (argv[arg], o->m_oid, &(o->m_len)))
      {
        snmp_perror (argv[arg]);
        const char *err = snmp_api_errstring (snmp_errno);
        if (err != 0)
        {
          if (write (c->getFD(), (void *) err, strlen (err)) == -1)
          {
            c->end();
            return;
          }
        }
        delete o;
      }
      else
        c->addOID (o);
    }
  }

  void server::deleteSession (client *c)
  {
    close (c->getFD());
    m_clients.erase(c->getFD());
    delete c;
  }

  snmp_pdu *server::createPDU (client *c)
  {
    switch (c->getType())
    {
      case client::_eSNMP_GET:
        return snmp_pdu_create (SNMP_MSG_GET);
      case client::_eSNMP_WALK:
        return snmp_pdu_create (SNMP_MSG_GETNEXT);
      default:
        return 0;
    }
    return 0; // cannot be reached
  }

  int server::callback (int op, snmp_session *s, int, snmp_pdu *pdu, void *me)
  {
    server *srv = reinterpret_cast<server *> (me);
    return srv->onData (op, s, pdu);
  }

  int server::onData (int op, snmp_session *s, snmp_pdu *pdu)
  {
//    fprintf (stderr, "onData()\n");
    session_t::iterator i = m_sessions.find(s);
    if (i == m_sessions.end())
      return 0;
    client *c = i->second;
    if (op == NETSNMP_CALLBACK_OP_RECEIVED_MESSAGE)
    {
      if (c->getType() == client::_eSNMP_WALK)
        return handleWalk(c, s, pdu);
      if (pdu->errstat == SNMP_ERR_NOERROR)
      {
        c->restoreOutputFlags();
        for (netsnmp_variable_list *vars = pdu->variables; vars; vars = vars->next_variable)
        {
          int wrote = snprint_variable (
            c->getWriteBuffer(), c->getWriteBuffSize(),
            vars->name, vars->name_length, vars);
          c->wroteBytes (wrote);
          *(c->getWriteBuffer()) = '\n';
          c->wroteBytes (1);
        }
        if (c->write() == -1)
        {
          c->end();
          return 1;
        }
      }
      OID *o = c->getNextOID();
      if (o != 0)
      {
        snmp_pdu *req = createPDU (c);
        snmp_add_null_var (req, o->m_oid, o->m_len);
        if (snmp_send (s, req))
          return 1;
        else
          snmp_free_pdu (req);
      }
    }
    if (!c->hasMoreOIDs() || op == NETSNMP_CALLBACK_OP_TIMED_OUT)
      c->done();
    return 1;
  }

  int server::handleWalk (client *c, snmp_session *s, snmp_pdu *pdu)
  {
    if (pdu->errstat == SNMP_ERR_NOSUCHNAME)
    {
      c->done();
      return 1;
    }
    else if (pdu->errstat != SNMP_ERR_NOERROR)
    {
      int ret = snprintf (c->getWriteBuffer(), c->getWriteBuffSize(),
                "Error in packet.\nReason: %s\n", snmp_errstring (pdu->errstat));
      c->wroteBytes (ret);
      if (!c->writeWillBlock())
        if (c->write() == -1)
        {
          c->end();
          return 1;
        }
      c->done();
      return 1;
    }

    c->restoreOutputFlags();
    const OID &o = c->getRootOID();
    for (netsnmp_variable_list *vars = pdu->variables; vars; vars = vars->next_variable)
    {
      if ((vars->name_length < c->getRootSize()) ||
          (memcmp (o.m_oid, vars->name, c->getRootSize() * sizeof (oid)) != 0))
      {
        // no more entries in this tree
        c->done();
        return 1;
      }

      // send data to client
      int wrote = snprint_variable (
        c->getWriteBuffer(), c->getWriteBuffSize(),
        vars->name, vars->name_length, vars);
      c->wroteBytes (wrote);
      *(c->getWriteBuffer()) = '\n';
      c->wroteBytes (1);
      if (!c->writeWillBlock())
        if (c->write() == -1)
        {
          c->end();
          return 1;
        }

      if (vars->type != SNMP_ENDOFMIBVIEW &&
          vars->type != SNMP_NOSUCHOBJECT &&
          vars->type != SNMP_NOSUCHINSTANCE)
      {
        snmp_pdu *req = createPDU (c);
        snmp_add_null_var (req, vars->name, vars->name_length);
        if (snmp_send (s, req))
          return 1;
        else
          snmp_free_pdu (req);
      }
      else
        c->done();
    }

    return 1;
  }

  void server::processArgs (int argc, char *const *argv, int opt)
  {
    if (opt == 'C')
    {
      while (*optarg)
        ++optarg;
    }
  }

  void server::reparseArgs (int argc, char **argv)
  {
    optind = 0;
    opterr = 1;
    optopt = '?';

    int arg;
    while ((arg = getopt (argc, argv, "Y:VhHm:M:O:I:P:D:dv:r:t:c:Z:e:E:n:u:l:x:X:a:A:p:T:-:3:s:S:L:C:")) != EOF)
    {
      switch (arg)
      {
        case 'I':
          setInputOpts (optarg, argc, argv);
          break;
        case 'O':
          setOutputOpts (optarg);
          break;
      }
    }
  }

  char *server::setOutputOpts (char *options)
  {
    while (*options)
    {
      switch (*options++)
      {
/*
      case '0':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID,
                                    NETSNMP_DS_LIB_2DIGIT_HEX_OUTPUT, 1);
          break;
*/
      case 'a':
          netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_STRING_OUTPUT_FORMAT,
                                                    NETSNMP_STRING_OUTPUT_ASCII);
          break;
      case 'b':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_DONT_BREAKDOWN_OIDS, 1);
          break;
      case 'e':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_NUMERIC_ENUM, 1);
          break;
      case 'E':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_ESCAPE_QUOTES, 1);
          break;
      case 'f':
          netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_OID_OUTPUT_FORMAT,
                                                    NETSNMP_OID_OUTPUT_FULL);
          break;
      case 'n':
          netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_OID_OUTPUT_FORMAT,
                                                    NETSNMP_OID_OUTPUT_NUMERIC);
          break;
      case 'q':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICK_PRINT, 1);
          break;
      case 'Q':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICKE_PRINT, 1);
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICK_PRINT, 1);
          break;
      case 's':
          netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_OID_OUTPUT_FORMAT,
                                                    NETSNMP_OID_OUTPUT_SUFFIX);
          break;
      case 'S':
          netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_OID_OUTPUT_FORMAT,
                                                    NETSNMP_OID_OUTPUT_MODULE);
          break;
      case 't':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_NUMERIC_TIMETICKS, 1);
          break;
      case 'T':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_HEX_TEXT, 1);
          break;
      case 'u':
          netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_OID_OUTPUT_FORMAT,
                                                    NETSNMP_OID_OUTPUT_UCD);
          break;
      case 'U':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_DONT_PRINT_UNITS, 1);
          break;
      case 'v':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_BARE_VALUE, 1);
          break;
      case 'x':
          netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_STRING_OUTPUT_FORMAT,
                                                    NETSNMP_STRING_OUTPUT_HEX);
          break;
      case 'X':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_EXTENDED_INDEX, 1);
          break;
      default:
          return options - 1;
      }
    }
    return 0;
  }

  char *server::setInputOpts (char *optarg, int argc, char **argv)
  {
    for (char *cp = optarg; *cp; cp++)
    {
      switch (*cp)
      {
      case 'b':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_REGEX_ACCESS, 1);
          break;
      case 'R':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_RANDOM_ACCESS, 1);
          break;
      case 'r':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_DONT_CHECK_RANGE, 1);
          break;
      case 'h':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_NO_DISPLAY_HINT, 1);
          break;
      case 'u':
          netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_READ_UCD_STYLE_OID, 1);
          break;
      case 's':
          /* What if argc/argv are null ? */
          if (!*(++cp))
              cp = argv[optind++];
          netsnmp_ds_set_string(NETSNMP_DS_LIBRARY_ID,
                                NETSNMP_DS_LIB_OIDSUFFIX,
                                cp);
          return NULL;

      case 'S':
          /* What if argc/argv are null ? */
          if (!*(++cp))
              cp = argv[optind++];
          netsnmp_ds_set_string(NETSNMP_DS_LIBRARY_ID,
                                NETSNMP_DS_LIB_OIDPREFIX,
                                cp);
          return NULL;

      default:
          return cp;
      }
    }
    return 0;
  }

  void server::setParseOpts ()
  {
    netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_MIB_PARSE_LABEL, 1);
    netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_MIB_COMMENT_TERM, 1);
    netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_MIB_ERRORS, 1);
    netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_MIB_REPLACE, 1);
  }
}
