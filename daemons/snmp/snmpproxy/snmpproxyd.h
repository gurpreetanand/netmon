#ifndef __SNMP_PROXY_DAEMON__
#define __SNMP_PROXY_DAEMON__

#include "daemon.h"

namespace netmon
{
  class server;

  class snmp_proxyd : public daemon
  {
    public:
      snmp_proxyd (int argc, char **argv)
        : daemon (argc, argv)
      {}
      ~snmp_proxyd ();

      virtual void init ();

      bool termSignaled ()
      {
        return isTermSignaled();
      }

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "snmpproxyd";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/snmpproxyd";
      }

      server *m_server;
  };
}

#endif // __SNMP_PROXY_DAEMON__
