#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <string>
#include <sstream>

#define SOCK_PATH "/var/run/.snmp_proxy.s"

bool getToolName (char *arg, std::string &name)
{
  if (arg == 0)
    return false;
  char *p = strrchr (arg, '/');
  if (p == 0)
    p = arg;
  else
    ++p;
  if (*p == '\0')
    return false;
  if (strcmp (p, "snmpget") == 0)
  {
    name = std::string (p);
    return true;
  }
  if (strcmp (p, "snmpwalk") == 0)
  {
    name = std::string (p);
    return true;
  }
  return false;
}

int main (int argc, char **argv)
{
  int s, t, len;
  bool verbose = false;
  sockaddr_un remote;
  char str[1024];

  std::ostringstream cmd;
  std::string name;
  if (!getToolName (argv[0], name))
  {
    printf ("Illegal command '%s'\n", argv[0]);
    return -1;
  }
  cmd << name;
  for (int i = 1; i < argc; ++i)
    if (strcmp (argv[i], "--verbose") != 0)
      cmd << " " << argv[i];
    else
      verbose = true;

  if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
  {
    perror ("socket");
    exit (1);
  }

  if (verbose)
    printf("Trying to connect...\n");

  remote.sun_family = AF_UNIX;
  strcpy(remote.sun_path, SOCK_PATH);
  len = strlen(remote.sun_path) + sizeof(remote.sun_family);
  if (connect(s, (struct sockaddr *)&remote, len) == -1)
  {
    perror ("connect");
    exit (1);
  }

  if (verbose)
    printf("Connected.\n");

  if (verbose)
    printf ("cmd: '%s'\n", cmd.str().c_str());
  cmd << "\n";

  write (s, (void *) cmd.str().c_str(), cmd.str().size());

  while (true)
  {
    if ((t = recv (s, str, 1023, 0)) > 0)
    {
      str[t] = '\0';
      printf ("%s", str);
    } 
    else
    {
      if (t < 0)
        perror ("recv");
      else
        if (verbose)
          printf ("Server closed connection\n");
      exit (1);
    }
  }

  close (s);

  return 0;
}
