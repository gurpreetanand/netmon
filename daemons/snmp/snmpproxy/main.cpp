#include <signal.h>
#include <errno.h>

#include <string>
#include <cstring>

#include "log.h"
#include "exception.h"
#include "snmpproxyd.h"

int running;

void sighandler (int sig)
{
  if (sig == SIGTERM)
    running = 0;
}

void installSignalHandler ()
{
  struct sigaction action;
  action.sa_handler = sighandler;
  sigemptyset(&action.sa_mask);
  action.sa_flags = 0;

  sigaction (SIGTERM, &action, NULL);
  sigaction (SIGPIPE, &action, NULL);
}

int main (int argc, char **argv)
{
  using namespace netmon;

  running = 1;

  try
  {
    snmp_proxyd *d = new snmp_proxyd (argc, argv);
    d->init();
    installSignalHandler();
    d->start();
    delete d;
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
  }
  catch (int e)
  {
    errno = e;
    perror ("snmpproxyd");
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, strerror (e));
  }
}
