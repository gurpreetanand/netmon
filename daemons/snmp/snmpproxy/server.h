#ifndef __SERVER__
#define __SERVER__

#include <unistd.h>

#include <map>
#include <string>

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

#include "exception.h"

namespace netmon
{
  class server_exc : public netmon_exception
  {
    public:
      server_exc (const std::string &err)
        : netmon_exception (err)
      {}
  };

  class client;
  class snmp_proxyd;

  class server
  {
    public:
      server (const std::string &, const std::string &, snmp_proxyd *);
      server (const char *, const char *, snmp_proxyd *);
      ~server ();

      void run ();

    protected:
      void createSocket ();
      void setFlags (int);
      void handleReads ();
      void handleWrites ();
      void handleRead (int);
      void handleTimeouts ();
      void handleAccept ();
      void removeOldSessions ();
      void createSession (client *);
      void deleteSession (client *);
      void parseOIDs (client *, int, int, char **);
      snmp_pdu *createPDU (client *);

      static int callback (int, snmp_session *, int, snmp_pdu *, void *);
      static void processArgs (int, char *const *, int);

      int onData (int, snmp_session *, snmp_pdu *);
      int handleWalk (client *, snmp_session *, snmp_pdu *);

      void reparseArgs (int, char **);
      char *setOutputOpts (char *);
      char *setInputOpts (char *, int, char **);
      void setParseOpts ();

      std::string m_path;
      std::string m_mibPath;
      int m_fd;
      fd_set m_readSet;
      fd_set m_writeSet;
      typedef std::map<int, client *> client_t;
      client_t m_clients;
      typedef std::map<snmp_session *, client *> session_t;
      session_t m_sessions;
      int m_outputStringFormat;
      int m_outputOIDFormat;
      std::map<int, int> m_outputFlags;
      snmp_proxyd *m_daemon;
  };
}

#endif // __SERVER__
