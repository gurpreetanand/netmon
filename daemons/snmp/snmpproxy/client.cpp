#include "client.h"
#include "util.h"

#include <unistd.h>
#include <errno.h>

namespace netmon
{
  client::client (int fd)
    : m_fd (fd), m_done (false), m_iter (m_oids.end()), m_rootValid (false),
      m_type (_eNULL), m_wantsWrite (false)
  {
    init ();
  }

  client::~client ()
  {
    delete m_session;
    delete[] m_readBuff;
    delete[] m_writeBuff;
    for (unsigned int i = 0; i < m_args.size(); ++i)
      delete[] m_args[i];
    m_args.clear();
    for (std::list<OID *>::iterator i = m_oids.begin(); i != m_oids.end(); ++i)
      delete *i;
    m_oids.clear();
  }

  void client::init ()
  {
    m_session = new snmp_session();
    m_readBuff = new char[m_buffSize];
    m_writeBuff = new char[m_buffSize];
    m_readP = m_writeP = m_parseP = m_wrote = 0;
    m_writeBuffSize = m_buffSize;
    m_arg.reserve(128);
  }

  char *client::getWriteBuffer ()
  {
    if ((m_writeBuffSize - m_wrote) < 128)
    {
      m_writeBuffSize *= 2;
      char *p = new char[m_writeBuffSize];
      memcpy ((void *) p, (void *) m_writeBuff, m_wrote);
      delete[] m_writeBuff;
      m_writeBuff = p;
    }
//    printf ("m_wrote: %d\n", m_wrote);
    return m_writeBuff + m_wrote;
  }

  int client::read ()
  {
    int ret = ::read (m_fd, (void *)(m_readBuff + m_readP), m_buffSize - m_readP);
    if (ret > 0)
    {
      m_readP += ret;
      return ret;
    }
    if (ret == -1 && errno == EAGAIN)
      return 0;
    return -1;
  }

  int client::write ()
  {
//    printf ("write()\n");
    if (m_wrote == 0 || m_wrote == m_writeP)
    {
      m_wantsWrite = false;
      return 0;
    }
    int ret = ::write (m_fd, (void *)(m_writeBuff + m_writeP), m_wrote - m_writeP);
//printf ("wrote %d bytez\n", ret);
    if (ret > 0)
    {
      if (ret == m_wrote)
      {
        m_writeP = m_wrote = 0;
        m_wantsWrite = false;
      }
      else
        m_writeP += ret;
      return ret;
    }
    if (ret == -1 && errno == EAGAIN)
    {
      m_wantsWrite = true;
      return 0;
    }
    return -1;
  }

  bool client::parseInput ()
  {
    bool ret = false;
    while (m_parseP <= m_readP)
    {
      switch (m_readBuff[m_parseP])
      {
        case '\n':
          printf ("found \\n\n");
          addArg();
{
FILE *f = fopen ("/var/log/netmon/snmp-proxy-requests", "a");
m_readBuff[m_parseP] = '\0';
fprintf (f, "%s\n", m_readBuff);
fflush (f);
fclose (f);
}
          ret = true;
          break;
        case ' ':
        case '\t':
          addArg();
          break;
        default:
          m_arg.push_back (m_readBuff[m_parseP]);
          break;
      }
      ++m_parseP;
    }
    return ret;
  }

  void client::addArg ()
  {
    if (m_arg.size() == 0)
      return;
    char *p = new char[m_arg.size() + 1];
    strcpy (p, m_arg.c_str());
    m_args.push_back (p);
    m_arg.clear();
  }

  void client::saveOutputFlags ()
  {
    util::saveOutputFlags (m_outputFlags, m_outputStringFormat, m_outputOIDFormat);
  }

  void client::restoreOutputFlags ()
  {
    util::restoreOutputFlags (m_outputFlags, m_outputStringFormat, m_outputOIDFormat);
  }
}

#ifdef __TEST__

#include <cstdio>

int main()
{
  netmon::client c(fileno(stdin));
  c.read();
  c.parseInput();
  char **p = c.getArgs();
  char **i = p;
  while (*i)
  {
    printf ("%s\n", *i);
    ++i;
  }
}

#endif // __TEST__
