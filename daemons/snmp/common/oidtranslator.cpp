#include "oidtranslator.h"
#include "db.h"

#include <cstring>

namespace netmon
{
  oid_translator::oid_translator (db *mydb)
    : m_db (mydb)
  {
    reload ();
  }

  void oid_translator::reload ()
  {
    m_oidTrans.clear ();
    loadOIDTranslations ();
  }

  bool oid_translator::getTranslation (const char *oid, std::string &name)
  {
    oid_trans::iterator i = m_oidTrans.find(std::string(oid));
    if (i == m_oidTrans.end())
      return false;
    name = i->second;
    return true;
  }

  void oid_translator::loadOIDTranslations ()
  {
    PGresult *res = m_db->execQuery ("SELECT OID, NAME FROM SNMP_OID_TRANS");
    for (int i = 0; i < PQntuples (res); ++i)
    {
      if (strlen (PQgetvalue (res, i, 0)) == 0 || strlen (PQgetvalue (res, i, 1)) == 0)
        continue;
      m_oidTrans[std::string(PQgetvalue (res, i, 0))] = std::string(PQgetvalue (res, i, 1));
    }
    PQclear (res);
  }
}
