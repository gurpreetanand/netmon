#ifndef __OID_TRANSLATOR__
#define __OID_TRANSLATOR__

#include <string>

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

namespace netmon
{
  class db;
  class oid_translator
  {
    public:
      oid_translator (db *);

      void reload ();
      bool getTranslation (const char *, std::string &);

    protected:
      void loadOIDTranslations ();

      struct string_hash
      {
        size_t operator() (const std::string &s) const
        {
          size_t h = 0;
          std::string::const_iterator p, p_end;
          for(p = s.begin(), p_end = s.end(); p != p_end; ++p)
          {
            h = 31 * h + (*p);
          }
          return h;
        }
      };

      typedef hash_map<std::string, std::string, string_hash> oid_trans;
      oid_trans m_oidTrans;
      db *m_db;
  };
}

#endif // __OID_TRANSLATOR__
