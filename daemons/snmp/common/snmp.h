#ifndef __SNMP__
#define __SNMP__

#include <string>
#include "snmp_pp/snmp_pp.h"

namespace netmon
{
  class snmpvbfunctor
  {
    public:
      virtual ~snmpvbfunctor () {}
      virtual void add (const Snmp_pp::Vb &) = 0;
  };

  class snmp
  {
    public:
      snmp ();
      virtual ~snmp ();

      void setPort (unsigned short port)
      {
        m_port = port;
      }

      unsigned short getPort () const
      {
        return m_port;
      }

      void setAddress (const char *ip)
      {
        m_address = Snmp_pp::UdpAddress (ip);
      }

      void setOID (const char *ip)
      {
        m_oid = Snmp_pp::Oid (ip);
      }

      void setCommunity (const char *com)
      {
        m_community = Snmp_pp::OctetStr (com);
      }

      int snmpGet (const char *, const char *, const char *, std::string &);
      int snmpGet (std::string &, unsigned long &);
      int walkSNMP (const char *, const char *, const Snmp_pp::Oid &, snmpvbfunctor &);

    protected:
      unsigned short m_port;
      Snmp_pp::UdpAddress m_address;
      Snmp_pp::Oid m_oid;
      Snmp_pp::OctetStr m_community;
  };
}

#endif // __SNMP__
