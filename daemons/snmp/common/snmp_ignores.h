#ifndef __SNMP_IGNORES_H__
#define __SNMP_IGNORES_H__

#include <map>
#include <string>
#include <stdint.h>

namespace netmon {
  class SnmpIgnores {

    public:
      void addSkip(std::string ip, std::string oid, int status, int timeout = 0);
      bool isOidValid(std::string ip, std::string oid);
      void cleanup();

    private:
      const std::string getKey(std::string ip, std::string oid) const;

      typedef std::map<std::string, uint32_t> blockMap;
      blockMap ipOidSkips;
      blockMap ipSkips;
  };
}
#endif
