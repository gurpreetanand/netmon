#include "snmpasync.h"
//#include "exception.h"

namespace netmon
{
  snmp_async::snmp_async ()
  {
    init();
  }

  snmp_async::~snmp_async ()
  {
    delete m_snmp;
    Snmp_pp::Snmp::socket_cleanup();
  }

  void snmp_async::addQuery (snmp_async_query *query)
  {
    query->m_address.set_port (query->m_port);

    m_map.insert (std::pair<Snmp_pp::UdpAddress, snmp_async_query *>(query->m_address, query));
  }

  void snmp_async::init ()
  {
    Snmp_pp::Snmp::socket_startup();

    int status;
    m_snmp = new Snmp_pp::Snmp (status, 0, false);
    if (status != SNMP_CLASS_SUCCESS)
      throw /*netmon_exception*/ ("cannot create an instance of Snmp");
  }

  void snmp_async::createPdu (const snmp_async_query::oid_map_t &oids, Snmp_pp::Pdu &pdu)
  {
    Snmp_pp::Vb vb;

    for (snmp_async_query::oid_map_t::const_iterator i = oids.begin(); i != oids.end(); ++i)
    {
      vb.set_oid (i->first);
      pdu += vb;
    }
  }

  int snmp_async::poll (int retries, int timeout)
  {
    int status;

    for (target_map_t::iterator i = m_map.begin(); i != m_map.end(); ++i)
    {
      Snmp_pp::CTarget ctarget (i->second->m_address);
      ctarget.set_version (i->second->m_version);
      ctarget.set_readcommunity (i->second->m_community);
      ctarget.set_retry (retries);
      ctarget.set_timeout (timeout);

      Snmp_pp::Pdu pdu;
      createPdu (i->second->m_oids, pdu);

      Snmp_pp::SnmpTarget *target = &ctarget;

      status = m_snmp->get (pdu, *target, callback, this);
      if (status != SNMP_CLASS_SUCCESS)
      {
        printf ("snmpget not ok\n");
        return -1;
      }
    }

    for (unsigned int t = 0; t < 5; ++t)
    {
      m_snmp->eventListHolder->SNMPProcessPendingEvents();
      sleep (1);
    }

    return 0;
  }

  void snmp_async::callback (int reason, Snmp_pp::Snmp *snmp, Snmp_pp::Pdu &pdu, Snmp_pp::SnmpTarget &target, void *t)
  {
    snmp_async *me = reinterpret_cast<snmp_async *> (t);

    Snmp_pp::Vb nextVb;
    int err;

    printf ("reason: %d msg: %s\n", reason, snmp->error_msg (reason));

    err = pdu.get_error_status();
    if (err)
    {
      // error
      printf ("error: %s\n", snmp->error_msg (err));
    }

    target_map_t &map = me->m_map;
    Snmp_pp::UdpAddress address (target.get_address().cast_udpaddress());
    target_map_t::iterator it = map.find (address);
    printf ("result from: '%s'\n", address.get_printable());
    if (it == map.end())
      return; // response we didn't ask for

    for (int i = 0; i < pdu.get_vb_count(); ++i)
    {
      pdu.get_vb (nextVb, i);
      printf ("%s -> %s\n", nextVb.get_printable_oid(), nextVb.get_printable_value());

      target_map_t::iterator end = map.upper_bound (address);
      it = map.find (address);
      Snmp_pp::Oid oid (nextVb.get_printable_oid());

      for (; it != end; ++it)
      {
        snmp_async_query *q = it->second;
        snmp_async_query::oid_map_t &oids = q->getOids();
        snmp_async_query::oid_map_t::iterator oi = oids.find (oid);
        if (oi != oids.end())
        {
          oids[oid]->m_value = std::string (nextVb.get_printable_value());
          oids[oid]->m_valid = true;
        }
      }
    }
  }
}

#ifdef __TEST__

using namespace netmon;

void t_same_ip ()
{
  snmp_async_query a("192.168.0.1");
  a.addOID ("1.3.6.1.2.1.1.1.0", 0);

  snmp_async_query b("192.168.0.1");
  b.addOID ("1.3.6.1.2.1.1.5.0", 0);

  snmp_async async;
  async.addQuery (&a);
  async.addQuery (&b);
  async.poll (2, 500);

  printf ("result for obj a:\n");
  const snmp_async_query::oid_map_t &oids = a.getOids();
  for (snmp_async_query::oid_map_t::const_iterator i = oids.begin(); i != oids.end(); ++i)
  {
    if (i->second->m_valid)
      printf ("%s ==> %s\n", i->first.get_printable(), i->second->m_value.c_str());
  }
  printf ("result for obj b:\n");
  const snmp_async_query::oid_map_t &oidsb = b.getOids();
  for (snmp_async_query::oid_map_t::const_iterator i = oidsb.begin(); i != oidsb.end(); ++i)
  {
    if (i->second->m_valid)
      printf ("%s ==> %s\n", i->first.get_printable(), i->second->m_value.c_str());
  }
}

int main()
{
  t_same_ip();
  return 0;

  snmp_async_query a;
  printf ("%d\n", a.port());
  a.community() = "public";
  printf ("%s\n", a.community().get_printable());
//  a.address() = "10.0.2.202";
  a.address() = "192.168.0.1";
  printf ("%s\n", a.address().get_printable());
  a.version() = Snmp_pp::version1;
  printf ("%d\n", a.version());
  a.addOID ("1.3.6.1.2.1.1.1.0", 0);
  a.addOID ("1.3.6.1.2.1.1.5.0", 0);
  snmp_async async;
  async.addQuery (&a);
  async.poll (2, 500);
  const snmp_async_query::oid_map_t oids = a.getOids();
  printf ("got result:\n");
  for (snmp_async_query::oid_map_t::const_iterator i = oids.begin(); i != oids.end(); ++i)
  {
    if (i->second->m_valid)
      printf ("%s ==> %s\n", i->first.get_printable(), i->second->m_value.c_str());
  }
}

#endif // __TEST__
