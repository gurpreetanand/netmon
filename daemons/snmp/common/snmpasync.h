#ifndef __SNMP_ASYNC__
#define __SNMP_ASYNC__

#include <map>
#include <string>
#include <stdint.h>
#include "snmpasyncquery.h"
#include "snmp_pp/snmp_pp.h"

namespace netmon
{
  class snmp_async
  {
    public:
      snmp_async ();
      ~snmp_async ();

      void addQuery (snmp_async_query *);
      int poll (int, int);

    protected:
      void init ();
      void createPdu (const snmp_async_query::oid_map_t &, Snmp_pp::Pdu &);

      static void callback (int, Snmp_pp::Snmp *, Snmp_pp::Pdu &, Snmp_pp::SnmpTarget &, void *);

      typedef std::multimap<Snmp_pp::UdpAddress, snmp_async_query *> target_map_t;

      target_map_t m_map;
      Snmp_pp::Snmp *m_snmp;
  };
}

#endif // __SNMP_ASYNC__
