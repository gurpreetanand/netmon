#ifndef __SNMPMOND_ALERT_MANAGER__
#define __SNMPMOND_ALERT_MANAGER__

#include "alertmanager.h"

namespace netmon
{
  class snmpmond_alert_manager : public alert_manager
  {
    public:
      snmpmond_alert_manager (db *mydb)
        : alert_manager (mydb)
      {}

      virtual ~snmpmond_alert_manager ()
      {}

    protected:
      virtual void getOverName (std::string &name)
      {
        name = "SNMP_IFACE_ABOVE_THRESHOLD";
      }

      virtual void getBelowName (std::string &name)
      {
        name = "SNMP_IFACE_BELOW_THRESHOLD";
      }
  };

  class iface_alert_manager : public snmpmond_alert_manager
  {
    public:
      iface_alert_manager (db *mydb)
        : snmpmond_alert_manager (mydb)
      {}

      virtual ~iface_alert_manager ()
      {}

      unsigned int getAlerts (unsigned int, unsigned int, unsigned int, const values_map &);

    protected:
      virtual void getReferenceTable (std::string &table)
      {
        table = "interfaces";
      }
  };

  class snmpcache;

  class device_alert_manager : public snmpmond_alert_manager
  {
    public:
      device_alert_manager (db *mydb, snmpcache *cache)
        : snmpmond_alert_manager (mydb), m_cache (cache)
      {}

      virtual unsigned int getAlerts (unsigned int, std::list<alerthandler *> &);

    protected:
      void getAlert (alert_map::iterator &, unsigned int, std::list<alerthandler *> &, unsigned int &);

      virtual void getReferenceTable (std::string &table)
      {
        table = "devices";
      }

      snmpcache *m_cache;
  };
}

#endif // __SNMPMOND_ALERT_MANAGER__
