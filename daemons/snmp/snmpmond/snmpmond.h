#ifndef __SNMP_DAEMON__
#define __SNMP_DAEMON__

#include "daemon.h"

namespace netmon
{
  class snmpcheck;

  class snmp_mond : public daemon
  {
    public:
      snmp_mond (int, char **);
      ~snmp_mond ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "snmpmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/snmpmond";
      }

      snmpcheck *m_checker;
  };
}

#endif // __SNMP_DAEMON__
