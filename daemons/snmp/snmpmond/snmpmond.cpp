#include "snmpmond.h"
#include "snmpcheck.h"
#include "log.h"

namespace netmon
{
  snmp_mond::snmp_mond (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  snmp_mond::~snmp_mond ()
  {
    delete m_checker;
  }

  void snmp_mond::init ()
  {
    daemon::init();
    m_checker = new snmpcheck (m_db, this);
  }

  int snmp_mond::run ()
  {
    while (!isTermSignaled())
    {
      m_checker->run();
      sleep (5);
      netmon::log::instance()->add(netmon::_eLogDebug, "Scanning SNMP devices...");
    }
    return 0; // never reached
  }
}
