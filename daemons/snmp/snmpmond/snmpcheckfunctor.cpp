#include "snmpcheckfunctor.h"

#include <cstdlib>

namespace netmon
{
  bool snmp_check_functor::parseOID (const char *oid, std::string &mac)
  {
    const std::string s(oid);
    size_t pos = s.size ();

    for (int i = 0; i < 6; ++i)
    {
      pos = s.rfind ('.', pos);
      if (pos == std::string::npos)
        return false;
      --pos;
    }
    mac = std::string (s, pos + 2);
    return true;
  }

  std::string snmp_check_functor::formatMAC (const std::string &mac)
  {
    unsigned int c[6];
    sscanf (mac.c_str(), "%d.%d.%d.%d.%d.%d", &c[0], &c[1], &c[2], &c[3], &c[4], &c[5]);
    char buf[20];
    snprintf (buf, 20, "%02X:%02X:%02X:%02X:%02X:%02X", c[0], c[1], c[2], c[3], c[4], c[5]);
    return std::string (buf);
  }

  camfunctor::camfunctor ()
    : snmp_check_functor (), m_vlaniterinit (false), m_mode (_eVLAN)
  {
  }

  void camfunctor::add (const Snmp_pp::Vb &v)
  {
    switch (m_mode)
    {
      case _eVLAN:
        addVLAN (v);
        return;
      case _eBridge:
        addBridge (v);
        return;
      case _ePort:
        addPort (v);
        return;
    }
  }

  bool camfunctor::getConnectedMAC (unsigned int port, std::string &mac)
  {
    if (m_ports.find (port) == m_ports.end())
      return false;
    if (m_bridge.find (m_ports[port]) == m_bridge.end())
      return false;
    mac = m_bridge[m_ports[port]].front();
    return true;
  }

  void camfunctor::clear ()
  {
    m_vlans.clear();
    m_bridge.clear();
    m_ports.clear();
    m_vlaniterinit = false;
    m_mode = _eVLAN;
  }

  void camfunctor::dump ()
  {
    for (std::map<unsigned int, unsigned int>::iterator i = m_ports.begin(); i != m_ports.end(); ++i)
    {
      printf ("Port %d\n", i->first);
      std::list<std::string> l = m_bridge[i->second];
      for (std::list<std::string>::iterator k = l.begin(); k != l.end(); ++k)
        printf ("\t%s\n", (*k).c_str());
    }
  }

  void camfunctor::addVLAN (const Snmp_pp::Vb &v)
  {
    if (*(v.get_printable_value()) != '1')
      return;
    std::string oid = std::string (v.get_printable_oid());
    size_t pos = oid.rfind ('.');
    if (pos == std::string::npos)
      return;
    std::string vlan = std::string (oid, pos + 1);
    m_vlans.push_back (vlan);
  }

  void camfunctor::addBridge (const Snmp_pp::Vb &v)
  {
    std::string index = std::string (v.get_printable_value());
    unsigned int idx = atoi (index.c_str());
    std::string mac;
    if (!parseOID (v.get_printable_oid(), mac))
      return;
    if (m_bridge.find (idx) == m_bridge.end())
      m_bridge[idx].push_back (formatMAC (mac));
  }

  void camfunctor::addPort (const Snmp_pp::Vb &v)
  {
    std::string port = std::string (v.get_printable_value());
    unsigned int prt = atoi (port.c_str());
    std::string br = std::string (v.get_printable_oid());
    size_t pos = br.rfind ('.');
    if (pos == std::string::npos)
      return;
    std::string sbr = std::string (br, pos + 1);
    unsigned int bridge = atoi (sbr.c_str());
    if (m_ports.find (prt) == m_ports.end())
      m_ports[prt] = bridge;
  }

  bool camfunctor::getNextVLAN (std::string &vlan)
  {
    if (!m_vlaniterinit)
    {
      m_vlaniter = m_vlans.begin();
      m_vlaniterinit = true;
    }
    if (m_vlaniter == m_vlans.end())
      return false;
    vlan = *m_vlaniter;
    ++m_vlaniter;
    return true;
  }
}
