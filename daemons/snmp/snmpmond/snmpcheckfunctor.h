#ifndef __SNMP_CHECK_FUNCTOR__
#define __SNMP_CHECK_FUNCTOR__

#include <list>
#include <map>
#include <string>

#include "snmp.h"
#include "snmp_pp/snmp_pp.h"

namespace netmon
{
  class ifacesfunctor : public snmpvbfunctor
  {
    public:
      virtual void add (const Snmp_pp::Vb &vb)
      {
        m_ifaces.push_back (std::string (vb.get_printable_value()));
      }

      std::list<std::string> &getInterfaces ()
      {
        return m_ifaces;
      }

    protected:
      std::list<std::string> m_ifaces;
      friend class snmpcheck;
  };

  class snmp_check_functor : public snmpvbfunctor
  {
    public:
      snmp_check_functor () {}
      virtual ~snmp_check_functor () {}

    protected:
      bool parseOID (const char *, std::string &);
      std::string formatMAC (const std::string &);
  };

  class camfunctor : public snmp_check_functor
  {
    public:
      enum mode { _eVLAN, _eBridge, _ePort };

      camfunctor ();
      virtual void add (const Snmp_pp::Vb &);

      bool getConnectedMAC (unsigned int, std::string &);

      const bool hasVLANs () const
      {
        return !m_vlans.empty();
      }

      bool getNextVLAN (std::string &);
      void clear ();
      void dump ();

      void setMode (mode m)
      {
        m_mode = m;
      }

    protected:
      void addVLAN (const Snmp_pp::Vb &);
      void addBridge (const Snmp_pp::Vb &);
      void addPort (const Snmp_pp::Vb &);

      std::list<std::string> m_vlans;
      std::list<std::string>::iterator m_vlaniter;
      std::map<unsigned int, std::list<std::string> > m_bridge;
      std::map<unsigned int, unsigned int> m_ports;
      bool m_vlaniterinit;
      mode m_mode;
      friend class snmpcheck;
  };
}

#endif // __SNMP_CHECK_FUNCTOR__
