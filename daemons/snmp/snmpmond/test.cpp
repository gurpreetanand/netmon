#include <errno.h>
#include <syslog.h>
#include <sys/types.h>

#include <string>
#include <cstdio>

#include "snmpcheck.h"
#include "netmon.h"
#include "db.h"

// global stuff from netmon.lib
char dbname [DBNAMELEN];
char dbuser [DBUSERLEN];
char syslogbuff [SYSLOGLINELEN];
char syslogfacility [SYSLOGFACILITYLEN];
int facility;
int errvalue;

static const char *dname = "snmpmond";

int main (int argc, char **argv)
{
  readConfig (dbname, dbuser);

  openlog (dname, LOG_PID, facility);

  try
  {
    netmon::snmpcheck checker (dbname, dbuser);

    for (;;)
    {
      checker.test ();
      sleep (60); // sleep 1 minute
      printf ("\n\n New scan\n=========\n");
    }
  }
  catch (std::string &e)
  {
    printf ("Error: %s\n", e.c_str ());
    syslog (LOG_ERR, e.c_str ());
  }
  catch (int e)
  {
    errno = e;
    perror ("snmpmond");
  }
}
