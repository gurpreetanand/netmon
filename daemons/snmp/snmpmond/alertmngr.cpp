#include "alertmngr.h"
#include "alerts.h"
#include "cache.h"

namespace netmon
{
  unsigned int iface_alert_manager::getAlerts (unsigned int id, unsigned int inval, unsigned int outval, const values_map &vmap)
  {
    alert_map::iterator i = m_alerts.find (id);
    if (i == m_alerts.end())
      return 0;

    alert_map::iterator end = m_alerts.upper_bound (id);
    unsigned int ret = 0;

    unsigned int comp;
    alerthandler *h;
    std::string inout;
    for (; i != end; ++i)
    {
      if (i->second->m_pattern == "IN")
      {
        comp = inval;
        inout = "Inbound";
      }
      else
        if (i->second->m_pattern == "OUT")
        {
          comp = outval;
          inout = "Outbound";
        }
        else
        {
          comp = inval + outval;
          inout = "Combined";
        }
      if (i->second->m_triggered && !i->second->m_comparator->compare(comp, i->second->m_threshold))
      {
        h = createAlertHandler (i->second, true);
        getReferenceData (id, h->getMap());
        h->addKeywordValue (alerthandler::eInOut, inout.c_str());
        h->addKeywordValue (alerthandler::eUtilization, comp);
        dispatchAlert (h, vmap);
        toggleAlertFlag (i->second->m_triggerID);
        ++ret;
      }
      if (!i->second->m_triggered && i->second->m_comparator->compare(comp, i->second->m_threshold))
      {
        h = createAlertHandler (i->second, false);
        getReferenceData (id, h->getMap());
        h->addKeywordValue (alerthandler::eInOut, inout.c_str());
        h->addKeywordValue (alerthandler::eUtilization, comp);
        dispatchAlert (h, vmap);
        toggleAlertFlag (i->second->m_triggerID);
        ++ret;
      }
    }

    return ret;
  }

  unsigned int device_alert_manager::getAlerts (unsigned int id, std::list<alerthandler *> &alerts)
  {
    alert_map::iterator i = m_alerts.find (id);
    if (i == m_alerts.end())
      return 0;

    alert_map::iterator end = m_alerts.upper_bound (id);
    unsigned int ret = 0;

    for (; i != end; ++i)
    {
      getAlert (i, id, alerts, ret);
    }
    return ret;
  }

  void device_alert_manager::getAlert (alert_map::iterator &i, unsigned int id, std::list<alerthandler *> &alerts, unsigned int &ret)
  {
    snmpcache::snmpmap::iterator it = m_cache->m_map.find (id);
    if (it == m_cache->m_map.end())
     return;

    snmpcache::ifacemap::iterator it2;
    if (i->second->m_triggered)
    {
      unsigned int value = 0;
      for (it2 = it->second.begin (); it2 != it->second.end (); ++it2)
      {
        value = (unsigned int) m_cache->getThroughputPercentage (it->first, it2->first, true);
        if (i->second->m_comparator->compare(value, i->second->m_threshold))
          return; // there's still at least one inteface with bandwith > threshold
      }
      alerts.push_back (createAlertHandler (i->second, true));
      toggleAlertFlag (i->second->m_triggerID);
      ++ret;
    }
    else
    {
      unsigned int value = 0;
      for (it2 = it->second.begin (); it2 != it->second.end (); ++it2)
      {
        value = (unsigned int) m_cache->getThroughputPercentage (it->first, it2->first, true);
        if (i->second->m_comparator->compare(value, i->second->m_threshold))
          break; // there's still at least one inteface with bandwith > threshold
      }
      alerthandler *h = createAlertHandler (i->second, false);
      h->addKeywordValue (alerthandler::eUtilization, value);
      alerts.push_back (h);
      toggleAlertFlag (i->second->m_triggerID);
      ++ret;
    }
  }
}
