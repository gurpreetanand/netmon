#ifndef __CACHE__
#define __CACHE__

#include <string>
#include <map>

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

#include <stdint.h>

namespace netmon
{
  struct snmpifacedata
  {
    snmpifacedata ()
      : m_inOctets (0), m_outOctets (0), m_time (0), m_init (false)
    {}
    snmpifacedata (uint64_t in, uint64_t out, time_t t)
      : m_inOctets (in), m_outOctets (out), m_time (t), m_init (true)
    {}
    snmpifacedata (const snmpifacedata &data)
    {
      m_inOctets = data.m_inOctets;
      m_outOctets = data.m_outOctets;
      m_time = data.m_time;
      m_init = data.m_init;
    }
    snmpifacedata &operator= (const snmpifacedata &data)
    {
      m_inOctets = data.m_inOctets;
      m_outOctets = data.m_outOctets;
      m_time = data.m_time;
      m_init = data.m_init;
      return *this;
    }
    uint64_t m_inOctets;
    uint64_t m_outOctets;
    time_t m_time;
    bool m_init;
  };

  struct snmpifaceaggdata
  {
    snmpifaceaggdata ()
      : m_prev (snmpifacedata()), m_now(snmpifacedata()), m_inResets (0), m_outResets (0)
    {}
    bool update (const snmpifacedata &);
    snmpifacedata m_prev;
    snmpifacedata m_now;
    uint32_t m_inResets;
    uint32_t m_outResets;
    uint32_t m_speed;
    uint32_t m_id;
  };

  class snmpcache
  {
    public:
      snmpcache ();
      virtual ~snmpcache ();

      virtual void update (uint32_t, uint32_t, uint32_t, const snmpifacedata &, uint32_t);
      uint32_t getInResets (uint32_t, uint32_t);
      uint32_t getOutResets (uint32_t, uint32_t);
      uint32_t getInbound (uint32_t, uint32_t);
      uint32_t getOutbound (uint32_t, uint32_t);
      double getThroughput (uint32_t, uint32_t, bool);
      double getThroughputPercentage (uint32_t, uint32_t, bool);

    protected:
      bool getData (uint32_t, uint32_t, snmpifaceaggdata &);

      typedef std::map<uint32_t, snmpifaceaggdata> ifacemap;
      typedef hash_map<uint32_t, ifacemap> snmpmap;

      snmpmap m_map;
      friend class snmpcheck;
      friend class device_alert_manager;
  };
}

#endif // __CACHE__
