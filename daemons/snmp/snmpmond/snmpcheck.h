#ifndef __SNMP_CHECK__
#define __SNMP_CHECK__

#include <string>
#include <list>
#include <map>
#include <jsoncpp/json/value.h>

#include "db.h"
#include "exception.h"
#include "snmp.h"
#include "snmpcheckfunctor.h"
#include "snmp_pp/snmp_pp.h"
#include "memcache.h"
#include "../common/snmp_ignores.h"

namespace netmon
{
  class snmp_mond;
  class snmpcache;
  class iface_alert_manager;

  class snmpcheck
  {
    public:
      snmpcheck (db *, snmp_mond *);
      ~snmpcheck ();
      void run () throw (netmon_exception);

    private:
      struct iface_data
      {
        unsigned int m_id;
        std::string m_name;
        std::string m_descr;
        std::string m_speed;
        std::string m_mac;
      };

    protected:
      typedef std::map<unsigned int, iface_data> iface_map;

      void init ();
      void setPendingFlag ();
      void clearPendingFlag (const char *);
      void setExaminedFlag (const char *, bool, bool = true);
      void checkAgents ();
      void findConnectedMACs (const char *, const char *);
      bool findSNMPInterfaces (const char *, const char *, const char *);
      void checkAgent (unsigned int, const char *, const char *, unsigned int);
      void insertInterface (const char *, const char *, const char *, const std::string &);
      void updateInterface (const char *, const char *, const char *, const std::string &, const iface_data &);
      bool getInterfaceData (const char *, const char *, const std::string &, iface_data &);
      void checkInterfaces (const char *, const char *, const char *);
      void getInterfaces (const char *, iface_map &);
      void updateCache (unsigned int, const char *, const char *, const std::string &, const std::string &, uint32_t);
      void getCounters (unsigned int, const char *, uint32_t &, uint32_t &);
      void checkIPAlerts ();
      void checkIfaceAlerts (unsigned int, unsigned int, unsigned int, const char *);
      void cacheInterfaceErrors(const char *, const char *, const std::string &);

      db *m_db;
      snmp_mond *m_daemon;
      snmpcache *m_cache;
      iface_alert_manager *m_ifaceAlertManager;
      camfunctor m_cam;
      int m_round;
      u_short m_currport;
      snmp *m_snmp;
      static Snmp_pp::Oid m_sysDescr;
      static Snmp_pp::Oid m_iFaces;
      Memcacher m_memcache;
      SnmpIgnores m_ignores;

      Json::Value m_dashboards;
  };
}

#endif // __SNMP_CHECK__
