#include "cache.h"

namespace netmon
{
  static unsigned long long maxl = (((unsigned long long)(1)) << 32) - 1;

  bool snmpifaceaggdata::update (const snmpifacedata &data)
  {
    bool ret = false;
    bool init = m_prev.m_init;
    m_prev = m_now;
    m_now = data;

    if (!init)
      return ret;

    if (m_prev.m_inOctets > data.m_inOctets)
    {
      ret = true;
      ++m_inResets;
    }
    if (m_prev.m_outOctets > data.m_outOctets)
    {
      ret = true;
      ++m_outResets;
    }
    return ret;
  }

  snmpcache::snmpcache ()
  {
  }

  snmpcache::~snmpcache ()
  {
    m_map.clear ();
  }

  void snmpcache::update (uint32_t id, uint32_t iface, uint32_t ifaceid, const snmpifacedata &data, uint32_t speed)
  {
    snmpmap::iterator it = m_map.find (id);

    if (it == m_map.end())
    {
      snmpifaceaggdata d;
      d.m_now = data;
      d.m_speed = speed;
      d.m_id = ifaceid;

      m_map[id][iface] = d;
    }
    else
    {
      ifacemap &map = it->second;
      ifacemap::iterator it2 = map.find (iface);
      if (it2 == map.end())
      {
        snmpifaceaggdata d;
        d.m_now = data;
        d.m_speed = speed;
        d.m_id = ifaceid;
        map[iface] = d;
      }
      else
      {
        snmpifaceaggdata &d = it2->second;
        d.update (data);
      }
    }
  }

  uint32_t snmpcache::getInResets (uint32_t id, uint32_t iface)
  {
    snmpifaceaggdata d;

    if (!getData (id, iface, d))
      return 0;

    return d.m_inResets;
  }

  uint32_t snmpcache::getOutResets (uint32_t id, uint32_t iface)
  {
    snmpifaceaggdata d;

    if (!getData (id, iface, d))
      return 0;

    return d.m_outResets;
  }

  uint32_t snmpcache::getInbound (uint32_t id, uint32_t iface)
  {
    snmpifaceaggdata d;

    if (!getData (id, iface, d))
      return 0;

    return d.m_now.m_inOctets;
  }

  uint32_t snmpcache::getOutbound (uint32_t id, uint32_t iface)
  {
    snmpifaceaggdata d;

    if (!getData (id, iface, d))
      return 0;

    return d.m_now.m_outOctets;
  }


  double snmpcache::getThroughputPercentage (uint32_t id, uint32_t iface, bool in)
  {
    snmpifaceaggdata d;

    if (!getData (id, iface, d))
      return -1;

    if (!d.m_prev.m_init)
      return -1;

    uint32_t speed = d.m_speed;
    if (speed == 0)
      return 0.0;
    double thr = getThroughput (id, iface, in);
    return (double)(thr * 100 / speed);
  }

  double snmpcache::getThroughput (uint32_t id, uint32_t iface, bool in)
  {
    snmpifaceaggdata d;

    if (!getData (id, iface, d))
      return -1;

    if (!d.m_prev.m_init)
      return -1;

    uint32_t speed = d.m_speed;
    if (speed == 0)
      return 0.0;
//    printf ("speed: %lu\n", speed);
    int ts = (int) (d.m_now.m_time - d.m_prev.m_time);
//    printf ("time delta: %d\n", ts);
    if (in)
    {
      if (d.m_prev.m_inOctets > d.m_now.m_inOctets)
        d.m_now.m_inOctets += maxl;
//      printf ("in prev: %Lu, now: %Lu\n", d.m_prev.m_inOctets, d.m_now.m_inOctets);
      return (double)(d.m_now.m_inOctets - d.m_prev.m_inOctets) * 8 / ts;
    }
    else
    {
      if (d.m_prev.m_outOctets > d.m_now.m_outOctets)
        d.m_now.m_outOctets += maxl;
//      printf ("out prev: %Lu, now: %Lu\n", d.m_prev.m_outOctets, d.m_now.m_outOctets);
      return (double)(d.m_now.m_outOctets - d.m_prev.m_outOctets) * 8 / ts;
    }
  }

  bool snmpcache::getData (uint32_t id, uint32_t iface, snmpifaceaggdata &data)
  {
    snmpmap::iterator it = m_map.find (id);

    if (it == m_map.end())
      return false;

    ifacemap &map = it->second;
    ifacemap::iterator it2 = map.find (iface);
    if (it2 == map.end())
      return false;

    data = it2->second;
    return true;
  }
}

#ifdef __TEST__

#include <cstdio>
#include <ctime>

int main ()
{
  using namespace netmon;
  snmpcache c;
  time_t t = time(0);
  snmpifacedata d(150000, 0, t);

  c.update (0, 0, d);
  d.m_inOctets = 100000;
  d.m_time += 1;
  c.update (0, 0, d);

  printf ("%lld\n", maxl);
  printf ("%f\n", c.getThreshold (0, 0, 1000000, true));
}

#endif // __TEST__
