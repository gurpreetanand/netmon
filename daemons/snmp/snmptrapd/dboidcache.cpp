#include "dboidcache.h"
#include "common/oidtranslator.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>

#include <sstream>

namespace netmon
{
  dboidcache *dboidcache::m_instance = 0;

  dboidcache::~dboidcache ()
  {
    delete m_oidTranslator;
  }

  dboidcache *dboidcache::instance ()
  {
    if (m_instance == 0)
      m_instance = new dboidcache;
    return m_instance;
  }

  void dboidcache::init (db *mydb)
  {
    m_db = mydb;
    m_oidTranslator = new oid_translator (m_db);
    loadOIDs ();
  }

  void dboidcache::reload ()
  {
    storeNewOIDs ();
    clearLogList ();
    loadOIDs ();
    m_oidTranslator->reload ();
  }

  bool dboidcache::getTranslation (const char *oid, std::string &name)
  {
    return m_oidTranslator->getTranslation (oid, name);
  }

  void dboidcache::storeNewOIDs ()
  {
    for (iphash::iterator it1 = m_newOIDs.begin (); it1 != m_newOIDs.end (); ++it1)
      for (oidset::iterator it2 = it1->second.begin (); it2 != it1->second.end (); ++it2)
        if (it2->m_isNew)
        {
          insert (it1->first, it2->m_oid);
          oidentry tmp = *it2;
          tmp.m_isNew = false;
          it1->second.erase (it2);
          it1->second.insert (tmp);
        }
  }

  void dboidcache::loadOIDs ()
  {
    unsigned int id = 0;
    PGresult *res = m_db->execQuery ("SELECT IP, SNMPOID, STORE, ID FROM SNMPOIDS");
    for (int i = 0; i < PQntuples (res); ++i)
    {
      in_addr addr;
      if (inet_aton (PQgetvalue (res, i, 0), &addr) == 0)
        continue;
      id = atoi (PQgetvalue (res, i, 3));
      if (*(PQgetvalue (res, i, 2)) == 't')
        addToLogList (addr.s_addr, id, PQgetvalue (res, i, 1));
      else
        updateCache (addr.s_addr, PQgetvalue (res, i, 1), id, false);
    }
    PQclear (res);
  }

  void dboidcache::insert (unsigned long ip, const std::string &oid)
  {
    in_addr addr;
    addr.s_addr = ip;
    std::ostringstream sql;
    sql << "INSERT INTO SNMPOIDS (IP, SNMPOID, STORE) VALUES ('" << inet_ntoa (addr) << "', '" << oid << "', 'false')";
    m_db->execSQL (sql.str().c_str());
  }

#ifdef __TEST__
  void dboidcache::dump ()
  {
    for (iphash::iterator it = m_newOIDs.begin (); it != m_newOIDs.end (); ++it)
      for (oidset::iterator it2 = it->second.begin (); it2 != it->second.end (); ++it2)
      {
        in_addr addr;
        addr.s_addr = it->first;
        printf ("%s (%s) %s\n", inet_ntoa (addr), it2->m_oid.c_str(), (it2->m_isNew ? "true" : "false"));
      }
  }
#endif
}

#ifdef __TEST__

#include <cstdio>
#include <cstdlib>
#include "db.h"

int main ()
{
  using namespace netmon;
  char dbname[] = "netmon";
  char dbuser[] = "root";

  try
  {
    db mydb;
    mydb.connect (dbname, dbuser);
    dboidcache::instance()->init (&mydb);
    dboidcache::instance()->dump ();
    (void) dboidcache::instance()->toLog ("127.0.0.1", "1.3.6.1.1.1.1");
    dboidcache::instance()->dump ();
    dboidcache::instance()->reload ();
    printf ("after reload\n");
    dboidcache::instance()->dump ();
    if (dboidcache::instance()->toLog ("10.0.2.2", "1.3.6.1.3"))
      printf ("to log OK\n");
  }
  catch (std::string &e)
  {
    printf ("error: %s\n", e.c_str());
    exit (-1);
  }
}

#endif
