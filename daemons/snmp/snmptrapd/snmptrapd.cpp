#include "snmptrapd.h"
#include "trapmonitor.h"

namespace netmon
{
  snmp_trap_daemon::snmp_trap_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  snmp_trap_daemon::~snmp_trap_daemon ()
  {
    delete m_monitor;
  }

  void snmp_trap_daemon::init ()
  {
    daemon::init();
    m_monitor = new trapmonitor (m_db, this);
  }

  int snmp_trap_daemon::run ()
  {
    m_monitor->run();
    return 0; // never reached
  }
}
