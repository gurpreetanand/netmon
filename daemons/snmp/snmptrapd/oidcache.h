#ifndef __CACHE__
#define __CACHE__

#include <string>
#include <set>

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

namespace netmon
{
  class oidcache
  {
    public:
      oidcache ();
      virtual ~oidcache ();

      bool toLog (const char *, const char *, unsigned int &);
      void addToLogList (unsigned long, unsigned int, const char *);
      void addToLogList (const char *, unsigned int, const char *);
      void clearLogList ();
      void clearNewList ();

    protected:
      struct oidentry
      {
        oidentry (const char *oid, unsigned int id, bool isnew)
          : m_oid (oid), m_id (id), m_isNew (isnew)
        {}
        oidentry (const char *oid, unsigned int id)
          : m_oid (oid), m_id (id), m_isNew (false)
        {}
        oidentry (const char *oid)
          : m_oid (oid), m_id (0), m_isNew (false)
        {}
        std::string m_oid;
        unsigned int m_id;
        bool m_isNew;
      };

      struct oidcomp
      {
        bool operator() (const oidentry &a, const oidentry &b) const
        {
          return std::less<std::string>() (a.m_oid, b.m_oid);
        }
      };

      typedef std::set<oidentry, oidcomp> oidset;
      typedef hash_map<unsigned long, oidset> iphash;

      void updateCache (unsigned long, const char *, unsigned int = 0, bool = true);
      void clearList (iphash &);

      iphash m_newOIDs;
      iphash m_logOIDs;
  };
}

#endif // __CACHE__
