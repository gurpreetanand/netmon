#ifndef __TRAP_ALERT_MANAGER__
#define __TRAP_ALERT_MANAGER__

#include "alertmanager.h"

namespace netmon
{
  class trap_alert_manager : public alert_manager
  {
    public:
      trap_alert_manager (db *mydb)
        : alert_manager (mydb, false)
      {}

      virtual ~trap_alert_manager ()
      {}

    protected:
      virtual void getReferenceTable (std::string &table)
      {
        table = "snmpoids";
      }

      virtual void getOverName (std::string &name)
      {
        name = "SNMP_TRAP";
      }

      virtual void getBelowName (std::string &name)
      {
      }
  };
}

#endif // __TRAP_ALERT_MANAGER__
