#ifndef __GETNAME__
#define __GETNAME__

int getname(char *smbhost, int port, char *servername, char *domain,
            char *inname, char *indomain, char *error_message);

#endif // __GETNAME__
