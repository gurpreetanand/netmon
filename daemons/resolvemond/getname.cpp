#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/types.h>

#include "netmon.h"
#include "log.h"

#define MAXLINE          1024

unsigned short lendian(unsigned char first, unsigned char second);
int prepare_114(char *line);
static int check_error(char *line);
static int analyze_114(char *line, char *inname, char *indomain);
static int str_copy(char *dest, char *src);

char    uid_l, uid_h, tid_l, tid_h;
int	unicode;

/******************************************************************************
* 
* Name: check_error_nbt
* Description: checks if received packet is a nbt response we expect
* 
* Arguments: Buffer that contains an NetBIOS packet
* Returns: 0 if no error, -1 otherwise
*
*****************************************************************************/

static int check_error_nbt(char *line, char *error_message)
{
  /* Positive session response */

  if ((unsigned char)*(line) == 0x82)
    return 0;
  /* No error */

  /* Negative session response */
  if ((unsigned char)*(line) == 0x83)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Negative session response, error code = %d", (unsigned char)*(line+4)); 
    return -5;
  } 

  /* Retarget session response */
  if ((unsigned char)*(line) == 0x84)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Netmon does not support retargeting.");
    return -6;
  }

  netmon::log::instance()->add(netmon::_eLogError, "Unknown session request response: %d.", (unsigned char)*(line));

  return -7;
}

/******************************************************************************
* 
* Name: NB_Encode
* Description: encodes string to NetBIOS preffered shape
* 
* Arguments: destination string, source string
* Returns: pointer to destination string
*
*****************************************************************************/
 
static unsigned char *NB_Encode( unsigned char *dst, unsigned char *name )
  {
  int i = 0;
  int j = 0;

  /* Encode the name. */
  while( ('\0' != name[i]) && (i < 16) )
    {
    dst[j++] = 'A' + ((name[i] & 0xF0) >> 4);
    dst[j++] = 'A' + (name[i++] & 0x0F);
    }

  /* Encode the padding bytes. */
  while( j < 32 )
    {
    dst[j++] = 'C';
    dst[j++] = 'A';
    }

  /* Terminate the string. */
  dst[32] = '\0';
  return( dst );

} /* NB_Encode */

/******************************************************************************
* 
* Name: prepare_nbt_129
* Description: assembles Netbios packet 129 (command = 0x81)
* 
* Arguments: Buffer to store NetBIOS packet
* Returns:   size of packet
*
*****************************************************************************/

static int prepare_nbt_129(char *line, char *server_name)
{
  char *pointer;
  char called_name[128], calling_name[128];

  pointer = line;

  /* Type */
  *pointer++ = 129;
  
  /* Flags */
  *pointer++ = 0;

  /* Length - to be determined later */
  *pointer++ = 0;
  pointer++;
  
  NB_Encode((unsigned char *) called_name, (unsigned char *) server_name);
  NB_Encode((unsigned char *) calling_name, (unsigned char *) "NETMON");

  *pointer++ = 0x20;
  
  strcpy(pointer, called_name);
  pointer += strlen(called_name) + 1;
  
  *pointer++ = 0x20;
  
  strcpy(pointer, calling_name);
  pointer += strlen(calling_name) + 1;

  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  
  *(line+3) = pointer - line - 4;

  return pointer - line;  
}

/******************************************************************************
* 
* Name: getspace
* Description: gets machine netbios name
* 
* Arguments: SMB host, port number,
*            server name, domain name
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int getname(char *smbhost, int port, char *servername, char *domain,
            char *inname, char *indomain, char *error_message)
{
  int                  sockfd, byteswritten, smb_len;
  struct sockaddr_in   serv_addr;
  char                 line[MAXLINE];
  int                  connect_reply = -1; 

  /* Fill the structure serv_addr with the actuall server address */
  
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family      = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(smbhost);
  serv_addr.sin_port        = htons(port);
          
  /* Open a TCP socket */
  if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Could not allocate socket.", port);
    return -1;
  }

  // set timeout value to 2.5 seconds
  timeval tv;
  tv.tv_sec = 2;
  tv.tv_usec = 500000;
  setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof (tv));
  setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *) &tv, sizeof (tv));

  /* Connect to the SMB server */
  connect_reply = connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

  if (connect_reply < 0)
  {
    netmon::log::instance()->add(netmon::_eLogInfo, "%d: Cannot connect to SMB server. ", port);

    /* Close socket */
    close(sockfd);

    return -2;
  }

  /* If SMB is going over NetBIOS, establish session */
  if (port == 139)
  {
    int nbt_len;

    memset(line, '\0', MAXLINE);
    
    /* 0x81 = SESSION REQUEST */
    nbt_len = prepare_nbt_129(line, servername);
    
    if ((byteswritten = write(sockfd, line, nbt_len)) != nbt_len)
    {
      netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending NBT_129).\
                         Number of bytes sent: %d", port, byteswritten);

      /* Close socket */
      close(sockfd);

      return 3;
    }

    memset(line, '\0', MAXLINE);  
    if (read(sockfd, line, MAXLINE) < 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

      /* Close socket */
      close(sockfd);

      return 4;
    }

    if (check_error_nbt(line, error_message) != 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "%d: Could not establish netbios session. ", port);

      /* Close socket */
      close(sockfd);

      return 5;
    }

  } /* NetBIOS session */

  memset(line, '\0', MAXLINE);
  smb_len = prepare_114(line);

  if ((byteswritten = write(sockfd, line, smb_len)) != smb_len)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: Error while writing to SMB server (while sending 114).\
                         Number of bytes sent: %d", port, byteswritten);

    /* Close socket */
    close(sockfd);

    return 6;
  }

  /* Hey, I was friendly, let's see what you have to say */

  memset(line, '\0', MAXLINE);  
  if (read(sockfd, line, MAXLINE) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d: SMB server not responding. ", port);

    /* Close socket */
    close(sockfd);

    return 7;
  }

  if (check_error(line) != 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "%d, SMB Protocol negotiation failed. ", port);

    /* Close socket */
    close(sockfd);

    return 8;
  } 
      
  /* This is convenient for debugging, but we don't really need it */
  analyze_114(line, inname, indomain);

  /* Check if we actually got the name */
  if (strlen(inname) == 0)
  {
    close (sockfd);
    return 9;
  }

  /* Disconnect */
  /* Close socket */
  
  close(sockfd);
  
  /* Everything went fine if we came to this point, return 0 */
  
  return 0;
}

/******************************************************************************
* 
* Name: prepare_114
* Description: assembles SMB block 114 (SMB command = 0x72)
* 
* Arguments: Buffer to store SMB block
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int prepare_114(char *line)
{
  char *pointer, *smb_bct;
  int copied;

  pointer = line;

  memset(pointer, '\0', 1024);

  /* First three bytes are zero */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;
  
  /* SMB size will be calculated later */
  pointer++;
  
  /* SMB header */
  *pointer++ = 255;
  *pointer++ = 'S';
  *pointer++ = 'M';
  *pointer++ = 'B';
  
  /* SMB command */
  *pointer++ = 0x72;
  
  /* Error code bytes */
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;
  *pointer++ = 0;
  
  /* SMB flags */
  *pointer++ = 8;
  
  /* SMB flags2 */
  *pointer++ = 1;
  *pointer++ = 0;
  
  /* Skip IPX reserved area */
  pointer += 12;
  
  /* SMB TID is 0 at this moment */
  *pointer++ = 0;
  *pointer++ = 0;
  
  /* PID doesn't really matter for us, we'll assume 19*256 + 19 */
  *pointer++ = 19;
  *pointer++ = 19;
  
  /* UID is still 0 */
  *pointer++ = 0;
  *pointer++ = 0;
  
  /* MID is 1 , only one thread */
  *pointer++ = 1;
  *pointer++ = 0;
  
  /* Word count */
  *pointer++ = 0;

  /* Byte count */
  smb_bct = pointer;
  pointer++;
  *pointer++ = 0;
  
  /* Delimiter */
  *pointer++ = 0x02;
  
  copied = str_copy(pointer, (char *) "PC NETWORK PROGRAM 1.0");
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "MICROSOFT NETWORKS 1.03");
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "MICROSOFT NETWORKS 3.0");
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "LANMAN1.0");
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "LM1.2X002");
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "Samba");
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "NT LANMAN 1.0");
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;
  *pointer++ = 0x02;
  copied = str_copy(pointer, (char *) "NT LM 0.12");
  pointer += copied;
  /* Delimiter */
  *pointer++ = 0x00;

  /* SMB size */
  *(line+3) = pointer - line - 4;

  /* Byte number */
  *smb_bct  = pointer - smb_bct - 2;
  
  return pointer-line;
}

/******************************************************************************
* 
* Name:unicode_copy
* Description: makes an unicode string from ascii
* 
* Arguments: Buffer to store unicode string, source ascii string
* Returns: length in bytes of destination string
*
*****************************************************************************/

int unicode_copy(char *dest, char *src)
{
  int len, counter;
  char *dstposition, *srcposition;
  
  len = strlen(src);

  dstposition = dest;
  srcposition = src;
  for (counter=0; counter<len; counter++)
  {
    *dstposition++ = *srcposition++;
    *dstposition++ = '\0';  
  }
  
  return 2*len;
}

/******************************************************************************
* 
* Name: str_copy
* Description: does the same as strcpy, except that return values is different
* 
* Arguments: destination string, source string
* Returns: length in bytes of destination string
*
*****************************************************************************/

static int str_copy(char *dest, char *src)
{
  int len, counter;
  char *dstposition, *srcposition;
  
  len = strlen(src);

  dstposition = dest;
  srcposition = src;
  for (counter=0; counter<len; counter++)
    *dstposition++ = *srcposition++;
  
  return len;
}

/******************************************************************************
* 
* Name: analyze_114
* Description: gets various information from received SMB block 114
* 
* Arguments: Buffer that contains received SMB block
* Returns: 0
*
*****************************************************************************/

static int analyze_114(char *line, char *inname, char *indomain)
{
  /* Word count is always 17 for 114 SMB command */
  /* Challenge/response key is always 8 */
  /* So, our position is 81 */

  char *start, *pointer;
  char a1, a2;
  char name[128], domain[128];
  
  memset(name,   0, 128);
  memset(domain, 0, 128);
  
// *********** DEBUG SECTION **************  
//  /* Get the lower byte of flags2 */
//  flags2 = *(line+14);
//  
//  if (flags2 & 0x01)
//  {
//    // DEBUG
//    printf("Unicode supported by server.\n");
//    unicode = 1; 
//  }
//  else
//  {
//    // DEBUG
//    printf("Unicode not supported by server.\n");
//    unicode = 0;
//  }
//
//  if (flags2 & 0x01) printf ("14: 0x01\n");
//  if (flags2 & 0x02) printf ("14: 0x02\n");
//  if (flags2 & 0x04) printf ("14: 0x04\n");
//  if (flags2 & 0x08) printf ("14: 0x08\n");
//  if (flags2 & 0x10) printf ("14: 0x10\n");
//  if (flags2 & 0x20) printf ("14: 0x20\n");
//  if (flags2 & 0x40) printf ("14: 0x40\n");
//  if (flags2 & 0x80) printf ("14: 0x80\n");
//
//  /* Get the upper byte of flags2 */
//  flags2 = *(line+15);
//
//  if (flags2 & 0x01) printf ("15: 0x01\n");
//  if (flags2 & 0x02) printf ("15: 0x02\n");
//  if (flags2 & 0x04) printf ("15: 0x04\n");
//  if (flags2 & 0x08) printf ("15: 0x08\n");
//  if (flags2 & 0x10) printf ("15: 0x10\n");
//  if (flags2 & 0x20) printf ("15: 0x20\n");
//  if (flags2 & 0x40) printf ("15: 0x40\n");
//  if (flags2 & 0x80) printf ("15: 0x80\n");
// ********** END OF DEBUG SECTION **********

  /* Go to start of Buffer area */
  start   = line + 81;

  pointer = start;

  a1 = *pointer;
  a2 = *(pointer+1);

  while ((a1 != 0) || (a2 != 0))
  {
    strncat(domain, pointer, 1);
  
    pointer += 2;
    a1 = *pointer;
    a2 = *(pointer+1);
  }
  strncat(domain, "\0", 1);
  
  pointer += 2;
  a1 = *pointer;
  a2 = *(pointer+1);

  while ((a1 != 0) || (a2 != 0))
  {
    strncat(name, pointer, 1);
    
    pointer += 2;
    a1 = *pointer;
    a2 = *(pointer+1);
  }
  strncat(name, "\0", 1);


  strcpy(inname, name);
  strcpy(indomain, domain);
  // printf("Computer name: %s\n", name);
  // printf("Domain   name: %s\n", domain);
    
  return 0;
}

/******************************************************************************
* 
* Name: lendian
* Description: assembles a short integer from 2 bytes
* 
* Arguments: lower byte, higher byte
* Returns: result (short integer)
*
*****************************************************************************/

unsigned short lendian(unsigned char first, unsigned char second)
{
  unsigned short firstbyte, secondbyte;
  unsigned short result;

  firstbyte  = first;
  secondbyte = second;
  
  result = firstbyte | (secondbyte << 8);

  return result;  
}

/******************************************************************************
* 
* Name: int_lendian
* Description: assembles an integer from 4 bytes
* 
* Arguments: 4 bytes, starting from lowest to the highest
* Returns: result (integer)
*
*****************************************************************************/

int int_lendian(unsigned char first, unsigned char second, unsigned third, unsigned forth)
{
  unsigned int firstbyte, secondbyte, thirdbyte, forthbyte;
  unsigned int result;  
  
  firstbyte  = first;
  secondbyte = second;
  thirdbyte  = third;
  forthbyte  = forth;
  
  result = firstbyte | (secondbyte << 8) | (thirdbyte << 16) | (forthbyte << 24);

  return result;
}

/******************************************************************************
* 
* Name: check_error
* Description: checks if received packet is an SMB packet, and if it's 
*              status is OK
* 
* Arguments: Buffer that contains an SMB packet
* Returns: 0 if no error, -1 otherwise
*
*****************************************************************************/
            
static int check_error(char *line)
{
  int status;
  unsigned char err_1, err_2, err_3, err_4;
  unsigned char s, m, b;
  
  s     = *(line+5);
  m     = *(line+6);
  b     = *(line+7);
  
  if ((s != 'S') || (m != 'M') || (b != 'B'))
  {
    // DEBUG
    // syslog(LOG_ERR, "Error, non-SMB packet received.\n", SYSLOGLINELEN);
    return -3;
  }
  
  err_1 = *(line+9);
  err_2 = *(line+10);
  err_3 = *(line+11);
  err_4 = *(line+12);
  
  status = int_lendian(err_1, err_2, err_3, err_4);

  if (status != 0)
  {
    // DEBUG
    // sprintf(syslogbuff, "SMB Error, status = %d\n", (unsigned int)status);
    // syslog(LOG_ERR, syslogbuff, SYSLOGLINELEN);
    return -4;
  }

  return 0;
}
