#include <cstdio>
#include <string>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>

#include <cstring>
#include <cstdlib>

#include "log.h"
#include "exception.h"
#include "resolverd.h"

void sighandler (int)
{
}

void installSigHandler ()
{
  struct sigaction action;
  action.sa_handler = sighandler;
  sigemptyset (&action.sa_mask);
  action.sa_flags = 0; 

  sigaction (SIGALRM, &action, NULL);
}

int main (int argc, char **argv)
{
  try
  {
    netmon::resolver_daemon *mon = new netmon::resolver_daemon (argc, argv);
    mon->init();
    mon->start();
  }
  catch (int e)
  {
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, strerror (e));
    exit (-1);
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    exit (-1);
  }
  return 0; // never reached
}
