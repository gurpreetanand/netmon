#ifndef __CACHE__
#define __CACHE__

#include <stdint.h>
#include <string>
#include <list>
#include <set>

#include "../netmond/mutex.h"
#include "../netmond/lock.h"

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

namespace netmon
{
  class cache
  {
    public:
      enum status { _eNew, _eNewDNSName, _eNewNBName, _eOld };

      cache ();
      virtual ~cache ();

      bool needsUpdate (uint32_t);
      void update (uint32_t);
      bool update (uint32_t, const char *, bool, bool = true);

      virtual void init () {}
      virtual void dump ();

    protected:
      mutex m_mutex;

      struct new_name;
      virtual void dumpName (const new_name &);

      struct ipnames
      {
        ipnames ()
          : m_IP (0), m_NBName (""), m_timestamp (-1)
        {}

        ipnames (uint32_t ip, int ts)
          : m_IP (ip), m_timestamp (ts)
        {}

        ipnames (uint32_t ip, const char *dnsname, const char *nbname, int ts = -1)
          : m_IP (ip), m_NBName (std::string (nbname)), m_timestamp (ts)
        {
          m_DNSNames.insert (std::string (dnsname));
        }

        uint32_t m_IP;
        std::string m_NBName;
        std::set<std::string> m_DNSNames;
        int m_timestamp;
      };

      typedef hash_map<uint32_t, ipnames> ipnameshash;
      ipnameshash m_hash;

      struct new_name
      {
        new_name (uint32_t ip, const char *name, bool isDNS)
          : m_ip (ip), m_name (name), m_isDNS (isDNS)
        {}

        uint32_t m_ip;
        std::string m_name;
        bool m_isDNS;
      };

      typedef std::list<new_name> name_list_t;
      name_list_t m_dirtyList;
  };
}

#endif // __CACHE__
