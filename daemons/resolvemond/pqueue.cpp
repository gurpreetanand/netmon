#include <cstdio>
#include <errno.h>
#include <mqueue.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main (int argc, char *argv[])
{
  if (argc == 1)
  {
    printf ("usage: %s IP1 [IP2] ...\n", argv[0]);
    return 0;
  }

//  if (mq_unlink ("/ipqueue") != 0)
//    printf ("cannot unlink queue\n");
  mq_attr m_qAttr;
  m_qAttr.mq_maxmsg = 16384;
  m_qAttr.mq_msgsize = 4;
  mqd_t fd = mq_open ("/ipqueue", O_CREAT | O_NONBLOCK | O_WRONLY, 0777, &m_qAttr);
  if (fd == (mqd_t) -1)
  {
    perror ("mq_open");
    return -1;
  }

  mq_getattr (fd, &m_qAttr);
printf ("queue: maxmsg %ld msgsize %ld\n", m_qAttr.mq_maxmsg, m_qAttr.mq_msgsize);

  for (int i = 1; i < argc; ++i)
  {
    in_addr_t addr = inet_addr (argv[i]);
    int ret = mq_send (fd, (const char *) &addr, sizeof (addr), 0);
    if (ret == -1 && errno == EAGAIN)
      printf ("queue full!\n");
  }
}
