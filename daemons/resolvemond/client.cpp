#include <cstdio>
#include <cstdlib>
#include <errno.h>
#include <mqueue.h>
#include <unistd.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main (int argc, char *argv[])
{
  if (argc == 1)
  {
    printf ("usage: %s no_of_ips\n", argv[0]);
    return 0;
  }

  int cnt = atoi (argv[1]);
  if (cnt < 1)
  {
    printf ("%d is not a valid number of IPs\n", cnt);
    return -1;
  }

  mq_attr m_qAttr;
  m_qAttr.mq_maxmsg = 16384;
  m_qAttr.mq_msgsize = 4;
  mqd_t fd = mq_open ("/ipqueue", O_CREAT | O_NONBLOCK | O_WRONLY, 0777, &m_qAttr);
  if (fd == (mqd_t) -1)
  {
    perror ("mq_open");
    return -1;
  }

  mq_getattr (fd, &m_qAttr);
  printf ("queue: maxmsg %ld msgsize %ld\n", m_qAttr.mq_maxmsg, m_qAttr.mq_msgsize);

  srandom ((unsigned int) time (0));
  in_addr_t addr;

  for (int i = 0; i < cnt; ++i)
  {
    if (i % 5 == 0 && i != 0)
      sleep (5);
    addr = random();
    int ret = mq_send (fd, (const char *) &addr, sizeof (addr), 0);
    if (ret == -1 && errno == EAGAIN)
    {
      printf ("queue full!\n");
      sleep (10);
    }
  }
}
