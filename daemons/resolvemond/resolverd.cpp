#include "resolverd.h"
#include "asyncresolver.h"
#include "dbcache.h"

namespace netmon
{
  resolver_daemon::resolver_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  resolver_daemon::~resolver_daemon ()
  {
    delete m_resolver;
  }

  void resolver_daemon::init ()
  {
    daemon::init();
    m_cache = new db_cache (m_db);
    m_cache->init();
    m_resolver = new async_resolver (m_cache, m_db);
  }

  int resolver_daemon::run ()
  {
    m_resolver->run();
    return 0;
  }
}
