#include "asyncresolver.h"
#include "db.h"
#include "cache.h"
#include "exception.h"
#include "udns/udns.h"

#include <cstdio>
#include <cstring>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>

namespace netmon
{
  async_resolver::async_resolver (cache *mycache, db *mydb)
    : m_cache (mycache), m_db (mydb), m_runningQueries (0), m_enqueuedIPs (0)
  {
    init();
  }

  async_resolver::~async_resolver ()
  {
    mq_close (m_queue);
    mq_unlink ("/ipqueue");
  }

  void async_resolver::run ()
  {
    signal_handler::instance()->register_handler_ev (SIGTERM, &m_sigHandler);

    drainQueue();

    while (!m_sigHandler.isTermSignaled())
      event_loop (EVLOOP_ONCE);
  }

  void async_resolver::onTimeout (int fd, short, void *t)
  {
    async_resolver *me = reinterpret_cast<async_resolver *> (t);
    me->onTimeout ();
  }

  void async_resolver::onTimeout ()
  {
    timeval tv = { 120, 0 };
    evtimer_add (&m_timer, &tv);

    m_cache->dump();

    m_localNets.clear();
    initLocalNets();
  }

  void async_resolver::onRead (int fd, short, void *t)
  {
printf ("onRead\n");
    async_resolver *me = reinterpret_cast<async_resolver *> (t);
    me->onRead (fd);
  }

  void async_resolver::onRead (int fd)
  {
    if (fd == m_queue)
    {
      printf ("queue event\n");
      drainQueue();
    }
    if (fd == m_fd)
    {
      printf ("dns event\n");
      dns_ioevent (0, time (0));
    }
  }

  void async_resolver::onDNSData (dns_ctx *ctx, void *result, void *data)
  {
    wrapper *w = reinterpret_cast<wrapper *> (data);
    query *q = w->m_query;
    async_resolver *r = w->m_resolver;
    delete w;
    r->onDNSData (ctx, result, q);
  }

  void async_resolver::onDNSData (dns_ctx *ctx, void *result, query *q)
  {
printf ("onDNSData\n");
    --m_runningQueries;
    int r = dns_status (ctx);
    in_addr addr;
    addr.s_addr = q->m_ip;
    printf ("%s -> ", inet_ntoa (addr));

    if (result == 0)
    {
      dns_strerror (r);
      m_cache->update (q->m_ip);
      delete q;
      printf ("no data\n");
      sendQueuedQueries();
      return;
    }

    dns_parse p;
    dns_rr rr;
    const unsigned char *pkt = (unsigned char *) result;
    const unsigned char *end = pkt + r;
    const unsigned char *cur = dns_payload (pkt);
    unsigned char dn[DNS_MAXDN];
    dns_getdn (pkt, &cur, end, dn, sizeof (dn));
    dns_initparse (&p, NULL, pkt, cur, end);

    p.dnsp_qtyp = DNS_T_PTR;
    p.dnsp_qcls = DNS_C_IN;

    while (dns_nextrr (&p, &rr))
      handleRR (&p, &rr, q->m_ip);

    free (result);
    delete q;

    sendQueuedQueries();
  }

  void async_resolver::handleRR (dns_parse *p, dns_rr *rr, uint32_t ip)
  {
    const unsigned char *pkt = p->dnsp_pkt;
    const unsigned char *end = p->dnsp_end;
    const unsigned char *dptr = rr->dnsrr_dptr;
    unsigned char *dn = rr->dnsrr_dn;

    if (rr->dnsrr_typ == DNS_T_PTR)
      if (dns_getdn(pkt, &dptr, end, dn, DNS_MAXDN) > 0)
      {
        printf("%s\n", dns_dntosp(dn));
        m_cache->update (ip, dns_dntosp (dn), true);
      }
  }

  void async_resolver::init ()
  {
    initUDNS();
    openQueue();
    initLocalNets();
    initEvents();
    m_ipqueue = new ip_queue;
    nb_thread *nbthread = new nb_thread (m_ipqueue, m_cache);
    nbthread->start();
  }

  void async_resolver::initEvents ()
  {
    event_init();

    event_set (&m_qEvent, m_queue, EV_READ | EV_PERSIST, onRead, this);
    event_add (&m_qEvent, 0);

    event_set (&m_sockEvent, m_fd, EV_READ | EV_PERSIST, onRead, this);
    event_add (&m_sockEvent, 0);

    timeval tv = { 120, 0 };
    evtimer_set (&m_timer, onTimeout, this);
    evtimer_add (&m_timer, &tv);
  }

  void async_resolver::initUDNS ()
  {
    if (dns_init (0, 0) < 0)
      throw netmon_exception ("Unable to initialize udns lib.");
    m_fd = dns_open (0);
    printf ("dns sock: %d\n", m_fd);
    if (m_fd < 0)
      throw netmon_exception ("Unable to initialize udns context.");
  }

  void async_resolver::initLocalNets ()
  {
    PGresult *res = m_db->execQuery ("SELECT NETWORK, BROADCAST FROM LOCALNETS");

    for (int i = 0; i < PQntuples (res); ++i)
    {
      m_localNets.push_back (std::make_pair (inetaddr (PQgetvalue (res, i, 0)),
                                             inetaddr (PQgetvalue (res, i, 1))));
    }
    PQclear (res);
  }

  void async_resolver::openQueue ()
  {
    mq_attr attr;
    attr.mq_maxmsg = maxQueueSize;
    attr.mq_msgsize = sizeof (uint32_t);

    m_queue = mq_open ("/ipqueue", O_RDONLY | O_NONBLOCK | O_CREAT, 0777, &attr);
    if (m_queue == (mqd_t) -1)
      throw netmon_exception (strerror (errno));

    mq_getattr (m_queue, &m_qAttr);
printf ("queue: maxmsg %ld msgsize %ld\n", m_qAttr.mq_maxmsg, m_qAttr.mq_msgsize);
  }

  void async_resolver::drainQueue ()
  {
    uint32_t ip;
    int ret = 0;

    while (true)
    {
      ret = mq_receive (m_queue, (char *) &ip, m_qAttr.mq_msgsize, 0);
      if (ret == -1 && errno == EAGAIN)
        return;
      if (ret == -1)
        throw netmon_exception (strerror (errno));
      printf ("read from queue %d\n", ip);
      if (m_cache->needsUpdate (ip))
        sendQuery (ip);
    }
  }

  bool async_resolver::sendQuery (uint32_t ip, bool toPoll /* = true */)
  {
    bool enqueued = false;

    if (isLocalAddress (ip))
    {
      if (m_ipqueue->full())
      {
        m_queuedIPs.push_back (ip);
        ++m_enqueuedIPs;
        enqueued = true;
      }
      else
        m_ipqueue->push (ip);
    }

    if (m_runningQueries >= maxRunningQueries)
    {
      if (enqueued)
        return false;

//in_addr a;
//a.s_addr = ip;
//printf ("pushin into queued query %s\n", inet_ntoa (a));

      m_queuedIPs.push_back (ip);
      ++m_enqueuedIPs;
      return false;
    }

    static unsigned char dn[DNS_MAXDN];
    static in_addr addr;
    addr.s_addr = ip;

    dns_a4todn (&addr, 0, dn, sizeof (dn));
    query *q = new query (ip, dn);
    wrapper *w = new wrapper;
    w->m_query = q;
    w->m_resolver = this;

    if (!dns_submit_dn (0, dn, DNS_C_IN, DNS_T_PTR, DNS_NOSRCH, 0, onDNSData, w))
    {
      printf ("submit failed\n");
      delete w;
      delete q;
      return false;
    }

    ++m_runningQueries;
    if (toPoll)
      dns_timeouts (0, -1, 0);
    return true;
  }

  void async_resolver::sendQueuedQueries ()
  {
    bool ret;
    uint32_t ip = 0;

    while (m_enqueuedIPs > 0 && m_runningQueries < maxRunningQueries)
    {
      ip = m_queuedIPs.front();
      if (m_cache->needsUpdate (ip))
      {
        ret = sendQuery (ip, false);
        if (!ret)
          break;
      }
      m_queuedIPs.pop_front();
      --m_enqueuedIPs;
    }
    dns_timeouts (0, -1, 0);
  }

  bool async_resolver::isLocalAddress (uint32_t ip)
  {
    inetaddr a(ip);
    in_addr_list_t::iterator end = m_localNets.end();
    for (in_addr_list_t::iterator i = m_localNets.begin(); i != end; ++i)
      if ((*i).first <= a && a <= (*i).second)
        return true;
    return false;
  }

  async_resolver::query::query (uint32_t ip, const unsigned char *dn)
    : m_ip (ip)
  {
    unsigned int len = dns_dnlen (dn);
    m_dn = new unsigned char[len];
    memcpy (m_dn, dn, len);
  }

  async_resolver::query::~query ()
  {
    delete[] m_dn;
  }
}

#ifdef __TEST__

using namespace netmon;

int main()
{
  async_resolver *r = new async_resolver;
  r->run();
}

#endif // __TEST__
