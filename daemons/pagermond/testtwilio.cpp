#include <iostream>
#include <vector>

#include "twilio-cplusplus/Utils.h"
#include "twilio-cplusplus/Rest.h"
#include "twilio-cplusplus/TwiML.h"

#define API_VERSION "2010-04-01"


int main(int argc, char** argv) {

    if (argc != 6) {
      std::cout << "Usage: " << argv[0] << " <twilio sid> <twilio token> <from number> <to number> <message>";
      return 1;
    }

    twilio::Rest t(argv[1], argv[2]);
    std::vector<twilio::Var> vars;

    vars.push_back(twilio::Var("From", argv[3]));
    vars.push_back(twilio::Var("To", argv[4]));
    vars.push_back(twilio::Var("Body", argv[5]));

    std::cout << "/" + std::string(API_VERSION) + "/Accounts/" + std::string(argv[1]) + "/SMS/Messages" << std::endl;
    std::string response = t.request("/" + std::string(API_VERSION) + "/Accounts/" + std::string(argv[1]) + "/SMS/Messages", "POST", vars);

    std::cout << response << std::endl;

    // TODO: If people use twilio, actually parse the response XML.
    if (response.find("RestException") == std::string::npos) {
      return true;
    } else {
      return false;
    }

  return 0;
}
