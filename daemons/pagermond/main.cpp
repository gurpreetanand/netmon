#include <errno.h>
#include <sys/types.h>

#include <string>
#include <cstdio>
#include <cstring>

#include "pagermond.h"
#include "exception.h"
#include "log.h"

int main (int argc, char **argv)
{
  try
  {
    netmon::pagermon_daemon *mon = new netmon::pagermon_daemon(argc, argv);
    mon->init();
    mon->start();
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
  }
  catch (int e)
  {
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, strerror (e));
  }
  return 0;
}
