#ifndef __PAGER_SENDER__
#define __PAGER_SENDER__

#include <list>
#include <string>
#include <vector>

#include "twilio-cplusplus/Utils.h"
#include "twilio-cplusplus/Rest.h"

namespace netmon
{
  class db;

  class pager_sender
  {
    public:
      pager_sender (db *mydb, const std::string &sid, const std::string &token, const std::string &number);

      void run ();

    protected:
      struct pager_data
      {
        unsigned int m_id;
        std::string m_message;
        std::string m_pager;
        std::string m_terminal;
      };

      typedef std::list<pager_data *> pagers_t;

      void clear ();
      void getMediaID ();
      void getPendingPages ();
      void sendAlerts ();
      void updateAlert (unsigned int, bool);
      void fixMessage (char *, std::string &);
      bool sendPage(std::string number, std::string message);

      db *m_db;
      pagers_t m_pagers;
      std::string m_twilioSid;
      std::string m_twilioToken;
      std::string m_twilioNumber;
      unsigned int m_mediaID;
      bool m_validConfig;
      twilio::Rest m_rest;
  };
}

#endif // __PAGER_SENDER__
