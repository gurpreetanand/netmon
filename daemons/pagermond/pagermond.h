#ifndef __PAGER_DAEMON__
#define __PAGER_DAEMON__

#include "daemon.h"

#include <dbus-c++/dbus.h>
#include <signal.h>

namespace netmon
{
  class pager_sender;

  class pagermon_daemon : public daemon
  {
    public:
      pagermon_daemon (int, char **);
      ~pagermon_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "pagermond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/pagermond";
      }

      int installTimer ();

      static void onTimer (int);

      enum { timer_interval = 60 };

      pager_sender *m_pSender;
      static sig_atomic_t m_timerFired;
      static DBus::BusDispatcher m_dispatcher;
  };
}

#endif //__PAGER_DAEMON__
