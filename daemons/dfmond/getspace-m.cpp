#include <stdio.h>
#include <stdlib.h>

#include "bigint/BigInteger.hh"
#include "getspace.h"

/* Test main function */
int main(int argc, char *argv[])
{
  char error_message[1024];
  int port;
  int percentage;
  int result;

  if (argc < 3){
    printf("Wrong number of arguments: %d\n", argc - 1);
    printf("Usage: %s <IP address> <Partition> [port]\n", argv[0]);
    exit(1);
  }
  
  if (argc == 4){
    port = atoi(argv[3]);
  }
  else{
    port = 5001;
  }

  try {
    BigInteger total = -1;
    BigInteger available = -1;

    result = getspace(argv[1], port, argv[2], &percentage, error_message, total, available);
    // printf("result is fetched");
    if (result != 0){         
      printf("An error ocurred: %s.\n", error_message);
    } else
    {
      printf("Percentage used: %d%\n", percentage);
    }   

  } catch (char const* x) {
    printf("\n\n\nexception %s",x);
  }
  
           
  return 0;
}
