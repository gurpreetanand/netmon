#include "dfmond.h"
#include "dfmonitor.h"
#include "log.h"

#include <unistd.h>

namespace netmon
{
  dfmon_daemon::dfmon_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  dfmon_daemon::~dfmon_daemon ()
  {
    delete m_dfMonitor;
  }

  void dfmon_daemon::init ()
  {
    daemon::init();
    m_dfMonitor = new dfmonitor (m_db);
  }

  int dfmon_daemon::run ()
  {
    while (!isTermSignaled())
    {
//      log::instance()->add(_eLogInfo, "New scan"); // commented on 14/12/2018 by sameer
      m_dfMonitor->run();
      usleep (60);
    }
    return 0;
  }
}
