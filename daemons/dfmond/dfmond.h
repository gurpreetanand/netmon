#ifndef __DF_DAEMON__
#define __DF_DAEMON__

#include "daemon.h"

namespace netmon
{
  class dfmonitor;

  class dfmon_daemon : public daemon
  {
    public:
      dfmon_daemon (int, char **);
      ~dfmon_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "dfmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/dfmond";
      }

      dfmonitor *m_dfMonitor;
  };
}

#endif //__DF_DAEMON__
