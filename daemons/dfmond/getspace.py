# import os
# import platform
# from subprocess import check_output

# SYSTEM = platform.system()
# if SYSTEM == 'Windows':
# 	PARTITION = 'C:'
# 	result = check_output(['wmic logicaldisk get size,freespace,caption'])
# else:
# 	PARTITION = '/dev/sda1'
# 	result = check_output(['/bin/df'])

# lines = result.split('\n')
# lines = lines[1:]
# for line in lines:
# 	if line.startswith(PARTITION):
# 		print(line)
# 	else:
# 		continue
from __future__ import division

import re
import socket
import platform

# SYSTEM = platform.system()



def detect_sys(result):
	if 'C:' in result:
		return 'Windows'
	elif '/dev/disk0s2' in result:
		return 'Darwin'
	else:
		return 'Linux'



def compute(line):
	words = line.split()
	info = {}
	info['Partition'] = words[0]
	if SYSTEM == 'Windows':
		info['Used'] = words[1]
		info['Available'] = words[2]
	else:
		info['Used'] = words[2]
		info['Available'] = words[3]
	info['Total'] = str(int(info['Used']) + int(info['Available']))
	info['Percentage Used'] = '{}%'.format(round(((int(info['Used'])/int(info['Total']))*100), 2))
	info['Percentage Available'] = '{}%'.format(round(((int(info['Available'])/int(info['Total']))*100), ))
	return info

def printinfo(info):
	for k,v in info.items():
		print('{}: {}'.format(k,v))

# next create a socket object 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)          
print "Socket successfully created"
  
# reserve a port on your computer in our 
# case it is 12345 but it can be anything 
port = 49152     
s.connect(('192.168.1.142',port))
# Next bind to the port 
# we have not typed any ip in the ip field 
# instead we have inputted an empty string 
# this makes the server listen to requests  
# coming from other computers on the network   
print "socket binded to %s" %(port) 
  
# put the socket into listening mode 
# s.listen(5)      
print "socket is listening"            
  
# a forever loop until we interrupt it or  
# an error occurs 
result = s.recv(4096)
print(result)
SYSTEM = detect_sys(result)

if SYSTEM == 'Windows':
	PARTITION = 'C:'
elif SYSTEM == 'Darwin':
	PARTITION = '/dev/disk0s2'
else:
	PARTITION = '/dev/sda1'
lines = result.split('\n')
lines = lines[1:]
for line in lines:
	if line.startswith(PARTITION):
		# print(line)
		info = compute(line)
		printinfo(info)
	else:
		continue
s.close()