#ifndef __DF_ALERT_MANAGER__
#define __DF_ALERT_MANAGER__

#include "srvalertmanager.h"
#include "alertutil.h"

namespace netmon
{
  class df_alert_manager : public service_alert_manager
  {
    public:
      using alert_manager::getAlerts;
      df_alert_manager (db *);
      void sendUp (unsigned int id, values_map vmap);

    protected:
      virtual void getReferenceTable (std::string &s)
      {
        s = "df_servers";
      }

      virtual void getOverName (std::string &s)
      {
        s = "DF_OVER_THRESHOLD";
      }

      virtual void getBelowName (std::string &s)
      {
        s = "DF_BELOW_THRESHOLD";
      }

      virtual void getUpName (std::string &s)
      {
        s = "DF_SERVICE_UP";
      }

      virtual void getDownName (std::string &s)
      {
        s = "DF_SERVICE_DOWN";
      }
  };
}

#endif // __DF_ALERT_MANAGER__
