#include "dfalertmngr.h"
#include "alertthrottler.h"

#include <sstream>

namespace netmon
{
  df_alert_manager::df_alert_manager (db *mydb)
    : service_alert_manager (mydb, true, true)
  {
  }

  void df_alert_manager::sendUp (unsigned int id, values_map vmap)
  {
    alert_map::iterator i = m_alerts.find (id);
    if (i == m_alerts.end())
      return;

    alerthandler *h = createAlertHandler (i->second);
    h->setRecovery (true);

    h->setSubject (m_upSubject);
    h->setTemplate (m_upTemplate);

    getReferenceData (id, h->getMap());
    dispatchAlert (h, vmap);
    toggleAlertFlag (i->second->m_triggerID);
  }

}
