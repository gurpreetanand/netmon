#ifndef __DF_MONITOR__
#define __DF_MONITOR__

#include <string>
#include <list>

#include "bigint/BigInteger.hh"

namespace netmon
{
  class db;
  class df_alert_manager;

  class dfmonitor
  {
    public:
      dfmonitor (db *);
      ~dfmonitor ();

      void run ();

    protected:
      struct server_data
      {
        unsigned int m_id;
        unsigned short m_port;
        unsigned int m_threshold;
        int m_status;
        std::string m_ip;
        std::string m_partition;
        std::string m_name;
      };

      typedef std::list<server_data *> server_list;

      void getServers ();
      void checkServer (server_data *);
      void clearServerList ();
      void resetFlags ();
      void setPendingFlag (unsigned int);
      void updateDBFailure (unsigned int, char *);
      void updateDBSuccess (int, int, BigInteger, BigInteger);

      server_list m_list;
      db *m_db;
      df_alert_manager *m_alertManager;
  };
}

#endif // __DF_MONITOR__
