#include "dfmonitor.h"
#include "dfalertmngr.h"
#include "db.h"
#include "getspace.h"
#include "log.h"
#include "bigint/BigIntegerUtils.hh"

#include <errno.h>
#include <cstdlib>
#include <sstream>

namespace netmon
{
  dfmonitor::dfmonitor (db *mydb)
    : m_db(mydb)
  {
    resetFlags();
    m_alertManager = new df_alert_manager (mydb);
    m_alertManager->init();
  }

  dfmonitor::~dfmonitor ()
  {
  }

  void dfmonitor::run ()
  {
    clearServerList();
    getServers();
    for (server_list::iterator i = m_list.begin(); i != m_list.end(); ++i)
      checkServer (*i);
  }

  void dfmonitor::checkServer (server_data *d)
  {
    static char err[512];
    int perc = 0;
    BigInteger total;
    BigInteger available;
    values_map vmap;

    setPendingFlag (d->m_id);

    vmap[alerthandler::eHostIP] = keyword_value (d->m_ip);
    vmap[alerthandler::eHostName] = keyword_value (d->m_name);
    vmap[alerthandler::ePartition] = keyword_value (d->m_partition);

    int ret = getspace (d->m_ip.c_str(), d->m_port, d->m_partition.c_str(), &perc, err, total, available);
    if (ret != 0)
    {
      if (ret > 0)
      {
        log::instance()->add(_eLogWarning, "Host %s, port %d, partition %s, could not connect.",
                d->m_ip.c_str(), d->m_port, d->m_partition.c_str());
      }
      else
      {
        log::instance()->add(_eLogWarning, "Host %s, port %d, partition %s, connected, but error ocurred.",
                d->m_ip.c_str(), d->m_port, d->m_partition.c_str());
      }
      updateDBFailure (d->m_id, err);
      vmap[alerthandler::eErrMsg] = keyword_value (err);
      vmap[alerthandler::eSysErr] = keyword_value (strerror (ret));
      m_alertManager->getDownAlerts (d->m_id, vmap);
    }
    else
    {
      updateDBSuccess (d->m_id, perc, total, available);
      vmap[alerthandler::eNewStatus] = keyword_value (perc);
      m_alertManager->getAlerts (d->m_id, perc, vmap);
      if (d->m_status == -1) {
        m_alertManager->sendUp(d->m_id, vmap);
      }
    }
  }

  void dfmonitor::getServers ()
  {
    std::ostringstream sql;
    sql.str("");
    
    sql << "SELECT SRV_ID, IP, PARTITION, PORT, SERVERNAME, "
        << "THRESHOLD, STATUS FROM DF_SERVERS WHERE PENDING != 'Y' "
        << "AND (TIMESTAMP + INTERVAL) < " << (int) time(0);

    PGresult *res = m_db->execQuery (sql.str().c_str());
    for (int i = 0; i < PQntuples(res); ++i)
    {
      server_data *d = new server_data;
      d->m_id = atoi (PQgetvalue(res, i, 0));
      d->m_ip = std::string (PQgetvalue(res, i, 1));
      d->m_partition = std::string (PQgetvalue(res, i, 2));
      d->m_port = (unsigned short) atoi (PQgetvalue(res, i, 3));
      d->m_name = std::string (PQgetvalue(res, i, 4));
      d->m_threshold = atoi (PQgetvalue(res, i, 5));
      d->m_status = atoi (PQgetvalue(res, i, 6));
      m_list.push_back (d);
    }
    PQclear (res);
  }

  void dfmonitor::resetFlags ()
  {
    m_db->execSQL ("UPDATE DF_SERVERS SET MESSAGE = '', \
    TIMESTAMP = 0, PENDING = 'N'");
  }

  void dfmonitor::setPendingFlag (unsigned int id)
  {
    std::ostringstream sql;
    sql.str("");
    sql << "UPDATE DF_SERVERS SET PENDING = 'Y' WHERE SRV_ID = " << id;
    m_db->execSQL (sql.str().c_str());
  }

  void dfmonitor::updateDBFailure (unsigned int id, char *msg)
  {
    static std::ostringstream sql;
    sql.str("");
    sql << "UPDATE DF_SERVERS SET STATUS = -1, TIMESTAMP = "
        << (int) time (0)
        << ", MESSAGE = '"
        << msg
        << "', PENDING = 'N' WHERE SRV_ID = "
        << id;

    m_db->execSQL (sql.str().c_str());

    sql.str("");
    sql << "INSERT INTO DF_SERVER_LOG (SRV_ID, STATUS, TIMESTAMP) "
        << "VALUES ("
        << id
        << ", -1, "
        << (int) time(0)
        << ")";

    m_db->execSQL (sql.str().c_str());
  }

  void dfmonitor::updateDBSuccess (int id, int perc, BigInteger total, BigInteger available)
  {
    static std::ostringstream sql;
    sql.str("");
    sql << "UPDATE DF_SERVERS SET STATUS = "
        << perc
        << ", TIMESTAMP = "
        << (int) time (0)
        << ", TOTAL = "
        << bigIntegerToString(total)
        << ", AVAILABLE = "
        << bigIntegerToString(available)
        << ", MESSAGE = '', "
        << "PENDING = 'N' WHERE SRV_ID = "
        << id;

    m_db->execSQL (sql.str().c_str());

    sql.str("");
    sql << "INSERT INTO DF_SERVER_LOG (SRV_ID, STATUS, TIMESTAMP, "
        << "TOTAL, AVAILABLE) VALUES ("
        << id
        << ", "
        << perc
        << ", "
        << (int) time(0)
        << ", "
        << bigIntegerToString(total)
        << ", "
        << bigIntegerToString(available)
        << ")";

    m_db->execSQL (sql.str().c_str());
  }

  void dfmonitor::clearServerList ()
  {
    for (server_list::iterator i = m_list.begin(); i != m_list.end(); ++i)
      delete *i;
    m_list.clear();
  }
}
