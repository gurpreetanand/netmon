#include "emailsender.h"
#include "arrayutil.h"
#include "db.h"
#include "email.h"
#include "log.h"
#include <sstream>

namespace netmon
{
  email_sender::email_sender (db *mydb, const std::string &server, const std::string &user, unsigned short port, int timeout)
    : m_db (mydb), m_server (server), m_user (user), m_port (port)
  {
    tv.tv_sec = timeout;
    tv.tv_usec = 10;
  }

  void email_sender::run ()
  {
    getPendingEMails();
    sendAlerts();
    clear();
  }

  void email_sender::sendSetupNotification (int f1, int f2)
  {
    std::string email;
    if (!getAdminEmail (email))
      return;

    email_data *d = new email_data;
    std::ostringstream msg;
    msg << "Your Netmon system has completed the network auto-discovery process."
        << std::endl << "Netmon has detected " << f2 << " hosts with open ports on your network, as well as "
        << f1 << " SNMP-enabled devices." << std::endl
        << "You should now log in to your Netmon system to create trackers for these services and devices.";
    d->m_id = 0;
    d->m_message = msg.str();
    d->m_subject = "Network auto-discovery process has been completed.";
    d->m_email = email;
    m_emails.push_back (d);
  }

  void email_sender::getPendingEMails ()
  {
    static const char *sql = 
    "select b.id, b.parsed_alert_message, b.parsed_subject, d.email \
     from alert_handlers a, alert_pending b, alert_medias c, users d \
     where b.handler_id = a.id \
     and d.id = a.user_id \
     and c.id = a.media_id \
     and c.name = 'email' \
     and (a.required_retries - b.retries_processed) > 0 \
     and b.sent = '0' \
     order by b.trigger_timestamp asc";

    PGresult *res;
    res = m_db->execQuery (sql);
    
    int count = PQntuples (res);
    std::ostringstream countmsg;
    countmsg <<"Pending emails count: " << count;
    netmon::log::instance()->add(netmon::_eLogDebug, countmsg.str().c_str());

     for (int i = 0; i < PQntuples (res); ++i)
     {
       email_data *d = new email_data;
       d->m_id = atoi (PQgetvalue (res, i, 0));
       d->m_message = std::string (PQgetvalue (res, i, 1));
       d->m_subject = std::string (PQgetvalue (res, i, 2));
       d->m_email = std::string (PQgetvalue (res, i, 3));
       m_emails.push_back (d);
       std::ostringstream mailmsg;
       mailmsg << "-----Alert-------\nid: " << d->m_id << "\nsubject: "<<d->m_subject<<"\nemail: "<<d->m_email<< "\nmessage: "<<d->m_message<<"\n----- EOM ------";
       netmon::log::instance()->add(netmon::_eLogDebug, mailmsg.str().c_str());
     }

     PQclear (res);
  }

  void email_sender::sendAlerts ()
  {
    int ret = 0;
    std::string email;
    for (emails_t::iterator i = m_emails.begin(); i != m_emails.end(); ++i)
    {
      ret = sendnotice (m_server.c_str(), m_port, m_user.c_str(), (*i)->m_email.c_str(), (*i)->m_subject.c_str(), (*i)->m_message.c_str(), tv);
      if ((*i)->m_id != 0)
        updateAlert ((*i)->m_id, ret == 0 ? true : false);
      else
        updateNotificationFlag ((ret == 0 ? true : false));
    }
  }

  void email_sender::updateAlert (unsigned int id, bool ok)
  {
    static std::ostringstream sql;
    sql.str("");
    if (ok)
    {
      sql << "UPDATE ALERT_PENDING SET SENT = '1', DISPATCH_TIMESTAMP = "
          << (int) time(0)
          << ", RETRIES_PROCESSED = RETRIES_PROCESSED+1 WHERE ID = "
          << id;
    }
    else
    {
      sql << "UPDATE ALERT_PENDING SET RETRIES_PROCESSED = RETRIES_PROCESSED+1"
          << ", DISPATCH_TIMESTAMP = "
          << (int) time(0)
          << " WHERE ID = "
          << id;
    }
    m_db->execSQL (sql.str().c_str());
  }

  void email_sender::updateNotificationFlag (bool sent)
  {
    if (!sent)
      return;

    try
    {
      m_db->execSQL (
      "update daemonsconfig set value = '1' where var = 'sent' and daemon_id"
      " = (select id from daemons where name = 'emailmond')");
    }
    catch (db_exception &e)
    {
      printf ("Error: %s\n", e.what());
    }
  }

  void email_sender::clear ()
  {
    for (emails_t::iterator i = m_emails.begin(); i != m_emails.end(); ++i)
      delete *i;
    m_emails.clear();
  }

  bool email_sender::getAdminEmail (std::string &email)
  {
    PGresult *res;
    res = m_db->execQuery (
    "SELECT email FROM users WHERE id = (SELECT user_id FROM user2groups WHERE "
    "group_id IN (SELECT id FROM groups WHERE group_name = 'Administrators') "
    "order by user_id limit 1)");

    if (PQntuples (res) == 0)
    {
      PQclear (res);
      return false;
    }

    email = std::string (PQgetvalue (res, 0, 0));
    PQclear (res);

    return true;
  }
}
