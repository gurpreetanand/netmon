#ifndef __EMAIL__
#define __EMAIL__

#include <sys/time.h>

int sendnotice(const char *smtphost, int smtpport, const char *emailfrom, const char *email, const char *subject, const char *message, struct timeval tv);

#endif // __EMAIL__
