#include "emailmond.h"
#include "emailsender.h"
#include "log.h"
#include "dvars.h"
#include "dbus.h"

#include <sys/time.h>

#include <string>

namespace netmon
{
  sig_atomic_t emailmon_daemon::m_timerFired = 1;
  DBus::BusDispatcher emailmon_daemon::m_dispatcher;

  emailmon_daemon::emailmon_daemon (int argc, char **argv)
    : daemon (argc, argv), m_notificationSent (0)
  {
  }

  emailmon_daemon::~emailmon_daemon ()
  {
    delete m_eSender;
  }

  void emailmon_daemon::init ()
  {
    daemon::init();

    installTimer();

    std::string server;
    std::string account;
    int port = 25, timeout;

    m_vars->get ("smtpserver", server);
    m_vars->get ("smtpaccount", account);
    m_vars->get ("smtpport", port);
    m_vars->get ("sent", m_notificationSent);
    m_vars->get ("smtp_timeout", timeout);

    m_eSender = new email_sender (m_db, server, account, port, timeout);

    DBus::default_dispatcher = &m_dispatcher;
  }

  int emailmon_daemon::run ()
  {
    DBus::Connection conn = DBus::Connection::SystemBus();
    conn.request_name ("ca.netmon.DBus.emailmond");

    dbus_emailmond_server server (conn, m_eSender);

    while (!isTermSignaled())
    {
      if (m_timerFired == 1)
      {
printf ("on timer\n");
        sendSetupNotification();
        m_eSender->run();
        m_timerFired = 0;
        netmon::log::instance()->add(netmon::_eLogInfo, "New iteration");
      }
      m_dispatcher.enter();
    }
    return 0; // never reached
  }

  int emailmon_daemon::installTimer ()
  {
    struct sigaction saction;

    saction.sa_handler = onTimer;
    if (sigemptyset (&saction.sa_mask) == -1)
      return -1;

    saction.sa_flags = 0;
    if (sigaction (SIGALRM, &saction, NULL) == -1)
      return -1;

    itimerval timer;
    timer.it_interval.tv_sec = timer_interval;
    timer.it_interval.tv_usec = 0;
    timer.it_value.tv_sec = timer_interval;
    timer.it_value.tv_usec = 0;

    if (setitimer (ITIMER_REAL, &timer, 0) == -1)
      return -1;

    return 0;
  }

  void emailmon_daemon::sendSetupNotification ()
  {
    if (m_notificationSent != 0)
      return;

    m_vars->reload();
    m_vars->get ("sent", m_notificationSent); // check once again
    if (m_notificationSent != 0)
        return;

    dvars v1 (*m_db, "snmpautod");
    dvars v2 (*m_db, "netscand");
    int f1;
    int f2;
    f1 = f2 = 0;

    v1.get ("found", f1);
    v2.get ("found", f2);

    if (f1 >= 0 && f2 >= 0)
      m_eSender->sendSetupNotification (f1, f2);
  }

  void emailmon_daemon::onTimer (int)
  {
    m_dispatcher.leave();
    m_timerFired = 1;
  }
}
