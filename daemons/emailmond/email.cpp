#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <sys/file.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <poll.h>

#include "../lib/netmon.h"
#include "../lib/log.h"

#define MAXLINE      4096
#define EMAILOUT     60
#define POLLTIME     2000

extern int  daemonstate;
int         errorcode;

/******************************************************************************
* 
* Name: readline(int fd, char *ptr, int maxlen)
* Description: reads a line from a stream socket
* 
* Arguments: Socket descriptor, pointer to a storage string, maximum length
* Returns:   Number of characters read
*
*****************************************************************************/
static int readline (int fd, char *ptr, int maxlen)
{
  static char buff[MAXLINE];
  bool has_line = false;
  int n, rc;
  char *str;
  char *prev = buff;

  n = 1;
  rc = read (fd, buff, MAXLINE);
  if (rc <= 0)
    return rc;

  if (rc < MAXLINE)
    buff[rc] = '\0';
  else
    return -1;

  str = strchr (buff, '\n');
  while (str != 0)
  {
    *str = '\0';
    has_line = true;
    printf (">>> %s\n", prev);
    if (str - buff >= maxlen)
      return 0;
    strcpy (ptr, prev);
    ++str;
    prev = str;
    str = strchr (str, '\n');
  }

  if (has_line)
    return rc;
  return 0;
}

/******************************************************************************
* 
* Name: writen(int fd, char *ptr, int maxlen)
* Description: writes a string to a stream socket
* 
* Arguments: Socket descriptor, pointer to a input string, maximum length
* Returns:   Number of characters writen
*
*****************************************************************************/
static int writen(int fd, const char *ptr, int nbytes)
{
  int nleft, nwritten;

  nleft = nbytes;

  printf ("<<< %s\n", ptr);

  while (nleft > 0) 
  {
    nwritten = write(fd, ptr, nleft);

    if (nwritten <= 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "socket writen error");

      return nwritten;  /* error */
    }

    nleft -= nwritten;
    ptr   += nwritten;
  }

  return (nbytes - nleft);
}

/******************************************************************************
* 
* Name: sendnotice(char *smtphost, int smtpport, char *email, char *message)
* Description: sends an email
* 
* Arguments: SMTP server, SMTP port, email address, message text
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int sendnotice(const char *smtphost, int smtpport, const char *emailfrom, const char *email, const char *subject, const char *message, struct timeval tv)
{
  int                  sockfd, byteswritten;
  struct sockaddr_in   serv_addr;
  char                 line[MAXLINE];
  struct hostent *server;

  server = gethostbyname(smtphost);
  if (server == 0)
  {
    printf ("cannot resolve '%s'\n", smtphost);
    return -1;
  }
  /* Fill the structure serv_addr with the actuall server address */

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family      = AF_INET;
//  serv_addr.sin_addr.s_addr = inet_addr(smtphost);
  serv_addr.sin_port        = htons(smtpport);

  bcopy ((char *)server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
  /* Open a TCP socket */
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "could not allocate socket.");
    printf ("could not allocate socket.\n");
    exit(1);
  }

//  setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof (tv));
//  setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *) &tv, sizeof (tv));

  /* Connect to the SMTP server */
  printf ("Connecting to SMTP server %s (port %d)...\n", smtphost, smtpport);
  if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
  {
    // Check for EINPROGRESS
    if (errno == EINPROGRESS) {
      // OK, we need to run a  poll
      pollfd topoll;
      topoll.fd = sockfd;
      topoll.events = POLLIN || POLLOUT;
      topoll.revents = POLLERR;
      if (poll(&topoll, 1, POLLTIME) <= 0) {
        // Poll didn't go through in time.
        netmon::log::instance()->add(netmon::_eLogError, "Cannot connect to SMTP server %s:%d", smtphost, smtpport);
        printf ("Cannot connect to SMTP server '%s' (port %d)\n", smtphost, smtpport);

        /* Close socket */
        close(sockfd);
        return -1;
      }
    } else {
      // Connect failed
      netmon::log::instance()->add(netmon::_eLogError, "Cannot connect to SMTP server %s:%d", smtphost, smtpport);
      printf ("Cannot connect to SMTP server '%s' (port %d)\n", smtphost, smtpport);

      /* Close socket */
      close(sockfd);
      return -1;
    }


  }

  /* As soon as we connect, we expect to receive code 220 */
  printf ("Connected.\n");
  if ((errorcode = readline(sockfd, line, MAXLINE)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "SMTP server not responding [%d] (expecting 220).  Timeout value [%d] seconds", errorcode, tv.tv_sec);

    /* Close socket */
    close(sockfd);

    return -1;
  }
  else if (strstr(line, "220") == NULL)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Got unexpected response from SMTP server: %s.", line);

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* OK, the response was 220, just as we expected, send HELO */

  sprintf(line, "HELO NETMON\r\n");  
  if ((byteswritten = writen(sockfd, line, strlen(line))) != (int) strlen(line))
  {
    netmon::log::instance()->add(netmon::_eLogError, "Error while writing to SMTP server (while sending HELO).\
                         Number of bytes sent: %d", byteswritten);

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* Hey, I was friendly, let's see what you have to say */

  if ((errorcode = readline(sockfd, line, MAXLINE)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "SMTP server not responding [%d] (expecting 250-1).", errorcode);

    /* Close socket */
    close(sockfd);

    return -1;
  }
  else if (strstr(line, "250") == NULL)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Got unexpected response from SMTP server: %s.", line);

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* OK, the response was 220, just as we expected, send MAIL FROM */

  sprintf(line, "MAIL FROM: <%s>\r\n", emailfrom);  
  if ((byteswritten = writen(sockfd, line, strlen(line))) != (int) strlen(line))
  {
    netmon::log::instance()->add(netmon::_eLogError, "Error while writing to SMTP server (while sending MAIL FROM).\
                         Number of bytes sent: %d", byteswritten);

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* Now we expect sender to be verified and to receive code 250 */

  if ((errorcode = readline(sockfd, line, MAXLINE)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "SMTP server not responding [%d] (expecting 250-2).", errorcode);

    /* Close socket */
    close(sockfd);

    return -1;
  }
  else if (strstr(line, "250") == NULL)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Got unexpected response from SMTP server: %s.", line);

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* OK, the response was 250, just as we expected, send RCPT TO */
  sprintf(line, "RCPT TO: <%s>\r\n", email);  
  if (writen(sockfd, line, strlen(line)) != (int) strlen(line))
  {
    netmon::log::instance()->add(netmon::_eLogError, "Error while writing to SMTP server (while sending RCPT TO).");

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* Now we expect recipient to be verified and to receive code 250 */

  if ((errorcode = readline(sockfd, line, MAXLINE)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "SMTP server not responding [%d] (expecting 250-3).", errorcode);

    /* Close socket */
    close(sockfd);

    return -1;
  }
  else if (strstr(line, "250") == NULL)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Got unexpected response from SMTP server: %s.", line);

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* Both sender and recipient verified, say that we are ready to send data */
  sprintf(line, "DATA\r\n");  
  if (writen(sockfd, line, strlen(line)) != (int) strlen(line))
  {
    netmon::log::instance()->add(netmon::_eLogError, "Error while writing to SMTP server (while sending command DATA).");

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* Now we expect permission to start sending data, code 354 */
  
  if ((errorcode = readline(sockfd, line, MAXLINE)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "SMTP server not responding [%d] (expecting 354).", errorcode);

    /* Close socket */
    close(sockfd);

    return -1;
  }
  else if (strstr(line, "354") == NULL)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Got unexpected response from SMTP server: %s.", line);

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* Send email headers */
  sprintf(line, "From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n", emailfrom, email, subject);
  if (writen(sockfd, line, strlen(line)) != (int) strlen(line))
  {
    netmon::log::instance()->add(netmon::_eLogError, "Error while writing to SMTP server (while sending message text).");

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* Send message text */
  if (writen(sockfd, message, strlen(message)) != (int) strlen(message))
  {
    netmon::log::instance()->add(netmon::_eLogError, "Error while writing to SMTP server (while sending message text).");

    /* Close socket */
    close(sockfd);

    return -1;
  }
  sprintf(line, "\r\n.\r\n");  
  if (writen(sockfd, line, strlen(line)) != (int) strlen(line))
  {
    netmon::log::instance()->add(netmon::_eLogError, "Error while writing to SMTP server (while sending message text).");

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* If message accepted for delivery, we should get code 250 */

  if ((errorcode = readline(sockfd, line, MAXLINE)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "SMTP server not responding [%d] (expecting 250-4).", errorcode);

    /* Close socket */
    close(sockfd);

    return -1;
  }
  else if (strstr(line, "250") == NULL)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Got unexpected response from SMTP server: %s.", line);

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* Disconnect */
  sprintf(line, "QUIT\r\n");  
  if (writen(sockfd, line, strlen(line)) != (int) strlen(line))
  {
    netmon::log::instance()->add(netmon::_eLogError, "Error while writing to SMTP server (while sending QUIT).");

    /* Close socket */
    close(sockfd);

    return -1;
  }
        
 /* Get final response, code 221 */
  if ((errorcode = readline(sockfd, line, MAXLINE)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "SMTP server not responding [%d] (expecting 221).", errorcode);

    /* Close socket */
    close(sockfd);

    return -1;
  }
  else if (strstr(line, "221") == NULL)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Got unexpected response from SMTP server: %s.", line);

    /* Close socket */
    close(sockfd);

    return -1;
  }

  /* Close socket */

  close(sockfd);

  /* Everything went fine if we came to this point, return 0 */

  printf ("Done.\n");
  return 0;
}
 
