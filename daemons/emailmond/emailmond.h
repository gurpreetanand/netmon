#ifndef __EMAIL_DAEMON__
#define __EMAIL_DAEMON__

#include "daemon.h"

#include <dbus-c++/dbus.h>
#include <signal.h>

namespace netmon
{
  class email_sender;

  class emailmon_daemon : public daemon
  {
    public:
      emailmon_daemon (int, char **);
      ~emailmon_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "emailmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/emailmond";
      }

      int installTimer ();
      void sendSetupNotification ();

      static void onTimer (int);

      enum { timer_interval = 60 };

      email_sender *m_eSender;
      int m_notificationSent;
      static sig_atomic_t m_timerFired;
      static DBus::BusDispatcher m_dispatcher;
  };
}

#endif //__EMAIL_DAEMON__
