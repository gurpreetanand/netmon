#include "dbus.h"
#include "emailsender.h"
#include <stdio.h>

namespace netmon
{
  static const char *SERVER_PATH = "/ca/netmon/DBus/emailmond";

  dbus_emailmond_server::dbus_emailmond_server (DBus::Connection &conn, email_sender *sender)
    : DBus::ObjectAdaptor(conn, SERVER_PATH), m_eSender (sender)
  {
  }

  void dbus_emailmond_server::alert ()
  {
    printf ("alert() called\n");
    m_eSender->run();
  }
}
