#include "srvmonitor.h"
#include "db.h"
#include "daemon.h"
#include "log.h"
#include "alerts.h"
#include "srvalertmgr.h"

#include <errno.h>
#include <cstdlib>
#include <sstream>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <exception>
#include <boost/lexical_cast.hpp>
#include <algorithm>

#define POLL_INTERVAL 5
#define SEC_FETCHSERVICES 1*60 // Fetch services at this second interval
#define MAX_SIZE 1024*1024*1024  // That's one meg to you and I

namespace netmon
{
  srvmonitor::srvmonitor (db *mydb, daemon *d)
    : m_db (mydb), m_daemon (d)
  {
    init();
  }

  srvmonitor::~srvmonitor ()
  {
    delete m_alertManager;
  }

  void srvmonitor::run ()
  {
    srvchecker *checker = new srvchecker;
    signal_handler::instance()->register_handler_ev (SIGTERM, &m_sigHandler);

    daemon_msg msg;
    servicelist services;
    timeval tstart;
    timeval tend;
    int sec = POLL_INTERVAL;
    int usec = 0;
    int cnt = 0;

    msg.m_type = 1;
    strcpy (msg.m_data, "svrmond");

    while (!m_sigHandler.isTermSignaled())
    {
      ++cnt;
      if (cnt > 12 || cnt == 1)
      {
        m_queue.send (msg, sizeof (msg), IPC_NOWAIT);
        cnt = 0;
      }

      gettimeofday (&tstart, 0);
      getServices (services);
      if (services.size() == 0 && checker->size() == 0)
      {
        printf ("nothing to check..zzzzz..zzzzzzz\n");
        sleep (POLL_INTERVAL);
        continue;
      }
      try
      {
        checker->addServices (services);
        checker->check (sec, usec);
        updateDB (checker);
        services.clear();
        gettimeofday (&tend, 0);
        long ts = (tend.tv_sec - tstart.tv_sec) * 1000000 + tend.tv_usec - tstart.tv_usec;
        ts -= sec * 1000000 + usec;
        if (ts > 2500000)
        {
          sec = POLL_INTERVAL;
          usec = 0;
        }
        else
        {
          ts = POLL_INTERVAL * 1000000 - ts;
          sec = (ts / 1000000);
          usec = ts % 1000000;
        }
        printf ("timeout is %d.%d\n", sec, usec);
      }
      catch (int e)
      {
        errno = e;
        perror ("srvmond");
      }
    }
  }

  void srvmonitor::getServices (servicelist &services)
  {
    unsigned long ts = ::time(0);
    static std::ostringstream sql;
    static std::ostringstream sql2;

    sql.str("");
    sql <<
    "SELECT SRV_ID, IP, PORT, PROTOCOL, STATUS, TIMEOUT*60, LOG_TIMEOUT, "
    "NAME, INTERVAL FROM SERVERS WHERE (TIMESTAMP + INTERVAL) <= "
        << (int) time(NULL) << " AND PENDING != 'Y'";

    PGresult *res = m_db->execQuery (sql.str().c_str());
printf ("Got %d services to check\n", PQntuples (res));
    for (int i = 0; i < PQntuples (res); ++i)
    {
      servicedata d;
      d.m_id = atoi (PQgetvalue (res, i, 0));
      d.m_ip = std::string (PQgetvalue (res, i, 1));

      if (strcmp (PQgetvalue (res, i, 3), "TCP") == 0)
        d.m_port = atoi (PQgetvalue (res, i, 2));
      if (strcmp (PQgetvalue (res, i, 4), "DOWN") == 0)
        d.m_up = false;

      d.m_timeout = atoi (PQgetvalue (res, i, 5));
      if (d.m_timeout > 5)
        d.m_timeout -= 5;
      d.m_logtimeout = atoi (PQgetvalue (res, i, 6));
      d.m_serverName = std::string (PQgetvalue (res, i, 7));
      d.m_interval = atoi (PQgetvalue (res, i, 8));
      d.m_timestamp = ts;

      services.push_back (d);

      sql2.str("");
      sql2 << "UPDATE SERVERS SET PENDING = 'Y' WHERE SRV_ID = " << d.m_id;
      m_db->execSQL (sql2.str().c_str());
    }

    PQclear (res);
  }

  void srvmonitor::updateDB (srvchecker *checker)
  {
    static std::ostringstream sql;
    srvchecker::sockmap::iterator it;

    m_alertManager->reload();

    for (it = checker->m_socks.begin(); it != checker->m_socks.end(); ++it)
    {
      it->second->m_timeout -= 5;
printf ("timeout val: %d\n", it->second->m_timeout);
      if (it->second->m_isok == false && it->second->m_timeout >= 0)
        continue;

      sql.str("");

      try
      {
        if (it->second->m_isok)
        {
          sql << "UPDATE SERVERS SET PENDING = 'N', STATUS = 'UP', TIMESTAMP = "
              << it->second->m_timestamp << ", LATENCY = "
              << it->second->m_latency << ", MESSAGE = ' ' WHERE SRV_ID = "
              << it->second->m_id;

          m_db->execSQL (sql.str().c_str());

          if (it->second->m_logtimeout == 0 || (it->second->m_logtimeout > 0 && ((unsigned)(it->second->m_logtimeout * 1000)) < it->second->m_latency))
          {
            sql.str("");
            sql <<
            "INSERT INTO SERVER_LOG (SRV_ID, TIMESTAMP, STATUS, LATENCY, "
            "INTERVAL, LOG_TIMEOUT) VALUES ("
                << it->second->m_id << ", "
                << it->second->m_timestamp << ", 'UP', "
                << it->second->m_latency << ", "
                << it->second->m_interval << ", "
                << it->second->m_logtimeout << ")";

            m_db->execSQL (sql.str().c_str());
          }
        }
        else
        {
          std::string errmsg;
          if (it->second->m_err == 0)
            errmsg = std::string ("Timed out");
          else
            errmsg = strerror (it->second->m_err);

          sql << "UPDATE SERVERS SET PENDING = 'N', STATUS = 'DOWN', TIMESTAMP = "
              << it->second->m_timestamp << ", LATENCY = NULL, MESSAGE = '" << errmsg << "' WHERE SRV_ID = "
              << it->second->m_id;

          m_db->execSQL (sql.str().c_str());

          if (it->second->m_logtimeout >= 0) // are we going to log this?
          {
            sql.str("");
            sql << "INSERT INTO SERVER_LOG (SRV_ID, TIMESTAMP, STATUS, MESSAGE, "
                << "INTERVAL, LOG_TIMEOUT) VALUES( "
                << it->second->m_id << ", "
                << it->second->m_timestamp << ", 'DOWN', '"
                << errmsg << "', "
                << it->second->m_interval << ", "
                << it->second->m_logtimeout << ")";

            m_db->execSQL (sql.str().c_str());
          }
        }
      }
      catch (netmon::db_exception &e)
      {
        netmon::log::instance()->add(netmon::_eLogError, e.what());
        continue;
      }

      if (it->second->m_up && (it->second->m_isok == false)) // was up, now down
        checkServiceAlert (it->second, false);

      if ((it->second->m_up == false) && it->second->m_isok) // was down, now up
        checkServiceAlert (it->second, true);

      if (it->second->m_up && it->second->m_isok) // check latency
        checkLatencyAlert (it->second);
    }
    checker->purge();
  }

  void srvmonitor::checkServiceAlert (srvchecker::servicestatus *s, bool up)
  {
    static std::ostringstream st;
    values_map vmap;

    vmap[netmon::alerthandler::eHostName] = keyword_value(s->m_serverName);
    vmap[netmon::alerthandler::eHostIP] = keyword_value(s->m_ip);

    if (s->m_port == -1)
    {
      st.str("");
      st << s->m_port << " (ICMP)";
      vmap[netmon::alerthandler::ePort] = keyword_value(st.str());
    }
    else
      vmap[netmon::alerthandler::ePort] = keyword_value(s->m_port);

    if (!up)
    {
      if (s->m_err != 0)
        vmap[netmon::alerthandler::eErrMsg] = keyword_value(strerror (s->m_err));
      else
        vmap[netmon::alerthandler::eErrMsg] = keyword_value("Timed out");
    }

    if (s->m_port == -1)
      m_alertManager->getPingAlerts (s->m_id, up, vmap);
    else
      m_alertManager->getServiceAlerts (s->m_id, up, vmap);
  }

  void srvmonitor::checkLatencyAlert (srvchecker::servicestatus *s)
  {
    static std::ostringstream st;
    values_map vmap;

    vmap[netmon::alerthandler::eHostName] = keyword_value(s->m_serverName);
    vmap[netmon::alerthandler::eHostIP] = keyword_value(s->m_ip);

    if (s->m_port == -1)
    {
      st.str("");
      st << s->m_port << " (ICMP)";
      vmap[netmon::alerthandler::ePort] = keyword_value(st.str());
    }
    else
      vmap[netmon::alerthandler::ePort] = keyword_value(s->m_port);

    vmap[netmon::alerthandler::eLatency] = keyword_value(formatLatency (s->m_latency));

    m_alertManager->getAlerts (s->m_id, s->m_latency / 1000, vmap);
  }

  void srvmonitor::init ()
  {
    m_db->execSQL ("UPDATE SERVERS SET PENDING = 'N', TIMESTAMP = 0");
    m_alertManager = new srv_alert_manager (m_db);
    m_alertManager->init();
    m_queue.create (queue_id, 0600);
  }

  const char *srvmonitor::formatLatency (unsigned int latency)
  {
    static char s[16];
    snprintf (s, 15, "%.3f ms", (double) latency / 1000);
    return s;
  }
}
