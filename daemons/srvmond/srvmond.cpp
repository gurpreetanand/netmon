#include "srvmond.h"
#include "srvmonitor.h"
#include "db.h"
#include "log.h"

namespace netmon
{
  srv_daemon::srv_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  srv_daemon::~srv_daemon ()
  {
    delete m_srvMonitor;
  }

  void srv_daemon::init ()
  {
    daemon::init();
    m_srvMonitor = new srvmonitor (m_db, this);
  }

  int srv_daemon::run ()
  {
    m_srvMonitor->run();
    return 0;
  }
}
