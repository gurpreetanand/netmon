#ifndef __SRV_DAEMON__
#define __SRV_DAEMON__

#include "daemon.h"

namespace netmon
{
  class srvmonitor;

  class srv_daemon : public daemon
  {
    public:
      srv_daemon (int, char **);
      ~srv_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "srvmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/srvmond";
      }

      srvmonitor *m_srvMonitor;
  };
}

#endif // __SRV_DAEMON__
