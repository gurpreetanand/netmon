#include "srvalertmgr.h"
#include "alerts.h"
#include "netmon.h"

namespace netmon
{
  srv_alert_manager::srv_alert_manager (db *mydb)
    : service_alert_manager (mydb)
  {
  }

  unsigned int srv_alert_manager::getPingAlerts (unsigned int id, bool up, values_map &vmap)
  {
    return getAlerts (id, up, ePingAlert, vmap);
  }

  unsigned int srv_alert_manager::getServiceAlerts (unsigned int id, bool up, values_map &vmap)
  {
    return getAlerts (id, up, eServiceAlert, vmap);
  }

  unsigned int srv_alert_manager::getAlerts (unsigned int id, bool up, service_type type, values_map &vmap)
  {
    alert_map::iterator i = m_alerts.find (id);
    if (i == m_alerts.end())
      return 0;

    alert_map::iterator end = m_alerts.upper_bound (id);
    unsigned int ret = 0;

    for (; i != end; ++i)
    {
      if (!isAlertValid (i->second))
        continue;

      alerthandler *h = createAlertHandler (i, up, type);
      getReferenceData (id, h->getMap());
      dispatchAlert (h, vmap);
      ++ret;
    }
    return ret;
  }

  alerthandler *srv_alert_manager::createAlertHandler (const alert_map::iterator &i, bool up, service_type type)
  {
    alerthandler *h = createAlertHandler (i->second);
    h->setRecovery (up);

    if (up)
    {
      if (type == ePingAlert)
      {
        h->setSubject (m_gotPingSubject);
        h->setTemplate (m_gotPingTemplate);
      }
      else
      {
        h->setSubject (m_upSubject);
        h->setTemplate (m_upTemplate);
      }
    }
    else
    {
      if (type == ePingAlert)
      {
        h->setSubject (m_noPingSubject);
        h->setTemplate (m_noPingTemplate);
      }
      else
      {
        h->setSubject (m_downSubject);
        h->setTemplate (m_downTemplate);
      }
    }
    return h;
  }

  void srv_alert_manager::loadStaticData ()
  {
    service_alert_manager::loadStaticData ();
    std::string tmpl;
    getNoPingName (tmpl);
    alert_manager::loadStaticData (tmpl, m_noPingTemplate, m_noPingSubject);
    getGotPingName (tmpl);
    alert_manager::loadStaticData (tmpl, m_gotPingTemplate, m_gotPingSubject);
  }
}
