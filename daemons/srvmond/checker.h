#ifndef __CHECKER__
#define __CHECKER__

#include <list>
#include <map>
#include <string>
#include <sys/types.h>
#include <event.h>
#include <netinet/in.h>

namespace netmon
{
  struct servicedata
  {
    servicedata ()
      : m_port (-1), m_up (true)
    {}

    servicedata (const servicedata &d)
    {
      m_id = d.m_id;
      m_port = d.m_port;
      m_ip = d.m_ip;
      m_serverName = d.m_serverName;
      m_up = d.m_up;
      m_timeout = d.m_timeout;
      m_logtimeout = d.m_logtimeout;
      m_timestamp = d.m_timestamp;
      m_interval = d.m_interval;
    }

    servicedata &operator= (const servicedata &d)
    {
      m_id = d.m_id;
      m_port = d.m_port;
      m_ip = d.m_ip;
      m_serverName = d.m_serverName;
      m_up = d.m_up;
      m_timeout = d.m_timeout;
      m_logtimeout = d.m_logtimeout;
      m_timestamp = d.m_timestamp;
      m_interval = d.m_interval;
      return *this;
    }

    unsigned int m_id;
    int m_port;
    std::string m_ip;
    std::string m_serverName;
    bool m_up;
    short m_timeout;
    int m_logtimeout;
    unsigned int m_timestamp;
    int m_interval;
  };

  typedef std::list <servicedata> servicelist;

  class srvchecker
  {
    friend class srvmonitor;
    public:
      srvchecker ();
      srvchecker (const servicelist &);
      virtual ~srvchecker ();

      void addServices (const servicelist &);
      void check (int = 5, int = 0);
      size_t size () const
      {
        return m_socks.size();
      }
      virtual void store ();

    protected:
      class servicestatus : public servicedata
      {
        private:
          servicestatus () {}

        public:
          servicestatus (const servicedata &);
          unsigned int m_latency;
          int m_err;
          bool m_isok;
          sockaddr_in m_srvaddr;
          event m_event;
          timeval m_tvstart;
      };
      typedef std::map<int, servicestatus*> sockmap;

      void init ();
      void installSigMask ();
      void createSocket (const servicedata &);
      void closeSocket (const sockmap::iterator &);
      void sendRequests ();
      void sendICMPRequest (int, sockaddr *, int);
      void poll (timeval *);
      static void handleICMP (int, short, void *);
      static void handleConnect (int, short, void *);
      void handleICMP (sockmap::iterator &);
      void handleConnect (sockmap::iterator &);
      void purge ();

      static void installSockFlags (int);
      static int checksum (u_short *, int);
      static bool ICMPOK (char *, int, sockaddr_in *);

      sockmap m_socks;
      unsigned int m_sequence;
      int m_ICMPprotocol;
  };
}

#endif // __CHECKER__
