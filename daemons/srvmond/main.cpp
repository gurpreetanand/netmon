#include <errno.h>
#include <sys/types.h>

#include <string>
#include <cstdio>
#include <cstring>

#include "exception.h"
#include "srvmond.h"
#include "log.h"

int main (int argc, char **argv)
{
  try
  {
    netmon::srv_daemon *daemon = new netmon::srv_daemon (argc, argv);
    daemon->init();
    daemon->start();
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
  }
  catch (int e)
  {
    errno = e;
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, strerror (e));
  }
  return 0;
}
