#include "netmonsniffer.h"

#include "connmanager.h"
#include "pluginmanager.h"
#include "executor.h"
#include "timer.h"

#include <unistd.h>
#include <dumbnet.h>
#include "pcap-int.h"

#define OFFSET_8021Q 4
#define NUM_EXECUTORS 4

namespace netmon
{
  netmon_sniffer::netmon_sniffer (db *mydb, plugin_manager *p, const char *dev, const char *f)
    : plugin_sniffer (p, dev, f)
  {
    init();
    m_queue = new task_queue;
    for (int i = 1; i <= NUM_EXECUTORS; i++)
    {
      m_executors.push_back(new netmon::executor(m_queue, p, i));
    }
    m_connMngr = new connection_manager;
  }

  netmon_sniffer::netmon_sniffer (db *mydb, plugin_manager *p)
    : plugin_sniffer (p)
  {
    init();
    m_queue = new task_queue;
    for (int i = 0; i <= NUM_EXECUTORS; i++)
    {
      m_executors.push_back(new netmon::executor(m_queue, p, i));
    }
    m_connMngr = new connection_manager;
  }

  netmon_sniffer::~netmon_sniffer ()
  {
    for (std::list<netmon::executor*>::iterator it = m_executors.begin(); 
         it != m_executors.end(); ++it)
    {
      delete (*it);
    }

    delete m_queue;
    delete m_connMngr;
  }

  void netmon_sniffer::run ()
  {
    signal_handler::instance()->register_handler_ev (SIGTERM, &m_sigHandler);

    for (std::list<netmon::executor*>::iterator it = m_executors.begin();
         it != m_executors.end(); ++it)
    {
      (*it)->start();
    }

    while (!m_sigHandler.isTermSignaled())
      event_loop (EVLOOP_ONCE);

    for (std::list<netmon::executor*>::iterator it = m_executors.begin();
         it != m_executors.end(); ++it)
    {
      (*it)->stop();
    }
  }

  void netmon_sniffer::init ()
  {
    evtimer_set (&m_timerEvent, onTimer, this);
    timeval tv = { 600, 0 };
    evtimer_add (&m_timerEvent, &tv);
  }
  
  void netmon_sniffer::onTimer (int, short, void *me)
  {
    netmon_sniffer *ns = reinterpret_cast<netmon_sniffer *> (me);
    ns->onTimer();
  }

  void netmon_sniffer::onTimer ()
  {
    timeval tv = { 600, 0 };
    evtimer_add (&m_timerEvent, &tv);
    m_connMngr->removeIddleConnections();
  }

  void netmon_sniffer::onRead (int fd)
  {
    dev_map::iterator i = m_fdmap.find (fd);
    if (i == m_fdmap.end())
      return;

    pcap_pkthdr* ph;
    const u_char* packet;

    if (pcap_next_ex (i->second->m_pcap, &ph, &packet) != 0)
    {
      uint16_t size;
      for (unsigned int j = 0; j < m_pluginManager->getSize(); ++j)
      {
        plugin_data *p = m_pluginManager->getPlugin (j);
        size = ph->caplen;
        if (!(i->second->m_devID & p->m_devices))
          continue;

        uint8_t *data = getAdjustedData (p, packet, size);
        if (data != 0)
	    p->m_plugin->onData(data, size, i->second->m_devID, &m_conv);
      }
    }
  }


  void netmon_sniffer::onTimeout (timer_wrapper *wrap)
  {
    printf("NS::onTimeout\n");
    plugin_sniffer::onTimeout (wrap);
    m_queue->push (wrap->m_timer);
  }

  uint8_t *netmon_sniffer::getAdjustedData (plugin_data *p, const u_char *packet, uint16_t &size)
  {
    eth_hdr *eth;
    ip_hdr *iph;

    uint16_t s = size;
    eth = (eth_hdr *) (packet);

    if (ntohs (eth->eth_type) == ETH_TYPE_8021Q) // VLAN
    {
      eth = (eth_hdr *) (packet + OFFSET_8021Q);
      s -= OFFSET_8021Q;
    }

    switch (p->m_protocol)
    {
      case _eETH:
        size = s;
        return (uint8_t *) eth;
      case _eIP:
        if (ntohs (eth->eth_type) == ETH_TYPE_IP)
        {
          size = s - sizeof (eth_hdr);
          return (uint8_t *) (eth) + sizeof (eth_hdr);
        }
        return 0;
      case _eTCP:
      case _eUDP:
        if (ntohs (eth->eth_type) != ETH_TYPE_IP)
          return 0;
        iph = (ip_hdr *) ((uint8_t *) (eth) + sizeof (eth_hdr));
        return wantsPacket (p, s - sizeof (eth_hdr), iph, size);
      default:
        return 0;
    }
    return 0;
  }

  uint8_t *netmon_sniffer::wantsPacket (plugin_data *p, uint16_t s, ip_hdr *iph, uint16_t &size)
  {
    uint8_t proto = iph->ip_p;
    uint8_t *ptr = (uint8_t *) (iph) + iph->ip_hl * 4;
    uint8_t flags = 0;

    uint16_t srcPort;
    uint16_t dstPort;

    m_conv.m_src = iph->ip_src;
    m_conv.m_dst = iph->ip_dst;
    s -= iph->ip_hl * 4;

    if ((proto == IP_PROTO_TCP && (p->m_protocol == _eTCP)) ||
        (proto == IP_PROTO_UDP && (p->m_protocol == _eUDP)))
    {
      if (proto == IP_PROTO_TCP)
      {
        tcp_hdr *t = (tcp_hdr *) ptr;
        flags = t->th_flags;
        srcPort = ntohs (t->th_sport);
        dstPort = ntohs (t->th_dport);
        m_conv.m_srcPort = t->th_sport;
        m_conv.m_dstPort = t->th_dport;
	/*
	in_addr one, two;
	one.s_addr = m_conv.m_src;
	two.s_addr = m_conv.m_dst;
	std::cout << "Source IP: " << inet_ntoa(one) << "(" << iph->ip_src << ")";
	std::cout << " Dest IP: " << inet_ntoa(two) << "(" << iph->ip_dst << ")";
	std::cout << " Sport : " << t->th_sport << " Dport: " << t->th_dport << " Seq #: " << t->th_seq 
	<< " ACK : " << t->th_ack << std::endl;
	*/
        if (!p->m_plugin->wantsHeaders())
        {
          s -= t->th_off * 4;
          ptr += t->th_off * 4;
        }
      }
      else
      {
        udp_hdr *u = (udp_hdr *) ptr;
        srcPort = ntohs (u->uh_sport);
        dstPort = ntohs (u->uh_dport);
        m_conv.m_srcPort = u->uh_sport;
        m_conv.m_dstPort = u->uh_dport;
        if (!p->m_plugin->wantsHeaders())
        {
          s -= sizeof (udp_hdr);
          ptr += sizeof (udp_hdr);
        }
      }
      if (!p->m_ports.empty() && (
          p->m_ports.find (srcPort) != p->m_ports.end() ||
          p->m_ports.find (dstPort) != p->m_ports.end()))
      {
//printf ("src: %d dst: %d\n", srcPort, dstPort);
        size = s;
        return ptr;
      }
      if (proto == IP_PROTO_TCP)
      {
        connection_manager::status st = m_connMngr->getStatus (iph->ip_src, iph->ip_dst, srcPort, dstPort, flags);
        if (st == connection_manager::eTracking)
        {
	  int pid = m_connMngr->getPluginID (iph->ip_src, iph->ip_dst, srcPort, dstPort);
          if (pid == p->m_dbid || pid == -1) {
            //printf("PID for this packet is [%d]\n", pid);
            size = s;
            return ptr;
	  } else {
	    //printf("Dropping Packet with PID [%d] for DBID [%d]\n", pid, p->m_dbid);
	    return 0;
	  }
        }
        if (st == connection_manager::eReady)
        {
          if (p->m_hasPattern && regnexec (&(p->m_re), (const char *) ptr, s, 0, 0, 0) == 0)
          {
            //printf ("pattern matched!\n");
            m_connMngr->setPluginID (iph->ip_src, iph->ip_dst, srcPort, dstPort, p->m_dbid);
            size = s;
            return ptr;
          }
        }
      }
    }
    return 0;
  }
}
