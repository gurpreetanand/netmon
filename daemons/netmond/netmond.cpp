#include "netmond.h"
#include "pluginmanager.h"
#include "netmonsniffer.h"
#include "db.h"
#include "log.h"

#include <string>
#include <sstream>
#include <getopt.h>

#include <iostream>

namespace netmon
{
  netmon_daemon::netmon_daemon (int argc, char **argv)
    : daemon (argc, argv), m_path ("/usr/local/lib")
  {
  }

  netmon_daemon::~netmon_daemon ()
  {
    delete m_sniffer;
  }

  void netmon_daemon::init ()
  {
    std::cout<<"calling daemon_init from netmon_daemon_init"<<std::endl;
    daemon::init();
    std::cout<<"after calling daemon_init"<<std::endl;
    std::list<pdata *> plugins;
    getPlugins (plugins);

    plugin_manager *p = new plugin_manager (m_db);

    for (std::list<pdata *>::iterator i = plugins.begin(); i != plugins.end(); ++i)
    {
      std::string pl_name = m_path + "/" + (*i)->m_name + ".so";
      printf ("%s\n", pl_name.c_str());
      p->createPlugin (pl_name, (*i)->m_id, (*i)->m_ports, (*i)->m_pattern);
      delete *i;
    }

    m_sniffer = new netmon_sniffer (m_db, p);
  }

  void netmon_daemon::getPlugins (std::list<pdata *> &list)
  {
    PGresult *res = m_db->execQuery (
    "select a.name, a.id, a.ports, a.pattern, a.start_ifaces from plugins a, daemons b where daemon_id "
    "= b.id and b.name = 'netmond'");

    for (int i = 0; i < PQntuples (res); ++i)
    {
      // We need to reset running_ifaces even if the plugin is not to be loaded
      if (PQgetisnull (res, i, 4) || strlen(PQgetvalue (res, i, 4)) == 0)
      {
        std::ostringstream sql;
	sql.str("");
        printf("Plugin %s not starting, clearing running flags\n", PQgetvalue(res,i,0));
	sql << "UPDATE plugins SET running_ifaces='' WHERE id=" << atoi(PQgetvalue(res,i,1));
	try {
	  m_db->execSQL(sql.str().c_str());
	} catch (db_exception& e) {
          std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
          netmon::log::instance()->add(netmon::_eLogFatal, e.what());
	}
	continue;
      }
      pdata *p = new pdata;
      p->m_name = std::string (PQgetvalue (res, i, 0));
      p->m_id = atoi (PQgetvalue (res, i, 1));
      p->m_ports = std::string (PQgetvalue (res, i, 2));
      p->m_pattern = std::string (PQgetvalue (res, i, 3));
      list.push_back (p);
    }

    PQclear (res);
  }

  int netmon_daemon::run ()
  {
    m_sniffer->run();
    return 0;
  }

  void netmon_daemon::usage ()
  {
    printf ("Usage: %s [-h] [-f] [-p]\n -f: run in foreground\n"
            " -h: gives this help\n -p <path>: path to plugins\n", getDaemonName());
  }

  void netmon_daemon::onOption (char c)
  {
    daemon::onOption (c);
    if (c == 'p')
      m_path = std::string (optarg);
  }

  const char *netmon_daemon::getShortOptions ()
  {
    m_opts = daemon::getShortOptions();
    m_opts += "p:";
    return m_opts.c_str();
  }
}
