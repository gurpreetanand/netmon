#ifndef __UTIL_H__
#define __UTIL_H__

#include <string>

namespace netmon
{
  enum
  {
    _eETH0 = 1,
    _eETH1 = 2,
    _eETH2 = 4,
    _eETH3 = 8,
    _eETH4 = 16,
    _eETH5 = 32,
    _eETH6 = 64,
    _eETH7 = 128,
    _eETHMax = 256
  };

  std::string inline devToString (unsigned int dev)
  {
    switch (dev)
    {
      case _eETH0:
        return "eth0";
      case _eETH1:
        return "eth1";
      case _eETH2:
        return "eth2";
      case _eETH3:
        return "eth3";
      case _eETH4:
        return "eth4";
      case _eETH5:
        return "eth5";
      case _eETH6:
        return "eth6";
      case _eETH7:
        return "eth7";
      default:
        return "";
    }
  }

  unsigned int inline stringToDev (const std::string &dev)
  {
    if (dev == "eth0")
      return _eETH0;
    if (dev == "eth1")
      return _eETH1;
    if (dev == "eth2")
      return _eETH2;
    if (dev == "eth3")
      return _eETH3;
    if (dev == "eth4")
      return _eETH4;
    if (dev == "eth5")
      return _eETH5;
    if (dev == "eth6")
      return _eETH6;
    if (dev == "eth7")
      return _eETH7;
    return 0;
  }
}

#endif // __UTIL_H__
