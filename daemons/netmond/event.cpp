#include <pcap.h>
#include <sys/time.h>
#include <event.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>

static char b[2048];

void doRead (int fd, short event, void *)
{
  printf ("event on fd: %d ", fd);
  int i = read (fd, (void *) b, 2047);
  printf ("(read %d bytes)\n", i);
}

int main ()
{
  event e;
  pcap_t *p;
  int fd;
  char err[PCAP_ERRBUF_SIZE];

  bpf_program fp;
  bpf_u_int32 maskp;
  bpf_u_int32 netp;
  char filter_app[] = "icmp";

  if (pcap_lookupnet ("eth0", &netp, &maskp, err) == -1)
  {
    printf ("error: %s\n", err);
    exit (-1);
  }

  p = pcap_open_live ("eth0", 1500, 1, 500, err);
  if (p == 0)
  {
    printf ("error: %s\n", err);
    exit (-1);
  }

  if (pcap_compile (p, &fp, filter_app, 0, netp) == -1)
  {
    printf ("error: cannot compile filter\n");
    exit (-1);
  }

  if (pcap_setfilter (p, &fp) == -1)
  {
    printf ("error: cannot set filter\n");
    exit (-1);
  }

  pcap_freecode (&fp);

  fd = pcap_get_selectable_fd (p);

  event_init ();
  event_set (&e, fd, EV_READ | EV_PERSIST, doRead, 0);
  event_add (&e, 0);
  event_dispatch ();
}
