#ifndef __CONDITION__
#define __CONDITION__

#include <limits.h>
#include <sys/time.h>

#include "mutex.h"

namespace netmon
{
  class condition
  {
    public:
      condition (mutex *mutex)
        : m_mutex (mutex)
      {
        pthread_cond_init (&m_cond, NULL);
      }

      ~condition ()
      {
        pthread_cond_destroy (&m_cond);
      }

      void wait (unsigned long time = ULONG_MAX)
      {
        if (time != ULONG_MAX)
        {
          timeval tv;
          gettimeofday (&tv, 0);

          timespec timeout;

          timeout.tv_nsec = (tv.tv_usec * 1000) + (time % 1000) * 1000;
          timeout.tv_sec = tv.tv_sec + (time / 1000) + (timeout.tv_nsec / 1000000000);
          timeout.tv_nsec %= 1000000000;

          pthread_cond_timedwait (&m_cond, &(m_mutex->m_mutex), &timeout);
        }
        else
          pthread_cond_wait (&m_cond, &(m_mutex->m_mutex));
      }

      void wake ()
      {
        pthread_cond_signal (&m_cond);
      }

      void wake_all ()
      {
        pthread_cond_broadcast (&m_cond);
      }

    private:
      pthread_cond_t m_cond;
      mutex *m_mutex;
  };
}

#endif //__CONDITION__
