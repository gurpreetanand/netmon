#ifndef __PLUGIN_MANAGER__
#define __PLUGIN_MANAGER__

#include <set>
#include <string>
#include <vector>

#include <tre/regex.h>

#include "plugin.h"
#include "exception.h"

namespace netmon
{
  class db;

  struct plugin_data
  {
    plugin_data ()
      : m_hasPattern (false) {}

    plugin *m_plugin;
    destroy_t *m_destroyf;
    unsigned int m_devices;
    unsigned int m_protocol;
    int m_dbid;
    bool m_hasPattern;
    std::vector<std::string> m_runningDevices;
    std::set<unsigned short> m_ports;
    regex_t m_re;
  };

  class plugin_exception : public netmon_exception
  {
    public:
      plugin_exception (const std::string &e)
        : netmon_exception (e)
      {}
  };

  class plugin_manager
  {
    public:
      plugin_manager (db *);
      ~plugin_manager ();

      void createPlugin (const std::string &, int, const std::string &, const std::string &);
      plugin_data *getPlugin (unsigned int) const;
      void insertRunningDevices (unsigned int);

      unsigned int getSize () const
      {
        return m_cnt;
      }

    protected:
      void initPlugin (plugin_data *);

      enum { max_plugins = 7 };
      plugin_data *m_plugins[max_plugins];
      db *m_db;
      unsigned int m_cnt;
  };
}

#endif // __PLUGIN_MANAGER__
