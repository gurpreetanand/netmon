#include <stdlib.h>
#include <sys/time.h>
#include <event.h>

#include <string>

#include "pluginsniffer.h"
#include "pluginmanager.h"
#include "plugin.h"
#include "timer.h"
#include "util.h"

namespace netmon
{
  plugin_sniffer::plugin_sniffer (plugin_manager *pm, const char *device, const char *filter)
    : base_sniffer (device, filter), m_pluginManager (pm)
  {
    init();
  }

  plugin_sniffer::plugin_sniffer (plugin_manager *pm)
    : base_sniffer (), m_pluginManager (pm)
  {
    init();
  }

  plugin_sniffer::~plugin_sniffer ()
  {
    for (event_list::iterator i = m_timerEvents.begin(); i != m_timerEvents.end(); ++i)
      delete *i;
    m_timerEvents.clear();
    for (std::list<timer_wrapper *>::iterator i = m_twlist.begin(); i != m_twlist.end(); ++i)
      delete *i;
    m_twlist.clear();
  }

  void plugin_sniffer::init ()
  {
    for (unsigned int i = 0; i < m_pluginManager->getSize(); ++i)
    {
      std::vector<timer *> timers;
      plugin_data *p = m_pluginManager->getPlugin (i);
      p->m_plugin->getTimers (timers);
      createEvents (timers);
      addDevices (p);
      m_pluginManager->insertRunningDevices (i);
    }
  }

  void plugin_sniffer::addDevices (plugin_data *p)
  {
    std::set<std::string> devs;
    unsigned int devices = p->m_plugin->getWantedDevice();

    for (unsigned int i = _eETH0; i < _eETHMax; i <<= 1)
      if (devices & i)
        devs.insert (devToString (i));

    for (std::set<std::string>::iterator i = devs.begin(); i != devs.end(); ++i)
    {
      if (base_sniffer::addDevice ((*i).c_str()))
        p->m_runningDevices.push_back (*i);
    }
  }

  void plugin_sniffer::createEvents (const std::vector<timer *> &timers)
  {
    for (unsigned int i = 0; i < timers.size(); ++i)
    {
      event *e = new event;
      timer_wrapper *wrap = new timer_wrapper (this, timers[i], e);
      evtimer_set (e, onTimeout, wrap);
      timeval tv = { timers[i]->getSeconds(), 0 };
      evtimer_add (e, &tv);
      m_timerEvents.push_back (e);
      m_twlist.push_back (wrap);
    }
  }

  void plugin_sniffer::onTimeout (int, short, void *me)
  {
    #ifdef DEBUG
      printf("PS::onTimeout: %d\n", (int) ::time(0));
    #endif
    timer_wrapper *wrap = reinterpret_cast<timer_wrapper *> (me);
    wrap->m_sniffer->onTimeout (wrap);
  }

  void plugin_sniffer::onTimeout (timer_wrapper *wrap)
  {
    #ifdef DEBUG
    printf ("onTimeout() PLUGIN ID: %d, ID: %d, getSeconds: %d, %d\n", wrap->m_timer->getPluginID(), wrap->m_timer->getTimerID(), wrap->m_timer->getSeconds(), (int) ::time(0));
    #endif
    timeval tv = { wrap->m_timer->getSeconds(), 0 };
    evtimer_add (wrap->m_event, &tv);
  }
}
