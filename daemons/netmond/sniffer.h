#ifndef __SNIFFER__
#define __SNIFFER__

#include <pcap.h>
#include <stdint.h>

#include <map>
#include <set>
#include <list>
#include <string>

#include "exception.h"

class event;

namespace netmon
{
  struct device_data
  {
    device_data (const char *dev)
      : m_device (dev)
    {}
    std::string m_device;
    pcap_t *m_pcap;
    bpf_u_int32 m_mask;
    bpf_u_int32 m_net;
    uint8_t m_devID;
  };

  class sniffer_exception : public netmon_exception
  {
    public:
      sniffer_exception (const std::string &e)
        : netmon_exception (e)
      {}
  };

  class base_sniffer
  {
    public:
      base_sniffer ();
      base_sniffer (const char *, const char * = 0);
      base_sniffer (const std::list<std::string> &, const char * = 0);
      virtual ~base_sniffer ();

      bool addDevice (const char *, const char * = 0);

    protected:
      void openDevice (const char *, const char *);
      static uint8_t getDeviceID (const char *);
      void setFilter (device_data *, const char *);
      void createEvent (int);

      virtual void init ();
      virtual void onRead (int) = 0;

      static void onRead (int, short, void *);

      typedef std::map<int, device_data *> dev_map;
      dev_map m_fdmap;

      typedef std::list<event *> event_list;
      event_list m_events;

      typedef std::set<std::string> dev_set;
      dev_set m_devices;

      bool m_running;

      static char m_err[PCAP_ERRBUF_SIZE];
  };
}

#endif // __SNIFFER__
