#ifndef __NETMON_SNIFFER__
#define __NETMON_SNIFFER__

#include "pluginsniffer.h"
#include "sigtermhandler.h"
#include "executor.h"

#include "plugins/common/iphashmap.h"

#include <sys/time.h>
#include <event.h>
#include <list>

struct ip_hdr;

namespace netmon
{
  class connection_manager;
  class plugin_manager;
  class db;

  class netmon_sniffer : public plugin_sniffer
  {
    public:
      netmon_sniffer (db *, plugin_manager *, const char *, const char * = 0);
      netmon_sniffer (db *, plugin_manager *);
      ~netmon_sniffer ();

      void run ();

    protected:
      virtual void onRead (int);
      virtual void onTimeout (timer_wrapper *);
      virtual void init ();

      static void onTimer (int, short, void *);
      void onTimer ();

      uint8_t *getAdjustedData (plugin_data *, const u_char *, uint16_t &);
      uint8_t *wantsPacket (plugin_data *, uint16_t, ip_hdr *, uint16_t &);

      std::list<executor*> m_executors;
      task_queue *m_queue;
      connection_manager *m_connMngr;
      ip_port_entry m_conv;
      event m_timerEvent;
      sig_term_handler m_sigHandler;
  };
}

#endif // __NETMON_SNIFFER__
