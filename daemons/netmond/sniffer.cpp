#include "sniffer.h"
#include "log.h"
#include "util.h"

#include <sys/time.h>
#include <event.h>

namespace netmon
{
  char base_sniffer::m_err[PCAP_ERRBUF_SIZE];

  base_sniffer::base_sniffer ()
    : m_running (false)
  {
    init();
  }

  base_sniffer::base_sniffer (const char *device, const char *filter /* = 0 */)
    : m_running (false)
  {
    init();
    (void) addDevice (device, filter);
  }

  base_sniffer::base_sniffer (const std::list<std::string> &devices, const char *filter /* = 0 */)
    : m_running (false)
  {
    init();
    for (std::list<std::string>::const_iterator i = devices.begin(); i != devices.end(); ++i)
      (void) addDevice ((*i).c_str(), filter);
  }

  base_sniffer::~base_sniffer ()
  {
    for (dev_map::iterator i = m_fdmap.begin(); i != m_fdmap.end(); ++i)
      delete i->second;
    m_fdmap.clear();
    for (event_list::iterator i = m_events.begin(); i != m_events.end(); ++i)
      delete *i;
    m_events.clear();
  }

  bool base_sniffer::addDevice (const char *dev, const char *filter /* = 0 */)
  {
    if (m_devices.find (std::string (dev)) != m_devices.end())
      return true;
    try
    {
      openDevice (dev, filter);
      m_devices.insert (std::string (dev));
      return true;
    }
    catch (sniffer_exception &e)
    {
      log::instance()->add(_eLogError, e.what());
    }
    return false;
  }

  void base_sniffer::init ()
  {
    event_init();
  }

  void base_sniffer::openDevice (const char *dev, const char *filter)
  {
    device_data *d = new device_data (dev);

    if (pcap_lookupnet (dev, &(d->m_net), &(d->m_mask), m_err) == -1)
    {
      delete d;
      throw sniffer_exception (m_err);
    }

    d->m_pcap = pcap_open_live (dev, 1516, 1, 500, m_err);
    if (d->m_pcap == 0)
    {
      delete d;
      throw sniffer_exception (m_err);
    }

    int fd = pcap_get_selectable_fd (d->m_pcap);
    m_fdmap[fd] = d;

    d->m_devID = getDeviceID (dev);

    createEvent (fd);

    if (filter != 0)
      setFilter (d, filter);
  }

  void base_sniffer::setFilter (device_data *d, const char *filter)
  {
    bpf_program fp;

    if (pcap_compile (d->m_pcap, &fp, (char *) filter, 0, d->m_net) == -1)
    {
      throw sniffer_exception ("Cannot compile filter program");
    }

    if (pcap_setfilter (d->m_pcap, &fp) == -1)
    {
      throw sniffer_exception ("Cannot set filter");
    }

    pcap_freecode (&fp);
  }

  void base_sniffer::createEvent (int fd)
  {
    event *e = new event;
    event_set (e, fd, EV_READ | EV_PERSIST, onRead, this);
    event_add (e, 0);
    m_events.push_back (e);
  }

  void base_sniffer::onRead (int fd, short flags, void *me)
  {
    base_sniffer *s = reinterpret_cast<base_sniffer *> (me);
    s->onRead (fd);
  }

  uint8_t base_sniffer::getDeviceID (const char *dev)
  {
    std::string d (dev);
    return (uint8_t) stringToDev (d);
  }
}
