#include <cstdio>
#include <unistd.h>

#include "thread.h"
#include "queue.h"

using namespace netmon;

class task
{
  public:
  task (int i)
    : m_int (i)
  {}
  virtual ~task () {}
  int m_int;
};

class mytask : public task
{
  public:
  mytask (int i)
    : task (i)
  {
    m_type = 1;
  }
  int m_type;
};

class urgent_task : public task
{
  public:
    urgent_task (int i)
      : task (i)
    {}
    int urg()
    {
      return -2;
    }
};

typedef queue<task *> myq;

class t1 : public thread
{
  public:
    t1 (myq *q)
      : m_queue (q)
    {}

  protected:
    virtual void run ()
    {
      int i = 0;
      while (true)
      {
        m_queue->push (new mytask (i++));
        if (i % 33 == 0)
          m_queue->push (new urgent_task (-2));
      }
    }
    myq *m_queue;
};

class t2 : public thread
{
  public:
    t2 (myq *q)
      : m_queue (q)
    {}

  protected:
    virtual void run ()
    {
      task *t;
      while (true)
      {
        m_queue->pop (t);
        mytask *mt = dynamic_cast<mytask*> (t);
        if (mt != 0 && mt->m_int % 1000 == 0)
        {
          printf ("%d\n", t->m_int);
          sleep (1);
        }
        else
        {
          urgent_task *ut = dynamic_cast<urgent_task *> (t);
          if (ut != 0)
          {
            printf ("urgent task: %d size: %d\n", ut->urg(), m_queue->size());
          }
        }
        delete t;
      }
    }
    myq *m_queue;
};

int main()
{
  myq *q = new myq(10);
  t1 th1(q);
  t2 th2(q);
  th1.start();
  th2.start();
  sleep (1000);
}
