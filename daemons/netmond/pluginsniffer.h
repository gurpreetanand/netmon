#ifndef __PLUGIN_SNIFFER__
#define __PLUGIN_SNIFFER__

#include "sniffer.h"

#include <list>
#include <string>
#include <vector>

struct event;

namespace netmon
{
  class plugin_data;
  class plugin_sniffer;
  class plugin_manager;
  class timer;

  struct timer_wrapper
  {
    timer_wrapper (plugin_sniffer *p, timer *t, event *e)
      : m_sniffer (p), m_timer (t), m_event (e)
    {}
    plugin_sniffer *m_sniffer;
    timer *m_timer;
    event *m_event;
  };

  class plugin_sniffer : public base_sniffer
  {
    public:
      plugin_sniffer (plugin_manager *, const char *, const char * = 0);
      plugin_sniffer (plugin_manager *, const std::list<std::string> &, const char * = 0);
      plugin_sniffer (plugin_manager *);
      ~plugin_sniffer ();

    protected:
      virtual void onTimeout (timer_wrapper *);
      void createEvents (const std::vector<timer *> &);
      void addDevices (plugin_data *);

      virtual void init ();

      static void onTimeout (int, short, void *);

      plugin_manager *m_pluginManager;
      event_list m_timerEvents;
      std::list<timer_wrapper *> m_twlist;
  };
}

#endif // __PLUGIN_SNIFFER__
