#include "plugin.h"
#include "db.h"

#include <ctype.h>
#include <string>

namespace netmon
{
  unsigned int plugin::getWantedDevices (db *mydb)
  {
    unsigned int wantedDevs = 0;
    std::string sql = "select start_ifaces from plugins where name = '";
    sql += std::string (getName());
    sql += "'";

    PGresult *res = mydb->execQuery (sql.c_str());
    if (PQntuples (res) == 0)
      return 0;

    const char *p = PQgetvalue (res, 0, 0);

    std::string eth;

    do
    {
      if (isspace (*p))
        continue;
      if (*p == ',')
      {
        wantedDevs |= parseDevice (eth);
        continue;
      }
      eth.push_back (*p);
    }
    while (*++p);

    wantedDevs |= parseDevice (eth);

    PQclear (res);

    return wantedDevs;
  }

  unsigned int plugin::parseDevice (std::string &eth)
  {
    const std::string &dev = eth;
    unsigned int ret = stringToDev (dev);
    eth = "";
    return ret;
  }
}
