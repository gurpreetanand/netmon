#include <dlfcn.h>

#include <sstream>

#include "pluginmanager.h"
#include "arrayutil.h"
#include "timer.h"
#include "db.h"
#include "log.h"

namespace netmon
{
  plugin_manager::plugin_manager (db *mydb)
    : m_db (mydb), m_cnt (0)
  {
  }

  plugin_manager::~plugin_manager ()
  {
    for (unsigned int i = 0; i < m_cnt; ++i)
    {
      m_plugins[i]->m_destroyf(m_plugins[i]->m_plugin);
      delete m_plugins[i];
    }
  }

  void plugin_manager::createPlugin (const std::string &pfile, int dbid, const std::string &ports, const std::string &pattern)
  {
    void *h = dlopen (pfile.c_str(), RTLD_LAZY);
    if (h == 0)
    {
      std::string err = "Cannot open plugin: ";
      err += dlerror();
      throw plugin_exception (err);
    }

    dlerror();
    create_t *createPlugin = (create_t *) dlsym (h, "create");
    const char *err = dlerror();
    if (err != 0)
    {
      std::string err = "Cannot create plugin instance: ";
      err += dlerror();
      throw plugin_exception (err);
    }

    destroy_t *destroyPlugin = (destroy_t *) dlsym (h, "destroy");
    err = dlerror();
    if (err != 0)
    {
      std::string err = "Cannot find plugin exitpoint: ";
      err += dlerror();
      throw plugin_exception (err);
    }

    plugin_data *p = new plugin_data;
    p->m_plugin = createPlugin (m_cnt);
    p->m_destroyf = destroyPlugin;
    p->m_dbid = dbid;

    if (pattern.size() != 0)
    {
      p->m_hasPattern = true;
      if (regcomp (&(p->m_re), pattern.c_str(), REG_EXTENDED) != 0)
        throw plugin_exception ("Cannot compile RegExp ('" + pattern + "')");
    }
    if (ports.size() != 0)
    {
      util::parseArray (ports, p->m_ports);
    }

    initPlugin (p);
    m_plugins[m_cnt] = p;
    ++m_cnt;
  }

  plugin_data *plugin_manager::getPlugin (unsigned int index) const
  {
    if (index >= m_cnt)
      throw plugin_exception ("No such plugin");
    return m_plugins[index];
  }

  void plugin_manager::insertRunningDevices (unsigned int index)
  {
    static std::ostringstream sql;
    sql.str("");
    plugin_data *p = getPlugin (index);

    sql << "update plugins set running_ifaces = '";

    for (unsigned int i = 0; i < p->m_runningDevices.size(); ++i)
    {
      sql << p->m_runningDevices[i];
      if (i != p->m_runningDevices.size() - 1)
        sql << ",";
    }

    sql << "' where id = " << p->m_dbid;
    m_db->execSQL (sql.str().c_str());
  }

  void plugin_manager::initPlugin (plugin_data *p)
  {
    p->m_plugin->init (m_db);
    p->m_devices = p->m_plugin->getWantedDevice();
    p->m_protocol = p->m_plugin->getWantedProtocol();
    printf ("dev: %d protos: %d\n", p->m_devices, (int) p->m_protocol);
    std::ostringstream message;
    message << p->m_plugin->getName() << " dev: " << p->m_devices << " protos: " << p->m_protocol << "\n";
    log::instance()->add(_eLogDebug, message.str().c_str());
  }
}
