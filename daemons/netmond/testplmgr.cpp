#include <cstdio>
#include "pluginmanager.h"

int main()
{
  using namespace netmon;

  try
  {
    plugin_manager p(0);
    p.createPlugin ("./dummyplugin.so");
  }
  catch (std::exception &e)
  {
    printf ("error: %s\n", e.what());
  }
  return 0;
}
