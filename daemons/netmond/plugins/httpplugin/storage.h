#ifndef __HTTP_STORAGE__
#define __HTTP_STORAGE__

#include "../common/iphashmap.h"
#include "../common/toplist.h"

#include <string>

namespace netmon
{
  class streamer;

  class storage
  {
    public:
      storage () {}

      virtual ~storage ()
      {
        clear();
      }

      void addEntry (const ip_port_entry &, uint32_t, const std::string &);
      void addData (const ip_port_entry &, uint32_t);
      bool hasEntry (const ip_port_entry &);
      void store (streamer &);
      void clear ();

    protected:
      struct data
      {
        std::string m_host;
        uint64_t m_octets;
      };

      typedef hash_map<ip_port_entry, data *, ip_port_hash, ip_port_eq> http_map_t;

      struct server_data
      {
        uint32_t m_ip;
        uint64_t m_octets;
      };

      struct string_hash
      {
        __gnu_cxx::hash<const char *> m_hasher;
        size_t operator() (const std::string &a) const
        {
          return m_hasher.operator()(a.c_str());
        }
      };

      struct string_eq
      {
        bool operator() (const std::string &a, const std::string &b) const
        {
          return a == b;
        }
      };

      typedef hash_map<std::string, server_data, string_hash, string_eq> server_map_t;

      struct server_item
      {
        std::string m_host;
        server_data m_data;
      };

      struct server_item_comp
      {
        bool operator() (const server_item &a, const server_item &b) const
        {
          return a.m_data.m_octets > b.m_data.m_octets;
        }
      };

      struct client_item
      {
        uint32_t m_ip;
        uint64_t m_octets;
      };

      struct client_item_comp
      {
        bool operator() (const client_item &a, const client_item &b) const
        {
          return a.m_octets > b.m_octets;
        }
      };

    public:
      typedef toplist<server_item, server_item_comp> top_servers_t;
      typedef toplist<client_item, client_item_comp> top_clients_t;

    protected:
      void getTopServers (top_servers_t &);
      void getTopClients (top_clients_t &);

      http_map_t m_map;
  };
}

#endif // __HTTP_STORAGE__
