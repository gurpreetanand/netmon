#include "httpplugin.h"
#include "reqstorage.h"
#include "dvars.h"
#include "lock.h"
#include "log.h"

#include <dumbnet.h>
#include <cstring>
#include <sstream>
#include <iostream>

namespace netmon
{
  http_plugin::~http_plugin ()
  {
    delete m_streamer;
    delete m_reqStorage;
  }

  void http_plugin::init (db *mydb)
  {
    dvars vars (*mydb, getName());
    int ll = 2;
    int maxwebs = 10;
    int maxwebc = 10;

    vars.get ("loglevel", ll);
    vars.get ("maxwebs", maxwebs);
    vars.get ("maxwebc", maxwebc);

    m_wantedProtocol = _eTCP;

    m_wantedDevs = getWantedDevices (mydb);

    m_total = 0;
    m_wantedData = 1;
    vars.get ("wanteddata", m_wantedData);
    if (m_wantedData < 1 || m_wantedData > 4)
      m_wantedData = 1;

    reload (mydb);

    m_streamer = new shm_streamer (maxwebs, maxwebc);
    m_reqStorage = new request_storage;
  }

  void http_plugin::reload (db *mydb)
  {
    mutex_lock lock (m_mutex);
    m_localnets.clear();
    m_extensions.clear();
    getLocalNets (mydb);
    getInvalidExtensions (mydb);
  }

  void http_plugin::onTimer (const timer *t, db *mydb)
  {
    if (t->getTimerID() == 1)
    {
      mutex_lock lock (m_mutex);
      m_streamer->setTotalOctets (m_total);
      m_streamer->setInterval (m_commitLiveInt);
      m_storage.store (*m_streamer);
      m_storage.clear();
      m_total = 0;
      return;
    }

    web_data_list_t wlist;

    mutex_lock lock (m_mutex);
    m_reqStorage->getWebData (wlist);
    m_reqStorage->clear();
    lock.unlock();
 

    for (web_data_list_t::iterator i = wlist.begin(); i != wlist.end(); i++)
    {
      try {
        mydb->execSQL(getInsert(*i, mydb).c_str());
      } catch (int e) {
        std::cout << "Bad UTF characters" << std::endl;
      }
      delete *i;
    }

    wlist.clear();
    reload (mydb);
  }

  std::string http_plugin::getInsert (web_data *data, db *mydb)
  {
    std::ostringstream sql("");
    in_addr addr;

    sql << "INSERT INTO web_traffic (src_ip, dst_ip, url, host_name, timestamp, content_type) VALUES (";
    addr.s_addr = data->m_src;
    sql << "'" << inet_ntoa (addr) << "',";
    addr.s_addr = data->m_dst;
    sql << "'" << inet_ntoa (addr) << "',"
        << mydb->escapeInsertString(data->m_url)   << ","
        << mydb->escapeInsertString(data->m_host) << ","
        << data->m_time << ","
        << mydb->escapeInsertString(data->m_contentType)
        << ")";


    return sql.str();
  }

  void http_plugin::onData (const uint8_t *data, uint16_t size, uint8_t, ip_port_entry *e)
  {
    static tcp_data td;

    if (size < 16) // this cannot be HTTP request header
      return;

    m_total += size;

    if (!validIp (e->m_src, e->m_dst)) // we want local IPs only
      return;

    td.m_inetAddrs.m_src = e->m_src;
    td.m_inetAddrs.m_dst = e->m_dst;
    td.m_inetAddrs.m_srcPort = ntohs (e->m_srcPort);
    td.m_inetAddrs.m_dstPort = ntohs (e->m_dstPort);
    td.m_data = const_cast<uint8_t *> (data);
    td.m_len = size;
    td.m_data[size - 1] = '\0';
//printf ("got: %s\n", tcp_data);
    parseData (td, size);
  }

  bool http_plugin::validIp (uint32_t src, uint32_t dst)
  {
    if (m_wantedData == 1) // 1 - log everything
      return true;

    inetaddr s(src);
    inetaddr d(dst);
    bool f1, f2;
    f1 = f2 = false;

    mutex_lock lock (m_mutex);
    for (net_list::iterator i = m_localnets.begin(); i != m_localnets.end(); ++i)
    {
      if ((*i).first <= s && s <= (*i).second)
        f1 = true;
      if ((*i).first <= d && d <= (*i).second)
        f2 = true;
    }
    lock.unlock();

    if (m_wantedData == 2 && f1 && !f2) // 2 - log local to external
      return true;

    if (m_wantedData == 3 && f1 && f2) // 3 - log local to local
      return true;

    if (m_wantedData == 4 && !f1 && f2) // 4 - external to local
      return true;

    return false;
  }

  bool http_plugin::validURL (const char *url)
  {
    int size = 0;
    const char *dot;
    const char *q = strchr (url, '?');
    if (q != 0)
    {
      dot = (char *) memrchr ((void *) url, '.', q - url);
      if (dot != 0)
        size = (int) (q - dot);
    }
    else
    {
      dot = strrchr (url, '.');
      if (dot != 0)
        size = strlen (dot);
    }

    if (dot == 0)
      return true;

    std::string ext = std::string (dot, size);
    mutex_lock lock (m_mutex);
    if (m_extensions.find (ext) != m_extensions.end())
      return false;
    return true;
  }

  void http_plugin::parseData (tcp_data &data, uint16_t size)
  {
    static char url[255];
    static char host[255];
    static char ct[255];

    bool isRequest = false;
    bool isResponse = false;
    unsigned rlen = 0;

    if (m_storage.hasEntry (data.m_inetAddrs)) // we've seen this connection before
      m_storage.addData (data.m_inetAddrs, size);

    if (strncmp ((char *) data.m_data, "GET ", 4) == 0) // this is an HTTP GET req.
    {
      isRequest = true;
      rlen = 4;
    }
    else
    {
      if (strncmp ((char *) data.m_data, "HTTP/1.1 ", 9) == 0 || // HTTP response
          strncmp ((char *) data.m_data, "HTTP/1.0 ", 9) == 0)
      {
        isResponse = true;
        rlen = 9;
      }
    }

    if (!isRequest && !isResponse)
      return;

    char *end = 0;
    char *endline;
    unsigned int len;

    if (isRequest)
    {
      // get URL
      end = strchr ((char *) (data.m_data + rlen), ' ');
      len = end - (char *) (data.m_data + rlen);
      if (end == 0 || len >= 255)
        return;
      memcpy ((void *) url, (void *) (data.m_data + rlen), len);
      url[len] = '\0';

      if (!validURL (url))
        return;

      // get hostname
      if (data.m_len < len + 14)
        return;
      end = strstr ((char *)(data.m_data + rlen + len + 10), "Host: ");
      if (end == 0)
        return;
      end += 6;
      endline = strpbrk (end, "\r\n");
      if (endline == 0)
        return;
      len = endline - end;
      if (len == 0 || len >= 255)
        return;
      memcpy ((void *) host, (void *) end, len);
      host[len] = '\0';
    }
    else if (isResponse)
    {
      // get content-type
      end = strstr ((char *)(data.m_data + rlen), "Content-Type: ");
      if (end != 0)
      {
        end += 14;
        endline = strpbrk (end, "; \r\n");
        if (endline == 0)
          return;
        len = endline - end;
        if (len == 0 || len >= 255)
          return;
        memcpy ((void *) ct, (void *) end, len);
        ct[len] = '\0';
      }
      else
        *ct = '\0';
    }

    if (isRequest)
    {
      mutex_lock lock (m_mutex);
      m_storage.addEntry (data.m_inetAddrs, size, std::string (host));
      m_reqStorage->addRequest (data.m_inetAddrs, url, host);
//      printf ("URL: %s Host: %s\n", url, host);
    }
    if (isResponse)
    {
      ip_port_entry tmp (data.m_inetAddrs.m_dst, data.m_inetAddrs.m_src, data.m_inetAddrs.m_dstPort, data.m_inetAddrs.m_srcPort);
      mutex_lock lock (m_mutex);
      m_reqStorage->addResponse (tmp, ct);
    }
  }

  void http_plugin::getLocalNets (db *mydb)
  {
    PGresult *res = mydb->execQuery ("SELECT NETWORK, BROADCAST FROM LOCALNETS");
    for (int i = 0; i < PQntuples (res); ++i)
    {
      m_localnets.push_back (std::make_pair (inetaddr (PQgetvalue (res, i, 0)),
                             inetaddr (PQgetvalue (res, i, 1))));
    }
    PQclear (res);
  }

  void http_plugin::getInvalidExtensions (db *mydb)
  {
    PGresult *res = mydb->execQuery ("SELECT EXTENSION FROM IGNORED_HTTP_EXTENSIONS");
    for (int i = 0; i < PQntuples (res); ++i)
    {
      m_extensions.insert (PQgetvalue (res, i, 0));
    }
    PQclear (res);
  }
}

extern "C" netmon::plugin *create (int id)
{
  return new netmon::http_plugin(id);
}

extern "C" void destroy (netmon::plugin *p)
{
  delete p;
}
