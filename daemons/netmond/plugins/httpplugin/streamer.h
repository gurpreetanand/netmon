#ifndef __STREAMER__
#define __STREAMER__

#include "netmon.h"
#include "storage.h"
#include "shm.h"

namespace netmon
{
  class streamer
  {
    public:
      streamer (int maxs, int maxc)
        : m_maxWebS(maxs), m_maxWebC(maxc)
      {}
      virtual ~streamer () {}

      virtual void serialize (const storage::top_servers_t &) = 0;
      virtual void serialize (const storage::top_clients_t &) = 0;

      void setInterval (int interval)
      {
        m_interval = interval;
      }

      void setTotalOctets (uint64_t octets)
      {
        m_octets = octets;
      }

      int getMaxServers () const
      {
        return m_maxWebS;
      }

      int getMaxClients () const
      {
        return m_maxWebC;
      }

    protected:
      int m_interval;
      uint64_t m_octets;
      int m_maxWebS;
      int m_maxWebC;
  };

  class stdout_streamer : public streamer
  {
    public:
      stdout_streamer ()
        : streamer (10, 10)
      {}
      virtual void serialize (const storage::top_servers_t &t)
      {
        for (storage::top_servers_t::const_iterator i = t.begin(); i != t.end(); ++i)
          //printf ("%s %" u64prefix "d\n", (*i).m_host.c_str(), (*i).m_data.m_octets);
          return;
      }

      virtual void serialize (const storage::top_clients_t &t)
      {
        for (storage::top_clients_t::const_iterator i = t.begin(); i != t.end(); ++i)
          //printf ("%" u64prefix "d\n", (*i).m_octets);
          return;
      }
  };

  class shm_streamer : public streamer
  {
    public:
      shm_streamer (int = 10, int = 10);
      ~shm_streamer ();

      virtual void serialize (const storage::top_servers_t &);
      virtual void serialize (const storage::top_clients_t &);

    protected:
      void initshm ();

      shm *m_shmwebs;
      shm *m_shmwebc;
  };
}

#endif // __STREAMER__
