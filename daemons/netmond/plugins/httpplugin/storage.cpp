#include "storage.h"
#include "streamer.h"

namespace netmon
{
  void storage::addEntry (const ip_port_entry &e, uint32_t octets, const std::string &host)
  {
    if (hasEntry(e))
      return;
    data *d = new data();
    d->m_host = host;
    for (unsigned int i = 0; i < d->m_host.size(); ++i)
      d->m_host[i] = tolower (d->m_host[i]);
    d->m_octets = octets;
    m_map[e] = d;
  }

  void storage::addData (const ip_port_entry &e, uint32_t octets)
  {
    if (!hasEntry(e))
      return;
    m_map[e]->m_octets += octets;
  }

  bool storage::hasEntry (const ip_port_entry &e)
  {
    return (m_map.find(e) != m_map.end());
  }

  void storage::clear ()
  {
    for (http_map_t::iterator i = m_map.begin(); i != m_map.end(); ++i)
      delete i->second;
    m_map.clear();
  }

  void storage::store (streamer &s)
  {
    top_servers_t l(s.getMaxServers());
    getTopServers (l);
    s.serialize (l);

    top_clients_t c(s.getMaxClients());
    getTopClients (c);
    s.serialize (c);
  }

  void storage::getTopServers (top_servers_t &l)
  {
    server_map_t m;
    server_map_t::iterator j;
    for (http_map_t::iterator it = m_map.begin(); it != m_map.end(); ++it)
    {
      j = m.find(it->second->m_host);
//printf ("found %s in hash_map\n", it->second->m_host.c_str());
      if (j == m.end())
      {
        server_data sd;
        sd.m_ip = it->first.m_dst;
        sd.m_octets = it->second->m_octets;
        m[it->second->m_host] = sd;
      }
      else
        j->second.m_octets += it->second->m_octets;
    }
    for (server_map_t::iterator it = m.begin(); it != m.end(); ++it)
    {
      server_item item;
      item.m_host = it->first;
      item.m_data = it->second;
      l.insert (item);
    }
  }

  void storage::getTopClients (top_clients_t &t)
  {
    hash_map<uint32_t, uint64_t> map;
    for (http_map_t::iterator it = m_map.begin(); it != m_map.end(); ++it)
    {
      if (map.find (it->first.m_src) != map.end())
        map[it->first.m_src] += it->second->m_octets;
      else
        map[it->first.m_src] = it->second->m_octets;
    }
    for (hash_map<uint32_t, uint64_t>::iterator it = map.begin(); it != map.end(); ++it)
    {
      client_item item;
      item.m_ip = it->first;
      item.m_octets = it->second;
      t.insert (item);
    }
  }
}
