#ifndef __REQUEST_STORAGE__
#define __REQUEST_STORAGE__

#include <list>
#include <string>

#include "../common/iphashmap.h"

namespace netmon
{
  struct web_data
  {
    web_data (uint32_t src, uint32_t dst, const char *url, const char *host, const char *ct)
      : m_src (src), m_dst (dst), m_url (url), m_host (host), m_contentType (ct), m_time (time(0))
    {}

    web_data (uint32_t src, uint32_t dst, const std::string &url, const std::string &host, const std::string &ct)
      : m_src (src), m_dst (dst), m_url (url), m_host (host), m_contentType (ct), m_time (time(0))
    {}

    uint32_t m_src;
    uint32_t m_dst;
    std::string m_url;
    std::string m_host;
    std::string m_contentType;
    uint32_t m_time;
  };

  typedef std::list<web_data *> web_data_list_t;

  class request_storage
  {
    public:
      request_storage ();
      ~request_storage ();

      void clear ();
      void addRequest (const ip_port_entry &, const char *, const char *);
      void addResponse (const ip_port_entry &, const char *);
      void getWebData (web_data_list_t &);
      const bool hasEntry (const ip_port_entry &) const;

    private:
      struct data
      {
        data (const char *url, const char *host)
          : m_url (url), m_host (host), m_time (time(0)), m_done (false)
        {}

        std::string m_url;
        std::string m_host;
        std::string m_contentType;
        uint32_t m_time;
        bool m_done;
      };

      typedef std::list<data *> data_list_t;
      typedef hash_map<ip_port_entry, data_list_t, ip_port_hash, ip_port_eq> req_map_t;

      req_map_t m_map;
  };
}

#endif // __REQUEST_STORAGE__
