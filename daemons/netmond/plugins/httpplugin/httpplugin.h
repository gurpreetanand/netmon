#ifndef __HTTP_PLUGIN__
#define __HTTP_PLUGIN__

#include <list>
#include <set>
#include <string>
#include <strings.h>
#include <stdint.h>

#include "inetaddr.h"
#include "plugin.h"
#include "timer.h"
#include "storage.h"
#include "reqstorage.h"
#include "streamer.h"
#include "mutex.h"
#include "../common/iphashmap.h"

namespace netmon
{
  class db;

  class http_plugin : public plugin
  {
    public:
      http_plugin (unsigned int id)
        : plugin (id)
      {
      }
      ~http_plugin ();

      virtual void init (db *);
      virtual unsigned int getWantedDevice ()
      {
        return m_wantedDevs;
      }

      virtual unsigned int getWantedProtocol ()
      {
        return m_wantedProtocol;
      }

      virtual const bool wantsHeaders () const
      {
        return false;
      }

      virtual void getTimers (std::vector<timer *> &vec)
      {
        vec.push_back (new full_span_timer (m_commitInt, 0, m_id));
        vec.push_back (new timer (m_commitLiveInt, 1, m_id));
      }

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0);
      virtual void onTimer (const timer *, db *);

      virtual const char *getName ()
      {
        return "mod_http";
      }

    protected:
      struct tcp_data
      {
        ip_port_entry m_inetAddrs;
        u_char *m_data;
        uint32_t m_len;
      };

      void reload (db *);
      void getLocalNets (db *);
      void getInvalidExtensions (db *);
      bool validIp (uint32_t, uint32_t);
      bool validURL (const char *);
      void parseData (tcp_data &, uint16_t);

      void commit ();
      std::string getInsert (web_data *data, db *mydb);

      typedef std::list<std::pair<inetaddr, inetaddr> > net_list;
      net_list m_localnets;

      struct casecomp
      {
        bool operator() (const std::string &a, const std::string &b) const
        {
          return (strcasecmp (a.c_str(), b.c_str()) < 0);
        }
      };

      typedef std::set<std::string, casecomp> ext_set;
      ext_set m_extensions;

      unsigned int m_wantedDevs;
      int m_wantedData;
      unsigned int m_wantedProtocol;
      storage m_storage;
      streamer *m_streamer;
      request_storage *m_reqStorage;
      uint64_t m_total;
      mutex m_mutex;
      static const int m_commitInt = 120;
      static const int m_commitLiveInt = 20;
  };
}

#endif // __HTTP_PLUGIN__
