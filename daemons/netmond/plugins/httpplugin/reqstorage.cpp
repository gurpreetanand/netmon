#include "reqstorage.h"

namespace netmon
{
  request_storage::request_storage ()
  {
  }

  request_storage::~request_storage ()
  {
    clear();
  }

  void request_storage::clear ()
  {
    for (req_map_t::iterator i = m_map.begin(); i != m_map.end(); ++i)
    {
      for (data_list_t::iterator j = i->second.begin(); j != i->second.end(); ++j)
        delete *j;
      i->second.clear();
    }
    m_map.clear();
  }

  void request_storage::addRequest (const ip_port_entry &ips, const char *url, const char *host)
  {
    m_map[ips].push_back (new data (url, host));
  }

  void request_storage::addResponse (const ip_port_entry &ips, const char *ct)
  {
    req_map_t::iterator i = m_map.find (ips);
    if (i == m_map.end())
      return;
    if (i->second.empty())
      return;
    for (data_list_t::reverse_iterator j = i->second.rbegin(); j != i->second.rend(); ++j)
      if ((*j)->m_done)
        continue;
      else
      {
        (*j)->m_contentType = ct;
        (*j)->m_done = true;
      }
  }

  void request_storage::getWebData (web_data_list_t &list)
  {
    for (req_map_t::iterator i = m_map.begin(); i != m_map.end(); ++i)
      for (data_list_t::iterator j = i->second.begin(); j != i->second.end(); ++j)
        list.push_back (new web_data (i->first.m_src, i->first.m_dst, (*j)->m_url, (*j)->m_host, (*j)->m_contentType));
  }

  const bool request_storage::hasEntry (const ip_port_entry &e) const
  {
    return (m_map.find (e) != m_map.end());
  }
}
