#ifndef __DUMMY_PLUGIN__
#define __DUMMY_PLUGIN__

#include "plugin.h"
#include "timer.h"
#include <cstdio>

namespace netmon
{
  class dummy_plugin : public plugin
  {
    public:
      dummy_plugin (unsigned int id)
        : plugin (id)
      {}

      ~dummy_plugin ()
      {
        printf ("plugin dtor() called\n");
      }

      virtual void init (db *)
      {
        printf ("init() called\n");
      }

      virtual unsigned int getWantedDevice ()
      {
        return 1;
      }

      virtual unsigned int getWantedProtocol ()
      {
        return _eIP;
      }

      virtual void getTimers (std::vector<timer *> &vec)
      {
        vec.push_back (new timer (5, 0, m_id));
        vec.push_back (new full_span_timer (10, 1, m_id));
      }

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0)
      {
      }

      virtual void onTimer (const timer *t, db *)
      {
        printf ("onTimer() id: %d (%d)\n", t->getTimerID(), (int) time(0));
      }

      virtual const char *getName ()
      {
        return "mod_dummy";
      }
  };
}

#endif // __DUMMY_PLUGIN__
