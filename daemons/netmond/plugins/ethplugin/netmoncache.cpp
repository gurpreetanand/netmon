#include "netmoncache.h"

#include <string>
#include <sstream>

#include "exception.h"
#include "netmon.h"
#include "log.h"

namespace netmon
{
  static const unsigned int semkey  = 0xff00cecb;
  static const unsigned int shmconv = 0xf5000000;
  static const unsigned int shmproto = 0xf5000001;

  netmoncache::netmoncache (int maxconv, int maxproto)
    : cache(), m_sem (semkey, IPC_CREAT | S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH)
  {
    if (maxconv < 1 || maxproto < 1)
      throw (netmon_exception ("Illegal parameter ( < 1)"));
    m_maxConv = maxconv;
    m_maxProto = maxproto;
    initshm();
  }

  netmoncache::~netmoncache ()
  {
    delete m_shmconv;
    delete m_shmproto;
  }

  void netmoncache::initshm ()
  {
    int size = m_maxConv * 70 + 4096;
    m_shmconv = new shm (shmconv, size, IPC_CREAT | 0666, true);
    size = m_maxProto * 40 + 4096;
    m_shmproto = new shm (shmproto, size, IPC_CREAT | 0666, true);
  }

  void netmoncache::storeLive (int interval)
  {
    m_sem.wait();
    storeConv (interval);
    storeProto (interval);
    m_sem.post();
  }

  void netmoncache::storeConv (int interval)
  {
    uint64_t total = getTotalBytes();

    try
    {
      toplist<ethconv, ethconvcomp> l(m_maxConv);
      getTopConversations (l);
      char *buf = m_shmconv->buffer();
      m_shmconv->fill (0xff);
      int i = 0;
      int size = m_shmconv->size();
      printf ("%d entries to shm\n", (int) l.size());
      log::instance()->add(_eLogDebug, "%d entries to shm\n", l.size());
      for (toplist<ethconv, ethconvcomp>::iterator it = l.begin(); it != l.end(); ++it)
      {
        std::string mac;
        mac = (*it).m_src.format();
        i += snprintf (buf + i, size - i, "%s\n", mac.c_str());
        mac = (*it).m_dst.format();
        i += snprintf (buf + i, size - i, "%s\n", mac.c_str());
        i += snprintf (buf + i, size - i, "%4.0X\n", (*it).m_type);
        if ((*it).m_type == 0x800)
          i += snprintf (buf + i, size - i, "%4.0X\n", (*it).m_ipproto);
        else
          i += snprintf (buf + i, size - i, "N/A\n");
        i += speedtostr (buf + i, size - i, (*it).m_bytes, interval);
        i += snprintf (buf + i, size - i, "%.2f", (double)((*it).m_bytes * 100 / total)) + 1;
      }
    }
    catch (int e)
    {
      log::instance()->add(_eLogError, "Cannot create shared memory segment");
    }
  }

  void netmoncache::storeProto (int interval)
  {
    uint64_t total = getTotalBytes();

    try
    {
      toplist<proto, protocomp> l(m_maxProto);
      getTopProtocols (l);
      char *buf = m_shmproto->buffer();
      m_shmproto->fill (0xff);
      int i = 0;
      int size = m_shmproto->size();
      for (toplist<proto, protocomp>::iterator it = l.begin(); it != l.end(); ++it)
      {
        i += snprintf (buf + i, size - i, "%4.0X\n", (*it).m_type);
        if ((*it).m_type == 0x800)
          i += snprintf (buf + i, size - i, "%4.0X\n", (*it).m_ipproto);
        else
          i += snprintf (buf + i, size - i, "N/A\n");
        i += speedtostr (buf + i, size - i, (*it).m_bytes, interval);
        i += snprintf (buf + i, size - i, "%.2f", (double)((*it).m_bytes * 100 / total)) + 1;
      }
    }
    catch (int e)
    {
      log::instance()->add(_eLogError, "Cannot create shared memory segment");
    }
  }
}
