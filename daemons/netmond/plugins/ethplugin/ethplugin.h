#ifndef __ETH_PLUGIN__
#define __ETH_PLUGIN__

#include "plugin.h"
#include "timer.h"
#include "mutex.h"

namespace netmon
{
  class cache;

  class eth_plugin : public plugin
  {
    public:
      eth_plugin (unsigned int id)
        : plugin (id), m_wantedDevs (0)
      {}
      ~eth_plugin ();

      virtual void init (db *);
      virtual unsigned int getWantedDevice ()
      {
        return m_wantedDevs;
      }

      virtual unsigned int getWantedProtocol ()
      {
        return m_wantedProtocol;
      }

      virtual void getTimers (std::vector<timer *> &vec)
      {
        vec.push_back (new timer (m_commitInt, 0, m_id));
      }

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0);
      virtual void onTimer (const timer *, db *);

      virtual const char *getName ()
      {
        return "mod_eth";
      }

    protected:
      cache *m_cache;
      unsigned int m_wantedDevs;
      unsigned int m_wantedProtocol;
      mutex m_mutex;
      static const int m_commitInt = 60;
  };
}

#endif // __ETH_PLUGIN__
