#include "ethplugin.h"
#include "dvars.h"
#include "log.h"
#include "lock.h"
#include "netmoncache.h"

#include <dumbnet.h>
#include <string>

namespace netmon
{
  eth_plugin::~eth_plugin ()
  {
    delete m_cache;
  }

  void eth_plugin::init (db *mydb)
  {
    dvars vars (*mydb, getName());
    std::string var;
    int maxconv = 300;
    int maxproto = 50;
    int ll = 2;

    vars.get ("maxconv", maxconv);
    vars.get ("maxproto", maxproto);
    vars.get ("loglevel", ll);

    m_cache = new netmoncache (maxconv, maxproto);

    m_wantedProtocol = _eETH;

    m_wantedDevs = getWantedDevices (mydb);
  }

  void eth_plugin::onTimer (const timer *t, db *)
  {
    if (t->getTimerID() == 0)
    {
      mutex_lock l(m_mutex);
      m_cache->storeLive (m_commitInt);
      m_cache->clearLive();
    }
  }

  void eth_plugin::onData (const uint8_t *data, uint16_t size, uint8_t, ip_port_entry *)
  {
    static const int eth_hdr_size = sizeof (eth_hdr);
    eth_hdr *eth = (eth_hdr *) data;
    ip_hdr *iph = (ip_hdr *) (data + eth_hdr_size);

    if (eth == 0)
      return;
    uint16_t type = ntohs (eth->eth_type);

    if (type <= 0x05dc)
      return;

    uint8_t proto = 0;
    if (type == ETH_TYPE_IP && iph != 0)
      proto = iph->ip_p;

    mutex_lock l(m_mutex);
    m_cache->add ((u_char *) eth->eth_src.data, (u_char *) eth->eth_dst.data, type, proto, size);
  }
}

extern "C" netmon::plugin *create (int id)
{
  return new netmon::eth_plugin(id);
}

extern "C" void destroy (netmon::plugin *p)
{
  delete p;
}
