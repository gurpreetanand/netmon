#ifndef __CACHE__
#define __CACHE__

#include <string>

#include "toplist.h"
#include <stdio.h>

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

#include <cstring>
#include <stdint.h>

namespace netmon
{
  struct MACKey
  {
    MACKey ()
    {
      memset (m_addr, 0, 6);
    }

    MACKey (const u_char *mac)
    {
      memcpy (m_addr, mac, 6);
    }

    std::string format () const
    {
      char buf[18];
      snprintf (buf, 18, "%02X:%02X:%02X:%02X:%02X:%02X", m_addr[0], m_addr[1], m_addr[2], m_addr[3], m_addr[4], m_addr[5]);
      return std::string (buf);
    }

    bool operator== (const MACKey &a) const
    {
      return memcmp ((void *) a.m_addr, (void *) m_addr, 6) == 0;
    }

    u_char m_addr[6];
  };

  struct MACKeyEq
  {
    bool operator() (const MACKey &k1, const MACKey &k2) const
    {
      int i = memcmp ((void *) k1.m_addr, (void *) k2.m_addr, 6);
      return i == 0;
    }
  };

  struct MACKeyHash
  {
    size_t operator() (netmon::MACKey __s) const
    {
      size_t tmp = *(size_t *)(__s.m_addr + 2);
      return tmp;
    }
  };

  struct ethconv
  {
    MACKey m_src;
    MACKey m_dst;
    u_short m_type;
    u_short m_ipproto;
    uint64_t m_bytes;
  };

  struct proto
  {
    u_short m_type;
    u_short m_ipproto;
    uint64_t m_bytes;
  };

  struct ethconvcomp
  {
    bool operator() (const ethconv &a, const ethconv &b)
    {
      return a.m_bytes > b.m_bytes;
    }
  };

  struct protocomp
  {
    bool operator() (const proto &a, const proto &b)
    {
      return a.m_bytes > b.m_bytes;
    }
  };

  class cache
  {
    public:
      cache ();
      virtual ~cache ();

      void add (const u_char*, const u_char *, u_short, u_short, uint64_t);

      uint64_t getTotalBytes () const;
      void getTopConversations (toplist<ethconv, ethconvcomp> &);
      void getTopProtocols (toplist<proto, protocomp> &);

      void clearLive ();

      virtual void storeLive (int) {}

    protected:
      typedef hash_map<size_t, uint64_t> protomap;

      protomap m_protomap;
      uint64_t m_total;

    private:
      typedef MACKey mac_t;

      struct eth_data
      {
        eth_data (const u_char *src, const u_char *dst)
          : m_src (src), m_dst (dst)
        {}

        eth_data &operator= (const eth_data &a)
        {
          m_src = a.m_src;
          m_dst = a.m_dst;
          m_type = a.m_type;
          m_proto = a.m_proto;
          return *this;
        }
        mac_t m_src;
        mac_t m_dst;
        uint16_t m_type;
        uint16_t m_proto;
      };

      struct eth_data_hash
      {
        size_t operator() (const eth_data &a) const
        {
          size_t hash = 12;
          unsigned short val = *((unsigned short *)(a.m_src.m_addr + 4));

          // step 1
          hash += val;
          val = *((unsigned short *)(a.m_dst.m_addr + 4));
          hash ^= hash << 16;
          hash ^= val << 11;
          hash += hash >> 11;

          val = a.m_type;
          // step 2
          hash += val;
          val = a.m_proto;
          hash ^= hash << 16;
          hash ^= val << 11;
          hash += hash >> 11;

          // final step
          hash ^= hash << 3;
          hash += hash >> 5;
          hash ^= hash << 2;
          hash += hash >> 15;
          hash ^= hash << 10;

          return hash;
        }
      };

      struct eth_data_eq
      {
        bool operator() (const eth_data &a, const eth_data &b) const
        {
          return (a.m_src == b.m_src && a.m_dst == b.m_dst && a.m_type == b.m_type && a.m_proto == b.m_proto);
        }
      };

      typedef hash_map<eth_data, uint64_t, eth_data_hash, eth_data_eq> eth_data_map_t;
      eth_data_map_t m_livemap;

      void add (const eth_data &, uint64_t);
  };
}

#endif //  __CACHE__
