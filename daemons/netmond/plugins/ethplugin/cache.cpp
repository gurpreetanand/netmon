#include "cache.h"

namespace netmon
{
  cache::cache ()
    : m_total (0)
  {
  }

  cache::~cache ()
  {
    m_livemap.clear();
    m_protomap.clear();
  }

  void cache::add (const u_char *src, const u_char *dst, u_short type, u_short proto, uint64_t bytes)
  {
    eth_data data (src, dst);
    data.m_type = type;
    data.m_proto = proto;

    size_t tmp = (type << 16) | proto;
    add (data, bytes);
    m_protomap[tmp] += bytes;
    m_total += bytes;
  }

  uint64_t cache::getTotalBytes () const
  {
    return m_total;
  }

  void cache::getTopConversations (toplist<ethconv, ethconvcomp> &tlist)
  {
    for (eth_data_map_t::iterator i = m_livemap.begin(); i != m_livemap.end(); ++i)
    {
      ethconv entry;
      entry.m_src = i->first.m_src;
      entry.m_dst = i->first.m_dst;
      entry.m_type = i->first.m_type;
      entry.m_ipproto = i->first.m_proto;
      entry.m_bytes = i->second;
      tlist.insert (entry);
    }
  }

  void cache::getTopProtocols (toplist<proto, protocomp> &tlist)
  {
    for (protomap::iterator it = m_protomap.begin(); it != m_protomap.end(); ++it)
    {
      proto tmp;
      tmp.m_type = (it->first & 0xffff0000) >> 16;
      tmp.m_ipproto = it->first;
      tmp.m_bytes = it->second;
      tlist.insert (tmp);
    }
  }

  void cache::clearLive ()
  {
    m_livemap.clear();
    m_protomap.clear();
    m_total = 0;
  }

  void cache::add (const eth_data &data, uint64_t bytes)
  {
    eth_data_map_t::iterator i = m_livemap.find (data);
    if (i != m_livemap.end())
      i->second += bytes;
    else
      m_livemap[data] = bytes;
  }
}
