#ifndef __NETMON_CACHE__
#define __NETMON_CACHE__

#include "cache.h"
#include "semaphore.h"
#include "shm.h"

namespace netmon
{
  class netmoncache : public cache
  {
    public:
      netmoncache (int = 300, int = 50);
      ~netmoncache ();

      virtual void storeLive (int);

    protected:
      void initshm ();
      void storeConv (int);
      void storeProto (int);

      shm *m_shmconv;
      shm *m_shmproto;
      semaphore m_sem;
      int m_maxConv;
      int m_maxProto;
  };
}

#endif // __NETMON_CACHE__
