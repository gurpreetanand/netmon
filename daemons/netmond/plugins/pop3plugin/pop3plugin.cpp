#include "pop3plugin.h"
#include "strings.h"
#include "dvars.h"
#include "lock.h"
#include "log.h"

#include <dumbnet.h>
#include <strings.h>
#include <cstring>
#include <string>
#include <sstream>

namespace netmon
{
  pop3_plugin::~pop3_plugin ()
  {
    delete m_eParser;
  }

  void pop3_plugin::init (db *mydb)
  {
    dvars vars (*mydb, getName());
    int ll = 2;

    vars.get ("loglevel", ll);

    m_wantedProtocol = _eTCP;

    m_wantedDevs = getWantedDevices (mydb);

    m_eParser = new email_parser (m_parser);
  }

  void pop3_plugin::onTimer (const timer *, db *mydb)
  {
    commit_list_t tmp;
    mutex_lock lock (m_mutex);

    uint32_t now = time (0);

    for (conn_map_t::iterator i = m_connections.begin(); i != m_connections.end(); )
    {
      // insert connections with time out
      if ((now - i->second->m_timestamp) > 600 || i->second->m_state == pop3_data::eDone)
      {
        tmp.push_back (commit_data (i->second));
        delete i->second;
        m_connections.erase (i++);
        continue;
      }
      else
        if (!i->second->m_emails.empty()) // insert emails that have been fully retrieved
          tmp.push_back (commit_data (i->second));
      ++i;
    }

    lock.unlock();

    commit_list_t::iterator end = tmp.end();
    for (commit_list_t::iterator i = tmp.begin(); i != end; ++i)
      for (std::list <email_data *>::iterator j = (*i).m_emails.begin(); j != (*i).m_emails.end(); ++j)
      {
        if (!(*j)->m_from.m_user.empty() && !(*j)->m_from.m_domain.empty() > 0 && !(*j)->m_to.empty()) {
          mydb->execSQL("BEGIN");
          commit ((*i).m_serverIP, (*i).m_clientIP, (*i).m_user, *j, "pop3", mydb);
          if ((*j)->m_hasAttach)
            commitAttach (*j, mydb);
          mydb->execSQL("COMMIT");
        }
	delete *j;
      }

  }

  void pop3_plugin::onData (const uint8_t *data, uint16_t size, uint8_t, ip_port_entry *e)
  {
    if (size == 0)
      return;

    prepareIPPortPair (*e, m_IPPortPair);

    mutex_lock lock (m_mutex);

    conn_map_t::iterator i = m_connections.find (m_IPPortPair);
    if (i == m_connections.end())
    {
      m_connections[m_IPPortPair] = new pop3_data (e->m_src, e->m_dst);
      return; // nothing else to do since this is server's greeting
    }
    parseData (i->second, data, size, e->m_src == i->second->m_clientIP);
  }

  void pop3_plugin::parseData (pop3_data *pop3, const uint8_t *data, uint16_t size, bool fromClient)
  {
    pop3->m_timestamp = time (0);

    if (fromClient)
    {
      handleClientCommand (pop3, data, size);
    }
    else
      handleServerResponse (pop3, data, size);
  }

  void pop3_plugin::handleClientCommand (pop3_data *pop3, const uint8_t *data, uint16_t size)
  {
    parser_state *ps = new parser_state;
    line_parser *lp = new line_parser;
    ps->newChunk(data, size);
    std::string line;
    
    while (!((line = lp->getLine(ps)).empty())) {
      switch (pop3->m_state)
      {
        case pop3_data::eNew:
        case pop3_data::eUser:
        case pop3_data::eUserOK:
        case pop3_data::ePass:
          handleLogging (pop3, line);
	  delete ps;
	  delete lp;
          return;
        case pop3_data::eLoggedin:
        case pop3_data::eRetr:
          handleRetrieve (pop3, line);
	  delete ps;
	  delete lp;
          return;
        default:
	  delete ps;
	  delete lp;
          return;
      }
    }
  }

  void pop3_plugin::handleServerResponse (pop3_data *pop3, const uint8_t *data, uint16_t size)
  {
    parser_state *ps = new parser_state;
    line_parser *lp = new line_parser;
    ps->newChunk(data, size);

    static const char *sOK = "+OK";
    static const uint8_t len = strlen (sOK);

    std::string line = lp->getLine(ps);
    if (strncasecmp (line.c_str(), sOK, len) == 0 || pop3->m_state == pop3_data::eReceiving)
    {
      do {
        if (line.size() < len)
          continue;

        switch (pop3->m_state)
        {
          case pop3_data::eUser:
            pop3->m_state = pop3_data::eUserOK;
	    delete ps;
	    delete lp;
            return;
          case pop3_data::ePass:
            pop3->m_state = pop3_data::eLoggedin;
	    delete ps;
	    delete lp;
            return;
          case pop3_data::eRetr:
            pop3->m_state = pop3_data::eReceiving;
            if (pop3->m_currEMail != 0)
              pop3->m_emails.push_back (pop3->m_currEMail);
            pop3->m_currEMail = new email_data;
            return handleMailData (pop3, ps, line);
	  case pop3_data::eReceiving:
	    return handleMailData (pop3, ps, line);
          default:
            delete ps;
	    delete lp;
            return; // should not be reached
        }
      } while (!((line = lp->getLine (ps)).empty()));
      delete lp;
    }
  }

  void pop3_plugin::handleLogging (pop3_data *pop3, std::string line)
  {
    static const char *sUser = "USER ";
    static const char *sPass = "PASS ";
    static const char *sAPOP = "APOP ";
    static const uint8_t len = strlen (sUser);

    if (line.size() <= len)
      return;

    if (strncasecmp (line.c_str(), sUser, len) == 0 && pop3->m_state == pop3_data::eNew)
    {
      pop3->m_user = std::string(line, len, line.size()-len);
      trim(pop3->m_user);
      pop3->m_state = pop3_data::eUser; // client sent 'USER'
      return;
    }

    if (strncasecmp (line.c_str(), sAPOP, len) == 0 && pop3->m_state == pop3_data::eNew)
    {
      int start = line.find_first_not_of(' ', len+1);
      int end = line.find_first_of(' ', start);
      pop3->m_user = std::string(line, start, end-start+1);
      pop3->m_state = pop3_data::ePass; // client sent 'APOP'
      return;
    }

    if (strncasecmp (line.c_str(), sPass, len) == 0 && pop3->m_state == pop3_data::eUserOK)
    {
      pop3->m_state = pop3_data::ePass; // client sent 'PASS'
      return;
    }
  }

  void pop3_plugin::handleRetrieve (pop3_data *pop3, std::string line)
  {
    static const char *sRetr = "RETR ";
    static const char *sQuit = "QUIT";
    static const uint8_t len = strlen (sRetr);
    static const uint8_t lenq = strlen (sQuit);

    if (line.size() >= lenq && strncasecmp (line.c_str(), sQuit, lenq) == 0)
    {
      pop3->m_state = pop3_data::eDone; // client sent QUIT
      return;
    }

    if (line.size() <= len)
      return;

    if (strncasecmp (line.c_str(), sRetr, len) == 0)
    {
      pop3->m_state = pop3_data::eRetr; // client sent RETR
      return;
    }
  }

  void pop3_plugin::handleMailData (pop3_data *pop3, parser_state *ps, std::string line)
  {
    pop3->m_currEMail->m_size += line.size();
    if (pop3->m_currEMail->m_eState == email_data::sHeaders)
    {
      m_eParser->handleHeaders (ps, pop3->m_currEMail);
    }
    if (pop3->m_currEMail->m_eState == email_data::sBody || 
        pop3->m_currEMail->m_eState == email_data::sAttach) {
      m_eParser->handleBody (ps, pop3->m_currEMail);
      if (pop3->m_currEMail->m_eState == email_data::sDone)
      {
        pop3->m_state = pop3_data::eLoggedin;
        pop3->m_emails.push_back (pop3->m_currEMail);
        pop3->m_currEMail = 0;
      }
    }
    delete ps;
  }

  void pop3_plugin::prepareIPPortPair (const ip_port_entry &in, ip_port_entry &out)
  {
    if (in.m_src < in.m_dst)
    {
      out.m_src = in.m_src;
      out.m_dst = in.m_dst;
    }
    else
    {
      out.m_src = in.m_dst;
      out.m_dst = in.m_src;
    }
    if (in.m_srcPort < in.m_dstPort)
    {
      out.m_srcPort = in.m_srcPort;
      out.m_dstPort = in.m_dstPort;
    }
    else
    {
      out.m_srcPort = in.m_dstPort;
      out.m_dstPort = in.m_srcPort;
    }
  }
}

extern "C" netmon::plugin *create (int id)
{
  return new netmon::pop3_plugin(id);
}

extern "C" void destroy (netmon::plugin *p)
{
  delete p;
}
