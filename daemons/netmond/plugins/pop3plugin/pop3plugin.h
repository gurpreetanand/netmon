#ifndef __POP3_PLUGIN__
#define __POP3_PLUGIN__

#include <list>
#include <set>
#include <string>
#include <stdint.h>

#include "inetaddr.h"
#include "plugin.h"
#include "timer.h"
#include "mutex.h"
#include "emailparser.h"
#include "emailplugin.h"
#include "lineparser.h"
#include "../common/iphashmap.h"

namespace netmon
{
  class db;

  class pop3_plugin : public plugin, public email_plugin
  {
    public:
      pop3_plugin (unsigned int id)
        : plugin (id)
      {
      }
      ~pop3_plugin ();

      virtual void init (db *);
      virtual unsigned int getWantedDevice ()
      {
        return m_wantedDevs;
      }

      virtual unsigned int getWantedProtocol ()
      {
        return m_wantedProtocol;
      }

      virtual const bool wantsHeaders () const
      {
        return false;
      }

      virtual void getTimers (std::vector<timer *> &vec)
      {
        vec.push_back (new full_span_timer (m_commitInt, 0, m_id));
      }

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0);
      virtual void onTimer (const timer *, db *);

      virtual const char *getName ()
      {
        return "mod_pop3";
      }

    protected:
      struct pop3_data
      {
        enum { eNew, eUser, eUserOK, ePass, eLoggedin, eRetr, eReceiving, eDone };

        pop3_data (uint32_t srv, uint32_t cln)
          : m_serverIP (srv), m_clientIP (cln), m_state (eNew),
            m_timestamp (time (0)), m_currEMail (0)
        {}

        ~pop3_data ()
        {
          if (m_currEMail != 0)
            delete m_currEMail;
          for (std::list <email_data *>::iterator i = m_emails.begin(); i != m_emails.end(); ++i)
            delete *i;
        }

        uint32_t m_serverIP;
        uint32_t m_clientIP;
        uint8_t m_state;
        uint32_t m_timestamp;
        std::string m_user;
        parser_state m_parserState;
        email_data *m_currEMail;
        std::list <email_data *> m_emails;
      };

      void parseData (pop3_data *, const uint8_t *, uint16_t, bool);

      void handleClientCommand (pop3_data *, const uint8_t *, uint16_t);
      void handleServerResponse (pop3_data *, const uint8_t *, uint16_t);

      void handleLogging (pop3_data *, std::string);
      void handleRetrieve (pop3_data *, std::string);
      void handleMailData (pop3_data *, parser_state *, std::string);

      void prepareIPPortPair (const ip_port_entry &, ip_port_entry &);

      unsigned int m_wantedDevs;
      unsigned int m_wantedProtocol;

      typedef hash_map<ip_port_entry, pop3_data *, ip_port_hash, ip_port_eq> conn_map_t;
      conn_map_t m_connections;

      line_parser m_parser;
      email_parser *m_eParser;
      mutex m_mutex;
      static const int m_commitInt = 120;

    private:
      ip_port_entry m_IPPortPair;

      struct commit_data
      {
        commit_data (pop3_data *p)
          : m_serverIP (p->m_serverIP), m_clientIP (p->m_clientIP),
            m_user (p->m_user), m_timestamp (p->m_timestamp)
        {
          m_emails.splice (m_emails.end(), p->m_emails);
        }

        uint32_t m_serverIP;
        uint32_t m_clientIP;
        std::string m_user;
        uint32_t m_timestamp;
        std::list <email_data *> m_emails;
      };

      typedef std::list<commit_data> commit_list_t;
  };
}

#endif // __POP3_PLUGIN__
