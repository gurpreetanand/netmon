#include "imapplugin.h"
#include "strings.h"
#include "dvars.h"
#include "lock.h"
#include "log.h"

#include <dumbnet.h>
#include <pcrecpp.h>
#include <string>
#include <sstream>

namespace netmon
{
  imap_plugin::~imap_plugin ()
  {
    delete m_eParser;
  }

  void imap_plugin::init (db *mydb)
  {
    dvars vars (*mydb, getName());
    int ll = 2;

    vars.get ("loglevel", ll);

    m_wantedProtocol = _eTCP;

    m_wantedDevs = getWantedDevices (mydb);

    m_eParser = new email_parser (m_parser);
  }

  void imap_plugin::onTimer (const timer *, db *mydb)
  {
    commit_list_t tmp;
    mutex_lock lock (m_mutex);

    uint32_t now = time (0);

    for (conn_map_t::iterator i = m_connections.begin(); i != m_connections.end(); )
    {
      // insert connections with time out
      if ((now - i->second->m_timestamp) > 600 || i->second->m_state == imap_data::eDone)
      {
	tmp.push_back (commit_data (i->second));
	delete i->second;
	m_connections.erase (i++);
	continue;
      }
      else
	if (!i->second->m_emails.empty()) // insert emails that have been fully retrieved
	  tmp.push_back (commit_data (i->second));
      ++i;
    }

    lock.unlock();

    commit_list_t::iterator end = tmp.end();
    for (commit_list_t::iterator i = tmp.begin(); i != end; ++i)
      for (std::list <email_data *>::iterator j = (*i).m_emails.begin(); j != (*i).m_emails.end(); ++j)
      {
        if (!(*j)->m_from.m_user.empty() && !(*j)->m_from.m_domain.empty() && !(*j)->m_to.empty())
	{
          mydb->execSQL("BEGIN");
          commit ((*i).m_serverIP, (*i).m_clientIP, (*i).m_user, *j, (*i).m_protocol.c_str(), mydb);
          if ((*j)->m_hasAttach)
            commitAttach (*j, mydb);
          mydb->execSQL("COMMIT");
	}
	delete *j;
      }

  }

  void imap_plugin::onData (const uint8_t *data, uint16_t size, uint8_t, ip_port_entry *e)
  {
    if (size == 0)
      return;

    prepareIPPortPair (*e, m_IPPortPair);

    mutex_lock lock (m_mutex);

    conn_map_t::iterator i = m_connections.find (m_IPPortPair);
    if (i == m_connections.end())
    {
      m_connections[m_IPPortPair] = new imap_data (e->m_src, e->m_dst);
      return; // nothing else to do since this is server's greeting
    }
    parseData (i->second, data, size, e->m_src == i->second->m_clientIP);
  }

  void imap_plugin::parseData (imap_data *imap, const uint8_t *data, uint16_t size, bool fromClient)
  {
    imap->m_timestamp = time (0);

    if (fromClient)
      handleClientCommand (imap, data, size);
    else
      handleServerResponse (imap, data, size);
  }

  void imap_plugin::handleClientCommand (imap_data *imap, const uint8_t *data, uint16_t size)
  {
    parser_state *ps = new parser_state;
    line_parser *lp = new line_parser;
    ps->newChunk(data, size);

    std::string line;

    while (!((line = lp->getLine(ps)).empty())) {
      switch (imap->m_state)
      {
	case imap_data::eNew:
	case imap_data::eLogging:
	  handleLogging (imap, line);
	  delete ps;
	  delete lp;
	  return;
	case imap_data::eLoggedin:
          handleLoggedin (imap, line);
	  delete ps;
	  delete lp;
	  return;
	case imap_data::eFetch:
	default:
	  delete ps;
	  delete lp;
	  return;
      }
    }
  }

  void imap_plugin::handleServerResponse (imap_data *imap, const uint8_t *data, uint16_t size)
  {
    static const char *sOK = " OK ";
    static const char *sStar = "* ";
    static const uint8_t len = strlen (sOK);
    static const uint8_t lens = strlen (sStar);

    parser_state *ps = new parser_state;
    line_parser *lp = new line_parser;
    ps->newChunk(data, size);

    std::string line;

    line = lp->getLine(ps);

    if (line.empty()) {
      delete ps;
      delete lp;
      return;
    }

    if (imap->m_state == imap_data::eFetching) {
      delete lp;
      return handleMailData (imap, ps, line);
    }

    do {
      if (line.size() < lens)
        continue;
      switch (imap->m_state) {
        case imap_data::eLogging:
        {
          if (strncasecmp (line.c_str(), sOK, len) == 0 )
          {
            imap->m_state = imap_data::eLoggedin;
          }
	  delete ps;
	  delete lp;
          return;
        }
        case imap_data::eLoggedin:
	  return;
	case imap_data::eFetch:
          if (strncasecmp (line.c_str(), sStar, lens) == 0)
          {
            imap->m_state = imap_data::eFetching;

            if (imap->m_currEMail != 0)
              imap->m_emails.push_back (imap->m_currEMail);

          imap->m_currEMail = new email_data;
	  delete lp;
          return handleMailData (imap, ps, line);
          } 
	  delete lp;
	  delete ps;
          return;
        case imap_data::eFetching:
	  delete lp;
          return handleMailData (imap, ps, line);

        default:
	  delete lp;
	  delete ps;
          return;
      }
    } while (!(line = lp->getLine (ps)).empty());
  }

  void imap_plugin::handleLogging (imap_data *imap, std::string line)
  {
    static uint8_t len = 11;
    pcrecpp::RE_Options opt;
    opt.set_caseless (true);
    pcrecpp::RE re ("^\\S+ LOGIN \"?([^ \"]+)\"?", opt);

    if (line.size() <= len)
      return;
    if (re.PartialMatch (pcrecpp::StringPiece (line.c_str(), line.size()), &(imap->m_user)))
    {
      log::instance()->add(_eLogFatal, "IMAP: Got User [%s]--Logged in\n", imap->m_user.c_str());

      imap->m_state = imap_data::eLoggedin; // client sent 'LOGIN'
      return;
    }
  }

  void imap_plugin::handleLoggedin (imap_data *imap, std::string line)
  {
    static const char *sBody = "BODY[]";
    static const char *sPeek = "BODY.PEEK[";
    static uint8_t len = 14; // min len

    pcrecpp::RE_Options opt;
    opt.set_caseless (true);
    pcrecpp::RE re ("^\\S+ UID FETCH \\S+ ", opt);

    if (line.size() < len)
      return;

    if (re.PartialMatch (pcrecpp::StringPiece (line.c_str(), line.size()))) //look for FETCH
    {
      if (line.find(sBody) != std::string::npos) // look for BODY[] 
      {
	imap->m_protocol = "imap";
        imap->m_state = imap_data::eFetch; // client sent 'FETCH'
        return;
      } else if (line.find(sPeek) != std::string::npos) { // look for BODY.PEEK[]
	imap->m_protocol = "peek";
	imap->m_state = imap_data::eFetch; // client sent FETCH PEEK
	return;
     }
    }
  }

  void imap_plugin::handleMailData (imap_data *imap, parser_state *ps, std::string line)
  {
    imap->m_currEMail->m_size += line.size();
    if (imap->m_currEMail->m_eState == email_data::sHeaders)
    {
      m_eParser->handleHeaders (ps, imap->m_currEMail);
    }
    if (imap->m_currEMail->m_eState == email_data::sBody || 
        imap->m_currEMail->m_eState == email_data::sAttach)
    {
      m_eParser->handleBody (ps, imap->m_currEMail, true);
      if (imap->m_currEMail->m_eState == email_data::sDone)
      {
        imap->m_state = imap_data::eLoggedin;
        imap->m_emails.push_back (imap->m_currEMail);
        imap->m_currEMail = 0;
      }
    }
  }

  void imap_plugin::prepareIPPortPair (const ip_port_entry &in, ip_port_entry &out)
  {
    if (in.m_src < in.m_dst)
    {
      out.m_src = in.m_src;
      out.m_dst = in.m_dst;
    }
    else
    {
      out.m_src = in.m_dst;
      out.m_dst = in.m_src;
    }
    if (in.m_srcPort < in.m_dstPort)
    {
      out.m_srcPort = in.m_srcPort;
      out.m_dstPort = in.m_dstPort;
    }
    else
    {
      out.m_srcPort = in.m_dstPort;
      out.m_dstPort = in.m_srcPort;
    }
  }
}

extern "C" netmon::plugin *create (int id)
{
  return new netmon::imap_plugin(id);
}

extern "C" void destroy (netmon::plugin *p)
{
  delete p;
}
