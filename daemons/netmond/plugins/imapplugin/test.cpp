#include <cstdio>
#include <string>
#include <pcrecpp.h>

int main()
{
  pcrecpp::RE_Options opt;
  std::string user;
  opt.set_caseless (true);
  pcrecpp::RE re ("^\\S+ LOGIN \"?([^ \"]+)\"?", opt); //  \"?\\S+\"?
  if (re.PartialMatch ("bup6 login \"damir\" \"hakija2!\"", &user))
    printf ("User '%s'\n", user.c_str());
}
