#ifndef __IMAP_PLUGIN__
#define __IMAP_PLUGIN__

#include <list>
#include <set>
#include <string>
#include <stdint.h>

#include "inetaddr.h"
#include "plugin.h"
#include "timer.h"
#include "mutex.h"
#include "emailparser.h"
#include "emailplugin.h"
#include "lineparser.h"
#include "../common/iphashmap.h"

namespace netmon
{
  class db;

  class imap_plugin : public plugin, public email_plugin
  {
    public:
      imap_plugin (unsigned int id)
        : plugin (id)
      {
      }
      ~imap_plugin ();

      virtual void init (db *);
      virtual unsigned int getWantedDevice ()
      {
        return m_wantedDevs;
      }

      virtual unsigned int getWantedProtocol ()
      {
        return m_wantedProtocol;
      }

      virtual const bool wantsHeaders () const
      {
        return false;
      }

      virtual void getTimers (std::vector<timer *> &vec)
      {
        vec.push_back (new full_span_timer (m_commitInt, 0, m_id));
      }

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0);
      virtual void onTimer (const timer *, db *);

      virtual const char *getName ()
      {
        return "mod_imap";
      }

    protected:
      struct imap_data
      {
        enum { eNew, eLogging, eLoggedin, eFetch, eFetching, eDone };

        imap_data (uint32_t srv, uint32_t cln)
          : m_serverIP (srv), m_clientIP (cln), m_state (eNew),
            m_timestamp (time (0)), m_currEMail (0)
        {}

        ~imap_data ()
        {
          if (m_currEMail != 0)
            delete m_currEMail;
          for (std::list <email_data *>::iterator i = m_emails.begin(); i != m_emails.end(); ++i)
            delete *i;
        }

        uint32_t m_serverIP;
        uint32_t m_clientIP;
        uint8_t m_state;
        uint32_t m_timestamp;
        std::string m_user;
        std::string m_tag;
        parser_state m_parserState;
        email_data *m_currEMail;
        std::list <email_data *> m_emails;
        std::string m_protocol;
      };

      void parseData (imap_data *, const uint8_t *, uint16_t, bool);

      void handleClientCommand (imap_data *, const uint8_t *, uint16_t);
      void handleServerResponse (imap_data *, const uint8_t *, uint16_t);

      void handleLogging (imap_data *, std::string);
      void handleLoggedin (imap_data *, std::string);
      void handleMailData (imap_data *, parser_state *, std::string);

      void prepareIPPortPair (const ip_port_entry &, ip_port_entry &);

      unsigned int m_wantedDevs;
      unsigned int m_wantedProtocol;

      typedef hash_map<ip_port_entry, imap_data *, ip_port_hash, ip_port_eq> conn_map_t;
      conn_map_t m_connections;

      line_parser m_parser;
      email_parser *m_eParser;
      mutex m_mutex;
      static const int m_commitInt = 120;

    private:
      ip_port_entry m_IPPortPair;

      struct commit_data
      {
        commit_data (imap_data *p)
          : m_serverIP (p->m_serverIP), m_clientIP (p->m_clientIP),
            m_user (p->m_user), m_timestamp (p->m_timestamp), m_protocol (p->m_protocol)
        {
          m_emails.splice (m_emails.end(), p->m_emails);
        }

        uint32_t m_serverIP;
        uint32_t m_clientIP;
        std::string m_user;
        uint32_t m_timestamp;
        std::list <email_data *> m_emails;
	std::string m_protocol;
      };

      typedef std::list<commit_data> commit_list_t;
  };
}

#endif // __IMAP_PLUGIN__
