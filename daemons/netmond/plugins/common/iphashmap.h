#ifndef __IP_HASH_MAP__
#define __IP_HASH_MAP__

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

#include <stdint.h>

namespace netmon
{
  struct ip_port_entry
  {
    ip_port_entry ()
      : m_src(0), m_dst(0), m_srcPort(0), m_dstPort(0)
    {
    }

    ip_port_entry (uint32_t src, uint32_t dst, uint16_t srcport, uint16_t dstport)
      : m_src(src), m_dst(dst), m_srcPort(srcport), m_dstPort(dstport)
    {
    }

    uint32_t m_src;
    uint32_t m_dst;
    uint16_t m_srcPort;
    uint16_t m_dstPort;
  };

  struct ip_port_hash
  {
    size_t operator() (const ip_port_entry &__s) const
    {
      size_t hash = 12;
      unsigned short val = (__s.m_src & 0xffff0000) >> 16;

      // step 1
      hash += val;
      val = __s.m_src & 0x0000ffff;
      hash ^= hash << 16;
      hash ^= val << 11;
      hash += hash >> 11;

      val = (__s.m_dst & 0xffff0000) >> 16;
      // step 2
      hash += val;
      val = __s.m_dst & 0x0000ffff;
      hash ^= hash << 16;
      hash ^= val << 11;
      hash += hash >> 11;

      unsigned short *d = (unsigned short *) &__s.m_srcPort;
      // step 3
      hash += *d;
      d = (unsigned short *) &__s.m_dstPort;
      hash ^= hash << 16;
      hash ^= *d << 11;
      hash += hash >> 11;

      hash ^= hash << 3;
      hash += hash >> 5;
      hash ^= hash << 2;
      hash += hash >> 15;
      hash ^= hash << 10;

      return hash;
    }
  };

  struct ip_port_eq
  {
    bool operator() (const ip_port_entry &a, const ip_port_entry &b) const
    {
      return (a.m_src == b.m_src && 
              a.m_dst == b.m_dst &&
              a.m_srcPort == b.m_srcPort &&
              a.m_dstPort == b.m_dstPort);
    }
  };
}

#endif // __IP_HASH_MAP__
