#include "udpsender.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>

#include <cstring>
#include <cstdlib>
#include <string>
#include <unistd.h>

namespace netmon
{
  const std::string udp_sender::defaultPort = "9996";

  udp_sender::udp_sender (const char *str)
  {
    ip_port_list_t list;
    parseList (str, list);
    createSockets (list);
  }

  udp_sender::~udp_sender ()
  {
    closeSockets();
  }

  void udp_sender::sendData (const uint8_t *data, size_t size)
  {
    int ret = 0;
    for (std::list<int>::iterator i = m_sockets.begin(); i != m_sockets.end(); ++i)
    {
      ret = write (*i, (void *) data, size);
      printf ("wrote %d bytes\n", ret);
      if (ret == -1)
        perror ("write");
    }
  }

  void udp_sender::parseList (const char *str, ip_port_list_t &list)
  {
    if (str == 0)
      throw address_list_parse_exception ("NULL string used for IP:port list");

    size_t size = strlen (str);
    if (size < 7)
      throw address_list_parse_exception ("Invalid format of IP:port list");

    std::string line = str;
    std::string ip;
    std::string port;

    size_t pos = 0;
    size_t tmp;

    in_addr addr;
    ip_port_t ipp;

    std::string strt;
    while (pos < size)
    {
      tmp = line.find (',', pos);
      if (tmp == std::string::npos)
      {
        strt = std::string (line, pos, line.size());
        pos = size;
      }
      else
      {
        strt = std::string (line, pos, tmp);
        pos = tmp + 1;
      }
      parseEntry (strt, ip, port);
      printf ("ip: %s port: %s\n", ip.c_str(), port.c_str());
      if (inet_aton (ip.c_str(), &addr) == 0)
      {
        std::string tmp = ip + "is not a valid IP address";
        throw address_list_parse_exception (tmp);
      }
      ipp.first = addr.s_addr;
      ipp.second = atoi (port.c_str());
      list.push_back (ipp);
    }
  }

  void udp_sender::parseEntry (const std::string &str, std::string &ip, std::string &port)
  {
    size_t tmp = str.find (':');
    if (tmp == std::string::npos)
    {
      port = defaultPort;
      ip = str;
      return;
    }
    if (tmp == str.size())
      throw address_list_parse_exception ("Invalid format of IP:port list");
    ip = str.substr (0, tmp);
    port = str.substr (tmp + 1, str.size());
  }

  void udp_sender::createSockets (const ip_port_list_t &list)
  {
    uint16_t port;

    for (ip_port_list_t::const_iterator i = list.begin(); i != list.end(); ++i)
    {
      port = htons ((*i).second);
      sockaddr_in addr;
      sockaddr_in srv;

      int fd = socket (AF_INET, SOCK_DGRAM, 0);
      if (fd == -1)
        continue;

      addr.sin_family = AF_INET;
      addr.sin_port = INADDR_ANY;
      addr.sin_addr.s_addr = INADDR_ANY;

      srv.sin_family = AF_INET;
      srv.sin_port = port;
      srv.sin_addr.s_addr = (*i).first;
      
      bind (fd, (sockaddr *) &addr, sizeof (addr));
      connect (fd, (sockaddr *) &srv, sizeof (srv));

      m_sockets.push_back (fd);
    }
  }

  void udp_sender::closeSockets ()
  {
    for (std::list<int>::iterator i = m_sockets.begin(); i != m_sockets.end(); ++i)
      close (*i);
  }
}
