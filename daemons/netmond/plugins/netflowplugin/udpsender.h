#ifndef __UDP_SENDER_H__
#define __UDP_SENDER_H__

#include <list>
#include <string>
#include <utility>
#include <stdint.h>

#include "exception.h"

namespace netmon
{
  class address_list_parse_exception : public netmon_exception
  {
    public:
      address_list_parse_exception (const std::string &err)
        : netmon_exception (err)
      {}
  };

  class udp_sender
  {
    public:
      udp_sender (const char *);
      ~udp_sender ();

      void sendData (const uint8_t *, size_t);

    protected:
      typedef std::pair<uint32_t, uint16_t> ip_port_t;
      typedef std::list<ip_port_t> ip_port_list_t;

      void init ();
      void parseList (const char *, ip_port_list_t &);
      void parseEntry (const std::string &, std::string &, std::string &);
      void createSockets (const ip_port_list_t &);
      void closeSockets ();

      std::list<int> m_sockets;
      static const std::string defaultPort;
  };
}

#endif // __UDP_SENDER_H__
