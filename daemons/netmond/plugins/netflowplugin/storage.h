#ifndef __STORAGE__
#define __STORAGE__

#include "mutex.h"
#include "../common/iphashmap.h"
#include "../common/toplist.h"

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

#include <stdint.h>

namespace netmon
{
  struct flow
  {
    uint32_t m_srcIP;
    uint32_t m_dstIP;
    uint16_t m_srcport;
    uint16_t m_dstport;
    uint32_t m_octets;
    uint32_t m_devID;
    uint8_t m_proto;
    uint8_t m_flags;
  };

  struct flow_data
  {
    flow_data ()
      : m_octets (0), m_proto (0), m_startTime (0), m_endTime (0),
        m_packets (0), m_done (false)
    {}

    flow_data (const flow &f)
      : m_octets (f.m_octets), m_proto (f.m_proto), m_flags (f.m_flags),
        m_packets (1), m_done (false)
    {
      m_startTime = m_endTime = m_lastSentTime = ::time (0);
      checkFlags();
    }

    void checkFlags ()
    {
      if ((m_flags & 0x01) || (m_flags & 0x04)) // FIN or RST set?
        m_done = true;
    }

    uint32_t m_octets;
    uint8_t m_proto;
    uint8_t m_flags;
    uint32_t m_startTime;
    uint32_t m_endTime;
    uint32_t m_lastSentTime;
    uint32_t m_packets;
    uint32_t m_ifIndex;
    bool m_done;
  };

  class udp_sender;
  class netflow_writer;
  class snmp_interfaces;

  class storage
  {
    public:
      storage (udp_sender *, int);
      ~storage ();

      void add (const flow &);

      uint64_t getNumberOfFlows () const
      {
        return m_total;
      }

      void clear ();
      void store ();

    protected:
      typedef hash_map<ip_port_entry, flow_data, ip_port_hash, ip_port_eq> flow_map_t;
      typedef std::pair<uint16_t, uint16_t> portpair;

      void addFlow (const flow &);

      flow_map_t m_flowMap;

      int m_aggTime;
      netflow_writer *m_writer;
      snmp_interfaces *m_snmp;
      uint64_t m_total;
      mutex m_mutex;
  };
}

#endif // __STORAGE__
