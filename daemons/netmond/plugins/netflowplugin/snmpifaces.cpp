#include "snmpifaces.h"
#include "snmp.h"
#include "exception.h"
#include "util.h"

#include "../snmp/snmpmond/snmpcheckfunctor.h"

#include <cstdlib>

namespace netmon
{
  Snmp_pp::Oid snmp_interfaces::m_iFaces ("1.3.6.1.2.1.2.2.1.1");

  snmp_interfaces::snmp_interfaces ()
  {
    m_snmp = new snmp;
    m_snmp->setPort (SNMPPort);
  }

  snmp_interfaces::~snmp_interfaces ()
  {
    delete m_snmp;
  }

  void snmp_interfaces::createSNMPMapping ()
  {
    ifacesfunctor ifaces;

    if (!m_snmp->walkSNMP ("127.0.0.1", "public", m_iFaces, ifaces))
      throw netmon_exception ("Cannot find SNMP interfaces");

    for (std::list<std::string>::iterator it = ifaces.getInterfaces().begin(); it != ifaces.getInterfaces().end(); ++it)
      getName (*it);
  }

  int snmp_interfaces::getSNMPIfIndex (int devID)
  {
    std::string dev = devToString (devID);
    iface_map_t::const_iterator i = m_map.find (dev);

    if (i == m_map.end())
      return 0;
    return i->second;
  }

  void snmp_interfaces::getName (const std::string &iface)
  {
    std::string ifName;
    std::string oid = "1.3.6.1.2.1.31.1.1.1.1." + iface;
    if (!m_snmp->snmpGet ("127.0.0.1", "public", oid.c_str(), ifName))
      return;
    m_map[ifName] = atoi (iface.c_str());
  }
}
