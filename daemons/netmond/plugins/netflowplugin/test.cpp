#include "udpsender.h"

#include <cstdio>

int main (int argc, char **argv)
{
  using namespace netmon;
  if (argc != 2)
  {
    printf ("usage: %s ip_list\n", argv[0]);
    return -1;
  }
  try
  {
    udp_sender *p = new udp_sender (argv[1]);
    p->sendData ((uint8_t *) "123", 3);
    sleep (1);
    p->sendData ((uint8_t *) "4567", 4);
    delete p;
  }
  catch (netmon_exception &e)
  {
    printf ("exception: %s\n", e.what());
  }
}
