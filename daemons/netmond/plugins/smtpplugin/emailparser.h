#ifndef __EMAIL_PARSER__
#define __EMAIL_PARSER__

#include "lineparser.h"
#include "log.h"

#include <list>
#include <string>

namespace netmon
{
  struct attachment_data
  {
    attachment_data ()
      : m_isRealAttach (false)
    {}
    bool m_isRealAttach;
    std::string m_name;
    std::string m_contentType;
    uint32_t m_size;
  };

  struct email_address
  {
    std::string m_user;
    std::string m_domain;
    std::string m_label;
  };

  struct email_data
  {
    enum { sHeaders, sBody, sAttachHeader, sAttach, sDone };
    enum { hUnknown, hTo, hCC, hCT, hSubject, hFrom, hID };

    email_data ()
      : m_header (hUnknown), m_hasAttach (false), m_eState (sHeaders),
        m_size (0), m_timestamp (time (0))
    { 
       m_contentType = std::string("");
       m_from.m_user = "";
       m_from.m_domain = "";
       m_from.m_label = "";
    }

    uint8_t m_header;
    bool m_hasAttach;
    uint8_t m_eState;
    attachment_data m_currAttach;

    uint32_t m_size;
    uint32_t m_timestamp;
    email_address m_from;
    std::string m_subject;
    std::string m_id;
    std::list<email_address> m_to;
    std::list<email_address> m_cc;
    std::string m_contentType;
    std::string m_boundary;
    std::list<std::string> m_rcpts;
    std::list<std::string> m_headers;
    std::list<attachment_data> m_attachments;
  };

  class email_parser
  {
    public:
      email_parser (line_parser &parser)
        : m_parser (parser)
      {}

      void handleHeaders (parser_state *, email_data *);
      void handleBody (parser_state *, email_data *, bool imap=false);

    protected:
      void handleHeader (email_data *, const std::string &);
      void handleMultiLineHeader (email_data *, const std::string &);
      std::string getHeaderValue (const std::string &);
      void parseAddress (const std::string &, email_address &);
      void parseAddressList (const std::list<std::string> &, std::list<email_address> &);

      line_parser m_parser;
  };
}

#endif // __EMAIL_PARSER__
