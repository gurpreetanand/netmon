#include "test.h"
#include "db.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <cstring>

namespace netmon
{
  test::test (const char *file)
    : smtp_plugin (0)
  {
    m_data = new smtp_plugin::smtp_data (0, 0);
    m_fd = open (file, O_RDONLY);

    if (m_fd == 0)
    {
      printf ("Cannot open '%s'\n", file);
      exit (-1);
    }
  }

  test::~test ()
  {
    close (m_fd);
//    delete m_data;
  }

  void test::run ()
  {
    const char *from = "MAIL FROM: test@test.com\r\n";
    const char *rcpt = "RCPT TO: test@test.com\r\n";
    const char *data = "DATA\r\n";
    const char *quit = "QUIT\r\n";

    newData ((uint8_t *) from, strlen (from));
    newData ((uint8_t *) rcpt, strlen (rcpt));
    newData ((uint8_t *) data, strlen (data));

    uint8_t buff[512];
    uint16_t r = 0;

    while (true)
    {
      if ((r = read (m_fd, (void *) buff, sizeof (buff))) == 0)
        break;
      newData (buff, r);
    }

    newData ((uint8_t *) quit, strlen (quit));

    dump();
    dumpToDB();
  }

  void test::newData (const uint8_t *b, uint16_t size)
  {
    parseData (m_data, b, size, true);
  }

  void test::dumpToDB ()
  {
    db mydb;
    try
    {
      mydb.connect ("netmon35", "root");
      onTimer (0, &mydb);
    }
    catch (db_exception &e)
    {
      printf ("error: %s\n", e.what());
    }
  }

  void test::dump ()
  {
    printf ("size: %d\n", m_data->m_size);
    printf ("from_u: '%s'\n", m_data->m_from.m_user.c_str());
    printf ("from_d: '%s'\n", m_data->m_from.m_domain.c_str());
    printf ("from_l: '%s'\n", m_data->m_from.m_label.c_str());
    printf ("subject: '%s'\n", m_data->m_subject.c_str());
    printf ("ct: '%s'\n", m_data->m_contentType.c_str());
    printf ("boundary: '%s'\n", m_data->m_boundary.c_str());

    for (std::list<email_address>::iterator i = m_data->m_to.begin(); i != m_data->m_to.end(); ++i)
    {
      printf ("to_u: '%s' ", (*i).m_user.c_str());
      printf ("to_d: '%s' ", (*i).m_domain.c_str());
      printf ("to_l: '%s'\n", (*i).m_label.c_str());
    }

    for (std::list<email_address>::iterator i = m_data->m_cc.begin(); i != m_data->m_cc.end(); ++i)
    {
      printf ("cc_u: '%s' ", (*i).m_user.c_str());
      printf ("cc_d: '%s' ", (*i).m_domain.c_str());
      printf ("cc_l: '%s'\n", (*i).m_label.c_str());
    }

    if (m_data->m_hasAttach)
    {
      printf ("Attachments:\n");
      for (std::list<attachment_data>::iterator i = m_data->m_attachments.begin(); i != m_data->m_attachments.end(); ++i)
        printf ("  ct: '%s' size: %d\n", (*i).m_contentType.c_str(), (*i).m_size);
    }
  }
}

int main(int argc, char **argv)
{
  using namespace netmon;
  if (argc != 2)
  {
    printf ("Usage: test <file_name>\n");
    return -1;
  }

  test t (argv[1]);
  t.run();
}
