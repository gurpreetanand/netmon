#ifndef __EMAIL_PLUGIN__
#define __EMAIL_PLUGIN__

#include "emailparser.h"

#include <list>
#include <string>

namespace netmon
{
  class db;
  class email_plugin
  {
    protected:
      void commit (uint32_t, uint32_t, const std::string &, email_data *, const char *, db *);
      void commitAttach (email_data *, db *);
      void commitAddress (const email_address &, const char *, db *);
      void commitAddresses (const std::list<email_address> &, const char *, db *);
      int insert_id;
  };
}

#endif // __EMAIL_PLUGIN__
