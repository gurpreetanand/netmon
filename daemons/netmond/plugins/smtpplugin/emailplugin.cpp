#include "emailplugin.h"
#include "db.h"
#include "log.h"
#include "strings.h"

#include <dumbnet.h>
#include <stdlib.h>
#include <sstream>

namespace netmon
{
  void email_plugin::commit (uint32_t srv_ip, uint32_t cln_ip, const std::string &user, email_data *data, const char *protocol, db *mydb)
  {
    static std::ostringstream sql;
    std::ostringstream fake_id;

    sql.str("");
    sql <<
    "INSERT INTO EMAIL (server_ip, client_ip, username, size, content_type, "
    "\"timestamp\", subject, headers, protocol, message_id) VALUES (";

    in_addr addr;

    addr.s_addr = srv_ip;
    sql << "'" << inet_ntoa (addr) << "', '"; // server_ip

    addr.s_addr = cln_ip;
    sql << inet_ntoa (addr) << "', "; // client_ip

    char *esc;
    if (user.size() == 0) {
      sql << "NULL, "; // username
    } else {
      esc = mydb->escapeString (user.c_str(), user.size());
      sql << "'" << esc << "', "; // username 
      delete[] esc;
    }

    sql << data->m_size << ", '"; // size

    if (data->m_contentType.empty()) {
       sql << "text/plain;', "; // IF content-type not set, text/plain
    } else {
    esc = mydb->escapeString (data->m_contentType.c_str(), data->m_contentType.size());
    sql << esc << "', "; // content_type
    delete[] esc;
    }

    sql << data->m_timestamp << ", '";

    esc = mydb->escapeString (data->m_subject.c_str(), data->m_subject.size());
    sql << esc << "', '"; // subject
    delete[] esc;

    std::string tmp = createText (data->m_headers);
    esc = mydb->escapeString (tmp.c_str(), tmp.size());
    sql << esc << "', '" << protocol << "', '";
    delete[] esc;

    if (data->m_id.empty()) {
      fake_id.str("");
      fake_id << time(0) << "-" << srv_ip;
      data->m_id = fake_id.str();
    }
      
    esc = mydb->escapeString (data->m_id.c_str(), data->m_id.size());
    sql << esc << "')"; // message id
    delete[] esc;

    try
    {
      mydb->execSQL (sql.str().c_str());
      PGresult *res = mydb->execQuery("select currval('email_id_seq')");
      insert_id = atoi(PQgetvalue(res, 0, 0));
      commitAddresses (data->m_to, "to", mydb);
      commitAddresses (data->m_cc, "cc", mydb);
      commitAddress (data->m_from, "from", mydb);
    }
    catch (netmon::db_exception &e)
    {
      log::instance()->add(netmon::_eLogError, e.what());
    }
  }

  void email_plugin::commitAddress (const email_address &e, const char *type, db *mydb)
  {
    static std::ostringstream sql, sql2;
    char *esc;
    int address_id;

    sql.str("");
    sql2.str("");
    sql << "SELECT get_or_create_email_id('";

    esc = mydb->escapeString (e.m_user.c_str(), e.m_user.size());
    sql << esc << "', '"; // user
    delete[] esc;

    esc = mydb->escapeString (e.m_domain.c_str(), e.m_domain.size());
    sql << esc << "', '"; // domain
    delete[] esc;

    esc = mydb->escapeString (e.m_label.c_str(), e.m_label.size());
    sql << esc << "')"; // label
    delete[] esc;

    try
    {
      PGresult *res = mydb->execQuery (sql.str().c_str());
      address_id = atoi(PQgetvalue (res, 0, 0));
    }
    catch (netmon_exception &ex)
    {
      log::instance()->add(netmon::_eLogError, ex.what());
    }


    sql2 << "INSERT INTO email2address (email_id, address_id, email_field) VALUES ("
         << insert_id << ", " << address_id << ", '" << type << "')"; 
    try
    {
      mydb->execSQL (sql2.str().c_str());
    }
    catch (netmon_exception &ex)
    {
      log::instance()->add(netmon::_eLogError, ex.what());
    }

  }

  void email_plugin::commitAddresses (const std::list<email_address> &list, const char *type, db *mydb)
  {
    for (std::list<email_address>::const_iterator i = list.begin(); i != list.end(); ++i)
      commitAddress (*i, type, mydb);
  }

  void email_plugin::commitAttach (email_data *data, db *mydb)
  {
    static std::ostringstream sql;

    std::list<attachment_data>::iterator end = data->m_attachments.end();

    for (std::list<attachment_data>::iterator i = data->m_attachments.begin(); i != end; ++i)
    {
      sql.str("");
      sql << "INSERT INTO email_attachments (size, content_type, email_id,"
      " filename) VALUES (";
      sql << (*i).m_size << ", '" << (*i).m_contentType 
          << "', (select currval('email_id_seq')), '";

      char *esc = mydb->escapeString ((*i).m_name.c_str(), (*i).m_name.size());
      sql << esc << "')";
      delete[] esc;

      try
      {
        mydb->execSQL (sql.str().c_str());
      }
      catch (netmon_exception &e)
      {
        log::instance()->add(netmon::_eLogError, e.what());
      }
    }
  }
}
