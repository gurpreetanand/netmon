#include "strings.h"
#include "db.h"

#include <sstream>

namespace netmon
{
  void tokenize (const std::string &str, std::list<std::string> &tokens)
  {
    char p = '\0';
    bool q = false;
    std::string::size_type pos = 0;

    for (std::string::size_type i = 0; i < str.size(); ++i)
    {
      if (str[i] == '"' && p != '\\')
        q = !q;
      if (str[i] == ',' && !q)
      {
        tokens.push_back (std::string (str, pos, i - pos));
        if (i <= str.size() - 1)
          pos = i + 1;
      }
      p = str[i];
    }
    if (pos == 0)
      tokens.push_back (str);
    else
      if (pos != 0 && pos <= str.size() - 1)
        tokens.push_back (std::string (str, pos, str.size() - pos));
  }

  void trim (std::string &str)
  {
    size_t pos = str.find_first_not_of (" \t\r\n");
    if (pos != std::string::npos)
      str.erase (0, pos);
    pos = str.find_last_not_of (" \t\r\n");
    if (pos != std::string::npos)
      str.erase (pos + 1, str.size());
  }

  void trimQuotes (std::string &str)
  {
    if (str.size() < 3)
      return;
    if ((str[0] == '\'' && str[str.size() - 1] == '\'') ||
        (str[0] == '"' && str[str.size() - 1] == '"'))
    {
      str.erase (str.size() - 1);
      str.erase (0, 1);
    }
  }

  void trim (std::list<std::string> &list)
  {
    std::list<std::string>::iterator end = list.end();
    for (std::list<std::string>::iterator i = list.begin(); i != end; ++i)
      trim (*i);
  }

  std::string createPGArray (const std::list<std::string> &list, db *mydb)
  {
    std::list<std::string>::const_iterator end = list.end();
    std::ostringstream ret;
    bool first = true;

    ret << "{";

    for (std::list<std::string>::const_iterator i = list.begin(); i != end; ++i)
    {
      char *esc = mydb->escapeString ((*i).c_str(), (*i).size());
      if (first)
        first = false;
      else
        ret << ", ";
      ret << "\"";

      char *p = esc;
      while (*p != '\0')
      {
        if (*p != '"')
          ret << *p;
        ++p;
      }

      ret << "\"";
      delete[] esc;
    }

    ret << "}";

    return ret.str();
  }

  std::string createText (const std::list<std::string> &list)
  {
    std::list<std::string>::const_iterator end = list.end();
    std::ostringstream ret;
    //bool first = true;

    for (std::list<std::string>::const_iterator i = list.begin(); i != end; ++i)
    {
    /***************** Shouldn't need these linebreaks*************************
      if (first)
        first = false;
      else
        ret << "\n";
    */
      ret << *i;
    }

    return ret.str();
  }
}

#ifdef __TEST__

#include <cstdio>

void testArray()
{
  std::list<std::string> list;
  list.push_back ("abc");
  list.push_back ("def");
  list.push_back ("ghi");
  std::string str = netmon::createPGArray (list);
  printf ("%s\n", str.c_str());
}

int main()
{
  testArray();
  std::list<std::string> list;
  list.push_back (" e 1   ");
  list.push_back ("e 2");
  list.push_back ("     e 3");
  netmon::trim (list);
  for (std::list<std::string>::iterator i = list.begin(); i != list.end(); ++i)
    printf ("'%s'\n", (*i).c_str());
}

#endif // __TEST__
