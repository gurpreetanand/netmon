#ifndef __PROTOCOL_ALERT_MANAGER__
#define __PROTOCOL_ALERT_MANAGER__

#include "alertmanager.h"

#include <list>
#include <map>
#include <set>
#include <string>
#include <stdint.h>

namespace netmon
{
  class connitem;
  class alerthandler;

  class protocol_alert_manager : public alert_manager
  {
    public:
      protocol_alert_manager (db *mydb)
        : alert_manager (mydb, false)
      {}

      virtual ~protocol_alert_manager ()
      {}

      virtual void init ();
      void checkItem (const connitem &);
      void dispatchAlerts ();

      using alert_manager::getAlerts;

    protected:
      virtual void loadStaticData ();

      virtual void getReferenceTable (std::string &table)
      {
        table = "protocols";
      }

      virtual void getOverName (std::string &name)
      {
        name = "PORT_PROTOCOL_TRAFFIC";
      }

      virtual void getBelowName (std::string &name)
      {
      }

      void createHelperQuery ();
      void updateActivity (unsigned int, uint32_t, uint32_t);

      typedef std::map<unsigned int, unsigned int> map_t;
      typedef std::set<std::pair<uint32_t, uint32_t> > set_t;
      typedef std::map<unsigned int, set_t> connmap_t;

      std::string formatConnections (unsigned int, set_t &);

      map_t m_protocolMap; // protocol -> id
      map_t m_idMap; // id -> protocol
      connmap_t m_connectionsMap;
      std::string m_helperQuery;
  };
}

#endif // __PROTOCOL_ALERT_MANAGER__
