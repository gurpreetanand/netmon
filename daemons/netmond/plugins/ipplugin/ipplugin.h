#ifndef __IP_PLUGIN__
#define __IP_PLUGIN__

#include "plugin.h"
#include "timer.h"

namespace netmon
{
  class netmonstorage;

  class ip_plugin : public plugin
  {
    public:
      ip_plugin (unsigned int id)
        : plugin (id), m_wantedDevs (0)
      {}
      ~ip_plugin ();

      virtual void init (db *);
      virtual unsigned int getWantedDevice ()
      {
        return m_wantedDevs;
      }

      virtual unsigned int getWantedProtocol ()
      {
        return m_wantedProtocol;
      }

      virtual void getTimers (std::vector<timer *> &vec)
      {
        vec.push_back (new timer (m_commitInt, 0, m_id));
        vec.push_back (new full_span_timer (m_commitInt1min, 1, m_id));
      }

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0);
      virtual void onTimer (const timer *, db *);

      virtual const char *getName ()
      {
        return "mod_ip";
      }

    protected:
      netmonstorage *m_cache;
      unsigned int m_wantedDevs;
      unsigned int m_wantedProtocol;
      static const int m_commitInt = 20;
      static const int m_commitInt1min = 60;
  };
}

#endif // __IP_PLUGIN__
