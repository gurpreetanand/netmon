#include "storage.h"
#include "lock.h"

#include <cstdio>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>

namespace netmon
{
  storage::storage ()
  {
    m_total = 0;
  }

  storage::~storage ()
  {
  }

  void storage::add (const connitem &item)
  {
    mutex_lock lock (m_mutex);

    addConversationDetailed (item, m_convDMap);
    addConversationDetailed (item, m_convDMapLive);

    addPorts (item);

    addConversation (item.m_srcIP, item.m_dstIP, item.m_octets, m_convMapLive);

    m_total += item.m_octets;
  }

  uint64_t storage::getTotalBytes () const
  {
    return m_total;
  }

  void storage::getTopConversations (toplist<convitem, comp<convitem> > &tlist)
  {
    for (conversationmap::iterator i = m_convMapLive.begin(); i != m_convMapLive.end(); ++i)
      for (ipbandwmap::iterator j = i->second.begin(); j != i->second.end(); ++j)
        tlist.insert (convitem (i->first, j->first, j->second));
  }

  void storage::getTopDetailConversations (toplist<connitem, comp<connitem> > &tlist)
  {
    cdmap::iterator i;

    for (i = m_convDMapLive.begin(); i != m_convDMapLive.end(); ++i)
    {
      connitem item;
      item.m_proto = i->second.m_proto;
      item.m_octets = i->second.m_octets;
      item.m_srcIP = i->first.m_src;
      item.m_dstIP = i->first.m_dst;
      item.m_srcport = i->first.m_srcPort;
      item.m_dstport = i->first.m_dstPort;
      item.m_flags = 0;
      tlist.insert (item);
    }
  }

  void storage::getTopProtocols (toplist<portitem, comp<portitem> > &tlist)
  {
    getTopProtocols (tlist, m_portMap);
  }

  void storage::getTopLiveProtocols (toplist<portitem, comp<portitem> > &tlist)
  {
    getTopProtocols (tlist, m_portMapLive);
  }

  void storage::getTopProtocols (toplist<portitem, comp<portitem> > &tlist, const portmap &map)
  {
    for (portmap::const_iterator it = map.begin(); it != map.end(); ++it)
      tlist.insert (portitem (it->first, it->second));
  }

  void storage::clear ()
  {
    mutex_lock lock (m_mutex);
    m_portMap.clear();
  }

  void storage::clearLive ()
  {
    mutex_lock lock (m_mutex);
    m_convMapLive.clear();
    m_convDMapLive.clear();
    m_portMapLive.clear();
    m_total = 0;
  }

  void storage::addConversationDetailed (const connitem &item, cdmap &map)
  {
    ip_port_entry e(item.m_srcIP, item.m_dstIP, item.m_srcport, item.m_dstport);
    cdmap::iterator i = map.find (e);
    if (i == map.end())
    {
      map[e] = conndata(item.m_octets, item.m_proto, ::time(0), ::time(0), item.m_flags);;
      return;
    }
    i->second.m_octets += item.m_octets;
    i->second.m_packets += 1;
    i->second.m_endTime = ::time(0);
    i->second.checkFlags (item.m_flags);
  }

  void storage::addIP (uint32_t IP, uint64_t size, ipbandwmap &map)
  {
    ipbandwmap::iterator it = map.find (IP);
    if (it == map.end())
      map[IP] = size;
    else
      it->second += size;
  }

  void storage::addConversation (uint32_t src, uint32_t dst, uint64_t bytes, conversationmap &map)
  {
    conversationmap::iterator it = map.find (src);
    if (it == map.end())
      map[src][dst] = bytes;
    else
    {
      ipbandwmap::iterator it2 = it->second.find (dst);
      if (it2 == it->second.end())
        it->second[dst] = bytes;
      else
        it->second[dst] += bytes;
    }
  }

  void storage::addPorts (const connitem &item)
  {
    if (item.m_octets == 0)
      return;

    uint16_t port = item.m_srcport;
    if (item.m_dstport < port)
      port = item.m_dstport;

    addPort (port, item.m_octets, m_portMap);
    addPort (port, item.m_octets, m_portMapLive);
  }

  void storage::addPort (uint16_t port, uint64_t octets, portmap &map)
  {
    if (map.find (port) == map.end())
      map[port] = octets;
    else
      map[port] += octets;
  }
}
