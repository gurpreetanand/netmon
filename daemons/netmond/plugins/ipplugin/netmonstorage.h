#ifndef __NETMON_STORAGE__
#define __NETMON_STORAGE__

#include "storage.h"
#include "semaphore.h"
#include "semaphore.h"
#include "shm.h"

#include <list>
#include <set>
#include <stdint.h>
#include <mqueue.h>

namespace netmon
{
  struct connitem_detail : public connitem
  {
    unsigned int m_packets;
    unsigned int m_startTime;
    unsigned int m_endTime;
  };

  class db;
  class protocol_alert_manager;

  class netmonstorage : public storage
  {
    public:
      netmonstorage (int = 10, int = 200, int = 10);
      ~netmonstorage ();

      void init (db *);

      virtual void store (int, db *);
      virtual void storeLive (int);

    protected:
      void initshm ();
      void storeConv (int);
      void storeStrm (int);
      void storeProt (int);
      void enqueueIPs ();
      void store (const connitem_detail &, int, db *);
      void storeProtocols (db *);
      void createArrays (const toplist<portitem, comp<portitem> > &, std::string &, std::string &);

      std::set<uint32_t> m_ips;
      protocol_alert_manager *m_alertManager;
      shm *m_shmconv;
      shm *m_shmstrm;
      shm *m_shmProtocols;
      semaphore m_sem;
      mqd_t m_queue;
      int m_maxConv;
      int m_maxStrm;
      int m_maxProt;
  };
}

#endif // __NETMON_STORAGE__
