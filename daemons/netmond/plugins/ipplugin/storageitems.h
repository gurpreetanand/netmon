#ifndef __STORAGE_ITEMS__
#define __STORAGE_ITEMS__

#include <stdint.h>

namespace netmon
{
  struct storageitem
  {
    virtual ~storageitem () {}
    virtual int serialize (char *, int) = 0;
  };

  struct connitem : public storageitem
  {
    virtual int serialize (char *, int);
    uint8_t m_proto;
    uint32_t m_srcIP;
    uint32_t m_dstIP;
    uint16_t m_srcport;
    uint16_t m_dstport;
    uint64_t m_octets;
    uint8_t m_flags;
  };

  struct portitem : public storageitem
  {
    portitem (uint16_t port, uint64_t bytes)
      : m_port (port), m_octets (bytes)
    {}
    virtual int serialize (char *, int);
    uint16_t m_port;
    uint64_t m_octets;
  };

  struct convitem : public storageitem
  {
    convitem (uint32_t src, uint32_t dst, uint64_t bytes)
      : m_src (src), m_dst (dst), m_octets (bytes)
    {}
    virtual int serialize (char *, int);
    uint32_t m_src;
    uint32_t m_dst;
    uint64_t m_octets;
  };

  struct ipitem : public storageitem
  {
    ipitem (uint32_t ip, uint64_t bytes)
      : m_ip (ip), m_octets (bytes)
    {}
    virtual int serialize (char *, int);
    uint32_t m_ip;
    uint64_t m_octets;
  };

  template <typename T>
  struct comp
  {
    bool operator() (const T &a, const T &b)
    {
      return a.m_octets > b.m_octets;
    }
  };
}

#endif // __STORAGE_ITEMS__
