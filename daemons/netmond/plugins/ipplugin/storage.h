#ifndef __STORAGE__
#define __STORAGE__

#include "storageitems.h"
#include "mutex.h"
#include "../common/iphashmap.h"
#include "../common/toplist.h"

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

#include <stdint.h>

namespace netmon
{
  struct conndata
  {
    conndata ()
      : m_octets (0), m_proto (0), m_startTime (0), m_endTime (0), m_packets (0), m_done (false)
    {}
    conndata (uint64_t bytes, uint8_t proto, unsigned int start, unsigned int end, uint8_t flags)
      : m_octets (bytes), m_proto (proto), m_startTime (start), m_endTime (end), m_packets (1), m_done (false)
    {
      checkFlags (flags);
    }

    void checkFlags (uint8_t flags)
    {
      if ((flags & 0x01) || (flags & 0x04)) // FIN or RST set?
      {
        m_done = true;
//        printf ("tcp flags: 0x%02x (proto: 0x%02x)\n", flags, m_proto);
      }
    }

    uint64_t m_octets;
    uint8_t m_proto;
    unsigned int m_startTime;
    unsigned int m_endTime;
    unsigned int m_packets;
    bool m_done;
  };

  class storage
  {
    public:
      storage ();
      virtual ~storage ();

      void add (const connitem &);

      uint64_t getTotalBytes () const;

      void getTopConversations (toplist<convitem, comp<convitem> > &);
      void getTopDetailConversations (toplist<connitem, comp<connitem> > &);

      void getTopProtocols (toplist<portitem, comp<portitem> > &);
      void getTopLiveProtocols (toplist<portitem, comp<portitem> > &);

      void clear ();
      void clearLive ();

      virtual void store (int) {}
      virtual void storeLive (int) {}

    protected:
      typedef hash_map<ip_port_entry, conndata, ip_port_hash, ip_port_eq> cdmap;
      typedef hash_map<uint32_t, uint64_t> ipbandwmap;
      typedef hash_map<uint32_t, ipbandwmap> conversationmap;
      typedef hash_map<uint16_t, uint64_t> portmap;
      typedef std::pair<uint16_t, uint16_t> portpair;

      void addConversationDetailed (const connitem &, cdmap &);
      void addIP (uint32_t, uint64_t, ipbandwmap &);
      void addConversation (uint32_t, uint32_t, uint64_t, conversationmap &);
      void addPorts (const connitem &);
      void addPort (uint16_t, uint64_t, portmap &);
      void getTopProtocols (toplist<portitem, comp<portitem> > &, const portmap &);

      conversationmap m_convMapLive;

      cdmap m_convDMap;
      cdmap m_convDMapLive;

      portmap m_portMap;
      portmap m_portMapLive;

      uint64_t m_total;
      mutex m_mutex;
  };
}

#endif // __STORAGE__
