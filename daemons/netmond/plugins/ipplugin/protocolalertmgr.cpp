#include "protocolalertmgr.h"
#include "storage.h"
#include "alerts.h"
#include "db.h"

#include <sstream>
#include <arpa/inet.h>

namespace netmon
{
  void protocol_alert_manager::init ()
  {
    createHelperQuery();
    alert_manager::init();
  }

  void protocol_alert_manager::checkItem (const connitem &item)
  {
    unsigned int key = (item.m_srcport << 16) | item.m_proto;
    map_t::iterator i = m_protocolMap.find(key);
    if (i != m_protocolMap.end())
      updateActivity (i->second, item.m_srcIP, item.m_dstIP);

    key = (item.m_dstport << 16) | item.m_proto;
    i = m_protocolMap.find(key);
    if (i != m_protocolMap.end())
      updateActivity (i->second, item.m_srcIP, item.m_dstIP);
  }

  void protocol_alert_manager::dispatchAlerts ()
  {
    for (connmap_t::iterator i = m_connectionsMap.begin(); i != m_connectionsMap.end(); ++i)
    {
      std::string str = formatConnections (i->first, i->second);
      values_map vmap;
      vmap[alerthandler::eConnectionList] = keyword_value (str);
      getAlerts (i->first, vmap);
    }
  }

  std::string protocol_alert_manager::formatConnections (unsigned int id, set_t &connections)
  {
    static std::ostringstream s;

    s.str("");
    map_t::iterator j = m_idMap.find(id);
    if (j == m_idMap.end())
      return "";

    unsigned short port = (unsigned short) (j->second >> 16);
    unsigned char proto = j->second & 0xff;
    in_addr addr;

    for (set_t::iterator i = connections.begin(); i != connections.end(); ++i)
    {
      addr.s_addr = (*i).first;
      s << "Source: "
        << inet_ntoa (addr);

      addr.s_addr = (*i).second;
      s << " Destination: "
        << inet_ntoa (addr)
        << " Port: "
        << port;
      if (proto == 0x06)
        s << "/TCP";
      else
        s << "/UDP";
      s << std::endl;
    }

    return s.str();
  }

  void protocol_alert_manager::updateActivity (unsigned int id, uint32_t src, uint32_t dst)
  {
    m_connectionsMap[id].insert (std::make_pair (src, dst));
  }

  void protocol_alert_manager::loadStaticData ()
  {
    alert_manager::loadStaticData();

    m_idMap.clear();
    m_connectionsMap.clear();

    PGresult *res = m_db->execQuery (m_helperQuery.c_str());

    for (int i = 0; i < PQntuples(res); ++i)
    {
      unsigned int id = atoi (PQgetvalue (res, i, 0));
      unsigned short port = (unsigned short) atoi (PQgetvalue (res, i, 1));
      unsigned char protocol = 0;
      if (strcmp (PQgetvalue (res, i, 2), "TCP") == 0)
        protocol = 0x06;
      if (strcmp (PQgetvalue (res, i, 2), "UDP") == 0)
        protocol = 0x11;
      unsigned int key = (port << 16) | protocol;
      m_protocolMap[key] = id;
      m_idMap[id] = key;
    }

    PQclear(res);
  }

  void protocol_alert_manager::createHelperQuery ()
  {
    std::ostringstream sql;
    std::string table;

    getReferenceTable (table);
    sql << "select c.id, c.port, c.protocol from protocols c, alert_triggers b "
        << "where b.reference_table_name = '"
        << table
        << "' and b.active = 't' and c.id = b.reference_pkey_val";

    m_helperQuery = sql.str();
  }
}
