#include "statsplugin.h"
#include "netmon.h"

namespace netmon
{
  void stats_plugin::onTimer (const timer *t, db *)
  {
    for (stat_map::iterator i = m_map.begin(); i != m_map.end(); ++i)
    {
      printf ("device: %d packets: %" u64prefix "u octets: %" u64prefix "u\n", i->first, i->second.m_packets, i->second.m_octets);
      i->second.m_packets = 0;
      i->second.m_octets = 0;
    }
  }

  void stats_plugin::onData (const uint8_t *data, uint16_t size, uint8_t dev, ip_port_entry *)
  {
    m_map[dev].m_packets++;
    m_map[dev].m_octets += size;
  }
}

extern "C" netmon::plugin *create (int id)
{
  return new netmon::stats_plugin(id);
}

extern "C" void destroy (netmon::plugin *p)
{
  delete p;
}
