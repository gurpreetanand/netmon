#ifndef __MUTEX_LOCK__
#define __MUTEX_LOCK__

#include "mutex.h"

namespace netmon
{
  class mutex_lock
  {
    public:
      mutex_lock (mutex &lock)
        : m_lock (lock), m_is_owner (false)
      {
        m_lock.acquire();
        m_is_owner = true;
      }

      ~mutex_lock ()
      {
        if (m_is_owner)
          m_lock.release();
      }

      void unlock ()
      {
        m_lock.release();
        m_is_owner = false;
      }

    private:
      mutex &m_lock;
      bool m_is_owner;
  };
}

#endif //__MUTEX_LOCK__
