#ifndef __EXECUTOR__
#define __EXECUTOR__

#include <stdint.h>
#include <cstring>

#include "thread.h"
#include "queue.h"
#include "../lib/netmon.h"

namespace netmon
{
  class timer;

  typedef queue<timer *> task_queue;

  class plugin_manager;
  class db;
  struct plugin_data;

  class executor : public thread
  {
    public:
      executor (task_queue *q, plugin_manager *p, int id)
        : thread(), m_queue (q), m_pluginManager (p), m_id(id), m_run (true)
      {
        connectToDB();
      }

      ~executor()
      {
        m_db->close();
        delete m_db;
      }

      void connectToDB ()
      {
        char dbname[DBNAMELEN];
        char dbuser[DBUSERLEN];

        readConfig (dbname, dbuser);
        m_db = new db;
        m_db->connect (dbname, dbuser);
      }

      void stop ()
      {
        m_run = false;
      }

    protected:
      virtual void run ();
      void handleTimer (timer *);

      task_queue *m_queue;
      plugin_manager *m_pluginManager;
      db *m_db;
      int m_id;
      mutable bool m_run;
  };
}

#endif // __EXECUTOR__
