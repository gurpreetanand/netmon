#ifndef __SEMAPHORE__
#define __SEMAPHORE__

#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <string>

#include "exception.h"

namespace netmon
{
  class semaphore_exception : public netmon_exception
  {
    public:
      semaphore_exception (const std::string &err)
        : netmon_exception (err)
      {}
  };

  class semaphore
  {
    private:
      union semun
      {
        int val;
        struct semid_ds *buf;
        unsigned short int *array;
        struct seminfo *__buf;
      };

    public:
      semaphore (int);
      semaphore (key_t, int);
      ~semaphore ();

      void wait (int = 0, int = -1);
      void post (int = 0, int = 1);

      int getValue ();

    protected:
      int initSemaphore ();
      void semop (int, int);
      int m_id;
  };
}

#endif // __SEMAPHORE__
