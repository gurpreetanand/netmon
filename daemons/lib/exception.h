#ifndef __NETMON_EXCEPTION__
#define __NETMON_EXCEPTION__

#include <string>
#include <stdexcept>

namespace netmon
{
  class netmon_exception : public std::exception
  {
    public:
      netmon_exception (const std::string &err)
        : std::exception (), m_err (err)
      {
      }

      virtual ~netmon_exception () throw () {}
      virtual const char *what() const throw ()
      {
        return m_err.c_str();
      }

    protected:
      std::string m_err;
  };
}

#endif // __NETMON_EXCEPTION__
