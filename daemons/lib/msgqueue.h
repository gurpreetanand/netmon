#ifndef __MESSAGE_QUEUE__
#define __MESSAGE_QUEUE__

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

namespace netmon
{
  struct msg
  {
    long m_type;
  };

  struct ipmsg : public msg
  {
    unsigned long m_ip;
  };

  class msgqueue
  {
    public:
      msgqueue ();
      msgqueue &create (key_t, int);
      msgqueue &access (key_t);
      msgqueue &dispose ();
      msgqueue &destroy ();
      msqid_ds &stat (msqid_ds &);
      msgqueue &set (msqid_ds &);
      bool send (msg &, size_t, int = 0);
      bool recv (msg &, size_t &, size_t, long, int = 0);
      int getError () const { return m_error; }
      key_t getKey () const { return m_key; }

    private:
      enum state { _eReady, _eNotReady };
      key_t m_key;
      int m_error;
      int m_id;

    protected:
      void verify (state);
  };
}

#endif // __MESSAGE_QUEUE__
