#include "memcache.h"

namespace netmon {
  Memcacher::Memcacher() {
    memcached_return rc;
    memc_ = memcached_create(NULL);
    servers_ = memcached_server_list_append(NULL, "127.0.0.1", 11211, &rc);
    rc = memcached_server_push(memc_, servers_);
    active_ = (rc == MEMCACHED_SUCCESS);
  }

  bool Memcacher::set(std::string key, std::string value, int expiry) {
    memcached_return rc;
    rc = memcached_set(
      memc_,
      key.c_str(),
      key.size(),
      value.c_str(),
      value.size(),
      (time_t) expiry,
      (uint32_t) 0
    );
    return (rc == MEMCACHED_SUCCESS);
  }

  std::string Memcacher::get(std::string key) {
    memcached_return rc;
    size_t value_length;
    uint32_t flags;
    std::string sret = "";

    char* ret = memcached_get(
      memc_,
      key.c_str(),
      key.size(),
      &value_length,
      &flags,
      &rc
    );

    if (rc == MEMCACHED_SUCCESS) {
      sret = std::string(ret);
      free(ret);
    }

    return sret;

  }
}
