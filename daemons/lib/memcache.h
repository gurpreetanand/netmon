#ifndef MEMCACHE_H
#define MEMCACHE_H

#include <string>
#include <libmemcached/memcached.h>

#define KEY_SEPARATOR ":"

namespace netmon {
  class Memcacher {
    public:
      Memcacher();
      bool set(std::string key, std::string value, int expiry = 0);
      std::string get(std::string key);

    private:
      bool active_;
      memcached_server_st *servers_;
      memcached_st *memc_;
  };
}

#endif
