#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <errno.h>
#include <syslog.h>

#include "exception.h"
#include "netmon.h"

static const char *CONFFILE = "/etc/netmon.conf";

int getentry (char *dest, const char *variable)
{
  FILE *fp;
  char line[128];
  char *value;  

  /* Open the configuration file */

  if ((fp = fopen (CONFFILE, "rt")) == NULL)
  {
    char err[512];
    snprintf (err, 511, "Configuration file %s not found. Exiting.", CONFFILE);
    syslog (LOG_ERR, err);
    throw (netmon::netmon_exception (err));
    exit (-1);
  }

  /* Truncate our input buffer, just in case */

  line[0] = '\0';

  /* Search for the entry we need */

  while ((strstr (line, variable) == NULL) && (!feof (fp)))
    fgets (line, 128, fp); 

  fclose (fp);

  /* What we need should be placed right after "=" sign */

  if (strstr (line, variable))
  {
    if ((value = strstr (line, "=")) == NULL)
      return 1;

    /* The value is one character after "=" sign */
    value++;

    strcpy (dest, value);

    /* If there is a newline at the end, strip it */

    if (dest[strlen(dest) - 1] == '\n')
      dest[strlen(dest) - 1] = '\0';
  }
  else
    return 1;

  return 0;
}

void readConfig (char *dbname, char *dbuser)
{
  if (getentry (dbname, "DBNAME"))
  {
    syslog (LOG_ERR, "Database name (DBNAME) not found in the configuration file.");
    exit (1);
  }
  if (getentry (dbuser, "DBUSER"))
  {
    syslog (LOG_ERR, "Database user name (DBUSER) not found in the configuration file.");
    exit (1);
  }
}
