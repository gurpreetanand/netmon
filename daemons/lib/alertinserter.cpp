#include "alertinserter.h"
#include "db.h"
#include "log.h"
#include "netmon.h"
#include "memcache.h"

#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sstream>
#include <dbus-c++/dbus.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>

namespace netmon
{
  static const char *DBUS_SERVER_NAME = "ca.netmon.DBus";
  // static const char *DBUS_SERVER_NAME = "192.168.3.139";
  // static const char *DBUS_SERVER_PATH = "/ca/netmon/DBus";
  static const char *DBUS_SERVER_PATH = "/ca/netmon/DBus";

  alert_inserter::alert_inserter ()
  {
  }

  alert_inserter::alert_inserter (int id, const std::string &atempl, const std::string &asubject)
    : m_alertID (id), m_template (atempl), m_subject (asubject), m_isRecovery (false)
  {
  }

  alert_inserter::~alert_inserter ()
  {
  }

  int alert_inserter::run (const commands_t &commands)
  {
    pid_t pid;
    if ((pid = fork()) < 0)
      return -1;
    else
      if (pid != 0)
        return 0;

    // new thread
    connectToDB();

    for (commands_t::const_iterator i = commands.begin(); i != commands.end(); ++i)
    {
      if ((*i).m_command.size() == 0)
        continue;
      if (m_isRecovery && !(*i).m_recovery)
        continue;
      if (!m_isRecovery && !(*i).m_failure)
        continue;
      std::string output;
      int ret = execCommand ((*i).m_command, (*i).m_async, output);
      if (!(*i).m_async)
      {
        m_template += "\n\n================================================================================";
        if (ret == 0)
        {
          m_template += "\nOutput of ";
          m_template += (*i).m_command;
          m_template += ":\n";
        }
        else
        {
          m_template += "\nSummary for command ";
          m_template += (*i).m_command;
          m_template += ":\n";
          m_template += "The return code of this command indicates that an error has occured.";
          m_template += "\nThe command return code was: ";
          std::ostringstream tmp;
          tmp << ret;
          m_template += tmp.str();
          m_template += "\nThe command output was:\n";
        }
        m_template += output;
      }
    }

    if (insertAlert())
      dispatchDBusMessage();

    cleanUp();
    return 0; // never reached
  }

  int alert_inserter::execCommand (const std::string &command, bool async, std::string &output)
  {
    if (async)
      return system (command.c_str());
    else
    {
      char buff[512];
      FILE *out = popen (command.c_str(), "r");
      if (out == 0)
        return -1;
      while (!feof (out))
      {
        memset (buff, 0, sizeof (buff));
        fread ((void *) buff, 511, 1, out);
        output += std::string (buff);
      }
      pclose (out);
      return 0;
    }
    return -1;
  }

  void alert_inserter::connectToDB ()
  {
    char dbname[DBNAMELEN];
    char dbuser[DBUSERLEN];

    readConfig (dbname, dbuser);
    m_db = new db;
    m_db->connect (dbname, dbuser);
  }

  void alert_inserter::cleanUp ()
  {
    m_db->close();
    delete m_db;
    exit (0);
  }

  bool alert_inserter::insertAlert ()
  {
    std::ostringstream triggerQuery;
    triggerQuery.str("");
    
    triggerQuery << "select trigger_id from alert_handlers where id=" << m_alertID;
    PGresult *triggerResult = NULL, *validResult = NULL;
    
    int trigger_id = 0;
    
    try {
      triggerResult = m_db->execQuery(triggerQuery.str().c_str());
      if (PQgetisnull(triggerResult, 0, 0) == 0)
	trigger_id = atoi(PQgetvalue(triggerResult, 0, 0));
    } catch (netmon::db_exception &e) {
       log::instance()->add(_eLogFatal, "Couldn't execute query [%s]", triggerQuery.str().c_str(), e.what());
       return false;
    }
    
    PQclear(triggerResult);
    
    std::ostringstream valid;
    
    valid.str("");
    valid << 	"SELECT id "
	  <<	"FROM alert_maintenance "
	  <<	"WHERE ( "
	  <<		"(recurrence_unit = 'day') "
	  <<		"OR ((recurrence_unit = 'week') AND (schedule_week = extract('dow' FROM now()))) "
	  <<		"OR ((recurrence_unit = 'month') AND (schedule_day = extract('day' FROM now()))) "
	  <<		"OR ((recurrence_unit = 'year') AND (schedule_month = extract('month' FROM now())) "
	  <<		"AND (schedule_day = extract('day' FROM now()))) "
	  <<	") "
	  <<	"AND (extract('hour' FROM now()) BETWEEN schedule_hour AND  schedule_hour::integer + maintenance_duration::integer) "
	  <<	"AND trigger_id =" << trigger_id
	  <<	" ORDER BY id ASC ";

    try {
      validResult = m_db->execQuery(valid.str().c_str());
      if (PQgetisnull(validResult, 0, 0) == 1) { //only one ID of output
	log::instance()->add(_eLogInfo, "Null value for trigger ID, sending alert.");
	static std::ostringstream sql;

	char *escmsg = new char[m_template.size() * 2 + 1];
	PQescapeString (escmsg, m_template.c_str(), m_template.size());
	char *escsub = new char[m_subject.size() * 2 + 1];
	PQescapeString (escsub, m_subject.c_str(), m_subject.size());

	if (strlen (escsub) > 255)
	  escsub[255] = '\0';

	sql.str("");
	sql << "INSERT INTO ALERT_PENDING (HANDLER_ID, SENT, RETRIES_PROCESSED, ID, \
		PARSED_ALERT_MESSAGE, PARSED_SUBJECT, TRIGGER_TIMESTAMP) VALUES ("
	    << m_alertID 
	    << ", '0'::bit, 0, DEFAULT, '"
	    << escmsg
	    << "', '"
	    << escsub
	    << "', "
	    << (int) time (0) << ")";


        
	m_db->execSQL (sql.str().c_str());
	PQclear(validResult);
        
        writeMemcacheAlert(trigger_id, escsub, escmsg);

        delete[] escmsg;
        delete[] escsub;
        
	return true;
      } else {
	std::ostringstream maintenance_ids;
	maintenance_ids.str("");
	int size = PQntuples(validResult);
	for (int j=0; j < size; j++) {
	   maintenance_ids << PQgetvalue(validResult, j, 0);
	   if (j < size-1)
	     maintenance_ids << ", ";
	}
	log::instance()->add(netmon::_eLogInfo, "Trigger ID is under maintenance.  Maintenance IDs: [%s]  Query: [%s]", 
			     maintenance_ids.str().c_str(), valid.str().c_str());
	PQclear(validResult);
	return false;
      }

    } catch (netmon::db_exception& e) {
     	log::instance()->add(netmon::_eLogFatal, "Fatal Error in inserting alert [%s]", e.what());
	PQclear(validResult);
	return false;
    }
  }
  
  void alert_inserter::writeMemcacheAlert(int trigger_id, std::string subject, std::string message) {
    Memcacher memcacher;
    std::ostringstream sql;
    // Get device ID
    if (trigger_id != 0) {
      std::string reference_pkey_val, reference_table_name;

      PGresult *deviceResult;
      sql.str("");
      sql << "SELECT reference_table_name, reference_pkey_val "
          << "FROM alert_triggers WHERE trigger_id=" << trigger_id;
      deviceResult = m_db->execQuery(sql.str().c_str());

      if (PQgetisnull(deviceResult, 0, 0) != 1) { //only one ID of output
        return;
      }
       
      reference_table_name = PQgetvalue(deviceResult, 0, 0);
      reference_pkey_val = PQgetvalue(deviceResult, 0, 1);

      PQclear(deviceResult);
      
      if (reference_table_name.size() > 0 && reference_table_name != "urls") {
        sql.str("");
        sql << "SELECT ip FROM " << reference_table_name << " WHERE "
            << " srv_id=" << reference_pkey_val;
            
        deviceResult = m_db->execQuery(sql.str().c_str());
        if (PQgetisnull(deviceResult, 0, 0) != 1) { //only one ID of output
          return;
        }

        std::string memcacheDeviceKey = std::string(PQgetvalue(deviceResult, 0, 0)) + "-alerts";
        std::string json = memcacher.get(memcacheDeviceKey);
        
        // Parse json from key
        Json::Reader reader;
        Json::Value deviceRoot;
        if (!reader.parse(json, deviceRoot)) {
          return;
        }
        
        std::ostringstream alertKey("");
        alertKey << "trigger_id:" << trigger_id;
        
        Json::Value alertInfo;
        
        alertInfo["timestamp"] = Json::Value((int)time(NULL));
        alertInfo["trigger_id"] = Json::Value(trigger_id);
        alertInfo["subject"] = Json::Value(subject);
        alertInfo["message"] = Json::Value(message);
        
        deviceRoot[alertKey.str()] = alertInfo;

        Json::FastWriter writer;
        std::string json_log = writer.write(deviceRoot);

        std::cout << "Setting memcache " << memcacheDeviceKey << " : " << json_log << std::endl;
        memcacher.set(memcacheDeviceKey, json_log);
      }
    } 
  }

  void alert_inserter::dispatchDBusMessage ()
  {
    static std::string en = std::string (DBUS_SERVER_NAME) + std::string (".emailmond");
    static std::string ep = std::string (DBUS_SERVER_PATH) + std::string ("/emailmond");
    static std::string pn = std::string (DBUS_SERVER_NAME) + std::string (".pagermond");
    static std::string pp = std::string (DBUS_SERVER_PATH) + std::string ("/pagermond");

    try
    {
      DBus::BusDispatcher dispatcher;
      DBus::default_dispatcher = &dispatcher;

      DBus::Connection conn1 = DBus::Connection::SystemBus();
      conn1.request_name (en.c_str());
      DBus::Connection conn2 = DBus::Connection::SystemBus();
      conn2.request_name (pn.c_str());

      DBus::CallMessage msg1 (en.c_str(), ep.c_str(), "ca.netmon.DBus.emailmond", "alert");
      DBus::CallMessage msg2 (pn.c_str(), pp.c_str(), "ca.netmon.DBus.pagermond", "alert");
      conn1.send (msg1);
      conn2.send (msg2);
    }
    catch (DBus::Error &e)
    {
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      log::instance()->add(_eLogFatal, e.what());
    }
  }
}

