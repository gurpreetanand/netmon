#ifndef __NETMON_DB__
#define __NETMON_DB__

#include <libpq-fe.h>
#include <string>

#include "exception.h"

namespace netmon
{
  class db_exception : public netmon_exception
  {
    public:
      db_exception (const std::string &err)
        : netmon_exception (err)
      {}
  };

  class db
  {
    public:
      db ();
      ~db ();
      void connect (const char *, const char *) throw (db_exception);
      void close ();
      int execSQL (const char *) throw (db_exception);
      void asyncSQL (const char *) throw (db_exception);
      void wait ();
      PGresult *execQuery (const char *) throw (db_exception);

      void beginCopy (const char *) throw (db_exception);
      void copyData (const char *, int);
      void endCopy (const char *);

      char *escapeString (const char *, size_t);
      std::string escapeInsertString (std::string str);

    protected:
      PGconn *m_conn;
      bool m_connected;
  };
}

#endif // __NETMON_DB__
