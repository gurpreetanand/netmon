#ifndef __ALERT_MANAGER__
#define __ALERT_MANAGER__

#include <list>
#include <map>
#include <string>

#include "compfactory.h"
#include "alertutil.h"
#include "exception.h"

namespace netmon
{
  class db;
  class alerthandler;
  class alert_throttler;
  class flapping_detector;

  class alert_no_template_exception : public netmon_exception
  {
    public:
      alert_no_template_exception (const std::string &type)
        : netmon_exception ("Cannot find '" + type + "' template")
      {}
  };

  class alert_manager
  {
    protected:
      typedef comparator<long long> int_comparator;
      typedef compare_factory<long long> int_compare_factory;

      struct alert_data
      {
        ~alert_data ()
        {
          delete m_comparator;
        }
        unsigned int m_alertID;
        unsigned int m_triggerID;
        long long m_threshold;
        std::string m_pattern;
        std::string m_label;
        std::string m_conditionalID;
        bool m_triggered;
        int_comparator *m_comparator;
      };

      typedef std::multimap<unsigned int, alert_data *> alert_map;

    public:
      alert_manager (db *, bool = true);
      virtual ~alert_manager ();

      virtual void reload ();
      virtual void init ();

      virtual unsigned int getAlerts (unsigned int, long long, const values_map &);
      virtual unsigned int getAlerts (unsigned int, const char *, const values_map &);
      virtual unsigned int getAlerts (unsigned int, const values_map &);

    protected:
      void dispatchAlert (alerthandler *, const values_map &);
      void loadStaticData (const std::string &, std::string &, std::string &);
      void loadAlerts ();
      void clearAlerts ();
      void toggleAlertFlag (unsigned int);
      void getReferenceData (unsigned int, std::map<std::string, std::string> &);
      alerthandler *createAlertHandler (const alert_data *);
      void addThrottleMessage (alerthandler *, int);
      void addFlappingMessage (alerthandler *);

      virtual bool match (const char *, const char *);
      virtual alerthandler *createAlertHandler (const alert_data *, bool);
      virtual void loadStaticData ();
      virtual void createAlertQuery ();

      virtual bool isAlertValid (alert_data *)
      {
        return true;
      }

      virtual void getIDName (std::string &s)
      {
        s = "id";
      }

      virtual void getReferenceTable (std::string &) = 0;
      virtual void getOverName (std::string &) = 0;
      virtual void getBelowName (std::string &) = 0;

      db *m_db;
      alert_throttler *m_throttler;
      flapping_detector *m_flapDetector;
      bool m_useUpDownAlerts;
      int_compare_factory *m_compFactory;
      std::string m_overTemplate;
      std::string m_overSubject;
      std::string m_belowTemplate;
      std::string m_belowSubject;
      std::string m_getAlertsQuery;

      alert_map m_alerts;
  };
}

#endif // __ALERT_MANAGER__
