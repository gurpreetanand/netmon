#include "condmanager.h"
#include "db.h"
#include "netmon.h"

#include <cstring>
#include <cstdlib>
#include <sstream>
#include <unistd.h>

namespace netmon
{
  conditional_manager::conditional_manager (db *mydb, unsigned int timeout /* = 60 */)
    : m_db (mydb), m_timeout (timeout)
  {
    reload (false);
  }

  conditional_manager::~conditional_manager ()
  {
    clear();
  }

  void conditional_manager::clear ()
  {
    for (cond_map_t::iterator i = m_map.begin(); i != m_map.end(); ++i)
      delete i->second;
    m_map.clear();
  }

  void conditional_manager::reload (bool check /* = true*/)
  {
    clear();

    PGresult *res = m_db->execQuery ("select cond_id, ip from conditionals");
    for (int i = 0; i < PQntuples (res); ++i)
    {
      int id = atoi (PQgetvalue (res, i, 0));
      if (check)
      {
        cond_map_t::iterator it = m_map.find (id);
        if (it != m_map.end())
        {
          if (strcmp (it->second->m_ip.c_str(), PQgetvalue (res, i, 1)) != 0)
          {
            it->second->m_ip = std::string (PQgetvalue (res, i, 1));
            it->second->m_timeStamp = 0;
            continue;
          }
        }
      }
      cond_data *d = new cond_data;
      d->m_ip = std::string (PQgetvalue (res, i, 1));
      d->m_timeStamp = 0;
      m_map[id] = d;
    }
    PQclear (res);
  }

  bool conditional_manager::conditionalValid (int id)
  {
    cond_map_t::iterator it = m_map.find (id);
    if (it == m_map.end()) // no such conditional, try to load it
    {
      if (!loadConditional (id))
        return true;
      else
        it = m_map.find (id);
    }
    unsigned int now = time (0);
    if ((now - it->second->m_timeStamp) > m_timeout)
    {
      int ret = 0;
      for (unsigned int i = 0; i < 5; ++i)
      {
        ret = send_ping (it->second->m_ip.c_str());
        if (ret != 0)
        {
          it->second->m_timeStamp = time (0);
          return true;
        }
        sleep (1);
      }
      return false;
    }
    return true;
  }

  bool conditional_manager::conditionalValid (const char *id)
  {
    int i = atoi (id);
    return conditionalValid (i);
  }

  bool conditional_manager::loadConditional (int id)
  {
    static std::ostringstream sql;

    sql.str("");
    sql << "select ip from conditionals where cond_id = " << id;

    PGresult *res = m_db->execQuery (sql.str().c_str());
    if (PQntuples (res) == 0)
    {
      PQclear (res);
      return false;
    }

    cond_data *d = new cond_data;
    d->m_ip = std::string (PQgetvalue (res, 0, 0));
    d->m_timeStamp = 0;
    m_map[id] = d;

    PQclear (res);

    return true;
  }
}

#ifdef __TEST__

#include <cstdio>

int main()
{
  using namespace netmon;
  try
  {
    db *d = new db;
    d->connect ("netmon35", "root");
    conditional_manager *c = new conditional_manager (d);
    if (c->conditionalValid (1))
      printf ("conditional is online\n");
    sleep (5);
    if (c->conditionalValid (1))
      printf ("conditional is online\n");
  }
  catch (netmon_exception &e)
  {
    printf ("error: %s\n", e.what());
  }
}

#endif // __TEST__
