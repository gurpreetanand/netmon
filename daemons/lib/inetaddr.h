#ifndef __INET_ADDR__
#define __INET_ADDR__

#include <netinet/in.h>

namespace netmon
{
  class inetaddr
  {
    public:
      inetaddr ();
      inetaddr (const char *);
      inetaddr (const in_addr &);
      inetaddr (unsigned long);

      const char *addr () const;
      unsigned long iaddr () const;
      inetaddr getBroadcast ();
      inetaddr getBroadcast0 ();
      bool isValid () const;

      bool operator== (const inetaddr &) const;
      bool operator< (const inetaddr &) const;
      bool operator<= (const inetaddr &) const;
      inetaddr &operator++ ();
      inetaddr operator++ (int);

    private:
      union netaddr
      {
        in_addr addr;
        unsigned char b[4];
      } __attribute__((packed));

    protected:
      netaddr m_addr;
      bool m_isInit;
  };
}

#endif // __INET_ADDR__
