#ifndef __NETMON__
#define __NETMON__

#include "db.h"

#define MAXPACKET        4096
#define DEF_DATALEN      56
#define SIZE_ICMP_HDR    8
#define SIZE_TIME_DATA   8
#define PINGRESPONSETIME  10
#define SYSLOGLINELEN     256
#define SYSLOGFACILITYLEN 32

#define DBNAMELEN         32
#define DBUSERLEN         32

#if __WORDSIZE == 64
#define u64prefix "l"
#else
#define u64prefix "ll"
#endif

int daemonize ();

int getentry (char *, const char *);
void readConfig (char *, char *);
long send_ping (const char *);
void installSigHandler (void (*)(int));

int speedtostr (char *, int, unsigned long long, int);

#endif // __NETMON__
