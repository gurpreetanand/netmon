#include <stdlib.h>
#include <errno.h>

#include "msgqueue.h"

namespace netmon
{
  msgqueue::msgqueue ()
  {
    msgqueue::dispose ();
  }

  void msgqueue::verify (state s)
  {
    if (s == _eReady && m_id < 0)
      throw m_error = EINVAL;
    if (s != _eReady && m_id >= 0)
      throw m_error = EINVAL;
  }

  msgqueue &msgqueue::dispose ()
  {
    m_key = IPC_PRIVATE;
    m_id = -1;
    return *this;
  }

  msgqueue &msgqueue::create (key_t key, int flags)
  {
    verify (_eNotReady);

    flags |= IPC_CREAT;
    m_key = key;

    m_id = ::msgget (m_key, flags);
    if (m_id == -1)
      throw m_error = errno;

    return *this;
  }

  msgqueue &msgqueue::access (key_t key)
  {
    verify (_eNotReady);

    m_key = key;
    m_id = ::msgget (m_key, 0);
    if (m_id == -1)
      throw m_error = errno;

    return *this;
  }

  msgqueue &msgqueue::destroy ()
  {
    verify (_eReady);

    if (::msgctl (m_id, IPC_RMID, 0) == -1)
      throw m_error = errno;

    msgqueue::dispose ();
    return *this;
  }

  msqid_ds &msgqueue::stat (msqid_ds &sbuf)
  {
    verify (_eReady);

    if (::msgctl (m_id, IPC_STAT, &sbuf) == -1)
      throw m_error = errno;

    return sbuf;
  }

  msgqueue &msgqueue::set (msqid_ds &sbuf)
  {
    verify (_eReady);

    if (::msgctl (m_id, IPC_SET, &sbuf) == -1)
      throw m_error = errno;

    return *this;
  }

  bool msgqueue::send (msg &m, size_t size, int flags)
  {
    verify (_eReady);

    size_t msize = size - sizeof (m.m_type);
    int ret;

    do
    {
      ret = ::msgsnd (m_id, &m, msize, flags);
    } while (ret == -1 && errno == EINTR);

    if (ret)
    {
      if (flags & IPC_NOWAIT && errno == EAGAIN)
        return false;
      throw m_error = errno;
    }

    return true;
  }

  bool msgqueue::recv (msg &m, size_t &size, size_t maxsize, long mtype, int flags)
  {
    verify (_eReady);

    size_t msize = maxsize - sizeof (m.m_type);
    int ret;

/*    do
    {
      ret = ::msgrcv (m_id, &m, msize, mtype, flags);
    } while (ret == -1 && errno == EINTR);
*/
    ret = ::msgrcv (m_id, &m, msize, mtype, flags);
    if (ret == -1 && errno == EINTR)
      return false;

    if (ret == -1)
    {
      if (flags & IPC_NOWAIT && errno == EAGAIN)
        return false;
      throw m_error = errno;
    }
    
    size = ret + sizeof (m.m_type);
    return true;
  }
}

#ifdef __TEST__

#include <cstdio>
#include <unistd.h>

using namespace netmon;

struct mymsg : public msg
{
  int m_no;
};

int main ()
{
  msgqueue m;

  try
  {
    m.create (0xff00ceca, 0600);
    mymsg mym;
    int cnt = 0;
    while (true)
    {
      mym.m_no = cnt;
      mym.m_type = 1;
      m.send (mym, sizeof (mym));
      printf ("Sent %d\n", cnt);
      ++cnt;
      sleep (1);
    }
  }
  catch (int e)
  {
    errno = e;
    perror ("test");
    exit (-1);
  }
}

#endif
