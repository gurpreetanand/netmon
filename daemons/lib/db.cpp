#include "db.h"

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <sstream>

namespace netmon
{
  db::db ()
    : m_connected (false)
  {
  }

  db::~db ()
  {
    close();
  }

  void db::close ()
  {
    if (m_connected)
    {
      PQfinish (m_conn);
      m_connected = false;
    }
  }

  void db::connect (const char *dbname, const char *dbuser) throw (db_exception)
  {
    std::string connstr;

    // create connection string
    connstr = "host=127.0.0.1 dbname=" + std::string (dbname) + " user=" + std::string (dbuser) + " port=5432";

    // make a connection to the PostgreSQL database

    m_conn = PQconnectdb (connstr.c_str());

    /* Check to see that the backend connection was successfully made */
    
    if (PQstatus (m_conn) == CONNECTION_BAD)
    {
      // throw an exception with error string
      std::string err = "Can't connect to postgreSQL server: ";
      err += std::string (PQerrorMessage (m_conn));
      throw (db_exception (err));
      
      /* Disconnect nicely from pgsql */
      PQfinish (m_conn);
    }
    m_connected = true;
  }

  int db::execSQL (const char *sql) throw (db_exception)
  {
    if (PQstatus (m_conn) == CONNECTION_BAD)
      PQreset (m_conn);

    PGresult *res = PQexec (m_conn, sql);
  
    if (!res || PQresultStatus(res) != PGRES_COMMAND_OK)
    {
      PQclear (res);
      throw (db_exception (PQerrorMessage (m_conn)));
    }

    char *rows = PQcmdTuples (res);
    int ret = 0;
    if (rows == 0 || strlen (rows) == 0)
      ret = -1;
    else
      ret = atoi (rows);

    PQclear (res);
    return ret;
  }

  void db::asyncSQL (const char *sql) throw (db_exception)
  {
    if (PQstatus (m_conn) == CONNECTION_BAD)
      PQreset (m_conn);

    int ret = PQsendQuery (m_conn, sql);
  
    if (ret != 1)
      throw (db_exception (PQerrorMessage (m_conn)));
  }

  void db::wait ()
  {
    PGresult *res;
    while ((res = PQgetResult (m_conn)))
      PQclear (res);
  }

  PGresult *db::execQuery (const char *sql) throw (db_exception)
  {
    if (PQstatus (m_conn) == CONNECTION_BAD)
      PQreset (m_conn);

    PGresult *res = PQexec (m_conn, sql);
  
    if (!res || PQresultStatus(res) != PGRES_TUPLES_OK)
    {
      if (res)
        PQclear (res);
      throw (db_exception (PQerrorMessage (m_conn)));
    }
  
    return res;
  }

  void db::beginCopy (const char *sql) throw (db_exception)
  {
    if (PQstatus (m_conn) == CONNECTION_BAD)
      PQreset (m_conn);

    PGresult *res = PQexec (m_conn, sql);
  
    if (!res || PQresultStatus(res) != PGRES_COPY_IN)
    {
      PQclear (res);
      throw (db_exception (PQerrorMessage (m_conn)));
    }

    PQclear (res);
  }

  void db::copyData (const char *data, int size)
  {
    PQputCopyData (m_conn, data, size);
  }

  void db::endCopy (const char *msg)
  {
    PQputCopyEnd (m_conn, msg);
  }

  char *db::escapeString (const char *str, size_t size)
  {
    if (str == 0)
      return 0;

    char *b = new char[size * 2 + 1];
    int err;
    (void) PQescapeStringConn (m_conn, b, str, size, &err);
    if (err == 0)
      return b;
    delete[] b;
    throw (db_exception (PQerrorMessage (m_conn)));
  }

  std::string db::escapeInsertString (std::string str)
  {
    try {
      // Use a stringstream to surround with quotes
      std::ostringstream escaped;

      // Escape original input
      char* orig = escapeString(str.c_str(), str.size());

      // Build the insert string
      escaped << "'";
      escaped << escapeString(str.c_str(), str.size());
      escaped << "'";

      // Free memory from PQescapeStringConn
      delete[] orig;

      return escaped.str();
    } catch (int e) {
      // In case escapeString failed, return empty string
      return "''";
    }
  }
}
