#ifndef __DAEMON__
#define __DAEMON__

#include <string>
#include "exception.h"
#include "nsignal.h"

namespace netmon
{
  class daemon_exc : public netmon_exception
  {
    public:
      daemon_exc (const std::string &e)
        : netmon_exception (e)
      {}
  };

  class db;
  class dvars;
  class serial_number;

  class daemon
  {
    public:
      daemon ();
      daemon (int, char **);
      virtual ~daemon ();

      virtual void init ();
      int start ();
      bool devLimitsValid ();

    protected:
      void daemonize ();
      void parseOptions (int, char **);
      void setRLimits ();
      void installSignalHandler ();
      bool isTermSignaled ()
      {
        return m_isTermSignaled == 1;
      }

      virtual const char *getDaemonName () = 0;
      virtual const char *getLogFileName () = 0;
      virtual int run () = 0;

      virtual void usage ();
      virtual void onOption (char);
      virtual const char *getShortOptions ();

      bool m_toDaemon;
      bool m_init;
      int m_argc;
      char **m_argv;
      db *m_db;
      dvars *m_vars;
      serial_number *m_sn;
      int m_devLimit;

    private:
      bool checkSPH ();
      void handleTermSignal ();

      class daemon_term_handler : public event_handler
      {
        public:
          virtual int handle_signal (int);
      };

      friend class daemon_term_handler;
      daemon_term_handler m_termHandler;
      static sig_atomic_t m_isTermSignaled;
  };
}

#endif // __DAEMON__
