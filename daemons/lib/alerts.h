#ifndef __ALERT_HANDLER__
#define __ALERT_HANDLER__

#include "alertinserter.h"

#include <string>
#include <map>

namespace netmon
{
  class db;

  class alerthandler
  {
    public:
      enum TemplateKeywords
      {
        eHostName,
        eHostIP,
        eErrMsg,
        eSysErr,
        eTime,
        eThreshold,
        eNewStatus,
        eInterface,
        eUtilization,
        ePort,
        eSysLogMsg,
        eSysLogSeverity,
        eInOut,
        eProtocol,
        eSrcIP,
        eDstIP,
        eOpenPorts,
        eMACAddr,
        eConnectionList,
        eOID,
        ePayload,
        eLatency,
        eLabel,
        ePattern,
        ePartition,
        eCommands,
        eMsgID,
        eDevice
      };

      alerthandler ()
      {
      }

      alerthandler (db *mydb)
        : m_db (mydb)
      {
      }

      ~alerthandler ()
      {
        m_kmap.clear ();
      }

      void setDB (db *mydb)
      {
        m_db = mydb;
      }

      void addKeywordValue (TemplateKeywords, const char *, bool = false);
      void addKeywordValue (TemplateKeywords,  int);
      void insertAlert ();
      void insertAlert (int, const char *);

      void setID (unsigned int id)
      {
        m_alertID = id;
      }

      unsigned int getID () const
      {
        return m_alertID;
      }

      void setTemplate (const std::string &tmpl)
      {
        m_template = tmpl;
      }

      std::string &getTemplate ()
      {
        return m_template;
      }

      void setSubject (const std::string &subject)
      {
        m_subject = subject;
      }

      std::string &getSubject ()
      {
        return m_subject;
      }

      void setRecovery (bool recovery)
      {
        m_isRecovery = recovery;
      }

      bool getRecovery ()
      {
        return m_isRecovery;
      }

      typedef std::map<std::string, std::string> ref_data_map_t;

      ref_data_map_t &getMap ()
      {
        return m_refmap;
      }

    protected:
      std::string formatMessage (const char *);
      std::string formatMessageFromRefData (const char *);
      void replaceKeywordWithValue (std::string &, const std::string &, const std::string &);
      void getCommands (alert_inserter::commands_t &);
      void setCommandsKeyWord (alert_inserter::commands_t &);

      typedef std::map<TemplateKeywords, std::string> keywordsvalsmap;

      keywordsvalsmap m_kmap;
      ref_data_map_t m_refmap;
      db *m_db;
      unsigned int m_alertID;
      std::string m_template;
      std::string m_subject;
      bool m_isRecovery;
  };
}

#endif // __ALERT_HANDLER__
