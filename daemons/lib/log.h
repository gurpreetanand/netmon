#ifndef __NETMON_LOG__
#define __NETMON_LOG__

#include <cstdio>

namespace netmon
{
  enum loglevel { _eLogDebug, _eLogInfo, _eLogWarning, _eLogError, _eLogFatal, _eLogUrgent };

  class log
  {
    public:
      static log *instance ();

      void init (const char *, loglevel);
      void init (FILE *, loglevel);
      int add (loglevel, const char *, ...);

      const loglevel &logLevel () const
      {
        return m_level;
      }

      loglevel &logLevel ()
      {
        return m_level;
      }

    protected:
      log ();
      ~log ();

      void printLevel (loglevel);

      FILE *m_log;
      loglevel m_level;
      bool m_init;
      static log *m_instance;
  };

  loglevel llevel (int);
}

#endif // __NETMON_LOG__
