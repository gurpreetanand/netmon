#include <stdlib.h>
#include <iostream>
#include <limits.h>
#include "compfactory.h"

void printusage(char* filename)
{
	std::cout << "Usage: " << filename << " <value> <threshold>" << std::endl;
	exit(1);
}

int main(int argc, char** argv)
{

	typedef unsigned long testtype;  // this is from the current code

	std::string types[] = {"<", "<=", ">", ">=", "==", "!="};

	if (argc != 3)
	{
		printusage(argv[0]);
	}

	int value = atoi(argv[1]);
	unsigned int threshold = atoi(argv[2]);

	const int size = sizeof(types)/sizeof(std::string);

	netmon::compare_factory<testtype> *m_compFactory = new netmon::compare_factory<testtype>;
	typedef netmon::comparator<testtype> int_comparator;

	for (int i=0; i < size; i++)
	{
		int_comparator *m_comparator;
		m_comparator = m_compFactory->createComparator(types[i].c_str());

		if (m_comparator->compare(value, threshold))
			std::cout << "TRUE: " << value << " " << types[i] << " " << threshold;
		else
			std::cout << "FALSE: " << value << " " << types[i] << " " << threshold;

		std::cout << std::endl;
		delete m_comparator;
	}
}
