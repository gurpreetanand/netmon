#include "inetaddr.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <cstdio>

static inline void inc (unsigned char *b)
{
  if (b[0] == 0xff && b[1] == 0xff && b[2] == 0xff && b[3] == 0xff)
    return;

  if (b[3] < 0xfe)
  {
    b[3] = b[3] + 1;
    return;
  }
  b[3] = 1;
  if (b[2] != 0xff)
  {
    b[2] = b[2] + 1;
    return;
  }
  b[2] = 0;
  if (b[1] != 0xff)
  {
    b[1] = b[1] + 1;
    return;
  }
  b[1] = 0;
  b[0] = b[0] + 1;
}

namespace netmon
{
  inetaddr::inetaddr ()
    : m_isInit (false)
  {
    m_addr.addr.s_addr = 0;
  }

  inetaddr::inetaddr (const char *addr)
  {
    if (inet_aton (addr, &m_addr.addr) != 0)
      m_isInit = true;
    else
      m_isInit = false;
  }

  inetaddr::inetaddr (const in_addr &ina)
  {
    m_addr.addr = ina;
    m_isInit = true;
  }

  inetaddr::inetaddr (unsigned long ip)
  {
    m_addr.addr.s_addr = ip;
    m_isInit = true;
  }

  const char *inetaddr::addr () const
  {
    return inet_ntoa (m_addr.addr);
  }

  unsigned long inetaddr::iaddr () const
  {
    return m_addr.addr.s_addr;
  }

  inetaddr inetaddr::getBroadcast ()
  {
    inetaddr tmp = *this;
    tmp.m_addr.b[3] = 0xff;
    return tmp;
  }

  inetaddr inetaddr::getBroadcast0 ()
  {
    inetaddr tmp = *this;
    tmp.m_addr.b[3] = 0x00;
    return tmp;
  }

  bool inetaddr::isValid () const
  {
    if (!m_isInit)
      return false;
    return (m_addr.b[3] != 0x00 && m_addr.b[3] != 0xff);
  }

  bool inetaddr::operator== (const inetaddr &a) const
  {
    return m_addr.addr.s_addr == a.m_addr.addr.s_addr;
  }

  bool inetaddr::operator< (const inetaddr &a) const
  {
    return (ntohl (m_addr.addr.s_addr) < ntohl (a.m_addr.addr.s_addr));
  }

  bool inetaddr::operator<= (const inetaddr &a) const
  {
    return (ntohl (m_addr.addr.s_addr) <= ntohl (a.m_addr.addr.s_addr));
  }

  inetaddr& inetaddr::operator++ ()
  {
    inc (m_addr.b);
    return *this;
  }

  inetaddr inetaddr::operator++ (int)
  {
    inetaddr tmp = *this;
    inc (m_addr.b);
    return tmp;
  }
}

#ifdef __TEST__
int main ()
{
/*
  in_addr x;
  inet_aton ("192.168.209.1", &x);
  unsigned long l = ntohl (x.s_addr);
  
  for (int i = 0; i < 260; ++i)
  {
    ++l;
    x.s_addr = htonl (l);
    printf ("%s\n", inet_ntoa (x));
  }

  return 0;
*/
  inetaddr a ("192.168.254.1");
  inetaddr b ("192.169.0.5");

  while (a <= b)
  {
    ++a;
    printf ("%s\n", a.addr ());
  }

/*
  for (int i = 0; i < 260; ++i)
  {
    ++a;
    printf ("%s\n", a.addr ());
  }
*/
}
#endif // __TEST__
