#ifndef __ALERT_THROTTLER__
#define __ALERT_THROTTLER__

#include "alertutil.h"

#include <list>
#include <map>

namespace netmon
{
  struct alert_values
  {
    alert_values()
      : m_counter (0)
    {}
    values_map m_values;
    unsigned int m_counter;
    unsigned int m_timestamp;
  };

  typedef std::list<alert_values *> values_list;

  class alert_throttler
  {
    protected:
      struct alert_data
      {
        alert_data (unsigned int interval)
          : m_interval (interval)
        {}
        unsigned int m_interval;
        values_list m_values;
      };

      typedef std::map<unsigned int, alert_data *> throttle_map;

    public:
      alert_throttler ();
      virtual ~alert_throttler ();

      void clear ();
      void removeOldEntries ();
      void updateAlert (unsigned int, unsigned int);
      bool toTrigger (unsigned int, const values_map &, unsigned int &);

    protected:
      values_list::iterator findValues (throttle_map::iterator, const values_map &);

      throttle_map m_tmap;
  };
}

#endif // __ALERT_THROTTLER__
