#ifndef __DVARS__
#define __DVARS__

#include <string>
#include <map>
#include "db.h"

namespace netmon
{
  class dvars
  {
    public:
      dvars (db &, const char *);
      ~dvars ();

      bool get (const char *, std::string &);
      bool get (const char *, int &);
      void reload ();

    protected:
      void populate ();

      typedef std::map<std::string, std::string> varmap;
      varmap m_vars;
      db &m_db;
      std::string m_dname;
  };
}

#endif // __DVARS__
