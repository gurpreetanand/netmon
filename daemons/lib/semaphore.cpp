#include "semaphore.h"
#include <string>

namespace netmon
{
  semaphore::semaphore (int flags)
  {
    int i = semget (IPC_PRIVATE, 1, flags);
    if (i < 0)
      throw (semaphore_exception ("Cannot create semaphore"));
    m_id = i;
    semop (1, 1);
  }

  semaphore::semaphore (key_t key, int flags)
  {
    int i = semget (key, 3, flags);
    if (i < 0)
    {
      i = semget (key, 3, 0);
      if (i < 0)
        throw (semaphore_exception ("Cannot create semaphore."));
      m_id = i;
    }
    else
    {
      m_id = i;
      int j = initSemaphore ();
      if (j < 0)
        throw (semaphore_exception ("Cannot initialize semaphore."));
    }
    semop (1, 1);
  }

  semaphore::~semaphore ()
  {
    semop (1, -1);
//    union semun ignored;
//    semctl (m_id, 1, IPC_RMID, ignored);
  }

  int semaphore::initSemaphore ()
  {
    union semun arg;
    unsigned short vals[3];
    vals[0] = 1;
    vals[1] = 0;
    vals[2] = 0;
    arg.array = vals;
    return semctl (m_id, 0, SETALL, arg);
  }

  int semaphore::getValue ()
  {
    union semun arg;
    return semctl (m_id, 0, GETVAL, arg);
  }

  void semaphore::wait (int semno, int semopr)
  {
    semop (semno, semopr);
  }

  void semaphore::post (int semno, int semopr)
  {
    semop (semno, semopr);
  }

  void semaphore::semop (int semno, int semopr)
  {
    struct sembuf ops[1];
    ops[0].sem_num = semno;
    ops[0].sem_op = semopr;
    ops[0].sem_flg = SEM_UNDO;

    int i = ::semop (m_id, ops, 1);
    if (i < 0)
      throw (semaphore_exception ("Cannot post semaphore."));
  }
}

#ifdef __TEST__

int main ()
{
  try
  {
    netmon::semaphore sem (0xff00ceca, IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH);
    sem.wait ();
    printf ("Acquire\n");
    sleep (5);
    sem.post ();
    printf ("Release\n");
  }
  catch (std::string e)
  {
    printf ("Fatal: %s\n", e.c_str ());
  }
}

#endif
