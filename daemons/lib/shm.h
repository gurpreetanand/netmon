#ifndef __SHM__
#define __SHM__

#include <sys/types.h>

namespace netmon
{
  class shm
  {
    public:
      shm (int, bool = false);
      shm (key_t, int, int, bool = false);
      ~shm ();

      void clear ();
      char *buffer ();
      int size ();
      void fill (unsigned char = 0x00);

    protected:
      int create (key_t, int, int);

      int m_id;
      void *m_addr;
      bool m_isAttached;
      bool m_toDestroy;
  };  
}

#endif // __SHM__
