#ifndef __SERVICE_ALERT_MANAGER__
#define __SERVICE_ALERT_MANAGER__

#include "alertmanager.h"

namespace netmon
{
  class conditional_manager;

  class service_alert_manager : public alert_manager
  {
    public:
      service_alert_manager (db *, bool = true, bool = true);
      virtual ~service_alert_manager ();

      unsigned int getDownAlerts (unsigned int, const values_map &);

      virtual void reload ();

    protected:
      virtual void loadStaticData ();
      virtual void getIDName (std::string &s)
      {
        s = "srv_id";
      }

      virtual bool isAlertValid (alert_data *);

      virtual void getUpName (std::string &) = 0;
      virtual void getDownName (std::string &) = 0;

      alerthandler *createDownAlertHandler (const alert_map::iterator &);

      conditional_manager *m_condManager;
      bool m_useUpAlert;
      std::string m_upTemplate;
      std::string m_upSubject;
      std::string m_downTemplate;
      std::string m_downSubject;
  };
}

#endif // __SERVICE_ALERT_MANAGER__
