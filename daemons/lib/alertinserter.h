#ifndef __ALERT_INSERTER__
#define __ALERT_INSERTER__

#include <list>
#include <string>

namespace netmon
{
  class db;

  class alert_inserter
  {
    public:
      struct command_data
      {
        std::string m_label;
        std::string m_command;
        bool m_async;
        bool m_recovery;
        bool m_failure;
        int m_timeout;
      };

      typedef std::list<command_data> commands_t;

      alert_inserter ();
      alert_inserter (int, const std::string &, const std::string &);
      ~alert_inserter ();

      int run (const commands_t &);

      const std::string &alertTemplate () const
      {
        return m_template;
      }

      std::string &alertTemplate ()
      {
        return m_template;
      }

      const std::string &alertSubject () const
      {
        return m_subject;
      }

      std::string &alertSubject ()
      {
        return m_subject;
      }

      const bool &recovery () const
      {
        return m_isRecovery;
      }

      bool &recovery ()
      {
        return m_isRecovery;
      }

    protected:
      int execCommand (const std::string &, bool, std::string &);
      void writeMemcacheAlert(int trigger_id, std::string subject, std::string message);
      void connectToDB ();
      void cleanUp ();
      bool insertAlert ();
      void dispatchDBusMessage ();

      int m_alertID;
      std::string m_template;
      std::string m_subject;
      db *m_db;
      bool m_isRecovery;
  };
}

#endif // __ALERT_INSERTER__
