#ifndef __SIG_TERM_HANDLER_H__
#define __SIG_TERM_HANDLER_H__

#include <nsignal.h>
#include "log.h"

namespace netmon
{
  class sig_term_handler : public event_handler
  {
    public:
      sig_term_handler ()
        : m_isTermSignaled (0)
      {}

      virtual int handle_signal (int)
      {
        m_isTermSignaled = 1;
        netmon::log::instance()->add(netmon::_eLogUrgent, "Killed by SIGTERM");
        return 0;
      }

      bool isTermSignaled ()
      {
        return m_isTermSignaled == 1;
      }

    protected:
      sig_atomic_t m_isTermSignaled;
  };
}

#endif // __SIG_TERM_HANDLER_H__
