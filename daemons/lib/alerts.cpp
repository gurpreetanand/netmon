#include <stdio.h>
#include <stdlib.h>

#include <map>
#include <string>
#include <sstream>

#include "alerts.h"
#include "netmon.h"
#include "alertinserter.h"

using namespace std;

namespace netmon
{
  static string translateEnum (alerthandler::TemplateKeywords e)
  {
    switch (e)
    {
      case alerthandler::eHostName:
        return "{$host}";
      case alerthandler::eHostIP:
        return "{$ip}";
      case alerthandler::eErrMsg:
        return "{$error_message}";
      case alerthandler::eSysErr:
        return "{$errvalue}";
      case alerthandler::eTime:
        return "{$time}";
      case alerthandler::eThreshold:
        return "{$threshold}";
      case alerthandler::eNewStatus:
        return "{$newstatus}";
      case alerthandler::eInterface:
        return "{$interface}";
      case alerthandler::eUtilization:
        return "{$utilization}";
      case alerthandler::ePort:
        return "{$port}";
      case alerthandler::eSysLogMsg:
        return "{$msg}";
      case alerthandler::eOpenPorts:
        return "{$ports}";
      case alerthandler::eMACAddr:
        return "{$mac}";
      case alerthandler::eInOut:
        return "{$inout}";
      case alerthandler::eProtocol:
        return "{$protocol}";
      case alerthandler::eSrcIP:
        return "{$srcip}";
      case alerthandler::eDstIP:
        return "{$dstip}";
      case alerthandler::eConnectionList:
        return "{$connections_list}";
      case alerthandler::eOID:
        return "{$oid}";
      case alerthandler::ePayload:
        return "{$payload}";
      case alerthandler::eLatency:
        return "{$latency}";
      case alerthandler::eLabel:
        return "{$label}";
      case alerthandler::ePattern:
        return "{$pattern}";
      case alerthandler::eSysLogSeverity:
        return "{$severity}";
      case alerthandler::ePartition:
        return "{$partition}";
      case alerthandler::eCommands:
        return "{$commands}";
      case alerthandler::eMsgID:
        return "{$msg_id}";
      case alerthandler::eDevice:
        return "{$device}";
    }
    return ""; // cannot reach this
  }

  string alerthandler::formatMessage (const char *tmpl)
  {
    string templ (tmpl);

    for (keywordsvalsmap::const_iterator i = m_kmap.begin (); i != m_kmap.end (); ++i)
    {
      string val = i->second;
      string keyword = translateEnum (i->first);
      replaceKeywordWithValue (templ, keyword, val);
    }

    return templ;
  }

  string alerthandler::formatMessageFromRefData (const char *tmpl)
  {
    string templ (tmpl);

    for (ref_data_map_t::const_iterator i = m_refmap.begin (); i != m_refmap.end (); ++i)
    {
      string val = i->second;
      string keyword = "{$obj." + i->first + "}";
      replaceKeywordWithValue (templ, keyword, val);
    }

    return templ;
  }

  void alerthandler::replaceKeywordWithValue (string &templ, const std::string &keyword, const std::string &val)
  {
    size_t pos = templ.find (keyword);

    while (pos != string::npos)
    {
      templ.replace (pos, keyword.size (), val);
      pos = templ.find (keyword, pos);
    }
  }

  void alerthandler::insertAlert (int id, const char *tmpl)
  {
    m_template = std::string (tmpl);
    m_alertID = id;

    insertAlert();
  }

  void alerthandler::insertAlert ()
  {
    alert_inserter::commands_t c;
    getCommands (c);
    setCommandsKeyWord (c);

    std::string ref_str = formatMessageFromRefData (m_template.c_str());
    std::string tmplt = formatMessage (ref_str.c_str());
    ref_str = formatMessageFromRefData (m_subject.c_str());
    std::string subject = formatMessage (ref_str.c_str());

    if (tmplt.size() == 0)
      return;

    alert_inserter ai (m_alertID, tmplt, subject);
    ai.recovery() = m_isRecovery;
    ai.run (c);
  }

  void alerthandler::getCommands (alert_inserter::commands_t &c)
  {
    std::ostringstream sql;
    std::string tmp;

    sql << "select label, command, async, timeout, perform_on_recovery, "
        << "perform_on_failure from alert_commands a, "
        << "alert_handler2command b where a.id = b.command_id and "
        << "b.handler_id = " << m_alertID;

    PGresult *res = m_db->execQuery (sql.str().c_str());

    for (int i = 0; i < PQntuples(res); ++i)
    {
      c.push_back (alert_inserter::command_data());
      c.back().m_label = std::string (PQgetvalue (res, i, 0));
      tmp = formatMessageFromRefData (PQgetvalue (res, i, 1));
      c.back().m_command = formatMessage (tmp.c_str());
      c.back().m_async = *PQgetvalue (res, i, 2) == 't' ? true : false;
      c.back().m_timeout = atoi (PQgetvalue (res, i, 3));
      c.back().m_recovery = *PQgetvalue (res, i, 4) == 't' ? true : false;
      c.back().m_failure = *PQgetvalue (res, i, 5) == 't' ? true : false;
    }

    PQclear (res);
  }

  void alerthandler::setCommandsKeyWord (alert_inserter::commands_t &c)
  {
    std::ostringstream tmp;
    tmp << "Alert Commands:\n";

    for (alert_inserter::commands_t::iterator i = c.begin(); i != c.end(); ++i)
    {
      tmp << "- " << (*i).m_command;
      if ((*i).m_async)
        tmp << " (asyncronous)";
      else
        tmp << " (non-asyncronous - see output below)";
      tmp << "\n";
    }
    if (c.size() != 0)
      addKeywordValue (eCommands, tmp.str().c_str());
    else
      addKeywordValue (eCommands, "");
  }

  void alerthandler::addKeywordValue (TemplateKeywords kw, const char *val, bool removebs)
  {
    string tmp = string (val);
    if (removebs)
    {
      size_t pos = tmp.find ('\n');
      if (pos != string::npos)
        tmp.erase (pos);
    }
    m_kmap[kw] = tmp;
  }

  void alerthandler::addKeywordValue (TemplateKeywords kw, int val)
  {
    ostringstream str;
    str << val;
    m_kmap[kw] = str.str ();
  }
}

#ifdef __MAIN__

#define DBCONNSTRINGLEN 64
#define MAXSQLQUERYLEN  512

int main ()
{
 PGconn *conn;
 char connstring  [DBCONNSTRINGLEN];
 const char *dbname = "netmon";
 const char *dbuser = "root";
 int i;

 sprintf (connstring, "dbname=%s user=%s", dbname, dbuser);
 conn = PQconnectdb (connstring);

 /* Check to see that the backend connection was successfully made */

 if (PQstatus (conn) == CONNECTION_BAD)
 {
   fprintf (stderr, "%s\n", PQerrorMessage(conn));
   PQfinish(conn);
   return -1;
 }

 templatemap mymap;

 getTemplates (conn, "srvmond", mymap);

 for (templatemap::iterator it = mymap.begin (); it != mymap.end (); ++it)
 {
   printf ("type: %s template: '%s'\n", it->first.c_str(), it->second.c_str ());
 }

 keywordsvalsmap vmap;
 vmap[eHostName] = "bitch";
 vmap[eHostIP] = "127.0.0.1";
 vmap[eThreshold] = "null";

 string str = formatMessage (mymap, "down", vmap);
 printf ("%s\n", str.c_str ());

 PQfinish (conn);

 return 0;
}

#endif // __MAIN__
