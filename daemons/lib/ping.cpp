#include <sys/socket.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>

#include "netmon.h"
#include "log.h"

/******************************************************************************
 *
 * Name: in_cksum(u_short *ptr, int nbytes)
 * Description: calculates checksum for raw IP packet
 * 
 * Arguments:
 * Returns: packet checksum
 *
 *****************************************************************************/
static int in_cksum(u_short *ptr, int nbytes)
{
  long		sum;                  /* assumes long == 32 bits */
  u_short	oddbyte;
  u_short	answer;	              /* assumes u_short == 16 bits */

  sum = 0;
  while (nbytes > 1)
  {
    sum += *ptr++;
    nbytes -= 2;
  }

  /* mop up an odd byte, if necessary */
  if (nbytes == 1)
  {
    oddbyte = 0;                               /* make sure top half is zero */
    *((u_char *) &oddbyte) = *(u_char *)ptr;   /* one byte only */
    sum += oddbyte;
  }

  sum  = (sum >> 16) + (sum & 0xffff);	       /* add high-16 to low-16 */
  sum += (sum >> 16);                          /* add carry */
  answer = ~sum;                               /* ones-complement, then truncate to 16 bits */

  return(answer);
}

/******************************************************************************
 *
 * Name: icmp_packet_ok(char *buf, int cc, struct sockaddr_in *from)
 * Description: checks to see if this is an echo reply we are expecting
 * 
 * Arguments:
 * Returns: 1 if yes, 0 otherwise
 *
 *****************************************************************************/
static int icmp_packet_ok(char *buf, int cc, struct sockaddr_in *from)
{
  int                   iphdrlen;
  struct ip             *ip;		/* ptr to IP header */
  register struct icmp  *icp;		/* ptr to ICMP header */
  struct timeval        tv;
  char                  *pr_type();

  from->sin_addr.s_addr = ntohl(from->sin_addr.s_addr);

  gettimeofday(&tv, (struct timezone *) 0);

  /*
   * We have to look at the IP header, to get its length.
   * We also verify that what follows the IP header contains at
   * least an ICMP header (8 bytes minimum).
   */

  ip = (struct ip *) buf;
  iphdrlen = ip->ip_hl << 2;	/* convert # 16-bit words to #bytes */

  if (cc < iphdrlen + ICMP_MINLEN)
  {
    netmon::log::instance()->add(netmon::_eLogInfo, "ICMP Packet length makes no sense.");
    return 0;
  }

  cc -= iphdrlen;

  icp = (struct icmp *)(buf + iphdrlen);
  if ((icp->icmp_type != ICMP_ECHOREPLY) && (icp->icmp_type != ICMP_ECHO))
  {
    /* The received ICMP packet is not an echo reply. */
    /* We accept echo request as reply, in case we pinged 127.0.0.1 */

    return 0;
  }

  /*
   * See if we sent the packet, and if not, just ignore it.
   */

  if (icp->icmp_id != getpid())
    return 0;

  return 1;
}

/******************************************************************************
 *
 * Name: send_ping(char *host)
 * Description: sends ICMP ECHO request and waits for reply
 * 
 * Arguments:
 * Returns: 0 if not succesfull, latency in microseconds otherwise
 *
 *****************************************************************************/

long send_ping(const char *host)
{
  int                  sockfd, fromlen, length;
  struct sockaddr_in   serv_addr, from;
  struct protoent      *proto;
  int                  protocol;
  struct timeval       tvstart;
  struct timeval       tvend;  
  long                 rvalue;

  register int             i;
  register struct icmp     *icp;       /* ICMP header */
  register u_char          *uptr;      /* start of user data */
  u_char                   sendpack[MAXPACKET];
  u_char                   recvpack[MAXPACKET];  

  /* Fill the structure serv_addr with the actuall server address */

  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family      = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(host);

  /* Check if ICMP is available */
  if ( (proto = getprotobyname("icmp")) == NULL)
    /*** getprotobyname failed, this is nonsense. We will assume the result is 1 for ICMP */
    /*** syslog(LOG_ERR, "getprotobyname(icmp) failed. Assuming 1.", SYSLOGLINELEN);      */
    protocol = 1;
  else
    protocol = proto->p_proto;

  /* Open a RAW socket */
  if ( (sockfd = socket(AF_INET, SOCK_RAW, protocol)) < 0)
  {
    netmon::log::instance()->add(netmon::_eLogError, "Could not allocate raw socket: %s.",
            strerror(errno));

    return 0;
  }

  /* Fill in the ICMP header */

  icp = (struct icmp *) sendpack;   /* pointer to ICMP header */
  icp->icmp_type  = ICMP_ECHO;
  icp->icmp_code  = 0;
  icp->icmp_cksum = 0;
  icp->icmp_id    = getpid();
  icp->icmp_seq   = 0;

  /* Add the time stamp of when we sent it */

  gettimeofday((struct timeval *) &sendpack[SIZE_ICMP_HDR],
               (struct timezone *) 0);

  if (gettimeofday(&tvstart, (struct timezone *) 0) != 0)
    /* Why the hack would gettimeofday fail ?! */
    return 0;

  /* Fill the remainder of the packet with the user data */

  uptr = &sendpack[SIZE_ICMP_HDR + SIZE_TIME_DATA];
  for (i = SIZE_TIME_DATA; i < DEF_DATALEN; i++)
    *uptr++ = i;

  /* Compute and store ICMP checksum */

  icp->icmp_cksum = in_cksum((u_short *) icp, DEF_DATALEN);

  // set timeout value to 2 second
  timeval tv;
  tv.tv_sec = 2;
  tv.tv_usec = 0;
  setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));

  /* Now send the datagram */
  i = sendto(sockfd, sendpack, DEF_DATALEN, 0,
             (struct sockaddr *) &serv_addr, sizeof(serv_addr));

  if (i < 0 || i != DEF_DATALEN)
  {
    netmon::log::instance()->add(netmon::_eLogError, "icmp sendto error (%d): %s", errno, strerror(errno));
    close(sockfd);

    return 0;
  }

  /* OK, let's hope we'll get a reply now */

  /* Set the timeout to PINGRESPONSETIME seconds */
  alarm(PINGRESPONSETIME);

  fromlen = sizeof (from);
  if ((length = recvfrom(sockfd, recvpack, sizeof(recvpack), 0,
                (struct sockaddr *) &from, (socklen_t *) &fromlen)) < 0)
  {
    close(sockfd);

    if (errno == EINTR)
     return 0;       /* We timed out */

    /* Some other error happened */

    /* Clear alarm */
    alarm(0);

    return 0;
  }

  /* Clear alarm */

  alarm(0);

  if (gettimeofday(&tvend, (struct timezone *) 0) != 0)
    /* Why the hack would gettimeofday fail ?! */
    return 0;

  /* We got reply, check if it is from the same host, well */
  /* who else, but just in case */

  if (!icmp_packet_ok((char *) recvpack, length, &from))
  {
    /* Who did we get this from?! */

    close(sockfd);

    return 0;
  }

  close(sockfd);

  rvalue = (tvend.tv_sec - tvstart.tv_sec) * 1000000
          + (tvend.tv_usec - tvstart.tv_usec);

  if (rvalue < 0)
    /* This can happed ONLY if gettimeofday returns incorrect times */
    rvalue = 0;

  return rvalue;
}
