#ifndef __ARP_MON__
#define __ARP_MON__

#include <string>
#include <list>
#include <mqueue.h>

#include "cache.h"
#include "../lib/inetaddr.h"
#include "../lib/db.h"

namespace netmon
{
  typedef std::pair<std::string, std::string> localnet;
  typedef std::list<localnet> netlist;
  typedef std::list<std::string> strlist;

  class arp_alert_manager;
  class db;

  class arpmonitor
  {
    public:
      arpmonitor (db *, const strlist &);
      ~arpmonitor ();

      void run () throw (std::string);

    protected:
      void init ();
      void populate ();
      void getNetworks ();
      void updateCache (const MACAddr &, const inetaddr &);
      void checkAlerts (const MACAddr &, const inetaddr &);
      void getDeviceAndInterface (const MACAddr &, std::string &, std::string &);
      void addLocalAddr ();
      void getLocalAddresses ();

      db *m_db;
      arp_alert_manager *m_alertManager;
      mqd_t m_queue;
      netlist m_networks;
      cache m_cache;
      strlist m_devices;
      unsigned int m_run;

      struct localaddr
      {
        localaddr (const std::string &device, const MACAddr &mac, const inetaddr &addr)
          : m_device(device), m_mac(mac), m_addr(addr)
        {}
        std::string m_device;
        MACAddr m_mac;
        inetaddr m_addr;
      };
      std::list<localaddr> m_localaddrs;
      static const unsigned int m_maxPollIPs = 10;
  };
}

#endif // __ARP_MON__
