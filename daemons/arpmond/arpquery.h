#ifndef __ARP_QUERY__
#define __ARP_QUERY__

#include <stdlib.h>
#include <sys/time.h>
#include <event.h>
#include <stdint.h>
#include <netinet/in.h>
#include <linux/if_packet.h>

#include <list>
#include <set>
#include <string>

#include "cache.h"
#include "exception.h"

namespace netmon
{
  class arp_exception : public netmon_exception
  {
    public:
      arp_exception (const std::string &err)
        : netmon_exception (err)
      {}
  };

  struct ip_mac
  {
    ip_mac (uint32_t ip, const MACAddr &mac)
      : m_ip(ip), m_mac(mac)
    {}
    uint32_t m_ip;
    MACAddr m_mac;
  };

  class arpquery
  {
    public:
      arpquery (const char *, uint32_t);
      ~arpquery ();

      void query (const std::list<std::string> &);
      void poll (int = 2);
      void getResults (std::list<ip_mac> &);

    protected:
      bool createSocket ();
      bool isDeviceValid (const char *);
      void bindSocket ();
      int sendPacket ();
      void query (const char *);
      int recievePacket (unsigned char *, int, struct sockaddr_ll *);

      static void onRead (int, short, void *);
      void onRead ();

      std::list<ip_mac> m_results;
      std::set<uint32_t> m_sentIPs;

      in_addr m_src, m_dst;
      sockaddr_ll m_me;
      sockaddr_ll m_he;
      int m_ifindex;
      int m_socket;
      char m_device[16];
      event m_event;
  };
}

#endif // __ARP_QUERY__
