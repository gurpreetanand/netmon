#define _BSD_SOURCE

#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <netdb.h>
#include <time.h>
#include <string.h>

#include "../lib/netmon.h"

#define PROMISCOUS  1

#ifndef ETHER_ADDR_LEN
#define ETHER_ADDR_LEN 6
#endif

/* Other definitions */
#define SYSLOGLINELEN    256
#define DBNAMELEN        32
#define DBUSERLEN        32
#define CLEANUPLEN       32
#define SYSLOGFACILITYLEN 32

void OpenDatabase();
void CloseDatabase();
void CleanUp();
void ExecuteSQL(char *sql);
void RecordPacket(char *sip,
		u_char smac0,
		u_char smac1,
		u_char smac2,
		u_char smac3,
		u_char smac4,
		u_char smac5,
		u_short,
		u_short
		);
void ProcessPacket(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void HandleSignal(int signo);

char syslogbuff[SYSLOGLINELEN];
int daemon_init(void);
int interactive = 0;
char syslogfacility [SYSLOGFACILITYLEN];
int  facility;
int  daemonstate    = 0;
long arpmond_start;

/* Now entries we will need to validate the host name by DNS */
struct hostent *hp;
unsigned long addr;
struct in_addr in;
char **p;
char **q;

/* Ethernet header */

struct sniff_ethernet 
{
  u_char  dhost[ETHER_ADDR_LEN];    /* Destination host address */
  u_char  shost[ETHER_ADDR_LEN];    /* Source host address */
  u_short type;                     /* IP? ARP? RARP? etc */
};

/* IP header */

struct sniff_ip 
{
  #if BYTE_ORDER == BIG_ENDIAN             /* Sparc, Motorola, etc */
  u_int   version:4,		           /* version */
          hdrlen:4;		           /* header length */
  #elif BYTE_ORDER == LITTLE_ENDIAN        /* Intel */
  u_int   hdrlen:4,		           /* header length */
          version:4;		           /* version */
  #else
  #error "Please fix endian.h"
  #endif  
  u_char  tos;		/* type of service */
  u_short len:16;	/* total length */
  u_short id;		/* identification */
  u_short offset;	/* fragment offset field */
  u_char  ttl;		/* time to live */
  u_char  protocol;	/* protocol */
  u_short sum;		/* checksum */
  struct in_addr src;   /* source address */
  struct in_addr dst;   /* dest address */
};

/* TCP header */
typedef u_int32_t tcp_seq;

struct sniff_tcp {
  u_short sport;                       /* source port */
  u_short dport;                       /* destination port */
  tcp_seq seq;                         /* sequence number */
  tcp_seq ack;                         /* acknowledgement number */
  #if BYTE_ORDER == LITTLE_ENDIAN      /* Intel platform */
    u_int unused:4,                    /* (unused) */
          offset:4;                    /* data offset */
  #elif BYTE_ORDER == BIG_ENDIAN       /* Sparc, Motorola, etc */
    u_int offset:4,                    /* data offset */
          unused:4;                    /* (unused) */
  #else
  #error "Please fix endian.h"
  #endif  
  u_char  flags;
  #define FIN  0x01
  #define SYN  0x02
  #define RST  0x04
  #define PSH  0x08
  #define ACK  0x10
  #define URG  0x20
  u_short win;                         /* window */
  u_short sum;                         /* checksum */
  u_short urp;                         /* urgent pointer */
};

/* UDP header */

struct sniff_udp {
  u_short sport;           /* source port */
  u_short dport;           /* destination port */
  u_short len; 	           /* udp package length */
  u_short sum;	           /* udp package checksum */
};

static int netmonlog(int type, char *bufp, int len);
static inline int is_valid_ether_addr (const u_char *addr)
{
  const char zaddr[6] = {0,};

  return !(addr[0]&1) && memcmp( addr, zaddr, 6);
}

/******************************************************************************
 *
 * Name: main()
 * Description: Calls signal handler, goes daemon, opens the database,
 *              opens syslog and opens device for sniffing
 * Returns: 0, or 1 in case of error
 * Arguments:
 *
 *****************************************************************************/
int main(int argc, char *argv[])
{
  char dev[16];                   /* Sniffing device */
  char errbuf[PCAP_ERRBUF_SIZE];  /* Error buffer */
  pcap_t *descr;                  /* Sniff handler */

  struct bpf_program fp;          /* hold compiled program */
  bpf_u_int32 maskp;              /* subnet mask */
  bpf_u_int32 netp;               /* ip */
  char filter_app[] = "";  /* No filter */

  arpmond_start = time(NULL);

  /* Get command line options */
  if (argc >1)
  {
    if (!strcmp(argv[1], "-i"))
      interactive = 1;
    else
    {
      printf("Usage: %s [-i]\n", argv[0]);
      exit(0);
    }
  }

  /* Signal handler */

  installSigHandler (HandleSignal);

  /* Parse configuration file */
  
  /* Set our device */
  // dev = pcap_lookupdev(errbuf);

//  strcpy(dev, "eth0");
  strcpy(dev, "vmnet1");

  pcap_lookupnet(dev, &netp, &maskp, errbuf);

  /* Print device to syslog */
  
  sprintf(syslogbuff, "arpmond started. Device: [%s]\n", dev);
  netmonlog(LOG_NOTICE, syslogbuff, 128);

  /* Open the device */
  
  descr = pcap_open_live(dev, BUFSIZ, PROMISCOUS, 0, errbuf);
  if (descr == NULL)
  {
    sprintf(syslogbuff, "pcap_open_live(): %s\n", errbuf);
    netmonlog(LOG_ERR, syslogbuff, 128);
    exit(1);
  }

  /* Apply the rules */

  if( pcap_compile(descr, &fp, filter_app, 0, netp) == -1)
  {
    sprintf(syslogbuff, "pcap_compile failed\n");
    netmonlog(LOG_ERR, syslogbuff, 128);
    exit(1);
  }
  if (pcap_setfilter(descr, &fp) == -1)
  {
    sprintf(syslogbuff, "pcap_setfilter failed\n");
    netmonlog(LOG_ERR, syslogbuff, 128);
    exit(1);
  }

  /* Start the callback function */

  while (1)
  {
    /* In the first minute, catch up to 10000 packets */
    if ((time(NULL) - arpmond_start) < 60)
      pcap_loop(descr, 100, ProcessPacket, NULL);

    /* Process 10 packets, then sleep for a minute, then go again */
    pcap_loop(descr, 10, ProcessPacket, NULL);
    
    sleep(60);
  }
  
  pcap_close(descr);

  /* Clean up, close open handles */

  if (interactive == 0)
    CleanUp();
  
  return 0;
}

/******************************************************************************
 *
 * Name: CleanUp
 * Description:  frees allocated memory, closes files and database connections
 * Arguments:
 * Returns: void
 *
 *****************************************************************************/
void CleanUp()
{
  closelog();
}

/******************************************************************************
 *
 * Name: RecordPacket
 * Description:  insert IP packet's description to the database
 * Arguments: protocol (TCP/UDP), source IP address, source port number,
 *            destination IP address, destination port number,
 *            number of data bytes in packet, unix timestamp              
 * Returns: void, in case of error ExecuteSQL will report message and exit.
 *
 *****************************************************************************/
void RecordPacket(
  char *sip, 
		u_char smac0,
		u_char smac1,
		u_char smac2,
		u_char smac3,
		u_char smac4,
		u_char smac5, u_short sport, u_short dport
  )
{
  char sqlstmt[1024];
  int  entries;
  long timestamp;

  printf ("IP: %s  %02X:%02X:%02X:%02X:%02X:%02X  %d->%d\n", sip, smac0, smac1, smac2, smac3, smac4, smac5, ntohs (sport), ntohs (dport));
}

/******************************************************************************
 *
 * Name: ProcessPacket
 * Description:  analyzing packet and logging it if it is of interest
 * Arguments: pcap arguments (not used), pcap header, packet
 * Returns: void, in case of error sub-functions will report message and exit.
 *
 *****************************************************************************/
void ProcessPacket(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
  const struct sniff_ethernet *ethernet;  /* The ethernet header */
  const struct sniff_ip *ip;              /* The IP header */
  const struct sniff_tcp *tcp;            /* The TCP header */
  const struct sniff_udp *udp;		  /* The UDP header */
  void  *data;

  int size_ethernet = sizeof(struct sniff_ethernet);
  int size_ip = sizeof(struct sniff_ip);
  int size_tcp = sizeof(struct sniff_tcp);

  char src_ip[15];
  char dst_ip[15];
  int i;

#ifdef LINUXSPARC
  u_char abuf[68];
#endif

  /* We need this information in case of segfault */
  daemonstate = 1;

  /* Point our structures to the right place in the packet */

  ethernet = (struct sniff_ethernet*)(packet);
  if (ethernet->type != htons (0x0800))
  {
    printf ("%04X is not IP protocol!\n", ntohs (ethernet->type));
    return;
  }
  if (!(is_valid_ether_addr (ethernet->dhost) && is_valid_ether_addr (ethernet->shost)))
  {
    printf ("Not valid eth address (");
    for (int i = 0; i < 6; ++i)
      printf ("%02X:", ethernet->dhost[i]);
    printf (" - ");
    for (int i = 0; i < 6; ++i)
      printf ("%02X:", ethernet->dhost[i]);
    printf (")\n");
    return;
  }
  
  ip = (struct sniff_ip*)(packet + size_ethernet);
  tcp = (struct sniff_tcp*)(packet + size_ethernet + size_ip);
  udp = (struct sniff_udp*)(packet + size_ethernet + size_ip);  
  data = (void *)(packet + size_ethernet + size_ip + size_tcp);

#ifdef LINUXSPARC
  if ((long)ip & 3) {
                memcpy((char *)abuf, (char *)ip, 68);
                ip = (struct ip *)abuf;
                tcp = (struct sniff_tcp*)(abuf + size_ip);   
                udp = (struct sniff_udp*)(abuf + size_ip);
  }
#endif

  /* We are not interested in anything else than IP protocol */
  if (ip->version != 4) return;

  /* We need this information in case of segfault */
  daemonstate = 2;

  /* Get IP's, inet_ntoa overwrites statically allocated buffer in subsequent calls */
  
  strcpy(src_ip, inet_ntoa(ip->src));
  strcpy(dst_ip, inet_ntoa(ip->dst));

  /* We need this information in case of segfault */
  daemonstate = 3;

  RecordPacket(src_ip,
    	ethernet->shost[0],
  	ethernet->shost[1],
  	ethernet->shost[2],
  	ethernet->shost[3],
  	ethernet->shost[4],
  	ethernet->shost[5], tcp->sport, tcp->dport);
  	
  RecordPacket(dst_ip,
    	ethernet->dhost[0],
  	ethernet->dhost[1],
  	ethernet->dhost[2],
  	ethernet->dhost[3],
  	ethernet->dhost[4],
  	ethernet->dhost[5], tcp->sport, tcp->dport);

}

/******************************************************************************
 *
 * Name: HandleSignal
 * Description:  handles TERM, USR1, USR2 and HUP signals
 * Arguments: signal number
 * Returns: void
 *
 *****************************************************************************/
void HandleSignal(int signo)
{
  switch (signo)
  {
    case SIGTERM:

      /* This is a normal way for a daemon to terminate */
    
      CleanUp();
      
      /* Let's put a mark in syslog that we are leaving now */
      
      sprintf(syslogbuff, "arpmond terminated in normal fashion.");
      netmonlog(LOG_NOTICE, syslogbuff, 128);
    
      /* Exit with 0, we are leaving with no hard feelings */
      
      exit(0);
    
    case SIGSEGV:

      sprintf(syslogbuff, "Segmentation violation detected, daemon state = %d.", daemonstate);      
      syslog(LOG_ERR, syslogbuff);

      /* Let's put a mark in syslog that we are leaving now */

      sprintf(syslogbuff, "arpmond terminated");
      netmonlog(LOG_NOTICE, syslogbuff, 128);

      /* Exit with -1 */

      exit(-1);

      break;

    /* default: */
    
      syslog(LOG_ERR, "Unknown signal received.");
      
      /* Try to ignore */
      /* We don't want to be killed by USR1, USR2 or HUP signals */
  }
  return;
}

/******************************************************************************
*
* Name: netmonlog()
* Description: Wrapper for syslog/printf function
*             
* Arguments: same arguments as syslog
* Returns: 0
*
*****************************************************************************/
static int netmonlog(int type, char *bufp, int len)
{
  if (interactive == 0)
    syslog(type, bufp, len);
  else
    printf("%s\n", bufp);
    
  return 0;
}

int sgets(char *dst, char *src, int n)
{
  int counter = 0;
  char *spointer, *dpointer;

  spointer = src;
  dpointer = dst;

  while ((*spointer != '\n') && (*spointer != '\r') && (*spointer != '\0') && (counter < n-1))
  {
    counter++;   
    *dpointer++ = *spointer++;
  }
  *dpointer = '\0';
  
  return counter;
}

int hostget(char *dst, char *src)
{
  char *host;

  if ((host = strstr(src, "Host: ")) != NULL)
  {
    host += 6;
    return sgets(dst, host, 80);
  }

  return 0;
}

/******************************************************************************
*
* Name: isHostValid()
* Description: Checks if an IP is a valid IP for a hostname
*             
* Arguments: hostname, ip
* Returns: 1 if yes, 0 if not
*
*****************************************************************************/

int isHostValid(char *hostname, char *ip)
{
  char nextip[32];

  if ((hp = gethostbyname(hostname)) == NULL)
  {            
    // DEBUG CODE HERE

    switch (h_errno)
    {          
      case HOST_NOT_FOUND:
        syslog(LOG_DEBUG, "HOST_NOT_FOUND");
        break; 
      case TRY_AGAIN:
        syslog(LOG_DEBUG, "TRY_AGAIN");
        break; 
      case NO_RECOVERY:
        syslog(LOG_DEBUG, "NO_RECOVERY");
        break; 
      case NO_ADDRESS:
        syslog(LOG_DEBUG, "NO_ADDRESS");
        break; 
      default: 
        syslog(LOG_DEBUG, "Have no idea what happened");
    }          

    /* host information not found, return */
    return 0;  
  }            

  for (p = hp->h_addr_list; *p != 0; p++)
  {
    memcpy(&in.s_addr, *p, sizeof (in.s_addr));

    strcpy(nextip, inet_ntoa(in));

    if (strcmp(nextip, ip) == 0)
    /* Found it ! */
      return 1;
  }

  /* Entry not valid */

  return 0;
}
