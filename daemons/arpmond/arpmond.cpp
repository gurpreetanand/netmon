#include "arpmond.h"
#include "arpmonitor.h"
#include "log.h"

#include <string>

namespace netmon
{
  arpmon_daemon::arpmon_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  arpmon_daemon::~arpmon_daemon ()
  {
    delete m_arpMonitor;
  }

  void arpmon_daemon::init ()
  {
    daemon::init();

    netmon::strlist devices;
    devices.push_back (std::string ("eth0"));
    devices.push_back (std::string ("eth1"));
    devices.push_back (std::string ("eth2"));
    devices.push_back (std::string ("eth3"));

    m_arpMonitor = new arpmonitor (m_db, devices);
  }

  int arpmon_daemon::run ()
  {
    while (!isTermSignaled())
    {
      m_arpMonitor->run();
//      netmon::log::instance()->add(netmon::_eLogInfo, "New scan"); //commented on 14/12/18 by sameer
    }
    return 0;
  }
}
