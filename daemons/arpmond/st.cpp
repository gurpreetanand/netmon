#include <string>
#include <cstdio>
#include <set>

using namespace std;

static void readMAC (const char *in, u_char *out)
{
  const char *p = in;
  bool first = true;
  int cnt = 0;
  u_char prev = 0;
  while (*p)
  {
    if (*p == ':')
    {
      ++p;
      continue;
    }
    u_char i = 0;
    if (*p >= 'a' && *p <= 'f')
    {
      i = *p - 'a' + 10;
    }
    else if (*p >= 'A' && *p <= 'F')
    {
      i = *p - 'A' + 10;
    }
    else
    {
      i = *p - '0';
    }
    if (first)
    {
      first = false;
      i <<= 4;
      prev = i;
      ++p;
      continue;
    }
    *(out + cnt) = i | prev;
    printf ("%02X\n", i | prev);
    first = true;
    prev = 0;
    ++p;
  }
}

int main ()
{
//  string p = "\x01\x00\x03\x10";
//  printf ("%d\n", p.length ());
  const char *m = "00:FE:02:AC:35:22";
  u_char mac[6];
  readMAC (m, mac);

  set <string> s;
  s.insert ("sisa");
  for (set<string>::iterator it = s.begin (); it != s.end (); ++it)
    printf ("%s\n", (*it).c_str ());
}
