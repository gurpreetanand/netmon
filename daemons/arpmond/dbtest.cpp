#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <string>
#include <sstream>
#include <cstdio>
#include <cstdlib>

#include "../lib/netmon.h"
#include "../lib/db.h"

// global stuff from netmon.lib
char dbname [DBNAMELEN];
char dbuser [DBUSERLEN];
char syslogbuff [SYSLOGLINELEN];
char syslogfacility [SYSLOGFACILITYLEN];
int facility;
int errvalue;

int main (int argc, char **argv)
{
  readConfig ();

  try
  {
    netmon::db db;
    db.connect (dbname, dbuser);

    std::ostringstream sql;
    srand ((int) time(NULL));

    db.execSQL ("BEGIN");
    for (int i = 0; i < 100000; ++i)
    {
      sql.str("");
      in_addr adr;
      adr.s_addr = rand();
      in_addr adr2;
      adr2.s_addr = rand();
      sql << "INSERT INTO AGG_TRAFFIC VALUES ('" << inet_ntoa (adr) << "', " << rand() % 65535
          << ", '" << inet_ntoa (adr2) << "', " << rand() % 65535 << ", " << rand() 
          << ", DEFAULT, " << (int) time(0) << ")";


      db.execSQL (sql.str().c_str());
      if (i % 100 == 0)
        printf ("%d\n", i);
    }
    db.execSQL ("COMMIT");
  }
  catch (std::string &e)
  {
    printf ("Error: %s\n", e.c_str ());
  }
  catch (int e)
  {
    errno = e;
    perror ("");
  }
  return 0;
}
