#ifndef __ARP_ALERT_MANAGER__
#define __ARP_ALERT_MANAGER__

#include "../lib/alertmanager.h"

namespace netmon
{
  class arp_alert_manager : public alert_manager
  {
    public:
      arp_alert_manager (db *mydb)
        : alert_manager (mydb, false)
      {}

      virtual ~arp_alert_manager ()
      {}

    protected:
      virtual void getReferenceTable (std::string &table)
      {
        table = "arptable";
      }

      virtual void getOverName (std::string &name)
      {
        name = "NEW_HOST";
      }

      virtual void getBelowName (std::string &name)
      {
      }

      virtual void createAlertQuery ();
  };
}

#endif // __ARP_ALERT_MANAGER__
