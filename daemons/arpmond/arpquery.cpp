#include "arpquery.h"
#include "../lib/inetaddr.h"
#include "../lib/log.h"

#include <stdlib.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <linux/sockios.h>
#include <sys/file.h>
#include <sys/time.h>
#include <sys/signal.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>

static void print_hex (unsigned char *p, int len)
{
  int i;
  for (i = 0; i < len; i++)
  {
    printf ("%02X", p[i]);
    if (i != len - 1)
      printf (":");
  }
}

namespace netmon
{
  arpquery::arpquery (const char *device, uint32_t localip)
  {
    if (!createSocket ())
      throw (arp_exception ("Cannot create socket"));
    if (!isDeviceValid (device))
      throw (arp_exception ("Invalid device"));
    strncpy (m_device, device, 15);
    bindSocket ();
    event_set (&m_event, m_socket, EV_READ | EV_PERSIST, onRead, this);
    event_add (&m_event, 0);
    m_src.s_addr = localip;
  }

  arpquery::~arpquery ()
  {
    close (m_socket);
    event_del (&m_event);
  }

  void arpquery::onRead (int fd, short flags, void *me)
  {
    arpquery *a = reinterpret_cast<arpquery *> (me);
    a->onRead ();
  }

  void arpquery::onRead ()
  {
    int cc;
    unsigned char packet[4096];
    struct sockaddr_ll from;
    int alen = sizeof (from);
printf ("onread()\n");
    if ((cc = recvfrom (m_socket, packet, sizeof (packet), 0, (struct sockaddr *) &from, (socklen_t *) &alen)) < 0)
    {
        log::instance()->add(_eLogInfo, "recvfrom");
        return;
    }
    recievePacket (packet, cc, &from);
  }

  void arpquery::query (const std::list<std::string> &ips)
  {
    m_results.clear ();
    for (std::list<std::string>::const_iterator i = ips.begin(); i != ips.end(); ++i)
      query ((*i).c_str());
  }

  void arpquery::poll (int seconds /* = 2 */)
  {
    if (seconds < 1)
      throw (arp_exception ("Illegal timeout value"));

    timeval tv;
    tv.tv_sec = seconds;
    tv.tv_usec = 0;
    event_loopexit (&tv);
    event_dispatch ();
  }

  void arpquery::getResults (std::list<ip_mac> &res)
  {
    res = m_results;
  }

  bool arpquery::createSocket ()
  {
    m_socket = socket (PF_PACKET, SOCK_DGRAM, 0);
    if (m_socket < 0)
    {
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      log::instance()->add(_eLogFatal, strerror (errno));
      return false;
    }
    int flags;
    flags = fcntl (m_socket, F_GETFL);
    flags |= O_NONBLOCK | O_ASYNC;
    fcntl (m_socket, F_SETFL, flags);

    timeval tv;
    tv.tv_sec = 2;
    tv.tv_usec = 0;
    setsockopt (m_socket, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof (tv));
    flags = 4096;
    setsockopt (m_socket, SOL_SOCKET, SO_RCVBUF, (void *) &flags, sizeof (flags));
    return true;
  }

  bool arpquery::isDeviceValid (const char *device)
  {
    struct ifreq ifr;
    memset (&ifr, 0, sizeof (ifr));
    strncpy (ifr.ifr_name, device, IFNAMSIZ - 1);
    if (ioctl (m_socket, SIOCGIFINDEX, &ifr) < 0)
    {
      log::instance()->add(_eLogError, "Unknown device '%s'", device);
      return false;
    }
    m_ifindex = ifr.ifr_ifindex;

    if (ioctl (m_socket, SIOCGIFFLAGS, (char *) &ifr))
    {
      log::instance()->add(_eLogError, "ioctl(SIOCGIFFLAGS)");
      return false;
    }
    if (!(ifr.ifr_flags & IFF_UP))
    {
      log::instance()->add(_eLogError, "Interface '%s' is down", device);
      return false;
    }
    if (ifr.ifr_flags & (IFF_NOARP | IFF_LOOPBACK))
    {
      log::instance()->add(_eLogError, "Interface '%s' is not ARPable", device);
      return false;
    }
    return true;
  }

  void arpquery::query (const char *ip)
  {
    log::instance()->add(_eLogDebug, "Querying %s", ip);
    printf ("Querying %s\n", ip);
    if (inet_aton (ip, &m_dst) == 0)
    {
      log::instance()->add(_eLogError, "invalid IP address '%s'", ip);
      throw (arp_exception ("Invalid IP address"));
    }
    m_he = m_me;
    memset (m_he.sll_addr, -1, m_he.sll_halen);
    m_sentIPs.insert (m_dst.s_addr);
    (void) sendPacket ();
  }

  void arpquery::bindSocket ()
  {
    m_me.sll_family = AF_PACKET;
    m_me.sll_ifindex = m_ifindex;
    m_me.sll_protocol = htons (ETH_P_ARP);
    if (bind (m_socket, (struct sockaddr *) &m_me, sizeof (m_me)) == -1)
    {
      log::instance()->add(_eLogError, "bind");
      throw (arp_exception ("Cannot bind"));
    }
    int alen = sizeof (m_me);
    if (getsockname (m_socket, (struct sockaddr *) &m_me, (socklen_t *) &alen) == -1)
    {
      log::instance()->add(_eLogError, "getsockname");
      throw (arp_exception ("getsockname failed"));
    }
    if (m_me.sll_halen == 0)
    {
      log::instance()->add(_eLogError, "Interface '%s' is not ARPable (no ll address)", m_device);
      throw (arp_exception ("Interface is not ARPable"));
    }
  }

  int arpquery::sendPacket ()
  {
    int err;
    struct timeval now;
    unsigned char buf[256];
    struct arphdr *ah = (struct arphdr *) buf;
    unsigned char *p = (unsigned char *) (ah + 1);

    ah->ar_hrd = htons (m_me.sll_hatype);
    if (ah->ar_hrd == htons (ARPHRD_FDDI))
      ah->ar_hrd = htons (ARPHRD_ETHER);
    ah->ar_pro = htons (ETH_P_IP);
    ah->ar_hln = m_me.sll_halen;
    ah->ar_pln = 4;
    ah->ar_op = htons (ARPOP_REQUEST);

    memcpy (p, &(m_me.sll_addr), ah->ar_hln);
    p += m_me.sll_halen;

    memcpy (p, &m_src, 4);
    p += 4;

    memcpy (p, &(m_he.sll_addr), ah->ar_hln);
    p += ah->ar_hln;

    memcpy (p, &m_dst, 4);
    p += 4;

    gettimeofday (&now, NULL);
    err = sendto (m_socket, buf, p - buf, 0, (struct sockaddr *) &m_he, sizeof (m_he));
    return err;
  }

  int arpquery::recievePacket (unsigned char *buf, int len, struct sockaddr_ll *FROM)
  {
    struct arphdr *ah = (struct arphdr *) buf;
    unsigned char *p = (unsigned char *) (ah + 1);
    struct in_addr src_ip, dst_ip;

    /* Filter out wild packets */
    if (FROM->sll_pkttype != PACKET_HOST &&
        FROM->sll_pkttype != PACKET_BROADCAST &&
        FROM->sll_pkttype != PACKET_MULTICAST)
      return 0;

    /* Only these types are recognised */
    if (ah->ar_op != htons (ARPOP_REQUEST) && ah->ar_op != htons (ARPOP_REPLY))
      return 0;

    /* ARPHRD check and this darned FDDI hack here :-( */
    if (ah->ar_hrd != htons (FROM->sll_hatype) &&
        (FROM->sll_hatype != ARPHRD_FDDI || ah->ar_hrd != htons (ARPHRD_ETHER)))
      return 0;

    /* Protocol must be IP. */
    if (ah->ar_pro != htons (ETH_P_IP))
      return 0;

    if (ah->ar_pln != 4)
      return 0;

    if (ah->ar_hln != m_me.sll_halen)
      return 0;

    if ((unsigned) len < sizeof (*ah) + 2 * (4 + ah->ar_hln))
      return 0;

    memcpy (&src_ip, p + ah->ar_hln, 4);
    memcpy (&dst_ip, p + ah->ar_hln + 4 + ah->ar_hln, 4);

    if (m_sentIPs.find(src_ip.s_addr) == m_sentIPs.end())
      return 0;

    if (m_src.s_addr != dst_ip.s_addr)
      return 0;

    if (memcmp (p + ah->ar_hln + 4, &m_me.sll_addr, ah->ar_hln))
      return 0;

    printf ("%s [", inet_ntoa (src_ip));
    print_hex (p, ah->ar_hln);
    printf ("] \n");

    m_results.push_back (ip_mac (src_ip.s_addr, MACAddr ((u_char *) p)));

    return 1;
  } 
}

#ifdef __MAIN__

using namespace netmon;

void doit (arpquery &a, inetaddr &from, inetaddr &to)
{
  std::list<std::string> ips;
  std::list<netmon::ip_mac> res;

  while (from < to)
  {
    ips.push_back (from.addr ());
    ++from;
  }
  a.query (ips);
  a.poll (3);
  a.getResults (res);
  in_addr addr;

  for (std::list<netmon::ip_mac>::iterator i = res.begin(); i != res.end(); ++i)
  {
    addr.s_addr = (*i).m_ip;
    printf ("%s [%s]\n", inet_ntoa (addr), (*i).m_mac.format().c_str());
  }
}

int main ()
{
  event_init ();
  try
  {
    netmon::inetaddr from ("10.0.2.171");
    netmon::inetaddr to ("10.0.2.181");
    const char *dev = "eth0";
    in_addr addr;
    inet_aton ("10.0.2.26", &addr);
    netmon::arpquery a(dev, addr.s_addr);
    doit (a, from, to);
    from = inetaddr ("10.0.2.181");
    to = inetaddr ("10.0.2.191");
    doit (a, from, to);
    return 0;
  }
  catch (const char *e)
  {
    printf ("error: %s\n", e);
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("exc: %s\n", e.what());
  }

  return 0;
}

#endif
