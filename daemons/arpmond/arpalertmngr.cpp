#include "arpalertmngr.h"
#include <sstream>

namespace netmon
{
  void arp_alert_manager::createAlertQuery ()
  {
    std::string table;
    std::ostringstream sql;

    getReferenceTable (table);

    sql << "select distinct a.id, -1"
        << ", b.triggered, b.trigger_threshold, b.pattern, "
        << "b.trigger_id, b.comp_exp, b.label, a.conditional_id, "
        << "b.throttle_interval "
        << "from alert_handlers a, alert_triggers b "
        << "where a.trigger_id = b.trigger_id and "
        << "b.active = 't' and "
        << "b.reference_table_name = '"
        << table << "' and b.trigger_threshold = -1";

    m_getAlertsQuery = sql.str();
  }
}
