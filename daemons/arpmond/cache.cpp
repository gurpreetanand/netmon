#include <cstring>

#include "cache.h"

namespace netmon
{
  cache::cache ()
  {
  }

  cache::~cache ()
  {
    for (machash::iterator it = m_hash.begin (); it != m_hash.end (); ++it)
      delete it->second;
    m_hash.clear ();
  }

  cache::MACStatus cache::addMAC (u_char *mac, const char *ip)
  {
    MACKey key (mac);
    return addMACImpl (key, ip);
  }

  cache::MACStatus cache::addMAC (const MACAddr &key, const char *ip)
  {
    return addMACImpl (key, ip);
  }

  cache::MACStatus cache::addMACImpl (const MACAddr &key, const char *ip)
  {
    machash::iterator it = m_hash.find (key);
    if (it != m_hash.end ())
    {
      if (it->second->m_IPs.find (std::string (ip)) != it->second->m_IPs.end ())
        return _eOld;
      it->second->m_IPs.insert (std::string (ip));
      return _eNewIP;
    }
    m_hash[key] = new MACEntry (key, ip);
    return _eNew;
  }
}
