#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

long getlocaladdr (u_char type, const char *eth, u_char *addr)
{
  struct ifreq ifr;
  struct ifreq *IFR;
  struct ifconf ifc;
  char buf[1024];
  int s, i;
  int ok = 0;

  s = socket (AF_INET, SOCK_DGRAM, 0);
  if (s == -1)
  {
    return -1;
  }

  ifc.ifc_len = sizeof (buf);
  ifc.ifc_buf = buf;
  ioctl (s, SIOCGIFCONF, &ifc);

  IFR = ifc.ifc_req;
  for (i = ifc.ifc_len / sizeof (struct ifreq); --i >= 0; IFR++)
  {
    strcpy (ifr.ifr_name, IFR->ifr_name);
    if (strcmp (ifr.ifr_name, eth) != 0)
      continue;
    if (ioctl (s, SIOCGIFFLAGS, &ifr) == 0)
    {
      if (!(ifr.ifr_flags & IFF_LOOPBACK))
      {
        if (type == 0)
        {
          if (ioctl (s, SIOCGIFADDR, &ifr) == 0)
          {
            ok = 1;
            break;
          }
        }
        else
          if (ioctl (s, SIOCGIFHWADDR, &ifr) == 0)
          {
            ok = 1;
            break;
          }
      }
    }
  }

  close (s);
  if (ok)
  {
    if (type == 0)
      bcopy (ifr.ifr_hwaddr.sa_data + 2, addr, 4);
    else
      bcopy( ifr.ifr_hwaddr.sa_data, addr, 6);
  }
  else
  {
    return -1;
  }
  return 0;
}

#ifdef __TEST__

int main ()
{
  unsigned long ip;
  struct in_addr addr;

  getlocaladdr (0, (u_char *) &ip);
  addr.s_addr = ip;
  printf ("%s\n", inet_ntoa (addr));

  u_char mac[6];
  getlocaladdr (1, mac);
  for (int i = 0; i < 5; ++i)
  {
    printf("%02X:", mac[i]);
  }
  printf("%02X", mac[5]);
  printf( "\n");
}

#endif
