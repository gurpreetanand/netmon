#ifndef __SNIFF__
#define __SNIFF__

#ifndef ETHER_ADDR_LEN
#define ETHER_ADDR_LEN 6
#endif

/* Ethernet header */

struct sniff_ethernet 
{
  u_char  dhost[ETHER_ADDR_LEN];    /* Destination host address */
  u_char  shost[ETHER_ADDR_LEN];    /* Source host address */
  u_short type;                     /* IP? ARP? RARP? etc */
};

/* IP header */

struct sniff_ip 
{
  #if BYTE_ORDER == BIG_ENDIAN             /* Sparc, Motorola, etc */
  u_int   version:4,		           /* version */
          hdrlen:4;		           /* header length */
  #elif BYTE_ORDER == LITTLE_ENDIAN        /* Intel */
  u_int   hdrlen:4,		           /* header length */
          version:4;		           /* version */
  #else
  #error "Please fix endian.h"
  #endif  
  u_char  tos;		/* type of service */
  u_short len:16;	/* total length */
  u_short id;		/* identification */
  u_short offset;	/* fragment offset field */
  u_char  ttl;		/* time to live */
  u_char  protocol;	/* protocol */
  u_short sum;		/* checksum */
  struct in_addr src;   /* source address */
  struct in_addr dst;   /* dest address */
};

/* TCP header */
typedef u_int32_t tcp_seq;

struct sniff_tcp {
  u_short sport;                       /* source port */
  u_short dport;                       /* destination port */
  tcp_seq seq;                         /* sequence number */
  tcp_seq ack;                         /* acknowledgement number */
  #if BYTE_ORDER == LITTLE_ENDIAN      /* Intel platform */
    u_int unused:4,                    /* (unused) */
          offset:4;                    /* data offset */
  #elif BYTE_ORDER == BIG_ENDIAN       /* Sparc, Motorola, etc */
    u_int offset:4,                    /* data offset */
          unused:4;                    /* (unused) */
  #else
  #error "Please fix endian.h"
  #endif  
  u_char  flags;
  #define FIN  0x01
  #define SYN  0x02
  #define RST  0x04
  #define PSH  0x08
  #define ACK  0x10
  #define URG  0x20
  u_short win;                         /* window */
  u_short sum;                         /* checksum */
  u_short urp;                         /* urgent pointer */
};

/* UDP header */

struct sniff_udp {
  u_short sport;           /* source port */
  u_short dport;           /* destination port */
  u_short len; 	           /* udp package length */
  u_short sum;	           /* udp package checksum */
};

#endif // __SNIFF__
