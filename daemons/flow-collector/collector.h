#ifndef __COLLECTOR__
#define __COLLECTOR__

namespace netmon
{
  class collector
  {
    public:
      collector () {}
      virtual ~collector () {}

      virtual void run () = 0;
  };
}

#endif // __COLLECTOR__
