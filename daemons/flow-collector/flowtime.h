#ifndef __FLOW_TIME__
#define __FLOW_TIME__

#include <stdint.h>
#include "nfheaders.h"
#include "flows.h"

namespace netmon
{
  class flow_time
  {
    public:
      flow_time ()
        : m_absTime (0)
      {}

      flow_time (nfheader &h)
      {
        m_absTime = h.getSeconds() - (h.getUpTime() / 1000);
      }

      uint32_t getStartTime (const flow &f) const
      {
        return m_absTime + (f.getStartTime() / 1000);
      }

      uint32_t getEndTime (const flow &f) const
      {
        return m_absTime + (f.getEndTime() / 1000);
      }

    protected:
      uint32_t m_absTime;
  };
}

#endif // __FLOW_TIME__
