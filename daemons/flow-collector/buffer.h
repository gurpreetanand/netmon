#ifndef __BUFFER__
#define __BUFFER__

#include <string>
#include <cstring>
#include <stdint.h>

#include "exception.h"

namespace netmon
{
  class read_exception : public netmon_exception
  {
    public:
      read_exception ()
        : netmon_exception ("Read pass the end of buffer")
      {}
  };

  class buffer_exception : public netmon_exception
  {
    public:
      buffer_exception ()
        : netmon_exception ("Null pointer")
      {}
  };

  class buffer
  {
    public:
      explicit buffer (const uint8_t *p, uint32_t size)
        : m_buf (p), m_size (size), m_pos (0)
      {
        if (p == 0 || size == 0)
          throw buffer_exception();
      }

      template <typename T>
      buffer &operator>> (T &t)
      {
        if (m_pos + sizeof (t) > m_size)
          throw read_exception();
        memcpy ((void *) &t, (void *)(m_buf + m_pos), sizeof (t));
        m_pos += sizeof (t);
        return *this;
      }

      void skip (uint32_t size)
      {
        if (m_pos + size > m_size)
          throw read_exception();
        m_pos += size;
      }

      const uint32_t &size() const
      {
        return m_size;
      }

      uint32_t read() const
      {
        return m_pos - m_size;
      }

      bool eof() const
      {
        return (m_pos - m_size) == 0;
      }

      const uint32_t &pos() const
      {
        return m_pos;
      }

      uint32_t &pos()
      {
        return m_pos;
      }

    protected:
      const uint8_t *m_buf;
      uint32_t m_size;
      uint32_t m_pos;
  };
}

#endif // __BUFFER__
