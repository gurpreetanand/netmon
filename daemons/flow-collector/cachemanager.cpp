#include "cachemanager.h"
#include "flowstoragedb.h"
#include "flowstorageliveshmdb.h"
#include "flows.h"
#include "flowtime.h"

namespace netmon
{
  cache_manager::cache_manager (db *mydb, const shm_sizes &s)
    : m_db (mydb), m_size (0), m_shmSizes (s)
  {
  }

  cache_manager::~cache_manager ()
  {
  }

  void cache_manager::addClient (uint32_t ip)
  {
    removeClient (ip);
    m_staticStorage[ip] = new flow_storage_db (ip, m_db);
  }

  void cache_manager::removeClient (uint32_t ip)
  {
    storagemap::iterator i = m_staticStorage.find (ip);
    if (i != m_staticStorage.end())
    {
      delete i->second;
      m_staticStorage.erase (i);
    }
  }

  void cache_manager::addInterface (uint32_t ip, uint16_t iface, uint32_t device, uint32_t shmkey)
  {
    removeInterface (ip, iface);
    ++m_size;
    livestoragemap::iterator i1 = m_liveStorage.find (ip);
    if (i1 == m_liveStorage.end())
    {
      m_liveStorage[ip][iface] = new flow_storage_live_shm_db (m_db, iface, device, shmkey, m_shmSizes.m_maxStreams, m_shmSizes.m_maxProtocols);
      return;
    }
    interfacemap::iterator i2 = i1->second.find (iface);
    if (i2 == i1->second.end())
    {
      i1->second[iface] = new flow_storage_live_shm_db (m_db, iface, device, shmkey);
      return;
    }
  }

  void cache_manager::removeInterface (uint32_t ip, uint16_t iface)
  {
    livestoragemap::iterator i1 = m_liveStorage.find (ip);
    if (i1 == m_liveStorage.end())
      return;

    interfacemap::iterator i2 = i1->second.find (iface);
    if (i2 == i1->second.end())
      return;

    --m_size;
    delete i2->second;
    i1->second.erase (i2);
    if (i1->second.empty())
      m_liveStorage.erase (i1);
  }

  void cache_manager::add (uint32_t ip, const flow &f, const flow_time &t)
  {
    bool added = false;
    uint16_t in = f.getInputInterface();
    uint16_t out = f.getOutputInterface();
    livestoragemap::iterator i1 = m_liveStorage.find (ip);
    if (i1 != m_liveStorage.end())
    {
      interfacemap::iterator i2 = i1->second.find (in);
      if (i2 != i1->second.end())
      {
        i2->second->add(f, t);
        m_staticStorage[ip]->add(f, t);
        added = true;
      }
      i2 = i1->second.find (out);
      if (i2 != i1->second.end())
      {
        i2->second->add(f, t);
        if (!added)
          m_staticStorage[ip]->add(f, t);
      }
    }
  }

  void cache_manager::store (int timestamp)
  {
    for (storagemap::iterator i = m_staticStorage.begin(); i != m_staticStorage.end(); ++i)
      i->second->store (timestamp);
  }

  void cache_manager::storeLive (int timestamp)
  {
    for (livestoragemap::iterator i = m_liveStorage.begin(); i != m_liveStorage.end(); ++i)
      for (interfacemap::iterator j = i->second.begin(); j != i->second.end(); ++j)
        j->second->store (timestamp);
  }

  void cache_manager::clear ()
  {
    for (storagemap::iterator i = m_staticStorage.begin(); i != m_staticStorage.end(); ++i)
      i->second->clear();
  }

  void cache_manager::clearLive ()
  {
    for (livestoragemap::iterator i = m_liveStorage.begin(); i != m_liveStorage.end(); ++i)
      for (interfacemap::iterator j = i->second.begin(); j != i->second.end(); ++j)
        j->second->clear();
  }
}
