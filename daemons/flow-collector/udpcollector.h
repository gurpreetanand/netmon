#ifndef __UDP_COLLECTOR__
#define __UDP_COLLECTOR__

#include <stdexcept>
#include <sys/types.h>
#include <event.h>
#include <stdint.h>
#include <stdint.h>
#include <netinet/in.h>

#include "exception.h"

namespace netmon
{
  class collector_exc : public netmon_exception
  {
    public:
      collector_exc (const std::string &e)
        : netmon_exception (e)
      {}
  };

  class client_manager;

  class udp_collector
  {
    public:
      udp_collector (int, client_manager *);
      virtual ~udp_collector ();

    protected:
      void onRead ();
      void init ();
      void initSocket ();

      static void onRead (int, short, void *);

      virtual void handleData () = 0;

      event m_readEvent;
      client_manager *m_clientMgr;
      int m_port;
      int m_fd;
      uint8_t *m_buf;
      ssize_t m_len;
      struct sockaddr_in m_from;
      enum { eBufsize = 2048 };
  };
}

#endif // __UDP_COLLECTOR__
