#include "sflowhandler.h"
#include "buffer.h"

#include <cstdio>

namespace netmon
{
  sflow_handler::sflow_handler (cache_manager *cache, uint32_t ip)
    : flow_handler (cache, ip)
  {}

  sflow_handler::~sflow_handler ()
  {}

  void sflow_handler::processBuffer (buffer &b)
  {
    m_header = sflow_header (b);

    uint32_t sType = 0;

    for (uint32_t i = 0; i < m_header.m_records; ++i)
    {
      b >> sType;
      sType = ntohl (sType);
      printf ("sample type: %d\n", sType);
      if (m_header.m_version >= eV5)
        handleSample (b, sType);
      else
        handleSampleV4 (b, sType);
    }
  }

  void sflow_handler::handleSample (buffer &b, uint32_t sType)
  {
    switch (sType)
    {
      case eFlowSample:
        handleFlowSample (b);
        break;
      case eCountersSample:
        handleCountersSample (b);
        break;
      case eFlowSampleEx:
        handleFlowSampleEx (b);
        break;
      case eCountersSampleEx:
        handleCountersSampleEx (b);
        break;
      default:
        {
          // unknown sample type
          uint32_t size;
          b >> size;
          size = ntohl (size);
          b.pos() += size;
        }
        break;
    }
  }

  void sflow_handler::handleSampleV4 (buffer &b, uint32_t sType)
  {
  }

  void sflow_handler::handleFlowSample (buffer &b)
  {
    printf ("handleFlowSample()\n");
    sflow_sample sample (b, m_header, m_cache, m_ip, false);
  }

  void sflow_handler::handleCountersSample (buffer &b)
  {
    printf ("handleCountersSample()\n");
    uint32_t len;
    b >> len;
    len = htonl (len);
    printf ("size: %d\n", len);
    b.pos() += len;
  }

  void sflow_handler::handleFlowSampleEx (buffer &b)
  {
    printf ("handleFlowSampleEx()\n");
    sflow_sample sample (b, m_header, m_cache, m_ip, true);
  }

  void sflow_handler::handleCountersSampleEx (buffer &b)
  {
    printf ("handleCountersSampleEx()\n");
    uint32_t len;
    b >> len;
    len = htonl (len);
    printf ("size: %d\n", len);
    b.pos() += len;
  }
}
