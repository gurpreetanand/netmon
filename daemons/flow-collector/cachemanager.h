#ifndef __CACHE_MANAGER__
#define __CACHE_MANAGER__

#include "db.h"
#include <map>
#include <stdint.h>

namespace netmon
{
  class flow;
  class flow_time;
  class flow_storage;
  class flow_storage_live;

  struct shm_sizes
  {
    shm_sizes ()
      : m_maxProtocols (20), m_maxStreams (200)
    {}
    shm_sizes (int maxp, int maxs)
      : m_maxProtocols (maxp), m_maxStreams (maxs)
    {}
    int m_maxProtocols;
    int m_maxStreams;
  };

  class cache_manager
  {
    public:
      cache_manager (db *, const shm_sizes &);
      ~cache_manager ();

      void addClient (uint32_t);
      void removeClient (uint32_t);

      void addInterface (uint32_t, uint16_t, uint32_t, uint32_t);
      void removeInterface (uint32_t, uint16_t);

      void add (uint32_t, const flow &, const flow_time &);

      unsigned int size () const
      {
        return m_size;
      }

      void store (int);
      void storeLive (int);
      void clear ();
      void clearLive ();

    protected:
      typedef std::map<uint32_t, flow_storage *> storagemap;
      typedef std::map<uint16_t, flow_storage_live *> interfacemap;
      typedef std::map<uint32_t, interfacemap> livestoragemap;

      storagemap m_staticStorage;
      livestoragemap m_liveStorage;

      db *m_db;
      unsigned int m_size;
      shm_sizes m_shmSizes;
  };
}

#endif // __CACHE_MANAGER__
