#include "sflowcollector.h"
#include "buffer.h"
#include "clientmanager.h"
#include "sflowhandler.h"
#include "cachemanager.h"
#include "exception.h"
#include "log.h"

namespace netmon
{
static uint32_t no_packets = 0;

  sflow_collector::sflow_collector (int port, client_manager *m)
    : udp_collector (port, m)
  {
  }

  sflow_collector::sflow_collector (int port, client_manager *m, cache_manager *s)
    : udp_collector (port, m), m_cache (s)
  {
  }

  void sflow_collector::handleData ()
  {
    if (m_len < 2) // invalid packet received
      return;
++no_packets;
    uint32_t *s = reinterpret_cast <uint32_t *> (m_buf);
    uint32_t sv = ntohl (*s);
    printf ("SFlow v%d\n", sv);
    if (sv == 2 || sv == 4 || sv >= 5)
    {
      if (m_clientMgr->isValidSFSource (m_from.sin_addr.s_addr))
        handleSFlow();
    }
    else
      printf ("Uknown sFlow version (%d)\n", sv);
  }

  void sflow_collector::handleSFlow ()
  {
    flow_handlers_t::iterator i = m_flowHandlers.find (m_from.sin_addr.s_addr);
    flow_handler *handler = i->second;

    if (i == m_flowHandlers.end())
    {
      handler = new sflow_handler (m_cache, m_from.sin_addr.s_addr);
      m_flowHandlers[m_from.sin_addr.s_addr] = handler;
    }

    try
    {
      buffer b (m_buf, m_len);
      handler->processBuffer (b);
    }
    catch (netmon_exception &e)
    {
      printf ("Error: %s\n", e.what());
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    }
  }
}
