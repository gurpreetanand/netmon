#ifndef __FLOW_STORAGE__
#define __FLOW_STORAGE__

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

#include <stdint.h>

namespace netmon
{
  class flow;
  class flow_time;

  struct flow_data_simple
  {
    flow_data_simple () {}
    flow_data_simple (const flow &, const flow_time &);
    flow_data_simple (const flow_data_simple &);

    uint32_t m_nextHop;
    uint16_t m_iiface;
    uint16_t m_oiface;
    uint32_t m_packets;
    uint32_t m_octets;
    uint32_t m_startTime;
    uint32_t m_endTime;
    uint8_t m_protocol;
    uint8_t m_tos;
    uint8_t m_tcpflags;
  };

  class flow_storage
  {
    public:
      flow_storage ();
      virtual ~flow_storage ();

      virtual void add (const flow &, const flow_time &);
      uint64_t getTotalBytes () const;
      virtual void clear ();
      virtual void store (int) {}

    protected:
      void addConversationDetailed (const flow &, const flow_time &);

      typedef std::pair<uint16_t, uint16_t> portpair;
      struct phash
      {
        size_t operator() (const portpair &__s) const
        {
          size_t ret = 0;
          ret = __s.first << 16 | __s.second;
          return ret;
        }
      };

      struct peq
      {
        bool operator() (const portpair &a, const portpair &b) const
        {
          return (a.first == b.first && a.second == b.second);
        }
      };

      typedef std::pair<uint32_t, uint32_t> ippair;
      struct iphash
      {
        size_t operator() (const ippair &__s) const
        {
          size_t ret = __s.first & 0xffff0000;
          ret |= (__s.second & 0xffff0000) >> 16;
          return ret;
        }
      };

      struct ipeq
      {
        bool operator() (const ippair &a, const ippair &b) const
        {
          return (a.first == b.first && a.second == b.second);
        }
      };

//      typedef hash_map<portpair, flow_data_simple, phash, peq> portmap;
//      typedef hash_map<ippair, portmap, iphash, ipeq> cdmap;

      struct ip_port_entry
      {
        ip_port_entry (uint32_t src, uint32_t dst, uint16_t srcport, uint16_t dstport)
          : m_src(src), m_dst(dst), m_srcPort(srcport), m_dstPort(dstport)
        {
        }

        uint32_t m_src;
        uint32_t m_dst;
        uint16_t m_srcPort;
        uint16_t m_dstPort;
      };

      struct ip_port_hash
      {
        size_t operator() (const ip_port_entry &__s) const
        {
          size_t hash = 12;
          uint16_t val = (__s.m_src & 0xffff0000) >> 16;

          // step 1
          hash += val;
          val = __s.m_src & 0x0000ffff;
          hash ^= hash << 16;
          hash ^= val << 11;
          hash += hash >> 11;

          val = (__s.m_dst & 0xffff0000) >> 16;
          // step 2
          hash += val;
          val = __s.m_dst & 0x0000ffff;
          hash ^= hash << 16;
          hash ^= val << 11;
          hash += hash >> 11;

          uint16_t *d = (uint16_t *) &__s.m_srcPort;
          // step 3
          hash += *d;
          d = (uint16_t *) &__s.m_dstPort;
          hash ^= hash << 16;
          hash ^= *d << 11;
          hash += hash >> 11;

          hash ^= hash << 3;
          hash += hash >> 5;
          hash ^= hash << 2;
          hash += hash >> 15;
          hash ^= hash << 10;

          return hash;
        }
      };

      struct ip_port_eq
      {
        bool operator() (const ip_port_entry &a, const ip_port_entry &b) const
        {
          return (a.m_src == b.m_src && 
                  a.m_dst == b.m_dst &&
                  a.m_srcPort == b.m_srcPort &&
                  a.m_dstPort == b.m_dstPort);
        }
      };

      typedef hash_map<ip_port_entry, flow_data_simple, ip_port_hash, ip_port_eq> cdmap;

      cdmap m_convDMap;
      uint64_t m_total;
  };
}

#endif // __FLOW_STORAGE__
