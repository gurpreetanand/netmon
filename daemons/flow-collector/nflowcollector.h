#ifndef __NFLOW_COLLECTOR__
#define __NFLOW_COLLECTOR__

#include "udpcollector.h"

#include <map>

namespace netmon
{
  class cache_manager;
  class flow_handler;

  class nflow_collector : public udp_collector
  {
    public:
      nflow_collector (int, client_manager *);
      nflow_collector (int, client_manager *, cache_manager *);

      void setCacheManager (cache_manager *storage)
      {
        m_cache = storage;
      }

    protected:
      virtual void handleData ();

      void handleFlowV1 ();
      void handleFlowV5 ();
      void handleFlowV7 ();
      void handleFlowV9 ();

      cache_manager *m_cache;

      typedef std::map<uint32_t, flow_handler *> flow_handlers_t;
      flow_handlers_t m_flowHandlers;

      enum { emaxV1Flows = 24, emaxV5Flows = 30 };
  };
}

#endif // __NFLOW_COLLECTOR__
