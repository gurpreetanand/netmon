#include "udpcollector.h"

#include <errno.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "clientmanager.h"

namespace netmon
{
  udp_collector::udp_collector (int port, client_manager *m)
    : m_clientMgr (m), m_port (port)
  {
    init();
  }

  udp_collector::~udp_collector ()
  {
    close (m_fd);
    delete[] m_buf;
  }

  void udp_collector::onRead (int fd, short event, void *me)
  {
    udp_collector *c = reinterpret_cast<udp_collector *> (me);
    c->onRead();
  }

  void udp_collector::onRead ()
  {
    int fromlen = sizeof (m_from);

    m_len = recvfrom (m_fd, m_buf, eBufsize, 0, (sockaddr *) &m_from, (socklen_t *) &fromlen);
    if (m_len < 0)
    {
      printf ("recv (%d)\n", errno);
      if (errno == EINTR) // interrupt - read again
        return onRead ();
      return; // some other error, return
    }

    if (m_clientMgr->isValid (m_from.sin_addr.s_addr))
      handleData();
  }

  void udp_collector::init ()
  {
    initSocket();
    m_buf = new uint8_t[eBufsize];

    event_set (&m_readEvent, m_fd, EV_READ | EV_PERSIST, onRead, (void *) this);
    event_add (&m_readEvent, 0);
  }

  void udp_collector::initSocket ()
  {
    m_fd = socket (AF_INET, SOCK_DGRAM, 0);
    if (m_fd == -1)
      throw collector_exc ("Cannot create socket");

    sockaddr_in sa;
    bzero ((char *) &sa, sizeof (sa));
    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr = htonl (INADDR_ANY);
    sa.sin_port = htons (m_port);

    if (bind (m_fd, (sockaddr *) &sa, sizeof (sa)) < 0)
      throw collector_exc ("Cannot bind socket");
  }
}
