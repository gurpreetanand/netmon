#include "flowstoragedb.h"
#include "log.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <errno.h>

#include <cstring>
#include <sstream>

namespace netmon
{
  flow_storage_db::flow_storage_db (uint32_t ip, db *mydb)
    : flow_storage(), m_db (mydb)
  {
    in_addr addr;
    addr.s_addr = ip;
    m_ip = std::string (inet_ntoa (addr));

    mq_attr attr;
    attr.mq_maxmsg = 16384;
    attr.mq_msgsize = sizeof (uint32_t);
    m_queue = mq_open ("/ipqueue", O_CREAT | O_NONBLOCK | O_WRONLY, 0777, &attr);
    if (m_queue == (mqd_t) -1)
    {
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      perror ("mq_open");
      log::instance()->add(_eLogFatal, strerror (errno));
    }
  }

  flow_storage_db::~flow_storage_db ()
  {
  }

  void flow_storage_db::store (int tstamp)
  {
    int now = ::time(0);

    m_ips.clear();
    m_db->execSQL ("BEGIN");
    m_db->beginCopy (
    "COPY NETFLOW (src_ip, src_port, dst_ip, dst_port, protocol, \
    octets, packets, start_time, end_time, in_iface, out_iface, flow_src, \
    timestamp) FROM STDIN");

unsigned int total = 0;
unsigned int stored = 0;

    for (cdmap::iterator i = m_convDMap.begin(); i != m_convDMap.end(); )
    {
      ++total;
      if ((i->second.m_tcpflags & 0x01) || (i->second.m_tcpflags & 0x04) || ((now - i->second.m_endTime) > 600))
      {
        ++stored;
        store (i->first.m_src, i->first.m_dst, i->first.m_srcPort, i->first.m_dstPort, i->second, tstamp);
        enqueueIPPair (i->first.m_src, i->first.m_dst);
        m_convDMap.erase(i++);
      }
      else
        ++i;
    }

    enqueueIPs();

//FILE *f = fopen ("store.log", "a");
printf (/*f,*/ "total: %d stored: %d\n", total, stored);
//fflush (stdout);
    m_db->endCopy (0);
    m_db->execSQL ("COMMIT");
  }

  void flow_storage_db::store (unsigned int src, unsigned int dst, unsigned short srcport, unsigned short dstport, const flow_data_simple &item, int tstamp)
  {
    in_addr addr;
    static std::ostringstream sql;

    sql.str("");

    addr.s_addr = src;
    sql << inet_ntoa (addr) << "\t" << srcport << "\t";
    addr.s_addr = dst;
    sql << inet_ntoa (addr) << "\t" << dstport << "\t"
        << (int) item.m_protocol << "\t"
        << item.m_octets << "\t" 
        << item.m_packets << "\t"
        << item.m_startTime << "\t"
        << item.m_endTime << "\t"
        << item.m_iiface << "\t"
        << item.m_oiface << "\t"
        << m_ip << "\t"
        << tstamp << "\n";

//    printf ("%s\n", sql.str().c_str());
    try
    {
      m_db->copyData (sql.str().c_str(), sql.str().size());
    }
    catch (std::string &e)
    {
      printf ("Error: %s\n", e.c_str());
      netmon::log::instance()->add(netmon::_eLogError, e.c_str());
    }
  }

  void flow_storage_db::enqueueIPPair (uint32_t src, uint32_t dst)
  {
    m_ips.insert (src);
    m_ips.insert (dst);
  }

  void flow_storage_db::enqueueIPs ()
  {
    uint32_t ip;

    for (std::set<uint32_t>::iterator i = m_ips.begin(); i != m_ips.end(); ++i)
    {
      ip = *i;
      mq_send (m_queue, (const char *) &ip, sizeof (ip), 0);
    }
  }
}
