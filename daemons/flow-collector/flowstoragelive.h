#ifndef __FLOW_STORAGE_LIVE__
#define __FLOW_STORAGE_LIVE__

#include "flowstorage.h"
#include "storageitems.h"
#include "toplist.h"

namespace netmon
{
  class flow_storage_live : public flow_storage
  {
    typedef flow_storage inherited;
    public:
      flow_storage_live (unsigned int, unsigned int);
      ~flow_storage_live ();

      virtual void add (const flow &, const flow_time &);
      virtual void clear ();

      void getTopDetailConversations (toplist<connitem, comp<connitem> > &);
      void getTopProtocols (toplist<portitem, comp<portitem> > &);

    protected:
      typedef hash_map<unsigned short, uint64_t> protocols_map;

      void getTopProtocols (const protocols_map &, toplist<portitem, comp<portitem> > &);

      protocols_map m_Protocols;

      unsigned int m_interface;
      unsigned int m_device;
  };
}

#endif // __FLOW_STORAGE_LIVE__
