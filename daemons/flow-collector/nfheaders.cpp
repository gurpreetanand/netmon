#include "nfheaders.h"

namespace netmon
{
  void nfheader::deserialize (buffer &b)
  {
    b >> m_version >> m_count >> m_uptime >> m_seconds;
  }

  void nfheader_v1::deserialize (buffer &b)
  {
    b >> m_nseconds;
  }

  void nfheader_v57::deserialize (buffer &b)
  {
    b >> m_sequence;
  }

  void nfheader_v5::deserialize (buffer &b)
  {
    uint8_t pad;

    b >> m_engineType >> m_engineID >> pad >> pad;
  }

  void nfheader_v7::deserialize (buffer &b)
  {
    uint32_t pad;

    b >> pad;
  }

  void nfheader_v9::deserialize (buffer &b)
  {
    b >> m_flowSequence >> m_sourceID;
  }
}
