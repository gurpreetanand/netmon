#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <cstdlib>
#include <sstream>

#include "clientmanager.h"
#include "cachemanager.h"

namespace netmon
{
  const unsigned int client_manager_db::m_baseKey;

  client_manager_db::client_manager_db (db *mydb, cache_manager *cm)
    : m_db (mydb), m_cache (cm)
  {
    reload();
  }

  client_manager_db::~client_manager_db ()
  {
    m_clients.clear();
  }

  bool client_manager_db::isValid (uint32_t ip)
  {
    client_map::iterator i = m_clients.find (ip);
    if (i != m_clients.end() && i->second.m_active)
    {
//      printf ("client valid\n");
      return true;
    }

    if (i == m_clients.end())
      addClient (ip);

    return false;
  }

  bool client_manager_db::isValidNFSource (uint32_t ip)
  {
    client_map::iterator i = m_clients.find (ip);
    if (i != m_clients.end() && i->second.m_netflow)
      return true;

    return false;
  }

  bool client_manager_db::isValidSFSource (uint32_t ip)
  {
    client_map::iterator i = m_clients.find (ip);
    if (i != m_clients.end() && i->second.m_sflow)
      return true;

    return false;
  }

  void client_manager_db::addClient (uint32_t ip)
  {
    client_entry e;
    e.m_ip = ip;
    e.m_active = false;
    m_clients[ip] = e;
    insertClient (e);
  }

  void client_manager_db::insertClient (const client_entry &e)
  {
    in_addr addr;
    addr.s_addr = e.m_ip;
    std::ostringstream sql;

    sql << "INSERT INTO DEVICES (IP_ADDRESS, SNMP_COMMUNITY, SYSDESCR) "
        << "VALUES ('" << inet_ntoa (addr) << "', '', 'Netflow Source')";
    m_db->execSQL (sql.str().c_str());
  }

  void client_manager_db::reload ()
  {
    reloadClients();
    reloadInterfaces();
  }

  void client_manager_db::reloadClients ()
  {
    PGresult *res = m_db->execQuery (
    "SELECT ID, IP_ADDRESS, ENABLE_NETFLOW, ENABLE_SFLOW FROM DEVICES ORDER BY ID");

    bool has_netflow = false;
    bool has_sflow = false;

    for (int i = 0; i < PQntuples (res); ++i)
    {
      client_entry e;
      in_addr addr;
      inet_aton (PQgetvalue (res, i, 1), &addr);
      e.m_ip = addr.s_addr;
      e.m_id = atoi (PQgetvalue (res, i, 0));
      has_netflow = *(PQgetvalue (res, i, 2)) == 't' ? true : false;
      has_sflow = *(PQgetvalue (res, i, 3)) == 't' ? true : false;
      e.m_active = has_netflow || has_sflow;
      e.m_netflow = has_netflow;
      e.m_sflow = has_sflow;

      client_map::iterator it = m_clients.find(e.m_ip);
      if (it != m_clients.end())
      {
        if (it->second.m_active && !e.m_active) // was active, now not
          m_cache->removeClient (e.m_ip);
        else if (!it->second.m_active && e.m_active) // was inactive, now active
          m_cache->addClient (e.m_ip);
      }
      else if (e.m_active)
        m_cache->addClient (e.m_ip);

      m_clients[e.m_ip] = e;
    }

    PQclear (res);
  }

  void client_manager_db::reloadInterfaces ()
  {
    PGresult *res = m_db->execQuery (
    "SELECT INTERFACES.ID, IP_ADDRESS, INTERFACE, SHM_KEY, \
    ENABLE_SHM, DEVICE_ID FROM INTERFACES, \
    DEVICES WHERE INTERFACES.DEVICE_ID = DEVICES.ID ORDER BY INTERFACES.ID");

    for (int i = 0; i < PQntuples (res); ++i)
    {
      interface_entry e;
      in_addr addr;
      inet_aton (PQgetvalue (res, i, 1), &addr);
      e.m_ip = addr.s_addr;
      e.m_id = atoi (PQgetvalue (res, i, 0));
      e.m_interface = atoi (PQgetvalue (res, i, 2));
      e.m_shmkey = atoi (PQgetvalue (res, i, 3));
      e.m_active = *(PQgetvalue (res, i, 4)) == 't' ? true : false;
      e.m_device = atoi (PQgetvalue (res, i, 5));

      interface_map::iterator i1 = m_interfaces.find (e.m_ip);
      if (i1 != m_interfaces.end())
      {
        interfaces::iterator i2 = i1->second.find (e.m_interface);
        if (i2 != i1->second.end())
        {
          if (i2->second.m_active && !e.m_active) // was active, now not
          {
            m_freeKeys.push_back (i2->second.m_shmkey);
            m_cache->removeInterface (e.m_ip, e.m_interface);
          }
          else if (!i2->second.m_active && e.m_active) // was not active, now is
          {
            addInterface (e);
          }
        }
        else if (e.m_active) // we don't have this iface, but it's active
        {
          addInterface (e);
        }
        i1->second[e.m_interface] = e;
      }
      else if (e.m_active) // we don't have this iface, but it's active
      {
        addInterface (e);
      }
      m_interfaces[e.m_ip][e.m_interface] = e;
    }

    PQclear (res);
  }

  unsigned int client_manager_db::getNextKey ()
  {
    unsigned int shmkey;
    if (!m_freeKeys.empty())
    {
      shmkey = m_freeKeys.front();
      m_freeKeys.pop_front();
    }
    else
      shmkey = m_cache->size() * 3 + m_baseKey;

    return shmkey;
  }

  void client_manager_db::addInterface (interface_entry &e)
  {
    unsigned int shmkey = getNextKey();
    e.m_shmkey = shmkey;
    m_cache->addInterface (e.m_ip, e.m_interface, e.m_device, shmkey);
    updateInterface (e);
  }

  void client_manager_db::updateInterface (const interface_entry &e)
  {
    std::ostringstream sql;
    sql << "UPDATE INTERFACES SET SHM_KEY = " << e.m_shmkey
        << " WHERE ID = " << e.m_id << " AND INTERFACE = " << e.m_interface;

    m_db->execSQL (sql.str().c_str());
  }
}
