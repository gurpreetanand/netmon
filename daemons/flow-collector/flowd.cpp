#include "flowd.h"
#include "flowcollector.h"
#include "sflowcollector.h"
#include "clientmanager.h"
#include "cachemanager.h"
#include "dvars.h"

namespace netmon
{
  flow_daemon::flow_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  flow_daemon::~flow_daemon ()
  {
    delete m_flowCollector;
    delete m_clientManager;
    delete m_cache;
  }

  void flow_daemon::init ()
  {
    daemon::init();

    int port = 3030;
    int sfport = 6343;
    shm_sizes s;

    dvars vars (*m_db, getDaemonName());
    vars.get ("listen_port", port);
    vars.get ("sflow_port", sfport);
    vars.get ("maxprotocols", s.m_maxProtocols);
    vars.get ("maxstreams", s.m_maxStreams);

    m_cache = new cache_manager (m_db, s);
    m_clientManager = new client_manager_db (m_db, m_cache);
    m_flowCollector = new flow_collector (port, sfport, m_cache, m_clientManager);
  }

  int flow_daemon::run ()
  {
    m_flowCollector->run();
    return 0; // never reached
  }
}
