#include "flowstorageliveshm.h"
#include "log.h"
#include "netmon.h"

namespace netmon
{
  static const unsigned int semkey  = 0xff00ceca;

  flow_storage_live_shm::flow_storage_live_shm (unsigned int interface, unsigned int device, uint32_t basekey, int maxstreams, int maxproto)
    : flow_storage_live (interface, device), m_sem (semkey, IPC_CREAT | S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH)
  {
    if (maxstreams < 1 || maxproto < 1)
      throw (std::string ("Illegal parameter ( < 1)"));
    m_maxStreams = maxstreams;
    m_maxProto = maxproto;
    initshm (basekey);
  }

  flow_storage_live_shm::~flow_storage_live_shm ()
  {
    delete m_shmStreams;
    delete m_shmProtocols;
  }

  void flow_storage_live_shm::initshm (uint32_t basekey)
  {
    int size = m_maxStreams * 80 + 4096; // we need 80 bytes for convitem serialization
    m_shmStreams = new shm (basekey, size, IPC_CREAT | 0666, true);
    size = m_maxProto * 64 + 4096; // at least 64 bytes for ipitem
    m_shmProtocols = new shm (basekey + 1, size, IPC_CREAT | 0666, true);
  }

  void flow_storage_live_shm::store (int interval)
  {
    m_sem.wait();
    storeStreams (interval);
    storeProtocols (interval);
    m_sem.post();
  }

  void flow_storage_live_shm::storeStreams (int interval)
  {
    uint64_t total = getTotalBytes();

    try
    {
      int size = m_shmStreams->size();
      char *buf = m_shmStreams->buffer();

      if (total == 0)
        return;

      m_shmStreams->fill (0xff);

      toplist<connitem, comp<connitem> > l(m_maxStreams);
      getTopDetailConversations (l);
      int i = 0;
      for (toplist<connitem, comp<connitem> >::iterator it = l.begin(); it != l.end(); ++it)
      {
        i += (*it).serialize (buf + i, size - i);
        i += speedtostr (buf + i, size - i, (*it).m_octets, interval);
        i += snprintf (buf + i, size - i, "%.2f", (double)((*it).m_octets * 100 / total)) + 1;
      }
    }
    catch (int e)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Cannot create shared memory segment");
    }
  }

  void flow_storage_live_shm::storeProtocols (int interval)
  {
    try
    {
      int size = m_shmProtocols->size();
      char *buf = m_shmProtocols->buffer();
      m_shmProtocols->fill (0xff);
      if (getTotalBytes() == 0)
        return;
      toplist<portitem, comp<portitem> > l(m_maxProto);
      getTopProtocols (l);
      int i = 0;
      for (toplist<portitem, comp<portitem> >::iterator it = l.begin(); it != l.end(); ++it)
      {
        i += (*it).serialize (buf + i, size - i) + 1;
      }
    }
    catch (int e)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Cannot create shared memory segment");
    }
  }
}
