/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <cstdlib>
#include <strings.h>
#include <unistd.h>

void error(char *);
int main (int argc, char *argv[])
{
   int sock, length, n;
   struct sockaddr_in server, from;
   struct hostent *hp;
   
   if (argc != 4)
   {
     printf("Usage: server port file_to_transfer\n");
     exit(1);
   }

   sock = socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0)
     error("socket");

   server.sin_family = AF_INET;
   hp = gethostbyname(argv[1]);
   if (hp == 0)
     error("Unknown host");

   bcopy((char *)hp->h_addr, (char *)&server.sin_addr, hp->h_length);
   server.sin_port = htons(atoi(argv[2]));
   length = sizeof(struct sockaddr_in);

  struct stat s;
  if (stat (argv[3], &s) != 0)
  {
    perror ("stat");
    return -1;
  }

  if (s.st_size == 0)
  {
    fprintf (stderr, "Illegal size\n");
    return -1;
  }

  uint8_t *buf = new uint8_t[s.st_size];
  int fd = open (argv[3], O_RDONLY);
  if (fd < 0)
  {
    perror ("open");
    delete[] buf;
    return -1;
  }

  if (read (fd, (void *) buf, s.st_size) != s.st_size)
  {
    perror ("read");
    delete[] buf;
    close (fd);
    return -1;
  }

  close (fd);

   n = sendto (sock, buf, s.st_size, 0, (const sockaddr *)&server, length);
   if (n < 0)
     error("Sendto");
  delete[] buf;
  close (sock);
}

void error(char *msg)
{
    perror(msg);
    exit(0);
}
