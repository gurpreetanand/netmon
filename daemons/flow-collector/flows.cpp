#include "flows.h"
#include "buffer.h"

namespace netmon
{
  void flow_base::deserialize (buffer &b)
  {
    b >> m_src
      >> m_dst
      >> m_nextHop
      >> m_iiface
      >> m_oiface
      >> m_packets
      >> m_octets
      >> m_startTime
      >> m_endTime
      >> m_srcPort
      >> m_dstPort;
  }

  void flow_v1::deserialize (buffer &b)
  {
    b.skip (sizeof (uint16_t));
    b >> m_protocol
      >> m_tos
      >> m_tcpflags;
    b.skip (3 * sizeof (uint8_t) + sizeof (uint32_t));
  }

  void flow_v5::deserialize (buffer &b)
  {
    b >> m_flags1
      >> m_tcpflags
      >> m_protocol
      >> m_tos
      >> m_srcAS
      >> m_dstAS
      >> m_srcMask
      >> m_dstMask
      >> m_flags2;
  }

  void flow_v7::deserialize (buffer &b)
  {
    b >> m_routerAddr;
  }

  flow_v9::set_t flow_v9::m_neededTypes = flow_v9::initSet();

  void flow_v9::deserialize (buffer &b)
  {
    for (tlist_t::iterator i = m_types.begin(); i != m_types.end(); ++i)
    {
      if (m_neededTypes.find ((*i).first) == m_neededTypes.end())
      {
        b.skip ((*i).second);
        continue;
      }
      handleType (b, (*i).first, (*i).second);
//      printf ("type: %d size: %d\n", (*i).first, (*i).second);
    }
  }

  void flow_v9::handleType (buffer &b, uint16_t type, uint16_t size)
  {
    switch (type)
    {
      case eInBytes:
        if (size == sizeof (m_octets))
          b >> m_octets;
        else
          b.skip (size);
        break;
      case eInPackets:
        if (size == sizeof (m_packets))
          b >> m_packets;
        else
          b.skip (size);
        break;
      case eProtocol:
        b >> m_protocol;
        break;
      case eTOS:
        b >> m_tos;
        break;
      case eTCPFlags:
        b >> m_tcpflags;
        break;
      case eSrcPort:
        b >> m_srcPort;
        break;
      case eSrcAddr:
        b >> m_src;
        break;
      case eInSNMP:
        if (size == sizeof (m_iiface))
          b >> m_iiface;
        else
          b.skip (size);
        break;
      case eDstPort:
        b >> m_dstPort;
        break;
      case eDstAddr:
        b >> m_dst;
        break;
      case eOutSNMP:
        if (size == sizeof (m_oiface))
          b >> m_oiface;
        else
          b.skip (size);
        break;
      case eLastSwitched:
        b >> m_endTime;
        break;
      case eFirstSwitched:
        b >> m_startTime;
        break;
    }
  }

  flow_v9::set_t flow_v9::initSet ()
  {
    set_t t;

    t.insert (eInBytes);
    t.insert (eInPackets);
    t.insert (eProtocol);
    t.insert (eTOS);
    t.insert (eTCPFlags);
    t.insert (eSrcPort);
    t.insert (eSrcAddr);
    t.insert (eInSNMP);
    t.insert (eDstPort);
    t.insert (eDstAddr);
    t.insert (eOutSNMP);
    t.insert (eLastSwitched);
    t.insert (eFirstSwitched);

    return t;
  }
}
