#ifndef __CLIENT_MANAGER__
#define __CLIENT_MANAGER__

#include "../lib/db.h"

#include <list>
#include <map>
#include <string>
#include <stdint.h>

namespace netmon
{
  class client_manager
  {
    public:
      client_manager () {}
      virtual ~client_manager () {}

      virtual bool isValid (uint32_t) = 0;
      virtual void reload () = 0;

      virtual bool isValidNFSource (uint32_t) = 0;
      virtual bool isValidSFSource (uint32_t) = 0;
  };

  class cache_manager;

  class client_manager_db : public client_manager
  {
    public:
      client_manager_db (db *, cache_manager *);
      ~client_manager_db ();

      virtual bool isValid (uint32_t);
      virtual void reload ();

      virtual bool isValidNFSource (uint32_t);
      virtual bool isValidSFSource (uint32_t);

    protected:
      struct client_entry
      {
        int m_id;
        unsigned int m_ip;
        bool m_active;
        bool m_netflow;
        bool m_sflow;
      };

      struct interface_entry
      {
        int m_id;
        unsigned int m_ip;
        unsigned int m_interface;
        unsigned int m_shmkey;
        unsigned int m_device;
        bool m_active;
      };

      void addClient (uint32_t);
      void insertClient (const client_entry &);

      void reloadClients ();
      void reloadInterfaces ();

      void addInterface (interface_entry &);
      void updateInterface (const interface_entry &);
      unsigned int getNextKey ();

      typedef std::map<unsigned int, client_entry> client_map;
      typedef std::map<unsigned int, interface_entry> interfaces;
      typedef std::map<unsigned int, interfaces> interface_map;
      typedef std::list<unsigned int> key_list;

      client_map m_clients;
      interface_map m_interfaces;
      key_list m_freeKeys;

      db *m_db;
      cache_manager *m_cache;

      static const unsigned int m_baseKey = 0xf4000006;
  };
}

#endif // __CLIENT_MANAGER__
