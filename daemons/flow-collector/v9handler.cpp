#include "v9handler.h"
#include "buffer.h"
#include "cachemanager.h"
#include "nfheaders.h"

namespace netmon
{
  v9_handler::v9_handler (cache_manager *cache, uint32_t ip)
    : flow_handler (cache, ip)
  {}

  v9_handler::~v9_handler ()
  {}

  void v9_handler::processBuffer (buffer &b)
  {
    nfheader_v9 h (b);
    m_flowTime = flow_time (h);
//    printf ("seq: %u id: %u\n", h.getSequence(), h.getSourceID());
    processData (b);
  }

  void v9_handler::processData (buffer &b)
  {
    uint16_t flowID;
    b >> flowID;
    flowID = ntohs (flowID);

printf ("ID: %d\n", flowID);
    if (flowID == eTemplate)
      return handleTemplateFlow (b);
    if (flowID == eOption)
      return handleOptionsFlow (b);
    if (flowID > eReserved)
      return handleDataFlow (b, flowID);
  }

  void v9_handler::handleTemplateFlow (buffer &b)
  {
    uint16_t len;
    uint16_t id;
    uint16_t count;

    b >> len >> id >> count;

    len = ntohs (len);
    id = ntohs (id);
    count = ntohs (count);

    printf ("len: %d id: %d cnt: %d\n", len, id, count);

    uint16_t ftype;
    uint16_t flen;

    m_template[id].clear();

    for (uint16_t i = 0; i < count; ++i)
    {
      b >> ftype >> flen;
      ftype = ntohs (ftype);
      flen = ntohs (flen);
      m_template[id].push_back (std::make_pair (ftype, flen));
    }

    if (!b.eof()) // more data in this buffer
      processData (b);
  }

  void v9_handler::handleOptionsFlow (buffer &b)
  {
  }

  void v9_handler::handleDataFlow (buffer &b, uint16_t id)
  {
    uint16_t len;

    b >> len;

    len = ntohs (len);
    if (len < 2*sizeof(len))
      return;


    tmap_t::iterator i = m_template.find (id);
    if (i == m_template.end()) // we still didn't receive template id for this flowset id
    {
      b.skip (len - 2 * sizeof (len));
      if (!b.eof())
        return processData (b);
      return;
    }

    printf ("template (%d) found.\n", id);

    uint32_t pos = b.pos();
    tlist_t &t = i->second;

    do
    {
      flow_v9 f(b, t);
      m_cache->add (m_ip, f, m_flowTime);
    }
    while (b.pos() - pos < (len - 2 * sizeof (uint16_t)));

    if (!b.eof()) // more data in this buffer
      processData (b);
  }
}
