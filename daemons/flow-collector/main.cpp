#include <errno.h>
#include <sys/types.h> 

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>

#include "log.h"
#include "exception.h"
#include "flowd.h"

int main (int argc, char **argv)
{
  try
  {
    netmon::flow_daemon *mon = new netmon::flow_daemon (argc, argv);
    mon->init();
    mon->start();
  }
  catch (int e)
  {
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, strerror (e));
    exit (-1);
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    exit (-1);
  }
  return 0; // never reached
}
