#include "nflowcollector.h"
#include "buffer.h"
#include "clientmanager.h"
#include "flows.h"
#include "flowtime.h"
#include "nfheaders.h"
#include "v9handler.h"
#include "cachemanager.h"
#include "exception.h"
#include "log.h"

namespace netmon
{
static uint32_t no_packets = 0;

  nflow_collector::nflow_collector (int port, client_manager *m)
    : udp_collector (port, m)
  {
  }

  nflow_collector::nflow_collector (int port, client_manager *m, cache_manager *s)
    : udp_collector (port, m), m_cache (s)
  {
  }

  void nflow_collector::handleData ()
  {
    if (m_len < 2) // invalid packet received
      return;
++no_packets;
    uint16_t *ver = reinterpret_cast <uint16_t *> (m_buf);

printf ("nflow v%d\n", ntohs (*ver));
    switch (ntohs (*ver))
    {
      case 0x01:
        handleFlowV1();
        break;
      case 0x05:
        handleFlowV5();
        break;
      case 0x07:
        handleFlowV7();
        break;
      case 0x08:
        break;
      case 0x09:
        handleFlowV9();
        break;
      default: // unknown netflow format
        printf ("Uknown format (%d)\n", ntohs (*ver));
        break;
    }
  }

  void nflow_collector::handleFlowV1 ()
  {
    try
    {
      buffer b (m_buf, m_len);
      nfheader_v1 h (b);
      uint16_t count = h.getCount();
      flow_time ft (h);

      if (count < 1 || count > emaxV1Flows)
        return;

      for (uint16_t i = 0; i < count; ++i)
      {
        flow_v1 flow (b);
        m_cache->add (m_from.sin_addr.s_addr, flow, ft);
      }
    }
    catch (netmon_exception &e)
    {
      printf ("Error: %s\n", e.what());
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    }
  }

  void nflow_collector::handleFlowV5 ()
  {
    try
    {
      buffer b (m_buf, m_len);
      nfheader_v5 h (b);
      uint16_t count = h.getCount();
      flow_time ft (h);

      if (count < 1 || count > emaxV5Flows)
        return;

      for (uint16_t i = 0; i < count; ++i)
      {
        flow_v5 flow (b);
        m_cache->add (m_from.sin_addr.s_addr, flow, ft);
      }
    }
    catch (netmon_exception &e)
    {
      printf ("Error: %s\n", e.what());
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    }
  }

  void nflow_collector::handleFlowV7 ()
  {
    try
    {
      buffer b (m_buf, m_len);
      nfheader_v7 h (b);
      uint16_t count = h.getCount();
      flow_time ft (h);

      if (count < 1 || count > emaxV5Flows)
        return;

      for (uint16_t i = 0; i < count; ++i)
      {
        flow_v7 flow (b);
        m_cache->add (m_from.sin_addr.s_addr, flow, ft);
      }
    }
    catch (netmon_exception &e)
    {
      printf ("Error: %s\n", e.what());
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    }
  }

  void nflow_collector::handleFlowV9 ()
  {
    flow_handlers_t::iterator i = m_flowHandlers.find (m_from.sin_addr.s_addr);
    flow_handler *handler = i->second;

    if (i == m_flowHandlers.end())
    {
      handler = new v9_handler (m_cache, m_from.sin_addr.s_addr);
      m_flowHandlers[m_from.sin_addr.s_addr] = handler;
    }

    try
    {
      buffer b (m_buf, m_len);
      handler->processBuffer (b);
    }
    catch (netmon_exception &e)
    {
      printf ("Error: %s\n", e.what());
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    }
  }
}
