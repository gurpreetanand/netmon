#include "storageitems.h"
#include "netmon.h"
#include <cstdio>
#include <cstring>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>

namespace netmon
{
  int connitem::serialize (char *buf, int size)
  {
    char src[16];
    char dst[16];
    in_addr addr;
    addr.s_addr = m_srcIP;
    strcpy (src, inet_ntoa (addr));
    addr.s_addr = m_dstIP;
    strcpy (dst, inet_ntoa (addr));
    return snprintf (buf, size, "%s\n%s\n%u\n%u\n%s\n%" u64prefix "u\n", src, dst, m_srcport, m_dstport, (m_proto == 0x06 ? "TCP" : "UDP"), m_octets);
  }

  int portitem::serialize (char *buf, int size)
  {
    return snprintf (buf, size, "%u\n%" u64prefix "u", m_port, m_octets);
  }
}
