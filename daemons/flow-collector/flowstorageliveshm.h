#ifndef __FLOW_STORAGE_LIVE_SHM__
#define __FLOW_STORAGE_LIVE_SHM__

#include "flowstoragelive.h"
#include "semaphore.h"
#include "shm.h"

namespace netmon
{
  class flow_storage_live_shm : public flow_storage_live
  {
    public:
      flow_storage_live_shm (unsigned int, unsigned int, uint32_t, int = 200, int = 10);
      ~flow_storage_live_shm ();

      virtual void store (int);

    protected:
      void initshm (uint32_t);
      void storeStreams (int);
      void storeProtocols (int);

      shm *m_shmStreams;
      shm *m_shmProtocols;
      semaphore m_sem;
      int m_maxStreams;
      int m_maxProto;
  };
}

#endif // __FLOW_STORAGE_LIVE_SHM__
