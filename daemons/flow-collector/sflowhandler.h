#ifndef __SFLOW_HANDLER__
#define __SFLOW_HANDLER__

#include "flowhandler.h"
#include "sflow.h"

namespace netmon
{
  class sflow_handler : public flow_handler
  {
    public:
      sflow_handler (cache_manager *, uint32_t);
      ~sflow_handler ();

      virtual void processBuffer (buffer &);

    protected:
      void handleSample (buffer &, uint32_t);
      void handleSampleV4 (buffer &, uint32_t);

      void handleFlowSample (buffer &);
      void handleCountersSample (buffer &);
      void handleFlowSampleEx (buffer &);
      void handleCountersSampleEx (buffer &);

      sflow_header m_header;

      enum { eV2 = 2, eV4 = 4, eV5 = 5 };
      enum { eFlowSample = 1, eCountersSample, eFlowSampleEx, eCountersSampleEx };
  };
}

#endif // __SFLOW_HANDLER__
