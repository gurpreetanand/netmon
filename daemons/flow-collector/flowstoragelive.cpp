#include "flowstoragelive.h"
#include "flows.h"
#include "flowtime.h"

namespace netmon
{
  flow_storage_live::flow_storage_live (unsigned int interface, unsigned int device)
    : flow_storage (), m_interface (interface), m_device (device)
  {
  }

  flow_storage_live::~flow_storage_live ()
  {
  }

  void flow_storage_live::add (const flow &f, const flow_time &t)
  {
    inherited::add (f, t);

    uint32_t octets = f.getOctets();
    uint32_t ts = (f.getEndTime() - f.getStartTime()) / 1000;

    if (ts > 120) // workaround for long timespans in netflow
    {
      double ratio = ts / 120;
      octets = (uint32_t) ((double) octets / ratio);
    }

    unsigned short port = f.getSrcPort();
    if (port > f.getDstPort())
      port = f.getDstPort();

    m_Protocols[port] += octets;
    
/*
    if (f.getInputInterface() == m_interface)
    {
      protocols_map::iterator i = m_outboundProtocols.find(f.getSrcPort());
      if (i == m_outboundProtocols.end())
        m_outboundProtocols[f.getSrcPort()] = octets;
      else
        m_outboundProtocols[f.getSrcPort()] += octets;
    }
    else
    {
      protocols_map::iterator i = m_inboundProtocols.find(f.getSrcPort());
      if (i == m_inboundProtocols.end())
        m_inboundProtocols[f.getSrcPort()] = octets;
      else
        m_inboundProtocols[f.getSrcPort()] += octets;
    }
*/
  }

  void flow_storage_live::getTopDetailConversations (toplist<connitem, comp<connitem> > &tlist)
  {
    for (cdmap::iterator i = m_convDMap.begin (); i != m_convDMap.end (); ++i)
    {
      connitem item;
      item.m_proto = i->second.m_protocol;
      item.m_octets = i->second.m_octets;
      item.m_srcIP = i->first.m_src;
      item.m_dstIP = i->first.m_dst;
      item.m_srcport = i->first.m_srcPort;
      item.m_dstport = i->first.m_dstPort;
      tlist.insert (item);
    }
  }

  void flow_storage_live::getTopProtocols (toplist<portitem, comp<portitem> > &tlist)
  {
    getTopProtocols (m_Protocols, tlist);
  }

  void flow_storage_live::getTopProtocols (const protocols_map &map, toplist<portitem, comp<portitem> > &tlist)
  {
    for (protocols_map::const_iterator i = map.begin(); i != map.end(); ++i)
    {
      if (i->second == 0)
        continue;
      tlist.insert (portitem (i->first, i->second));
    }
  }

  void flow_storage_live::clear ()
  {
    inherited::clear ();
  }
}
