#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include <string>

#ifndef ETHER_ADDR_LEN
#define ETHER_ADDR_LEN 6
#endif

#ifndef tcp_seq
typedef   u_int32_t tcp_seq;
#endif

#ifndef TH_SYN
#define TH_SYN  0x02
#endif
#ifndef TH_ACK
#define TH_ACK  0x10
#endif

void ProcessPacket (u_char *, const struct pcap_pkthdr *, const u_char *);
void HandleSignal(int signo);

int errvalue;

/* Ethernet header */

struct sniff_ethernet 
{
  u_char  dhost[ETHER_ADDR_LEN];    /* Destination host address */
  u_char  shost[ETHER_ADDR_LEN];    /* Source host address */
  u_short type;                     /* IP? ARP? RARP? etc */
};

/* IP header */

struct sniff_ip 
{
  #if BYTE_ORDER == BIG_ENDIAN             /* Sparc, Motorola, etc */
  u_int   version:4,		           /* version */
          hdrlen:4;		           /* header length */
  #elif BYTE_ORDER == LITTLE_ENDIAN        /* Intel */
  u_int   hdrlen:4,		           /* header length */
          version:4;		           /* version */
  #else
  #error "Please fix endian.h"
  #endif  
  u_char  tos;		/* type of service */
  u_short len:16;	/* total length */
  u_short id;		/* identification */
  u_short offset;	/* fragment offset field */
  u_char  ttl;		/* time to live */
  u_char  protocol;	/* protocol */
  u_short sum;		/* checksum */
  struct in_addr src;   /* source address */
  struct in_addr dst;   /* dest address */
};

/* TCP header */

struct sniff_tcp {
  u_short sport;                       /* source port */
  u_short dport;                       /* destination port */
  tcp_seq seq;                         /* sequence number */
  tcp_seq ack;                         /* acknowledgement number */
  #if BYTE_ORDER == LITTLE_ENDIAN      /* Intel platform */
    u_int unused:4,                    /* (unused) */
          offset:4;                    /* data offset */
  #elif BYTE_ORDER == BIG_ENDIAN       /* Sparc, Motorola, etc */
    u_int offset:4,                    /* data offset */
          unused:4;                    /* (unused) */
  #else
  #error "Please fix endian.h"
  #endif  
  u_char  flags;
  #define FIN  0x01
  #define SYN  0x02
  #define RST  0x04
  #define PSH  0x08
  #define ACK  0x10
  #define URG  0x20
  u_short win;                         /* window */
  u_short sum;                         /* checksum */
  u_short urp;                         /* urgent pointer */
};

/* UDP header */

struct sniff_udp {
  u_short sport;           /* source port */
  u_short dport;           /* destination port */
  u_short len; 	           /* udp package length */
  u_short sum;	           /* udp package checksum */
};

int sock, length;
unsigned int flows = 0;
struct sockaddr_in server, from;
struct hostent *hp;

void error(char *msg)
{
  perror(msg);
  exit(0);
}

int main(int argc, char **argv)
{
  char dev[16];                       /* Sniffing device */
  char errbuf[PCAP_ERRBUF_SIZE];      /* Error buffer */
  pcap_t *descr;                      /* Sniff handler */

  /* Open the device */
  if (argc != 4)
  {
    printf("Usage: server port file_to_transfer\n");
    exit(1);
  }

  descr = pcap_open_offline(argv[3], errbuf);
  if (descr == NULL)
  {
    printf ("pcap_open_offline(): %s\n", errbuf);
    exit (1);
  }

  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0)
    error("socket");

  server.sin_family = AF_INET;
  hp = gethostbyname(argv[1]);
  if (hp == 0)
    error("Unknown host");

  bcopy((char *)hp->h_addr, (char *)&server.sin_addr, hp->h_length);
  server.sin_port = htons(atoi(argv[2]));
  length = sizeof(struct sockaddr_in);

  pcap_loop(descr, -1, ProcessPacket, NULL);
  pcap_close(descr);

  printf ("Sent: %u packets\n", flows);
  return 0;
}

/******************************************************************************
 *
 * Name: ProcessPacket
 * Description:  analyzing packet and logging it if it is of interest
 * Arguments: pcap arguments (not used), pcap header, packet
 * Returns: void, in case of error sub-functions will report message and exit.
 *
 *****************************************************************************/
void ProcessPacket(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
  static timeval tv = { 0, 0 };
  static bool first = true;
  const struct sniff_ethernet *ethernet;  /* The ethernet header */
  const struct sniff_ip *ip;              /* The IP header */
  const struct sniff_tcp *tcp;            /* The TCP header */
  const struct sniff_udp *udp;		  /* The UDP header */

  if (!first)
  {
    unsigned int ts = (header->ts.tv_sec - tv.tv_sec) * 1000000 + header->ts.tv_usec - tv.tv_usec;
    usleep (ts);
    tv = header->ts;
  }
  else
  {
    first = false;
    tv = header->ts;
  }

  int size_ethernet = sizeof(struct sniff_ethernet);
  int size_ip = sizeof(struct sniff_ip);

  int tcp_packet_size;
  int udp_packet_size;
  char src_ip[15];
  char dst_ip[15];

  /* Point our structures to the right place in the packet */

  ethernet = (struct sniff_ethernet*)(packet);

  if (ntohs (ethernet->type) == 0x8100) //VLAN packet
  {
    ip = (struct sniff_ip*)(packet + size_ethernet + 4);
    tcp = (struct sniff_tcp*)(packet + size_ethernet + 4 + size_ip);
    udp = (struct sniff_udp*)(packet + size_ethernet + 4 + size_ip);  
  }
  else
  {
    ip = (struct sniff_ip*)(packet + size_ethernet);
    tcp = (struct sniff_tcp*)(packet + size_ethernet + size_ip);
    udp = (struct sniff_udp*)(packet + size_ethernet + size_ip);  
  }

  /* Check if this is an IP v4 packet */
  /* If not an IP v4 packet, return. Otherwise, continue. */

  if (ip->version != 4)
    return;
  else
  {
    /* IP packet with less than 20 bytes of headers?! Nonsense! */
    
    if (ip->hdrlen < 5) return;

    /* Get IP's, inet_ntoa overwrites statically allocated buffer in subsequent calls */
  
    strcpy(src_ip, inet_ntoa(ip->src));
    strcpy(dst_ip, inet_ntoa(ip->dst));

    int n;
    switch (ip->protocol)
    {
      /* OK, this looks like a TCP packet */    
    
      case 6:
        /* First, let's get the size of the packet */
        
        tcp_packet_size = ntohs(ip->len) - (ip->hdrlen + tcp->offset)*4;
        if (tcp_packet_size < 0)
          break;

        break;

      /* And this should be an UDP packet */

      case 17:

        /* Get the size of the UDP packet */
        
        udp_packet_size = ntohs(ip->len) - (ip->hdrlen + 2)*4;
        if (udp_packet_size < 0)
          break;

        n = sendto (sock, packet + size_ethernet + size_ip + sizeof(sniff_udp), udp_packet_size, 0, (const sockaddr *)&server, length);
        if (n < 0)
          error("Sendto");

        ++flows;
        if (flows % 10 == 0)
          usleep (2000);
        break;

      default:

        /* We are not really interested in anything other than TCP/UDP at this time */

        return;
    }
  }
}
