#ifndef __FLOWS__
#define __FLOWS__

#include <arpa/inet.h>
#include <list>
#include <set>

namespace netmon
{
  class buffer;

  class flow
  {
    public:
      flow (bool onlyLive = false)
        : m_onlyLiveData (onlyLive)
      {}

      virtual ~flow () {}

      uint32_t getSrc () const
      {
        return m_src;
      }

      uint32_t getDst () const
      {
        return m_dst;
      }

      uint32_t getNextHop () const
      {
        return m_nextHop;
      }

      uint16_t getInputInterface () const
      {
        return ntohs (m_iiface);
      }

      uint16_t getOutputInterface () const
      {
        return ntohs (m_oiface);
      }

      uint32_t getPackets () const
      {
        return ntohl (m_packets);
      }

      uint32_t getOctets () const
      {
        return ntohl (m_octets);
      }

      uint32_t getStartTime () const
      {
        return ntohl (m_startTime);
      }

      uint32_t getEndTime () const
      {
        return ntohl (m_endTime);
      }

      uint16_t getSrcPort () const
      {
        return ntohs (m_srcPort);
      }

      uint16_t getDstPort () const
      {
        return ntohs (m_dstPort);
      }

      uint8_t getProtocol () const
      {
        return m_protocol;
      }

      uint8_t getTOS () const
      {
        return m_tos;
      }

      uint8_t getTCPFlags () const
      {
        return m_tcpflags;
      }

    protected:
      virtual void deserialize (buffer &) = 0;

      bool m_onlyLiveData;
      uint32_t m_src;
      uint32_t m_dst;
      uint32_t m_nextHop;
      uint16_t m_iiface;
      uint16_t m_oiface;
      uint32_t m_packets;
      uint32_t m_octets;
      uint32_t m_startTime;
      uint32_t m_endTime;
      uint16_t m_srcPort;
      uint16_t m_dstPort;
      uint8_t m_protocol;
      uint8_t m_tos;
      uint8_t m_tcpflags;
  };

  class flow_base : public flow
  {
    public:
      flow_base (buffer &b)
        : flow ()
      {
        deserialize (b);
      }

    protected:
      virtual void deserialize (buffer &);
  };

  class flow_v1 : public flow_base
  {
    public:
      flow_v1 (buffer &b)
        : flow_base (b)
      {
        deserialize (b);
      }

    protected:
      virtual void deserialize (buffer &);
  };

  class flow_v5 : public flow_base
  {
    public:
      flow_v5 (buffer &b)
        : flow_base (b)
      {
        deserialize (b);
      }

      uint16_t getSrcAS () const
      {
        return ntohs (m_srcAS);
      }

      uint16_t getDstAS () const
      {
        return ntohs (m_dstAS);
      }

      uint8_t getSrcMask () const
      {
        return m_srcMask;
      }

      uint8_t getDstMask () const
      {
        return m_dstMask;
      }

    protected:
      virtual void deserialize (buffer &);

      uint16_t m_srcAS;
      uint16_t m_dstAS;
      uint8_t m_srcMask;
      uint8_t m_dstMask;
      uint8_t m_flags1; // this is not used in v5, but in v7
      uint16_t m_flags2; // this is not used in v5, but in v7
  };

  class flow_v7 : public flow_v5
  {
    public:
      flow_v7 (buffer &b)
        : flow_v5 (b)
      {
        deserialize (b);
      }

      uint8_t getFlags1 () const
      {
        return m_flags1;
      }

      uint16_t getFlags2 () const
      {
        return m_flags2;
      }

      uint32_t getRouterAddress () const
      {
        return m_routerAddr;
      }

    protected:
      virtual void deserialize (buffer &);

      uint32_t m_routerAddr;
  };

  typedef std::pair<uint16_t, uint16_t> tpair_t;
  typedef std::list<tpair_t> tlist_t;

  class flow_v9 : public flow
  {
    public:
      flow_v9 (buffer &b, tlist_t &t)
        : flow (), m_types (t)
      {
        deserialize (b);
      }

    protected:
      virtual void deserialize (buffer &);
      void handleType (buffer &, uint16_t, uint16_t);

      tlist_t &m_types;

    private:
      enum
      {
        eInBytes = 1,
        eInPackets = 2,
        eProtocol = 4,
        eTOS = 5,
        eTCPFlags = 6,
        eSrcPort = 7,
        eSrcAddr = 8,
        eInSNMP = 10,
        eDstPort = 11,
        eDstAddr = 12,
        eOutSNMP = 14,
        eLastSwitched = 21,
        eFirstSwitched = 22
      };

      typedef std::set<uint16_t> set_t;
      static set_t m_neededTypes;

      static set_t initSet ();
  };
}

#endif // __FLOWS__
