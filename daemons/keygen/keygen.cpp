#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <getopt.h>

#include "md5.h"

const char version[] = "1.1.0";

static struct option const long_options[] =
{
  {"help",       no_argument, 0, 'h'},
  {"version",    no_argument, 0, 'v'},
  {"mac",        no_argument, 0, 'm'},
  {"registration-key", required_argument, 0, 'k'},
  {"dev_limit",  required_argument, 0, 'd'},
  {"trial",      no_argument, 0, 't'},
  {"expires",    required_argument, 0, 'e'},
  {0, 0, 0, 0}
};

int printversion ()
{
  printf ("%s\n", version);
  return 0;
}

int usage ()
{
  printf ("Usage: keygen [-h] [-v] -k registration-key -d device-limit -m MAC Address\n");
  printf ("\t-h: gives this help\n");
  printf ("\t-v: prints version information\n");
  printf ("\t-k registration key: use this reg key\n");
  printf ("\t-d device-limit: device limit\n");
  printf ("\t-m MAC: use this MAC Address\n");
  printf ("\t-t: generate a trial key\n");

  return 1;
}

bool scanmac (const char *mac, unsigned char *n)
{
  unsigned int m[6];
  int ret;

  ret = sscanf (mac, "%02x:%02x:%02x:%02x:%02x:%02x", &m[0], &m[1], &m[2], &m[3], &m[4], &m[5]);
  if (ret != 6)
  {
    printf ("Illegal MAC address format\n");
    return false;
  }

  for (ret = 0; ret < 6; ++ret)
    n[ret] = m[ret];

  return true;
}

int main (int argc, char **argv)
{
  int c, longind, dev_limit;
  u_char addr[6];
  std::string mac;
  std::string cn;
  bool is_trial = false;
  char *dl = 0;
  char *date = 0;

  dev_limit = 0;

  while ((c = getopt_long (argc, argv, "thvk:d:e:m:", long_options, &longind)) != EOF)
  {
    switch (c)
    {
      case 'h':
        return usage();
      case 'v':
        return printversion();
      case 'm':
        mac = optarg;
        break;
      case 'k':
        cn = optarg;
        break;
      case 'd':
        dl = optarg;
        break;
      case 't':
        is_trial = true;
        break;
      case 'e':
        date = optarg;
        break;
      default:
        printf ("Illegal argument, use --help for help\n");
        exit (-1);
    }
  }

  if (cn.size() == 0)
  {
    printf ("Missing registration key.\n");
    return usage();
  }

  if (dl == 0)
  {
    printf ("Missing device_limit.\n");
    return usage();
  }

  if (is_trial && date == 0)
  {
    printf ("Missing expiration date.\n");
    return usage();
  }

  if (!scanmac (mac.c_str(), addr))
    return usage();

  char *err = dl;
  strtol (dl, &err, 10);
  if (*err != '\0')
  {
    printf ("Illegal value for device limit: %s\n", dl);
    return -1;
  }

  md5 md;
  md.update ((uint8 *) addr, 3);
  md.update ((uint8 *) cn.c_str(), cn.size());
  md.update ((uint8 *) addr + 3, 1);
  md.update ((uint8 *) dl, strlen (dl));
  md.update ((uint8 *) (is_trial ? "1" : "0"), 1);
  if (date != 0)
    md.update ((uint8 *) date, strlen (date));
  md.update ((uint8 *) addr + 4, 2);
  md5Digest d;
  md.report (d);

  printf ("%s\n", d.hex_digest());

  return 0;
}
